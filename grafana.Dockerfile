FROM grafana/grafana:11.2.0

# make the below possible...
USER root

# manipulate some things in the config directly as url_login via ENV does not seem to work
RUN sed -i 's/;url_login = false/url_login = true/g' /etc/grafana/grafana.ini

# drop privileges
USER 472
