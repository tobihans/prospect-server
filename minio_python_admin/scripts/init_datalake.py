import json
import os

from minio import Minio, MinioAdmin
from minio.credentials import StaticProvider
from minio.error import S3Error

ACCESS_KEY = os.environ.get("MINIO_ROOT_USER")
SECRET_KEY = os.environ.get("MINIO_ROOT_PASSWORD")
MINIO_HOST = os.environ.get("MINIO_HOST")
MINIO_PORT = os.environ.get("MINIO_PORT")
MINIO_CLIENT_HOST = "%s:%s" % (MINIO_HOST, MINIO_PORT)
MINIO_DATALAKE_BUCKET = os.environ.get("MINIO_DATALAKE_BUCKET")
MINIO_DATALAKE_WRITER_USERNAME = os.environ.get("MINIO_DATALAKE_WRITER_USERNAME")
MINIO_DATALAKE_WRITER_PASSWORD = os.environ.get("MINIO_DATALAKE_WRITER_PASSWORD")
MINIO_DATALAKE_READER_USERNAME = os.environ.get("MINIO_DATALAKE_READER_USERNAME")
MINIO_DATALAKE_READER_PASSWORD = os.environ.get("MINIO_DATALAKE_READER_PASSWORD")


WRITER_POLICY_DATALAKE = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:*"],
            "Resource": [f"arn:aws:s3:::{MINIO_DATALAKE_BUCKET}/*"],
        }
    ],
}

READER_POLICY_DATALAKE = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:GetBucketLocation", "s3:GetObject"],
            "Resource": [f"arn:aws:s3:::{MINIO_DATALAKE_BUCKET}/*"],
        }
    ],
}


def write_policy(filename, content):
    print("Writing policy", filename)
    f = open(filename, "w")
    f.write(json.dumps(content))
    f.close()


def create_bucket(bucketname, client):
    # creating the bucket if not exists
    if not client.bucket_exists(bucketname):
        client.make_bucket(bucketname)
        print("Created bucket", bucketname)
    else:
        print("Bucket", bucketname, "already exists")


def add_policy(policy, filename, admin_client):
    if policy not in admin_client.policy_list():
        print("Adding policy", policy)
        admin_client.policy_add(policy, policy_file=filename)
    else:
        print("Policy", policy, "already exists")


def add_user(username, password, admin_client):
    if username not in admin_client.user_list():
        print("Adding user", username)
        admin_client.user_add(access_key=username, secret_key=password)
    else:
        print("User", username, "already exists")


def attach_policy(policyname, username, admin_client):
    if policyname not in admin_client.user_info(access_key=username):
        admin_client.policy_set(policy_name=policyname, user=username)
        print("Attached Policy", policyname)
    else:
        print("Policy", policyname, "already attached")


def main():
    client = Minio(
        MINIO_CLIENT_HOST, access_key=ACCESS_KEY, secret_key=SECRET_KEY, secure=False
    )

    create_bucket(MINIO_DATALAKE_BUCKET, client)

    write_policy("datalake_writer.json", WRITER_POLICY_DATALAKE)
    write_policy("datalake_reader.json", READER_POLICY_DATALAKE)

    admin_client = MinioAdmin(
        MINIO_CLIENT_HOST,
        credentials=StaticProvider(access_key=ACCESS_KEY, secret_key=SECRET_KEY),
        secure=False,
    )

    add_policy("datalake_writer", "datalake_writer.json", admin_client)
    add_policy("datalake_reader", "datalake_reader.json", admin_client)

    add_user(
        MINIO_DATALAKE_WRITER_USERNAME, MINIO_DATALAKE_WRITER_PASSWORD, admin_client
    )
    add_user(
        MINIO_DATALAKE_READER_USERNAME, MINIO_DATALAKE_READER_PASSWORD, admin_client
    )

    attach_policy("datalake_writer", MINIO_DATALAKE_WRITER_USERNAME, admin_client)
    attach_policy("datalake_reader", MINIO_DATALAKE_READER_USERNAME, admin_client)


# main
if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("ERROR:", exc)
