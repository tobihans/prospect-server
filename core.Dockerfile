FROM ruby:3.2.2-slim-bookworm
ARG git_commit=unknown_commit

RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update -qq && \
    apt-get install -yqq --no-install-recommends \
        build-essential \
        # proper pager (for development)
        less \
        git \
        chromium \
        # lsb-release \
        gnupg2 \
        # for typhoeus gem
        curl \
        # libyaml not bundled anymore since 3.2
        libyaml-dev \
        # postgres client libraries
        libpq-dev \
        # needed for pg_dump
        postgresql-client \
        && rm -rf /var/lib/apt/lists/*

# Version 3.5.1 installs bundler 2.5.1. Keep in sync with "BUNDLED WITH" in Gemfile.lock
RUN gem update --system 3.5.1 --silent

WORKDIR /core

COPY core/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

ENV BUNDLE_PATH=/bundle \
    BUNDLE_CACHE_PATH=/bundle \
    BUNDLE_CACHE_ALL=/bundle \
    BUNDLE_DEFAULT_INSTALL_USES_PATH=/bundle
COPY core/Gemfile core/Gemfile.lock /core/
RUN bundle install

ENV GIT_COMMIT=$git_commit
COPY core /core
RUN bundle exec rake assets:precompile

ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000
