![Coverage](https://gitlab.com/prospect-energy/prospect-server/badges/main/coverage.svg?job=merge-coverage&key_text=Coverage&key_width=60)
![Core coverage](https://gitlab.com/prospect-energy/prospect-server/badges/main/coverage.svg?job=merge-rspec-coverage&key_text=Core+Coverage&key_width=90)
![Dataworker coverage](https://gitlab.com/prospect-energy/prospect-server/badges/main/coverage.svg?job=dataworker-tests&key_text=Dataworker+Coverage&key_width=130)
![Pipeline Status](https://gitlab.com/prospect-energy/prospect-server/badges/main/pipeline.svg)


# Prospect Server
---
**NOTE:**

Prospect is under heavy development and might change massively within short time frames. Please use current state of development for experimental use only

---

Prospect is a server run application for data gathering and visualization in field of renewable energy. It consists of a set of open source tools running in Docker containers and a RubyOnRails Application combining these parts of Software. See it up and running: https://app.prospect.energy/

---

Prospect Server is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation.

---

If you want to contribute to Prospect (e.g. connecting own data sources), you are most welcome. Please use our [developer wiki](https://gitlab.com/prospect-energy/prospect-server/-/wikis/home) to get started.


## General Software setup and Architecture

Please see the [wiki for more information](https://gitlab.com/prospect-energy/prospect-server/-/wikis/home).

## 1. Prerequisites

* Install [git](https://github.com/git-guides/install-git) for your OS
  * Windows only: run the following commands in your cmd terminal to not mix up [LF / CRLF](https://adaptivepatchwork.com/2012/03/01/mind-the-end-of-your-line/)
     ```
     git config --global core.autocrlf input
     git config --global core.eol lf
     ```
* Install the latest docker and docker compose for your OS: [Get Docker](https://docs.docker.com/get-docker/)
  * Ubuntu only: don't use snap, but [apt repository](https://docs.docker.com/engine/install/ubuntu/#installation-methods)
  * Windows only: docker setup will ask you to install Windows Subsystem for Linux ([WSL](https://learn.microsoft.com/en-us/windows/wsl/install))
* Checkout the [repo](https://gitlab.com/prospect-energy/prospect-server) in a folder of your choice by running the command in your system terminal (MacOSX: Terminal, Windows: cmd, Linux: Terminal). This folder will be the working directory for all following commands.
```
git clone https://gitlab.com/prospect-energy/prospect-server.git
```


## 2. Setting up the Dev environment

There are two supported options.
- The simpler one (2.1) runs all parts in docker managed by docker compose. This is the ideal option, if you want to test the project.
- If you want to deep dive and debug via your local IDE, you can choose to have the core/ruby project run on your local machine, whilst using docker compose for the rest of the infrastructure (2.2).

### 2.1 Docker compose - all batteries included

#### 2.1.1 Starting the engines

Make sure, you're in the root directory of the project, which contains `docker-compose.yml` as the recipe for the full prospect stack.

Start the whole set of containers with:

```bash
docker compose up     # run in foreground, CTRL-C will stop everything
docker compose up -d  # detach, you will be able to close the terminal
```

After a while dependencies or the containers might have bee updated, so to rebuild the containers run:

```bash
docker compose up --build
```

For the docker compose setup init scripts are already taking care of:
* Creating the db, precompile assets and starting the prospect core server
* Creating connection, users and buckets for minio
* Running pending DB migrations

#### 2.1.2 Stopping the engines

To stop everything, just use CTRL-C or if run in background use your Docker Desktop or

```bash
docker compose down
```

Next time you wanna start using it again, just use `docker compose up` again.
As all data is stored on persistent volumes you can continue to work where you left.

***That's it, you can start testing!***
Checkout **Overview about all endpoints** to get an overview about all endpoints.
Also checkout **Logging**, to get to know how to use the built-in logging system.

#### 2.1.3 Resetting the environment

Sometimes you might want to start with a clean state, so best just clear out the volumes. This will delete all the data.

```bash
docker compose down -v
```

#### 2.1.4 Code Changes

Even though the code is mounted dynamically into the respective containers, already running processes need to pick up changes.
To do this just restart the affected containers, this is much faster that taking everything just down and up again:

```bash
docker compose restart core       # restart core container, have it pick up changes to things like initializers
docker compose restart dataworker # restart dataworker container
```

Note: The core Rails App will normally automatically pick up all code changes within `core/app` directory on each page reload.

#### 2.1.5 Logs and Troubleshooting

Sometimes it is great to see what is going on within the containers:

```bash
docker compose logs                  # print logs of all containers
docker compose logs -f               # follow logs of all containers
docker compose logs core             # print the logs of the core container and exit
docker compose logs -f -t dataworker # follow the logs of the dataworker container with added timestamps
```

#### 2.1.6 Database Migrations

Normally when you (re)start the `core` docker container all pending DB migrations should be run.

For Devs writing migrations, that behavior might be unwanted, as they might have unfinished migrations.
If you do not want migration run automatically on core container start, just set the following in your local .env.

```
PROSPECT_AUTOMATIC_DB_MIGRATE=false
```

To run migrations manually open a bash inside the core container and run the migrations via the rake task:

```bash
docker compose exec core bash # Open a bash console inside the core service
bundle exec rake db:migrate   # run the database migrations
```

#### 2.2 Core app development

Normally the core app is running and developed with docker compose.

If you optionally want the app running locally on your machine and have the rest of the infrastructure managed by docker you can
follow the [instructions from the wiki](https://gitlab.com/prospect-energy/prospect-server/-/wikis/Development/Core-App-Local-Development)

Most of the commands below should be run from where the core application is running, so enter the core container with

```bash
docker compose exec core bash
```

The following command will set up the database and create the admin user. ( Normally done via docker compose)

```bash
bundle exec rake db:setup
```

You also might want to dynamically compile the css while developing the UI.
This tasks runs indefinite and will recreate all assets while developing, so give it an extra terminal to run.

```bash
bundle exec rails tailwindcss:watch
```

Start the development server (if not running via docker).
Then you will be able to log in with the credentials generated above: http://localhost:3000 :confetti_ball:

```bash
bundle exec rails s -p 3000 -b '127.0.0.1'  # locally
```

After code changes some migrations might need to be run, dependencies updated, so...

```bash
bundle install                # too lazy to rebuild container? just install dependencies
bundle exec rake db:migrate   # run database- (and some few data-) migrations
```

Get a repl inside the app

```bash
bundle exec rails c
```

Keep your code clean before committing, to not get caught by the CI later. So run the linter with

```bash
bundle exec rubocop     # just print violations
bundle exec rubocop -A  # autocorrect, 99% of the time, nothing breaks, but sometimes it does :)
```

Running the tests, using the [RSpec](https://rspec.info/) framework

```bash
bundle exec rspec
```

## 3. Overview about all endpoints

If you have followed the guide until here, you should have a working development setup by now. Check your `.env` as it contains the default credentials for all of these endpoints. The following endpoints should be available:

* Prospect App: [http://localhost:3000](http://localhost:3000)
  ```
  PROSPECT_ADMIN_USER='default@example.com'
  PROSPECT_ADMIN_PASSWORD='Oraech*ai2ve3Me7Cae9'
  ```
* Grafana: [http://localhost:3001](http://localhost:3001)
  ```
  GRAFANA_ADMIN_USER='admin'
  GRAFANA_ADMIN_PASSWORD='oozaimahv9iemaeDa5xu'
  ```
* Postgres: Access the DB through a SQL client
  ```
  POSTGRES_HOST='localhost'
  POSTGRES_PORT=5431
  POSTGRES_USER='postgres'
  POSTGRES_PASSWORD='postgres'
  POSTGRES_DATABASE='prospect_development'
  ```
* Minio: [http://localhost:9090](http://localhost:9090)
  ```
  MINIO_ROOT_USER=admin
  MINIO_ROOT_PASSWORD='Ri7icee1Iechiecu1roe'
  ```
* Faktory: [http://localhost:7420](http://localhost:7420)
  just leave the username blank for the basic auth, password is
  `FAKTORY_PASSWORD='faktorypassword'`

Overwrite these credentials if needed like described below.

## 4. Additional configurations

### ENV Variables

The app is mostly configured via ENV variables using the [dotenv-rails](https://github.com/bkeepers/dotenv) gem.
The respective files required for development already mounted via docker compose.
If you feel the need to override those locally, you can just use `core/.env.local` to overwrite all variables specified in `.env`.

### Google Auth
The App can also use Google Auth locally, if you need that then follow the steps for Google OAuth setup [here](https://support.google.com/cloud/answer/6158849?hl=en)
and enter the credentials in your `core/.env.local`. Set `GOOGLE_AUTH='true'`. Use `http://localhost:3000/session/callback?provider=google?` as authorized redirect URI.

## 5. Connectors

Prospect data connectors allow to import data into the system.

| Connector | Status | Description |
| ---       | ---    | ---         |
| manual_upload | ok | Allow upload of files directly into prospect |
| api_push | ok | Pushing data to the Prospect API |
| s3_bucket | ok | Regular checking of a S3 bucket for new files to import |
| google_drive | ok | Google Drive Connector provides functionality to use GDrive folders as source for imports. It needs to be setup with a Service Account (one per prospect instance). Users share their folders with this Service Account. Create and download ([instructions](https://developers.google.com/workspace/guides/create-credentials#service-account)) the credential json and place it were the application can read it. Adjust the `GOOGLE_DRIVE_CREDENTIAL_FILE` env variable in `.env` to the actual location. |
| api/angaza | ok | Importing data from [Angaza](https://payg.angazadesign.com/data/clients) |
| api/spark_meter | ok | Importing data from Spark API https://api.sparkmeter.io/ |
| api/upya | ok | Importing data from Upya APIs https://developer.upya.io/ |
| api/solarman | ok | Importing data from Solarman APIs https://home.solarmanpv.com/ |
| api/victron | ok | Importing data from Victron API https://vrmapi.victronenergy.com |
| api/stellar | ok | Importing data from Stellar API https://stellar.newsunroad.com |
| api/stellar_multi_org_parent | ok | This is a modified stellar connector that downloads meter data for many organizations in one API call. The StellarMultiOrgChild connector uses this data to make it available to specific organizations. Only the main organization on the platform can set up this connector. And it should do so only once.|
| api/stellar_multi_org_child | ok | Needs a working StellarMultiOrgParent connector. This connector doesn't connect to the stellar API. It gets data from StellarMultiOrgParent connector. This is why no credentials are needed to setup this connector by individual organizations. |

## 6. Main Organization

Members of the *Main Organization* can see/do more on the platform. This helps to hide complexity from normal users and allow for features that need a more centralized approach. Set the `MAIN_ORG_ID` env variable to the main organization's id.

Main organization features:
- setup Stellar Multi Org Parent connector

## 7. Beta: Importing Verasol product data

Admin users can navigate to Admin/Misc and upload the Verasol product data into our `common_products` db table. This is a beta feature as the common_products table is not yet used.

## 8. Logging

You can use the `docker logs` command to check the logs as well, e.g. the following command to check the logs of minio via the docker cli:

```bash
docker logs --follow prospect-server-minio-1
```

## 9. Production Deployment

Prospect Demo Core is automatically deployed. A successful gitlab pipeline run on main branch triggers redeployment.
See [event](https://gitlab.com/a2ei/prospect/event) project for more details.

In a production setup make sure to back up your `Rails.application.secret_key_base`.
It's the key you need to reproduce the pseudonymization step applied to all sensitive data.
Don't store it next to your database backups, because access to it would make it much easier for an attacker to reverse the pseudonymization.

**Warning**: When you set up a new production instance make sure you delete the default user (added by `seed.rb`).
Also use different credentials than the ones provided with the default environment variables and generate a new JWK key in place of the default one in: `docker_shared/jwks_pk.json`
via `docker compose run --rm core rake grafana:generate_jwk` for example.
If no key is present, the core container will also generate a new key, but Grafana will not start without one and is a required service for the core container.
This poses a little "hen-egg-problem", therefore the default key.
