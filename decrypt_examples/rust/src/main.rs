use std::{collections::HashMap, string::FromUtf8Error};

use aes::cipher::{KeyIvInit, StreamCipher};
use base64::{prelude::*, DecodeError};
use pbkdf2::pbkdf2_hmac;
use pkcs8::{
    pkcs5::{self, pbes2::Parameters, EncryptionScheme},
    DecodePrivateKey, EncryptedPrivateKeyInfo,
};
use rsa::{Oaep, RsaPrivateKey};
use sha2::Sha256;
use thiserror::Error;

struct Decrypter {
    key: RsaPrivateKey,
    shared_keys: HashMap<String, Decrypter>,
}

#[derive(Error, Debug)]
enum DecrypterError {
    #[error("Base64 decode error")]
    DecodeError(#[from] DecodeError),

    #[error("ConversionError (iv)")]
    ConversionError,

    #[error("TryIntoError")]
    TryIntoError,

    #[error("PKCS8 Error")]
    PKCS8Error(#[from] pkcs8::Error),

    #[error("PKCS5 Error: invalid salt or iv")]
    PKCS5Error(pkcs5::Error),

    #[error("RsaError")]
    RsaError(#[from] rsa::errors::Error),

    #[error("UTF8 Error")]
    FromUtf8Error(#[from] FromUtf8Error),
}

impl From<pkcs5::Error> for DecrypterError {
    fn from(err: pkcs5::Error) -> Self {
        DecrypterError::PKCS5Error(err)
    }
}

const MAX_KEY_LEN: usize = 32;

impl Decrypter {
    const ITERATIONS: u32 = 100000;

    fn new(
        wrapped_key: &str,
        iv: &str,
        salt: &str,
        password: &str,
        shared_keys: Vec<(&str, &str)>,
    ) -> Result<Decrypter, DecrypterError> {
        let salt: Vec<u8> = BASE64_STANDARD.decode(salt)?;
        let iv: [u8; 16] = BASE64_STANDARD
            .decode(iv)?
            .try_into()
            .map_err(|_| DecrypterError::ConversionError)?;
        let encrypted_data: Vec<u8> = BASE64_STANDARD.decode(wrapped_key)?;

        let parameters = Parameters::pbkdf2_sha256_aes256cbc(Decrypter::ITERATIONS, &salt, &iv)?;
        let encryption_algorithm = EncryptionScheme::Pbes2(parameters);
        let mut buffer = [0u8; MAX_KEY_LEN];
        // let key = EncryptionKey::derive_from_password(password.as_ref(), &parameters.kdf, parameters.encryption.key_size())?;
        pbkdf2_hmac::<sha2::Sha256>(password.as_bytes(), &salt, 100000, &mut buffer[..16]);

        let private_key_info: EncryptedPrivateKeyInfo = EncryptedPrivateKeyInfo {
            encryption_algorithm,
            encrypted_data: &encrypted_data,
        };

        let secret_document = private_key_info.decrypt(password)?;

        let key = RsaPrivateKey::from_pkcs8_der(secret_document.as_bytes())?;

        let mut shared_key_map = HashMap::new();

        for (id, skey) in shared_keys {
            let (_, aes, enc_msg) = Decrypter::split_msg(skey);

            let aes_parts = BASE64_STANDARD.decode(aes)?;
            let aes_parts: Vec<u8> = Decrypter::decrypt_rsa(&key, aes_parts)?;
            let aes_parts = String::from_utf8(aes_parts)?;

            let skey: Vec<u8> = Decrypter::decrypt_aes(&aes_parts, enc_msg)?;
            let rsa_key = RsaPrivateKey::from_pkcs8_der(&skey)?;

            shared_key_map.insert(
                id.to_string(),
                Decrypter {
                    key: rsa_key,
                    shared_keys: HashMap::new(),
                },
            );
        }

        Ok(Decrypter {
            key,
            shared_keys: shared_key_map,
        })
    }

    fn decrypt_to_string(&self, encrypted_msg: &str) -> Result<String, DecrypterError> {
        let result = self.decrypt(encrypted_msg)?;
        Ok(String::from_utf8(result)?)
    }

    fn decrypt(&self, encrypted_msg: &str) -> Result<Vec<u8>, DecrypterError> {
        let (id, aes, enc_msg) = Decrypter::split_msg(encrypted_msg);

        let key = if let Some(decrypter) = self.shared_keys.get(&id.to_string()) {
            &decrypter.key
        } else {
            &self.key
        };

        let aes_parts = BASE64_STANDARD.decode(aes)?;
        let aes_parts: Vec<u8> = Decrypter::decrypt_rsa(key, aes_parts)?;
        let aes_parts = String::from_utf8(aes_parts)?;

        Decrypter::decrypt_aes(&aes_parts, enc_msg)
    }

    fn decrypt_aes(aes_parts: &str, enc_msg: &str) -> Result<Vec<u8>, DecrypterError> {
        let aes_parts: Vec<&str> = aes_parts.split(':').collect();

        let key: [u8; 16] = BASE64_STANDARD
            .decode(aes_parts[0])?
            .as_slice()
            .try_into()
            .map_err(|_| DecrypterError::TryIntoError)?;

        let iv: [u8; 16] = BASE64_STANDARD
            .decode(aes_parts[1])?
            .as_slice()
            .try_into()
            .map_err(|_| DecrypterError::TryIntoError)?;

        type Aes128Ctr = ctr::Ctr128BE<aes::Aes128>;
        let mut aes_cipther = Aes128Ctr::new(&key.into(), &iv.into());

        let mut msg = BASE64_STANDARD.decode(enc_msg)?;

        aes_cipther.apply_keystream(msg.as_mut());

        Ok(msg)
    }

    fn split_msg(encrypted_msg: &str) -> (&str, &str, &str) {
        let parts: Vec<&str> = encrypted_msg.split(':').collect();
        (parts[0], parts[1], parts[2])
    }

    fn decrypt_rsa(key: &RsaPrivateKey, encrypted_msg: Vec<u8>) -> Result<Vec<u8>, DecrypterError> {
        Ok(key.decrypt(Oaep::new::<Sha256>(), &encrypted_msg)?)
    }
}

fn main() {
    // org 8
    let wrapped_key = "1l23l9/O3ynk6RoZTjlTB8CNcKrj3E6M2sBHM8u12TUjMJJzvNxBv5PjzI+tlp7ElqHVyDq4VzgyXMPQsLAt87dQYd/Aj2OJqHGzMPIZwoC1RNvuBnKufXMMrBQPk1usEWjBpUDlycOgsIottRObcX8kDvA+ustw6mzZt3JVmUfxqV/kthlxhRWhDDPuauYOcTfMaXwMill6Yf1lY5fq4DhdZbR+JIHlipMiGxSiumraPjYLzzttrVVTt/KIdAS0rZd5wm9q70D2eV5/cel/ZLhMwAXZgqD71G2UX5jBh/G/v0vB6w+qW4HHt3sOuT1HyTtH5feVV08qCEL157s8Ph/MB19NNXr5vQQ62oy0MqnANyUdMjuFHWWj3efP8rmCs5Se1VEQNe3/b0qxzX1gtq6C1PHToJBj8rKSYdClIv4kiJHZtlz6iwC9O1ONy0YRQJIVOYiOCcOQvGhEgs8H0CzuaAH4CxCsI6nyLvxTIXHw3U84zkkSAUn3W175IRn6JuHaOimszELhqjmymV3CNkn1PPHqBGhXTSu4p+kOrXVD8SBLv1SN8McNNlQ9qG9/iEo1Sj4RJ4+VCJINmJa4Ne5IXDaHnSentVNL+Vokb5Ff8CJSZZbWR4RjmdDUIarU7efZwcNsjb3kcHZhhS+OWYG1K1h2p6ilX/lTUU6J0iH/9WJecB3ZZJ63v8/piljJISSgC0SkmUJSjW8SpZJsSASeuufLpvWIshvhpKSaQ80gOuwNwlTXYABd32sebd6zSYLkcGENvLMOnUDyr7LSfvVaDZ0FOHKuwMylESzxCuSRVvDjDKfsNr5m0yJbyhi+TqEO9TS/6HEHjAJgC9RVibVgtdrowoCkvwuV5W3/kRT4iST6j8w16S2XR4n1FY0QXYTcjBmHmrqHIDUwNiSqB+W5oDeyiTHbEJjW23JbSlLvqRAehXbq/37+NK3LomckSKD+I87gIZdcUpoYfCm5CwZO25ThPw6tX1bi1o3BAJz2zM0mesaPJ+bZChaY8roZXIHX2D19eCBRZ4CAFPrTSinAKeLGbEvXOIAwUfdsCq5h315UXWFi6Jqz+XJxvp8qiATb/sNRsLWXqkxiHSk5CADfzRUFLly0P8DRm8tfs6L4cvnA9GO6QSHTflAoVKUvYo29H+j9nkVNgN4v0pVL5bD725JFDfOjvXcbpCZVy2gOGRUTMP8lFrQtgHikAShnakL4zjJjBrVprxYxoW25YhkKN3gZk7VurvN3050skM24kLSzONooUItGB36N5/hrtO47glDhHpuR87H4ncZg0sxpAkTG0ztkdCTUw3Oi3L2ZqI2mC+p0jAkuoUH1DLw8MRIRbta7SsHk+QkQQHnovV4IdhrQuBqj/egWqfV7yh3xdq1GK7TLHm9N23Gi8qbCaye0YLsZkL1ALFT7mCwizE1Ru5kE/yJ+iFR9dkWHy+rfJm6vK5DJ2IhzHno0tbYj3hTTj2j874qvqRfr4azd9gGvalpKv5Ux6TP0sr6XIKodsaNOKsnqbrnFc9vb622pGp3wZnBmg5WBOUmGjUt8akLqXhP/eCOpsTBoMojAEMpgzsdLUQLnuoWkrvgJGbOlhI5twV/pv8sxsSYL8rmxaQ==";
    let salt = "T+r9zYIxoGqk91y3HjcQPA==";
    let iv = "bXPmiAc/N2CmGMRBExL8QA==";
    let _public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0BD0YCHr+xmrC5ShZvaFTCFAT7R1sF06irmqQkxCeJosCOfB3OB9zICbeF6OyjKqDogvoJgYClsfzFB5YEGTGHrfLZ7JurDUMhSF9Ozr1Ad98rWv9kg9NR25upkKoTktneNSaXKtuWteLyvTDi1pzw0f8HgP3ro9c6HXIGBycMhWVb4OrvIKtj/09hcZ9ESJzpXmXA3SFyy1P0QVCsGa3phQ/erSXwGWcnUH7LijXTyTVJlcKZO9OCT6x6Y/H+xIjMpE3Ok13kYte/JTB5V68CllT+VZdAUd35a3tfNt0DUSiRd8LoA7AKvio53uv7DlVvGYVoiBJ48JdxHeINE3gQIDAQAB";
    let password = "super_secret_encryption_password";

    // "super secret message"
    let encrypted_msg = "8:DcSKPKLGQ9QNuEwz3MVL4EfKOLq2eEOq3hH6PodUev8R4QCbT4h9YitL8KhZCAGhQTBH8sF7OkFRJrUC5VknQ2Y+P4ZB67aLj43+SUidkjJpXuANaZxBqALuiPgmpYvlUHlkgWbno76gOpdH5cPoeq6fAXrwhlDKxKrfz7pCLUvMqZlXuEtLecOHan29cOZd0mqnMGAR2k0BlzVGzeg2hBGVSqolfJEHkwHpIEU89bjT2VCgKeFp7VrA+25LmNYkZO+uERxC8qXUmsb1/7k9dKNLuLqm1CFsJFJbEKvqntOJQu8j/SKy6XQ53uwQ9in8thgjyrM7KYO2J6tEqx18TA==:vfYPUnIDNKtyIkES2HrJ8bRrlZQ=";

    let shared_keys:Vec<(&str, &str)> = [("9", "8:hIynMGQfESisy2dK1NsXfvJhYBu6/kYnMIrVsGJ6j+z/2TFDd7nUdwu0a6rv9VB2CDQxOF5T3Hv7dNPTD/S17g8KSidjcvPLqwTvtyFlFUlsf6yrIEEaSM4kPYINInMS/uVlPJwyppdvdkAWfqebT72wtyzzX+xAQhOf8nz05j0o9Pu7tfnSTH2eumIpmbX5/mckgcIaLR7Y/5P6cuJ7unhgxGsTes3Z+EutfmasOXc2z4i4ZCt7/gQs9Tu5NeYJTNcSWsh3b1PmdEYP6mR1M48P01egSX4+mZwyBu0U2N8l6LCceH9YhUXUdzwdqOLT3Bw8n38kGuhX8DnjuMuEWA==:D4N3xDkGjVjv9vDU+Gl0UQwgroefFRt8UCqO7dm1tyBwZgAzRyRVrGgHcJaQPCeloTP0CXxk6/UB07jqUbTu33x9ivIKdEnFeJD8y5pq6/ZnRcxzJ2lNR4Z6fQ7QJuMlAl0Lv0AKvE6ATe/yrk1UGKFwXYWPlGGcMt8pahgnHQFPFh8dft/4REoasEGgPZWnhxbpczf5DfLjK2ogdLH0XQKKp6g5l+NWxvgg0/6JbFyuIsDaL0WUwLYdJFy9aTdQBbh7arF3qRlF8hux3qdLwREFxTaR2TEum0q9cqWNXgue5bm6NqeZj3OpQXgoJRtU/gcdJ59FAS/pvfn7MwcGPxRf2td0kguulPWCa7md5RB6in38eo5wHWApKNU1s+T4T3bWR7ynm9X8up7RoMl4ZIsqp3Jb+vvtedtAVp8Mmd4CYkdCi2FWcum0LVgkGEkS/VHGhSxO0ixf+Rqjy2cyMs6U1+mCPfVFDK61MZpqS+pfTwYEUcaR9ySANzvjLWXBjtPliqeO4LZ5aVUMoeNQCKS4bncqBgzxU4Xbm3vz4ZMOgSfk1vVaIRvR01uPf5CjQWaKnzmcyhBXAnhm1gi+K/PCgB/y8pRc1NNLjrql18pUuPOXxmu/i5wRsIp7l9hWbuq9GNPK6zn2FLpV0AlgpsVQMIJ+w4vIYcaCl+PdJa5q58HF6gJfS66PSOLy5KcQ3yuBzVhTa+3/MlSOOH//i5QaT7xcSAbmIkYhzyzgGhS8GNzBngoxzUGNLAF2/coNyNzrZANj75HCRt4p5muAGjvXIfaaFP5elrXuoWpQ/6bAuYoZBbVrBwy7SKrHimxm02i0tX7OEhKTyAUYdhaFftQ34yAvcGXU+yM60cVCbZXwR4oqF2LiBwQtk8VkiAgxDLCUaYfdHH1aABtFfOtbS4HOckH+or7d7Yx8lzsVfgl9tPerlMvAEFvSP6OTJrHBpQbj/uj7n3bSWlnyQd03hbho4BjzKw5xgqEmFZljnlOf7OLOCmGzcl+NDlrXN0R4BPkjPT3ukGWttUDsxVw/pKrS5SjpD/DQQz+1cUaZFfNpGvpVTLQOeFPnMSYNdFJcoFxXTQ88asHb7oUitHqUU6DHEO5koMYgPfpLRLBTVENBTNzM/iQHFR7zqP+kdgoYBhuNwF3aoQzK2K1a2MmQtf07YTiWjZ0/s0PFihZhVekHMfetTK+ib+VmXxMBytzZFe6e1gc8c8h3dyVLDGSuiMG0UTge11JdT7hOcwMF7k7mHX3ozS6ChyfgnXUNKTh1ogKQ7Ms9naWTPUvGN+jer3HPyQT5Iw2a/dsT2LtlE9WpkXUa3T+ZJufFF5ivseG43hkAL6yPbA4tBXAvJ0uXzqsm31LbrUhsRpy2KHoP6+MPHXTlkOgP20159N2cHUD7WkjRzfb0OW0tCd9+lNE3iaOVTUu6cLFci3YM2Z+4Fhziz5X7tRvpq5a0Ou4q221NvOyY29FnCARaTw5RZDgG5E+b57/q4ousfA83iTSqOUs/dLqmNX1cdUAq+gWY5i0tsjHps9WSAcP3VJQgO8Wh5sq8eHMCReGlMrEKPsWv6TJlgDvs3AhwaLxhVfhjWmHOcetaQrPmWGbm/PJL2n0+36g6nQ==")].to_vec();

    // org 9
    // let wrapped_key = "d1i9IbV2U/ddY3yBn6GrySTpzTIiaETEff1Z7NUwFY31DUM3gloagXyPZfGHx2VjlF+ryRU3yB8rcEAk/+IuqGdlDbVixqaXSP8qhCG/Xj4oLU9ngYTBE4KUoIbcaFlk9cvSY3L1pbzI+F4RvVLTrSnleBtEDkIcJwnf0DhzQH5k8i6nkPdNeDH9BEPGa/d4VfeTHVsCclwMslNHRR3acHUxjI6JhXmihMqqmAOR3osn+SXoJfADs5aC1EJrUohLWVrNjP2DodBIPA6jyn1HKVlD70RZgZYTpI2Ly1ZpgJQ/89k/Qs3dg/zICGiNsP9P9rK9uZD3ovn7M4/PdSjI0JaTXjx3ikSlZl8Fh+qLRSMjUhsx//xjkBUnOqPI/T/W6s+mD0l7WqacmB7Lfz1aHTBKocKn59iB37qsRobFy8kAWoJqX2Wq67Pce8SXPzsO9bH3e3uiCpNdz+pa6lqXqD2YzbsC8qlxJCFywVgZakBzjOKPj8DqYpn5aSu49195YrPyIZ3Vkko383qFcbulAb8fngBe7QCy91WBP4q1GOxjCYqRlMwSIqJivGL4isSBxUJEicjecFxOSTjuuC9CafwbYZ6buz2yHrmY2WkM61aekTXFy5SZOccD2RVHK32PUEYyEF6uwWGoi+fuob+QxiN2GpxFjCgP/kNmDMswfYjNaeDbQarrhuIgGl9V98mv+WSWOCRno6iHryLwtjUBqepnamwdlPnDjrHKDoeu/TxeSWuqSXAKaeozittGX+8fVxdjFlF+wm+3h4mpjm4boYRLV251F2RCyhw6lG+yBSCnhwVj2LYiJAQtrIVa5ZT0K746DRSReU/tseb3DXDz5bG+oG9S22EpuoLrX9IF8ImTZdT3QDWebhj46G/donPbrW2svCArGYMNOS106Wt0R2oP97obmsiBwTIoRDhHqWHng9xj0yYvu48p+sxzABZIZRmwLyDDVJwWIBHigZCo6gjcS/i7UM0qEN53lLZMVCSnrdlskpxICJMtM6OVdslZ+O2rAbtgYnQ1f3/l7l2ahj8CoOMF4sKwCJJ9HfRjllndRgN2s3iOT4glRDY2u+e4nOMoHs+P/OZvb2hInF/JptswAiUq7/3h48RhtKCulB3J+bGWDcGWkyR7lSWz0Y1SHCtpJlLlBcRAU+aUtj449mNn8Q4+BFTSPzK7NbVHlkOo4cZ/OZCcIiP+45spLThAnepfEXtDKed3RELbyN+1a1itH04fW7fqLQKXLLsbc4ipuz61PLau67fvCYDAOlob5nz0mcuyKy6SkvWKP3wbxRrpn9ZQVJQVHPsKr4TE8t45sPOa2ahM+oW39cuBzu3Lyt6uMsjOObPxjJWORoB0mH9H0Fh3rh10q9mNnojMyGQ31AthT9PjhlaU8VjFNj73DZ328Ry6ti7g5D/b9df0zvVkB3QyqrKHlx94wX71mpV8XBkjJ/UtORjhAmBgqj6Uu6teMnYfDVdt3P+hJpA2IedbUil9niWJIXltU84Fp2ICG0runRD/wFJIuEz0QEyIY4T5eSE36/vsJ64VQrV0Rzp/PKscssV/U7utEP9TPv5fkOqkNLJN1zwADwTezes8w1pzojJo6/7k+51iocLGiKgcB/7K0PsdbCcqSS5/k5w=";
    // let salt = "aQ549eK/w/K8aVtKty8qfg==";
    // let iv = "n4aXa6crgHwcma8HmGfsJQ==";
    // let _public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+4ZQPflVnsFc4GgXiYZ/rh6dQ0v5osX79hpjzGyMIroKY51ofi+4OfVUi2Pni2eZvWU9YKifpihxcVLvjtYn2geuTdhJ8ijhE97fPhqQI4BYtwZfeicj8U42Xq7RaqNk1KAiZcMrqtrMvl3Wc5E520DJYV9+CP6lFJnvfdby5k1ICp/8sZxZMNbBA0D+8+JZDJtS+azysZUMnCzKgMjAPXL8J7Cs6ULv5bWB2oVTZivVbidNF5zLV1tH726povNIi3xHBRrEqcIVz3PTmudf2aulO4+l4tE+d5t+Uc8NhLBueZZx9D7dnlpoKQnhFYMN0my05hOafIrnVN+WC1J3QIDAQAB";
    // let password = "super_secret_encryption_password";

    // "another super secret message"
    let shared_encrypted_msg = "9:B1Q7Ys+TAnZuoN9d+IA4XZ9AVoDzXgGyQMXWXw11aMPsmCDoLdARSqY7KkM79TyffTwFLYF6vv1wzXmZVV7JM/e/KewqjKO3olDl0uU427t7E0hcFrH1DRIf4UbjSA+/tG0qFWMSvKuSDhyQrmLqm1Jz2db7S83raVR3kD6vWA5YYtMfoCTgNQGJDXyAryVz5WRD/WAfdTnMbGMpg2swbbc9xA/lNXHrNYlqLPCVo1PXMl8SsHXVOnETAOb5ROcVCoQHzhIPBlNBFaBkARABI+C6M3L+2jKvBO7xEEuO4J/mPPKqFFf0w8GgX3fyN70zSs9B2XmCN8QJE1bBInXlCA==:YyFh910GphuFsiFKAAapowTrW8WFvtF99B+wTQ==";

    let decrypter = match Decrypter::new(wrapped_key, iv, salt, password, shared_keys) {
        Ok(decrypter) => decrypter,
        Err(e) => panic!("{}", e),
    };

    println!("Msg: {:?}", decrypter.decrypt_to_string(encrypted_msg));
    println!(
        "shared Msg: {:?}",
        decrypter.decrypt_to_string(shared_encrypted_msg)
    );
}
