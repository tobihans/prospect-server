async function import_crypto() {
    const crypto = await import("node:crypto");
}

class Decrypt {
    privateKey
    enc_shared_keys
    shared_keys
    saltArrayBuffer
    ivArrayBuffer
    wrappedKeyBuffer

    constructor(params) {
        this.buildBytesFromParams(params.wrapped_key, params.salt, params.iv)
        this.enc_shared_keys = params.enc_shared_keys
        this.shared_keys = {}
    }

    buildBytesFromParams(wKeyString, saltString, ivString) {
        this.wrappedKeyBuffer = this.base64ToArrayBuffer(wKeyString);
        this.saltArrayBuffer = this.base64ToArrayBuffer(saltString);
        this.ivArrayBuffer = this.base64ToArrayBuffer(ivString);
    }

    async setPasswordUnwrapKey(password) {
        this.privateKey = await this.unwrapPrivateKey(password);
        this.decryptSharedKeys();
    }

    async setPrivateKey(key_b64) {
        const key = this.base64ToArrayBuffer(key_b64)
        this.privateKey = await crypto.subtle.importKey("pkcs8", key, { name: "RSA-OAEP", hash: "SHA-256" }, true, ["decrypt"])
        this.decryptSharedKeys();
    }

    async decryptSharedKeys() {
        for (const [id, enc_key] of this.enc_shared_keys) {
            const array_buffer = (await this.decryptMessage(enc_key, false));

            const key = await crypto.subtle.importKey(
                "pkcs8",
                array_buffer,
                {
                    name: "RSA-OAEP",
                    hash: "SHA-256"
                },
                false,
                ["decrypt"]
            )
            this.shared_keys[id] = key
        }
    }

    base64ToArrayBuffer(base64) {
        return Uint8Array.from(atob(base64), char => char.charCodeAt(0))
    }

    getKeyMaterial(password) {
        const enc = new TextEncoder();
        return crypto.subtle.importKey(
            "raw",
            enc.encode(password),
            { name: "PBKDF2" },
            false,
            ["deriveBits", "deriveKey"],
        );
    }

    async getUnwrappingKey(password) {
        // get the key material (user-supplied password)
        const keyMaterial = await this.getKeyMaterial(password);

        // derive the key from key material and salt
        return crypto.subtle.deriveKey(
            {
                name: "PBKDF2",
                salt: this.saltArrayBuffer,
                iterations: 100000,
                hash: "SHA-256",
            },
            keyMaterial,
            { name: "AES-CBC", length: 256 },
            true,
            ["wrapKey", "unwrapKey"],
        );
    }

    async unwrapPrivateKey(password) {
        // 1. get the unwrapping key
        const unwrappingKey = await this.getUnwrappingKey(password);

        try {
            return await crypto.subtle.unwrapKey(
                "pkcs8", // import format
                this.wrappedKeyBuffer, // ArrayBuffer representing key to unwrap
                unwrappingKey, // CryptoKey representing key encryption key
                {
                    // algorithm params for key encryption key
                    name: "AES-CBC",
                    iv: this.ivArrayBuffer,
                },
                {
                    // algorithm params for key to unwrap
                    name: "RSA-OAEP",
                    hash: "SHA-256",
                },
                true, // extractability of key to unwrap
                ["decrypt"] // key usages for key to unwrap
            );

        } catch (e) {
            console.error(`Could not unwrap the private key: ${e}`);
            alert("It seems the password was not correct - please try again");
            this.password = null;
            throw "Incorrect password"
        }
    }

    // checking base64 via regex can fail if the string to test is way to long, so we try this:
    // https://stackoverflow.com/questions/77464085/validate-base64-regex-causes-rangeerror-maximum-call-stack-size-exceeded
    isValidBase64(msg) {
        try {
            return msg.length > 3 && !(msg.length % 4) && atob(msg) && true;
        } catch (err) {
            return false;
        }
    }

    checkBase64AndConvertToArrayBuffer(enc_msg) {
        if (!this.isValidBase64(enc_msg)) {
            console.error(`The encoded data is not base64 encoded:${enc_msg}`);
            return null;
        }
        return this.base64ToArrayBuffer(enc_msg);
    }

    async decryptRSA(enc_msg, rsa_key) {
        const decrypted = await crypto.subtle.decrypt(
            {
                name: "RSA-OAEP"
            },
            rsa_key,
            enc_msg,
        );
        return decrypted;
    }

    async decryptAES(aes_data, enc_msg) {
        const [key, iv] = aes_data.split(":")

        const aes_key = await crypto.subtle.importKey(
            "raw",
            this.base64ToArrayBuffer(key),
            {
                name: "AES-CTR"
            },
            false,
            ["decrypt"]
        )

        const msg = await crypto.subtle.decrypt(
            {
                name: "AES-CTR",
                counter: this.base64ToArrayBuffer(iv), // intuitive naming...
                length: 128
            },
            aes_key,
            enc_msg,
        );
        return msg;
    }

    // the encrypted messages have two parts, the first is the encrypted AES key that needs to be decrypted via the
    // private RSA key and the second part is the actual AES encrypted message
    async decryptMessage(message, encode = true, rsa_key = this.privateKey) {
        if (message === null || message === undefined || message.length === 0) {
            return null
        }

        const dec = new TextDecoder();

        // message could be a pure RSA encrypted one or AES and RSA combined
        try {
            if (message.includes(":")) {
                const [id, aes, enc_msg] = message.split(':');
                let use_rsa_key;
                if (this.shared_keys[id]) {
                    use_rsa_key = this.shared_keys[id]
                } else {
                    use_rsa_key = rsa_key
                }
                const aes_data = await this.decryptMessage(aes, true, use_rsa_key)
                const enc_msg_ab = this.checkBase64AndConvertToArrayBuffer(enc_msg)

                const decrypted = await this.decryptAES(aes_data, enc_msg_ab)
                if (encode) {
                    return dec.decode(decrypted)
                }
                return decrypted
            }

            const enc_msg_ab = this.checkBase64AndConvertToArrayBuffer(message)
            const msg = await this.decryptRSA(enc_msg_ab, rsa_key)
            if (encode) {
                return dec.decode(msg)
            }
            return msg
        } catch (e) {
            console.error(`Could not decrypt the message: ${message}; Error: ${e}`);
            return null;
        }
    }
}

import_crypto()

let wrapped_key = "1l23l9/O3ynk6RoZTjlTB8CNcKrj3E6M2sBHM8u12TUjMJJzvNxBv5PjzI+tlp7ElqHVyDq4VzgyXMPQsLAt87dQYd/Aj2OJqHGzMPIZwoC1RNvuBnKufXMMrBQPk1usEWjBpUDlycOgsIottRObcX8kDvA+ustw6mzZt3JVmUfxqV/kthlxhRWhDDPuauYOcTfMaXwMill6Yf1lY5fq4DhdZbR+JIHlipMiGxSiumraPjYLzzttrVVTt/KIdAS0rZd5wm9q70D2eV5/cel/ZLhMwAXZgqD71G2UX5jBh/G/v0vB6w+qW4HHt3sOuT1HyTtH5feVV08qCEL157s8Ph/MB19NNXr5vQQ62oy0MqnANyUdMjuFHWWj3efP8rmCs5Se1VEQNe3/b0qxzX1gtq6C1PHToJBj8rKSYdClIv4kiJHZtlz6iwC9O1ONy0YRQJIVOYiOCcOQvGhEgs8H0CzuaAH4CxCsI6nyLvxTIXHw3U84zkkSAUn3W175IRn6JuHaOimszELhqjmymV3CNkn1PPHqBGhXTSu4p+kOrXVD8SBLv1SN8McNNlQ9qG9/iEo1Sj4RJ4+VCJINmJa4Ne5IXDaHnSentVNL+Vokb5Ff8CJSZZbWR4RjmdDUIarU7efZwcNsjb3kcHZhhS+OWYG1K1h2p6ilX/lTUU6J0iH/9WJecB3ZZJ63v8/piljJISSgC0SkmUJSjW8SpZJsSASeuufLpvWIshvhpKSaQ80gOuwNwlTXYABd32sebd6zSYLkcGENvLMOnUDyr7LSfvVaDZ0FOHKuwMylESzxCuSRVvDjDKfsNr5m0yJbyhi+TqEO9TS/6HEHjAJgC9RVibVgtdrowoCkvwuV5W3/kRT4iST6j8w16S2XR4n1FY0QXYTcjBmHmrqHIDUwNiSqB+W5oDeyiTHbEJjW23JbSlLvqRAehXbq/37+NK3LomckSKD+I87gIZdcUpoYfCm5CwZO25ThPw6tX1bi1o3BAJz2zM0mesaPJ+bZChaY8roZXIHX2D19eCBRZ4CAFPrTSinAKeLGbEvXOIAwUfdsCq5h315UXWFi6Jqz+XJxvp8qiATb/sNRsLWXqkxiHSk5CADfzRUFLly0P8DRm8tfs6L4cvnA9GO6QSHTflAoVKUvYo29H+j9nkVNgN4v0pVL5bD725JFDfOjvXcbpCZVy2gOGRUTMP8lFrQtgHikAShnakL4zjJjBrVprxYxoW25YhkKN3gZk7VurvN3050skM24kLSzONooUItGB36N5/hrtO47glDhHpuR87H4ncZg0sxpAkTG0ztkdCTUw3Oi3L2ZqI2mC+p0jAkuoUH1DLw8MRIRbta7SsHk+QkQQHnovV4IdhrQuBqj/egWqfV7yh3xdq1GK7TLHm9N23Gi8qbCaye0YLsZkL1ALFT7mCwizE1Ru5kE/yJ+iFR9dkWHy+rfJm6vK5DJ2IhzHno0tbYj3hTTj2j874qvqRfr4azd9gGvalpKv5Ux6TP0sr6XIKodsaNOKsnqbrnFc9vb622pGp3wZnBmg5WBOUmGjUt8akLqXhP/eCOpsTBoMojAEMpgzsdLUQLnuoWkrvgJGbOlhI5twV/pv8sxsSYL8rmxaQ==";
let salt = "T+r9zYIxoGqk91y3HjcQPA==";
let iv = "bXPmiAc/N2CmGMRBExL8QA==";
let _public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0BD0YCHr+xmrC5ShZvaFTCFAT7R1sF06irmqQkxCeJosCOfB3OB9zICbeF6OyjKqDogvoJgYClsfzFB5YEGTGHrfLZ7JurDUMhSF9Ozr1Ad98rWv9kg9NR25upkKoTktneNSaXKtuWteLyvTDi1pzw0f8HgP3ro9c6HXIGBycMhWVb4OrvIKtj/09hcZ9ESJzpXmXA3SFyy1P0QVCsGa3phQ/erSXwGWcnUH7LijXTyTVJlcKZO9OCT6x6Y/H+xIjMpE3Ok13kYte/JTB5V68CllT+VZdAUd35a3tfNt0DUSiRd8LoA7AKvio53uv7DlVvGYVoiBJ48JdxHeINE3gQIDAQAB";
let password = "super_secret_encryption_password";

// shared encryption key from another organization to encrypt shared data
const shared_keys = [[9, "8:hIynMGQfESisy2dK1NsXfvJhYBu6/kYnMIrVsGJ6j+z/2TFDd7nUdwu0a6rv9VB2CDQxOF5T3Hv7dNPTD/S17g8KSidjcvPLqwTvtyFlFUlsf6yrIEEaSM4kPYINInMS/uVlPJwyppdvdkAWfqebT72wtyzzX+xAQhOf8nz05j0o9Pu7tfnSTH2eumIpmbX5/mckgcIaLR7Y/5P6cuJ7unhgxGsTes3Z+EutfmasOXc2z4i4ZCt7/gQs9Tu5NeYJTNcSWsh3b1PmdEYP6mR1M48P01egSX4+mZwyBu0U2N8l6LCceH9YhUXUdzwdqOLT3Bw8n38kGuhX8DnjuMuEWA==:D4N3xDkGjVjv9vDU+Gl0UQwgroefFRt8UCqO7dm1tyBwZgAzRyRVrGgHcJaQPCeloTP0CXxk6/UB07jqUbTu33x9ivIKdEnFeJD8y5pq6/ZnRcxzJ2lNR4Z6fQ7QJuMlAl0Lv0AKvE6ATe/yrk1UGKFwXYWPlGGcMt8pahgnHQFPFh8dft/4REoasEGgPZWnhxbpczf5DfLjK2ogdLH0XQKKp6g5l+NWxvgg0/6JbFyuIsDaL0WUwLYdJFy9aTdQBbh7arF3qRlF8hux3qdLwREFxTaR2TEum0q9cqWNXgue5bm6NqeZj3OpQXgoJRtU/gcdJ59FAS/pvfn7MwcGPxRf2td0kguulPWCa7md5RB6in38eo5wHWApKNU1s+T4T3bWR7ynm9X8up7RoMl4ZIsqp3Jb+vvtedtAVp8Mmd4CYkdCi2FWcum0LVgkGEkS/VHGhSxO0ixf+Rqjy2cyMs6U1+mCPfVFDK61MZpqS+pfTwYEUcaR9ySANzvjLWXBjtPliqeO4LZ5aVUMoeNQCKS4bncqBgzxU4Xbm3vz4ZMOgSfk1vVaIRvR01uPf5CjQWaKnzmcyhBXAnhm1gi+K/PCgB/y8pRc1NNLjrql18pUuPOXxmu/i5wRsIp7l9hWbuq9GNPK6zn2FLpV0AlgpsVQMIJ+w4vIYcaCl+PdJa5q58HF6gJfS66PSOLy5KcQ3yuBzVhTa+3/MlSOOH//i5QaT7xcSAbmIkYhzyzgGhS8GNzBngoxzUGNLAF2/coNyNzrZANj75HCRt4p5muAGjvXIfaaFP5elrXuoWpQ/6bAuYoZBbVrBwy7SKrHimxm02i0tX7OEhKTyAUYdhaFftQ34yAvcGXU+yM60cVCbZXwR4oqF2LiBwQtk8VkiAgxDLCUaYfdHH1aABtFfOtbS4HOckH+or7d7Yx8lzsVfgl9tPerlMvAEFvSP6OTJrHBpQbj/uj7n3bSWlnyQd03hbho4BjzKw5xgqEmFZljnlOf7OLOCmGzcl+NDlrXN0R4BPkjPT3ukGWttUDsxVw/pKrS5SjpD/DQQz+1cUaZFfNpGvpVTLQOeFPnMSYNdFJcoFxXTQ88asHb7oUitHqUU6DHEO5koMYgPfpLRLBTVENBTNzM/iQHFR7zqP+kdgoYBhuNwF3aoQzK2K1a2MmQtf07YTiWjZ0/s0PFihZhVekHMfetTK+ib+VmXxMBytzZFe6e1gc8c8h3dyVLDGSuiMG0UTge11JdT7hOcwMF7k7mHX3ozS6ChyfgnXUNKTh1ogKQ7Ms9naWTPUvGN+jer3HPyQT5Iw2a/dsT2LtlE9WpkXUa3T+ZJufFF5ivseG43hkAL6yPbA4tBXAvJ0uXzqsm31LbrUhsRpy2KHoP6+MPHXTlkOgP20159N2cHUD7WkjRzfb0OW0tCd9+lNE3iaOVTUu6cLFci3YM2Z+4Fhziz5X7tRvpq5a0Ou4q221NvOyY29FnCARaTw5RZDgG5E+b57/q4ousfA83iTSqOUs/dLqmNX1cdUAq+gWY5i0tsjHps9WSAcP3VJQgO8Wh5sq8eHMCReGlMrEKPsWv6TJlgDvs3AhwaLxhVfhjWmHOcetaQrPmWGbm/PJL2n0+36g6nQ=="]];
// "super secret message"
const encrypted_msg = "8:DcSKPKLGQ9QNuEwz3MVL4EfKOLq2eEOq3hH6PodUev8R4QCbT4h9YitL8KhZCAGhQTBH8sF7OkFRJrUC5VknQ2Y+P4ZB67aLj43+SUidkjJpXuANaZxBqALuiPgmpYvlUHlkgWbno76gOpdH5cPoeq6fAXrwhlDKxKrfz7pCLUvMqZlXuEtLecOHan29cOZd0mqnMGAR2k0BlzVGzeg2hBGVSqolfJEHkwHpIEU89bjT2VCgKeFp7VrA+25LmNYkZO+uERxC8qXUmsb1/7k9dKNLuLqm1CFsJFJbEKvqntOJQu8j/SKy6XQ53uwQ9in8thgjyrM7KYO2J6tEqx18TA==:vfYPUnIDNKtyIkES2HrJ8bRrlZQ=";

// encrypted messages of another organization
// "another super secret message"
const shared_encrypted_msg = "9:B1Q7Ys+TAnZuoN9d+IA4XZ9AVoDzXgGyQMXWXw11aMPsmCDoLdARSqY7KkM79TyffTwFLYF6vv1wzXmZVV7JM/e/KewqjKO3olDl0uU427t7E0hcFrH1DRIf4UbjSA+/tG0qFWMSvKuSDhyQrmLqm1Jz2db7S83raVR3kD6vWA5YYtMfoCTgNQGJDXyAryVz5WRD/WAfdTnMbGMpg2swbbc9xA/lNXHrNYlqLPCVo1PXMl8SsHXVOnETAOb5ROcVCoQHzhIPBlNBFaBkARABI+C6M3L+2jKvBO7xEEuO4J/mPPKqFFf0w8GgX3fyN70zSs9B2XmCN8QJE1bBInXlCA==:YyFh910GphuFsiFKAAapowTrW8WFvtF99B+wTQ==";

const params = { wrapped_key: wrapped_key, salt: salt, iv: iv, enc_shared_keys: shared_keys }
const decrypt = new Decrypt(params)

// decrypt both messages, first is directly encrypted for organization 8
// and second message is encryption for organization 9 but key is shared with organization 8
decrypt.setPasswordUnwrapKey(password).then(async ()=>{
    let decrypted_message = await decrypt.decryptMessage(encrypted_msg);
    console.log(`Msg1: ${decrypted_message}`)

    decrypted_message = await decrypt.decryptMessage(shared_encrypted_msg);
    console.log(`Msg2: ${decrypted_message}`)

}).then(() => {
    // encrypt the shared message from organization 9 directly
    wrapped_key = "d1i9IbV2U/ddY3yBn6GrySTpzTIiaETEff1Z7NUwFY31DUM3gloagXyPZfGHx2VjlF+ryRU3yB8rcEAk/+IuqGdlDbVixqaXSP8qhCG/Xj4oLU9ngYTBE4KUoIbcaFlk9cvSY3L1pbzI+F4RvVLTrSnleBtEDkIcJwnf0DhzQH5k8i6nkPdNeDH9BEPGa/d4VfeTHVsCclwMslNHRR3acHUxjI6JhXmihMqqmAOR3osn+SXoJfADs5aC1EJrUohLWVrNjP2DodBIPA6jyn1HKVlD70RZgZYTpI2Ly1ZpgJQ/89k/Qs3dg/zICGiNsP9P9rK9uZD3ovn7M4/PdSjI0JaTXjx3ikSlZl8Fh+qLRSMjUhsx//xjkBUnOqPI/T/W6s+mD0l7WqacmB7Lfz1aHTBKocKn59iB37qsRobFy8kAWoJqX2Wq67Pce8SXPzsO9bH3e3uiCpNdz+pa6lqXqD2YzbsC8qlxJCFywVgZakBzjOKPj8DqYpn5aSu49195YrPyIZ3Vkko383qFcbulAb8fngBe7QCy91WBP4q1GOxjCYqRlMwSIqJivGL4isSBxUJEicjecFxOSTjuuC9CafwbYZ6buz2yHrmY2WkM61aekTXFy5SZOccD2RVHK32PUEYyEF6uwWGoi+fuob+QxiN2GpxFjCgP/kNmDMswfYjNaeDbQarrhuIgGl9V98mv+WSWOCRno6iHryLwtjUBqepnamwdlPnDjrHKDoeu/TxeSWuqSXAKaeozittGX+8fVxdjFlF+wm+3h4mpjm4boYRLV251F2RCyhw6lG+yBSCnhwVj2LYiJAQtrIVa5ZT0K746DRSReU/tseb3DXDz5bG+oG9S22EpuoLrX9IF8ImTZdT3QDWebhj46G/donPbrW2svCArGYMNOS106Wt0R2oP97obmsiBwTIoRDhHqWHng9xj0yYvu48p+sxzABZIZRmwLyDDVJwWIBHigZCo6gjcS/i7UM0qEN53lLZMVCSnrdlskpxICJMtM6OVdslZ+O2rAbtgYnQ1f3/l7l2ahj8CoOMF4sKwCJJ9HfRjllndRgN2s3iOT4glRDY2u+e4nOMoHs+P/OZvb2hInF/JptswAiUq7/3h48RhtKCulB3J+bGWDcGWkyR7lSWz0Y1SHCtpJlLlBcRAU+aUtj449mNn8Q4+BFTSPzK7NbVHlkOo4cZ/OZCcIiP+45spLThAnepfEXtDKed3RELbyN+1a1itH04fW7fqLQKXLLsbc4ipuz61PLau67fvCYDAOlob5nz0mcuyKy6SkvWKP3wbxRrpn9ZQVJQVHPsKr4TE8t45sPOa2ahM+oW39cuBzu3Lyt6uMsjOObPxjJWORoB0mH9H0Fh3rh10q9mNnojMyGQ31AthT9PjhlaU8VjFNj73DZ328Ry6ti7g5D/b9df0zvVkB3QyqrKHlx94wX71mpV8XBkjJ/UtORjhAmBgqj6Uu6teMnYfDVdt3P+hJpA2IedbUil9niWJIXltU84Fp2ICG0runRD/wFJIuEz0QEyIY4T5eSE36/vsJ64VQrV0Rzp/PKscssV/U7utEP9TPv5fkOqkNLJN1zwADwTezes8w1pzojJo6/7k+51iocLGiKgcB/7K0PsdbCcqSS5/k5w="
    salt = "aQ549eK/w/K8aVtKty8qfg==";
    iv = "n4aXa6crgHwcma8HmGfsJQ==";
    _public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+4ZQPflVnsFc4GgXiYZ/rh6dQ0v5osX79hpjzGyMIroKY51ofi+4OfVUi2Pni2eZvWU9YKifpihxcVLvjtYn2geuTdhJ8ijhE97fPhqQI4BYtwZfeicj8U42Xq7RaqNk1KAiZcMrqtrMvl3Wc5E520DJYV9+CP6lFJnvfdby5k1ICp/8sZxZMNbBA0D+8+JZDJtS+azysZUMnCzKgMjAPXL8J7Cs6ULv5bWB2oVTZivVbidNF5zLV1tH726povNIi3xHBRrEqcIVz3PTmudf2aulO4+l4tE+d5t+Uc8NhLBueZZx9D7dnlpoKQnhFYMN0my05hOafIrnVN+WC1J3QIDAQAB"
    password = "super_secret_encryption_password";

    const params = { wrapped_key: wrapped_key, salt: salt, iv: iv, enc_shared_keys: [] }
    const decrypt = new Decrypt(params)

    decrypt.setPasswordUnwrapKey(password).then(async ()=>{
        decrypted_message = await decrypt.decryptMessage(shared_encrypted_msg);
        console.log(`Msg3: ${decrypted_message}`)
    })
})
