FROM python:3

WORKDIR /scripts

COPY minio_python_admin/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./init_datalake.py" ]
