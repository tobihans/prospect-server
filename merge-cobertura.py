#!/usr/bin/env python
"""
Cobertura XML report merger.

Written for merging OpenCppCoverage reports, thus not completely supporting all
Cobertura XML attributes, only line coverage which OpenCppCoverage generates.
Original version found at:
https://github.com/gammu/gammu/blob/master/contrib/coveragehelper/merge-cobertura.py
Was adapted to extract infos from ruby and python coverage modules.
Several attributes may simply discarded in the process.

Copyright (C) 2018 Michal Cihar <michal@cihar.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import argparse
from collections.abc import Iterable
import glob
import logging
import sys
from typing import IO
from xml.etree.ElementTree import Element, ElementTree, SubElement, iterparse

logging.basicConfig(
    level=logging.INFO,
    # format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ],
)
HEADER = b"""<?xml version="1.0"?>
<!DOCTYPE coverage SYSTEM "https://raw.githubusercontent.com/cobertura/web/master/htdocs/xml/coverage-04.dtd">
"""


def read_files(names: Iterable[str], exclude_prefix: str) -> dict:
    """Read coverage data from input files."""
    result = {}
    lines = {}
    outfile = None
    logging.info("Reading files:")

    for filename in names:
        logging.info(f"* {filename}")
        for event, elem in iterparse(filename, events=("start", "end")):
            if elem.tag == "class" and event == "start":
                outfile = elem.get("filename")
                lines = result.get(outfile, {})
            elif elem.tag == "line" and event == "end":
                line = elem.get("number")
                lines[line] = lines.get(line, 0) + int(elem.get("hits"))
            elif elem.tag == "class" and event == "end":
                if not exclude_prefix or not outfile.startswith(exclude_prefix):
                    result[outfile] = lines
    logging.info("")

    return result


def get_line_rates(data: dict) -> dict:
    """Calculate line hit rates from raw coverage data."""
    result = {}
    total_lines = 0
    total_hits = 0
    logging.info("Counting line rates:")
    for item in data:
        lines = len(data[item])
        if not lines:
            continue
        hits = sum(1 for x in data[item].values() if x)
        result[item] = 1.0 * hits / lines
        logging.info(f" * {item} = {result[item]} ({hits} / {lines})")
        total_lines += lines
        total_hits += hits

    result["_"] = 1.0 * total_hits / total_lines
    result["_hits"] = total_hits
    result["_lines"] = total_lines

    return result


def write_data(data: dict, handle: IO):
    """Write Cobertura XML for coverage data."""
    line_rates = get_line_rates(data)

    logging.info("Generating output...")
    root = Element("coverage")
    root.set("line-rate", str(line_rates["_"]))
    root.set("branch-rate", "0")
    root.set("complexity", "0")
    root.set("branches-covered", "0")
    root.set("branches-valid", "0")
    root.set("timestamp", "0")
    root.set("lines-covered", str(line_rates["_hits"]))
    root.set("lines-valid", str(line_rates["_lines"]))
    root.set("version", "0")

    packages = SubElement(root, "packages")
    package = SubElement(packages, "package")
    package.set("name", "Prospect Server")
    package.set("line-rate", str(line_rates["_"]))
    package.set("branch-rate", "0")
    package.set("complexity", "0")
    classes = SubElement(package, "classes")

    for item in data:
        if item not in line_rates:
            continue
        obj = SubElement(classes, "class")
        # NB: only useful in Windows world
        obj.set("name", item.rsplit("\\", 1)[-1])
        obj.set("filename", item)
        obj.set("line-rate", str(line_rates[item]))
        obj.set("branch-rate", "0")
        obj.set("complexity", "0")
        SubElement(obj, "methods")
        lines = SubElement(obj, "lines")
        for line in sorted(data[item], key=lambda x: int(x)):
            obj = SubElement(lines, "line")
            obj.set("number", line)
            obj.set("hits", str(data[item][line]))

    tree = ElementTree(root)

    handle.write(HEADER)

    tree.write(handle, xml_declaration=False)

    logging.info(
        f"Merged coverage files for a coverage of {line_rates["_"]:.2%}: "
        f"{line_rates["_hits"]} hits from {line_rates["_lines"]} lines"
    )


def main():
    """Command line interface."""
    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-m", "--match", help="wildcard to match files to process")
    parser.add_argument("-o", "--output", help="output file, stdout used if omitted")
    parser.add_argument(
        "-e", "--exclude-prefix", default="", help="exclude coverage paths by prefix"
    )
    parser.add_argument("file", nargs="*", help="files to process")
    args = parser.parse_args()
    exclude_prefix = args.exclude_prefix
    files = glob.glob(args.match) if args.match else args.file

    result = read_files(files, exclude_prefix)

    if args.output:
        with open(args.output, "wb") as handle:
            write_data(result, handle)
    else:
        write_data(result, sys.stdout)


if __name__ == "__main__":
    main()
