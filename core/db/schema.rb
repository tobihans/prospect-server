# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_12_11_135720) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "timescaledb"

  # Custom types defined in this database.
  # Note that some types may not work with other database engines. Be careful if changing database.
  create_enum "account_status", ["active", "completed", "defaulted", "cancelled", "locked", "returned"]
  create_enum "enum_example", ["a", "b", "c"]
  create_enum "gender", ["M", "F", "O"]
  create_enum "payments_ts_account_origin", ["meters", "shs"]
  create_enum "phase", ["1", "2", "3"]
  create_enum "trust_trace_subject_origin", ["grids", "meters", "shs", "payments_ts"]

  create_table "active_storage_attachments", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.string "name", null: false
    t.bigint "record_id", null: false
    t.string "record_type", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.string "content_type"
    t.datetime "created_at", null: false
    t.string "filename", null: false
    t.string "key", null: false
    t.text "metadata"
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "authentications", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "provider", null: false
    t.string "uid", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.index ["provider", "uid"], name: "index_authentications_on_provider_and_uid"
  end

  create_table "common_products", force: :cascade do |t|
    t.string "brand", limit: 255
    t.datetime "created_at", null: false
    t.string "data_origin", limit: 255
    t.string "kind", limit: 255
    t.string "model_number", limit: 255
    t.string "name", limit: 500, null: false
    t.jsonb "properties", default: "{}", null: false
    t.datetime "updated_at", null: false
    t.string "website", limit: 255
    t.index ["name", "brand", "model_number"], name: "index_common_products_on_name_and_brand_and_model_number", unique: true
    t.index ["name"], name: "index_common_products_on_name"
  end

  create_table "data_custom", primary_key: "uid", id: :string, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "external_id", null: false
    t.bigint "import_id", null: false
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.bigint "organization_id", null: false
    t.bigint "source_id", null: false
    t.datetime "updated_at", null: false
    t.index ["external_id"], name: "index_data_custom_on_external_id"
    t.index ["import_id"], name: "index_data_custom_on_import_id"
    t.index ["last_import_id"], name: "index_data_custom_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_custom_on_last_source_id"
    t.index ["organization_id"], name: "index_data_custom_on_organization_id"
    t.index ["source_id"], name: "index_data_custom_on_source_id"
  end

  create_table "data_grids", primary_key: "uid", id: :string, force: :cascade do |t|
    t.string "country"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "external_id"
    t.bigint "import_id", null: false
    t.boolean "is_offgrid"
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.float "latitude"
    t.string "location_area_1"
    t.string "location_area_2"
    t.string "location_area_3"
    t.string "location_area_4"
    t.float "longitude"
    t.string "name", null: false
    t.string "operator_company"
    t.string "operator_phone_e"
    t.string "operator_phone_p"
    t.bigint "organization_id", null: false
    t.float "power_rating_kw"
    t.float "primary_input_installed_kw"
    t.string "primary_input_source"
    t.float "secondary_input_installed_kw"
    t.string "secondary_input_source"
    t.bigint "source_id", null: false
    t.float "storage_capacity_kwh"
    t.datetime "updated_at", null: false
    t.index ["external_id"], name: "index_data_grids_on_external_id"
    t.index ["import_id"], name: "index_data_grids_on_import_id"
    t.index ["last_import_id"], name: "index_data_grids_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_grids_on_last_source_id"
    t.index ["organization_id"], name: "index_data_grids_on_organization_id"
    t.index ["source_id"], name: "index_data_grids_on_source_id"
  end

  create_table "data_grids_ts", primary_key: ["grid_uid", "metered_at"], force: :cascade do |t|
    t.float "battery_charge_state_percent"
    t.float "battery_to_customer_kwh"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "grid_name", null: false
    t.string "grid_uid", null: false
    t.integer "households_connected"
    t.bigint "import_id", null: false
    t.integer "interval_seconds"
    t.datetime "metered_at", precision: nil, null: false
    t.bigint "organization_id", null: false
    t.float "primary_source_to_battery_kwh"
    t.float "primary_source_to_customer_kwh"
    t.float "secondary_source_to_battery_kwh"
    t.float "secondary_source_to_customer_kwh"
    t.bigint "source_id", null: false
    t.datetime "updated_at", null: false
    t.index ["import_id", "metered_at"], name: "index_data_grids_ts_on_import_id_and_metered_at", order: :desc
    t.index ["import_id"], name: "index_data_grids_ts_on_import_id"
    t.index ["metered_at"], name: "data_grids_ts_metered_at_idx", order: :desc
    t.index ["organization_id", "metered_at"], name: "index_data_grids_ts_on_organization_id_and_metered_at", order: :desc
    t.index ["organization_id"], name: "index_data_grids_ts_on_organization_id"
    t.index ["source_id", "metered_at"], name: "index_data_grids_ts_on_source_id_and_metered_at", order: :desc
    t.index ["source_id"], name: "index_data_grids_ts_on_source_id"
  end

  create_table "data_meter_events_ts", primary_key: ["device_uid", "start_at"], force: :cascade do |t|
    t.string "category", null: false
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "device_uid", null: false
    t.integer "duration_sec"
    t.datetime "end_at", precision: nil, null: false
    t.float "energy_consumed_wh"
    t.float "energy_end_wh"
    t.float "energy_start_wh"
    t.string "manufacturer", default: "", null: false
    t.bigint "organization_id", null: false
    t.string "serial_number", default: "", null: false
    t.bigint "source_id", null: false
    t.datetime "start_at", precision: nil, null: false
    t.datetime "updated_at", null: false
    t.index ["category", "start_at"], name: "index_data_meter_events_ts_on_category_and_start_at", order: :desc
    t.index ["organization_id", "start_at"], name: "index_data_meter_events_ts_on_organization_id_and_start_at", order: :desc
    t.index ["source_id", "start_at"], name: "index_data_meter_events_ts_on_source_id_and_start_at", order: :desc
    t.index ["start_at"], name: "index_data_meter_events_ts_on_start_at", order: :desc
  end

  create_table "data_meters", primary_key: "uid", id: :string, force: :cascade do |t|
    t.string "account_external_id"
    t.string "account_uid"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "customer_address_e"
    t.string "customer_address_p"
    t.integer "customer_birth_year"
    t.string "customer_category"
    t.string "customer_country"
    t.string "customer_email_e"
    t.string "customer_email_p"
    t.string "customer_external_id", null: false
    t.string "customer_former_electricity_source"
    t.enum "customer_gender", enum_type: "gender"
    t.integer "customer_household_size"
    t.string "customer_id_number_e"
    t.string "customer_id_number_p"
    t.string "customer_id_type"
    t.float "customer_latitude_b"
    t.string "customer_latitude_e"
    t.string "customer_latitude_p"
    t.string "customer_location_area_1"
    t.string "customer_location_area_2"
    t.string "customer_location_area_3"
    t.string "customer_location_area_4"
    t.string "customer_location_area_5"
    t.float "customer_longitude_b"
    t.string "customer_longitude_e"
    t.string "customer_longitude_p"
    t.string "customer_name_e"
    t.string "customer_name_p"
    t.string "customer_phone_2_e"
    t.string "customer_phone_2_p"
    t.string "customer_phone_e"
    t.string "customer_phone_p"
    t.string "customer_profession"
    t.string "customer_sub_category"
    t.string "customer_uid", null: false
    t.string "data_origin", null: false
    t.string "device_external_id", null: false
    t.string "device_uid", null: false
    t.string "firmware_version"
    t.string "grid_name"
    t.string "grid_uid"
    t.bigint "import_id", null: false
    t.date "installation_date"
    t.boolean "is_locked"
    t.boolean "is_test"
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.string "manufacturer", null: false
    t.float "max_power_w"
    t.string "model"
    t.bigint "organization_id", null: false
    t.string "payment_plan"
    t.float "payment_plan_amount_financed"
    t.string "payment_plan_currency"
    t.float "payment_plan_days"
    t.float "payment_plan_down_payment"
    t.string "primary_use"
    t.date "purchase_date"
    t.date "repossession_date"
    t.string "serial_number", null: false
    t.bigint "source_id", null: false
    t.float "total_price"
    t.datetime "updated_at", null: false
    t.index ["account_external_id"], name: "index_data_meters_on_account_external_id"
    t.index ["account_uid"], name: "index_data_meters_on_account_uid"
    t.index ["customer_external_id"], name: "index_data_meters_on_customer_external_id"
    t.index ["customer_uid"], name: "index_data_meters_on_customer_uid"
    t.index ["device_external_id"], name: "index_data_meters_on_device_external_id"
    t.index ["device_uid"], name: "index_data_meters_on_device_uid"
    t.index ["grid_uid"], name: "index_data_meters_on_grid_uid"
    t.index ["import_id"], name: "index_data_meters_on_import_id"
    t.index ["last_import_id"], name: "index_data_meters_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_meters_on_last_source_id"
    t.index ["organization_id"], name: "index_data_meters_on_organization_id"
    t.index ["serial_number"], name: "index_data_meters_on_serial_number"
    t.index ["source_id"], name: "index_data_meters_on_source_id"
  end

  create_table "data_meters_ts", primary_key: ["device_uid", "metered_at"], force: :cascade do |t|
    t.datetime "billing_cycle_start_at", precision: nil
    t.datetime "created_at", null: false
    t.float "current_a"
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "device_uid", null: false
    t.float "energy_interval_wh"
    t.float "energy_lifetime_wh"
    t.float "frequency_hz"
    t.bigint "import_id", null: false
    t.integer "interval_seconds"
    t.string "manufacturer", default: "", null: false
    t.datetime "metered_at", precision: nil, null: false
    t.bigint "organization_id", null: false
    t.enum "phase", default: "1", enum_type: "phase"
    t.float "power_factor"
    t.float "power_w"
    t.string "serial_number", default: "", null: false
    t.bigint "source_id", null: false
    t.datetime "updated_at", null: false
    t.float "voltage_v"
    t.index ["import_id", "metered_at"], name: "index_data_meters_ts_on_import_id_and_metered_at", order: :desc
    t.index ["metered_at"], name: "data_meters_ts_metered_at_idx", order: :desc
    t.index ["organization_id", "metered_at"], name: "index_data_meters_ts_on_organization_id_and_metered_at", order: :desc
    t.index ["source_id", "metered_at"], name: "index_data_meters_ts_on_source_id_and_metered_at", order: :desc
  end

  create_table "data_paygo_accounts_snapshots_ts", primary_key: ["account_uid", "snapshot_date"], force: :cascade do |t|
    t.string "account_external_id", null: false
    t.enum "account_status", null: false, enum_type: "account_status"
    t.string "account_uid", null: false
    t.datetime "created_at", null: false
    t.integer "cumulative_days_locked", null: false
    t.float "cumulative_paid_amount", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.integer "days_in_repayment", null: false
    t.integer "days_to_cutoff", null: false
    t.bigint "organization_id", null: false
    t.date "snapshot_date", null: false
    t.bigint "source_id", null: false
    t.float "total_remaining_amount", null: false
    t.datetime "updated_at", null: false
    t.index ["account_external_id", "snapshot_date"], name: "index_paygo_snapshots_on_account_ex_id_and_snapshot_date", order: :desc
    t.index ["organization_id", "snapshot_date"], name: "index_paygo_snapshots_on_org_id_and_snapshot_date", order: :desc
    t.index ["snapshot_date"], name: "index_data_paygo_accounts_snapshots_ts_on_snapshot_date", order: :desc
    t.index ["source_id", "snapshot_date"], name: "index_paygo_snapshots_on_src_id_and_snapshot_date", order: :desc
  end

  create_table "data_payments_ts", primary_key: ["uid", "paid_at"], force: :cascade do |t|
    t.string "account_external_id", null: false
    t.enum "account_origin", null: false, enum_type: "payments_ts_account_origin"
    t.string "account_uid", null: false
    t.float "amount", null: false
    t.datetime "created_at", null: false
    t.string "currency", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.float "days_active"
    t.bigint "import_id", null: false
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.bigint "organization_id", null: false
    t.datetime "paid_at", null: false
    t.string "payment_external_id", null: false
    t.string "provider_name"
    t.string "provider_transaction_id"
    t.string "purchase_item"
    t.float "purchase_quantity"
    t.string "purchase_unit"
    t.string "purpose"
    t.datetime "reverted_at"
    t.bigint "source_id", null: false
    t.string "uid", null: false
    t.datetime "updated_at", null: false
    t.index ["account_external_id"], name: "index_data_payments_ts_on_account_external_id"
    t.index ["account_origin"], name: "index_data_payments_ts_on_account_origin"
    t.index ["account_uid"], name: "index_data_payments_ts_on_account_uid"
    t.index ["import_id", "paid_at"], name: "index_data_payments_ts_on_import_id_and_paid_at", order: :desc
    t.index ["last_import_id"], name: "index_data_payments_ts_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_payments_ts_on_last_source_id"
    t.index ["organization_id", "paid_at"], name: "index_data_payments_ts_on_organization_id_and_paid_at", order: :desc
    t.index ["paid_at"], name: "data_payments_ts_paid_at_idx", order: :desc
    t.index ["payment_external_id", "paid_at"], name: "index_data_payments_ts_on_payment_external_id_and_paid_at"
    t.index ["provider_transaction_id", "paid_at"], name: "index_data_payments_ts_on_provider_transaction_id_and_paid_at"
    t.index ["source_id", "paid_at"], name: "index_data_payments_ts_on_source_id_and_paid_at", order: :desc
  end

  create_table "data_shs", primary_key: "uid", id: :string, force: :cascade do |t|
    t.string "account_external_id", null: false
    t.string "account_uid", null: false
    t.float "battery_energy_wh"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "customer_address_e"
    t.string "customer_address_p"
    t.integer "customer_birth_year"
    t.string "customer_category"
    t.string "customer_country"
    t.string "customer_email_e"
    t.string "customer_email_p"
    t.string "customer_external_id", null: false
    t.string "customer_former_electricity_source"
    t.enum "customer_gender", enum_type: "gender"
    t.integer "customer_household_size"
    t.string "customer_id_number_e"
    t.string "customer_id_number_p"
    t.string "customer_id_type"
    t.float "customer_latitude_b"
    t.string "customer_latitude_e"
    t.string "customer_latitude_p"
    t.string "customer_location_area_1"
    t.string "customer_location_area_2"
    t.string "customer_location_area_3"
    t.string "customer_location_area_4"
    t.string "customer_location_area_5"
    t.float "customer_longitude_b"
    t.string "customer_longitude_e"
    t.string "customer_longitude_p"
    t.string "customer_name_e"
    t.string "customer_name_p"
    t.string "customer_phone_2_e"
    t.string "customer_phone_2_p"
    t.string "customer_phone_e"
    t.string "customer_phone_p"
    t.string "customer_profession"
    t.string "customer_sub_category"
    t.string "customer_uid", null: false
    t.string "data_origin", null: false
    t.string "device_external_id", null: false
    t.string "device_uid", null: false
    t.bigint "import_id", null: false
    t.boolean "is_test"
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.string "manufacturer", null: false
    t.string "model"
    t.bigint "organization_id", null: false
    t.date "paid_off_date"
    t.float "payment_plan_amount_financed"
    t.string "payment_plan_currency"
    t.float "payment_plan_days"
    t.float "payment_plan_down_payment"
    t.string "payment_plan_type"
    t.string "primary_use"
    t.date "purchase_date"
    t.float "pv_power_w"
    t.float "rated_power_w"
    t.date "repossession_date"
    t.string "seller_address_e"
    t.string "seller_address_p"
    t.string "seller_country"
    t.string "seller_email_e"
    t.string "seller_email_p"
    t.string "seller_external_id"
    t.enum "seller_gender", enum_type: "gender"
    t.float "seller_latitude_b"
    t.string "seller_latitude_e"
    t.string "seller_latitude_p"
    t.string "seller_location_area_1"
    t.string "seller_location_area_2"
    t.string "seller_location_area_3"
    t.string "seller_location_area_4"
    t.float "seller_longitude_b"
    t.string "seller_longitude_e"
    t.string "seller_longitude_p"
    t.string "seller_name_e"
    t.string "seller_name_p"
    t.string "seller_phone_e"
    t.string "seller_phone_p"
    t.string "serial_number", null: false
    t.bigint "source_id", null: false
    t.float "total_price"
    t.datetime "updated_at", null: false
    t.float "voltage_v"
    t.index ["account_external_id"], name: "index_data_shs_on_account_external_id"
    t.index ["account_uid"], name: "index_data_shs_on_account_uid"
    t.index ["customer_external_id"], name: "index_data_shs_on_customer_external_id"
    t.index ["customer_uid"], name: "index_data_shs_on_customer_uid"
    t.index ["device_external_id"], name: "index_data_shs_on_device_external_id"
    t.index ["device_uid"], name: "index_data_shs_on_device_uid"
    t.index ["import_id"], name: "index_data_shs_on_import_id"
    t.index ["last_import_id"], name: "index_data_shs_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_shs_on_last_source_id"
    t.index ["organization_id"], name: "index_data_shs_on_organization_id"
    t.index ["seller_external_id"], name: "index_data_shs_on_seller_external_id"
    t.index ["serial_number"], name: "index_data_shs_on_serial_number"
    t.index ["source_id"], name: "index_data_shs_on_source_id"
  end

  create_table "data_shs_ts", primary_key: ["device_uid", "metered_at"], force: :cascade do |t|
    t.float "battery_charge_percent"
    t.float "battery_io_w"
    t.float "battery_v"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.string "device_uid", null: false
    t.float "grid_energy_interval_wh"
    t.float "grid_energy_lifetime_wh"
    t.float "grid_input_v"
    t.float "grid_input_w"
    t.bigint "import_id", null: false
    t.integer "interval_seconds"
    t.string "manufacturer", null: false
    t.datetime "metered_at", precision: nil, null: false
    t.bigint "organization_id", null: false
    t.float "output_energy_interval_wh"
    t.float "output_energy_lifetime_wh"
    t.integer "paygo_days_left"
    t.boolean "paygo_is_paid_off"
    t.float "pv_energy_interval_wh"
    t.float "pv_energy_lifetime_wh"
    t.float "pv_input_w"
    t.string "serial_number", null: false
    t.bigint "source_id", null: false
    t.float "system_output_w"
    t.datetime "updated_at", null: false
    t.index ["import_id", "metered_at"], name: "index_data_shs_ts_on_import_id_and_metered_at", order: :desc
    t.index ["metered_at"], name: "data_shs_ts_metered_at_idx", order: :desc
    t.index ["organization_id", "metered_at"], name: "index_data_shs_ts_on_organization_id_and_metered_at", order: :desc
    t.index ["source_id", "metered_at"], name: "index_data_shs_ts_on_source_id_and_metered_at", order: :desc
  end

  create_table "data_test", primary_key: "uid", id: :string, force: :cascade do |t|
    t.boolean "bool_column"
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.enum "enum_column", enum_type: "enum_example"
    t.float "float_column", null: false
    t.bigint "import_id", null: false
    t.integer "int_column"
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.string "name_column_e"
    t.string "name_column_p"
    t.bigint "organization_id", null: false
    t.string "personal_column"
    t.string "something_uid"
    t.bigint "source_id", null: false
    t.datetime "time_column"
    t.datetime "updated_at", null: false
    t.index ["import_id"], name: "index_data_test_on_import_id"
    t.index ["last_import_id"], name: "index_data_test_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_test_on_last_source_id"
    t.index ["organization_id"], name: "index_data_test_on_organization_id"
    t.index ["something_uid"], name: "index_data_test_on_something_uid"
    t.index ["source_id"], name: "index_data_test_on_source_id"
  end

  create_table "data_trust_trace", primary_key: "uid", id: :string, force: :cascade do |t|
    t.string "check", null: false
    t.datetime "created_at", null: false
    t.jsonb "custom", default: {}, null: false
    t.string "data_origin", null: false
    t.bigint "import_id", null: false
    t.bigint "last_import_id"
    t.bigint "last_source_id"
    t.bigint "organization_id", null: false
    t.string "result", null: false
    t.bigint "source_id", null: false
    t.enum "subject_origin", null: false, enum_type: "trust_trace_subject_origin"
    t.string "subject_uid", null: false
    t.datetime "updated_at", null: false
    t.index ["check"], name: "index_data_trust_trace_on_check"
    t.index ["import_id"], name: "index_data_trust_trace_on_import_id"
    t.index ["last_import_id"], name: "index_data_trust_trace_on_last_import_id"
    t.index ["last_source_id"], name: "index_data_trust_trace_on_last_source_id"
    t.index ["organization_id"], name: "index_data_trust_trace_on_organization_id"
    t.index ["result"], name: "index_data_trust_trace_on_result"
    t.index ["source_id"], name: "index_data_trust_trace_on_source_id"
    t.index ["subject_origin"], name: "index_data_trust_trace_on_subject_origin"
    t.index ["subject_uid"], name: "index_data_trust_trace_on_subject_uid"
  end

  create_table "dynamic_forms", force: :cascade do |t|
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.string "description"
    t.json "form_data"
    t.string "name"
    t.bigint "rbf_claim_template_id"
    t.datetime "updated_at", null: false
    t.index ["rbf_claim_template_id"], name: "index_dynamic_forms_on_rbf_claim_template_id"
  end

  create_table "encrypted_blobs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.text "error"
    t.bigint "import_id", null: false
    t.datetime "updated_at", null: false
    t.text "url_e"
    t.index ["import_id"], name: "index_encrypted_blobs_on_import_id"
  end

  create_table "history", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "data_table"
    t.bigint "import_id", null: false
    t.jsonb "new_row"
    t.jsonb "old_row", null: false
    t.bigint "organization_id", null: false
    t.datetime "reverted_at"
    t.bigint "source_id", null: false
    t.string "uid"
    t.index ["data_table"], name: "index_history_on_data_table"
    t.index ["import_id"], name: "index_history_on_import_id"
    t.index ["organization_id"], name: "index_history_on_organization_id"
    t.index ["source_id"], name: "index_history_on_source_id"
    t.index ["uid"], name: "index_history_on_uid"
  end

  create_table "imports", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "error"
    t.datetime "ingestion_finished_at", precision: nil
    t.datetime "ingestion_started_at", precision: nil
    t.string "last_imported_hint"
    t.datetime "processing_finished_at", precision: nil
    t.datetime "processing_started_at", precision: nil
    t.text "protocol"
    t.integer "rows_duplicated"
    t.integer "rows_failed"
    t.integer "rows_inserted"
    t.integer "rows_updated"
    t.bigint "source_id"
    t.datetime "updated_at", null: false
    t.index ["source_id"], name: "index_imports_on_source_id"
  end

  create_table "org_device_mappers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.bigint "device_id", null: false
    t.string "device_type", null: false
    t.bigint "organization_id", null: false
    t.datetime "updated_at", null: false
    t.index ["device_type", "device_id"], name: "index_org_device_mappers_on_device_type_and_device_id", unique: true
    t.index ["device_type", "organization_id"], name: "index_org_device_mappers_on_device_type_and_organization_id"
    t.index ["organization_id"], name: "index_org_device_mappers_on_organization_id"
  end

  create_table "organization_group_members", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.bigint "organization_group_id", null: false
    t.bigint "organization_id", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.boolean "visible_for_sharing", default: false, null: false
    t.index ["organization_group_id", "organization_id"], name: "idx_on_organization_group_id_organization_id_3f94b906ec", unique: true
    t.index ["organization_group_id"], name: "index_organization_group_members_on_organization_group_id"
    t.index ["organization_id"], name: "index_organization_group_members_on_organization_id"
  end

  create_table "organization_groups", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "name", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["name"], name: "index_organization_groups_on_name", unique: true
  end

  create_table "organizations", force: :cascade do |t|
    t.string "api_key", limit: 1000
    t.datetime "created_at", null: false
    t.datetime "crypto_added_at", precision: nil
    t.integer "grafana_org_id"
    t.text "iv"
    t.string "name", null: false
    t.string "postgres_password", limit: 1000
    t.text "public_key"
    t.text "salt"
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.text "wrapped_key"
    t.index ["name"], name: "index_organizations_on_name", unique: true
    t.index ["user_id"], name: "index_organizations_on_user_id"
  end

  create_table "organizations_users", force: :cascade do |t|
    t.datetime "activated_at"
    t.bigint "organization_id", null: false
    t.bigint "user_id", null: false
    t.index ["organization_id"], name: "index_organizations_users_on_organization_id"
    t.index ["user_id", "organization_id"], name: "index_organizations_users_on_user_id_and_organization_id", unique: true
    t.index ["user_id"], name: "index_organizations_users_on_user_id"
  end

  create_table "privileges", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "name"
    t.bigint "organization_user_id", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.index ["name"], name: "index_privileges_on_name"
    t.index ["organization_user_id", "name"], name: "index_privileges_on_organization_user_id_and_name", unique: true
    t.index ["organization_user_id"], name: "index_privileges_on_organization_user_id"
    t.index ["user_id"], name: "index_privileges_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "description"
    t.string "name", null: false
    t.integer "organization_id", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["name", "organization_id"], name: "index_projects_on_name_and_organization_id", unique: true
    t.index ["organization_id"], name: "index_projects_on_organization_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "rbf_claim_templates", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "currency", null: false
    t.bigint "managing_organization_id", null: false
    t.string "name"
    t.bigint "organization_group_id", null: false
    t.bigint "supervising_organization_id", null: false
    t.string "trust_trace_check", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.bigint "verifying_organization_id", null: false
    t.index ["managing_organization_id"], name: "index_rbf_claim_templates_on_managing_organization_id"
    t.index ["name"], name: "index_rbf_claim_templates_on_name", unique: true
    t.index ["organization_group_id"], name: "index_rbf_claim_templates_on_organization_group_id"
    t.index ["supervising_organization_id"], name: "index_rbf_claim_templates_on_supervising_organization_id"
    t.index ["user_id"], name: "index_rbf_claim_templates_on_user_id"
    t.index ["verifying_organization_id"], name: "index_rbf_claim_templates_on_verifying_organization_id"
  end

  create_table "rbf_claim_verifiers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.bigint "rbf_claim_id", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "verifier_id", null: false
    t.index ["rbf_claim_id", "verifier_id"], name: "index_rbf_claim_verifiers_on_rbf_claim_id_and_verifier_id", unique: true
  end

  create_table "rbf_claims", force: :cascade do |t|
    t.string "aasm_state"
    t.datetime "approved_at", precision: nil
    t.bigint "approved_by_id"
    t.bigint "assigned_to_verifier_id"
    t.jsonb "claimed_data", default: {}, null: false
    t.integer "claimed_subunit"
    t.string "code"
    t.datetime "created_at", null: false
    t.bigint "organization_id", null: false
    t.datetime "paid_out_at", precision: nil
    t.bigint "paid_out_by_id"
    t.bigint "rbf_claim_template_id", null: false
    t.datetime "rejected_at", precision: nil
    t.bigint "rejected_by_id"
    t.datetime "submitted_to_program_at", precision: nil
    t.bigint "submitted_to_program_by_id"
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.datetime "verification_at", precision: nil
    t.bigint "verification_by_id"
    t.jsonb "verified_data", default: {}, null: false
    t.integer "verified_subunit"
    t.index ["aasm_state"], name: "index_rbf_claims_on_aasm_state"
    t.index ["code"], name: "index_rbf_claims_on_code", unique: true
    t.index ["organization_id"], name: "index_rbf_claims_on_organization_id"
    t.index ["rbf_claim_template_id"], name: "index_rbf_claims_on_rbf_claim_template_id"
    t.index ["user_id"], name: "index_rbf_claims_on_user_id"
  end

  create_table "rbf_device_claims", force: :cascade do |t|
    t.jsonb "claimed_devices_data", default: {}, null: false
    t.integer "claimed_incentive", default: 0
    t.jsonb "claimed_payments_data", default: [], null: false
    t.integer "claimed_subsidy", default: 0
    t.jsonb "claimed_trust_trace_data", default: {}, null: false
    t.datetime "created_at", null: false
    t.string "device_category"
    t.bigint "parent_rbf_device_claim_id"
    t.bigint "rbf_claim_id", null: false
    t.string "rejection_category"
    t.text "reviewer_comment"
    t.bigint "reviewer_id"
    t.string "state", null: false
    t.string "subject_origin", null: false
    t.string "subject_uid", null: false
    t.string "submission_category"
    t.text "submission_comment"
    t.string "trust_trace_uid"
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.string "verification_category"
    t.jsonb "verified_data", default: {}, null: false
    t.integer "verified_incentive", default: 0
    t.integer "verified_subsidy", default: 0
    t.index ["parent_rbf_device_claim_id"], name: "index_rbf_device_claims_on_parent_rbf_device_claim_id"
    t.index ["rbf_claim_id"], name: "index_rbf_device_claims_on_rbf_claim_id"
    t.index ["reviewer_id"], name: "index_rbf_device_claims_on_reviewer_id"
    t.index ["subject_origin", "subject_uid"], name: "index_rbf_device_claims_on_subject_origin_and_subject_uid"
    t.index ["trust_trace_uid"], name: "index_rbf_device_claims_on_trust_trace_uid"
    t.index ["user_id"], name: "index_rbf_device_claims_on_user_id"
  end

  create_table "rbf_organization_configs", force: :cascade do |t|
    t.string "code", null: false
    t.datetime "created_at", null: false
    t.integer "organization_id", null: false
    t.integer "rbf_claim_template_id", null: false
    t.float "total_grant_allocation", default: 0.0, null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["code", "rbf_claim_template_id"], name: "idx_on_code_rbf_claim_template_id_967e2caa74", unique: true
    t.index ["organization_id", "rbf_claim_template_id"], name: "idx_on_organization_id_rbf_claim_template_id_2d999cb87a", unique: true
  end

  create_table "rbf_product_prices", force: :cascade do |t|
    t.integer "base_subsidized_sales_price", null: false
    t.integer "base_subsidy_amount", null: false
    t.string "category", null: false
    t.datetime "created_at", null: false
    t.float "original_sales_price", null: false
    t.bigint "rbf_product_id", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.date "valid_from", null: false
    t.index ["rbf_product_id", "category", "valid_from"], name: "idx_on_rbf_product_id_category_valid_from_73af8c87d8", unique: true
    t.index ["rbf_product_id"], name: "index_rbf_product_prices_on_rbf_product_id"
  end

  create_table "rbf_products", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "manufacturer", null: false
    t.string "model", null: false
    t.bigint "organization_id", null: false
    t.integer "rated_power_w"
    t.string "rbf_category", null: false
    t.bigint "rbf_claim_template_id", null: false
    t.string "technology"
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["model", "organization_id", "manufacturer"], name: "idx_on_model_organization_id_manufacturer_c8532bbe4b", unique: true
    t.index ["organization_id"], name: "index_rbf_products_on_organization_id"
    t.index ["rbf_claim_template_id"], name: "index_rbf_products_on_rbf_claim_template_id"
    t.index ["user_id"], name: "index_rbf_products_on_user_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.text "data"
    t.string "session_id", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "shared_keys_vaults", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "encrypted_key", null: false
    t.bigint "organization_id", null: false
    t.bigint "shared_with_organization_id", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["organization_id", "shared_with_organization_id"], name: "idx_on_organization_id_shared_with_organization_id_41045e9954", unique: true
    t.index ["user_id"], name: "index_shared_keys_vaults_on_user_id"
  end

  create_table "sources", force: :cascade do |t|
    t.datetime "activated_at", precision: nil
    t.datetime "created_at", null: false
    t.string "data_category"
    t.jsonb "details", default: {}, null: false
    t.string "kind", null: false
    t.string "name", null: false
    t.bigint "project_id", null: false
    t.string "secret", limit: 1000
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["activated_at"], name: "index_sources_on_activated_at"
    t.index ["kind"], name: "index_sources_on_kind"
    t.index ["name", "project_id"], name: "index_sources_on_name_and_project_id", unique: true
    t.index ["project_id"], name: "index_sources_on_project_id"
    t.index ["user_id"], name: "index_sources_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer "access_count_to_reset_password_page", default: 0
    t.datetime "active_at", precision: nil
    t.boolean "admin", default: false, null: false
    t.datetime "archived_at", precision: nil
    t.json "code_of_conduct", default: {}
    t.datetime "created_at", null: false
    t.string "crypted_password"
    t.json "data_protection_policy", default: {}
    t.string "email", null: false
    t.string "invite_code"
    t.integer "organization_id"
    t.datetime "reset_password_email_sent_at"
    t.string "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.string "salt"
    t.json "terms_of_use", default: {}
    t.datetime "updated_at", null: false
    t.string "username"
    t.index ["active_at"], name: "index_users_on_active_at"
    t.index ["archived_at"], name: "index_users_on_archived_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invite_code"], name: "index_users_on_invite_code", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token"
    t.index ["username"], name: "index_users_on_username"
  end

  create_table "visibilities", force: :cascade do |t|
    t.jsonb "columns", default: {}, null: false
    t.jsonb "conditions", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "data_from"
    t.datetime "data_until"
    t.string "license"
    t.integer "organization_id", null: false
    t.bigint "project_id", null: false
    t.boolean "resharing", default: false, null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["organization_id"], name: "index_visibilities_on_organization_id"
    t.index ["project_id", "organization_id"], name: "index_visibilities_on_project_id_and_organization_id", unique: true
    t.index ["user_id"], name: "index_visibilities_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "shared_keys_vaults", "organizations"
  add_foreign_key "shared_keys_vaults", "organizations", column: "shared_with_organization_id"
  add_foreign_key "shared_keys_vaults", "users"

  create_function :try_cast_numeric, sql_definition: <<-'SQL'
      CREATE OR REPLACE FUNCTION public.try_cast_numeric(string character varying, fallback numeric DEFAULT NULL::numeric)
       RETURNS numeric
       LANGUAGE plpgsql
      AS $function$
      BEGIN
          RETURN cast(string as numeric);
      EXCEPTION
          WHEN OTHERS THEN return fallback;
      END
      $function$
  SQL
  create_function :try_cast_timestamp, sql_definition: <<-'SQL'
      CREATE OR REPLACE FUNCTION public.try_cast_timestamp(string character varying, fallback timestamp without time zone DEFAULT NULL::timestamp without time zone)
       RETURNS timestamp without time zone
       LANGUAGE plpgsql
      AS $function$
      BEGIN
          RETURN cast(string as timestamp);
      EXCEPTION
          WHEN OTHERS THEN return fallback;
      END
      $function$
  SQL
  create_function :track_changes_in_history, sql_definition: <<-'SQL'
      CREATE OR REPLACE FUNCTION public.track_changes_in_history()
       RETURNS trigger
       LANGUAGE plpgsql
       SECURITY DEFINER
      AS $function$
      BEGIN
          IF TG_OP = 'UPDATE' OR TG_OP = 'DELETE' THEN
          INSERT INTO history (
                                    data_table,
                                    old_row,
                                    new_row,
                                    created_at,

                                    uid,
                                    organization_id,
                                    source_id,
                                    import_id
                               )
          VALUES (
                  TG_RELNAME,       /* table name */
                  row_to_json(OLD), /* whole row before UPDATE */
                  row_to_json(NEW), /* whole new row, is NULL for DELETE */
                  NOW(),            /* time the history entry is created */

                  /* some things never change */
                  OLD.uid,
                  OLD.organization_id,

                  /* use original / initial from OLD on DELETE */
                  COALESCE(NEW.last_source_id, OLD.source_id),
                  COALESCE(NEW.last_import_id, OLD.import_id)
                 );
          END IF;
          RETURN NULL;
      END;
      $function$
  SQL
  create_function :update_checks_and_deduplication, sql_definition: <<-'SQL'
      CREATE OR REPLACE FUNCTION public.update_checks_and_deduplication()
       RETURNS trigger
       LANGUAGE plpgsql
       SECURITY DEFINER
      AS $function$
      DECLARE
          /* the columns we want to ignore when checking for just duplicated insert*/
          excluded_keys CONSTANT text[] :=
              '{import_id,source_id,updated_at,created_at,last_import_id,last_source_id}'::text[];
      BEGIN
          IF TG_OP != 'UPDATE' THEN
              RAISE EXCEPTION SQLSTATE 'ONLUP'
                  USING HINT = 'Function can only be called on UPDATE: update_checks_and_deduplication()';

          ELSIF (row_to_json(OLD)::jsonb - excluded_keys) = (row_to_json(NEW)::jsonb - excluded_keys) THEN
              /* skip update if actual data is unchanged, row meta could be different, we don't care */
              RETURN NULL;

          ELSIF NEW.organization_id != OLD.organization_id THEN
              /* prevent changing the owner of the data */
              RAISE EXCEPTION SQLSTATE 'ORGUP'
                  USING HINT = 'You can not update organization_id';

          ELSE
              /* created_at never changes! */
              NEW.created_at = OLD.created_at;

              /* make sure updated at is always NOW(). There is no reason to fake that */
              NEW.updated_at = NOW();

              /* Write new source_id and import_id into fields to track last updater */
              NEW.last_import_id = NEW.import_id;
              NEW.last_source_id = NEW.source_id;

              /* Always keep initial source_id and import_id */
              NEW.source_id = OLD.source_id;
              NEW.import_id = OLD.import_id;

              RETURN NEW;
          END IF;
      END;

      $function$
  SQL

  create_trigger :before_update_checks_custom, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_custom BEFORE UPDATE ON data_custom FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_grids, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_grids BEFORE UPDATE ON data_grids FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_meters, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_meters BEFORE UPDATE ON data_meters FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_payments_ts, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_payments_ts BEFORE UPDATE ON data_payments_ts FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_shs, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_shs BEFORE UPDATE ON data_shs FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_test, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_test BEFORE UPDATE ON data_test FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :before_update_checks_trust_trace, sql_definition: <<-SQL
      CREATE TRIGGER before_update_checks_trust_trace BEFORE UPDATE ON data_trust_trace FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()
  SQL
  create_trigger :track_history_custom, sql_definition: <<-SQL
      CREATE TRIGGER track_history_custom AFTER DELETE OR UPDATE ON data_custom FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_grids, sql_definition: <<-SQL
      CREATE TRIGGER track_history_grids AFTER DELETE OR UPDATE ON data_grids FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_meters, sql_definition: <<-SQL
      CREATE TRIGGER track_history_meters AFTER DELETE OR UPDATE ON data_meters FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_payments_ts, sql_definition: <<-SQL
      CREATE TRIGGER track_history_payments_ts AFTER DELETE OR UPDATE ON data_payments_ts FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_shs, sql_definition: <<-SQL
      CREATE TRIGGER track_history_shs AFTER DELETE OR UPDATE ON data_shs FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_test, sql_definition: <<-SQL
      CREATE TRIGGER track_history_test AFTER DELETE OR UPDATE ON data_test FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_trigger :track_history_trust_trace, sql_definition: <<-SQL
      CREATE TRIGGER track_history_trust_trace AFTER DELETE OR UPDATE ON data_trust_trace FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()
  SQL
  create_hypertable "data_grids_ts", time_column: "metered_at", chunk_time_interval: "30 days"
  create_hypertable "data_meter_events_ts", time_column: "start_at", chunk_time_interval: "30 days"
  create_hypertable "data_meters_ts", time_column: "metered_at", chunk_time_interval: "30 days"
  create_hypertable "data_paygo_accounts_snapshots_ts", time_column: "snapshot_date", chunk_time_interval: "30 days"
  create_hypertable "data_payments_ts", time_column: "paid_at", chunk_time_interval: "30 days"
  create_hypertable "data_shs_ts", time_column: "metered_at", chunk_time_interval: "30 days"
end
