CREATE FUNCTION try_cast_numeric(string varchar, fallback numeric default null) RETURNS NUMERIC AS $$
BEGIN
    RETURN cast(string as numeric);
EXCEPTION
    WHEN OTHERS THEN return fallback;
END
$$ LANGUAGE plpgsql;
