CREATE FUNCTION track_changes_in_history() RETURNS trigger AS $$
BEGIN
    IF TG_OP = 'UPDATE' OR TG_OP = 'DELETE' THEN
    INSERT INTO history (
                              data_table,
                              old_row,
                              new_row,
                              created_at,

                              uid,
                              organization_id,
                              source_id,
                              import_id
                         )
    VALUES (
            TG_RELNAME,       /* table name */
            row_to_json(OLD), /* whole row before UPDATE */
            row_to_json(NEW), /* whole new row, is NULL for DELETE */
            NOW(),            /* time the history entry is created */

            /* some things never change */
            OLD.uid,
            OLD.organization_id,

            /* use original / initial from OLD on DELETE */
            COALESCE(NEW.last_source_id, OLD.source_id),
            COALESCE(NEW.last_import_id, OLD.import_id)
           );
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;
