CREATE FUNCTION try_cast_timestamp(string varchar, fallback timestamp default null) RETURNS TIMESTAMP AS $$
BEGIN
    RETURN cast(string as timestamp);
EXCEPTION
    WHEN OTHERS THEN return fallback;
END
$$ LANGUAGE plpgsql;
