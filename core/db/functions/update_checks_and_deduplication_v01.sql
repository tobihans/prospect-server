CREATE FUNCTION update_checks_and_deduplication() RETURNS trigger AS $$
DECLARE
    /* the columns we want to ignore when checking for just duplicated insert*/
    excluded_keys CONSTANT text[] :=
        '{import_id,source_id,updated_at,created_at,last_import_id,last_source_id}'::text[];
BEGIN
    IF TG_OP != 'UPDATE' THEN
        RAISE EXCEPTION SQLSTATE 'ONLUP'
            USING HINT = 'Function can only be called on UPDATE: update_checks_and_deduplication()';

    ELSIF (row_to_json(OLD)::jsonb - excluded_keys) = (row_to_json(NEW)::jsonb - excluded_keys) THEN
        /* skip update if actual data is unchanged, row meta could be different, we don't care */
        RETURN NULL;

    ELSIF NEW.organization_id != OLD.organization_id THEN
        /* prevent changing the owner of the data */
        RAISE EXCEPTION SQLSTATE 'ORGUP'
            USING HINT = 'You can not update organization_id';

    ELSE
        /* created_at never changes! */
        NEW.created_at = OLD.created_at;

        /* make sure updated at is always NOW(). There is no reason to fake that */
        NEW.updated_at = NOW();

        /* Write new source_id and import_id into fields to track last updater */
        NEW.last_import_id = NEW.import_id;
        NEW.last_source_id = NEW.source_id;

        /* Always keep initial source_id and import_id */
        NEW.source_id = OLD.source_id;
        NEW.import_id = OLD.import_id;

        RETURN NEW;
    END IF;
END;

$$ LANGUAGE plpgsql SECURITY DEFINER;
