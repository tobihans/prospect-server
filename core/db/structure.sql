SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

-- Name: public; Type: SCHEMA

CREATE SCHEMA IF NOT EXISTS public;

-- Name: SCHEMA public; Type: COMMENT

COMMENT ON SCHEMA public IS 'standard public schema';

-- Name: timescaledb; Type: EXTENSION

CREATE EXTENSION IF NOT EXISTS timescaledb WITH SCHEMA public;

-- Name: EXTENSION timescaledb; Type: COMMENT

-- Name: account_status; Type: TYPE

CREATE TYPE public.account_status AS ENUM (
    'active',
    'completed',
    'defaulted',
    'cancelled',
    'locked',
    'returned'
);

-- Name: enum_example; Type: TYPE

CREATE TYPE public.enum_example AS ENUM (
    'a',
    'b',
    'c'
);

-- Name: gender; Type: TYPE

CREATE TYPE public.gender AS ENUM (
    'M',
    'F',
    'O'
);

-- Name: payments_ts_account_origin; Type: TYPE

CREATE TYPE public.payments_ts_account_origin AS ENUM (
    'meters',
    'shs'
);

-- Name: phase; Type: TYPE

CREATE TYPE public.phase AS ENUM (
    '1',
    '2',
    '3'
);

-- Name: trust_trace_subject_origin; Type: TYPE

CREATE TYPE public.trust_trace_subject_origin AS ENUM (
    'grids',
    'meters',
    'shs',
    'payments_ts'
);

-- Name: track_changes_in_history(); Type: FUNCTION

CREATE FUNCTION public.track_changes_in_history() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    IF TG_OP = 'UPDATE' OR TG_OP = 'DELETE' THEN
    INSERT INTO history (
                              data_table,
                              old_row,
                              new_row,
                              created_at,

                              uid,
                              organization_id,
                              source_id,
                              import_id
                         )
    VALUES (
            TG_RELNAME,       /* table name */
            row_to_json(OLD), /* whole row before UPDATE */
            row_to_json(NEW), /* whole new row, is NULL for DELETE */
            NOW(),            /* time the history entry is created */

            /* some things never change */
            OLD.uid,
            OLD.organization_id,

            /* use original / initial from OLD on DELETE */
            COALESCE(NEW.last_source_id, OLD.source_id),
            COALESCE(NEW.last_import_id, OLD.import_id)
           );
    END IF;
    RETURN NULL;
END;
$$;

-- Name: try_cast_numeric(character varying, numeric); Type: FUNCTION

CREATE FUNCTION public.try_cast_numeric(string character varying, fallback numeric DEFAULT NULL::numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN cast(string as numeric);
EXCEPTION
    WHEN OTHERS THEN return fallback;
END
$$;

-- Name: try_cast_timestamp(character varying, timestamp without time zone); Type: FUNCTION

CREATE FUNCTION public.try_cast_timestamp(string character varying, fallback timestamp without time zone DEFAULT NULL::timestamp without time zone) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN cast(string as timestamp);
EXCEPTION
    WHEN OTHERS THEN return fallback;
END
$$;

-- Name: update_checks_and_deduplication(); Type: FUNCTION

CREATE FUNCTION public.update_checks_and_deduplication() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    /* the columns we want to ignore when checking for just duplicated insert*/
    excluded_keys CONSTANT text[] :=
        '{import_id,source_id,updated_at,created_at,last_import_id,last_source_id}'::text[];
BEGIN
    IF TG_OP != 'UPDATE' THEN
        RAISE EXCEPTION SQLSTATE 'ONLUP'
            USING HINT = 'Function can only be called on UPDATE: update_checks_and_deduplication()';

    ELSIF (row_to_json(OLD)::jsonb - excluded_keys) = (row_to_json(NEW)::jsonb - excluded_keys) THEN
        /* skip update if actual data is unchanged, row meta could be different, we don't care */
        RETURN NULL;

    ELSIF NEW.organization_id != OLD.organization_id THEN
        /* prevent changing the owner of the data */
        RAISE EXCEPTION SQLSTATE 'ORGUP'
            USING HINT = 'You can not update organization_id';

    ELSE
        /* created_at never changes! */
        NEW.created_at = OLD.created_at;

        /* make sure updated at is always NOW(). There is no reason to fake that */
        NEW.updated_at = NOW();

        /* Write new source_id and import_id into fields to track last updater */
        NEW.last_import_id = NEW.import_id;
        NEW.last_source_id = NEW.source_id;

        /* Always keep initial source_id and import_id */
        NEW.source_id = OLD.source_id;
        NEW.import_id = OLD.import_id;

        RETURN NEW;
    END IF;
END;

$$;

SET default_tablespace = '';

-- Name: active_storage_attachments; Type: TABLE

CREATE TABLE public.active_storage_attachments (
    blob_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    name character varying NOT NULL,
    record_id bigint NOT NULL,
    record_type character varying NOT NULL
);

CREATE INDEX index_active_storage_attachments_on_blob_id ON public.active_storage_attachments USING btree (blob_id);
CREATE UNIQUE INDEX index_active_storage_attachments_uniqueness ON public.active_storage_attachments USING btree (record_type, record_id, name, blob_id);

-- Name: active_storage_attachments_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.active_storage_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: active_storage_attachments_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.active_storage_attachments_id_seq OWNED BY public.active_storage_attachments.id;

-- Name: active_storage_blobs; Type: TABLE

CREATE TABLE public.active_storage_blobs (
    byte_size bigint NOT NULL,
    checksum character varying,
    content_type character varying,
    created_at timestamp(6) without time zone NOT NULL,
    filename character varying NOT NULL,
    id bigint NOT NULL,
    key character varying NOT NULL,
    metadata text,
    service_name character varying NOT NULL
);

CREATE UNIQUE INDEX index_active_storage_blobs_on_key ON public.active_storage_blobs USING btree (key);

-- Name: active_storage_blobs_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.active_storage_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: active_storage_blobs_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.active_storage_blobs_id_seq OWNED BY public.active_storage_blobs.id;

-- Name: active_storage_variant_records; Type: TABLE

CREATE TABLE public.active_storage_variant_records (
    blob_id bigint NOT NULL,
    id bigint NOT NULL,
    variation_digest character varying NOT NULL
);

CREATE UNIQUE INDEX index_active_storage_variant_records_uniqueness ON public.active_storage_variant_records USING btree (blob_id, variation_digest);

-- Name: active_storage_variant_records_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.active_storage_variant_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: active_storage_variant_records_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.active_storage_variant_records_id_seq OWNED BY public.active_storage_variant_records.id;

-- Name: ar_internal_metadata; Type: TABLE

CREATE TABLE public.ar_internal_metadata (
    created_at timestamp(6) without time zone NOT NULL,
    key character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    value character varying
);

-- Name: authentications; Type: TABLE

CREATE TABLE public.authentications (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    provider character varying NOT NULL,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer NOT NULL
);

CREATE INDEX index_authentications_on_provider_and_uid ON public.authentications USING btree (provider, uid);

-- Name: authentications_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.authentications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: authentications_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.authentications_id_seq OWNED BY public.authentications.id;

-- Name: common_products; Type: TABLE

CREATE TABLE public.common_products (
    brand character varying(255),
    created_at timestamp(6) without time zone NOT NULL,
    data_origin character varying(255),
    id bigint NOT NULL,
    kind character varying(255),
    model_number character varying(255),
    name character varying(500) NOT NULL,
    properties jsonb DEFAULT '"{}"'::jsonb NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    website character varying(255)
);

CREATE INDEX index_common_products_on_name ON public.common_products USING btree (name);
CREATE UNIQUE INDEX index_common_products_on_name_and_brand_and_model_number ON public.common_products USING btree (name, brand, model_number);

-- Name: common_products_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.common_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: common_products_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.common_products_id_seq OWNED BY public.common_products.id;

-- Name: data_custom; Type: TABLE

CREATE TABLE public.data_custom (
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    external_id character varying NOT NULL,
    import_id bigint NOT NULL,
    last_import_id bigint,
    last_source_id bigint,
    organization_id bigint NOT NULL,
    source_id bigint NOT NULL,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_custom_on_external_id ON public.data_custom USING btree (external_id);
CREATE INDEX index_data_custom_on_import_id ON public.data_custom USING btree (import_id);
CREATE INDEX index_data_custom_on_last_import_id ON public.data_custom USING btree (last_import_id);
CREATE INDEX index_data_custom_on_last_source_id ON public.data_custom USING btree (last_source_id);
CREATE INDEX index_data_custom_on_organization_id ON public.data_custom USING btree (organization_id);
CREATE INDEX index_data_custom_on_source_id ON public.data_custom USING btree (source_id);

-- Name: data_grids; Type: TABLE

CREATE TABLE public.data_grids (
    country character varying,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    external_id character varying,
    import_id bigint NOT NULL,
    is_offgrid boolean,
    last_import_id bigint,
    last_source_id bigint,
    latitude double precision,
    location_area_1 character varying,
    location_area_2 character varying,
    location_area_3 character varying,
    location_area_4 character varying,
    longitude double precision,
    name character varying NOT NULL,
    operator_company character varying,
    operator_phone_e character varying,
    operator_phone_p character varying,
    organization_id bigint NOT NULL,
    power_rating_kw double precision,
    primary_input_installed_kw double precision,
    primary_input_source character varying,
    secondary_input_installed_kw double precision,
    secondary_input_source character varying,
    source_id bigint NOT NULL,
    storage_capacity_kwh double precision,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_grids_on_external_id ON public.data_grids USING btree (external_id);
CREATE INDEX index_data_grids_on_import_id ON public.data_grids USING btree (import_id);
CREATE INDEX index_data_grids_on_last_import_id ON public.data_grids USING btree (last_import_id);
CREATE INDEX index_data_grids_on_last_source_id ON public.data_grids USING btree (last_source_id);
CREATE INDEX index_data_grids_on_organization_id ON public.data_grids USING btree (organization_id);
CREATE INDEX index_data_grids_on_source_id ON public.data_grids USING btree (source_id);

-- Name: data_grids_ts; Type: TABLE

CREATE TABLE public.data_grids_ts (
    battery_charge_state_percent double precision,
    battery_to_customer_kwh double precision,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    grid_name character varying NOT NULL,
    grid_uid character varying NOT NULL,
    households_connected integer,
    import_id bigint NOT NULL,
    interval_seconds integer,
    metered_at timestamp without time zone NOT NULL,
    organization_id bigint NOT NULL,
    primary_source_to_battery_kwh double precision,
    primary_source_to_customer_kwh double precision,
    secondary_source_to_battery_kwh double precision,
    secondary_source_to_customer_kwh double precision,
    source_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX data_grids_ts_metered_at_idx ON public.data_grids_ts USING btree (metered_at DESC);
CREATE INDEX index_data_grids_ts_on_import_id ON public.data_grids_ts USING btree (import_id);
CREATE INDEX index_data_grids_ts_on_import_id_and_metered_at ON public.data_grids_ts USING btree (import_id DESC, metered_at DESC);
CREATE INDEX index_data_grids_ts_on_organization_id ON public.data_grids_ts USING btree (organization_id);
CREATE INDEX index_data_grids_ts_on_organization_id_and_metered_at ON public.data_grids_ts USING btree (organization_id DESC, metered_at DESC);
CREATE INDEX index_data_grids_ts_on_source_id ON public.data_grids_ts USING btree (source_id);
CREATE INDEX index_data_grids_ts_on_source_id_and_metered_at ON public.data_grids_ts USING btree (source_id DESC, metered_at DESC);

-- Name: data_meter_events_ts; Type: TABLE

CREATE TABLE public.data_meter_events_ts (
    category character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    device_uid character varying NOT NULL,
    duration_sec integer,
    end_at timestamp without time zone NOT NULL,
    energy_consumed_wh double precision,
    energy_end_wh double precision,
    energy_start_wh double precision,
    manufacturer character varying DEFAULT ''::character varying NOT NULL,
    organization_id bigint NOT NULL,
    serial_number character varying DEFAULT ''::character varying NOT NULL,
    source_id bigint NOT NULL,
    start_at timestamp without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_meter_events_ts_on_category_and_start_at ON public.data_meter_events_ts USING btree (category DESC, start_at DESC);
CREATE INDEX index_data_meter_events_ts_on_organization_id_and_start_at ON public.data_meter_events_ts USING btree (organization_id DESC, start_at DESC);
CREATE INDEX index_data_meter_events_ts_on_source_id_and_start_at ON public.data_meter_events_ts USING btree (source_id DESC, start_at DESC);
CREATE INDEX index_data_meter_events_ts_on_start_at ON public.data_meter_events_ts USING btree (start_at DESC);

-- Name: data_meters; Type: TABLE

CREATE TABLE public.data_meters (
    account_external_id character varying,
    account_uid character varying,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    customer_address_e character varying,
    customer_address_p character varying,
    customer_birth_year integer,
    customer_category character varying,
    customer_country character varying,
    customer_email_e character varying,
    customer_email_p character varying,
    customer_external_id character varying NOT NULL,
    customer_former_electricity_source character varying,
    customer_gender public.gender,
    customer_household_size integer,
    customer_id_number_e character varying,
    customer_id_number_p character varying,
    customer_id_type character varying,
    customer_latitude_b double precision,
    customer_latitude_e character varying,
    customer_latitude_p character varying,
    customer_location_area_1 character varying,
    customer_location_area_2 character varying,
    customer_location_area_3 character varying,
    customer_location_area_4 character varying,
    customer_location_area_5 character varying,
    customer_longitude_b double precision,
    customer_longitude_e character varying,
    customer_longitude_p character varying,
    customer_name_e character varying,
    customer_name_p character varying,
    customer_phone_2_e character varying,
    customer_phone_2_p character varying,
    customer_phone_e character varying,
    customer_phone_p character varying,
    customer_profession character varying,
    customer_sub_category character varying,
    customer_uid character varying NOT NULL,
    data_origin character varying NOT NULL,
    device_external_id character varying NOT NULL,
    device_uid character varying NOT NULL,
    firmware_version character varying,
    grid_name character varying,
    grid_uid character varying,
    import_id bigint NOT NULL,
    installation_date date,
    is_locked boolean,
    is_test boolean,
    last_import_id bigint,
    last_source_id bigint,
    manufacturer character varying NOT NULL,
    max_power_w double precision,
    model character varying,
    organization_id bigint NOT NULL,
    payment_plan character varying,
    payment_plan_amount_financed double precision,
    payment_plan_currency character varying,
    payment_plan_days double precision,
    payment_plan_down_payment double precision,
    primary_use character varying,
    purchase_date date,
    repossession_date date,
    serial_number character varying NOT NULL,
    source_id bigint NOT NULL,
    total_price double precision,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_meters_on_account_external_id ON public.data_meters USING btree (account_external_id);
CREATE INDEX index_data_meters_on_account_uid ON public.data_meters USING btree (account_uid);
CREATE INDEX index_data_meters_on_customer_external_id ON public.data_meters USING btree (customer_external_id);
CREATE INDEX index_data_meters_on_customer_uid ON public.data_meters USING btree (customer_uid);
CREATE INDEX index_data_meters_on_device_external_id ON public.data_meters USING btree (device_external_id);
CREATE INDEX index_data_meters_on_device_uid ON public.data_meters USING btree (device_uid);
CREATE INDEX index_data_meters_on_grid_uid ON public.data_meters USING btree (grid_uid);
CREATE INDEX index_data_meters_on_import_id ON public.data_meters USING btree (import_id);
CREATE INDEX index_data_meters_on_last_import_id ON public.data_meters USING btree (last_import_id);
CREATE INDEX index_data_meters_on_last_source_id ON public.data_meters USING btree (last_source_id);
CREATE INDEX index_data_meters_on_organization_id ON public.data_meters USING btree (organization_id);
CREATE INDEX index_data_meters_on_serial_number ON public.data_meters USING btree (serial_number);
CREATE INDEX index_data_meters_on_source_id ON public.data_meters USING btree (source_id);

-- Name: data_meters_ts; Type: TABLE

CREATE TABLE public.data_meters_ts (
    billing_cycle_start_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    current_a double precision,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    device_uid character varying NOT NULL,
    energy_interval_wh double precision,
    energy_lifetime_wh double precision,
    frequency_hz double precision,
    import_id bigint NOT NULL,
    interval_seconds integer,
    manufacturer character varying DEFAULT ''::character varying NOT NULL,
    metered_at timestamp without time zone NOT NULL,
    organization_id bigint NOT NULL,
    phase public.phase DEFAULT '1'::public.phase,
    power_factor double precision,
    power_w double precision,
    serial_number character varying DEFAULT ''::character varying NOT NULL,
    source_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    voltage_v double precision
);

CREATE INDEX data_meters_ts_metered_at_idx ON public.data_meters_ts USING btree (metered_at DESC);
CREATE INDEX index_data_meters_ts_on_import_id_and_metered_at ON public.data_meters_ts USING btree (import_id DESC, metered_at DESC);
CREATE INDEX index_data_meters_ts_on_organization_id_and_metered_at ON public.data_meters_ts USING btree (organization_id DESC, metered_at DESC);
CREATE INDEX index_data_meters_ts_on_source_id_and_metered_at ON public.data_meters_ts USING btree (source_id DESC, metered_at DESC);

-- Name: data_paygo_accounts_snapshots_ts; Type: TABLE

CREATE TABLE public.data_paygo_accounts_snapshots_ts (
    account_external_id character varying NOT NULL,
    account_status public.account_status NOT NULL,
    account_uid character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    cumulative_days_locked integer NOT NULL,
    cumulative_paid_amount double precision NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    days_in_repayment integer NOT NULL,
    days_to_cutoff integer NOT NULL,
    organization_id bigint NOT NULL,
    snapshot_date date NOT NULL,
    source_id bigint NOT NULL,
    total_remaining_amount double precision NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_paygo_accounts_snapshots_ts_on_snapshot_date ON public.data_paygo_accounts_snapshots_ts USING btree (snapshot_date DESC);
CREATE INDEX index_paygo_snapshots_on_account_ex_id_and_snapshot_date ON public.data_paygo_accounts_snapshots_ts USING btree (account_external_id DESC, snapshot_date DESC);
CREATE INDEX index_paygo_snapshots_on_org_id_and_snapshot_date ON public.data_paygo_accounts_snapshots_ts USING btree (organization_id DESC, snapshot_date DESC);
CREATE INDEX index_paygo_snapshots_on_src_id_and_snapshot_date ON public.data_paygo_accounts_snapshots_ts USING btree (source_id DESC, snapshot_date DESC);

-- Name: data_payments_ts; Type: TABLE

CREATE TABLE public.data_payments_ts (
    account_external_id character varying NOT NULL,
    account_origin public.payments_ts_account_origin NOT NULL,
    account_uid character varying NOT NULL,
    amount double precision NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    currency character varying NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    days_active double precision,
    import_id bigint NOT NULL,
    last_import_id bigint,
    last_source_id bigint,
    organization_id bigint NOT NULL,
    paid_at timestamp(6) without time zone NOT NULL,
    payment_external_id character varying NOT NULL,
    provider_name character varying,
    provider_transaction_id character varying,
    purchase_item character varying,
    purchase_quantity double precision,
    purchase_unit character varying,
    purpose character varying,
    reverted_at timestamp(6) without time zone,
    source_id bigint NOT NULL,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX data_payments_ts_paid_at_idx ON public.data_payments_ts USING btree (paid_at DESC);
CREATE INDEX index_data_payments_ts_on_account_external_id ON public.data_payments_ts USING btree (account_external_id);
CREATE INDEX index_data_payments_ts_on_account_origin ON public.data_payments_ts USING btree (account_origin);
CREATE INDEX index_data_payments_ts_on_account_uid ON public.data_payments_ts USING btree (account_uid);
CREATE INDEX index_data_payments_ts_on_import_id_and_paid_at ON public.data_payments_ts USING btree (import_id DESC, paid_at DESC);
CREATE INDEX index_data_payments_ts_on_last_import_id ON public.data_payments_ts USING btree (last_import_id);
CREATE INDEX index_data_payments_ts_on_last_source_id ON public.data_payments_ts USING btree (last_source_id);
CREATE INDEX index_data_payments_ts_on_organization_id_and_paid_at ON public.data_payments_ts USING btree (organization_id DESC, paid_at DESC);
CREATE INDEX index_data_payments_ts_on_payment_external_id_and_paid_at ON public.data_payments_ts USING btree (payment_external_id, paid_at);
CREATE INDEX index_data_payments_ts_on_provider_transaction_id_and_paid_at ON public.data_payments_ts USING btree (provider_transaction_id, paid_at);
CREATE INDEX index_data_payments_ts_on_source_id_and_paid_at ON public.data_payments_ts USING btree (source_id DESC, paid_at DESC);

-- Name: data_shs; Type: TABLE

CREATE TABLE public.data_shs (
    account_external_id character varying NOT NULL,
    account_uid character varying NOT NULL,
    battery_energy_wh double precision,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    customer_address_e character varying,
    customer_address_p character varying,
    customer_birth_year integer,
    customer_category character varying,
    customer_country character varying,
    customer_email_e character varying,
    customer_email_p character varying,
    customer_external_id character varying NOT NULL,
    customer_former_electricity_source character varying,
    customer_gender public.gender,
    customer_household_size integer,
    customer_id_number_e character varying,
    customer_id_number_p character varying,
    customer_id_type character varying,
    customer_latitude_b double precision,
    customer_latitude_e character varying,
    customer_latitude_p character varying,
    customer_location_area_1 character varying,
    customer_location_area_2 character varying,
    customer_location_area_3 character varying,
    customer_location_area_4 character varying,
    customer_location_area_5 character varying,
    customer_longitude_b double precision,
    customer_longitude_e character varying,
    customer_longitude_p character varying,
    customer_name_e character varying,
    customer_name_p character varying,
    customer_phone_2_e character varying,
    customer_phone_2_p character varying,
    customer_phone_e character varying,
    customer_phone_p character varying,
    customer_profession character varying,
    customer_sub_category character varying,
    customer_uid character varying NOT NULL,
    data_origin character varying NOT NULL,
    device_external_id character varying NOT NULL,
    device_uid character varying NOT NULL,
    import_id bigint NOT NULL,
    is_test boolean,
    last_import_id bigint,
    last_source_id bigint,
    manufacturer character varying NOT NULL,
    model character varying,
    organization_id bigint NOT NULL,
    paid_off_date date,
    payment_plan_amount_financed double precision,
    payment_plan_currency character varying,
    payment_plan_days double precision,
    payment_plan_down_payment double precision,
    payment_plan_type character varying,
    primary_use character varying,
    purchase_date date,
    pv_power_w double precision,
    rated_power_w double precision,
    repossession_date date,
    seller_address_e character varying,
    seller_address_p character varying,
    seller_country character varying,
    seller_email_e character varying,
    seller_email_p character varying,
    seller_external_id character varying,
    seller_gender public.gender,
    seller_latitude_b double precision,
    seller_latitude_e character varying,
    seller_latitude_p character varying,
    seller_location_area_1 character varying,
    seller_location_area_2 character varying,
    seller_location_area_3 character varying,
    seller_location_area_4 character varying,
    seller_longitude_b double precision,
    seller_longitude_e character varying,
    seller_longitude_p character varying,
    seller_name_e character varying,
    seller_name_p character varying,
    seller_phone_e character varying,
    seller_phone_p character varying,
    serial_number character varying NOT NULL,
    source_id bigint NOT NULL,
    total_price double precision,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    voltage_v double precision
);

CREATE INDEX index_data_shs_on_account_external_id ON public.data_shs USING btree (account_external_id);
CREATE INDEX index_data_shs_on_account_uid ON public.data_shs USING btree (account_uid);
CREATE INDEX index_data_shs_on_customer_external_id ON public.data_shs USING btree (customer_external_id);
CREATE INDEX index_data_shs_on_customer_uid ON public.data_shs USING btree (customer_uid);
CREATE INDEX index_data_shs_on_device_external_id ON public.data_shs USING btree (device_external_id);
CREATE INDEX index_data_shs_on_device_uid ON public.data_shs USING btree (device_uid);
CREATE INDEX index_data_shs_on_import_id ON public.data_shs USING btree (import_id);
CREATE INDEX index_data_shs_on_last_import_id ON public.data_shs USING btree (last_import_id);
CREATE INDEX index_data_shs_on_last_source_id ON public.data_shs USING btree (last_source_id);
CREATE INDEX index_data_shs_on_organization_id ON public.data_shs USING btree (organization_id);
CREATE INDEX index_data_shs_on_seller_external_id ON public.data_shs USING btree (seller_external_id);
CREATE INDEX index_data_shs_on_serial_number ON public.data_shs USING btree (serial_number);
CREATE INDEX index_data_shs_on_source_id ON public.data_shs USING btree (source_id);

-- Name: data_shs_ts; Type: TABLE

CREATE TABLE public.data_shs_ts (
    battery_charge_percent double precision,
    battery_io_w double precision,
    battery_v double precision,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    device_uid character varying NOT NULL,
    grid_energy_interval_wh double precision,
    grid_energy_lifetime_wh double precision,
    grid_input_v double precision,
    grid_input_w double precision,
    import_id bigint NOT NULL,
    interval_seconds integer,
    manufacturer character varying NOT NULL,
    metered_at timestamp without time zone NOT NULL,
    organization_id bigint NOT NULL,
    output_energy_interval_wh double precision,
    output_energy_lifetime_wh double precision,
    paygo_days_left integer,
    paygo_is_paid_off boolean,
    pv_energy_interval_wh double precision,
    pv_energy_lifetime_wh double precision,
    pv_input_w double precision,
    serial_number character varying NOT NULL,
    source_id bigint NOT NULL,
    system_output_w double precision,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX data_shs_ts_metered_at_idx ON public.data_shs_ts USING btree (metered_at DESC);
CREATE INDEX index_data_shs_ts_on_import_id_and_metered_at ON public.data_shs_ts USING btree (import_id DESC, metered_at DESC);
CREATE INDEX index_data_shs_ts_on_organization_id_and_metered_at ON public.data_shs_ts USING btree (organization_id DESC, metered_at DESC);
CREATE INDEX index_data_shs_ts_on_source_id_and_metered_at ON public.data_shs_ts USING btree (source_id DESC, metered_at DESC);

-- Name: data_test; Type: TABLE

CREATE TABLE public.data_test (
    bool_column boolean,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    enum_column public.enum_example,
    float_column double precision NOT NULL,
    import_id bigint NOT NULL,
    int_column integer,
    last_import_id bigint,
    last_source_id bigint,
    name_column_e character varying,
    name_column_p character varying,
    organization_id bigint NOT NULL,
    personal_column character varying,
    something_uid character varying,
    source_id bigint NOT NULL,
    time_column timestamp(6) without time zone,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_test_on_import_id ON public.data_test USING btree (import_id);
CREATE INDEX index_data_test_on_last_import_id ON public.data_test USING btree (last_import_id);
CREATE INDEX index_data_test_on_last_source_id ON public.data_test USING btree (last_source_id);
CREATE INDEX index_data_test_on_organization_id ON public.data_test USING btree (organization_id);
CREATE INDEX index_data_test_on_something_uid ON public.data_test USING btree (something_uid);
CREATE INDEX index_data_test_on_source_id ON public.data_test USING btree (source_id);

-- Name: data_trust_trace; Type: TABLE

CREATE TABLE public.data_trust_trace (
    "check" character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    custom jsonb DEFAULT '{}'::jsonb NOT NULL,
    data_origin character varying NOT NULL,
    import_id bigint NOT NULL,
    last_import_id bigint,
    last_source_id bigint,
    organization_id bigint NOT NULL,
    result character varying NOT NULL,
    source_id bigint NOT NULL,
    subject_origin public.trust_trace_subject_origin NOT NULL,
    subject_uid character varying NOT NULL,
    uid character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_data_trust_trace_on_check ON public.data_trust_trace USING btree ("check");
CREATE INDEX index_data_trust_trace_on_import_id ON public.data_trust_trace USING btree (import_id);
CREATE INDEX index_data_trust_trace_on_last_import_id ON public.data_trust_trace USING btree (last_import_id);
CREATE INDEX index_data_trust_trace_on_last_source_id ON public.data_trust_trace USING btree (last_source_id);
CREATE INDEX index_data_trust_trace_on_organization_id ON public.data_trust_trace USING btree (organization_id);
CREATE INDEX index_data_trust_trace_on_result ON public.data_trust_trace USING btree (result);
CREATE INDEX index_data_trust_trace_on_source_id ON public.data_trust_trace USING btree (source_id);
CREATE INDEX index_data_trust_trace_on_subject_origin ON public.data_trust_trace USING btree (subject_origin);
CREATE INDEX index_data_trust_trace_on_subject_uid ON public.data_trust_trace USING btree (subject_uid);

-- Name: dynamic_forms; Type: TABLE

CREATE TABLE public.dynamic_forms (
    active boolean DEFAULT true NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    description character varying,
    form_data json,
    id bigint NOT NULL,
    name character varying,
    rbf_claim_template_id bigint,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_dynamic_forms_on_rbf_claim_template_id ON public.dynamic_forms USING btree (rbf_claim_template_id);

-- Name: dynamic_forms_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.dynamic_forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: dynamic_forms_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.dynamic_forms_id_seq OWNED BY public.dynamic_forms.id;

-- Name: encrypted_blobs; Type: TABLE

CREATE TABLE public.encrypted_blobs (
    created_at timestamp(6) without time zone NOT NULL,
    error text,
    id bigint NOT NULL,
    import_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    url_e text
);

CREATE INDEX index_encrypted_blobs_on_import_id ON public.encrypted_blobs USING btree (import_id);

-- Name: encrypted_blobs_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.encrypted_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: encrypted_blobs_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.encrypted_blobs_id_seq OWNED BY public.encrypted_blobs.id;

-- Name: history; Type: TABLE

CREATE TABLE public.history (
    created_at timestamp(6) without time zone NOT NULL,
    data_table character varying,
    id bigint NOT NULL,
    import_id bigint NOT NULL,
    new_row jsonb,
    old_row jsonb NOT NULL,
    organization_id bigint NOT NULL,
    reverted_at timestamp(6) without time zone,
    source_id bigint NOT NULL,
    uid character varying
);

CREATE INDEX index_history_on_data_table ON public.history USING btree (data_table);
CREATE INDEX index_history_on_import_id ON public.history USING btree (import_id);
CREATE INDEX index_history_on_organization_id ON public.history USING btree (organization_id);
CREATE INDEX index_history_on_source_id ON public.history USING btree (source_id);
CREATE INDEX index_history_on_uid ON public.history USING btree (uid);

-- Name: history_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: history_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.history_id_seq OWNED BY public.history.id;

-- Name: imports; Type: TABLE

CREATE TABLE public.imports (
    created_at timestamp(6) without time zone NOT NULL,
    error character varying,
    id bigint NOT NULL,
    ingestion_finished_at timestamp without time zone,
    ingestion_started_at timestamp without time zone,
    last_imported_hint character varying,
    processing_finished_at timestamp without time zone,
    processing_started_at timestamp without time zone,
    protocol text,
    rows_duplicated integer,
    rows_failed integer,
    rows_inserted integer,
    rows_updated integer,
    source_id bigint,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE INDEX index_imports_on_source_id ON public.imports USING btree (source_id);

-- Name: imports_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.imports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: imports_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.imports_id_seq OWNED BY public.imports.id;

-- Name: org_device_mappers; Type: TABLE

CREATE TABLE public.org_device_mappers (
    created_at timestamp(6) without time zone NOT NULL,
    device_id bigint NOT NULL,
    device_type character varying NOT NULL,
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE UNIQUE INDEX index_org_device_mappers_on_device_type_and_device_id ON public.org_device_mappers USING btree (device_type, device_id);
CREATE INDEX index_org_device_mappers_on_device_type_and_organization_id ON public.org_device_mappers USING btree (device_type, organization_id);
CREATE INDEX index_org_device_mappers_on_organization_id ON public.org_device_mappers USING btree (organization_id);

-- Name: org_device_mappers_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.org_device_mappers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: org_device_mappers_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.org_device_mappers_id_seq OWNED BY public.org_device_mappers.id;

-- Name: organization_group_members; Type: TABLE

CREATE TABLE public.organization_group_members (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    organization_group_id bigint NOT NULL,
    organization_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint,
    visible_for_sharing boolean DEFAULT false NOT NULL
);

CREATE UNIQUE INDEX idx_on_organization_group_id_organization_id_3f94b906ec ON public.organization_group_members USING btree (organization_group_id, organization_id);
CREATE INDEX index_organization_group_members_on_organization_group_id ON public.organization_group_members USING btree (organization_group_id);
CREATE INDEX index_organization_group_members_on_organization_id ON public.organization_group_members USING btree (organization_id);

-- Name: organization_group_members_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.organization_group_members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: organization_group_members_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.organization_group_members_id_seq OWNED BY public.organization_group_members.id;

-- Name: organization_groups; Type: TABLE

CREATE TABLE public.organization_groups (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    name character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint
);

CREATE UNIQUE INDEX index_organization_groups_on_name ON public.organization_groups USING btree (name);

-- Name: organization_groups_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.organization_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: organization_groups_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.organization_groups_id_seq OWNED BY public.organization_groups.id;

-- Name: organizations; Type: TABLE

CREATE TABLE public.organizations (
    api_key character varying(1000),
    created_at timestamp(6) without time zone NOT NULL,
    crypto_added_at timestamp without time zone,
    grafana_org_id integer,
    id bigint NOT NULL,
    iv text,
    name character varying NOT NULL,
    postgres_password character varying(1000),
    public_key text,
    salt text,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer,
    wrapped_key text
);

CREATE UNIQUE INDEX index_organizations_on_name ON public.organizations USING btree (name);
CREATE INDEX index_organizations_on_user_id ON public.organizations USING btree (user_id);

-- Name: organizations_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.organizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: organizations_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.organizations_id_seq OWNED BY public.organizations.id;

-- Name: organizations_users; Type: TABLE

CREATE TABLE public.organizations_users (
    activated_at timestamp(6) without time zone,
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    user_id bigint NOT NULL
);

CREATE INDEX index_organizations_users_on_organization_id ON public.organizations_users USING btree (organization_id);
CREATE INDEX index_organizations_users_on_user_id ON public.organizations_users USING btree (user_id);
CREATE UNIQUE INDEX index_organizations_users_on_user_id_and_organization_id ON public.organizations_users USING btree (user_id, organization_id);

-- Name: organizations_users_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.organizations_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: organizations_users_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.organizations_users_id_seq OWNED BY public.organizations_users.id;

-- Name: privileges; Type: TABLE

CREATE TABLE public.privileges (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    name character varying,
    organization_user_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint NOT NULL
);

CREATE INDEX index_privileges_on_name ON public.privileges USING btree (name);
CREATE INDEX index_privileges_on_organization_user_id ON public.privileges USING btree (organization_user_id);
CREATE UNIQUE INDEX index_privileges_on_organization_user_id_and_name ON public.privileges USING btree (organization_user_id, name);
CREATE INDEX index_privileges_on_user_id ON public.privileges USING btree (user_id);

-- Name: privileges_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.privileges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: privileges_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.privileges_id_seq OWNED BY public.privileges.id;

-- Name: projects; Type: TABLE

CREATE TABLE public.projects (
    created_at timestamp(6) without time zone NOT NULL,
    description character varying,
    id bigint NOT NULL,
    name character varying NOT NULL,
    organization_id integer NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer
);

CREATE UNIQUE INDEX index_projects_on_name_and_organization_id ON public.projects USING btree (name, organization_id);
CREATE INDEX index_projects_on_organization_id ON public.projects USING btree (organization_id);
CREATE INDEX index_projects_on_user_id ON public.projects USING btree (user_id);

-- Name: projects_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: projects_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;

-- Name: rbf_claim_templates; Type: TABLE

CREATE TABLE public.rbf_claim_templates (
    created_at timestamp(6) without time zone NOT NULL,
    currency character varying NOT NULL,
    id bigint NOT NULL,
    managing_organization_id bigint NOT NULL,
    name character varying,
    organization_group_id bigint NOT NULL,
    supervising_organization_id bigint NOT NULL,
    trust_trace_check character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint NOT NULL,
    verifying_organization_id bigint NOT NULL
);

CREATE INDEX index_rbf_claim_templates_on_managing_organization_id ON public.rbf_claim_templates USING btree (managing_organization_id);
CREATE UNIQUE INDEX index_rbf_claim_templates_on_name ON public.rbf_claim_templates USING btree (name);
CREATE INDEX index_rbf_claim_templates_on_organization_group_id ON public.rbf_claim_templates USING btree (organization_group_id);
CREATE INDEX index_rbf_claim_templates_on_supervising_organization_id ON public.rbf_claim_templates USING btree (supervising_organization_id);
CREATE INDEX index_rbf_claim_templates_on_user_id ON public.rbf_claim_templates USING btree (user_id);
CREATE INDEX index_rbf_claim_templates_on_verifying_organization_id ON public.rbf_claim_templates USING btree (verifying_organization_id);

-- Name: rbf_claim_templates_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_claim_templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_claim_templates_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_claim_templates_id_seq OWNED BY public.rbf_claim_templates.id;

-- Name: rbf_claim_verifiers; Type: TABLE

CREATE TABLE public.rbf_claim_verifiers (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    rbf_claim_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint,
    verifier_id bigint NOT NULL
);

CREATE UNIQUE INDEX index_rbf_claim_verifiers_on_rbf_claim_id_and_verifier_id ON public.rbf_claim_verifiers USING btree (rbf_claim_id, verifier_id);

-- Name: rbf_claim_verifiers_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_claim_verifiers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_claim_verifiers_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_claim_verifiers_id_seq OWNED BY public.rbf_claim_verifiers.id;

-- Name: rbf_claims; Type: TABLE

CREATE TABLE public.rbf_claims (
    aasm_state character varying,
    approved_at timestamp without time zone,
    approved_by_id bigint,
    assigned_to_verifier_id bigint,
    claimed_data jsonb DEFAULT '{}'::jsonb NOT NULL,
    claimed_subunit integer,
    code character varying,
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    paid_out_at timestamp without time zone,
    paid_out_by_id bigint,
    rbf_claim_template_id bigint NOT NULL,
    rejected_at timestamp without time zone,
    rejected_by_id bigint,
    submitted_to_program_at timestamp without time zone,
    submitted_to_program_by_id bigint,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint NOT NULL,
    verification_at timestamp without time zone,
    verification_by_id bigint,
    verified_data jsonb DEFAULT '{}'::jsonb NOT NULL,
    verified_subunit integer
);

CREATE INDEX index_rbf_claims_on_aasm_state ON public.rbf_claims USING btree (aasm_state);
CREATE UNIQUE INDEX index_rbf_claims_on_code ON public.rbf_claims USING btree (code);
CREATE INDEX index_rbf_claims_on_organization_id ON public.rbf_claims USING btree (organization_id);
CREATE INDEX index_rbf_claims_on_rbf_claim_template_id ON public.rbf_claims USING btree (rbf_claim_template_id);
CREATE INDEX index_rbf_claims_on_user_id ON public.rbf_claims USING btree (user_id);

-- Name: rbf_claims_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_claims_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_claims_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_claims_id_seq OWNED BY public.rbf_claims.id;

-- Name: rbf_device_claims; Type: TABLE

CREATE TABLE public.rbf_device_claims (
    claimed_devices_data jsonb DEFAULT '{}'::jsonb NOT NULL,
    claimed_incentive integer DEFAULT 0,
    claimed_payments_data jsonb DEFAULT '[]'::jsonb NOT NULL,
    claimed_subsidy integer DEFAULT 0,
    claimed_trust_trace_data jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    device_category character varying,
    id bigint NOT NULL,
    parent_rbf_device_claim_id bigint,
    rbf_claim_id bigint NOT NULL,
    rejection_category character varying,
    reviewer_comment text,
    reviewer_id bigint,
    state character varying NOT NULL,
    subject_origin character varying NOT NULL,
    subject_uid character varying NOT NULL,
    submission_category character varying,
    submission_comment text,
    trust_trace_uid character varying,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint NOT NULL,
    verification_category character varying,
    verified_data jsonb DEFAULT '{}'::jsonb NOT NULL,
    verified_incentive integer DEFAULT 0,
    verified_subsidy integer DEFAULT 0
);

CREATE INDEX index_rbf_device_claims_on_parent_rbf_device_claim_id ON public.rbf_device_claims USING btree (parent_rbf_device_claim_id);
CREATE INDEX index_rbf_device_claims_on_rbf_claim_id ON public.rbf_device_claims USING btree (rbf_claim_id);
CREATE INDEX index_rbf_device_claims_on_reviewer_id ON public.rbf_device_claims USING btree (reviewer_id);
CREATE INDEX index_rbf_device_claims_on_subject_origin_and_subject_uid ON public.rbf_device_claims USING btree (subject_origin, subject_uid);
CREATE INDEX index_rbf_device_claims_on_trust_trace_uid ON public.rbf_device_claims USING btree (trust_trace_uid);
CREATE INDEX index_rbf_device_claims_on_user_id ON public.rbf_device_claims USING btree (user_id);

-- Name: rbf_device_claims_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_device_claims_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_device_claims_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_device_claims_id_seq OWNED BY public.rbf_device_claims.id;

-- Name: rbf_organization_configs; Type: TABLE

CREATE TABLE public.rbf_organization_configs (
    code character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    rbf_claim_template_id integer NOT NULL,
    total_grant_allocation double precision DEFAULT 0.0 NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer
);

CREATE UNIQUE INDEX idx_on_code_rbf_claim_template_id_967e2caa74 ON public.rbf_organization_configs USING btree (code, rbf_claim_template_id);
CREATE UNIQUE INDEX idx_on_organization_id_rbf_claim_template_id_2d999cb87a ON public.rbf_organization_configs USING btree (organization_id, rbf_claim_template_id);

-- Name: rbf_organization_configs_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_organization_configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_organization_configs_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_organization_configs_id_seq OWNED BY public.rbf_organization_configs.id;

-- Name: rbf_product_prices; Type: TABLE

CREATE TABLE public.rbf_product_prices (
    base_subsidized_sales_price integer NOT NULL,
    base_subsidy_amount integer NOT NULL,
    category character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    original_sales_price double precision NOT NULL,
    rbf_product_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint,
    valid_from date NOT NULL
);

CREATE UNIQUE INDEX idx_on_rbf_product_id_category_valid_from_73af8c87d8 ON public.rbf_product_prices USING btree (rbf_product_id, category, valid_from);
CREATE INDEX index_rbf_product_prices_on_rbf_product_id ON public.rbf_product_prices USING btree (rbf_product_id);

-- Name: rbf_product_prices_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_product_prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_product_prices_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_product_prices_id_seq OWNED BY public.rbf_product_prices.id;

-- Name: rbf_products; Type: TABLE

CREATE TABLE public.rbf_products (
    created_at timestamp(6) without time zone NOT NULL,
    id bigint NOT NULL,
    manufacturer character varying NOT NULL,
    model character varying NOT NULL,
    organization_id bigint NOT NULL,
    rated_power_w integer,
    rbf_category character varying NOT NULL,
    rbf_claim_template_id bigint NOT NULL,
    technology character varying,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint
);

CREATE UNIQUE INDEX idx_on_model_organization_id_manufacturer_c8532bbe4b ON public.rbf_products USING btree (model, organization_id, manufacturer);
CREATE INDEX index_rbf_products_on_organization_id ON public.rbf_products USING btree (organization_id);
CREATE INDEX index_rbf_products_on_rbf_claim_template_id ON public.rbf_products USING btree (rbf_claim_template_id);
CREATE INDEX index_rbf_products_on_user_id ON public.rbf_products USING btree (user_id);

-- Name: rbf_products_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.rbf_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: rbf_products_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.rbf_products_id_seq OWNED BY public.rbf_products.id;

-- Name: schema_migrations; Type: TABLE

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);

-- Name: sessions; Type: TABLE

CREATE TABLE public.sessions (
    created_at timestamp(6) without time zone NOT NULL,
    data text,
    id bigint NOT NULL,
    session_id character varying NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);

CREATE UNIQUE INDEX index_sessions_on_session_id ON public.sessions USING btree (session_id);
CREATE INDEX index_sessions_on_updated_at ON public.sessions USING btree (updated_at);

-- Name: sessions_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;

-- Name: shared_keys_vaults; Type: TABLE

CREATE TABLE public.shared_keys_vaults (
    created_at timestamp(6) without time zone NOT NULL,
    encrypted_key character varying NOT NULL,
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    shared_with_organization_id bigint NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id bigint
);

CREATE UNIQUE INDEX idx_on_organization_id_shared_with_organization_id_41045e9954 ON public.shared_keys_vaults USING btree (organization_id, shared_with_organization_id);
CREATE INDEX index_shared_keys_vaults_on_user_id ON public.shared_keys_vaults USING btree (user_id);

-- Name: shared_keys_vaults_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.shared_keys_vaults_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: shared_keys_vaults_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.shared_keys_vaults_id_seq OWNED BY public.shared_keys_vaults.id;

-- Name: sources; Type: TABLE

CREATE TABLE public.sources (
    activated_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    data_category character varying,
    details jsonb DEFAULT '{}'::jsonb NOT NULL,
    id bigint NOT NULL,
    kind character varying NOT NULL,
    name character varying NOT NULL,
    project_id bigint NOT NULL,
    secret character varying(1000),
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer
);

CREATE INDEX index_sources_on_activated_at ON public.sources USING btree (activated_at);
CREATE INDEX index_sources_on_kind ON public.sources USING btree (kind);
CREATE UNIQUE INDEX index_sources_on_name_and_project_id ON public.sources USING btree (name, project_id);
CREATE INDEX index_sources_on_project_id ON public.sources USING btree (project_id);
CREATE INDEX index_sources_on_user_id ON public.sources USING btree (user_id);

-- Name: sources_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: sources_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.sources_id_seq OWNED BY public.sources.id;

-- Name: users; Type: TABLE

CREATE TABLE public.users (
    access_count_to_reset_password_page integer DEFAULT 0,
    active_at timestamp without time zone,
    admin boolean DEFAULT false NOT NULL,
    archived_at timestamp without time zone,
    code_of_conduct json DEFAULT '{}'::json,
    created_at timestamp(6) without time zone NOT NULL,
    crypted_password character varying,
    data_protection_policy json DEFAULT '{}'::json,
    email character varying NOT NULL,
    id bigint NOT NULL,
    invite_code character varying,
    organization_id integer,
    reset_password_email_sent_at timestamp(6) without time zone,
    reset_password_token character varying,
    reset_password_token_expires_at timestamp(6) without time zone,
    salt character varying,
    terms_of_use json DEFAULT '{}'::json,
    updated_at timestamp(6) without time zone NOT NULL,
    username character varying
);

CREATE INDEX index_users_on_active_at ON public.users USING btree (active_at);
CREATE INDEX index_users_on_archived_at ON public.users USING btree (archived_at);
CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);
CREATE UNIQUE INDEX index_users_on_invite_code ON public.users USING btree (invite_code);
CREATE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);
CREATE INDEX index_users_on_username ON public.users USING btree (username);

-- Name: users_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: users_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;

-- Name: visibilities; Type: TABLE

CREATE TABLE public.visibilities (
    columns jsonb DEFAULT '{}'::jsonb NOT NULL,
    conditions jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    data_from timestamp(6) without time zone,
    data_until timestamp(6) without time zone,
    id bigint NOT NULL,
    license character varying,
    organization_id integer NOT NULL,
    project_id bigint NOT NULL,
    resharing boolean DEFAULT false NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    user_id integer
);

CREATE INDEX index_visibilities_on_organization_id ON public.visibilities USING btree (organization_id);
CREATE UNIQUE INDEX index_visibilities_on_project_id_and_organization_id ON public.visibilities USING btree (project_id, organization_id);
CREATE INDEX index_visibilities_on_user_id ON public.visibilities USING btree (user_id);

-- Name: visibilities_id_seq; Type: SEQUENCE

CREATE SEQUENCE public.visibilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Name: visibilities_id_seq; Type: SEQUENCE OWNED BY

ALTER SEQUENCE public.visibilities_id_seq OWNED BY public.visibilities.id;

-- Name: active_storage_attachments id; Type: DEFAULT

ALTER TABLE ONLY public.active_storage_attachments ALTER COLUMN id SET DEFAULT nextval('public.active_storage_attachments_id_seq'::regclass);

-- Name: active_storage_blobs id; Type: DEFAULT

ALTER TABLE ONLY public.active_storage_blobs ALTER COLUMN id SET DEFAULT nextval('public.active_storage_blobs_id_seq'::regclass);

-- Name: active_storage_variant_records id; Type: DEFAULT

ALTER TABLE ONLY public.active_storage_variant_records ALTER COLUMN id SET DEFAULT nextval('public.active_storage_variant_records_id_seq'::regclass);

-- Name: authentications id; Type: DEFAULT

ALTER TABLE ONLY public.authentications ALTER COLUMN id SET DEFAULT nextval('public.authentications_id_seq'::regclass);

-- Name: common_products id; Type: DEFAULT

ALTER TABLE ONLY public.common_products ALTER COLUMN id SET DEFAULT nextval('public.common_products_id_seq'::regclass);

-- Name: dynamic_forms id; Type: DEFAULT

ALTER TABLE ONLY public.dynamic_forms ALTER COLUMN id SET DEFAULT nextval('public.dynamic_forms_id_seq'::regclass);

-- Name: encrypted_blobs id; Type: DEFAULT

ALTER TABLE ONLY public.encrypted_blobs ALTER COLUMN id SET DEFAULT nextval('public.encrypted_blobs_id_seq'::regclass);

-- Name: history id; Type: DEFAULT

ALTER TABLE ONLY public.history ALTER COLUMN id SET DEFAULT nextval('public.history_id_seq'::regclass);

-- Name: imports id; Type: DEFAULT

ALTER TABLE ONLY public.imports ALTER COLUMN id SET DEFAULT nextval('public.imports_id_seq'::regclass);

-- Name: org_device_mappers id; Type: DEFAULT

ALTER TABLE ONLY public.org_device_mappers ALTER COLUMN id SET DEFAULT nextval('public.org_device_mappers_id_seq'::regclass);

-- Name: organization_group_members id; Type: DEFAULT

ALTER TABLE ONLY public.organization_group_members ALTER COLUMN id SET DEFAULT nextval('public.organization_group_members_id_seq'::regclass);

-- Name: organization_groups id; Type: DEFAULT

ALTER TABLE ONLY public.organization_groups ALTER COLUMN id SET DEFAULT nextval('public.organization_groups_id_seq'::regclass);

-- Name: organizations id; Type: DEFAULT

ALTER TABLE ONLY public.organizations ALTER COLUMN id SET DEFAULT nextval('public.organizations_id_seq'::regclass);

-- Name: organizations_users id; Type: DEFAULT

ALTER TABLE ONLY public.organizations_users ALTER COLUMN id SET DEFAULT nextval('public.organizations_users_id_seq'::regclass);

-- Name: privileges id; Type: DEFAULT

ALTER TABLE ONLY public.privileges ALTER COLUMN id SET DEFAULT nextval('public.privileges_id_seq'::regclass);

-- Name: projects id; Type: DEFAULT

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);

-- Name: rbf_claim_templates id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_claim_templates ALTER COLUMN id SET DEFAULT nextval('public.rbf_claim_templates_id_seq'::regclass);

-- Name: rbf_claim_verifiers id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_claim_verifiers ALTER COLUMN id SET DEFAULT nextval('public.rbf_claim_verifiers_id_seq'::regclass);

-- Name: rbf_claims id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_claims ALTER COLUMN id SET DEFAULT nextval('public.rbf_claims_id_seq'::regclass);

-- Name: rbf_device_claims id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_device_claims ALTER COLUMN id SET DEFAULT nextval('public.rbf_device_claims_id_seq'::regclass);

-- Name: rbf_organization_configs id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_organization_configs ALTER COLUMN id SET DEFAULT nextval('public.rbf_organization_configs_id_seq'::regclass);

-- Name: rbf_product_prices id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_product_prices ALTER COLUMN id SET DEFAULT nextval('public.rbf_product_prices_id_seq'::regclass);

-- Name: rbf_products id; Type: DEFAULT

ALTER TABLE ONLY public.rbf_products ALTER COLUMN id SET DEFAULT nextval('public.rbf_products_id_seq'::regclass);

-- Name: sessions id; Type: DEFAULT

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);

-- Name: shared_keys_vaults id; Type: DEFAULT

ALTER TABLE ONLY public.shared_keys_vaults ALTER COLUMN id SET DEFAULT nextval('public.shared_keys_vaults_id_seq'::regclass);

-- Name: sources id; Type: DEFAULT

ALTER TABLE ONLY public.sources ALTER COLUMN id SET DEFAULT nextval('public.sources_id_seq'::regclass);

-- Name: users id; Type: DEFAULT

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);

-- Name: visibilities id; Type: DEFAULT

ALTER TABLE ONLY public.visibilities ALTER COLUMN id SET DEFAULT nextval('public.visibilities_id_seq'::regclass);

-- Name: active_storage_attachments active_storage_attachments_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT active_storage_attachments_pkey PRIMARY KEY (id);

-- Name: active_storage_blobs active_storage_blobs_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.active_storage_blobs
    ADD CONSTRAINT active_storage_blobs_pkey PRIMARY KEY (id);

-- Name: active_storage_variant_records active_storage_variant_records_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.active_storage_variant_records
    ADD CONSTRAINT active_storage_variant_records_pkey PRIMARY KEY (id);

-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);

-- Name: authentications authentications_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.authentications
    ADD CONSTRAINT authentications_pkey PRIMARY KEY (id);

-- Name: common_products common_products_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.common_products
    ADD CONSTRAINT common_products_pkey PRIMARY KEY (id);

-- Name: dynamic_forms dynamic_forms_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.dynamic_forms
    ADD CONSTRAINT dynamic_forms_pkey PRIMARY KEY (id);

-- Name: encrypted_blobs encrypted_blobs_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.encrypted_blobs
    ADD CONSTRAINT encrypted_blobs_pkey PRIMARY KEY (id);

-- Name: history history_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);

-- Name: imports imports_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (id);

-- Name: org_device_mappers org_device_mappers_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.org_device_mappers
    ADD CONSTRAINT org_device_mappers_pkey PRIMARY KEY (id);

-- Name: organization_group_members organization_group_members_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.organization_group_members
    ADD CONSTRAINT organization_group_members_pkey PRIMARY KEY (id);

-- Name: organization_groups organization_groups_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.organization_groups
    ADD CONSTRAINT organization_groups_pkey PRIMARY KEY (id);

-- Name: organizations organizations_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);

-- Name: organizations_users organizations_users_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.organizations_users
    ADD CONSTRAINT organizations_users_pkey PRIMARY KEY (id);

-- Name: data_custom pk_data_custom_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_custom
    ADD CONSTRAINT pk_data_custom_on_uid PRIMARY KEY (uid);

-- Name: data_grids pk_data_grids_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_grids
    ADD CONSTRAINT pk_data_grids_on_uid PRIMARY KEY (uid);

-- Name: data_grids_ts pk_data_grids_ts_on_grid_uid_and_metered_at; Type: CONSTRAINT

ALTER TABLE ONLY public.data_grids_ts
    ADD CONSTRAINT pk_data_grids_ts_on_grid_uid_and_metered_at PRIMARY KEY (grid_uid, metered_at);

-- Name: data_meter_events_ts pk_data_meter_events_ts_on_device_uid_and_start_at; Type: CONSTRAINT

ALTER TABLE ONLY public.data_meter_events_ts
    ADD CONSTRAINT pk_data_meter_events_ts_on_device_uid_and_start_at PRIMARY KEY (device_uid, start_at);

-- Name: data_meters pk_data_meters_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_meters
    ADD CONSTRAINT pk_data_meters_on_uid PRIMARY KEY (uid);

-- Name: data_meters_ts pk_data_meters_ts_on_device_uid_and_metered_at; Type: CONSTRAINT

ALTER TABLE ONLY public.data_meters_ts
    ADD CONSTRAINT pk_data_meters_ts_on_device_uid_and_metered_at PRIMARY KEY (device_uid, metered_at);

-- Name: data_paygo_accounts_snapshots_ts pk_data_paygo_accounts_snapshots_ts_on_account_uid_and_snapshot; Type: CONSTRAINT

ALTER TABLE ONLY public.data_paygo_accounts_snapshots_ts
    ADD CONSTRAINT pk_data_paygo_accounts_snapshots_ts_on_account_uid_and_snapshot PRIMARY KEY (account_uid, snapshot_date);

-- Name: data_payments_ts pk_data_payments_ts_on_uid_and_paid_at; Type: CONSTRAINT

ALTER TABLE ONLY public.data_payments_ts
    ADD CONSTRAINT pk_data_payments_ts_on_uid_and_paid_at PRIMARY KEY (uid, paid_at);

-- Name: data_shs pk_data_shs_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_shs
    ADD CONSTRAINT pk_data_shs_on_uid PRIMARY KEY (uid);

-- Name: data_shs_ts pk_data_shs_ts_on_device_uid_and_metered_at; Type: CONSTRAINT

ALTER TABLE ONLY public.data_shs_ts
    ADD CONSTRAINT pk_data_shs_ts_on_device_uid_and_metered_at PRIMARY KEY (device_uid, metered_at);

-- Name: data_test pk_data_test_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_test
    ADD CONSTRAINT pk_data_test_on_uid PRIMARY KEY (uid);

-- Name: data_trust_trace pk_data_trust_trace_on_uid; Type: CONSTRAINT

ALTER TABLE ONLY public.data_trust_trace
    ADD CONSTRAINT pk_data_trust_trace_on_uid PRIMARY KEY (uid);

-- Name: privileges privileges_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.privileges
    ADD CONSTRAINT privileges_pkey PRIMARY KEY (id);

-- Name: projects projects_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);

-- Name: rbf_claim_templates rbf_claim_templates_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_claim_templates
    ADD CONSTRAINT rbf_claim_templates_pkey PRIMARY KEY (id);

-- Name: rbf_claim_verifiers rbf_claim_verifiers_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_claim_verifiers
    ADD CONSTRAINT rbf_claim_verifiers_pkey PRIMARY KEY (id);

-- Name: rbf_claims rbf_claims_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_claims
    ADD CONSTRAINT rbf_claims_pkey PRIMARY KEY (id);

-- Name: rbf_device_claims rbf_device_claims_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_device_claims
    ADD CONSTRAINT rbf_device_claims_pkey PRIMARY KEY (id);

-- Name: rbf_organization_configs rbf_organization_configs_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_organization_configs
    ADD CONSTRAINT rbf_organization_configs_pkey PRIMARY KEY (id);

-- Name: rbf_product_prices rbf_product_prices_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_product_prices
    ADD CONSTRAINT rbf_product_prices_pkey PRIMARY KEY (id);

-- Name: rbf_products rbf_products_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.rbf_products
    ADD CONSTRAINT rbf_products_pkey PRIMARY KEY (id);

-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);

-- Name: sessions sessions_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);

-- Name: shared_keys_vaults shared_keys_vaults_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.shared_keys_vaults
    ADD CONSTRAINT shared_keys_vaults_pkey PRIMARY KEY (id);

-- Name: sources sources_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (id);

-- Name: users users_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

-- Name: visibilities visibilities_pkey; Type: CONSTRAINT

ALTER TABLE ONLY public.visibilities
    ADD CONSTRAINT visibilities_pkey PRIMARY KEY (id);

-- Name: data_custom before_update_checks_custom; Type: TRIGGER

CREATE TRIGGER before_update_checks_custom BEFORE UPDATE ON public.data_custom FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_grids before_update_checks_grids; Type: TRIGGER

CREATE TRIGGER before_update_checks_grids BEFORE UPDATE ON public.data_grids FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_meters before_update_checks_meters; Type: TRIGGER

CREATE TRIGGER before_update_checks_meters BEFORE UPDATE ON public.data_meters FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_payments_ts before_update_checks_payments_ts; Type: TRIGGER

CREATE TRIGGER before_update_checks_payments_ts BEFORE UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_shs before_update_checks_shs; Type: TRIGGER

CREATE TRIGGER before_update_checks_shs BEFORE UPDATE ON public.data_shs FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_test before_update_checks_test; Type: TRIGGER

CREATE TRIGGER before_update_checks_test BEFORE UPDATE ON public.data_test FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_trust_trace before_update_checks_trust_trace; Type: TRIGGER

CREATE TRIGGER before_update_checks_trust_trace BEFORE UPDATE ON public.data_trust_trace FOR EACH ROW EXECUTE FUNCTION public.update_checks_and_deduplication();

-- Name: data_custom track_history_custom; Type: TRIGGER

CREATE TRIGGER track_history_custom AFTER DELETE OR UPDATE ON public.data_custom FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_grids track_history_grids; Type: TRIGGER

CREATE TRIGGER track_history_grids AFTER DELETE OR UPDATE ON public.data_grids FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_meters track_history_meters; Type: TRIGGER

CREATE TRIGGER track_history_meters AFTER DELETE OR UPDATE ON public.data_meters FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_payments_ts track_history_payments_ts; Type: TRIGGER

CREATE TRIGGER track_history_payments_ts AFTER DELETE OR UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_shs track_history_shs; Type: TRIGGER

CREATE TRIGGER track_history_shs AFTER DELETE OR UPDATE ON public.data_shs FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_test track_history_test; Type: TRIGGER

CREATE TRIGGER track_history_test AFTER DELETE OR UPDATE ON public.data_test FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: data_trust_trace track_history_trust_trace; Type: TRIGGER

CREATE TRIGGER track_history_trust_trace AFTER DELETE OR UPDATE ON public.data_trust_trace FOR EACH ROW EXECUTE FUNCTION public.track_changes_in_history();

-- Name: shared_keys_vaults fk_rails_743cf39962; Type: FK CONSTRAINT

ALTER TABLE ONLY public.shared_keys_vaults
    ADD CONSTRAINT fk_rails_743cf39962 FOREIGN KEY (organization_id) REFERENCES public.organizations(id);

-- Name: active_storage_variant_records fk_rails_993965df05; Type: FK CONSTRAINT

ALTER TABLE ONLY public.active_storage_variant_records
    ADD CONSTRAINT fk_rails_993965df05 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);

-- Name: shared_keys_vaults fk_rails_c18b8a8ce7; Type: FK CONSTRAINT

ALTER TABLE ONLY public.shared_keys_vaults
    ADD CONSTRAINT fk_rails_c18b8a8ce7 FOREIGN KEY (shared_with_organization_id) REFERENCES public.organizations(id);

-- Name: active_storage_attachments fk_rails_c3b3935057; Type: FK CONSTRAINT

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT fk_rails_c3b3935057 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);

-- Name: shared_keys_vaults fk_rails_f0c550fb16; Type: FK CONSTRAINT

ALTER TABLE ONLY public.shared_keys_vaults
    ADD CONSTRAINT fk_rails_f0c550fb16 FOREIGN KEY (user_id) REFERENCES public.users(id);

-- PostgreSQL database dump complete

SET search_path TO "$user", public;

-- Creating Hypertables (after we set the correct search_path)

SELECT create_hypertable('data_grids_ts', 'metered_at', chunk_time_interval => INTERVAL '30 days');

SELECT create_hypertable('data_meter_events_ts', 'start_at', chunk_time_interval => INTERVAL '30 days');

SELECT create_hypertable('data_meters_ts', 'metered_at', chunk_time_interval => INTERVAL '30 days');

SELECT create_hypertable('data_paygo_accounts_snapshots_ts', 'snapshot_date', chunk_time_interval => INTERVAL '30 days');

SELECT create_hypertable('data_payments_ts', 'paid_at', chunk_time_interval => INTERVAL '30 days');

SELECT create_hypertable('data_shs_ts', 'metered_at', chunk_time_interval => INTERVAL '30 days');

INSERT INTO "schema_migrations" (version) VALUES
 ('20220912141756')
,('20220912141757')
,('20220919142511')
,('20220919142615')
,('20220919142622')
,('20220928105156')
,('20220928105350')
,('20221004092514')
,('20221006071940')
,('20221102154659')
,('20221102154940')
,('20221102162108')
,('20221102170123')
,('20221122025345')
,('20221123105613')
,('20221123204213')
,('20221123205123')
,('20221124081738')
,('20221129011414')
,('20221205111748')
,('20221205121315')
,('20221206100202')
,('20221212141555')
,('20230102112733')
,('20230105155550')
,('20230116143801')
,('20230118113051')
,('20230118132407')
,('20230118161054')
,('20230119133009')
,('20230123101919')
,('20230126152408')
,('20230131142320')
,('20230206033314')
,('20230206165259')
,('20230214134442')
,('20230215102810')
,('20230315075000')
,('20230315110308')
,('20230321094632')
,('20230406093004')
,('20230413090457')
,('20230414141554')
,('20230414142226')
,('20230417131731')
,('20230418092749')
,('20230419070559')
,('20230419073359')
,('20230425095536')
,('20230425193406')
,('20230425194629')
,('20230426090318')
,('20230426095348')
,('20230503044910')
,('20230504051457')
,('20230504093206')
,('20230504101838')
,('20230504102605')
,('20230504114956')
,('20230504115706')
,('20230504132318')
,('20230509073956')
,('20230510055955')
,('20230511074024')
,('20230511155025')
,('20230512081928')
,('20230512111711')
,('20230512154029')
,('20230523094800')
,('20230523121610')
,('20230601084605')
,('20230608142126')
,('20230613133404')
,('20230712100359')
,('20230717102818')
,('20230717131032')
,('20230717133933')
,('20230717134334')
,('20230718133023')
,('20230725102545')
,('20230725102907')
,('20230802154816')
,('20230808113309')
,('20230823140327')
,('20230824105112')
,('20230830115321')
,('20230831091219')
,('20230831093736')
,('20230905103555')
,('20230906072338')
,('20230913090825')
,('20230913140940')
,('20230921075440')
,('20231006144604')
,('20231010215629')
,('20231010231604')
,('20231023131304')
,('20231026154633')
,('20231027112700')
,('20231031121935')
,('20231107115014')
,('20231108123912')
,('20231109093232')
,('20231113105435')
,('20231114083236')
,('20231205110649')
,('20231207114106')
,('20231212105053')
,('20231220162056')
,('20231220202344')
,('20231221095935')
,('20240102171838')
,('20240109114802')
,('20240209081451')
,('20240213092631')
,('20240213122135')
,('20240213123857')
,('20240222084059')
,('20240222170921')
,('20240227091422')
,('20240314165230')
,('20240314221617')
,('20240318182507')
,('20240328150907')
,('20240331084400')
,('20240402151542')
,('20240411083707')
,('20240423161917')
,('20240425085211')
,('20240430115623')
,('20240502112402')
,('20240503213729')
,('20240507095614')
,('20240507103419')
,('20240519172659')
,('20240530150109')
,('20240531083444')
,('20240628102039')
,('20240702103654')
,('20240808081519')
,('20240815134812')
,('20240903080730')
,('20240923123547')
,('20240926090447')
,('20241002103858')
,('20241018093750')
,('20241019092304')
,('20241024154833')
,('20241113133055')
,('20241211135720')
;

