# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# default organization and user

Grafana::Client.new # make sure we raise if we can not yet connect to grafana.

pw = ENV.fetch("PROSPECT_ADMIN_PASSWORD")
email = ENV.fetch("PROSPECT_ADMIN_USER")
u = User.create! username: "default",
                 email:,
                 admin: true,
                 password: pw,
                 active_at: Time.zone.now,
                 organization: Organization.find_or_create_by!(name: "Default Organization")

puts ">>> Created User with email: #{email} and password #{pw}" # rubocop:disable Rails/Output

################### RBF SETUP ###############################################################################
##
##

managing_org = Organization.find_or_create_by! name: "UECCC"
super_org = Organization.find_or_create_by! name: "Worldbank"
esco_org = Organization.find_or_create_by! name: "ESCO Company"

if Rails.env.development?
  # rubocop:disable Layout/LineLength

  # ueccc decryption password: thisisueccc
  managing_org.update! iv: "sD016X5XUJBR1pakyYh3Vg==", salt: "ZkTn3jJd7/979xSuIAUe2A==",
                       wrapped_key: "uPoSuFdRuY0G3TK1I4VKpnUY+pBHvh1tEVMej00Eu+zSR/akqm6okA5yqxIkuYhEZSJVLtmYIv3v1aw96ZIDM4wp/ngYvCBFChM8naMU1a9a32cuGHR+dGvioIXNPjL22qqjzSKzcAHz/G4amiSjvSvZLLDj08yPaIMrCgQYM4x5YkhgGdtYKYrqcLcOHySHyk5wgIuLuYhP8LD6C3JRMD9bbFMnHHyeXrSvRZrCvhJMRj4UDs73AGJzmeqctcbvfYqnKZHfysqOXt6b6PykYbXvxOhYDom4oLWJ3upY/y2uADL93y4YRMq9X0zMwNzBQCYbVTKvgS52D2rsiMou2IFkQ7r+64131DTOVFSQTnR8PEQMpodl8hrcZBT1Oj5szLbeIzKFHV+RR45E/xG7wNNljUDGmvpNOGvRMK+vz+/X7YTg3v5iDTv3TYKZdWQuz+c8WeZ5TbEy17rFbLjEt+efSLKpHfbB9x/YePcHB/fUGtNWX5DpQ26GepR0Y5kxf1JlmF5oz8PTn6lMEPvO0UFtVrIkmuQw083z4MAFRg9PkroWCYgNoXHiKInjwH1jNPlpiL8dBfiBIcDmaE1Pkc5RjBGhFYdWkG1iMFDGz3RALoLy3y6eS26O65VwOM+aSXRhMttYCex5jrkUsJ/srECGfmtv2C/Mg6QBwIZU0sogACD14w4mb3L8tajpal22DWVcOvX1/ZtMcWlB8HfEheUkfsdT3uvMq7rRKrsgKfkx6Hf5S6mDZBp27xhUxqvi2m0KEn/T9no0cYMzGH9cY0vJr0l7DzExuaLZfIrxYSsCztERtLRO/Ujev35fqETxRplO3pfA/GKI94ONUGspnibIkYGrhDWMbI69lwidBMGgr5Ydd7boodfeWfkMxtK1yUmPWMfZWIQASO1inVwCGa2KQZ2O+qhld+l1qhwa0fYN0jXyjeCvhiymJ0C68rffpwdA4XcBNo1MKc+zrxMux8E6X7i0oFRz839fj7SR6l5/hbx+jdX//8TlOJeGesxTKohSC7DK7oSi5XF+7vgIiBvGb5I7HIiLHSbbbTOlYmg7TU8h1qpOph1BzjlQyd5pUxf0Mhsx7sZUmsKXQz64grTI0qDo/nIHNsvE92vui6rYzfsYnhwOaF1d8rkrrZegK4xoQhs03qcV7oTZ/gvS7zmKRzOGrjj5EgHyuCGCcke7759CUZ7/bs++kA4TDM9k5Z88L7IX6+IUDVIR52JPS9qXBZwh54jCSMGxwgzVhceR0znBt1WB8PEprH4G6Wcnb7Gq68EalFAZf47PEtLhifcEu/bTaSfRRqNqb5ZME7BOBQUaeHTBMou46K4fiiOlrLXrHK699Jt4IzAuuz5+PALCeUYb30lkCRwyuBB9CNfRWvvjLAiPkzptanS90EZeCs4KwyrVUW51UCQIrkSilDueWE8jeBC4ccnwU2FqArOxQxrsjKEeGFYvJvf/DwAe8Ynw4LL4c/EZ5+kR8GSKrvSHiOxIDiSYoTIZ7nBkmpVH9YeqM4cknBSZLlieiFSCuOYrKYadosEQIobOugF4X/aLEd8n/m+E7ALMwGsIwVh+wifv+aeY2Hr+w7N9IplNGRwHLKH6q1hJfj1/b31AnmWViTCVMNW2ttbjCz6JEHI=",
                       public_key: "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArm+svgRew5jU/2cdi9x22eLx3t6YJ1Qh5XTMyA+ObTdT8JwHHXXOR5KM//WI1rlprKb9CrIVSftVo0rUJSBaf5Qrg6cO+sZDQ7wJ2qiNROEQCg/pr8UqiEjue5wuhgHuQ0kZq+bPiiaKzfeDdOphOmAmpPsP3dmQ6ymhLCAvAIMjVhcvaHROtltb1LzsHcbbUZZwqiaxgsrUsKWtyFpmQEEO8i1feLfn8wJou0i/d2f8mUi/YGIeCxCB+4bqSqaP2OBMeORQbzhYvtXiisWmtgGbCXw/b4UySuk/9D7TtTgyXup4gJC2V67xxmQeK6lvLF2JVODzcbxCVN0ZNTocrQIDAQAB"

  # esco decryption password: thisisanesco
  esco_org.update! iv: "ozts8AKBdDCXLfRisZ2Y9g==", salt: "2X78OFyD7VuqxOUlmfroGA==",
                   wrapped_key: "GJBz661YqF5JTDSB8JJi7BFvaavkWIwRujqfcg68uMC0AJrUzf3dsTj6yhMSfpCM553h0Fq2kOyut++WSN4KDHowHPv3hr0/+67VSm1eRlf0wvjzkJyRUHkpr6d0kbIooHXSinV9VOkQk+z8T1g3K6V9Lz2XXmBnDbV0IGBAyA8uS3MSprvPaIKaNcgk2JlcQQRzzIYwhtVFxQMmhaKe9iuRDdsVxhNgdokD/NTviY3k3tBZV5ZGAmUqs7qZVYiHkRRmk0lPQ/91pGeVeKh8qOJlNSgMOTdK6QFvPlrVCbeKNyYy6qTcjaTxMfQ8wNjmvTS+1tqzPJlhtrmcCI8UTYdEICzuiL9gE3bntwCbE8/CHv2CMRMPaiiwgezIARGeCn55YA2NFoIgSianPDOEc+VL9aXvlO70eUEX2OmobargtUoXvZ6nMZkzRSzAdBEzSR0V3bp+G+zJkYw/PA4QgKK3RYTNUT1tEVUCcyPiwksFDGg5Zcmi1rYlO84sKptwp7nna108PIIZQRpqmxcKcdlJ/n/YDF7jCfaesseCRWHas9JaFBXXh0HYvmADF8yB1aR3e7nBGlE9lGzRC78FeJAi5zuvJxOaNLEKSWK5Q8mr2aETISD/ieXlV3Ol9TaHYBnHogCbz0QVJm0/SYElk5BMV6HsEtbSxS1TaoKe03WmuifMxOwkNBw+JnpkFDnKCFfFEtmii237+rRCh+k3NMabYsgphT4cZs+Hhms6LGGF87I8uLp35HWgWwXQAtPAnnT60tIphaoRCj5U6V4fWopOyW5ADoPKur/KXdsHZweBqQTSI/G+T3GZ3Wrfz94EfkpDI8Ma1JecgmL8G6iuoKXardPhzNc5dAKdzqgW6i3q84jDvXKczx95v2bTf5PyrIOBx2xTRbNuyN1p2zdlwRFgvbH6td54TmYKNtcGJ4DC1zkyPTvT7T7YNyscsOXh+vIiwGUOqscuoTc+mGknFZcCJvQA3g6izben19mkJUttFbm0OqIW2pC2WOZ2QPOomJMpxbB/f7AgrP3VC0IyMZz6VL3gp3i+4YZOB8q53McMWtnxF6rTnKedb7zyCK0PdcH8ZrP0wo1snl38zRGB90qU4/WLKX48ltSAfEDCFPiM4VproopzaR56mdJuCS2QXGq+z2BIrLJrm+lFAqw5HKGa9rRfuWRKcN+pu+v8Kuo7jvBIKEOxBG6ycGJzlXVhMo+pAIlQonuiVtlI3iAlS2sikp+F52NXEnyW4bAXuFAWhXH+VNI5NBd2DLrV3uLr3BoQUaYGPU2VQqXo9MzUaHGeP59eRzWueGU1jyCpVVGwqMLZ3FcCVqMFYMKSOqbMjQFbfNZXG2jjsG8Ke+w77q0hcPz200gP6gq3vNEw4DBvW+n5tkl+rNfuAS8zK4yIsAaDMSis2gc+oZ/EkEdpQS/F4OZwaXx8YQAAB20OWS06rHUBPdgZS9fK5Lq3aOOqu2dy47v19Ig9Tsr5ybVq7Bo+vQJj9dNUpv0dtjwS9jW/9RVU5m/xCmGKqLdBVfW4IjnfboZN2NECy8dfZfLR9fjByazUvjdWw4cp9eU8NNdFBHr5U7PT/ESBeI04dRffyOiGsEm2OD0v397DE6pw7PoYKzLBFMCmboWBSKb02us=",
                   public_key: "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwCW4lf7obKnGEcTgA+Q9vAf6yI/SmEHv7CURH5HuJrwERKPeHbYUSydw4NWxc6anuy0s/2RH5ESZeD05YaK29mylaSah+4hCPd/3s95RhRHrOGien8egUbffSJWMJVTsbnDTqCvJ5s8nBTHvLnQplWSLLQp4BaFc6G2oBoUIBiQv/uxWm9WEqfsRtCX7l7wUiLecy6B1Vf/b13wqwa7KlYb5okiGRVpeX77malsPCSboTDAGLzKGGhrsM0SQ6FHakVC8z6I1TmHXqUv/lkLZPBoIq8sLT+frbZ9zCAPZKHTr7Se/9Fkwimj/xB6wWvNzR3kt9zFjRWrxDtwgEHmDCwIDAQAB"

  # rubocop:enable Layout/LineLength
end

org_group = OrganizationGroup.find_or_create_by! name: "RBF UG OGS Group"
org_group_ccs = OrganizationGroup.find_or_create_by! name: "RBF UG CCS Group"
org_group_pue = OrganizationGroup.find_or_create_by! name: "RBF UG PUE Group"
[managing_org, super_org, esco_org].each do |org|
  OrganizationGroupMember.find_or_create_by! organization: org, organization_group: org_group, visible_for_sharing: (org.name == "UECCC")
  OrganizationGroupMember.find_or_create_by! organization: org, organization_group: org_group_ccs, visible_for_sharing: (org.name == "UECCC")
  OrganizationGroupMember.find_or_create_by! organization: org, organization_group: org_group_pue, visible_for_sharing: (org.name == "UECCC")
  ou = OrganizationUser.find_or_create_by! user: u, organization: org
  ou.activate!
end

RbfClaimTemplate.find_or_create_by! organization_group: org_group,
                                    supervising_organization: super_org,
                                    managing_organization: managing_org,
                                    verifying_organization: managing_org,
                                    name: "UG EASP OGS",
                                    currency: "UGX",
                                    trust_trace_check: "rbf_easp_ogs_is_eligible",
                                    user_id: u.id

x = [
  {
    organization_group: OrganizationGroup.find_by!(name: "RBF UG CCS Group"),
    supervising_organization: Organization.find_by!(name: "UECCC"),
    managing_organization: Organization.find_by!(name: "UECCC"),
    verifying_organization: Organization.find_by!(name: "UECCC"),
    currency: "UGX",
    trust_trace_check: "rbf_easp_ccs_is_eligible",
    user_id: u.id,
    name: "UG EASP CCS"
  },
  {
    organization_group: OrganizationGroup.find_by!(name: "RBF UG PUE Group"),
    supervising_organization: Organization.find_by!(name: "UECCC"),
    managing_organization: Organization.find_by!(name: "UECCC"),
    verifying_organization: Organization.find_by!(name: "UECCC"),
    currency: "UGX",
    trust_trace_check: "rbf_easp_pue_is_eligible",
    user_id: u.id,
    name: "UG EASP PUE"
  }
]
x.each do |h|
  RbfClaimTemplate.create!(**h)
end

rbf_products = []

rbf_products << RbfProduct.find_or_create_by!(
  organization: esco_org,
  rbf_claim_template: RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_ogs_is_eligible"),
  manufacturer: "Manuf",
  model: "Solarlight 5000",
  rbf_category: "A"
)

rbf_products << RbfProduct.find_or_create_by!(
  organization: esco_org,
  rbf_claim_template: RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_ccs_is_eligible"),
  manufacturer: "Cookstove Inc",
  model: "Superburner 3000 LPG",
  rbf_category: "C"
)

rbf_products << RbfProduct.find_or_create_by!(
  organization: esco_org,
  rbf_claim_template: RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_ccs_is_eligible"),
  manufacturer: "Cookstove Inc",
  model: "Superburner 5000 Eth",
  rbf_category: "D"
)

rbf_products << RbfProduct.find_or_create_by!(
  organization: esco_org,
  rbf_claim_template: RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_pue_is_eligible"),
  manufacturer: "Waterworld",
  model: "Pump 3",
  rbf_category: "Ai"
)

rbf_products << RbfProduct.find_or_create_by!(
  organization: esco_org,
  rbf_claim_template: RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_pue_is_eligible"),
  manufacturer: "Coolfreeze",
  model: "SunFridge 80L",
  rbf_category: "B"
)

rbf_products.each do |p|
  RbfProductPrice.find_or_create_by! rbf_product: p, category: :cash, valid_from: 6.months.ago,
                                     base_subsidized_sales_price: 100_000, base_subsidy_amount: 60_000,
                                     original_sales_price: (100_000 + 60_000)
  RbfProductPrice.find_or_create_by! rbf_product: p, category: :cash, valid_from: 2.weeks.ago,
                                     base_subsidized_sales_price: 150_000, base_subsidy_amount: 60_000,
                                     original_sales_price: (150_000 + 60_000)
  RbfProductPrice.find_or_create_by! rbf_product: p, category: :paygo, valid_from: 6.months.ago,
                                     base_subsidized_sales_price: 200_000, base_subsidy_amount: 60_000,
                                     original_sales_price: (200_000 + 60_000)
  RbfProductPrice.find_or_create_by! rbf_product: p, category: :paygo, valid_from: 2.weeks.ago,
                                     base_subsidized_sales_price: 230_000, base_subsidy_amount: 60_000,
                                     original_sales_price: (230_000 + 60_000)
end

# project = Project.find_or_create_by! name: "Rbf Ogs Project", organization: esco_org
# source = Source.find_or_create_by! project:, kind: "manual_input/ueccc",
#                                    data_category: "shs", name: "Ogs Form Data"
# source.update_column :activated_at, Time.zone.now
#
# imp = if source.imports.any?
#         source.imports.last
#       else
#         Import.create! source:,
#                        ingestion_finished_at: Time.zone.now,
#                        ingestion_started_at: Time.zone.now,
#                        processing_finished_at: Time.zone.now,
#                        processing_started_at: Time.zone.now,
#                        protocol: "all good :)",
#                        rows_inserted: 50
#       end
#
# def raw_db_insert(hash = nil, **args)
#   data = hash || args
#   raise "need some data for insertion" if data.empty?
#
#   qs = data.map do |table, rows|
#     columns = rows.map(&:keys).flatten.uniq
#
#     values = rows.map do |row|
#       columns.map do |c|
#         v = row[c]
#         if v.nil?
#           "NULL"
#         elsif [String, Time].any? { v.is_a? _1 }
#           sql_quote v.to_s
#         elsif v.is_a?(Hash) && v.key?(:raw)
#           v[:raw]
#         else
#           v
#         end
#       end
#     end
#
#     # make sure they are always column names!
#     columns.map! { "\"#{_1}\"" }
#
#     <<~SQL.squish
#       INSERT INTO #{table}#{' '}
#         (#{columns.join(', ')})
#       VALUES
#         #{values.map { "(#{_1.join(', ')})" }.join(', ')}
#     SQL
#   end
#   qs.each { ActiveRecord::Base.connection.execute _1 }
# end
#
# def sql_quote(val)
#   quoted = ActiveRecord::Base.connection.quote_string val.to_s
#   "'#{quoted}'"
# end
#
# shs = {
#   import_id: imp.id,
#   source_id: imp.source.id,
#   organization_id: imp.organization.id,
#   data_origin: "manual_input",
#   manufacturer: "Manuf",
#   model: "Solarlight 5000",
#   purchase_date: (Time.zone.today - 1.week).to_s,
#   created_at: Time.zone.now.to_s,
#   updated_at: Time.zone.now.to_s
# }.transform_keys(&:to_s)
#
# shs_full = Postgres::DataTable.new("shs").examples.merge(shs)
# exclude_cols = Postgres::DataTable.new("shs").columns.select { _1.sql_columns.count > 1 }.map(&:name)
# shss = 55.times.map do |n|
#   ids  = {
#     serial_number: "SER1000#{n + 1}",
#     device_uid: "#{imp.organization.id}_Manuf_SER1000#{n + 1}",
#     customer_external_id: "CUS1000#{n + 1}",
#     customer_uid: "#{imp.organization.id}_Manuf_CUS1000#{n + 1}",
#     account_external_id: "ACC1000#{n + 1}",
#     account_uid: "#{imp.organization.id}_Manuf_CUS1000#{n + 1}",
#     uid: "#{imp.organization.id}_Manuf_SER1000#{n + 1}_#{imp.organization.id}_Manuf_CUS1000#{n + 1}",
#     customer_country: "UG"
#   }.transform_keys(&:to_s)
#
#   shs_full.merge(ids).without(*exclude_cols)
# end
#
# raw_db_insert data_shs: shss
#
# bad_tt =
#   {
#     uid: "shs_1234_over_there_795_1234_company_ATS_6548_rbf_easp_ogs_is_eligible",
#     subject_uid: "1234_over_there_795_1234_company_ATS_6548",
#     subject_origin: "shs",
#     check: "rbf_easp_ogs_is_eligible",
#     organization_id: 1234,
#     source_id: 123_456_789,
#     data_origin: "over_there",
#     import_id: 123_456_789,
#     result: "False",
#     custom: {
#       subcheck__customer_data_is_complete: "False",
#       subcheck__customer_data_is_unique_within_past_two_years: "True",
#       subcheck__payment_plan_is_complete: "True",
#       subcheck__product_is_on_white_list: "True",
#       subcheck__sale_is_not_older_than_three_months: "False",
#       subcheck__device_is_repossessed_at_most_once: "True"
#     }.to_json
#   }.transform_keys(&:to_s)
#
# good_tt =
#   {
#     uid: "shs_1234_over_there_795_1234_company_ATS_6548_rbf_easp_ogs_is_eligible",
#     subject_uid: "1234_over_there_795_1234_company_ATS_6548",
#     subject_origin: "shs",
#     check: "rbf_easp_ogs_is_eligible",
#     organization_id: 1234,
#     source_id: 123_456_789,
#     data_origin: "over_there",
#     import_id: 123_456_789,
#     result: "True",
#     custom: {
#       subcheck__customer_data_is_complete: "True",
#       subcheck__customer_data_is_unique_within_past_two_years: "True",
#       subcheck__payment_plan_is_complete: "True",
#       subcheck__product_is_on_white_list: "True",
#       subcheck__sale_is_not_older_than_three_months: "True",
#       subcheck__device_is_repossessed_at_most_once: "True",
#       subsidy__amount: 100_000,
#       subsidy__category: "Ai",
#       incentive__amount: 15_000,
#       incentive__type: "Central/Islands"
#     }.to_json
#   }.transform_keys(&:to_s)
#
# dtt = 55.times.map do |n|
#   x = {
#     subject_uid: "#{imp.organization.id}_Manuf_SER1000#{n + 1}_#{imp.organization.id}_Manuf_CUS1000#{n + 1}",
#     uid: "shs_#{imp.organization.id}_Manuf_SER1000#{n + 1}_#{imp.organization.id}_Manuf_CUS1000#{n + 1}_rbf_easp_ogs_is_eligible",
#     import_id: imp.id,
#     source_id: imp.source.id,
#     organization_id: imp.organization.id,
#     created_at: Time.zone.now.to_s,
#     updated_at: Time.zone.now.to_s
#   }.transform_keys(&:to_s)
#   if n < 40
#     good_tt.merge x
#   else
#     bad_tt.merge x
#   end
# end

# raw_db_insert data_trust_trace: dtt

Visibility.full_views_refresh!

Ueccc::FormGenerator.run!

# po = Organization.create! name: "Test Parent"
# t = Time.zone.now
# cols = Postgres::DataTable.all.map{ [_1.table_name, _1.sql_columns]}.to_h
# 200.times do |n|
#   puts "#{n} #{Time.zone.now - t}"
#   t = Time.zone.now
#
#   o = Organization.create! name: "Test Child #{n}"
#   p = Project.create! name: "Test Project #{n}"
#   s = Source.create! name: "Test Source #{n}", project: p, kind: "manual_upload",
#                      data_category: "shs", activated_at: Time.zone.now
#   v = Visibility.create! project: p, organization: po, columms: cols
# end
#
