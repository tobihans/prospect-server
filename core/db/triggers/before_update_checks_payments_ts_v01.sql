CREATE OR REPLACE TRIGGER before_update_checks_payments_ts
    BEFORE UPDATE ON data_payments_ts
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();