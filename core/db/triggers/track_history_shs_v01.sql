CREATE OR REPLACE TRIGGER track_history_shs
    AFTER UPDATE OR DELETE ON data_shs
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();