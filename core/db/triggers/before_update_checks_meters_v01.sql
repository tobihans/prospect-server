CREATE OR REPLACE TRIGGER before_update_checks_meters
    BEFORE UPDATE ON data_meters
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();