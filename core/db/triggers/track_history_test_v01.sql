CREATE OR REPLACE TRIGGER track_history_test
    AFTER UPDATE OR DELETE ON data_test
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();