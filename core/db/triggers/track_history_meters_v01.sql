CREATE OR REPLACE TRIGGER track_history_meters
    AFTER UPDATE OR DELETE ON data_meters
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();