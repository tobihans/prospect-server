CREATE OR REPLACE TRIGGER before_update_checks_custom
    BEFORE UPDATE ON data_custom
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();