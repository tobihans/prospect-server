CREATE OR REPLACE TRIGGER track_history_grids
    AFTER UPDATE OR DELETE ON data_grids
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();