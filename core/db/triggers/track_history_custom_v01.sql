CREATE OR REPLACE TRIGGER track_history_custom
    AFTER UPDATE OR DELETE ON data_custom
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();