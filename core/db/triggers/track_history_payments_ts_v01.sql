CREATE OR REPLACE TRIGGER track_history_payments_ts
    AFTER UPDATE OR DELETE ON data_payments_ts
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();