CREATE OR REPLACE TRIGGER before_update_checks_grids
    BEFORE UPDATE ON data_grids
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();