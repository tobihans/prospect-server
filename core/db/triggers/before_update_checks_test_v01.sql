CREATE OR REPLACE TRIGGER before_update_checks_test
    BEFORE UPDATE ON data_test
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();