CREATE OR REPLACE TRIGGER track_history_trust_trace
    AFTER UPDATE OR DELETE ON data_trust_trace
    FOR EACH ROW
EXECUTE FUNCTION track_changes_in_history();