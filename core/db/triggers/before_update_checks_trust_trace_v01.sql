CREATE OR REPLACE TRIGGER before_update_checks_trust_trace
    BEFORE UPDATE ON data_trust_trace
    FOR EACH ROW
EXECUTE FUNCTION update_checks_and_deduplication();