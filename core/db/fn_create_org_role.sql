CREATE OR REPLACE FUNCTION fn_create_org_role(p_name text, p_password text)
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER
AS $$
DECLARE
    role_infix varchar := '_prospect_organization_';
    role_start varchar := current_database() || role_infix;
    valid_role boolean := starts_with(p_name, role_start);
    valid_len  boolean := length(p_name) > length(role_start);
    valid_pw   boolean := length(p_password) > 10;
    valid_all  boolean := valid_role AND valid_len AND valid_pw;
BEGIN
    IF NOT valid_all THEN
        RAISE EXCEPTION SQLSTATE 'IORNP' USING HINT = 'Invalid organization role name or password';
    END IF;
    IF EXISTS(SELECT FROM pg_catalog.pg_roles WHERE rolname = p_name) THEN
        EXECUTE format('ALTER USER %I WITH PASSWORD %L;',p_name, p_password);
    ELSE
        EXECUTE format('CREATE ROLE %I WITH LOGIN PASSWORD %L;', p_name, p_password);
    END IF;

    EXECUTE format('GRANT CONNECT ON DATABASE %s TO %I',current_database(), p_name);
    EXECUTE format('CREATE SCHEMA IF NOT EXISTS %s', p_name);
    EXECUTE format('GRANT USAGE ON SCHEMA %s TO %I',p_name, p_name);
    EXECUTE format('GRANT SELECT ON ALL TABLES IN SCHEMA %s TO %I', p_name , p_name);
    EXECUTE format('ALTER DEFAULT PRIVILEGES IN SCHEMA %s GRANT SELECT ON TABLES TO %I', p_name , p_name);

    RETURN;
END $$;