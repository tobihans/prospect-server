# frozen_string_literal: true

class AddCryptokeysToOrganisation < ActiveRecord::Migration[7.0]
  def change
    change_table :organizations, bulk: true do |t|
      t.text :iv
      t.text :salt
      t.text :public_key
      t.text :wrapped_key
      t.timestamp :crypto_added_at
    end
  end
end
