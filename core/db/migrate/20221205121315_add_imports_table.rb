# frozen_string_literal: true

class AddImportsTable < ActiveRecord::Migration[7.0]
  def change
    create_table :imports do |t|
      t.references :source
      t.timestamp :ingestion_started_at
      t.timestamp :ingestion_finished_at
      t.timestamp :processing_started_at
      t.timestamp :processing_finished_at
      t.string :error
      t.timestamps
    end
  end
end
