# frozen_string_literal: true

class ChangeOrganizationRoleToAdminFlag < ActiveRecord::Migration[7.0]
  def up
    say "Converting: admin role to admin flag"
    execute <<~SQL.squish
      UPDATE users
      SET admin = true
      FROM organizations_users
      WHERE users.id = organizations_users.user_id
        AND organizations_users.role = 'admin'
    SQL

    say "Updating role: admin -> manager"
    execute <<~SQL.squish
      UPDATE organizations_users
      SET role = 'manager'
      WHERE role = 'admin'
    SQL
  end

  def down
    say "Converting: admin flag to admin role"
    execute <<~SQL.squish
      UPDATE organizations_users
      SET role = 'admin'
      FROM users
      WHERE users.id = organizations_users.user_id
        AND users.admin = true
    SQL

    say "Updating: all admin flags to false"
    execute <<~SQL.squish
      UPDATE users
      SET admin = false
    SQL
  end
end
