# frozen_string_literal: true

class ApiKeyDeterministic < ActiveRecord::Migration[7.0]
  def up
    say "switched from non-deterministic encryption to deterministic encryption. Need to rewrite the api_key attribute."

    Organization.find_each do |o|
      api_key = o.api_key
      o.api_key = "we need a change"
      o.save!
      o.api_key = api_key
      o.save!
    end
  end

  def down
    say "Can't go back to non-deterministic without code change"
  end
end
