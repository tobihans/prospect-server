# frozen_string_literal: true

# This model holds key informations about the dynamic forms logic

class CreateDynamicForms < ActiveRecord::Migration[7.0]
  def change
    create_table :dynamic_forms do |t|
      t.string :name
      t.json :form_data
      t.boolean :active, default: true, null: false
      t.string :description
      t.timestamps
    end
  end
end
