# frozen_string_literal: true

class AddSourcesTable < ActiveRecord::Migration[7.0]
  def change
    create_table :sources do |t|
      t.string :name, null: false
      t.integer :project_id, null: false
      t.string :kind, null: false
      t.string :secret, limit: 1000 # overhead for encryption!
      t.jsonb :details, null: false, default: "{}"
      t.timestamp :activated_at
      t.timestamps null: false
    end

    add_index :sources, :project_id
    add_index :sources, :kind
    add_index :sources, :activated_at
  end
end
