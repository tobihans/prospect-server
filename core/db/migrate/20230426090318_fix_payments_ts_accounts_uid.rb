# frozen_string_literal: true

class FixPaymentsTsAccountsUid < ActiveRecord::Migration[7.0]
  def change
    remove_check_constraint :data_payments_ts,
                            "num_nonnulls(shs_account_uid, meters_account_uid) = 1",
                            name: "xor_account_uids"

    create_enum :payments_ts_account_origin, %w[meters shs]

    Postgres::RemoteControl.drop_all_organization_views! :payments_ts

    reversible do |direction|
      direction.up do
        execute "TRUNCATE data_payments_ts"
      end
    end

    change_table :data_payments_ts, bulk: true do |t|
      t.remove :meters_account_uid, type: :string
      t.remove :shs_account_uid, type: :string

      t.string :account_external_id, null: false, index: true
      t.string :account_uid, null: false, index: true
      t.enum :account_origin, enum_type: :payments_ts_account_origin, null: false, index: true
    end
  end
end
