# frozen_string_literal: true

class AddDataCustomTable < ActiveRecord::Migration[7.0]
  def change
    create_table :data_custom do |t|
      t.string :uid, null: false
      t.string :external_id, null: false, index: true

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: true
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.float :trust_score
      t.string :data_origin, null: false
    end

    add_index :data_custom, :uid, unique: true
  end
end
