# frozen_string_literal: true

class AddSubsidyInfoToRbfPrices < ActiveRecord::Migration[7.1]
  def change
    change_table :rbf_product_prices, bulk: true do |t|
      t.rename :amount, :original_sales_price
      t.integer :subsidized_sales_price
      t.integer :subsidy_amount
    end

    reversible do |direction|
      direction.up do
        execute "UPDATE rbf_product_prices SET subsidized_sales_price = 0, subsidy_amount = 0"
      end
    end

    change_column_null :rbf_product_prices, :subsidized_sales_price, false
    change_column_null :rbf_product_prices, :subsidy_amount, false
  end
end
