# frozen_string_literal: true

class AddNewPurchaseColumnsToPaymentsTs < ActiveRecord::Migration[7.0]
  def change
    change_table :data_payments_ts, bulk: true do |t|
      t.string :purchase_item
      t.string :purchase_unit
      t.float :purchase_quantity
    end
  end
end
