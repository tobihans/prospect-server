# frozen_string_literal: true

class PopulateUserActiveAt < ActiveRecord::Migration[7.0]
  def change
    User.update_all active_at: Time.zone.now # rubocop:disable Rails/SkipsModelValidations
  end
end
