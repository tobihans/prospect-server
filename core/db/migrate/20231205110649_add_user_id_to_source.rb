# frozen_string_literal: true

class AddUserIdToSource < ActiveRecord::Migration[7.0]
  def change
    add_column :sources, :user_id, :integer
    add_index :sources, :user_id
  end
end
