# frozen_string_literal: true

class AddPaymentPlanToMeters < ActiveRecord::Migration[7.1]
  def change
    change_table :data_meters, bulk: true do |t|
      t.float :payment_plan_days
      t.float :payment_plan_down_payment
      t.float :payment_plan_amount_financed
      t.string :payment_plan_currency
      t.date :purchase_date
    end
  end
end
