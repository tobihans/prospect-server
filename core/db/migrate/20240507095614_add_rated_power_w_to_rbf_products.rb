# frozen_string_literal: true

class AddRatedPowerWToRbfProducts < ActiveRecord::Migration[7.1]
  def change
    add_column :rbf_products, :rated_power_w, :integer
  end
end
