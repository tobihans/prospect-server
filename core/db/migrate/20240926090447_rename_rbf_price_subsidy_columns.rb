# frozen_string_literal: true

class RenameRbfPriceSubsidyColumns < ActiveRecord::Migration[7.1]
  def change
    change_table :rbf_product_prices, bulk: true do |t|
      t.rename :subsidy_amount, :base_subsidy_amount
      t.rename :subsidized_sales_price, :base_subsidized_sales_price
    end
  end
end
