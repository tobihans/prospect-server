# frozen_string_literal: true

class AddNewEnergyColumnsInSHSTsAndMetersTs < ActiveRecord::Migration[7.0]
  def change
    change_table :data_meters_ts, bulk: true do |t|
      t.float :energy_interval_wh, null: true
      t.rename :energy_wh, :energy_lifetime_wh
    end

    change_table :data_shs_ts, bulk: true do |t|
      t.float :output_energy_interval_wh, null: true
      t.float :pv_energy_interval_wh, null: true
      t.float :grid_energy_interval_wh, null: true
      t.rename :output_energy_wh, :output_energy_lifetime_wh
      t.rename :pv_energy_wh, :pv_energy_lifetime_wh
      t.rename :grid_energy_wh, :grid_energy_lifetime_wh
    end
  end
end
