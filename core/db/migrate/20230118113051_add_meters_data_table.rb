# frozen_string_literal: true

class AddMetersDataTable < ActiveRecord::Migration[7.0]
  def change
    # rubocop:disable Naming/VariableNumber
    create_table :data_meters do |t|
      t.string :customer_external_id, null: false, index: true
      t.string :customer_name_p
      t.string :customer_category
      t.string :customer_sub_category
      t.enum :customer_gender, enum_type: :gender # created in previouse migration
      t.integer :customer_birth_year
      t.string :customer_former_electricity_source
      t.integer :customer_household_size
      t.string :customer_profession
      t.string :customer_address_p
      t.string :customer_country
      t.float :customer_latitude_b
      t.float :customer_longitude_b
      t.float :customer_latitude_p
      t.float :customer_longitude_p
      t.string :customer_email_p
      t.string :customer_phone_p
      t.string :customer_location_area_1
      t.string :customer_location_area_2
      t.string :customer_location_area_3
      t.string :customer_location_area_4

      t.string :account_external_id, null: false, index: true
      t.string :payment_plan

      t.string :device_external_id, null: false, index: true
      t.string :serial_number, null: false, index: true
      t.string :manufacturer, null: false
      t.string :model
      t.string :firmware_version
      t.float :max_power_w
      t.boolean :is_locked # rubocop:disable Rails/ThreeStateBooleanColumn
      t.boolean :is_test # rubocop:disable Rails/ThreeStateBooleanColumn

      t.date :installation_date
      t.date :repossession_date

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: true
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.float :trust_score
      t.string :data_origin, null: false

      t.string :uid, null: false, index: true, unique: true
      t.string :device_uid, null: false, index: true
      t.string :customer_uid, null: false, index: true
      t.string :account_uid, null: false, index: true
      t.string :grid_uid, index: true
    end
    # rubocop:enable Naming/VariableNumber
  end
end
