# frozen_string_literal: true

class NormalizeUserEmails < ActiveRecord::Migration[7.0]
  def up
    data = execute("SELECT id, email FROM users").values
    # [[80, "CAPITAL@email.com"], ...]

    data.each do |arr|
      arr << arr.last.to_s.strip.downcase
    end
    # [[80, "CAPITAL@email.com", "capital@email.com"], ...]

    data.reject! { |_id, orig, norm| orig == norm }

    say "Normalizing #{data.count} emails"
    data.each do |id, orig, norm|
      say "Updating #{orig} => #{norm}"

      norm_q = ActiveRecord::Base.connection.quote norm

      execute "UPDATE users SET email = #{norm_q} WHERE id = #{id}"
    end
  end

  def down
    say "No we will not mess up the emails again! Nothing to see here."
  end
end
