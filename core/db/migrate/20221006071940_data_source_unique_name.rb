# frozen_string_literal: true

class DataSourceUniqueName < ActiveRecord::Migration[7.0]
  def change
    remove_index :sources, :project_id
    add_index :sources, %i[project_id name], unique: true
  end
end
