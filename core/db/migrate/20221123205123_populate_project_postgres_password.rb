# frozen_string_literal: true

class PopulateProjectPostgresPassword < ActiveRecord::Migration[7.0]
  def up
    # not pure SQL I know, but its an encrypted attribute...
    Organization.table_name = "projects" # project was renamed to organization
    Organization.where(postgres_password: nil).find_each do |p|
      say "Generating Postgres password for Organization #{p.id}"

      p.generate_postgres_password
      p.save validate: false
    end
  end

  def down
    say "This is irreversible / just a data migration"
  end
end
