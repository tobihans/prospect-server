# frozen_string_literal: true

class AddLegalInfoToUser < ActiveRecord::Migration[7.0]
  def change
    change_table :users, bulk: true do |t|
      t.json :terms_of_use, default: {}
      t.json :data_protection_policy, default: {}
      t.json :code_of_conduct, default: {}
    end
  end
end
