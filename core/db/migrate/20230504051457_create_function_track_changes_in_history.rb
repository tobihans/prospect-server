# frozen_string_literal: true

class CreateFunctionTrackChangesInHistory < ActiveRecord::Migration[7.0]
  def change
    create_function :track_changes_in_history
  end
end
