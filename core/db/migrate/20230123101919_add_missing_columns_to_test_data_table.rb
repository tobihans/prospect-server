# frozen_string_literal: true

class AddMissingColumnsToTestDataTable < ActiveRecord::Migration[7.0]
  def change
    create_enum :enum_example, %w[a b c]

    change_table :data_test, bulk: true do |t|
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.string :data_origin, null: false
      t.float :trust_score
      t.enum :enum_column, enum_type: :enum_example
    end
    rename_column :data_test, :name_column, :name_column_p
  end
end
