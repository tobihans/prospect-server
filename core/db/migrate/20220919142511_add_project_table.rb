# frozen_string_literal: true

class AddProjectTable < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.string :api_key, limit: 1000 # overhead for encryption!
      t.timestamps null: false
    end
  end
end
