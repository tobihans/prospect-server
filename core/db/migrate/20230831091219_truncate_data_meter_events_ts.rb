# frozen_string_literal: true

class TruncateDataMeterEventsTs < ActiveRecord::Migration[7.0]
  def up
    truncate :data_meter_events_ts
  end
end
