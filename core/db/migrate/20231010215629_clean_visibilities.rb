# frozen_string_literal: true

class CleanVisibilities < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.drop_all_organization_views!

    reversible do |direction|
      direction.up do
        remove_weird_test_views!
      end
    end

    change_table :visibilities, bulk: true do |t|
      t.remove :access, type: :integer, index: true
      t.remove_index column: %i[organization_id source_id]
      t.remove :source_id, type: :index
      t.index :organization_id
    end
  end

  # REMOVE SOME VIEW ARTIFACTS FROM GOT KNOWS WHERE
  def remove_weird_test_views!
    execute "DROP VIEW IF EXISTS prospect_project_1.data_test CASCADE"
    execute "DROP VIEW IF EXISTS prospect_project_2.data_test CASCADE"
    execute "DROP VIEW IF EXISTS prospect_project_3.data_test CASCADE"
  end
end
