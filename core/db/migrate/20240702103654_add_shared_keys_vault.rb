# frozen_string_literal: true

class AddSharedKeysVault < ActiveRecord::Migration[7.1]
  def change
    create_table :shared_keys_vaults do |t|
      t.references :organization, null: false, index: false, foreign_key: { to_table: :organizations }
      t.references :shared_with_organization, null: false, index: false, foreign_key: { to_table: :organizations }
      t.string :encrypted_key, null: false
      t.references :user, index: true, foreign_key: { to_table: :users }
      t.index %i[organization_id shared_with_organization_id], unique: true

      t.timestamps
    end
  end
end
