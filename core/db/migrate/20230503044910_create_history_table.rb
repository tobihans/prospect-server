# frozen_string_literal: true

class CreateHistoryTable < ActiveRecord::Migration[7.0]
  def change
    create_table :history do |t|
      # basic structure
      t.string :data_table, index: true
      t.jsonb :old_row, null: false
      t.jsonb :new_row
      t.datetime :created_at, null: false

      # our data
      t.string :uid, index: true
      t.bigint :source_id, null: false, index: true
      t.bigint :import_id, null: false, index: true
      t.bigint :organization_id, null: false, index: true

      # currently no function, will be used if this entry has been rolled back
      t.datetime :reverted_at
    end
  end
end
