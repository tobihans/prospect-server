# frozen_string_literal: true

class DropAndUpdateSparkmeterPowerEnergy < ActiveRecord::Migration[7.0]
  def up
    #
    # - move energy_wh to custom.total_cycle_energy
    # - move power_w to energy_wh
    # - NULL out power_w
    #

    execute <<~SQL.squish
      UPDATE public.data_meters_ts
      SET custom = custom || jsonb_build_object('total_cycle_energy', energy_wh)
      WHERE data_origin = 'spark_meter';
    SQL

    execute <<~SQL.squish
      UPDATE public.data_meters_ts
      SET energy_wh = power_w
      WHERE data_origin = 'spark_meter';
    SQL

    execute <<~SQL.squish
      UPDATE public.data_meters_ts
      SET power_w = NULL
      WHERE data_origin = 'spark_meter';
    SQL
  end

  def down
    #
    # - move energy_wh back to power_w
    # - move custom.total_cycle_energy back to energy_wh
    #

    execute <<~SQL.squish
      UPDATE public.data_meters_ts
      SET power_w = energy_wh
      WHERE data_origin = 'spark_meter';
    SQL

    execute <<~SQL.squish
      UPDATE public.data_meters_ts
      SET energy_wh = CAST(custom ->> 'total_cycle_energy' AS double precision)
      WHERE data_origin = 'spark_meter';
    SQL
  end
end
