# frozen_string_literal: true

class CreateOrgDeviceMappers < ActiveRecord::Migration[7.0]
  def up
    create_table "org_device_mappers" do |t|
      t.references :organization
      t.bigint :device_id
      t.string :device_type
      t.timestamps
      t.index(%i[device_type device_id], unique: true)
      t.index(%i[device_type organization_id])
    end

    change_table :org_device_mappers, bulk: true do |t|
      t.change :device_type, :string, null: false
      t.change :organization_id, :bigint, null: false
      t.change :device_id, :bigint, null: false
    end
    execute("INSERT INTO org_device_mappers (device_id, organization_id, device_type, created_at, updated_at)
             SELECT meter_id, organization_id, 'hop_meter', NOW(), NOW() FROM dataworker_stellar_org_to_meter_map;")
    drop_table :dataworker_stellar_org_to_meter_map
  end

  def down
    remove_index(:org_device_mappers, %i[device_type organization_id])
    remove_index(:org_device_mappers, %i[device_type device_id])
    rename_table :org_device_mappers, :dataworker_stellar_org_to_meter_map
    rename_column :dataworker_stellar_org_to_meter_map, :device_id, :meter_id
    remove_column :dataworker_stellar_org_to_meter_map, :device_type
  end
end
