# frozen_string_literal: true

class ChangeSourceJsonbDefault < ActiveRecord::Migration[7.0]
  def change
    change_column_default :sources, :details, from: "{}", to: {}
  end
end
