# frozen_string_literal: true

class DefaultToSharingEverything < ActiveRecord::Migration[7.0]
  # I forgot to convert the visibilities from old schema to full visibility
  # adding all the columns now.
  # default visibilities can stay as they are...

  def up
    vis = Visibility.all.reject(&:default?)

    dt_h = Postgres::DataTable.non_custom.to_h do |dt|
      cols = dt.columns.reject(&:mandatory_visible?).map(&:sql_columns).flatten
      [dt.table_name, cols]
    end

    vis.map do |v|
      cus_h = v.project.sources.active.custom_data.to_h do |src|
        [src.custom_view, src.details["custom_columns"].map(&:first)]
      end

      col_h = cus_h.merge dt_h

      v.update! columns: col_h
    end
  end

  def down
    execute "UPDATE visibilities SET columns = '{}'"
  end
end
