# frozen_string_literal: true

class AddPrimaryUseToMeters < ActiveRecord::Migration[7.0]
  def change
    add_column :data_meters, :primary_use, :string

    reversible do |direction|
      direction.up do
        execute <<~SQL.squish
          UPDATE data_meters
          SET primary_use = custom ->> 'primary_appliance'
        SQL
      end
    end
  end
end
