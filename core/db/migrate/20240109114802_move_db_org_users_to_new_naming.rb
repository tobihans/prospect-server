# frozen_string_literal: true

class MoveDbOrgUsersToNewNaming < ActiveRecord::Migration[7.0]
  def up
    Organization.find_each do |o|
      sleep 2
      old = "prospect_organization_#{o.id}"
      fresh = "#{ActiveRecord::Base.connection.current_database}_#{old}"

      say "Renaming from #{old} => #{fresh}"

      rename_db_things! old, fresh
      rename_grafana_ds! old, fresh, o.grafana_org_id
    end
  end

  def down
    Organization.find_each do |o|
      old = "prospect_organization_#{o.id}"
      fresh = "#{ActiveRecord::Base.connection.current_database}_#{old}"

      say "Renaming from #{fresh} => #{old}"

      rename_db_things! fresh, old
      rename_grafana_ds! fresh, old, o.grafana_org_id
    end
  end

  def rename_db_things!(old, fresh)
    execute "ALTER USER #{old} RENAME TO #{fresh}"
    execute "ALTER SCHEMA #{old} RENAME TO #{fresh}"
  end

  def rename_grafana_ds!(old, fresh, grafana_org_id)
    client = Grafana::RemoteControl.send(:client)
    dss = client.data_sources(org_id: grafana_org_id)

    say "Grafana Org ID: #{grafana_org_id}"
    say "Rename Grafana Datasource User: #{old} -> #{fresh}"
    say "Found Datasources: #{dss.inspect}"

    old_ds = dss.find { _1[:user] == old }
    new_ds = dss.find { _1[:user] == fresh }

    say "Old DS: #{old_ds.inspect}"
    say "New DS: #{new_ds.inspect}"

    raise "Old ds and new_ds can not exist at the same time!" if old_ds && new_ds
    raise "Did not find neither old nor already renamed Grafana datasource #{old}" if [old_ds, new_ds].none?

    if new_ds
      say "New datasource already exists, nothing to do on Grafana side"
    else
      # only old_ds
      say "Changing old datasource to have proper db user"
      resp = client.update_data_source uid: old_ds[:uid],
                                       org_id: grafana_org_id,
                                       data_source: { user: fresh }
      say "Request Result: #{resp}"
    end
  end
end
