# frozen_string_literal: true

class FixGrafanaUserRoles < ActiveRecord::Migration[7.0]
  def up
    say_with_time "Iterating all users and their organizations and updating Grafana role." do
      User.find_each do |u|
        say "User: #{u} (#{u.id}) ...", true
        original_org = u.organization

        u.organizations.each do |org|
          say "... Switch to #{org} (#{org.id})", true
          u.switch_organization! org

          say "... Updating role #{u.grafana_role}", true
          u.update_grafana_rights
        end

        say "... Switch back to original org #{original_org} (#{original_org.id})", true
        u.switch_organization! original_org
      end
    end
  end

  def down
    say "This migration is just a fix. Nothing to revert here."
  end
end
