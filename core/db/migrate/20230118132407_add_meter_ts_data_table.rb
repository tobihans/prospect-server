# frozen_string_literal: true

class AddMeterTsDataTable < ActiveRecord::Migration[7.0]
  def change
    create_enum :phase, %w[1 2 3]

    hypertable_options = {
      time_column: "metered_at",
      chunk_time_interval: "30 days"
    }

    create_table :data_meters_ts, hypertable: hypertable_options, id: false do |t|
      t.string :device_uid, null: false
      t.timestamp :metered_at, null: false
      t.enum :phase, enum_type: :phase, default: "1"
      t.float :voltage_v
      t.float :power_factor
      t.float :power_w
      t.float :energy_wh
      t.float :frequency_hz
      t.float :current_a
      t.integer :interval_seconds
      t.timestamp :billing_cycle_start_at

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: false
      t.references :import, null: false, index: false
      t.references :organization, null: false, index: false
      t.float :trust_score
      t.string :data_origin, null: false
    end

    add_index :data_meters_ts, %i[device_uid metered_at], order: { device_uid: :desc, metered_at: :desc }, unique: true
    add_index :data_meters_ts, %i[source_id metered_at], order: { source_id: :desc, metered_at: :desc }
    add_index :data_meters_ts, %i[import_id metered_at], order: { import_id: :desc, metered_at: :desc }
    add_index :data_meters_ts, %i[organization_id metered_at], order: { organization_id: :desc, metered_at: :desc }
  end
end
