# frozen_string_literal: true

class UserAllowNullUsername < ActiveRecord::Migration[7.0]
  def up
    change_column :users, :username, :string, null: true
  end

  def down
    change_column :users, :username, :string, null: true
  end
end
