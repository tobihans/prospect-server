# frozen_string_literal: true

class AddGridFieldsToMeters < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.migration_without_views :meters do
      change_table :data_meters, bulk: true do |t|
        t.string :grid_operator_company
        t.string :grid_name
      end
    end
  end
end
