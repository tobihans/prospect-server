# frozen_string_literal: true

class AddTotalPriceToSHSAndMeters < ActiveRecord::Migration[7.1]
  def change
    add_column :data_shs, :total_price, :float
    add_column :data_meters, :total_price, :float
  end
end
