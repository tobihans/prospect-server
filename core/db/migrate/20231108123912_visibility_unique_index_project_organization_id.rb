# frozen_string_literal: true

class VisibilityUniqueIndexProjectOrganizationId < ActiveRecord::Migration[7.0]
  def change
    remove_index :visibilities, :project_id
    add_index :visibilities, %i[project_id organization_id], unique: true
  end
end
