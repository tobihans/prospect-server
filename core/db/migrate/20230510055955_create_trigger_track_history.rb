# frozen_string_literal: true

class CreateTriggerTrackHistory < ActiveRecord::Migration[7.0]
  def change
    create_trigger :track_history_custom, on: :data_custom
    create_trigger :track_history_grids, on: :data_grids
    create_trigger :track_history_meters, on: :data_meters
    create_trigger :track_history_payments_ts, on: :data_payments_ts
    create_trigger :track_history_shs, on: :data_shs
    create_trigger :track_history_test, on: :data_test
  end
end
