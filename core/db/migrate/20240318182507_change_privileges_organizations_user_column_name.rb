# frozen_string_literal: true

class ChangePrivilegesOrganizationsUserColumnName < ActiveRecord::Migration[7.1]
  def change
    rename_column :privileges, :organizations_user_id, :organization_user_id
    add_index :privileges, %i[organization_user_id name], unique: true
  end
end
