# frozen_string_literal: true

class InitializeProjectSqlStuff < ActiveRecord::Migration[7.0]
  def up
    Organization.table_name = "projects" # project was renamed to organization

    say "Setting up all organizations postgres users, schemas etc.."
    Postgres::RemoteControl.setup_all_organizations!

    say "Setting up all organizations data table views..."
    Postgres::RemoteControl.create_all_views!
  end

  def down
    say "Sorry, there is no way back!"
  end
end
