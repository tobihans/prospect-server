# frozen_string_literal: true

class RemoveRoleFromOrganizationUser < ActiveRecord::Migration[7.1]
  def change
    remove_column :organizations_users, :role, :string
  end
end
