# frozen_string_literal: true

class CreateFunctionTryCastNumeric < ActiveRecord::Migration[7.0]
  def change
    create_function :try_cast_numeric
  end
end
