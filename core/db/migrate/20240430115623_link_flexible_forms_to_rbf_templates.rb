# frozen_string_literal: true

class LinkFlexibleFormsToRbfTemplates < ActiveRecord::Migration[7.1]
  def change
    add_column :dynamic_forms, :rbf_claim_template_id, :bigint
    add_index :dynamic_forms, :rbf_claim_template_id
  end
end
