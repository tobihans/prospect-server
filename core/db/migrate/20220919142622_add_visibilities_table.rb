# frozen_string_literal: true

class AddVisibilitiesTable < ActiveRecord::Migration[7.0]
  def change
    create_table :visibilities do |t|
      t.integer :project_id, null: false
      t.integer :source_id, null: false
      t.integer :access # used as enum
      t.timestamps null: false
    end

    add_index :visibilities, %i[project_id source_id], unique: true
    add_index :visibilities, :access
  end
end
