# frozen_string_literal: true

class AddPaygoAccountsSnapshotsTs < ActiveRecord::Migration[7.0]
  def change
    create_enum :account_status, %w[active completed defaulted cancelled locked returned]

    hypertable_options = {
      time_column: "snapshot_date",
      chunk_time_interval: "30 days"
    }

    create_table :data_paygo_accounts_snapshots_ts, hypertable: hypertable_options, id: false do |t|
      t.string :account_uid, null: false
      t.date :snapshot_date, null: false
      t.enum :account_status, enum_type: :account_status, null: false
      t.integer :days_in_repayment, null: false
      t.float :cumulative_paid_amount, null: false
      t.integer :days_to_cutoff, null: false
      t.integer :cumulative_days_locked, null: false
      t.float :total_remaining_amount, null: false

      t.string :account_external_id, null: false
      t.bigint :organization_id, null: false
      t.bigint :source_id, null: false
      t.jsonb :custom, default: {}, null: false
      t.string :data_origin, null: false
      t.float :trust_score

      t.index :snapshot_date, order: :desc
      t.index %i[account_uid snapshot_date], name: :index_paygo_snapshots_on_account_uid_and_snapshot_date,
                                             order: :desc,
                                             unique: true
      t.index %i[account_external_id snapshot_date], name: :index_paygo_snapshots_on_account_ex_id_and_snapshot_date,
                                                     order: :desc
      t.index %i[organization_id snapshot_date], name: :index_paygo_snapshots_on_org_id_and_snapshot_date,
                                                 order: :desc
      t.index %i[source_id snapshot_date], name: :index_paygo_snapshots_on_src_id_and_snapshot_date,
                                           order: :desc
      t.timestamps
    end
  end
end
