# frozen_string_literal: true

class RenameSourceDetailsFormatToFileFormat < ActiveRecord::Migration[7.1]
  def up
    query = <<-SQL.squish
      UPDATE sources
      SET details = details - 'format' || jsonb_build_object('file_format', details->'format')
      WHERE details ? 'format'
    SQL

    execute query
  end

  def down
    query = <<-SQL.squish
      UPDATE sources
      SET details = details - 'file_format' || jsonb_build_object('format', details->'file_format')
      WHERE details ? 'file_format'
    SQL

    execute query
  end
end
