# frozen_string_literal: true

class AddRbfClaimVerifiers < ActiveRecord::Migration[7.1]
  def change
    create_table :rbf_claim_verifiers do |t|
      t.timestamps
      t.bigint :user_id

      t.bigint :rbf_claim_id, null: false
      t.bigint :verifier_id, null: false

      t.index %i[rbf_claim_id verifier_id], unique: true
    end
  end
end
