# frozen_string_literal: true

class AddSHSTsDataTable < ActiveRecord::Migration[7.0]
  def change
    hypertable_options = {
      time_column: "metered_at",
      chunk_time_interval: "30 days"
    }

    create_table :data_shs_ts, hypertable: hypertable_options, id: false do |t|
      t.string :device_uid, null: false
      t.timestamp :metered_at, null: false
      t.float :pv_input_w
      t.float :grid_input_w
      t.float :grid_input_v
      t.float :battery_v
      t.float :battery_io_w
      t.float :battery_charge_percent
      t.float :system_output_w
      t.float :grid_energy_wh
      t.float :pv_energy_wh
      t.float :output_energy_wh
      t.boolean :paygo_is_paid_off # rubocop:disable Rails/ThreeStateBooleanColumn
      t.integer :paygo_days_left

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: false
      t.references :import, null: false, index: false
      t.references :organization, null: false, index: false
      t.float :trust_score
      t.string :data_origin, null: false
    end

    add_index :data_shs_ts, %i[device_uid metered_at], order: { device_uid: :desc, metered_at: :desc }, unique: true
    add_index :data_shs_ts, %i[source_id metered_at], order: { source_id: :desc, metered_at: :desc }
    add_index :data_shs_ts, %i[import_id metered_at], order: { import_id: :desc, metered_at: :desc }
    add_index :data_shs_ts, %i[organization_id metered_at], order: { organization_id: :desc, metered_at: :desc }
  end
end
