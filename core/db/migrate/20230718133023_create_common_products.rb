# frozen_string_literal: true

class CreateCommonProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :common_products do |t|
      t.string :name, limit: 500, null: false
      t.string :brand, limit: 255
      t.string :model_number, limit: 255
      t.string :kind, limit: 255
      t.string :website, limit: 255
      t.jsonb :properties, null: false, default: "{}"
      t.string :data_origin, limit: 255

      t.timestamps
    end
    add_index :common_products, :name
    add_index :common_products, %i[name brand model_number], unique: true
  end
end
