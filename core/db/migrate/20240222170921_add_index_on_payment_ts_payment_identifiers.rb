# frozen_string_literal: true

class AddIndexOnPaymentTsPaymentIdentifiers < ActiveRecord::Migration[7.1]
  def change
    add_index :data_payments_ts, %i[payment_external_id paid_at]
    add_index :data_payments_ts, %i[provider_transaction_id paid_at]
  end
end
