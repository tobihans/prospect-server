# frozen_string_literal: true

class AddPostgresPasswordToProject < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :postgres_password, :string, limit: 1000 # overhead for encryption!
  end
end
