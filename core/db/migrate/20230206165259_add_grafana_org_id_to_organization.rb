# frozen_string_literal: true

class AddGrafanaOrgIdToOrganization < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :grafana_org_id, :integer

    say "Updating Grafana Organization ids" do
      Grafana::Client.new.all_orgs.each do |org|
        say "Updating #{org[:name]} -> #{org[:id]}"
        parts = org[:name].split("_")
        next if parts.length != 3

        Organization.find(parts.last).update! grafana_org_id: org[:id]
      end
    end
  end
end
