# frozen_string_literal: true

class MakeGridsTsAHypertable < ActiveRecord::Migration[7.0]
  def change
    create_hypertable :data_grids_ts, time_column: "metered_at", chunk_time_interval: "30 days", migrate_data: true
  end
end
