# frozen_string_literal: true

class RecalculateVoltageVInMetersTsWhereManufacturerHop < ActiveRecord::Migration[7.1]
  def up
    execute <<~SQL.squish
      UPDATE data_meters_ts
      SET	voltage_v =	(
        (
          COALESCE((custom ->> 'voltage_v_phase_1')::FLOAT, 0) +
          COALESCE((custom ->> 'voltage_v_phase_2')::FLOAT, 0) +
          COALESCE((custom ->> 'voltage_v_phase_3')::FLOAT, 0)
        ) /	NULLIF(
          COALESCE(((custom ->> 'voltage_v_phase_1')::FLOAT > 0)::INTEGER, 0) +
          COALESCE(((custom ->> 'voltage_v_phase_2')::FLOAT > 0)::INTEGER, 0) +
          COALESCE(((custom ->> 'voltage_v_phase_3')::FLOAT > 0)::INTEGER, 0), 0
        )
      )
      WHERE manufacturer = 'HOP'
    SQL
  end

  def down
    say "This migration ist not properly reversible."
  end
end
