# frozen_string_literal: true

class CreateTriggerBeforeUpdateChecks < ActiveRecord::Migration[7.0]
  def change
    create_trigger :before_update_checks_custom, on: :data_custom
    create_trigger :before_update_checks_grids, on: :data_grids
    create_trigger :before_update_checks_meters, on: :data_meters
    create_trigger :before_update_checks_payments_ts, on: :data_payments_ts
    create_trigger :before_update_checks_shs, on: :data_shs
    create_trigger :before_update_checks_test, on: :data_test
  end
end
