# frozen_string_literal: true

class MetersAccountNonNull < ActiveRecord::Migration[7.0]
  def up
    change_table :data_meters, bulk: true do |t|
      t.change_null :account_external_id, true
      t.change_null :account_uid, true
    end
  end

  def down
    change_table :data_meters, bulk: true do |t|
      t.change_null :account_external_id, false
      t.change_null :account_uid, false
    end
  end
end
