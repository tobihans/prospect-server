# frozen_string_literal: true

class RenameSourceKindVictronToVictronGrids < ActiveRecord::Migration[7.0]
  def up
    execute "UPDATE sources SET data_category = 'grids' WHERE kind = 'api/victron'"
  end

  def down
    say "This migration ist not properly reversible."
  end
end
