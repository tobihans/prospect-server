# frozen_string_literal: true

class AddOperatorAndNameToGridsTs < ActiveRecord::Migration[7.0]
  def change
    change_table :data_grids_ts, bulk: true do |t|
      t.string :operator_company
      t.string :name
    end
  end
end
