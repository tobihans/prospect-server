# frozen_string_literal: true

class AddClaimedPaymentsTsDataToRbfClaim < ActiveRecord::Migration[7.1]
  def change
    add_column :rbf_device_claims, :claimed_payments_data, :jsonb, default: [], null: false
  end
end
