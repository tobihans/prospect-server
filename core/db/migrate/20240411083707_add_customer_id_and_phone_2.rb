# frozen_string_literal: true

class AddCustomerIdAndPhone2 < ActiveRecord::Migration[7.1]
  def change
    change_table :data_shs, bulk: true do |t|
      t.string :customer_id_type
      t.string :customer_id_number_p
      t.string :customer_id_number_e
      t.string :customer_phone_2_p
      t.string :customer_phone_2_e
      t.string :customer_location_area_5 # rubocop:disable Naming/VariableNumber
    end

    change_table :data_meters, bulk: true do |t|
      t.string :customer_id_type
      t.string :customer_id_number_p
      t.string :customer_id_number_e
      t.string :customer_phone_2_p
      t.string :customer_phone_2_e
      t.string :customer_location_area_5 # rubocop:disable Naming/VariableNumber
    end
  end
end
