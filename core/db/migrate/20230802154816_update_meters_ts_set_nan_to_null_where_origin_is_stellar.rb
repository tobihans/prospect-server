# frozen_string_literal: true

class UpdateMetersTsSetNanToNullWhereOriginIsStellar < ActiveRecord::Migration[7.0]
  def up
    #
    # - reassign NaN values with Nulls
    #
    %w[voltage_v power_factor power_w energy_wh frequency_hz current_a].each do |column|
      execute <<~SQL.squish
        UPDATE public.data_meters_ts
        SET
            #{column} = NULL
        WHERE
            #{column} = 'NaN'
        AND data_origin = 'stellar';
      SQL
    end
  end

  def down
    #
    # - not reversible
    #
  end
end
