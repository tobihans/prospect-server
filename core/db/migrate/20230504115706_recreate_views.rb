# frozen_string_literal: true

class RecreateViews < ActiveRecord::Migration[7.0]
  def change
    reversible do |direction|
      direction.up do
        Postgres::RemoteControl.after_migration!
      end
      direction.down do
        Postgres::RemoteControl.drop_all_organization_views!
      end
    end
  end
end
