# frozen_string_literal: true

class GridsTsMandatoryGridName < ActiveRecord::Migration[7.0]
  def up
    say_with_time "Populating Grid related tables with grid_name and build new grid_uid" do
      populate_gridname_and_build_uids
    end

    # after thorough analysys of the data we just decide to:
    # just DELETE grids_ts data which could not be matched with a grid

    execute "DELETE FROM data_grids_ts WHERE grid_name IS NULL"

    change_column_null :data_grids_ts, :grid_name, false
  end

  def down
    say "DANGER! the data migration is not reversible!"
    change_column_null :data_grids_ts, :grid_name, true
  end

  def populate_gridname_and_build_uids
    ### For each grid we now run 3 queries, like those:
    #
    # UPDATE data_meters SET grid_uid = '1_SMU 12 Chinsanka', grid_name = 'SMU 12 Chinsanka' WHERE grid_uid = 'Standard Microgrid_SMU 12 Chinsanka'
    # UPDATE data_grids_ts SET grid_uid = '1_SMU 12 Chinsanka', grid_name = 'SMU 12 Chinsanka' WHERE grid_uid = 'Standard Microgrid_SMU 12 Chinsanka'
    # UPDATE data_grids SET uid = '1_SMU 12 Chinsanka' WHERE uid = 'Standard Microgrid_SMU 12 Chinsanka'
    #

    r = ActiveRecord::Base.connection.execute "SELECT uid, organization_id, name FROM data_grids"

    queries = r.values.map do |old_uid, organization_id, name|
      new_uid = "#{organization_id}_#{name}"

      say "#{old_uid} => #{new_uid}"

      q = "UPDATE %{table} SET grid_uid = '#{new_uid}', grid_name = '#{name}' WHERE grid_uid = '#{old_uid}' AND organization_id = #{organization_id}"

      qs = []
      qs << (q % { table: :data_meters })
      qs << (q % { table: :data_grids_ts })

      qs << "UPDATE data_grids SET uid = '#{new_uid}' WHERE uid = '#{old_uid}' AND organization_id = #{organization_id}"

      qs
    end

    queries.flatten.each do |q|
      execute q
    end
  end
end
