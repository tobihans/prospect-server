# frozen_string_literal: true

class AddAddPaymentsTsDataTable < ActiveRecord::Migration[7.0]
  def change
    hypertable_options = {
      time_column: "paid_at",
      chunk_time_interval: "30 days"
    }

    create_table :data_payments_ts, hypertable: hypertable_options, id: false do |t|
      t.string :payment_external_id, null: false
      t.string :purpose
      t.datetime :paid_at, null: false
      t.float :amount, null: false
      t.string :currency, null: false
      t.string :provider_name
      t.string :provider_transaction_id
      t.datetime :reverted_at
      t.float :days_active

      t.timestamps
      t.jsonb :custom, null: false, default: {}
      t.string :uid, null: false
      t.string :shs_account_uid, null: false
      t.string :meters_account_uid, null: false

      t.references :source, null: false, index: false
      t.references :import, null: false, index: false
      t.references :organization, null: false, index: false
      t.string :data_origin, null: false
      t.float :trust_score
    end

    add_index :data_payments_ts, %i[payment_external_id paid_at], order: { payment_external_id: :desc, paid_at: :desc }, unique: true
    add_index :data_payments_ts, %i[source_id paid_at], order: { source_id: :desc,  paid_at: :desc }
    add_index :data_payments_ts, %i[import_id paid_at], order: { import_id: :desc,  paid_at: :desc }
    add_index :data_payments_ts, %i[organization_id paid_at], order: { organization_id: :desc, paid_at: :desc }
    add_index :data_payments_ts, %i[uid paid_at], order: { uid: :desc, paid_at: :desc }
    add_index :data_payments_ts, %i[shs_account_uid paid_at], order: { shs_account_uid: :desc, paid_at: :desc }
    add_index :data_payments_ts, %i[meters_account_uid paid_at], order: { meters_account_uid: :desc, paid_at: :desc }
  end
end
