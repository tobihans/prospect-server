# frozen_string_literal: true

class ChangePaymentPlanDaysToFloat < ActiveRecord::Migration[7.0]
  reversible do |dir|
    dir.up do
      Postgres::RemoteControl.migration_without_views :shs do
        change_column :data_shs, :payment_plan_days, :float
      end
    end
    dir.down do
      Postgres::RemoteControl.migration_without_views :shs do
        change_column :data_shs, :payment_plan_days, :integer
      end
    end
  end
end
