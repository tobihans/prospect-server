# frozen_string_literal: true

class AddEncryptedFieldsToGridsTable < ActiveRecord::Migration[7.0]
  def change
    change_table :data_grids do |t|
      t.string :operator_phone_e
    end
  end
end
