# frozen_string_literal: true

class AddEncryptedFieldsToSHSTable < ActiveRecord::Migration[7.0]
  def change
    change_table :data_shs, bulk: true do |t|
      t.string :customer_name_e
      t.string :customer_address_e
      t.string :customer_latitude_e
      t.string :customer_longitude_e
      t.string :customer_email_e
      t.string :customer_phone_e
      t.string :seller_name_e
      t.string :seller_address_e
      t.string :seller_latitude_e
      t.string :seller_longitude_e
      t.string :seller_email_e
      t.string :seller_phone_e
    end
  end
end
