# frozen_string_literal: true

class RenameGridExternalIdToExtenalIdOnGrid < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.migration_without_views :grids do
      rename_column :data_grids, :grid_external_id, :external_id
    end
  end
end
