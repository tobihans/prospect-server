# frozen_string_literal: true

class AddGridsDataTable < ActiveRecord::Migration[7.0]
  def change
    # rubocop:disable Naming/VariableNumber
    create_table :data_grids do |t|
      t.string :uid, null: false, index: true
      t.string :grid_external_id, null: false, index: true
      t.string :name, null: false
      t.string :operator_company, null: false

      t.string :location_area_1
      t.string :location_area_2
      t.string :location_area_3
      t.string :location_area_4
      t.string :country, null: false
      t.float :latitude
      t.float :longitude

      t.float :power_rating_kw
      t.float :primary_input_installed_kw
      t.float :secondary_input_installed_kw
      t.float :storage_capacity_kwh

      t.string :primary_input_source
      t.string :secondary_input_source
      t.string :operator_phone_p
      t.boolean :is_offgrid, null: false # rubocop:disable Rails/ThreeStateBooleanColumn

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: true
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.float :trust_score
      t.string :data_origin, null: false
    end
    # rubocop:enable Naming/VariableNumber
  end
end
