# frozen_string_literal: true

class UpdateMetersTsSerialNumberWhereOriginIsStellar < ActiveRecord::Migration[7.0]
  def up
    #
    # - reassign serial_number with serial_number from data_meters
    # - rebuild and reassign device_uid from data_meters
    #

    execute <<~SQL.squish
      UPDATE public.data_meters_ts meters_ts
      SET
        serial_number = meters.serial_number,
        device_uid = meters.device_uid
      FROM (
        SELECT
          customer_external_id,
          serial_number,
          device_uid,
          organization_id || '_' || manufacturer || '_a2ei_' || customer_external_id || '_mtr' as device_uid_old
        FROM public.data_meters
        WHERE data_origin = 'stellar'
        ) meters
      WHERE
        meters.device_uid_old = meters_ts.device_uid
      AND meters_ts.data_origin = 'stellar';
    SQL
  end

  def down
    #
    # - rebuild and reassign serial_number using customer_external_id from data_meters
    # - rebuild and reassign device_uid using customer_external_id from data_meters
    #

    execute <<~SQL.squish
      UPDATE public.data_meters_ts meters_ts
      SET
        serial_number = 'a2ei_' || meters.customer_external_id || '_mtr',
        device_uid = organization_id || '_' || manufacturer || '_a2ei_' || customer_external_id || '_mtr'
      FROM (
        SELECT
          customer_external_id,
          serial_number,
          device_uid
        FROM public.data_meters
        WHERE data_origin = 'stellar'
      ) meters
      WHERE
        meters.device_uid = meters_ts.device_uid
      AND meters_ts.data_origin = 'stellar';
    SQL
  end
end
