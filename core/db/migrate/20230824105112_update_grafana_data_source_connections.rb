# frozen_string_literal: true

class UpdateGrafanaDataSourceConnections < ActiveRecord::Migration[7.0]
  def change
    say "Updating all Grafana Organization Postgres Datasources to new Default connection settings..."
    Organization.all.map do |o|
      c = Grafana::Client.new
      uid = c.
            data_sources(org_id: o.grafana_org_id).
            find { |ds| ds[:name] == Naming.grafana_data_source(o) }.
            try(:[], :uid)

      unless uid
        say "No Datasource found for Organization #{o.id}, skipping!"
        next
      end

      say "Updating Datasource #{uid} for Organization #{o.id}...", true

      c.update_data_source uid:, org_id: o.grafana_org_id,
                           data_source: Grafana::RemoteControl::DATA_SOURCE_DEFAULT
    end
  end
end
