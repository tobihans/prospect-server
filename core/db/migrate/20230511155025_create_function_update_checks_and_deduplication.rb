# frozen_string_literal: true

class CreateFunctionUpdateChecksAndDeduplication < ActiveRecord::Migration[7.0]
  def change
    create_function :update_checks_and_deduplication
  end
end
