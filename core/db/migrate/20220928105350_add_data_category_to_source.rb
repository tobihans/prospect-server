# frozen_string_literal: true

class AddDataCategoryToSource < ActiveRecord::Migration[7.0]
  def change
    add_column :sources, :data_category, :string
  end
end
