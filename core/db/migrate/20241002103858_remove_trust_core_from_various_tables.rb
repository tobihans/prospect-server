# frozen_string_literal: true

class RemoveTrustCoreFromVariousTables < ActiveRecord::Migration[7.1]
  def change
    %w[data_custom data_grids data_grids_ts data_meter_events_ts data_meters data_meters_ts data_paygo_accounts_snapshots_ts data_payments_ts data_shs data_shs_ts data_test
       data_trust_trace].each do |table_name|
      remove_column table_name.to_sym, :trust_score, :float
    end
  end
end
