# frozen_string_literal: true

class EnhanceRbfClaimTimestampsAndReviewer < ActiveRecord::Migration[7.1]
  def change
    change_table :rbf_claims, bulk: true do |t|
      t.timestamp :submitted_to_program_at
      t.bigint :submitted_to_program_by_id
      t.timestamp :verification_at
      t.bigint :verification_by_id
      t.timestamp :approved_at
      t.bigint :approved_by_id
      t.timestamp :paid_out_at
      t.bigint :paid_out_by_id
      t.timestamp :rejected_at
      t.bigint :rejected_by_id

      t.bigint :assigned_to_verifier_id
    end
  end
end
