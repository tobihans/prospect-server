# frozen_string_literal: true

class AddImportCounters < ActiveRecord::Migration[7.0]
  def change
    change_table :imports, bulk: true do |t|
      t.integer :rows_inserted
      t.integer :rows_updated
      t.integer :rows_duplicated
      t.integer :rows_failed
    end
  end
end
