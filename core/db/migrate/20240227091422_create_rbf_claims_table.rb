# frozen_string_literal: true

class CreateRbfClaimsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :rbf_claim_templates do |t|
      t.bigint :organization_group_id, index: true, null: false
      t.bigint :supervising_organization_id, index: true, null: false
      t.bigint :managing_organization_id, index: true, null: false
      t.bigint :verifying_organization_id, index: true, null: false
      t.bigint :user_id, index: true, null: false
      t.string :name, index: { unique: true }
      t.string :form_name, null: false
      t.string :trust_trace_check, null: false
      t.string :currency, null: false
      t.timestamps
    end

    create_table :rbf_claims do |t|
      t.string :aasm_state, index: true

      t.bigint :user_id, index: true, null: false
      t.bigint :rbf_claim_template_id, index: true, null: false
      t.bigint :organization_id, index: true, null: false

      t.jsonb :claimed_data, null: false, default: {}
      t.jsonb :verified_data, null: false, default: {}

      t.integer :claimed_subunit
      t.integer :verified_subunit

      t.timestamps
    end

    create_table :rbf_products do |t|
      t.string :manufacturer, null: false
      t.string :model, null: false
      t.string :rbf_category, null: false
      t.string :rbf_claim_template_id, index: true, null: false
      t.string :organization_id, index: true, null: false
      t.bigint :user_id, index: true
      t.timestamps
    end

    add_index :rbf_products, %i[model organization_id manufacturer], unique: true

    create_table :rbf_device_claims do |t|
      t.bigint :rbf_claim_id, index: true, null: false
      t.bigint :parent_rbf_device_claim_id, index: true
      t.bigint :user_id, index: true, null: false
      t.bigint :reviewer_id, index: true

      t.string :subject_origin, null: false
      t.string :subject_uid, null: false

      t.jsonb :claimed_devices_data, null: false, default: {}
      t.jsonb :claimed_trust_trace_data, null: false, default: {}
      t.jsonb :verified_data, null: false, default: {}

      t.string :verification_category
      t.string :submission_category
      t.string :rejection_category

      t.text :submission_comment
      t.text :reviewer_comment

      t.string :state, null: false
      t.timestamps
    end

    add_index :rbf_device_claims, %i[subject_origin subject_uid]
  end
end
