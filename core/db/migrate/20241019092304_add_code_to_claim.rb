# frozen_string_literal: true

class AddCodeToClaim < ActiveRecord::Migration[7.1]
  def change
    add_column :rbf_claims, :code, :string
    add_index :rbf_claims, :code, unique: true
  end
end
