# frozen_string_literal: true

class CreateEncryptedBlobs < ActiveRecord::Migration[7.1]
  def change
    create_table :encrypted_blobs do |t|
      t.references :import, null: false
      t.text :error
      t.text :url_e

      t.timestamps
    end
  end
end
