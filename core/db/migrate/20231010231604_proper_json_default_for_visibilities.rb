# frozen_string_literal: true

class ProperJsonDefaultForVisibilities < ActiveRecord::Migration[7.0]
  def change
    change_table :visibilities, bulk: true do |t|
      t.change_default :columns, from: "{}", to: {}
      t.change_default :conditions, from: "{}", to: {}

      reversible do |direction|
        direction.up do
          execute "UPDATE visibilities SET columns = '{}'::json"
          execute "UPDATE visibilities SET conditions = '{}'::json"
        end
      end
    end
  end
end
