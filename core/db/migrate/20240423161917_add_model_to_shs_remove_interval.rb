# frozen_string_literal: true

class AddModelToSHSRemoveInterval < ActiveRecord::Migration[7.1]
  def change
    change_table :data_shs, bulk: true do |t|
      t.string :model
      t.remove :interval_seconds, type: :integer
    end
  end
end
