# frozen_string_literal: true

class DataTestCustomNonNullable < ActiveRecord::Migration[7.0]
  def change
    change_column_null :data_test, :custom, false
  end
end
