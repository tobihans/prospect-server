# frozen_string_literal: true

class DataTablesPromotePrimaryKey < ActiveRecord::Migration[7.1]
  def up
    Postgres::DataTable.select(&:ts?).each do |dt|
      pks = dt.columns.select(&:primary_key?).map(&:name)
      ActiveRecord::Base.connection.indexes(dt.table_name).select(&:unique).each do |index|
        execute("DROP INDEX #{index.name}")
      end

      execute("ALTER TABLE #{dt.table_name} ADD CONSTRAINT pk_#{dt.table_name}_on_#{pks.join('_and_')} PRIMARY KEY (#{pks.join(', ')})")
    end

    Postgres::DataTable.reject(&:ts?).each do |dt|
      next unless dt.columns.map(&:name).include?("uid")

      pks = dt.columns.select(&:primary_key?).map(&:name)

      execute("ALTER TABLE #{dt.table_name} DROP COLUMN id")
      execute("ALTER TABLE #{dt.table_name} ADD CONSTRAINT pk_#{dt.table_name}_on_#{pks.join} PRIMARY KEY USING INDEX index_#{dt.table_name}_on_uid")
    end
  end

  def down
    Postgres::DataTable.select(&:ts?).each do |dt|
      pks = dt.columns.select(&:primary_key?).map(&:name)

      execute("ALTER TABLE #{dt.table_name} DROP CONSTRAINT pk_#{dt.table_name}_on_#{pks.join('_and_')}")
      execute("CREATE UNIQUE INDEX index_#{dt.table_name}_on_#{pks.join('_and_')} ON #{dt.table_name} (#{pks.join(' DESC, ')} DESC) ")
    end

    Postgres::DataTable.reject(&:ts?).each do |dt|
      next unless dt.columns.map(&:name).include?("uid")

      pks = dt.columns.select(&:primary_key?).map(&:name)

      execute("ALTER TABLE #{dt.table_name} DROP CONSTRAINT pk_#{dt.table_name}_on_#{pks.join}")

      execute("ALTER TABLE #{dt.table_name} ADD COLUMN id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY")
      execute("ALTER TABLE #{dt.table_name} ADD PRIMARY KEY (id)")

      execute("CREATE UNIQUE INDEX index_#{dt.table_name}_on_uid ON #{dt.table_name} (uid)")
    end
  end
end
