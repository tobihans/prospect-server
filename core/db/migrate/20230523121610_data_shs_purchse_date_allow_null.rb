# frozen_string_literal: true

class DataSHSPurchseDateAllowNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :data_shs, :purchase_date, true
  end
end
