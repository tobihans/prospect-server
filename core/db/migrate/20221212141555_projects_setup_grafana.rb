# frozen_string_literal: true

class ProjectsSetupGrafana < ActiveRecord::Migration[7.0]
  def up
    Organization.table_name = "projects" # project was renamed to organization
    Grafana::RemoteControl.setup_all_organizations!
  end

  def down
    client = Grafana::Client.new
    client.all_users.each { |user| client.delete_user(user[:id]) }
    client.all_orgs.each { |org| client.delete_org(id: org[:id]) }
  end
end
