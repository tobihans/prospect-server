# frozen_string_literal: true

class ProjectNameOrganizationIndex < ActiveRecord::Migration[7.0]
  def change
    add_index :projects, %i[name organization_id], unique: true
  end
end
