# frozen_string_literal: true

class FixIndicesUid < ActiveRecord::Migration[7.0]
  def change
    # rubocop:disable Rails/ReversibleMigration
    remove_index :data_grids, :uid
    add_index :data_grids, :uid, unique: true

    remove_index :data_grids, name: :index_data_payments_ts_on_uid_and_paid_at
    add_index :data_payments_ts, %i[uid paid_at], order: { uid: :desc, paid_at: :desc }, unique: true

    change_column :data_meters, :customer_latitude_p, :string
    change_column :data_meters, :customer_longitude_p, :string

    change_column :data_shs, :customer_latitude_p, :string
    change_column :data_shs, :customer_longitude_p, :string

    change_column :data_shs, :seller_latitude_p, :string
    change_column :data_shs, :seller_longitude_p, :string

    remove_index :data_test, :uid
    add_index :data_test, :uid, unique: true

    remove_index :data_shs, :uid
    add_index :data_shs, :uid, unique: true

    remove_index :data_meters, :uid
    add_index :data_meters, :uid, unique: true
    # rubocop:enable Rails/ReversibleMigration Rails/BulkChangeTable
  end
end
