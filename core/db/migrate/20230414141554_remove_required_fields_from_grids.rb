# frozen_string_literal: true

class RemoveRequiredFieldsFromGrids < ActiveRecord::Migration[7.0]
  def up
    change_table :data_grids, bulk: true do |t|
      t.change_null :grid_external_id, true
      t.change_null :country, true
      t.change_null :is_offgrid, true
    end
  end

  def down
    change_table :data_grids, bulk: true do |t|
      t.change_null :grid_external_id, false
      t.change_null :country, false
      t.change_null :is_offgrid, false
    end
  end
end
