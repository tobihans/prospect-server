# frozen_string_literal: true

class FixPaymentTsIndex < ActiveRecord::Migration[7.0]
  def up
    remove_index :data_payments_ts, name: "index_data_payments_ts_on_payment_external_id_and_paid_at"
  end

  def down; end
end
