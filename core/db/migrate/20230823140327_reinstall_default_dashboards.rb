# frozen_string_literal: true

class ReinstallDefaultDashboards < ActiveRecord::Migration[7.0]
  def change
    say_with_time "Reinstalling all default dashboards in Grafana" do
      say "Disabled the code because it broke!"
      # Organization.all.each do |o|
      #  Grafana::RemoteControl.default_dashboards_for_organization! o
      # end
    end
  end
end
