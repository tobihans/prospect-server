# frozen_string_literal: true

class PaymentsTsXorSHSAndMetersAccountAllowNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :data_payments_ts, :shs_account_uid, true
    change_column_null :data_payments_ts, :meters_account_uid, true
  end
end
