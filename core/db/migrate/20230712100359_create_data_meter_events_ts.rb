# frozen_string_literal: true

class CreateDataMeterEventsTs < ActiveRecord::Migration[7.0]
  def change
    hypertable_options = {
      time_column: "start_at",
      chunk_time_interval: "30 days"
    }
    create_table :data_meter_events_ts, hypertable: hypertable_options, id: false do |t|
      t.string "device_uid", null: false
      t.bigint "organization_id", null: false
      t.string "manufacturer", default: "", null: false
      t.string "serial_number", default: "", null: false
      t.datetime "start_at", precision: nil, null: false
      t.datetime "end_at", precision: nil, null: false
      t.string "category", null: false

      t.integer "duration_sec"
      t.float "energy_start_wh"
      t.float "energy_end_wh"
      t.float "energy_consumed_wh"

      t.jsonb "custom", default: {}, null: false
      t.string "data_origin", null: false
      t.bigint "source_id", null: false
      t.float "trust_score"
      t.index :start_at, order: :desc
      t.index %i[device_uid start_at], unique: true, order: :desc
      t.index %i[organization_id start_at], order: :desc
      t.index %i[source_id start_at], order: :desc
      t.index %i[category start_at], order: :desc
      t.timestamps
    end
  end
end
