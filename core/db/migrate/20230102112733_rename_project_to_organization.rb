# frozen_string_literal: true

class RenameProjectToOrganization < ActiveRecord::Migration[7.0]
  def change
    rename_table :projects, :organizations
    rename_column :sources, :project_id, :organization_id
    rename_column :users, :project_id, :organization_id
    rename_column :visibilities, :project_id, :organization_id
  end
end
