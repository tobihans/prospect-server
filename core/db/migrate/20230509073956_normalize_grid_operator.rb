# frozen_string_literal: true

class NormalizeGridOperator < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.migration_without_views :meters, :grids_ts do
      remove_column :data_meters, :grid_operator_company, :string
      remove_column :data_grids_ts, :grid_operator_company, :string
    end
  end
end
