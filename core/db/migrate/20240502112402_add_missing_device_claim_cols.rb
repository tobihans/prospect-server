# frozen_string_literal: true

class AddMissingDeviceClaimCols < ActiveRecord::Migration[7.1]
  def change
    change_table :rbf_device_claims, bulk: true do |t|
      t.string :trust_trace_uid, index: true
      t.string :device_category
      t.integer :claimed_subsidy, default: 0
      t.integer :claimed_incentive, default: 0
      t.integer :verified_subsidy, default: 0
      t.integer :verified_incentive, default: 0
    end
  end
end
