# frozen_string_literal: true

class AddLastUpdateTrackingToUpdateableDataTables < ActiveRecord::Migration[7.0]
  def change
    tables = %w[custom grids meters payments_ts shs test]
    tables.each do |cat|
      change_table "data_#{cat}", bulk: true do |t|
        t.references :last_source, index: true
        t.references :last_import, index: true
      end
    end
  end
end
