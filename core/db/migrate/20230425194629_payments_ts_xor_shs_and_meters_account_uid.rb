# frozen_string_literal: true

class PaymentsTsXorSHSAndMetersAccountUid < ActiveRecord::Migration[7.0]
  # XOR check for payments_ts - payment can only belong to either shs_account_uid or meters_account_uid
  # https://stackoverflow.com/a/41874830

  def change
    add_check_constraint :data_payments_ts,
                         "num_nonnulls(shs_account_uid, meters_account_uid) = 1",
                         name: "xor_account_uids"
  end
end
