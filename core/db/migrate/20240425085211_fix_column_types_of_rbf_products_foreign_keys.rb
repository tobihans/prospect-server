# frozen_string_literal: true

class FixColumnTypesOfRbfProductsForeignKeys < ActiveRecord::Migration[7.1]
  def up
    change_table :rbf_products, bulk: true do |t|
      t.change :rbf_claim_template_id, "bigint USING CAST(rbf_claim_template_id AS integer)"
      t.change :organization_id, "bigint USING CAST(organization_id AS integer)"
    end
  end

  def down
    change_table :rbf_products, bulk: true do |t|
      t.change :rbf_claim_template_id, :string
      t.change :organization_id, :string
    end
  end
end
