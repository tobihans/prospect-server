# frozen_string_literal: true

class AddEncryptedFieldsToTestDataTable < ActiveRecord::Migration[7.0]
  def change
    change_table :data_test do |t|
      t.string :name_column_e
    end
  end
end
