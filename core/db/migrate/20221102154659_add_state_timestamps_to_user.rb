# frozen_string_literal: true

class AddStateTimestampsToUser < ActiveRecord::Migration[7.0]
  def change
    change_table :users, bulk: true do |t|
      t.timestamp :active_at
      t.timestamp :archived_at
    end

    add_index :users, :active_at
    add_index :users, :archived_at
  end
end
