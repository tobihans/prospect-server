# frozen_string_literal: true

class AddRbfOrganizationConfig < ActiveRecord::Migration[7.1]
  def change
    create_table :rbf_organization_configs do |t|
      t.integer :organization_id, null: false
      t.integer :rbf_claim_template_id, null: false
      t.integer :user_id

      t.float :total_grant_allocation, null: false, default: 0
      t.string :code, null: false

      t.timestamps

      t.index %i[organization_id rbf_claim_template_id], unique: true
      t.index %i[code rbf_claim_template_id], unique: true
    end
  end
end
