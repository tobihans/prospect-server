# frozen_string_literal: true

class CreateTriggersForTrustTrace < ActiveRecord::Migration[7.1]
  def change
    create_trigger :track_history_trust_trace, on: :data_trust_trace
    create_trigger :before_update_checks_trust_trace, on: :data_trust_trace
  end
end
