# frozen_string_literal: true

class AddPaymentExternalIdToPaymentsTsUid < ActiveRecord::Migration[7.0]
  def up
    execute "DROP TRIGGER before_update_checks_payments_ts ON data_payments_ts"
    execute "DROP TRIGGER track_history_payments_ts ON data_payments_ts"

    execute 'UPDATE data_payments_ts SET uid = account_origin::TEXT || \'_\' || "account_uid" || \'_\'  || "payment_external_id" || \'_\' || "paid_at"'

    execute "CREATE TRIGGER track_history_payments_ts AFTER DELETE OR UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()"
    execute "CREATE TRIGGER before_update_checks_payments_ts BEFORE UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()"
  end

  def down
    execute "DROP TRIGGER before_update_checks_payments_ts ON data_payments_ts"
    execute "DROP TRIGGER track_history_payments_ts ON data_payments_ts"

    execute 'UPDATE data_payments_ts SET uid = account_origin::TEXT || \'_\' || "account_uid" || \'_\' || "paid_at"'

    execute "CREATE TRIGGER track_history_payments_ts AFTER DELETE OR UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION track_changes_in_history()"
    execute "CREATE TRIGGER before_update_checks_payments_ts BEFORE UPDATE ON public.data_payments_ts FOR EACH ROW EXECUTE FUNCTION update_checks_and_deduplication()"
  end
end
