# frozen_string_literal: true

class FixStellarMapperTable < ActiveRecord::Migration[7.0]
  def up
    change_column :dataworker_stellar_org_to_meter_map, :meter_id, :bigint
  end

  def down
    change_column :dataworker_stellar_org_to_meter_map, :meter_id, :integer
  end
end
