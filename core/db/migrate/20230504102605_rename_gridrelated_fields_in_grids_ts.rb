# frozen_string_literal: true

class RenameGridrelatedFieldsInGridsTs < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.migration_without_views :grids_ts do
      change_table :data_grids_ts, bulk: true do |t|
        t.rename :operator_company, :grid_operator_company
        t.rename :name, :grid_name
      end
    end
  end
end
