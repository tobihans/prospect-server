# frozen_string_literal: true

class AddGridTsDataTable < ActiveRecord::Migration[7.0]
  def change
    hypertable_options = {
      time_column: "metered_at",
      chunk_time_interval: "30 days"
    }

    create_table :data_grids_ts, hypertable_options:, id: false do |t|
      t.string :grid_uid, null: false
      t.string :grid_external_id, null: false, index: true
      t.timestamp :metered_at, null: false

      t.float :primary_source_to_customer_kwh
      t.float :primary_source_to_battery_kwh
      t.float :secondary_source_to_customer_kwh
      t.float :secondary_source_to_battery_kwh
      t.float :battery_to_customer_kwh
      t.float :battery_charge_state_percent

      t.integer :interval_seconds
      t.integer :households_connected

      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: true
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.float :trust_score
      t.string :data_origin, null: false
    end

    add_index :data_grids_ts, %i[grid_uid metered_at], order: { device_uid: :desc, metered_at: :desc }, unique: true
    add_index :data_grids_ts, %i[source_id metered_at], order: { source_id: :desc, metered_at: :desc }
    add_index :data_grids_ts, %i[import_id metered_at], order: { import_id: :desc, metered_at: :desc }
    add_index :data_grids_ts, %i[organization_id metered_at], order: { organization_id: :desc, metered_at: :desc }
  end
end
