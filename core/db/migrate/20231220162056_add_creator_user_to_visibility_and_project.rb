# frozen_string_literal: true

class AddCreatorUserToVisibilityAndProject < ActiveRecord::Migration[7.0]
  def change
    add_column :visibilities, :user_id, :integer
    add_index :visibilities, :user_id

    add_column :projects, :user_id, :integer
    add_index :projects, :user_id
  end
end
