# frozen_string_literal: true

class AddNewIntervalColumnInSHSTs < ActiveRecord::Migration[7.0]
  def change
    add_column :data_shs_ts, :interval_seconds, :integer
  end
end
