# frozen_string_literal: true

class AddRbfProductPricesTable < ActiveRecord::Migration[7.1]
  def change
    create_table :rbf_product_prices do |t|
      t.bigint :rbf_product_id, null: false, index: true
      t.string :category, null: false
      t.date :valid_from, null: false
      t.float :amount, null: false
      t.bigint :user_id
      t.timestamps

      t.index %i[rbf_product_id category valid_from], unique: true
    end
  end
end
