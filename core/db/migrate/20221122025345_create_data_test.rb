# frozen_string_literal: true

class CreateDataTest < ActiveRecord::Migration[7.0]
  def change
    create_table :data_test do |t|
      t.string :uid, null: false
      t.integer :int_column
      t.boolean :bool_column # rubocop:disable Rails/ThreeStateBooleanColumn
      t.float :float_column, null: false
      t.datetime :time_column
      t.string :name_column
      t.string :personal_column
      t.string :something_uid

      t.references :source, null: false
      t.jsonb :custom, default: {}
      t.timestamps

      t.index :uid
      t.index :something_uid
    end
  end
end
