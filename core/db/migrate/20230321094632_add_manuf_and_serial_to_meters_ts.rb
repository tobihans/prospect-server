# frozen_string_literal: true

class AddManufAndSerialToMetersTs < ActiveRecord::Migration[7.0]
  def change
    change_table :data_meters_ts, bulk: true do |t|
      t.string :manufacturer, null: false, default: ""
      t.string :serial_number, null: false, default: ""
    end
  end
end
