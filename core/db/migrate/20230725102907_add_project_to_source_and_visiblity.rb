# frozen_string_literal: true

class AddProjectToSourceAndVisiblity < ActiveRecord::Migration[7.0]
  def change
    add_reference :sources, :project
    add_reference :visibilities, :project

    say_with_time "Create a project for each source. Populate ids accordingly" do
      reversible do |direction|
        direction.up do
          populate_projects_and_relations!
          delete_dangling_visibilities!
        end

        direction.down do
          truncate :projects
        end
      end
    end

    change_column_null :sources, :project_id, false
    change_column_null :visibilities, :project_id, false
  end

  def populate_projects_and_relations!
    Source.pluck(:id, :organization_id, :name).each do |source_id, org_id, source_name|
      r = execute <<~SQL.squish
        INSERT INTO projects (organization_id, name, created_at, updated_at)
        VALUES (#{org_id}, '#{ActiveRecord::Base.connection.quote_string(source_name)}', NOW(), NOW())
        RETURNING id
      SQL

      project_id = r.values.flatten.first

      execute <<~SQL.squish
        UPDATE sources
        SET project_id = #{project_id}
        WHERE id = #{source_id}
      SQL

      execute <<~SQL.squish
        UPDATE visibilities
        SET project_id = #{project_id}
        WHERE source_id = #{source_id}
      SQL
    end
  end

  def delete_dangling_visibilities!
    source_ids = Source.pluck :id

    execute <<~SQL.squish
      DELETE FROM visibilities
      WHERE source_id NOT IN (#{source_ids.join(',')})
    SQL
  end
end
