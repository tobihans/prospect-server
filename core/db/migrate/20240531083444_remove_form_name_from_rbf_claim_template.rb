# frozen_string_literal: true

class RemoveFormNameFromRbfClaimTemplate < ActiveRecord::Migration[7.1]
  def change
    remove_column :rbf_claim_templates, :form_name, :string
  end
end
