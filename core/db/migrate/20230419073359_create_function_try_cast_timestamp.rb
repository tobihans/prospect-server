# frozen_string_literal: true

class CreateFunctionTryCastTimestamp < ActiveRecord::Migration[7.0]
  def change
    create_function :try_cast_timestamp
  end
end
