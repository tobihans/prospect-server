# frozen_string_literal: true

class AddSHSDataTable < ActiveRecord::Migration[7.0]
  def change
    create_enum :gender, %w[M F O]

    # rubocop:disable Naming/VariableNumber
    create_table :data_shs do |t|
      t.string :device_external_id, null: false, index: true
      t.string :account_external_id, null: false, index: true
      t.string :serial_number, null: false, index: true

      t.date :purchase_date, null: false
      t.date :repossession_date
      t.date :paid_off_date

      t.string :manufacturer, null: false
      t.float :pv_power_w
      t.float :battery_energy_wh
      t.float :voltage_v
      t.float :rated_power_w
      t.bigint :interval_seconds
      t.boolean :is_test # rubocop:disable Rails/ThreeStateBooleanColumn
      t.string :primary_use

      t.integer :payment_plan_days
      t.float :payment_plan_down_payment
      t.float :payment_plan_amount_financed
      t.string :payment_plan_type
      t.string :payment_plan_currency

      t.string :customer_external_id, null: false, index: true
      t.string :customer_name_p
      t.string :customer_category
      t.string :customer_sub_category
      t.enum :customer_gender, enum_type: :gender
      t.integer :customer_birth_year
      t.string :customer_former_electricity_source
      t.integer :customer_household_size
      t.string :customer_profession
      t.string :customer_address_p
      t.string :customer_country
      t.float :customer_latitude_b
      t.float :customer_longitude_b
      t.float :customer_latitude_p
      t.float :customer_longitude_p
      t.string :customer_email_p
      t.string :customer_phone_p
      t.string :customer_location_area_1
      t.string :customer_location_area_2
      t.string :customer_location_area_3
      t.string :customer_location_area_4

      t.string :seller_external_id, index: true
      t.string :seller_name_p
      t.enum :seller_gender, enum_type: :gender
      t.string :seller_address_p
      t.string :seller_country
      t.float :seller_latitude_b
      t.float :seller_longitude_b
      t.float :seller_latitude_p
      t.float :seller_longitude_p
      t.string :seller_email_p
      t.string :seller_phone_p
      t.string :seller_location_area_1
      t.string :seller_location_area_2
      t.string :seller_location_area_3
      t.string :seller_location_area_4

      t.string :uid, null: false, index: true, unique: true
      t.string :device_uid, null: false, index: true
      t.string :customer_uid, null: false, index: true
      t.string :account_uid, null: false, index: true

      # default stuff
      t.timestamps null: false
      t.jsonb :custom, null: false, default: {}
      t.references :source, null: false, index: true
      t.references :import, null: false, index: true
      t.references :organization, null: false, index: true
      t.float :trust_score
      t.string :data_origin, null: false
    end
    # rubocop:enable Naming/VariableNumber
  end
end
