# frozen_string_literal: true

class AddDetailsToVisibilities < ActiveRecord::Migration[7.0]
  def change
    change_table :visibilities, bulk: true do |t|
      t.datetime :data_from
      t.datetime :data_until
      t.boolean :resharing, default: false, null: false
      t.string :license
      t.jsonb :columns, default: "{}", null: false
      t.jsonb :conditions, default: "{}", null: false
    end
  end
end
