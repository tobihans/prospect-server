# frozen_string_literal: true

# we assume this migration run already on all systems.
# New system will just jump over this migration because OrganizationUser has no role attribute anymore.
#
class MigrateManagers < ActiveRecord::Migration[7.1]
  def up
    return unless OrganizationUser.attribute_method? :role

    # grants a manager all the currently defined privileges
    OrganizationUser.where(role: :manager).find_each do |ou|
      ::Current.user = ou.user # used as creator in add_privilege
      ou.add_manager_privileges
    end

    # a normal user gets only a subset of privileges
    OrganizationUser.where(role: :user).find_each do |ou|
      ::Current.user = ou.user # used as creator in add_privilege
      ou.add_default_privileges
    end
  end

  def down
    return unless OrganizationUser.attribute_method? :role

    # this is not exactly a rollback, you might loose or add privileges when doing rollback&migrate
    OrganizationUser.find_each do |ou|
      Privilege::PRIVILEGES.each { |p| ou.remove_privilege p }
    end
  end
end
