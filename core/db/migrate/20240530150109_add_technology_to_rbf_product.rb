# frozen_string_literal: true

class AddTechnologyToRbfProduct < ActiveRecord::Migration[7.1]
  def change
    add_column :rbf_products, :technology, :string
  end
end
