# frozen_string_literal: true

class AddEncryptedFieldsToMetersTable < ActiveRecord::Migration[7.0]
  def change
    change_table :data_meters, bulk: true do |t|
      t.string :customer_name_e
      t.string :customer_address_e
      t.string :customer_phone_e
      t.string :customer_email_e
      t.string :customer_latitude_e
      t.string :customer_longitude_e
    end
  end
end
