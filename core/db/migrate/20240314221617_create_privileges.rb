# frozen_string_literal: true

class CreatePrivileges < ActiveRecord::Migration[7.1]
  def change
    create_table :privileges do |t|
      t.string :name
      t.references :user, null: false # reference to Creator
      t.references :organizations_user, null: false

      t.timestamps
    end
    add_index :privileges, :name
  end
end
