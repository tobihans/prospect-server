# frozen_string_literal: true

class AddLastImportedHintToImports < ActiveRecord::Migration[7.0]
  def change
    add_column :imports, :last_imported_hint, :string
  end
end
