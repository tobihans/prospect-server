# frozen_string_literal: true

class CreateOrganizationGroups < ActiveRecord::Migration[7.1]
  def change
    create_table :organization_groups do |t|
      t.string :name, null: false
      t.bigint :user_id
      t.timestamps

      t.index :name, unique: true
    end

    create_table :organization_group_members do |t|
      t.bigint :organization_group_id, null: false, index: true
      t.bigint :organization_id, null: false, index: true
      t.boolean :visible_for_sharing, null: false, default: false
      t.bigint :user_id
      t.timestamps

      t.index %i[organization_group_id organization_id], unique: true
    end
  end
end
