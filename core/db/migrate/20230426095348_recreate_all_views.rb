# frozen_string_literal: true

class RecreateAllViews < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.after_migration!
  end
end
