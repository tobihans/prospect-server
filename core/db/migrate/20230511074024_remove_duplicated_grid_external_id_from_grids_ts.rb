# frozen_string_literal: true

class RemoveDuplicatedGridExternalIdFromGridsTs < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.migration_without_views :grids_ts do
      remove_column :data_grids_ts, :grid_external_id, :string
    end
  end
end
