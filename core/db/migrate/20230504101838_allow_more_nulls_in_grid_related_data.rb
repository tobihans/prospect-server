# frozen_string_literal: true

class AllowMoreNullsInGridRelatedData < ActiveRecord::Migration[7.0]
  def change
    change_column_null :data_grids, :operator_company, true
    change_column_null :data_grids_ts, :grid_external_id, true
  end
end
