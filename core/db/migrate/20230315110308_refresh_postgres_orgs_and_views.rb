# frozen_string_literal: true

class RefreshPostgresOrgsAndViews < ActiveRecord::Migration[7.0]
  def change
    Postgres::RemoteControl.setup_all_organizations!
    Postgres::RemoteControl.create_all_views!
  rescue
    say <<~TXT
      Failed to re-setup the organizations and views. Please run manually:
        Postgres::RemoteControl.setup_all_organizations!
        Postgres::RemoteControl.create_all_views!
    TXT
  end
end
