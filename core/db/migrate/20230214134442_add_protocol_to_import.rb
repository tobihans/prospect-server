# frozen_string_literal: true

class AddProtocolToImport < ActiveRecord::Migration[7.0]
  def change
    add_column :imports, :protocol, :text
  end
end
