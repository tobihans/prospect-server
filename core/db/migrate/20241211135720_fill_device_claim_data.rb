# frozen_string_literal: true

class FillDeviceClaimData < ActiveRecord::Migration[7.1]
  def up
    result = { limited: 0, all: 0 }

    RbfDeviceClaim.where("claimed_payments_data = '[]'").find_each do |rdc|
      Rails.logger.debug { "Converting #{rdc.id} / #{rdc.subject_uid}" }

      payments_data = payments_for_device_data rdc.claimed_devices_data
      required_sum = price rdc

      pd_limited = payments_data.select { _1["created_at"] <= rdc.created_at }

      if payment_sum(pd_limited) >= required_sum
        Rails.logger.debug "Adding payments which were created before claim creation"
        rdc.update_columns claimed_payments_data: pd_limited # rubocop:disable Rails/SkipsModelValidations
        result[:limited] += 1
      else
        Rails.logger.debug "Adding all payments"
        rdc.update_columns claimed_payments_data: payments_data # rubocop:disable Rails/SkipsModelValidations
        result[:all] += 1
      end
    end

    Rails.logger.debug result
  rescue => e
    Rails.logger.debug "ERROR: we failed to post-fill the payments data of existing device claims."
    Sentry.capture_exception e
  end

  def down
    Rails.logger.debug "Nothing to see here... This was just an irreversible data migration"
  end

  private

  def payments_for_device_data(device_data)
    account_uid = device_data["account_uid"]
    Data::PaymentsTs.where(account_uid:).order(paid_at: :asc).map(&:attributes)
  end

  def price(rdc)
    if rdc.claimed_devices_data["payment_plan_type"] == "paygo"
      rdc.claimed_devices_data["payment_plan_down_payment"]
    else
      rdc.claimed_trust_trace_data["custom"]["subsidy__subsidized_sales_price"]
    end
  end

  def payment_sum(payments_data)
    payments_data.pluck("amount").sum
  end
end
