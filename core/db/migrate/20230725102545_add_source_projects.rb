# frozen_string_literal: true

class AddSourceProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.integer :organization_id, null: false
      t.string :name, null: false
      t.string :description
      t.timestamps null: false
    end

    add_index :projects, :organization_id
  end
end
