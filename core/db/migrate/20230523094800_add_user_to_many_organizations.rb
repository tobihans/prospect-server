# frozen_string_literal: true

class AddUserToManyOrganizations < ActiveRecord::Migration[7.0]
  def up
    create_join_table :users, :organizations do |t|
      t.index :user_id
      t.index :organization_id
      t.string :role, default: "user", null: false
      t.datetime :activated_at
      t.index %i[user_id organization_id], unique: true
    end

    execute "INSERT INTO organizations_users(user_id, organization_id, activated_at, role) SELECT id, organization_id, NOW(), role FROM users"

    remove_column :users, :role
  end

  def down
    add_column :users, :role, :string, default: "user"

    execute "UPDATE users SET role = org.role FROM organizations_users AS org WHERE org.user_id = users.id"

    drop_join_table :users, :organizations
  end
end
