# frozen_string_literal: true

class DropOrganizationIdFromSources < ActiveRecord::Migration[7.0]
  def change
    remove_index :sources, column: %w[organization_id name], unique: true
    remove_column :sources, :organization_id, :integer, null: false
    add_index :sources, %w[name project_id], unique: true
  end
end
