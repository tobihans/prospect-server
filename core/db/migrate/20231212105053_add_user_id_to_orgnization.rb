# frozen_string_literal: true

class AddUserIdToOrgnization < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :user_id, :integer
    add_index :organizations, :user_id
  end
end
