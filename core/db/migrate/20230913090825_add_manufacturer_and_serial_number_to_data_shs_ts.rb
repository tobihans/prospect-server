# frozen_string_literal: true

class AddManufacturerAndSerialNumberToDataSHSTs < ActiveRecord::Migration[7.0]
  def change
    change_table :data_shs_ts, bulk: true do |t|
      t.string :serial_number
      t.string :manufacturer
    end

    reversible do |direction|
      direction.up do
        say_with_time "Filling Manufacturer and Serial Numbers" do
          fill_manufacturer_and_serial_number
        end
      end
    end

    change_column_null :data_shs_ts, :serial_number, false
    change_column_null :data_shs_ts, :manufacturer, false
  end

  def fill_manufacturer_and_serial_number
    uids = execute("SELECT DISTINCT device_uid FROM data_shs_ts").values.flatten
    uids.each do |uid|
      _org_id, man, ser = uid.split "_"
      say "Updating #{uid} -> #{man} / #{ser}"

      q = <<~SQL.squish
        UPDATE data_shs_ts
        SET manufacturer = '#{man}', serial_number = '#{ser}'
        WHERE device_uid = '#{uid}'
      SQL

      execute q
    end
  end
end
