# frozen_string_literal: true

class AddTrustTraceTable < ActiveRecord::Migration[7.1]
  def change
    create_enum :trust_trace_subject_origin, %w[grids meters shs payments_ts]

    create_table :data_trust_trace do |t|
      t.timestamps
      t.jsonb :custom, default: {}, null: false
      t.string :data_origin, null: false
      t.bigint :import_id, null: false, index: true
      t.bigint :last_import_id, index: true
      t.bigint :last_source_id, index: true
      t.bigint :organization_id, null: false, index: true
      t.bigint :source_id, null: false, index: true
      t.float :trust_score

      t.string :uid, null: false
      t.string :subject_uid, null: false, index: true
      t.enum :subject_origin, enum_type: :trust_trace_subject_origin, null: false, index: true
      t.string :check, null: false, index: true
      t.string :result, null: false, index: true

      t.index :uid, unique: true
    end
  end
end
