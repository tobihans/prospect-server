# frozen_string_literal: true

class OrganizationsUsers < ActiveRecord::Migration[7.1]
  #
  # Adding a proper PK column to join table (Before it was a composed PK).
  # Needing id to attach other relations to the join table.
  #
  def change
    add_column :organizations_users, :id, :primary_key # rubocop:disable Rails/DangerousColumnNames
  end
end
