# frozen_string_literal: true

class AddStellarOrgMap < ActiveRecord::Migration[7.0]
  def up
    create_table "dataworker_stellar_org_to_meter_map", id: false do |t|
      t.references :organization, default: 1
      t.integer :meter_id, primary_key: true
    end
  end

  def down
    drop_table "dataworker_stellar_org_to_meter_map"
  end
end
