# frozen_string_literal: true

require "rails_helper"

describe SessionTrimWorker do
  it "trims old sessions" do
    create :session, updated_at: 31.days.ago
    expect { described_class.new.perform }.to change(ActiveRecord::SessionStore::Session, :count).by(-1)
  end
end
