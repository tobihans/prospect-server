# frozen_string_literal: true

require "rails_helper"

describe ImportPostProcessingWorker do
  let(:import) { create :import }
  let(:finished_import) { create :import, :finished }

  it "has class method to enqueue for an import" do
    expect(described_class).
      to receive(:perform_in).with(5.seconds, import.id, "rbf_easp_is_eligible_per_import", 0)

    described_class.enqueue_for_rbf_import import
  end

  it "pushes rbf job if import has processing finished" do
    expect(finished_import).to be_processing_finished

    expect(FaktoryPusher).to receive(:push)

    described_class.new.perform finished_import.id, "job_name", 0
  end

  it "enqueues again with increased polling count when not processing finished" do
    expect(described_class).
      to receive(:perform_in).with(5.seconds, import.id, "job_name", 1)

    described_class.new.perform import.id, "job_name", 0
  end

  it "enqueues again with increased polling count and biger delay when not processing finished" do
    expect(described_class).
      to receive(:perform_in).with(10.seconds, import.id, "job_name", 2)

    described_class.new.perform import.id, "job_name", 1
  end

  it "raises an error when MAX_POLLING_COUNT is reached" do
    expect do
      described_class.new.perform import.id, "job_name", 50
    end.to raise_error(/MAX_POLLING_COUNT reached/)
  end
end
