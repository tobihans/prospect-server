# frozen_string_literal: true

require "rails_helper"

describe GrafanaPreviewWorker do
  let(:organization) { create :organization }

  it "trigger preview service" do
    empty_png = [137, 80, 78, 71, 13, 10, 26, 10].pack("C*")
    org = create :organization
    allow(Grafana::DashboardPreview).to receive(:create_grafana_dashboard_preview).and_return(empty_png)
    dbs = { "General" =>
      [{ id: 4,
         uid: "UID",
         title: "Some Title",
         url: "/some/url" }] }
    allow(Grafana::RemoteControl).to receive(:organization_dashboards).and_return(dbs)

    described_class.new.perform(organization.id, "/some/url", "UID")

    expect(Dir["#{Grafana::DashboardPreview::PATH}/#{org.grafana_org_id}_UID.png"].count).to eq 1
  end
end
