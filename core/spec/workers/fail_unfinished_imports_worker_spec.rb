# frozen_string_literal: true

require "rails_helper"

describe FailUnfinishedImportsWorker do
  let(:old_unfinished_import) { create :import, created_at: 3.days.ago, processing_finished_at: nil }
  let(:old_finished_import) { create :import, created_at: 3.days.ago, processing_finished_at: Time.zone.now }
  let(:new_unfinished_import) { create :import, processing_finished_at: Time.zone.now }

  [true, false].each do |unlimited|
    it "puts error on old unfinished imports when called with unlimited #{unlimited}" do
      expect([old_unfinished_import, old_finished_import, new_unfinished_import]).not_to include(be_failed)

      described_class.new.perform(unlimited)

      [old_unfinished_import, old_finished_import, new_unfinished_import].each(&:reload)

      expect([old_finished_import, new_unfinished_import]).not_to include(be_failed)
      expect(old_unfinished_import).to be_failed
      expect(old_unfinished_import.error).to eq "Stale import which never managed to finish"
    end
  end
end
