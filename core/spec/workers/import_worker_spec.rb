# frozen_string_literal: true

require "rails_helper"

describe ImportWorker do
  let(:source) { create :source }

  it "triggers the import of the source" do
    source_dbl = instance_double Source
    expect(source_dbl).to receive(:trigger_import!).with(nil, use_db_import_hint: true, import_hint: nil)
    allow(Source).to receive(:find).and_return(source_dbl)

    described_class.new.perform("source_id" => source.id)
  end

  it "triggers import with params" do
    source_dbl = instance_double Source
    expect(source_dbl).to receive(:trigger_import!).with(nil, use_db_import_hint: false, import_hint: "start_here")
    allow(Source).to receive(:find).and_return(source_dbl)

    described_class.new.perform("source_id" => source.id, "use_db_import_hint" => false, "import_hint" => "start_here")
  end
end
