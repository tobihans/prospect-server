# frozen_string_literal: true

require "rails_helper"

describe DefaultDashboardUpdateWorker do
  it "update default dashboards" do
    create :organization

    allow(Grafana::RemoteControl).to receive_messages(default_dashboards_for_organization!: true, delete_default_dashboards!: true)

    expect(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).at_least(1).time

    described_class.new.perform
  end
end
