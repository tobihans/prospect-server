# frozen_string_literal: true

require "rails_helper"

describe PullBlobWorker do
  let(:import) { create :import }
  let(:blob) { EncryptedBlob.create!(import:) }

  it "pulls blob" do
    request_object = HTTParty::Request.new Net::HTTP::Get, "/"
    empty_png = [137, 80, 78, 71, 13, 10, 26, 10].pack("C*")
    response_object = Net::HTTPOK.new("1.1", 200, "OK")
    response = HTTParty::Response.new(request_object, response_object, -> {}, { body: empty_png })
    allow(HTTParty).to receive(:get).and_return(response)

    described_class.new.perform("http://example.com/text.png", blob.id)
    blob.reload

    expect(blob.blob).to be_attached
    expect(blob.blob.download).not_to be_nil
    expect(blob.error).to be_nil
  end

  it "encrypts url on failed request" do
    request_object = HTTParty::Request.new Net::HTTP::Get, "/"
    response_object = Net::HTTPNotFound.new("1.1", 404, "NOT FOUND")
    response = HTTParty::Response.new(request_object, response_object, -> {}, { body: "" })
    allow(HTTParty).to receive(:get).and_return(response)

    described_class.new.perform("http://example.com/text.png", blob.id)
    blob.reload

    expect(blob.blob).not_to be_attached
    expect(blob.url_e.length).to be > 0
    expect(blob.error).to eq "Received Error Code: 404 while fetching data"
  end

  it "catches exception" do
    allow(HTTParty).to receive(:get).and_raise(Timeout::Error)

    described_class.new.perform("http://example.com/text.png", blob.id)
    blob.reload

    expect(blob.blob).not_to be_attached
    expect(blob.url_e.length).to be > 0
    expect(blob.error).to eq "Timeout::Error"
  end
end
