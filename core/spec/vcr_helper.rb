# frozen_string_literal: true

VCR.configure do |config|
  config.cassette_library_dir = "spec/vcr_cassettes"
  config.hook_into :webmock
  config.ignore_localhost = true
  config.ignore_request do |request|
    # HACK: somehow VCR gets in the way of typhoeus stubbing
    # here we explicitly tell VCR to let those requests pass
    # we are quite specific, so that chances are low to accidently let requests pass
    uri = URI(request.uri)
    uri.host == "stellar.newsunroad.com" && uri.path == "/api/v0/rawData/a2ei_0123"
  end
end
