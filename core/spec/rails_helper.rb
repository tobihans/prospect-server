# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
require "spec_helper"
require "factory_bot"

ENV["RAILS_ENV"] = "test" if ["development", "", nil].include? ENV["RAILS_ENV"]

require_relative "../config/environment"
require File.expand_path("../config/environment", __dir__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require "rspec/rails"
# Add additional requires below this line. Rails is not loaded until this point!
require_relative "support/sorcery_test_helper_rails"
require_relative "support/capybara_chrome"
require_relative "vcr_helper"
require_relative "support/faktory"

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  ActiveRecord::Migration.maintain_test_schema!
  Postgres::RemoteControl.install_fn_org_role!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end
RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  # future format: config.fixture_paths << "#{Rails.root}/spec/fixtures"
  config.fixture_path = Rails.root.join "/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # You can uncomment this line to turn off ActiveRecord support entirely.
  # config.use_active_record = false

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, type: :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
  config.include FactoryBot::Syntax::Methods

  config.include ActiveJob::TestHelper

  config.include Sorcery::TestHelpers::Rails, type: :feature

  # Sorcery helpers
  config.include Sorcery::TestHelpers::Rails::Controller, type: :controller
  config.include Sorcery::TestHelpers::Rails::Request, type: :request
  config.include Sorcery::TestHelpers::Rails::Integration, type: :feature

  config.before do
    ActiveJob::Base.queue_adapter.enqueued_jobs.clear
    ActiveJob::Base.queue_adapter.performed_jobs.clear
  end
end

FactoryBot::SyntaxRunner.class_eval do
  include RSpec::Mocks::ExampleMethods
end

## give me a hash or named args and i will insert data into db table(s):
#  table_name: [
#     {col: "string1",  int_col: 5},
#     {col: "string 2", time_col: {raw: "NOW()"}}
#   ], ...
#  will create:
#   INSERT INTO table_name (col, int_col, time_col)
#   VALUES ('string1', 5, NULL), ('string 2', NULL, NOW())
#
# Use Just for test, not much sanitation.
# Strings will automatically be quoted. If you don't want that just pass it as {raw: "STRING"}
# You dont need to declare NULL values
#
def raw_db_insert(hash = nil, **args)
  data = hash || args
  raise "need some data for insertion" if data.empty?

  qs = data.map do |table, rows|
    columns = rows.map(&:keys).flatten.uniq

    values = rows.map do |row|
      columns.map do |c|
        v = row[c]
        if v.nil?
          "NULL"
        elsif c == "uid"
          sql_quote "The UUID - #{rand(10_000)}}" # avoid duplication
        elsif [String, Time].any? { v.is_a? _1 }
          sql_quote v.to_s
        elsif v.is_a?(Hash) && v.key?(:raw)
          v[:raw]
        elsif c == "id"
          rand(10_000) # avoid duplication
        else
          v
        end
      end
    end

    # make sure they are always column names!
    columns.map! { "\"#{_1}\"" }

    <<~SQL.squish
      INSERT INTO #{table}#{' '}
        (#{columns.join(', ')})
      VALUES
        #{values.map { "(#{_1.join(', ')})" }.join(', ')}
    SQL
  end
  qs.each { ActiveRecord::Base.connection.execute _1 }
end

def sql_quote(val)
  quoted = ActiveRecord::Base.connection.quote_string val.to_s
  "'#{quoted}'"
end

# Creates one row of test data in specified data_category.
# @import: used to set correct context for data (organization_id, etc.)
#
def test_example_insert(import, data_category)
  dt = Postgres::DataTable.new data_category

  view = Naming.db_data_table  data_category

  table_columns = {}

  # Add columns with example value. The example depends on the type (blurred) etc. of the column
  dt.columns.each do |c|
    c.sql_columns_hint.each do |c_name, c_type|
      example = case c_type
                when :blurred
                  1.123
                when :encrypted
                  "encrypted #{c.name}"
                when :pseudonymized
                  "p14n #{c.name}"
                else
                  c.example
                end

      table_columns[c_name] = example
    end
  end

  # take context from import and overwrite example values
  table_columns["organization_id"] &&= import.organization.id
  table_columns["import_id"] &&= import.id
  table_columns["source_id"] &&= import.source.id

  table_columns["created_at"] &&= DateTime.now.to_fs :db
  table_columns["updated_at"] &&= DateTime.now.to_fs :db

  raw_db_insert view => [table_columns]
end
