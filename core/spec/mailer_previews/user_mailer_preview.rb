# frozen_string_literal: true

class UserMailerPreview < ActionMailer::Preview
  def welcome
    UserMailer.with(user: User.last).welcome
  end

  def invite
    u = User.last
    u.invite_code ||= "invite_code"

    UserMailer.with(user: u, inviting_user: User.first).invite
  end

  def organization_invite
    UserMailer.with(inviting_user: User.first, user: User.last, organization: Organization.first).organization_invite
  end

  def reset_password_email
    u = User.first
    u.reset_password_token = "PrEvIeWrEsEtPaSsWoRdToKen"
    UserMailer.reset_password_email u
  end

  def reset_password_other_auth
    UserMailer.reset_password_other_auth User.first
  end
end
