# frozen_string_literal: true

require "rails_helper"

describe PasswordResetsController do
  let(:user) { create :user }

  render_views

  describe "#new" do
    it "renders the reset password form" do
      get :new
      expect(response).to have_http_status :ok
      expect(response.body).to include "Reset Password"
    end
  end

  describe "#create" do
    describe "password auth" do
      it "sends mail with password reset instructions" do
        expect do
          post :create, params: { email: user.email }
        end.to have_enqueued_job.with "UserMailer", "reset_password_email", any_args
      end

      it "notifies about email being send out" do
        post :create, params: { email: user.email }
        expect(flash[:notice]).to include "Instructions have been sent to"
      end

      it "creates password reset token" do
        expect(user.reset_password_token).to be_nil
        post :create, params: { email: user.email }

        user.reload
        expect(user.reset_password_token).to be_present
      end

      it "creates password reset token expiry" do
        expect(user.reset_password_token_expires_at).to be_nil
        post :create, params: { email: user.email }

        user.reload
        expect(user.reset_password_token_expires_at).to be_within(1.hour).of(1.day.after)
      end
    end

    describe "alternative authentication" do
      let(:user) { create :user, :google_auth }

      it "send mail to inform about alternative authentiation method" do
        expect do
          post :create, params: { email: user.email }
        end.to have_enqueued_job.with "UserMailer", "reset_password_other_auth", any_args
      end

      it "does not create reset token nor expiry" do
        expect(user.reset_password_token_expires_at).to be_nil
        expect(user.reset_password_token).to be_nil

        post :create, params: { email: user.email }

        user.reload
        expect(user.reset_password_token_expires_at).to be_nil
        expect(user.reset_password_token).to be_nil
      end
    end

    describe "unknown email" do
      it "does not deliver no mail" do
        expect do
          post :create, params: { email: "un@knwo.wn" }
        end.not_to have_enqueued_job
      end

      it "gives same flash message as if the mail would exist" do
        post :create, params: { email: "un@knwo.wn" }
        expect(flash[:notice]).to include "Instructions have been sent to"
      end
    end
  end

  describe "#edit" do
    let(:user) { create :user, reset_password_token: "abcdefg" }

    it "does not work for wrong token" do
      get :edit, params: { id: "unknowntoken" }
      expect(response).not_to have_http_status :ok
    end

    it "renders for form password change" do
      get :edit, params: { id: user.reset_password_token }
      expect(response).to have_http_status :ok
      expect(response.body).to include "New Password for"
    end
  end

  describe "#update" do
    let!(:user) { create :user, reset_password_token: "abcdefg" }
    let(:random_pw) { SecureRandom.hex }

    it "sets a new password" do
      params = { id: "abcdefg", user: { password: random_pw, password_confirmation: random_pw } }
      expect do
        put(:update, params:)
      end.to(change { User.find(user.id).crypted_password })
    end

    it "notifies about updated password" do
      params = { id: "abcdefg", user: { password: random_pw, password_confirmation: random_pw } }
      put(:update, params:)
      expect(flash[:notice]).to include "Password successfully updated."
    end

    it "nils password reset info" do
      params = { id: "abcdefg", user: { password: random_pw, password_confirmation: random_pw } }
      put(:update, params:)

      expect(user.reload.reset_password_token).to be_nil
      expect(user.reload.reset_password_token_expires_at).to be_nil
    end

    it "fails and re-renders and warns about too short password" do
      params = { id: "abcdefg", user: { password: "a", password_confirmation: "a" } }

      expect do
        put(:update, params:)
      end.not_to(change { User.find(user.id).crypted_password })

      expect(response.body).to include "Password is too short"
    end

    it "fails to set new password if confirmations does not match" do
      params = { id: "abcdefg", user: { password: random_pw, password_confirmation: SecureRandom.hex } }

      expect do
        put(:update, params:)
      end.not_to(change { User.find(user.id).crypted_password })

      expect(response.body).to include "Password confirmation doesn"
    end
  end
end
