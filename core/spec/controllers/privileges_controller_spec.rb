# frozen_string_literal: true

require "rails_helper"

describe PrivilegesController do
  let(:user) { create :user, privileges: ["user.manage"] }
  let(:ou) { user.current_organization_user }
  let(:privilege) { create :privilege, organization_user: ou }

  render_views

  before { login_user user }

  it "#index" do
    get :index, params: { organization_user_id: ou.id }
    expect(response).to have_http_status :ok

    Privilege::PRIVILEGES.each do |p|
      expect(response.body).to include p
    end
  end

  it "#update" do
    put :update, params: { organization_user_id: ou.id, privileges: [Privilege::PRIVILEGES.first, Privilege::PRIVILEGES.third] }

    ou.reload

    expect(ou.privileges.length).to eq 2
    expect(ou.privilege?(Privilege::PRIVILEGES.first)).to be true
    expect(ou.privilege?(Privilege::PRIVILEGES.third)).to be true
  end

  it "#update remove privileges" do
    ou.privileges.create name: Privilege::PRIVILEGES.second, user: (create :user)
    ou.save

    put :update, params: { organization_user_id: ou.id, privileges: [Privilege::PRIVILEGES.third] }

    # expect that the second is deleted, and the third added
    ou.reload

    expect(ou.privileges.length).to eq 1
    expect(ou.privilege?(Privilege::PRIVILEGES.third)).to be true
  end
end
