# frozen_string_literal: true

require "rails_helper"

describe SourcesController do
  let(:admin) { create :user, :admin }

  let(:source) do
    create :source, data_category: "test", kind: "manual_upload",
                    secret: "top_secret123", details: { file_format: "csv" },
                    name: "Some crazy name", organization: admin.organization
  end

  render_views

  before do
    login_user admin
  end

  it "#show" do
    get :show, params: { id: source.id }
    expect(response.body).to match(source.name)
    expect(response.body).to match("Created At")
  end

  it "#destroys clean source" do
    get :destroy, params: { id: source.id }
    expect(Source.find_by(id: source.id)).to be_nil
  end

  it "#destroys also when source has already imports" do
    create(:import, source:)
    get :destroy, params: { id: source.id }
    expect(Source.find_by(id: source.id)).not_to be_present
  end

  it "#show_secret visible" do
    get :show_secret, params: { source_id: source.id, visible: true }
    expect(response.body).to match("top_secret123")
  end

  it "#show_secret invisible" do
    get :show_secret, params: { source_id: source.id, visible: false }
    expect(response.body).not_to match("top_secret123")
  end

  describe "#manual_upload" do
    it "prevents upload to non manual_upload source" do
      source.update! kind: "s3_bucket"

      post :manual_upload, params: { source_id: source.id }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end

    it "complains about missing data" do
      post :manual_upload, params: { source_id: source.id }
      expect(flash[:alert]).to eq("No data provided.")
    end

    it "complains about failed import" do
      f = Rack::Test::UploadedFile.new Rails.root.join "spec/fixtures/data_table_test_example_broken.csv"

      post :manual_upload, params: { source_id: source.id, source: { file: f } }
      expect(flash[:alert]).to eq("Could not ingest your data.")
    end

    it "can successfully import the data" do
      f = Rack::Test::UploadedFile.new Rails.root.join "spec/fixtures/data_table_test_example.csv"
      post :manual_upload, params: { source_id: source.id, source: { file: f } }
      expect(flash[:notice]).to include "Data uploaded."
    end

    it "can also import the data from an excel file" do
      source.details[:file_format] = "xlsx"
      source.save!

      f = Rack::Test::UploadedFile.new Rails.root.join "spec/fixtures/data_table_test_example.xlsx"
      post :manual_upload, params: { source_id: source.id, source: { file: f } }
      expect(flash[:notice]).to include "Data uploaded."
    end
  end

  describe "recorded upload" do
    let(:recorded_source) { create :source, kind: "recorded_upload/airtel_zm" }
    let(:valid_code) { VerifiedCode.generate "#{recorded_source.id} 0", expires_in: 1.day }

    describe "#public_recorded" do
      it "works with correct code" do
        get :public_recorded, params: { code: valid_code }
        expect(response).to have_http_status :ok
        expect(response.body).to include "Recorded Upload for"
      end

      it "fails with invalid code" do
        get :public_recorded, params: { code: "12345" }
        expect(response).to have_http_status :bad_request
      end

      it "fails with code with invalid source id" do
        code = VerifiedCode.generate("#{Source.last&.id.to_i + 1} 0", expires_in: 1.day)

        get :public_recorded, params: { code: }
        expect(response).to have_http_status :bad_request
      end

      it "fails with code with invalid last import id" do
        i = create :import, source: recorded_source
        code = VerifiedCode.generate "#{recorded_source.id} #{i.id - 1}", expires_in: 1.day

        get :public_recorded, params: { code: }

        expect(response).to have_http_status :bad_request
      end
    end

    describe "#public recorded upload" do
      it "works and saves file and recording" do
        import_count = Import.count

        post :public_recorded_upload, params: { file: fixture_file_upload("airtel_zm.csv"),
                                                recording: fixture_file_upload("recording.webm"),
                                                code: valid_code }

        expect(response).to have_http_status :ok
        expect(response.parsed_body["error"]).to be_nil
        expect(Import.count).to eq import_count + 1
        i = Import.last
        expect(i.file).to be_attached
        expect(i.recording).to be_attached
      end

      it "fails for bad code" do
        post :public_recorded_upload, params: { file: fixture_file_upload("airtel_zm.csv"),
                                                recording: fixture_file_upload("recording.webm"),
                                                code: "123456" }
        expect(response).to have_http_status :bad_request
      end

      it "fails for missing data" do
        post :public_recorded_upload, params: { file: nil,
                                                recording: fixture_file_upload("recording.webm"),
                                                code: valid_code }
        expect(response.parsed_body["error"]).to be_present
      end

      it "fails for missing recording" do
        post :public_recorded_upload, params: { file: fixture_file_upload("airtel_zm.csv"),
                                                recording: nil,
                                                code: valid_code }
        expect(response.parsed_body["error"]).to be_present
      end
    end
  end

  describe "#trigger_now" do
    let!(:pull_source) { create :source, kind: "s3_bucket" }

    it "enqueues a worker" do
      source.update! kind: "s3_bucket"
      expect do
        post :trigger_now, params: { source_id: pull_source.id }
      end.to change { Faktory::Queues["imports"].size }.by 1
    end

    it "shows a notice" do
      source.update! kind: "s3_bucket"
      post :trigger_now, params: { source_id: pull_source.id }
      expect(flash[:notice]).to include "Data import triggered to run in the background"
    end

    it "fails for push source" do
      expect(source.adapter).to be_push
      post :trigger_now, params: { source_id: source.id }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end

    it "fails for non admin" do
      user = create :user
      login_user user

      post :trigger_now, params: { source_id: source.id }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end

  it "#backfill" do
    pull_source = create :source, :s3_bucket

    get :backfill, params: { source_id: pull_source.id }

    expect(response.body).to include "Backfill will import"
  end

  describe "#backfill_completely" do
    let!(:pull_source) { create :source, :s3_bucket }

    it "enqueues a worker" do
      expect do
        post :backfill_completely, params: { source_id: pull_source.id }
      end.to change { Faktory::Queues["imports"].size }.by 1
    end

    it "shows a notice" do
      post :backfill_completely, params: { source_id: pull_source.id }
      expect(flash[:notice]).to include "Data import with initial import hint triggered to run in the background"
    end

    it "fails for non admin" do
      user = create :user
      login_user user

      post :backfill_completely, params: { source_id: source.id }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end

  describe "#backfill_from_date" do
    let!(:pull_source) { create :source, :s3_bucket }

    it "enqueues a worker" do
      expect do
        post :backfill_from_date, params: { source_id: pull_source.id, from_date: 1.day.ago }
      end.to change { Faktory::Queues["imports"].size }.by 1
    end

    it "shows a notice" do
      post :backfill_from_date, params: { source_id: pull_source.id, from_date: 1.day.ago }
      expect(flash[:notice]).to include "Data import starting from"
    end

    it "fails for non admin" do
      user = create :user
      login_user user

      post :backfill_from_date, params: { source_id: source.id, from_date: 1.day.ago }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end

  describe "move to another project" do
    let(:another_project) { create :project, organization: source.organization }

    it "shows edit project page" do
      get :edit_project, params: { source_id: source.id }
      expect(response).to have_http_status :ok
      expect(response.body).to include "Move to Project"
    end

    it "can move source to another project" do
      expect do
        post :update_project, params: { source_id: source.id, source: { project_id: another_project.id } }
      end.to(change { source.reload.project_id })
    end

    it "fails moving source to project with source of same name" do
      another_source = create :source, project: another_project
      source.update! name: another_source.name

      expect do
        post :update_project, params: { source_id: source.id, source: { project_id: another_project.id } }
      end.not_to(change { source.reload.project_id })

      expect(response.body).to include "Name has already been taken"
    end

    it "fails to moving to project of another org" do
      login_user create(:user, privileges: ["source.manage"]) # admin can do anything
      foreign_project = create :project
      old_project = source.project
      expect(old_project.organization).not_to eq foreign_project.organization

      post :update_project, params: { source_id: source.id, source: { project_id: foreign_project.id } }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."

      expect(source.reload.project).to eq old_project
    end
  end

  describe "dynamic form access" do
    before do
      logout_user
    end

    describe "public form" do
      let!(:source) { create :source, :rbf_ueccc_ogs }

      it "allows public access without user" do
        get :dynamic_form, params: { slug: source.details["slug"] }

        expect(response).to have_http_status :ok
        expect(response.body).to include "OGS Sales Form"
      end

      it "allows access of logged-in user by for public form" do
        login_user create(:user)
        get :dynamic_form, params: { slug: source.details["slug"] }

        expect(response).to have_http_status :ok
        expect(response.body).to include "OGS Sales Form"
      end
    end

    describe "non-public form" do
      let!(:source) { create :source, :rbf_ueccc_ogs }

      before do
        source.details["public_access"] = false
        source.save!
      end

      it "has proper source public access" do
        expect(source.adapter.try(:public_access?)).to be_falsey
      end

      it "denies public access without user" do
        get :dynamic_form, params: { slug: source.details["slug"] }

        expect(response).to have_http_status :redirect
        expect(flash[:alert]).to match "You are not authorized to perform this action."
      end

      it "denies access for user without priv source.upload_data" do
        login_user create(:user)
        get :dynamic_form, params: { slug: source.details["slug"] }

        expect(response).to have_http_status :redirect
        expect(flash[:alert]).to match "You are not authorized to perform this action."
      end

      it "allows access for user with priv source.upload_data" do
        login_user create(:user, privileges: ["source.upload_data"])
        get :dynamic_form, params: { slug: source.details["slug"] }

        expect(response).to have_http_status :ok
        expect(response.body).to include "OGS Sales Form"
      end
    end
  end

  describe "inactive form" do
    let!(:source) { create :source, :rbf_ueccc_ogs, activated_at: nil }

    it "source is not active" do
      expect(source).not_to be_active
    end

    it "can not be accessed publicly" do
      get :dynamic_form, params: { slug: source.details["slug"] }

      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end

    it "can not be accessed even by admin" do
      login_user admin
      get :dynamic_form, params: { slug: source.details["slug"] }

      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end
end
