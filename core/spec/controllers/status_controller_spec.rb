# frozen_string_literal: true

require "rails_helper"

describe StatusController do
  describe "ping" do
    it "works via GET" do
      allow(controller).to receive_messages(test_postgres: true, test_faktory: true, test_grafana: true, test_minio: true)

      get :ping
      expect(response).to have_http_status :ok
      expect(response.body).to include "PONG : FAKTORY OK | GRAFANA OK | MINIO OK | POSTGRES OK"
    end

    it "works via POST too" do
      allow(controller).to receive_messages(test_postgres: false, test_faktory: false, test_grafana: false, test_minio: false)

      post :ping
      expect(response).to have_http_status 533
      expect(response.body).to include "DEGRADED : FAKTORY DOWN | GRAFANA DOWN | MINIO DOWN | POSTGRES DOWN"
    end

    it "can handle errors while checking" do
      allow(controller).to receive(:test_postgres).and_raise("postgres error")
      allow(controller).to receive(:test_faktory).and_raise("faktory error")
      allow(controller).to receive(:test_grafana).and_raise("grafana error")
      allow(controller).to receive(:test_minio).and_raise("minio error")

      get :ping
      expect(response).to have_http_status 533
      expect(response.body).to include "DEGRADED : FAKTORY ERROR | GRAFANA ERROR | MINIO ERROR | POSTGRES ERROR"
    end

    it "sums up the error codes" do
      allow(controller).to receive(:test_grafana).and_raise("grafana error")
      allow(controller).to receive_messages(test_postgres: true, test_faktory: false, test_minio: true)

      get :ping
      expect(response).to have_http_status 512
      expect(response.body).to include "DEGRADED : FAKTORY DOWN | GRAFANA ERROR | MINIO OK | POSTGRES OK"
    end

    it "works with mocked services" do
      allow(Organization).to receive(:count).and_return(true)
      allow(Faktory::Client).to receive(:new).and_return(OpenStruct.new(close: nil))
      allow(Grafana::RemoteControl).to receive(:client).and_return(OpenStruct.new(current_org: true))
      blob_service = ActiveStorage::Blob.service
      allow(blob_service).to receive(:exist?).with(anything).and_return true
      allow(ActiveStorage::Blob).to receive(:service).and_return(blob_service)

      get :ping
      expect(response).to have_http_status :ok
    end

    it "fails with mocked services" do
      allow(Organization).to receive(:count).and_raise
      allow(Faktory::Client).to receive(:new).and_raise
      allow(Grafana::Client).to receive(:new).and_raise
      blob_service = ActiveStorage::Blob.service
      allow(blob_service).to receive(:exist?).with(anything).and_raise
      allow(ActiveStorage::Blob).to receive(:service).and_return(blob_service)

      get :ping
      expect(response).to have_http_status 533
    end
  end
end
