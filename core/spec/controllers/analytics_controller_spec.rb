# frozen_string_literal: true

require "rails_helper"

describe AnalyticsController do
  let(:admin) { create :user, :admin }

  render_views

  before do
    login_user admin

    mock_db_list = { General: [{ id: 1, uid: "test", title: "Test", uri: "db/test", url: "/d/test/test" }] }

    allow(Grafana::RemoteControl).to receive(:organization_dashboards).and_return mock_db_list
  end

  it "#index works" do
    get :index
    expect(response).to have_http_status(:ok)
  end

  it "#show works" do
    get :show, params: { id: "test" }
    expect(response).to have_http_status(:ok)
  end

  describe "#grafana_new" do
    it "type folder works" do
      get :grafana_new, params: { type: "folder" }
      expect(response).to have_http_status(:ok)
    end

    it "type dashboard works" do
      get :grafana_new, params: { type: "dashboard" }
      expect(response).to have_http_status(:ok)
    end

    it "type import_dashboard works" do
      get :grafana_new, params: { type: "import_dashboard" }
      expect(response).to have_http_status(:ok)
    end

    it "unknown type complains" do
      expect do
        get :grafana_new, params: { type: "something" }
      end.to raise_error "Unknown type something"
    end

    it "missing user rights complains" do
      allow(Grafana::RemoteControl).to receive(:update_user_permission).and_return(true)
      admin.update! admin: false

      get :grafana_new, params: { type: "folder" }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end
end
