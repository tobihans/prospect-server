# frozen_string_literal: true

require "rails_helper"

describe ImportsController do
  let(:import) { create :import }
  let(:admin) { create :user, :admin }

  render_views

  before do
    login_user admin
  end

  it "#show" do
    get :show, params: { id: import.id }
    expect(response.body).to include import.id.to_s
    expect(response).to have_http_status :ok
  end

  it "#downloads" do
    get :download, params: { import_id: import.id }
    expect(response).to have_http_status :ok
  end
end
