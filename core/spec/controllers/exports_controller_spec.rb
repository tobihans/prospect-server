# frozen_string_literal: true

require "rails_helper"

describe ExportsController do
  let!(:user) { create :user, privileges: ["data.download"] }

  render_views

  before { login_user user }

  describe "with available data" do
    let!(:project) { create :project, organization: user.organization }
    let!(:own_source) { create :source, project: }
    let!(:shared_source) { create :source }
    let!(:unrelated_source) { create :source }
    let!(:shared_visibility) do # rubocop:disable RSpec/LetSetup
      create :visibility, project: shared_source.project, organization: user.organization,
                          columns: { data_test: %w[uid] }
    end

    before do
      raw_db_insert data_test: [
        {
          uid: "some_uid_1",
          int_column: 7,
          float_column: 8.9,
          personal_column: "personal name",
          created_at: { raw: "NOW()" },
          updated_at: { raw: "NOW()" },
          source_id: own_source.id,
          custom: '{"some": "value"}',
          data_origin: "api",
          import_id: 1,
          organization_id: 1
        }, {
          uid: "some_uid_2",
          int_column: 7,
          float_column: 8.9,
          personal_column: "personal name",
          created_at: { raw: "NOW()" },
          updated_at: { raw: "NOW()" },
          source_id: shared_source.id,
          custom: '{"some": "value"}',
          data_origin: "api",
          import_id: 1,
          organization_id: 1
        }, {
          uid: "some_uid_3",
          int_column: 7,
          float_column: 8.9,
          personal_column: "personal name",
          created_at: { raw: "NOW()" },
          updated_at: { raw: "NOW()" },
          source_id: unrelated_source.id,
          custom: '{"some": "value"}',
          data_origin: "api",
          import_id: 1,
          organization_id: 1
        }
      ]
    end

    it "#index" do
      get :index
      expect(response.body).to include "2"
    end

    it "#export as csv" do
      get :export, params: { data_category: "test", format: "csv" }
      expect(response.body).to include "int_column"
      expect(response.body).to include "some_uid_1"
      expect(response.body).to include "some_uid_2"
      expect(response.body).not_to include "some_uid_3"
    end

    it "#export limited to source_id of shared source" do
      get :export, params: { data_category: "test", format: "csv", source_id: shared_source.id }
      expect(response.body).to include "int_column"
      expect(response.body).to include "some_uid_2"
      expect(response.body).not_to include "some_uid_1"
      expect(response.body).not_to include "some_uid_3"
    end

    it "#export limited to source_id of own source" do
      get :export, params: { data_category: "test", format: "csv", source_id: own_source.id }
      expect(response.body).to include "int_column"
      expect(response.body).to include "some_uid_1"
      expect(response.body).not_to include "some_uid_2"
      expect(response.body).not_to include "some_uid_3"
    end

    it "#export limited to source_id of unrelated source shows nothing" do
      get :export, params: { data_category: "test", format: "csv", source_id: unrelated_source.id }
      expect(response.body).to include "int_column"
      expect(response.body).not_to include "some_uid_1"
      expect(response.body).not_to include "some_uid_2"
      expect(response.body).not_to include "some_uid_3"
    end

    it "#export as xlsx" do
      get :export, params: { data_category: "test", format: "xlsx" }
      expect(response.headers["Content-Disposition"]).to include "prospect_test.xlsx"
    end

    it "#export fails if neither data_category or custom view" do
      expect { get :export, params: { format: "xlsx" } }.to raise_error(/either data_category or custom_view/)
    end

    it "#export fails for non existing custom view" do
      expect { get :export, params: { custom_view: "non_existing_custom_view", format: "csv" } }.
        to raise_error(/unknown custom_view non_existing_custom_view/)
    end

    it "#export works for custom view" do
      source = create :source, :custom
      shared_cols = %w[uid external_id txt_col ts_col num_col]
      Visibility.create! project: source.project, organization: user.organization,
                         columns: { source.custom_view => shared_cols }

      get :export, params: { custom_view: source.custom_view, format: "csv" }

      shared_cols.each do |col|
        expect(response.body).to include col
      end
    end
  end

  describe "works even in a fresh organization without any sources" do
    before do
      Visibility.find_each(&:destroy!)
    end

    it "#index" do
      get :index
      expect(response).to have_http_status :ok
    end
  end
end
