# frozen_string_literal: true

require "rails_helper"

describe UsersController do
  let(:user) { create :user, privileges: Privilege::TEMPLATES["manager"] }
  let(:admin) { create :user, :admin }

  render_views

  describe "welcome" do
    it "welcome login" do
      get :welcome
      expect(response.body).to match(/Login/)
    end

    it "welcome logged in" do
      login_user user
      get :welcome
      expect(response.body).to match(/Analytics/) # renders same template, but you should see the full header
    end
  end

  describe "with no auth" do
    it "unauthorized" do
      get :me
      expect(response).to have_http_status(:found)
      expect(response.redirect_url).to eq root_url
    end
  end

  describe "#new" do
    it "shows the invite new user form" do
      login_user user
      get :new
      expect(response.body).to match "Invite New User"
    end
  end

  describe "#create" do
    let(:params) do
      {
        user: { email: "new_mail@fresh2000.hey",
                organization_id: user.organization.id,
                priv_template: "viewer" }
      }
    end

    before do
      allow(Grafana::RemoteControl).to receive_messages(create_user: {}, create_organization: { OrgId: 1 }, update_user_permission: {})
      login_user user
    end

    describe "simple case" do
      it "creates a new user" do
        expect { post(:create, params:) }.to change(User, :count).by 1
      end

      it "creates a user with correct properties" do
        post(:create, params:)
        u = User.last
        expect(u).to be_invited
        expect(u.invite_code).to be_present
        expect(u.organization).to eq user.organization
        expect(u.email).to eq "new_mail@fresh2000.hey"
        expect(u).not_to be_admin

        # privileges generated
        expect(u.current_organization_user.privileges.map(&:name)).to match_array(Privilege::VIEWER_PRIVILEGES)
      end

      it "creates a manager - check privs" do
        params[:user][:priv_template] = "manager"
        post(:create, params:)
        u = User.last

        # privileges generated
        expect(u.current_organization_user.privileges.map(&:name)).to match_array(Privilege::PRIVILEGES)
      end

      it "displays that user was invited" do
        post(:create, params:)
        expect(response).not_to have_http_status(:error)
      end

      it "existing user will be invited to organization" do
        params[:user][:email] = user.email
        expect { post(:create, params:) }.not_to change(User, :count)
        expect(response).to have_http_status :found
        expect(flash[:notice]).to match "The invited user is already part of this organization."
      end

      it "normal users should not be allowed to invite users whilst making them admin" do
        Current.user = user
        user.current_organization_user.add_privilege "user.manage"
        expect(user).not_to be_admin

        org2 = create :organization
        user2 = create :user, organization: org2

        params[:user][:email] = user2.email
        params[:user][:organization_id] = user.organization_id
        params[:user][:role] = "manager"
        params[:user][:admin] = true
        post(:create, params:)

        expect(user2.reload).not_to be_admin
      end

      it "normal users should not be allowed to create admin users" do
        Current.user = user
        user.current_organization_user.add_privilege "user.manage"
        expect(user).not_to be_admin

        params[:user][:email] = "different@email.avc"
        params[:user][:organization_id] = user.organization_id
        params[:user][:role] = "manager"
        params[:user][:admin] = true

        expect do
          post(:create, params:)
        end.to change(User, :count).by 1

        expect(User.last).not_to be_admin
      end

      it "failes on invalid user form" do
        params[:user][:role] = "invalid_role"
        params[:user][:email] = ""
        expect { post(:create, params:) }.not_to change(User, :count)
        expect(response).to have_http_status :unprocessable_content
      end
    end

    describe "with user.manage" do
      it "is sure we are a manager" do
        expect(user).not_to be_admin
        expect(user).to be_can("user.manage")
      end

      it "does not immediately create admin users" do
        params[:user][:admin] = "true"
        expect do
          post(:create, params:)
        end.to change(User, :count).by 1
        expect(User.last).not_to be_admin
      end

      it "forbids to add user to a different organization" do
        organization = create :organization
        params[:user][:organization_id] = organization.id

        user_count = User.count

        expect { post(:create, params:) }.
          to raise_error "Only admins can add users to a different organization"
        expect(User.count).to eq user_count
      end
    end
  end

  describe "edit and update" do
    before do
      login_user admin
    end

    it "renders the edit page" do
      get :edit, params: { id: user.id }
      expect(response.body).to include "Edit"
    end

    it "can update the user" do
      post :update, params: { id: user.id, user: { username: "Another Name" } }
      expect(user.reload.username).to eq "Another Name"
    end

    it "also can fail to update the user" do
      post :update, params: { id: user.id, user: { password: "1234", password_confirmation: "12345" } }
      expect(response).to have_http_status :unprocessable_content
      expect(response.body).to include "Password confirmation"
    end
  end

  describe "toggle archive" do
    before do
      login_user admin
    end

    it "deletes an invited user" do
      u = create :user, :invited
      post :toggle_archive, params: { id: u.id }
      expect { u.reload }.to raise_error ActiveRecord::RecordNotFound
    end

    it "archives an active user" do
      u = user
      expect(u).to be_active
      post :toggle_archive, params: { id: u.id }
      expect(u.reload).to be_archived
    end

    it "reactivates an archived user" do
      u = create :user, :archived
      post :toggle_archive, params: { id: u.id }
      expect(u.reload).to be_active
    end

    it "fails for unknown state" do
      u = create :user
      allow(u).to receive(:state).and_return(:unhandled_state)
      allow(User).to receive(:find).and_return(u)

      expect { post :toggle_archive, params: { id: u.id } }.to raise_exception "Unhandled user state, can not toggle nothing!"
    end
  end

  describe "#show" do
    before do
      login_user user
    end

    it "show privileges" do
      privilege = user.current_organization_user.privileges.first

      get :me

      expect(response.body).to include privilege.name
    end
  end

  describe "invitation" do
    before do
      # our user it seems was invited once already for tests sake...
      user.generate_invite_code
      user.save!
    end

    let(:invited_user) { create :user, :invited }

    it "does not work again for already active users" do
      get :invitation, params: { invite_code: user.invite_code }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You already completed the registration. If not logged in yet please login using your credentials."
    end

    it "requires the correct invite code" do
      expect { get :invitation, params: { invite_code: "someweirdcode556677" } }.
        to raise_error ActiveRecord::RecordNotFound
    end

    it "works for users in invited state" do
      get :invitation, params: { invite_code: invited_user.invite_code }
      expect(response).to have_http_status :ok
      expect(response.body).to match "Register"
    end
  end

  describe "register" do
    let(:invited_user) { create :user, :invited, admin: true }

    let(:params) do
      {
        invite_code: invited_user.invite_code,
        user: {
          emanresu_in_reverse: "Hallo Ich",
          password: "megasupercrazysecret884762um493lk",
          password_confirmation: "megasupercrazysecret884762um493lk"
        }
      }
    end

    it "does not work for already activated users" do
      post :register, params: { invite_code: user.invite_code }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end

    it "updates the attributes" do
      post(:register, params:)

      invited_user.reload

      expect(invited_user.username).to eq "Hallo Ich"
      expect(invited_user.crypted_password).to be_present
    end

    it "activates the invited user" do
      post(:register, params:)
      expect(invited_user.reload).to be_active
    end

    it "updates the user attributes" do
      post(:register, params:)
      expect(response).not_to have_http_status :error
    end

    it "can also fail with invalid data" do
      params[:user][:password_confirmation] = "something else"

      post(:register, params:)
      expect(response).to have_http_status :unprocessable_content
      expect(response.body).to match "Password confirmation does"
      expect(invited_user.reload).to be_invited
    end
  end

  describe "terms and conditions" do
    before do
      login_user user
    end

    it "redirects to terms and conditions page" do
      user.update! terms_of_use: {}, data_protection_policy: {}, code_of_conduct: {}

      get(:me)

      expect(response).to have_http_status :redirect
      expect(response.location).to match(/accept_terms/)

      user.update! terms_of_use: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:terms_of_use]
                   },
                   data_protection_policy: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:data_protection_policy]
                   },
                   code_of_conduct: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:code_of_conduct]
                   }
      get(:me)
      expect(response).to have_http_status :ok
    end
  end

  describe "user without organization" do
    before do
      # lets imagine the organization where a user was got deleted
      user.update_column :organization_id, nil # rubocop:disable Rails/SkipsModelValidations
      login_user user
    end

    it "gets always redirected to missing organization page" do
      get(:me)
      expect(response).to have_http_status :redirect
      expect(response.location).to match(/missing_organization/)
    end

    it "shows the organizations into which the user can switch" do
      get :missing_organization
      expect(response).to have_http_status :ok
      expect(response.body).to match user.organizations.first.to_s
      expect(response.body).to match "Click to switch to one of your organizations"
    end

    it "shows accept reject for open invitations" do
      user.organization_users.delete_all
      OrganizationUser.create!(user:, organization: Organization.first)

      get :missing_organization
      expect(response).to have_http_status :ok
      expect(response.body).to match "You are invited into those organizations:"
      expect(response.body).to match Organization.first.to_s
      expect(response.body).to match "Accept"
      expect(response.body).to match "Reject"
    end
  end
end
