# frozen_string_literal: true

require "rails_helper"

describe ProjectsController do
  let(:import) { create :import }
  let(:source) { import.source }
  let(:project) { import.project }
  let(:organization) { import.organization }
  let(:user) { create :user, privileges: ["project.manage", "project.delete"], organization: }

  let(:another_source) { create :source }
  let(:another_project) { another_source.project }
  let(:another_organization) { another_source.organization }

  let!(:another_visibility) { create :visibility, :test_column, project: another_source.project, organization: } # rubocop:disable RSpec/LetSetup
  let!(:visibility) { create :visibility, :test_column, project:, organization: another_organization } # rubocop:disable RSpec/LetSetup

  render_views

  before { login_user user }

  it "#index" do
    get :index
    expect(response).to have_http_status :ok
    expect(response.body).to include project.name
    expect(response.body).to include source.name

    expect(response.body).to include another_source.project.name
    expect(response.body).to include another_organization.name
  end

  it "#show" do
    get :show, params: { id: project.id }
    expect(response).to have_http_status :ok
    expect(response.body).to include project.name
    expect(response.body).to include source.name
    expect(response.body).to include another_organization.name
  end

  it "#new" do
    get :new
    expect(response).to have_http_status :ok
  end

  it "#create" do
    expect do
      post :create, params: { project: { name: "Some name", description: "Some description" } }
    end.to change(Project, :count).by 1

    expect(Project.last.name).to eq "Some name"
    expect(Project.last.description).to eq "Some description"
  end

  it "#create can fail" do
    expect do
      post :create, params: { project: { name: project.name, description: "Some description" } }
    end.not_to change(Project, :count)
    expect(response).to have_http_status :unprocessable_content
  end

  it "#edit" do
    get :edit, params: { id: project.id }
    expect(response).to have_http_status :ok
    expect(response.body).to include project.name
  end

  it "#edit fails for project of another organization" do
    get :edit, params: { id: another_project.id }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end

  it "#update" do
    expect(project.name).not_to eq "Some name"
    expect(project.description).not_to eq "Some description"

    post :update, params: { id: project.id, project: { name: "Some name", description: "Some description" } }

    project.reload
    expect(project.name).to eq "Some name"
    expect(project.description).to eq "Some description"
  end

  it "#update can fail for invalid data" do
    expect do
      post :update, params: { id: project.id, project: { name: " ", description: "Some description" } }
    end.not_to(change { project.reload.name })
    expect(response).to have_http_status :unprocessable_content
  end

  it "#destroy works for empty project" do
    new_project = create(:project, organization:)
    expect do
      post :destroy, params: { id: new_project.id }
    end.to change(Project, :count).by(-1)
    expect(response).to redirect_to(projects_path)
  end

  it "#destroy fails if project has datasources" do
    expect do
      post :destroy, params: { id: project.id }
    end.to not_change(Project, :count).
      and not_change(Source, :count).
      and not_change(Import, :count)
    expect(response).to redirect_to project_path(project)
  end

  it "non-admin user can not just destroy project of another organization" do
    foreign_project = create :project
    expect(foreign_project.organization).not_to eq project.organization

    post :destroy, params: { id: foreign_project.id }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end
end
