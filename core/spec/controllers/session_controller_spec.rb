# frozen_string_literal: true

require "rails_helper"

describe SessionController do
  describe "Login" do
    it "without credentials" do
      post :create
      expect(response).to have_http_status(:unauthorized)
    end

    it "with credentials" do
      user = create :user
      post :create, params: { email: user.email, password: "very_secret_test_password" }
      expect(response).to have_http_status(:found)
    end
  end

  describe "Oauth", type: :request do
    it "google auth" do
      get auth_at_provider_path(:google)
      expect(response.location).to match(%r{accounts.google.com/o/oauth2/auth})
    end
  end

  describe "#callback - external login and activation" do
    let(:user) { create :user, :google_auth }
    let(:archived_user) { create :user, :google_auth, :archived }
    let(:invited_user) { create :user, :invited }
    let(:external_login_params) { { provider: "google", code: "123134" } }

    it "fails for non existing user" do
      h = { uid: "non_existing_uid", email: "a@b.c", username: "somebody" }
      allow(controller).to receive(:sorcery_user_info).and_return(h)

      get :callback, params: external_login_params

      expect(flash[:alert]).to match "Failed to login with Google"
    end

    it "fails for archived user" do
      h = {
        uid: archived_user.authentications.first.uid,
        email: archived_user.email,
        username: archived_user.username
      }
      allow(controller).to receive(:sorcery_user_info).and_return(h)

      get :callback, params: external_login_params

      expect(flash[:alert]).to match "Failed to login with Google"
    end

    it "logs in an active user" do
      h = {
        uid: user.authentications.first.uid,
        email: user.email,
        username: user.username
      }
      allow(controller).to receive_messages(sorcery_user_info: h, login_from: user)

      get :callback, params: external_login_params

      expect(flash[:notice]).to match "Logged in with Google!"
    end

    it "activates an invited user" do
      h = {
        uid: "some_crazy_uid",
        email: invited_user.email,
        username: "Hey Yeah"
      }
      expect(invited_user.authentications).to be_empty

      allow(controller).to receive_messages(sorcery_user_info: h, login_from: invited_user)

      get :callback, params: external_login_params

      expect(flash[:notice]).to match "Logged in with Google!"

      invited_user.reload
      expect(invited_user).to be_active
      expect(invited_user.authentications.first.uid).to eq "some_crazy_uid"
      expect(invited_user.username).to eq "Hey Yeah"
    end

    it "fails if it can not properly activate invited user" do
      invited_user.update_columns email: "not_an_email" # make it invalid, # rubocop:disable Rails/SkipsModelValidations

      h = {
        uid: "some_crazy_uid",
        email: invited_user.email,
        username: "Hey Yeah"
      }
      expect(invited_user.authentications).to be_empty

      allow(controller).to receive_messages(sorcery_user_info: h, login_from: invited_user)

      get :callback, params: external_login_params

      expect(flash[:alert]).to match "Failed to login with Google"

      invited_user.reload
      expect(invited_user).not_to be_active
      expect(invited_user.authentications).to be_empty
      expect(invited_user.username).to be_nil
    end

    it "can also login with deeper mock level for 100 pc code cov" do
      # we basically check that it works when we mock sorcery gem controller internals...
      # and we get the coverage on the method we just mocked above...
      allow(controller).to receive(:sorcery_fetch_user_hash)
      controller.instance_variable_set :@user_hash, {
        uid: user.authentications.first.uid,
        user_info: { email: user.email, username: user.username }
      }
      controller.instance_variable_set :@provider, Sorcery::Controller::Config.google
      allow(controller).to receive(:login_from).and_return(user)

      get :callback, params: external_login_params

      expect(flash[:notice]).to match "Logged in with Google!"
    end
  end
end
