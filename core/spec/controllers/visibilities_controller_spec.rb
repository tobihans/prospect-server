# frozen_string_literal: true

require "rails_helper"

describe VisibilitiesController do
  let(:source) { create :source }
  let(:project) { source.project }
  let(:organization) { source.organization }
  let(:user) { create :user, privileges: ["visibility.manage"], organization: }

  let(:another_project) { create :project }
  let(:another_organization) { another_project.organization }
  let!(:visibility) { create :visibility, :test_column, project:, organization: another_organization }
  let!(:another_visibility) { create :visibility, :test_column, project: another_project, organization: }

  render_views

  before { login_user user }

  it "#show" do
    get :show, params: { id: visibility.id }
    expect(response).to have_http_status :ok
    expect(response.body).to include project.name
    expect(response.body).to include another_organization.name
  end

  it "#new" do
    get :new, params: { project_id: project.id }
    expect(response).to have_http_status :ok
  end

  it "#new fails for foreign visibility" do
    get :new, params: { project_id: create(:project, organization: another_organization).id }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end

  it "#create" do
    different_org = create :organization

    expect do
      post :create, params:
        { project_id: project.id, visibility: { organization_id: different_org.id, data_from: 1.day.ago.to_s } }
    end.to change(Visibility, :count).by 1

    expect(response).to redirect_to visibility_path(Visibility.last)
    expect(Visibility.last.project).to eq project
    expect(Visibility.last.organization).to eq different_org
  end

  it "#create can fail" do # is invalid, because visibility already exists
    expect do
      post :create, params:
        { project_id: project.id, visibility: { organization_id: another_organization.id, data_from: 1.day.ago.to_s } }
    end.not_to change(Visibility, :count)

    expect(response).to have_http_status :unprocessable_content
  end

  it "#create fails for foreign organization" do
    different_org = create :organization
    post :create, params:
      { project_id: another_project.id, visibility: { organization_id: different_org.id, data_from: 1.day.ago.to_s } }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end

  it "#edit" do
    get :edit, params: { id: visibility.id }
    expect(response).to have_http_status :ok
    expect(response.body).to include project.name
  end

  it "#edit fails for project of another organization" do
    get :edit, params: { id: another_visibility.id }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end

  it "#update" do
    expect do
      post :update, params: { id: visibility.id, visibility: { data_from: 1.day.ago.to_s } }
    end.to(change { visibility.reload.data_from })
    expect(response).to redirect_to visibility_path(visibility)
  end

  it "#update can fail for invalid data" do
    expect do
      post :update, params: { id: visibility.id, visibility: { data_from: 1.day.ago.to_s, data_until: 2.days.ago } }
    end.not_to(change { visibility.reload.data_from })
    expect(response).to have_http_status :unprocessable_content
  end

  it "#update fails for project of another organization" do
    post :update, params: { id: another_visibility.id, visibility: { data_from: 1.day.ago.to_s } }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end

  it "#destroy" do
    expect do
      post :destroy, params: { id: visibility.id }
    end.to change(Visibility, :count).by(-1)
    expect(response).to redirect_to(project_path(project))
  end

  it "#destroy failure is handled" do
    allow(visibility).to receive(:destroy).and_return(false)
    allow(Visibility).to receive(:find).and_return(visibility)

    expect do
      post :destroy, params: { id: visibility.id }
    end.not_to change(Visibility, :count)

    expect(response).to redirect_to(visibility_path(visibility))
  end

  it "#destroy fails for foreign visibility" do
    post :destroy, params: { id: another_visibility.id }
    expect(response).to have_http_status :redirect
    expect(flash[:alert]).to match "You are not authorized to perform this action."
  end
end
