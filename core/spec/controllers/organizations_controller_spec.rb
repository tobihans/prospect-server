# frozen_string_literal: true

require "rails_helper"

describe OrganizationsController do
  let(:admin) { create :user, :admin }
  let(:non_admin) { create :user }
  let(:organization) { admin.organization }

  render_views

  describe "as admin" do
    before do
      login_user admin
    end

    it "#show" do
      get :show, params: { id: organization.id }
      expect(response).to have_http_status :ok
      expect(response.body).to include admin.username
      expect(response.body).to include organization.name
    end

    it "#min" do
      get :mine
      expect(response).to have_http_status :ok
      expect(response.body).to include admin.username
      expect(response.body).to include organization.name
    end

    it "#edit" do
      get :edit, params: { id: organization.id }
      expect(response).to have_http_status :ok
      expect(response.body).to include organization.name
    end

    it "#updates" do
      post :update, params: { id: organization.id, organization: { name: "Another organization New name" } }
      expect(response).to have_http_status :found
      expect(response).to redirect_to organization_path(organization)
      expect(organization.reload.name).to eq "Another organization New name"
    end

    it "#does not update keys only if existent" do
      post :update, params: { id: organization.id, organization: { name: "Another organization New name", iv: "fsdfdsf", salt: "fsdfdsf<d", wrapped_key: "fsdfsdfdsfs", public_key: "fsdfdsfd" } }
      expect(response).to have_http_status :found
      expect(response).to redirect_to organization_path(organization)
      expect(organization.reload.iv).not_to eq "fsdfdsf"
    end

    it "#updates keys if not existent" do
      organization.remove_encryption_keys!
      post :update, params: { id: organization.id, organization: { name: "Another organization New name", iv: "fsdfdsf", salt: "fsdfdsf<d", wrapped_key: "fsdfsdfdsfs", public_key: "fsdfdsfd" } }
      expect(response).to have_http_status :found
      expect(response).to redirect_to organization_path(organization)
      expect(organization.reload.iv).to eq "fsdfdsf"
      expect(organization.reload.salt).to eq "fsdfdsf<d"
      expect(organization.reload.wrapped_key).to eq "fsdfsdfdsfs"
      expect(organization.reload.public_key).to eq "fsdfdsfd"
    end
  end

  describe "as non-admin" do
    before do
      login_user non_admin
    end

    it "#updates only as admin and Manager" do
      post :update, params: { id: organization.id, organization: { name: "Another organization" } }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end

    it "#edits only as admin and manager" do
      post :edit, params: { id: organization.id, organization: { name: "Another organization" } }
      expect(response).to have_http_status :redirect
      expect(flash[:alert]).to match "You are not authorized to perform this action."
    end
  end
end
