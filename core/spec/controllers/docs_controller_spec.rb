# frozen_string_literal: true

require "rails_helper"

describe DocsController do
  let(:admin) { create :user, :admin }

  render_views

  before { login_user admin }

  it "#index gives table names and descriptions" do
    get :index
    Postgres::DataTable.each do |dt|
      expect(response.body).to include dt.data_category
      expect(response.body).to include dt.desc
    end
  end

  describe "#show html" do
    let(:dt) { Postgres::DataTable.new "test" }

    before { get :show, params: { id: "test" } }

    it "includes table name and desc" do
      expect(response.body).to include "test"
      expect(response.body).to include dt.desc
    end

    it "includes links to api doc and csv" do
      expect(response.body).to include "/api-docs"
    end

    it "includes name and description for all columns" do
      dt.columns.each do |c|
        expect(response.body).to include c.name
        expect(response.body).to include c.desc
      end
    end
  end

  it "#show can give example csv" do
    get :show, params: { id: "test" }, format: :csv

    expect(response.body).to include "float_column"
    expect(response.body).to include "3.14"
  end
end
