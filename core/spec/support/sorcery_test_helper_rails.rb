# frozen_string_literal: true

module Sorcery
  module TestHelpers
    module Rails
      def sign_in(user)
        visit "/"

        # we absolutly make sure we logged in
        while has_field?("email") && has_field?("password")
          within("form") do
            fill_in "email", with: user.email
            fill_in "password", with: "very_secret_test_password"
          end
          click_link_or_button "Login"

          visit "/"
        end

        Current.user = user
      end

      def sign_out
        visit "/users/logout"
        Current.user = nil
      end
    end
  end
end
