# frozen_string_literal: true

require "capybara/cuprite"

# LOGGER = StringIO.new

def register_driver(driver_name)
  opts = {
    # capabilities: options,
    # logger: $stdout,    # get detailed logs
    # inspector: true,
    window_size: [1200, 800],
    browser_options: { "no-sandbox" => nil },
    timeout: 20
  }

  Capybara.register_driver(driver_name) do |app|
    Capybara::Cuprite::Driver.new(app, opts)
  end

  Capybara::Screenshot.register_driver(driver_name) do |driver, path|
    driver.save_screenshot(path, full: true)
  end
end

register_driver(:cuprite)
Capybara.current_driver = :cuprite
Capybara.javascript_driver = :cuprite

Capybara.server = :puma, { Silent: true, Threads: "0:1" }
