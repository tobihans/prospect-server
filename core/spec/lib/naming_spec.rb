# frozen_string_literal: true

require "rails_helper"

describe Naming do
  let(:organization) { create :organization }
  let(:user) { create :user }
  let(:data_category) { "meters" }
  let(:datatable) { Postgres::DataTable.new data_category }

  before do
    allow(organization).to receive(:id).and_return(17)
    allow(user).to receive(:id).and_return(35)
  end

  it "#db_data_table_prefix" do
    expect(described_class.db_data_table_prefix).to eq "data"
    expect(described_class.db_data_table_prefix(prefix: true)).to eq "data_"
  end

  it "#organization_prefix" do
    expect(described_class.organization_prefix).to eq "prospect_organization"
    expect(described_class.organization_prefix(prefix: true)).to eq "prospect_organization_"
  end

  it "#user_prefix" do
    expect(described_class.user_prefix).to eq "prospect_user"
    expect(described_class.user_prefix(prefix: true)).to eq "prospect_user_"
  end

  it "#db_data_table" do
    expect(described_class.db_data_table(data_category)).to eq "data_meters"
    expect(described_class.db_data_table(data_category, prefix: true)).to eq "data_meters_"
  end

  it "#db_organization_user" do
    expect(described_class.db_organization_user(organization)).to eq "prospect_test#{test_number}_prospect_organization_17"
    expect(described_class.db_organization_user(organization, prefix: true)).to eq "prospect_test#{test_number}_prospect_organization_17_"
  end

  it "#db_organization_schema" do
    expect(described_class.db_organization_schema(organization)).to eq "prospect_test#{test_number}_prospect_organization_17"
    expect(described_class.db_organization_schema(organization, prefix: true)).to eq "prospect_test#{test_number}_prospect_organization_17_"
  end

  it "#db_organization_data_view" do
    expect(described_class.db_organization_data_view(organization, data_category)).
      to eq "prospect_test#{test_number}_prospect_organization_17.data_meters"
    expect(described_class.db_organization_data_view(organization, data_category, prefix: true)).
      to eq "prospect_test#{test_number}_prospect_organization_17.data_meters_"
  end

  it "#db_organization_shared_visibilities_view" do
    expect(described_class.db_organization_shared_visibilities_view(organization)).
      to eq "prospect_test#{test_number}_prospect_organization_17.shared_data"
  end

  it "#db_visibility_prefix" do
    expect(described_class.db_visibility_prefix).to eq "vis"
    expect(described_class.db_visibility_prefix(prefix: true)).to eq "vis_"
  end

  it "#visibility_default_prefix" do
    expect(described_class.visibility_default_prefix).to eq "default"
    expect(described_class.visibility_default_prefix(prefix: true)).to eq "default_"
  end

  it "#db_visibility_view_default" do
    expect(described_class.db_visibility_view_default(datatable)).to eq "default_data_meters"
    expect(described_class.db_visibility_view_default(datatable, prefix: true)).to eq "default_data_meters_"
  end

  it "#db_visibility_view_default_with_schema" do
    expect(described_class.db_visibility_view_default_with_schema(datatable)).
      to eq "visibilities.default_data_meters"
    expect(described_class.db_visibility_view_default_with_schema(datatable, prefix: true)).
      to eq "visibilities.default_data_meters_"
  end

  describe "custom view" do
    let(:source) { create :source, name: "soME 4672 W€ir%d", data_category: "custom" }
    let(:visibility) { source.project.default_visibility }

    before do
      allow(source).to receive_messages(id: 31, organization:)
      allow(visibility).to receive(:organization).and_return(organization)
    end

    it "#db_custom_view_prefix" do
      expect(described_class.db_custom_view_prefix).to eq "custom"
      expect(described_class.db_custom_view_prefix(prefix: true)).to eq "custom_"
    end

    it "#db_custom_view" do
      expect(described_class.db_custom_view(source)).to eq "custom_31_some_4672_w_ir_d"
      expect(described_class.db_custom_view(source, prefix: true)).
        to eq "custom_31_some_4672_w_ir_d_"
    end

    it "#db_visibility_custom_view" do
      expect(described_class.db_visibility_custom_view(visibility, source)).
        to eq "prospect_test#{test_number}_prospect_organization_17.custom_31_some_4672_w_ir_d"
      expect(described_class.db_visibility_custom_view(visibility, source, prefix: true)).
        to eq "prospect_test#{test_number}_prospect_organization_17.custom_31_some_4672_w_ir_d_"
    end

    it "#db_organization_custom_view" do
      expect(described_class.db_organization_custom_view(organization, "some_view_name")).
        to eq "prospect_test#{test_number}_prospect_organization_17.some_view_name"
      expect(described_class.db_organization_custom_view(organization, "some_view_name", prefix: true)).
        to eq "prospect_test#{test_number}_prospect_organization_17.some_view_name_"
    end
  end

  it "#db_organization_rbf_summary_view" do
    expect(described_class.db_organization_rbf_summary_view(organization)).
      to eq "prospect_test#{test_number}_prospect_organization_17.rbf_summary"
    expect(described_class.db_organization_rbf_summary_view(organization, prefix: true)).
      to eq "prospect_test#{test_number}_prospect_organization_17.rbf_summary_"
  end

  it "#db_organization_rbf_cofig_view" do
    expect(described_class.db_organization_rbf_config_view(organization)).
      to eq "prospect_test#{test_number}_prospect_organization_17.rbf_config"
    expect(described_class.db_organization_rbf_config_view(organization, prefix: true)).
      to eq "prospect_test#{test_number}_prospect_organization_17.rbf_config_"
  end

  it "#grafana_organization_org" do
    expect(described_class.grafana_organization_org(organization)).to eq "prospect_organization_17"
    expect(described_class.grafana_organization_org(organization, prefix: true)).to eq "prospect_organization_17_"
  end

  it "#grafana_user" do
    expect(described_class.grafana_user(user)).to eq "prospect_user_35"
    expect(described_class.grafana_user(user, prefix: true)).to eq "prospect_user_35_"
  end

  it "#grafana_data_source" do
    expect(described_class.grafana_data_source(organization)).
      to eq "postgres_sql_prospect_organization_17"
    expect(described_class.grafana_data_source(organization, prefix: true)).
      to eq "postgres_sql_prospect_organization_17_"
  end

  it "#join" do
    expect(described_class.join("a", 1, "c")).to eq "a_1_c"
    expect(described_class.join("a", 1, "c", prefix: true)).to eq "a_1_c_"
  end
end
