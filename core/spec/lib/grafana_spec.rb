# frozen_string_literal: true

require "rails_helper"

describe Grafana do
  describe "Client" do
    it "can succesfuly login" do
      VCR.use_cassette :grafana_login do
        client = Grafana::Client.new(settings: { debug: false })
        expect(client.session_cookies["grafana_session"]).to eq "be5e33ce90c4edd28321ecb41c4797b2"
      end
    end

    it "no session with wrong credentials" do
      VCR.use_cassette :grafana_login_fail do
        expect { Grafana::Client.new(user: "some_user", pass: "wrong", settings: { debug: false }) }.
          to raise_error(/Unauthorized/)
      end
    end

    it "sets login_at" do
      VCR.use_cassette :grafana_login do
        client = Grafana::Client.new(settings: { debug: false })
        expect(client.login_at).to be_within(3.seconds).of(Time.zone.now)
      end
    end

    it "#login_expired? false when just created" do
      VCR.use_cassette :grafana_login do
        client = Grafana::Client.new(settings: { debug: false })
        expect(client).not_to be_login_expired
      end
    end

    it "#login_expired? true when login is older" do
      VCR.use_cassette :grafana_login do
        client = Grafana::Client.new(settings: { debug: false })
        client.instance_variable_set :@login_at, 10.days.ago
        expect(client).to be_login_expired
      end
    end

    it "RemoteControl#client creates new client when login expired" do
      client, other_client = nil

      VCR.use_cassette :grafana_login do
        client = Grafana::RemoteControl.client
      end

      same_client = Grafana::RemoteControl.client
      expect(same_client).to eq client

      client.instance_variable_set :@login_at, 10.days.ago

      VCR.use_cassette :grafana_login do
        other_client = Grafana::RemoteControl.client
        expect(other_client).not_to eq client
      end

      expect(client).to be_login_expired
      expect(other_client).not_to be_login_expired
    end
  end

  describe "Admin" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "fetch settings" do
      VCR.use_cassette :grafana_admin_settings do
        settings = client.admin_settings
        expect(settings).not_to be_nil
        expect(settings).to be_a Hash
        expect(settings[:DEFAULT][:instance_name]).to eq "72872670a488"
      end
    end

    it "creates user" do
      VCR.use_cassette :grafana_admin_create_user do
        properties = {
          name: "user",
          login: "user",
          password: "userpassword",
          OrgId: 1
        }

        result = client.create_user(properties)
        expect(result[:message]).to eq "User created"
        expect(result[:id]).to be_a Integer
      end
    end

    it "updates user pass" do
      VCR.use_cassette :grafana_admin_update_user_pass do
        result = client.update_user_pass(3, "new_password")
        expect(result[:message]).to eq "User password updated"
      end
    end

    it "updates user permissions" do
      VCR.use_cassette :grafana_admin_update_user_perm do
        result = client.update_user_permissions(3, "Editor", 11)
        expect(result[:message]).to eq "Organization user updated"

        result = client.update_user_permissions(3, "Viewer", 11)
        expect(result[:message]).to eq "Organization user updated"

        result = client.update_user_permissions(3, "Admin", 11)
        expect(result[:message]).to eq "Organization user updated"

        result = client.update_user_permissions(3, { isGrafanaAdmin: false }, 11)
        expect(result[:message]).to eq "User permissions updated"

        result = client.update_user_permissions(3, "Non Existing Permission", 11)
        expect(result).to be false

        result = client.update_user_permissions(3, { isGrafanaAdmin: "not a boolean" }, 11)
        expect(result).to be false

        result = client.update_user_permissions(3, { what: "this" }, 11)
        expect(result).to be false
      end
    end

    it "deletes user" do
      VCR.use_cassette :grafana_admin_delete_user do
        result = client.delete_user(3)
        expect(result[:message]).to eq "User deleted"

        result = client.delete_user(1)
        expect(result).to be false
      end
    end
  end

  describe "User" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "current user" do
      VCR.use_cassette :grafana_current_user do
        result = client.current_user
        expect(result).to be_a Hash
        expect(result[:id]).to eq 1
        expect(result[:login]).to eq "admin"
      end
    end

    it "update current user pass" do
      VCR.use_cassette :grafana_current_user_update_password do
        result = client.update_current_user_pass(
          old_password: "admin",
          new_password: "admin_new",
          confirm_new: "admin_new"
        )
        expect(result[:message]).to eq "User password changed"
      end
    end

    it "current user org" do
      VCR.use_cassette :grafana_current_user_org do
        result = client.current_user_orgs
        expect(result).to be_a Array
        expect(result.first[:orgId]).not_to be_nil
        expect(result.first[:orgId]).to be 1
      end
    end

    it "list all users" do
      VCR.use_cassette :grafana_all_users do
        result = client.all_users
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(id: 2), a_hash_including(id: 1)])
      end
    end

    it "user by id" do
      VCR.use_cassette :grafana_user_by_id do
        result = client.user_by_id 1
        expect(result).to be_a Hash
        expect(result).to match(a_hash_including(id: 1, login: "admin"))
      end
    end

    it "search user by" do
      VCR.use_cassette :grafana_user_search_by do
        result = client.search_for_users_by(login: "admin", isAdmin: true)
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(id: 1, login: "admin")])
        result = client.search_for_users_by(login: "user1")
        expect(result).to match([a_hash_including(id: 2, login: "user1")])
      end
    end

    it "list user orgs" do
      VCR.use_cassette :grafana_user_orgs do
        result = client.user_orgs(1)
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(orgId: 1, name: "Main Org.")])
      end
    end
  end

  describe "Organization" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "changes org context" do
      VCR.use_cassette :grafana_change_org_context do
        result = client.create_org(name: "Test Org")

        result = client.switch_org_context org_id: result[:orgId]
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Active organization changed"

        result = client.switch_org_context org_id: 1
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Active organization changed"
      end
    end

    it "show current org" do
      VCR.use_cassette :grafana_curr_org do
        result = client.current_org
        expect(result).to be_a Hash
        expect(result).to a_hash_including(id: 1, name: "Main Org.")
      end
    end

    it "update current org" do
      VCR.use_cassette :grafana_update_curr_org do
        result = client.update_current_org({ name: "Main Org." }.to_json)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Organization updated"
      end
    end

    it "current org users" do
      VCR.use_cassette :grafana_curr_org_users do
        result = client.current_org_users
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(userId: 1, login: "admin"),
                                 a_hash_including(userId: 2, login: "user1")])
      end
    end

    it "create org" do
      VCR.use_cassette :grafana_create_org do
        result = client.create_org(name: "New Org Inc.")
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Organization created"
        expect(result[:orgId]).to eq 2
      end
    end

    it "gets org" do
      VCR.use_cassette :grafana_get_org do
        result = client.org(1)
        expect(result).to be_a Hash
        expect(result[:id]).to eq 1
        expect(result[:name]).to eq "Main Org."
      end
    end

    it "update org" do
      VCR.use_cassette :grafana_update_org do
        result = client.update_org({ name: "Even Newer Org Inc." }, org_id: 2)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Organization updated"
      end
    end

    it "delete org" do
      VCR.use_cassette :grafana_delete_org do
        result = client.delete_org(org_id: 2)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Organization deleted"
      end
    end

    it "list all orgs" do
      VCR.use_cassette :grafana_list_all_orgs do
        result = client.all_orgs
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(id: 1), a_hash_including(id: 2)])
      end
    end

    it "add user to current org" do
      VCR.use_cassette :grafana_add_user_curr_org do
        properties = {
          name: "user2",
          email: "user2@example.com",
          login: "user2",
          password: "user2_passwword",
          OrgId: 2
        }
        result = client.create_user(properties)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "User created"

        result = client.add_user_to_current_org(login_or_email: "user2", role: "Editor")
        expect(result).to be_a Hash
        expect(result[:message]).to eq "User added to organization"
        expect(result[:userId]).to eq 3
      end
    end

    it "add/remove user to/from org" do
      VCR.use_cassette :grafana_add_remove_user_to_from_org do
        result = client.delete_user_from_org(3, org_id: 1)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "User removed from organization"

        result = client.add_user_to_org(login_or_email: "user2", role: "Editor", org_id: 1)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "User added to organization"
        expect(result[:userId]).to eq 3
      end
    end

    it "list org users" do
      VCR.use_cassette :grafana_list_org_users do
        result = client.org_users(org_id: 1)
        expect(result).to be_a Array
        expect(result).to match([a_hash_including(userId: 1, login: "admin"),
                                 a_hash_including(userId: 2, login: "user1"),
                                 a_hash_including(userId: 3, login: "user2")])
      end
    end

    it "update org user" do
      VCR.use_cassette :grafana_update_org_user do
        result = client.update_org_user(1, { role: "Admin" }, org_id: 1)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Organization user updated"
      end
    end
  end

  describe "Datasource" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "creates datasource" do
      VCR.use_cassette :grafana_create_datasource do
        result = client.create_data_source(data_source: {}, org_id: 1)
        expect(result).to be false
        data_source = { name: "PostgreSQL",
                        type: "postgres",
                        typeName: "PostgreSQL",
                        url: "postgres:5432",
                        user: "postgres",
                        jsonData: { database: "prospect_development" },
                        access: "proxy" }
        result = client.create_data_source(data_source:, org_id: 1)
        expect(result[:message]).to eq "Datasource added"
        expect(result[:name]).to eq "PostgreSQL"
        expect(result[:datasource]).to be_a Hash
      end
    end

    it "lists datasources" do
      VCR.use_cassette :grafana_list_datasource do
        result = client.data_sources(org_id: 1)
        expect(result).to be_a Array
        data_source = result.first
        expect(data_source).to be_a Hash
        expect(data_source[:url]).to eq "postgres:5432"
        expect(data_source[:user]).to eq "postgres"
        expect(data_source[:database]).to eq "prospect_development"
      end
    end

    it "query datasource" do
      VCR.use_cassette :grafana_query_datasource do
        result = client.data_source(uid: "G5Yl-bd4z", org_id: 1)
        expect(result).to be_a Hash
        expect(result[:url]).to eq "postgres:5432"
        expect(result[:user]).to eq "postgres"
        expect(result[:database]).to eq "prospect_development"
      end
    end

    it "updates datasource" do
      VCR.use_cassette :grafana_update_datasource do
        result = client.update_data_source uid: "G5Yl-bd4z",
                                           data_source: { user: "user1", database: "data" },
                                           org_id: 1
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Datasource updated"
        expect(result[:datasource][:user]).to eq "user1"
        expect(result[:datasource][:database]).to eq "data"
      end
    end

    it "deletes datasources" do
      VCR.use_cassette :grafana_delete_datasource do
        result = client.delete_data_source(uid: "G5Yl-bd4z", org_id: 1)
        expect(result).to be_a Hash
        expect(result[:message]).to eq "Data source deleted"
      end
    end
  end

  describe "Dashboard" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "installs default dashboards for an organization" do
      VCR.use_cassette(:grafana_install_default_dashboards, erb: default_dashboard_infos) do
        org = create :organization, id: 12_345, grafana_org_id: 2

        VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
          org.setup_grafana!
        end

        result = client.dashboard_list org_id: 2
        expect(result).to be_a Array
        expect(result.count).to eq Dir["#{Grafana::RemoteControl::DASHBOARDS_PATH}/*.json"].count
        result.map do |db|
          permissions = client.dashboard_permissions(uid: db[:uid], org_id: 2)
          expect(permissions).to include(include(role: "Editor", permissionName: "View"))
          expect(permissions).to include(include(role: "Viewer", permissionName: "View"))
        end
      end
    end

    it "queries dashboard json" do
      VCR.use_cassette :grafana_query_dashboard do
        result = client.dashboard "shs", org_id: 2
        expect(result).to be_a Hash
        expect(result[:dashboard][:title]).to eq "SHS"
      end
    end

    it "query home dashboard" do
      VCR.use_cassette :grafana_home_dashboard do
        result = client.home_dashboard
        expect(result).to be_a Hash
        expect(result[:dashboard][:title]).to eq "Home"
      end
    end

    it "list dashboards" do
      VCR.use_cassette :grafana_list_dashboards do
        result = client.dashboard_list org_id: 1
        expect(result).to be_a Array
      end
    end
  end

  describe "Folders" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "creates folders" do
      VCR.use_cassette(:grafana_create_folder) do
        result = client.create_folder "Test Folder", org_id: 1
        expect(result).to be_a Hash
        expect(result).to have_key :uid
        expect(result).to have_key :title
        expect(result[:title]).to eq "Test Folder"
      end
    end

    it "lists all or single folders" do
      VCR.use_cassette(:grafana_list_folders) do
        client.create_folder "Test Folder", org_id: 1
        result = client.folders org_id: 1
        expect(result).to be_a Array
        expect(result.first).to be_a Hash
        expect(result.first).to have_key :uid
        expect(result.first).to have_key :title
        expect(result.first[:title]).to eq "Test Folder"

        result = client.folder uid: result.first[:uid], org_id: 1
        expect(result).to be_a Hash
        expect(result).to have_key :uid
        expect(result).to have_key :title
        expect(result[:title]).to eq "Test Folder"
      end
    end

    it "delets folders" do
      VCR.use_cassette(:grafana_delete_folders) do
        result = client.create_folder "Test Folder", org_id: 1
        result = client.delete_folder uid: result[:uid], org_id: 1

        expect(result[:message]).to eq "Folder Test Folder deleted"

        result = client.folders org_id: 1
        expect(result).to be_a Array
        expect(result).to be_empty
      end
    end
  end

  describe "Team" do
    let(:client) { Grafana::Client.new(settings: { debug: true }) }

    it "create and delete grafana team" do
      VCR.use_cassette(:grafana_create_and_delete_team) do
        result = client.create_team name: "Test Team", org_id: 1
        team_id = result[:teamId]
        expect(result[:message]).to eq "Team created"

        result = client.get_team name: "Test Team", org_id: 1
        expect(result[:id]).to eq team_id
        expect(result[:orgId]).to eq 1
        expect(result[:name]).to eq "Test Team"

        result = client.delete_team id: team_id, org_id: 1
        expect(result[:message]).to eq "Team deleted"

        result = client.get_team name: "Test Team", org_id: 1
        expect(result).to be_nil
      end
    end

    it "add and remove members from teams" do
      VCR.use_cassette(:grafana_add_and_remobe_team_members) do
        client.create_team name: "Test Team", org_id: 1
        team = client.get_team name: "Test Team", org_id: 1
        user = client.create_user({ login: "test_user", password: "super_secret_test_password", OrgId: 1 })

        result = client.add_team_member user_id: user[:id], team_id: team[:id], org_id: 1
        expect(result[:message]).to eq "Member added to Team"

        result = client.get_team_members team_id: team[:id], org_id: 1

        expect(result).to include(a_hash_including(login: "test_user", userId: user[:id]))

        result = client.remove_team_member user_id: user[:id], team_id: team[:id], org_id: 1
        expect(result[:message]).to eq "Team Member removed"

        result = client.get_team_members team_id: team[:id], org_id: 1
        expect(result).not_to include(a_hash_including(login: "test_user", userId: user[:id]))
      end
    end
  end

  describe "Remote Control" do
    # fixed id for http replays
    # hope this does not explode...
    let(:organization) { create :organization, id: 12_345 }
    let(:user) { create :user, organization:, id: 12_345 }
    let(:admin) { create :user, :admin, organization:, id: 12_346 }

    it "#setup_all_organizations" do
      VCR.use_cassette(:grafana_setup_all_organizations, erb: default_dashboard_infos) do
        allow(Grafana::RemoteControl).to receive(:update_user_permission).and_call_original
        allow_any_instance_of(Grafana::Client).to receive(:org).and_call_original
        allow(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).and_call_original

        # trigger creation
        VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
          organization.setup_grafana!
        end

        Grafana::RemoteControl.instance_variable_set("@client", nil)
        user.create_grafana_user

        client = Grafana::RemoteControl.client

        uid = client.data_sources(org_id: organization.grafana_org_id).select { |ds| ds[:name] == Naming.grafana_data_source(organization) }.first[:uid]
        client.delete_data_source(uid:, org_id: organization.grafana_org_id)
        client.delete_org(org_id: client.all_orgs.select { |org| org[:name] == Naming.grafana_organization_org(organization) }.first[:id])

        VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
          expect { Grafana::RemoteControl.setup_all_organizations! }.not_to raise_error
        end

        expect(client.data_sources(org_id: organization.grafana_org_id).select { |ds| ds[:name] == Naming.grafana_data_source(organization) }).not_to be_empty
        expect(client.all_orgs.select { |org| org[:name] == Naming.grafana_organization_org(organization) }).not_to be_empty
        expect(client.all_users.select { |u| u[:login] == Naming.grafana_user(user) }).not_to be_empty
      end
    end

    it "updates library panels" do
      VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
        allow(Grafana::RemoteControl).to receive(:update_library_panels).and_return(nil)
        allow(Grafana::RemoteControl).to receive(:update_user_permission).and_call_original
        allow_any_instance_of(Grafana::Client).to receive(:org).and_call_original
        allow(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).and_call_original

        organization.setup_grafana!
      end

      VCR.use_cassette(:grafana_update_library_panels) do
        Grafana::RemoteControl.instance_variable_set("@client", nil)
        client = Grafana::RemoteControl.client

        dashboard = JSON.parse(Grafana::RemoteControl.read_dashboard_file("data")).with_indifferent_access
        dashboard[:__elements].each { _2[:model][:options][:__prospect_version] = 1 }
        allow(Grafana::RemoteControl).to receive(:read_dashboard_file).and_return(dashboard.to_json)

        allow(Grafana::RemoteControl).to receive(:available_default_dashboards).and_return(["data"])
        allow(Grafana::RemoteControl).to receive(:update_library_panels).and_call_original

        Grafana::RemoteControl.default_dashboards_for_organization! organization

        elements = client.available_library_elements(org_id: organization.grafana_org_id)[:result][:elements]
        expect(elements.map { _1.dig(:model, :options, :__prospect_version) }.compact.tally[1]).to eq 4 # 4 elements with version 1

        dashboard[:__elements].first.last[:model][:options][:__prospect_version] = 2
        allow(Grafana::RemoteControl).to receive(:read_dashboard_file).and_return(dashboard.to_json)
        Grafana::RemoteControl.default_dashboards_for_organization! organization
        elements = client.available_library_elements(org_id: organization.grafana_org_id)[:result][:elements]

        versions = elements.map { _1.dig(:model, :options, :__prospect_version) }.compact.tally
        expect(versions[1]).to eq 3 # 3 elements with version 1
        expect(versions[2]).to eq 1 # 1 elements with version 2
      end
    end

    it "lists available dashboards" do
      dashboards = Grafana::RemoteControl.available_default_dashboards

      expect(dashboards).not_to be_empty
    end

    it "updates user permissions" do
      VCR.use_cassette(:grafana_update_user_permissions) do
        admin
        user
        expect(Grafana::RemoteControl).to receive(:update_user_permission).and_call_original
        result = Grafana::RemoteControl.update_user_permission(user.current_organization_user)
        expect(result[:message]).to eq "Organization user updated"

        expect(Grafana::RemoteControl).to receive(:update_user_permission).and_call_original
        result = Grafana::RemoteControl.update_user_permission(admin.current_organization_user)
        expect(result[:message]).to eq "Organization user updated"
      end
    end
  end

  describe "Default Dashboards" do
    let(:db_jsons) { Dir["config/grafana_templates/*/**.json"].map { JSON.load_file _1 } }
    let(:table_names) { Postgres::DataTable.all.map(&:table_name) }

    it "check if all default dashboards have `__default` attribute set" do
      db_jsons.each do |db_json|
        expect(db_json.keys).to include "__default"
      end
    end

    it "checks if all default dashboards have `__tables` attribute set" do
      db_jsons.each do |db_json|
        expect(db_json.keys).to include "__tables"
        expect(db_json["__tables"]).to be_a Array
        expect(db_json["__tables"]).to all(be_in(table_names))
      end
    end

    it "checks if all default dashboards are in external use format" do
      db_jsons.each do |db_json|
        expect(db_json.keys).to include("__inputs", "__elements", "__requires")
      end
    end
  end
end
