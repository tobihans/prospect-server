# frozen_string_literal: true

require "rails_helper"

describe P14n do
  it "pseudonymize" do
    expect(described_class.pseudonymize("test")).not_to match(/test/)
    expect(described_class.pseudonymize("test")).not_to be_empty
  end

  it "pseudonymize_key" do
    our_hash =
      { "customers" => [
        { "name" => "Bill", "other" => "not critical", "meters" =>
          [{ "id" => 1, "serial" => "abc123" }] }
      ] }

    described_class.pseudonymize_key our_hash["customers"].first, "name"
    described_class.pseudonymize_key our_hash["customers"].first["meters"].first, "serial"

    expect(our_hash["customers"].first["name_p"]).not_to match(/Bill/)
    expect(our_hash["customers"].first["name"]).to match(/Bill/)

    # dont touch other keys
    expect(our_hash["customers"].first["other"]).to eq("not critical")

    # deeper nested
    expect(our_hash["customers"].first["meters"].first["serial_p"]).not_to match(/abc123/)
    expect(our_hash["customers"].first["meters"].first["serial"]).to match(/abc123/)
  end

  it "text" do
    expect(described_class.text(" This is a Text! ")).to eq(described_class.text("this is a text!"))
  end

  it "phone_number" do
    expect(described_class.phone_number("0049 (0)30 56789")).to eq(described_class.phone_number("+493056789"))
    expect(described_class.phone_number(" 1234 ")).to eq(described_class.phone_number("1234"))
  end

  it "text_key" do
    our_hash =
      { "customers" => [
        { "name" => "Bill bond ", "other" => "not critical" }
      ] }

    our_hash2 =
      { "customers" => [
        { "name" => "BILL BOND", "other" => "not critical" }
      ] }

    described_class.text_key our_hash["customers"].first, "name"
    described_class.text_key our_hash2["customers"].first, "name"

    expect(our_hash["customers"].first["name_p"]).not_to match(/Bill/)
    # different formats of name should have the some hash value...
    expect(our_hash["customers"].first["name_p"]).to eq(our_hash2["customers"].first["name_p"])
  end

  it "geo - combined in one field" do
    our_hash = { "coord" => "50.1234567, 20.1" }

    described_class.geo_key(our_hash, "coord")

    expect(our_hash).to have_key "coord"
    geo_expectations(our_hash)
  end

  it "geo - separated field" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon")

    expect(our_hash).to have_key "lat"
    expect(our_hash).to have_key "lon"

    geo_expectations(our_hash)
  end

  it "geo - with key prefix" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", key_prefix: "customer")

    %w[lat_p lon_p lat_b lon_b].each do |k|
      expect(our_hash).not_to have_key k
    end

    %w[customer_lat_p customer_lon_p customer_lat_b customer_lon_b].each do |k|
      expect(our_hash[k]).to be_present
    end
  end

  it "geo - with full key name" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", key_full: true)

    %w[lat_p lon_p lat_b lon_b].each do |k|
      expect(our_hash).not_to have_key k
    end

    %w[latitude_p longitude_p latitude_b longitude_b].each do |k|
      expect(our_hash[k]).to be_present
    end
  end

  it "can combine key prefix and full and mixes nothing up" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", key_full: true, key_prefix: "meter")

    %w[lat_p lon_p lat_b lon_b].each do |k|
      expect(our_hash).not_to have_key k
    end

    expect(our_hash["meter_latitude_p"]).to eq(described_class.pseudonymize("50.123457")) # rounded to 6 digits
    expect(our_hash["meter_longitude_p"]).to eq(described_class.pseudonymize("20.1"))
    expect(our_hash["meter_latitude_b"]).to eq(50.123)
    expect(our_hash["meter_longitude_b"]).to eq(20.1)
  end

  def geo_expectations(our_hash)
    expect(our_hash["lat_p"]).to eq(P14n.pseudonymize("50.123457")) # rounded to 6 digits
    expect(our_hash["lon_p"]).to eq(P14n.pseudonymize("20.1"))
    expect(our_hash["lat_b"]).to eq(50.123)
    expect(our_hash["lon_b"]).to eq(20.1)
  end

  it "geo - coord empty" do
    our_hash = { "coord" => "" }

    described_class.geo_key(our_hash, "coord")
    expect(our_hash).to have_key "coord"
    expect(our_hash["lat_p"]).to be_nil
    expect(our_hash["lon_p"]).to be_nil
    expect(our_hash["lat_b"]).to be_nil
    expect(our_hash["lon_b"]).to be_nil
  end

  it "geo - coord not parsable" do
    our_hash = { "coord" => "50.01, lon missing" }

    expect { described_class.geo_key(our_hash, "coord") }.to raise_error(ArgumentError)
    expect(our_hash["lat_p"]).to be_nil
    expect(our_hash["lon_p"]).to be_nil
    expect(our_hash["lat_b"]).to be_nil
    expect(our_hash["lon_b"]).to be_nil
  end

  it "handles missing lat param and puts empty values" do
    our_hash = { "lat" => nil, "lon" => "12.345" }

    described_class.geo_key(our_hash, "lat", "lon")

    expect(our_hash["lat_p"]).to be_nil
    expect(our_hash["lon_p"]).to be_nil
    expect(our_hash["lat_b"]).to be_nil
    expect(our_hash["lon_b"]).to be_nil
  end
end
