# frozen_string_literal: true

require "rails_helper"

describe RsaOaepEncrypt do
  let(:organization) { create :organization }

  it "encrypts" do
    expect(described_class.encrypt("test", organization)).not_to match(/test/)
    expect(described_class.encrypt("test", organization)).not_to be_empty
  end

  it "pseudonymize_key" do
    our_hash =
      { "customers" => [
        { "name" => "Bill", "other" => "not critical", "meters" =>
          [{ "id" => 1, "serial" => "abc123" }] }
      ] }

    described_class.text_key our_hash["customers"].first, "name", organization
    described_class.text_key our_hash["customers"].first["meters"].first, "serial", organization

    expect(our_hash["customers"].first["name_e"]).not_to match(/Bill/)
    expect(our_hash["customers"].first["name_e"]).not_to be_empty
    expect(our_hash["customers"].first).to have_key "name"

    # dont touch other keys
    expect(our_hash["customers"].first["other"]).to eq("not critical")

    # deeper nested
    expect(our_hash["customers"].first["meters"].first["serial_e"]).not_to match(/abc123/)
    expect(our_hash["customers"].first["meters"].first["serial_e"]).not_to be_empty
    expect(our_hash["customers"].first["meters"].first).to have_key "serial"
  end

  it "geo - combined in one field" do
    our_hash = { "coord" => "50.1234567, 20.1" }

    described_class.geo_key(our_hash, "coord", nil, organization)

    expect(our_hash).to have_key "coord"
    geo_expectations(our_hash)
  end

  it "geo - separated field" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", organization)

    expect(our_hash).to have_key "lat"
    expect(our_hash).to have_key "lon"

    geo_expectations(our_hash)
  end

  it "geo - with key prefix" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", organization, key_prefix: "customer")

    %w[lat_e lon_e].each do |k|
      expect(our_hash).not_to have_key k
    end

    %w[customer_lat_e customer_lon_e].each do |k|
      expect(our_hash[k]).to be_present
      expect(our_hash[k]).not_to be_empty
    end
  end

  it "geo - with full key name" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", organization, key_full: true)

    %w[latitude_e longitude_e].each do |k|
      expect(our_hash[k]).to be_present
      expect(our_hash[k]).not_to be_empty
    end
  end

  it "can combine key prefix and full and mixes nothing up" do
    our_hash = { "lat" => "50.1234567", "lon" => "20.1" }

    described_class.geo_key(our_hash, "lat", "lon", organization, key_full: true, key_prefix: "meter")

    expect(our_hash["meter_latitude_e"]).not_to be_empty
    expect(our_hash["meter_longitude_e"]).not_to be_empty
  end

  def geo_expectations(our_hash)
    expect(our_hash["lat_e"]).not_to be_empty
    expect(our_hash["lon_e"]).not_to be_empty
  end

  it "geo - coord empty" do
    our_hash = { "coord" => "" }

    described_class.geo_key(our_hash, "coord", nil, organization)
    expect(our_hash["lat_e"]).to eq("")
    expect(our_hash["lon_e"]).to eq("")
  end

  it "geo - coord not parsable" do
    our_hash = { "coord" => "50.01, lon missing" }

    expect { described_class.geo_key(our_hash, "coord", nil, organization) }.to raise_error(ArgumentError)
    expect(our_hash["lat_e"]).to eq("")
    expect(our_hash["lon_e"]).to eq("")
  end

  it "handles missing lat param and puts empty values" do
    our_hash = { "lat" => nil, "lon" => "12.345" }

    described_class.geo_key(our_hash, "lat", "lon", organization)

    expect(our_hash["lat_e"]).to eq("")
    expect(our_hash["lon_e"]).to eq("")
  end
end
