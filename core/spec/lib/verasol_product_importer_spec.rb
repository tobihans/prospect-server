# frozen_string_literal: true

require "rails_helper"

describe VerasolProductImporter do
  let(:csv) { Rails.root.join("spec/fixtures/verasol_data_example.csv").read }

  it "import example file" do
    log = ""
    expect  do
      log = described_class.import(csv).join("\n")
    end.to change(CommonProduct, :count).by(5)

    expect(log).to include("processing row 5/5")
  end

  it "import file with failure" do
    # make sure name of last product is longer than 500 characters
    csv.gsub!("StarTimes Software", "H" * 500)
    log = ""
    expect  do
      log = described_class.import(csv).join("\n")
    end.to change(CommonProduct, :count).by(4)

    expect(log).to include "Can't add , record: 5"
    expect(log).to include '{"Total rows"=>5, "Saved successfully"=>4'
  end
end
