# frozen_string_literal: true

require "rails_helper"

describe Cleaner do
  let(:organization) { create :organization }
  let(:cleaner) { described_class.new(organization) }

  let(:example_data) do
    [
      {
        "serial_number" => "ser/ial/123",
        "customer_name" => "  Meter paFFay   ",
        "customer_address" => "  Some STREET 444  ",
        "customer_latitude" => 11.22334455,
        "customer_longitude" => 22.33445566,
        "customer_phone" => "004917512345678", # valid number
        "customer_birthday" => Time.zone.today.to_s,
        "seller_name" => "  schnucki ducki ",
        "seller_latitude" => 33.444555666,
        "seller_longitude" => 44.555666777,
        "seller_phone" => " +49 161 33 4566745", # invalid number
        "neighbour" => {
          "contact" => {
            "first_name" => "Hey",
            "last_name" => "There",
            "address" => "Dreamland"
          }
        }
      },
      {
        "serial_number" => "ano/ther/456",
        "customer_name" => "judo uergens"
      }
    ]
  end

  it "pseudonizes, blurs and deletes key" do
    cleaner.clean_key(example_data.first, "customer_name", :text, :all)
    cleaner.clean_key(example_data.first, "customer_phone", :phone_number, :all)
    expect(example_data.first).not_to have_key("customer_name")
    expect(example_data.first).not_to have_key("customer_phone")

    expect(example_data.first["customer_name_p"]).to eq(P14n.text(" Meter paFFay   "))
    expect(example_data.first["customer_name_e"]).not_to be_empty # non deterministic outcome

    expect(example_data.first["customer_phone_p"]).to eq(P14n.phone_number("004917512345678"))
    expect(example_data.first["customer_phone_e"]).not_to be_empty # non deterministic outcome
  end

  it "p14, birthday" do
    birthday = example_data.first["customer_birthday"]
    cleaner.clean_key(example_data.first, "customer_birthday", :birthday, :all)

    expect(example_data.first).not_to have_key("customer_birthday")

    expect(example_data.first["customer_birthday_p"]).to eq(P14n.text(birthday))
    expect(example_data.first["customer_birthday_e"]).not_to be_empty # non deterministic outcome
    expect(example_data.first["customer_birthday_b"]).to eq(Time.zone.today.year)
  end

  it "p14, birthday nil" do
    expect do
      cleaner.clean_key(example_data.first, "customer_birthday_not_there", :birthday, :all)
    end.not_to raise_error
  end

  it "p14, birthday unparsable" do
    cleaner.clean_key(example_data.first, "customer_name", :birthday, :all)
    expect(example_data.first).not_to have_key("customer_name")

    expect(example_data.first["customer_name_p"]).to eq(P14n.text(" Meter paFFay   "))
    expect(example_data.first["customer_name_e"]).not_to be_empty # non deterministic outcome
  end

  it "pseudonizes, blurs and DOES NOT delete key" do
    cleaner.clean_key(example_data.first, "customer_name", :text, :all, delete_key: false)
    expect(example_data.first).to have_key("customer_name")
    expect(example_data.first["customer_name"]).to eq("  Meter paFFay   ")
  end

  it "only pseudonomizes" do
    cleaner.clean_key(example_data.first, "customer_name", :text, :pseudonymize)
    expect(example_data.first["customer_name_p"]).to eq(P14n.text(" Meter paFFay   "))
    expect(example_data.first).not_to have_key("customer_name_e")
  end

  it "only encrypts" do
    cleaner.clean_key(example_data.first, "customer_name", :text, :encrypt)
    expect(example_data.first["customer_name_e"]).not_to be_empty # non deterministic outcome
    expect(example_data.first).not_to have_key("customer_name_p")
  end

  it "cleans and blurs geo" do
    cleaner.clean_key(example_data.first, %w[seller_latitude seller_longitude], :geo, :all, key_prefix: "seller")
    expect(example_data.first["seller_lat_e"]).not_to be_empty # non deterministic outcome
    expect(example_data.first["seller_lon_e"]).not_to be_empty # non deterministic outcome
    expect(example_data.first["seller_lat_p"]).to eq(P14n.p(33.444555666.round(6).to_s))
    expect(example_data.first["seller_lon_p"]).to eq(P14n.p(44.555666777.round(6).to_s))
  end

  it "nested field: pseudomizes, blurs and deletes key" do
    cleaner.clean_nested_key(example_data.first, "neighbour.contact.first_name", :text, :all)
    expect(example_data.first["neighbour"]["contact"]).not_to have_key("first_name")

    expect(example_data.first["neighbour"]["contact"]["first_name_p"]).to eq(P14n.text("Hey"))
    expect(example_data.first["neighbour"]["contact"]["first_name_e"]).not_to be_empty # non deterministic outcome
  end

  it "nested field: ignores non existing or nil keys" do
    expect do
      cleaner.clean_nested_key(example_data.first, "neighbourxyz.contact.first_name", :text, :all)
    end.not_to raise_error
  end

  it "nested hash: pseudomizes, blurs and deletes key" do
    # should work like neighbour.contact.*
    cleaner.clean_nested_key(example_data.first, "neighbour.contact", :text, :all)

    # check 2 first fields
    expect(example_data.first["neighbour"]["contact"]).not_to have_key("first_name")
    expect(example_data.first["neighbour"]["contact"]).not_to have_key("last_name")

    expect(example_data.first["neighbour"]["contact"]["first_name_p"]).to eq(P14n.text("Hey"))
    expect(example_data.first["neighbour"]["contact"]["first_name_e"]).not_to be_empty # non deterministic outcome

    expect(example_data.first["neighbour"]["contact"]["last_name_p"]).to eq(P14n.text("There"))
    expect(example_data.first["neighbour"]["contact"]["last_name_e"]).not_to be_empty # non deterministic outcome
  end
end
