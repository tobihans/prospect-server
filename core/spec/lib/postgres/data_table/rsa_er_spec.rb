# frozen_string_literal: true

require "rails_helper"

describe Postgres::DataTable::RsaEr do
  let(:rsaer) { Postgres::DataTable.new("shs").rsaer }
  let(:organization) { create :organization }
  let(:example_data) do
    [
      {
        "serial_number" => "ser/ial/123",
        "customer_name" => "  Meter paFFay   ",
        "customer_address" => "  Some STREET 444  ",
        "customer_latitude" => 11.22334455,
        "customer_longitude" => 22.33445566,
        "customer_phone" => "004917512345678", # valid number
        "seller_name" => "  schnucki ducki ",
        "seller_latitude" => 33.444555666,
        "seller_longitude" => 44.555666777,
        "seller_phone" => " +49 161 33 4566745" # invalid number
      },
      {
        "serial_number" => "ano/ther/456",
        "customer_name" => "judo uergens"
      }
    ]
  end

  let(:apply_rsa!) { rsaer.apply! example_data, organization }

  it "removes the original keys" do
    old_keys = example_data.first.keys.without "serial_number"
    apply_rsa!
    new_keys = example_data.first.keys.without "serial_number"

    expect(old_keys & new_keys).not_to be_empty
  end

  it "keeps the non-encrypted keys" do
    apply_rsa!
    expect(example_data.first["serial_number"]).to eq "ser/ial/123"
  end

  it "creates the correct data" do
    apply_rsa!
    expect(example_data.first["customer_name_e"]).not_to be_empty
    expect(example_data.first["customer_latitude_e"]).not_to be_empty
    expect(example_data.first["customer_longitude_e"]).not_to be_empty
    expect(example_data.first["customer_longitude_e"]).not_to eq "51.2222"
    expect(example_data.first["customer_phone_e"]).not_to be_empty
    expect(example_data.first["seller_name_e"]).not_to be_empty
    expect(example_data.first["seller_latitude_e"]).not_to be_empty
    expect(example_data.first["seller_longitude_e"]).not_to be_empty
    expect(example_data.first["seller_phone_e"]).not_to be_empty
  end

  it "iterates also to the second row" do
    apply_rsa!
    expect(example_data.second["customer_name_e"]).not_to be_empty
  end
end
