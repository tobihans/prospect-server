# frozen_string_literal: true

require "rails_helper"

describe Postgres::DataTable::Column do
  let(:column) { Postgres::DataTable.new("test").column("float_column") }

  describe "methods are matching the config" do
    describe "booleans" do
      it "#default?" do
        expect(column).not_to be_default
      end

      it "#required?" do
        expect(column).to be_required
      end

      it "#blurred?" do
        expect(column).not_to be_blurred
      end

      it "#encrypted?" do
        expect(column).not_to be_encrypted
      end

      it "#exclude_from_ingress?" do
        expect(column).not_to be_exclude_from_ingress
      end

      it "#exclude_from_egress?" do
        expect(column).not_to be_exclude_from_egress
      end
    end

    describe "passed through" do
      it "#type" do
        expect(column.type).to eq "float"
      end

      it "#desc" do
        expect(column.desc).to match(/floating around/)
      end

      it "#pseudonymized" do
        expect(column.pseudonymized).to be_falsey
      end

      it "#encrypted" do
        expect(column.encrypted).to be_falsey
      end

      it "#example" do
        expect(column.example).to eq 3.14
      end
    end

    describe "derived" do
      it "#exposed_for_ingress?" do
        expect(column).to be_exposed_for_ingress
      end

      it "#exposed_for_egress?" do
        expect(column).to be_exposed_for_egress
      end

      it "#sql_column_blurred" do
        expect(column.sql_column_blurred).to be_nil
      end

      it "#sql_column_pseudonymized" do
        expect(column.sql_column_pseudonymized).to be_nil
      end

      it "#sql_column_encrypted" do
        expect(column.sql_column_encrypted).to be_nil
      end

      it "#sql_column" do
        expect(column.sql_column).to eq "float_column"
      end

      it "#sql_columns" do
        expect(column.sql_columns).to eq ["float_column"]
      end

      it "#to_s" do
        expect(column.to_s).to eq "Postgres::DataTable::Column float_column"
      end

      it "#mandatory_visible??" do
        expect(column).not_to be_mandatory_visible
      end

      it "#optional_visible?" do
        expect(column).to be_optional_visible
      end
    end
  end

  it "returns nil when casting into unsupported type" do
    allow(column).to receive(:type).and_return("nonexistant_type")
    expect(column.cast("123")).to be_nil
  end
end
