# frozen_string_literal: true

require "rails_helper"

describe Postgres::DataTable::P14ner do
  let(:p14ner) { Postgres::DataTable.new("shs").p14ner }

  let(:example_data) do
    [
      {
        "serial_number" => "ser/ial/123",
        "customer_name" => "  Meter paFFay   ",
        "customer_address" => "  Some STREET 444  ",
        "customer_latitude" => 11.22334455,
        "customer_longitude" => 22.33445566,
        "customer_phone" => "004917512345678", # valid number
        "seller_name" => "  schnucki ducki ",
        "seller_latitude" => 33.444555666,
        "seller_longitude" => 44.555666777,
        "seller_phone" => " +49 161 33 4566745" # invalid number
      },
      {
        "serial_number" => "ano/ther/456",
        "customer_name" => "judo uergens"
      }
    ]
  end

  let(:apply_p14n!) { p14ner.apply! example_data }

  it "removes the original keys" do
    old_keys = example_data.first.keys.without "serial_number"
    apply_p14n!
    new_keys = example_data.first.keys.without "serial_number"

    expect(old_keys & new_keys).not_to be_empty
  end

  it "keeps the non-p14n keys" do
    apply_p14n!
    expect(example_data.first["serial_number"]).to eq "ser/ial/123"
  end

  it "creates the correct data" do
    apply_p14n!
    expected = {
      "serial_number" => "ser/ial/123",
      "customer_name" => "  Meter paFFay   ",
      "customer_name_p" => P14n.p("meter paffay"),
      "customer_address" => "  Some STREET 444  ",
      "customer_address_p" => P14n.p("some street 444"),
      "customer_latitude_p" => P14n.p("11.223345"),
      "customer_latitude" => 11.22334455,
      "customer_latitude_b" => 11.223,
      "customer_longitude" => 22.33445566,
      "customer_longitude_p" => P14n.p("22.334456"),
      "customer_longitude_b" => 22.334,
      "customer_phone" => "004917512345678", # valid number
      "customer_phone_p" => P14n.p("+4917512345678"),
      "seller_name" => "  schnucki ducki ",
      "seller_name_p" => P14n.p("schnucki ducki"),
      "seller_latitude" => 33.444555666,
      "seller_latitude_p" => P14n.p("33.444556"),
      "seller_latitude_b" => 33.445,
      "seller_longitude" => 44.555666777,
      "seller_longitude_p" => P14n.p("44.555667"),
      "seller_longitude_b" => 44.556,
      "seller_phone" => " +49 161 33 4566745",
      "seller_phone_p" => P14n.p("+49 161 33 4566745")
    }

    expect(example_data.first).to eq expected
  end

  it "iterates also to the second row" do
    apply_p14n!
    expected = { "serial_number" => "ano/ther/456", "customer_name_p" => P14n.p("judo uergens"), "customer_name" => "judo uergens" }

    expect(example_data.second).to eq expected
  end
end
