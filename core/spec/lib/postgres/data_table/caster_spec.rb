# frozen_string_literal: true

require "rails_helper"

describe Postgres::DataTable::Caster do
  let(:caster) { Postgres::DataTable.new("test").caster(input_data) }

  describe "validation" do
    let(:input_data) do
      [{ "uid" => "faux_uid",
         "bla" => "crumbles",
         "enum_column" => "d",
         "time_column" => 1234,
         "something_uid" => "134234" }]
    end

    describe "problem hash" do
      it "complains about required value" do
        expect(caster.errors).to include "Line 1: float_column is required"
      end

      it "complains about not castable time value" do
        expect(caster.errors).to include "Line 1: time_column can not be casted as datetime"
      end

      it "complains about a bad enum" do
        expect(caster.errors).to include "Line 1: enum_column can not be casted as enum with allowed values a, b, c"
      end

      it "complains about given value excluded from ingress" do
        expect(caster.errors).to include "Line 1: uid must not be present"
      end

      it "complains about the something_uid" do
        expect(caster.errors).to include "Line 1: something_uid must not be present"
      end

      it "is invalid" do
        expect(caster).not_to be_valid
      end

      it "has errors" do
        expect(caster.errors).to be_present
      end
    end
  end

  describe "casting" do
    let(:input_data) do
      [{ "int_column" => "12",
         "bool_column" => "yes",
         "float_column" => "7.8000",
         "time_column" => "2022-02-22 02:22:22",
         "name_column" => " My   Name   ",
         "enum_column" => "a",
         "additional_column" => "  4  4  ",
         "personal_column" => "   " }]
    end

    it "is valid" do
      expect(caster).to be_valid
    end

    it "casts integer" do
      expect(caster.data.first["int_column"]).to eq 12
    end

    it "casts boolean" do
      expect(caster.data.first["bool_column"]).to be true
    end

    it "properly casts false boolean" do
      # we must
      c = Postgres::DataTable.new("test").caster([{ "bool_column" => "false" }])
      expect(c.data.first["bool_column"]).to be false
    end

    it "casts time" do
      expect(caster.data.first["time_column"]).to eq "2022-02-22 02:22:22 UTC"
    end

    describe "casting dates" do
      it "casts date from string" do
        input_data = [{ "purchase_date" => "2011-12-13" }]
        caster = Postgres::DataTable.new("shs").caster(input_data)

        expect(caster.data.first["purchase_date"]).to eq "2011-12-13"
      end

      it "casts date from date like given by excel parser" do
        input_data = [{ "purchase_date" => Date.parse("2011-12-13") }]
        caster = Postgres::DataTable.new("shs").caster(input_data)

        expect(caster.data.first["purchase_date"]).to eq "2011-12-13"
      end
    end

    it "casts float" do
      expect(caster.data.first["float_column"]).to eq 7.8
    end

    it "takes enum" do
      expect(caster.data.first["enum_column"]).to eq "a"
    end

    it "takes string and cleans it" do
      expect(caster.data.first["name_column"]).to eq "My Name"
    end

    it "nils blank fields" do
      expect(caster.data.first["personal_column"]).to be_nil
    end

    it "leaves additional columns completely alone" do
      expect(caster.data.first["additional_column"]).to eq "  4  4  "
    end
  end

  describe "column ingress_autogenerated?" do
    it "can autogenerate_ids" do
      caster = Postgres::DataTable.new("custom").caster([])
      expect(caster.autogenerate_id).to match(/auto_\d+_\w{4}/)
    end

    it "autogenerated data for column if not given" do
      caster = Postgres::DataTable.new("custom").caster([{ "some_col" => "bla" }])
      expect(caster.data.first["external_id"]).to match(/auto_\d+_\w{4}/)
    end

    it "uses given value and not autogenerates if present" do
      caster = Postgres::DataTable.new("custom").caster([{ "external_id" => "some_id" }])
      expect(caster.data.first["external_id"]).to eq "some_id"
    end
  end
end
