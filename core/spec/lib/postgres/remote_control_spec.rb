# frozen_string_literal: true

require "rails_helper"

describe Postgres::RemoteControl do
  it "can setup a organization twice without raising an error" do
    p = create :organization
    expect { described_class.setup_organization!(p) }.not_to raise_error
    expect { described_class.setup_organization!(p) }.not_to raise_error
  end

  describe "fn_create_org_role" do
    def call_fn(name, pass)
      described_class.query! described_class.fn_create_org_role_q(name, pass)
    end

    it "can install the function without errors" do
      expect { described_class.install_fn_org_role! }.not_to raise_error
    end

    it "can work with proper parameters" do
      expect do
        call_fn "prospect_test#{test_number}_prospect_organization_4", "12345678901"
      end.not_to raise_error
    end

    it "fails for wrong prefix of name" do
      expect  do
        call_fn "prospect_dev_prospect_organization_4", "12345678901"
      end.to raise_error(/IORNP/)
    end

    it "fails with missing org id" do
      expect  do
        call_fn "prospect_test#{test_number}_prospect_organization_", "12345678901"
      end.to raise_error(/IORNP/)
    end

    it "fails with bad role name" do
      expect  do
        call_fn "prospect_test#{test_number}_prospect_organizzation_4", "12345678901"
      end.to raise_error(/IORNP/)
    end

    it "fails for too short pw" do
      expect  do
        call_fn "prospect_test#{test_number}_prospect_organization_4", "12345678"
      end.to raise_error(/IORNP/)
    end
  end

  describe "migration methods" do
    # no idea how to test properly, as the callbacks create that when I build the things with factories
    let!(:organizations) { create_list :organization, 3 } # rubocop:disable RSpec/LetSetup

    it "#setup_all_organizations" do
      expect { described_class.setup_all_organizations! }.not_to raise_error
    end

    it "#create_all_views" do
      expect { described_class.create_all_views! }.not_to raise_error
    end
  end

  it "return current postgres version" do
    expect(described_class.current_version).to eq 1400

    allow(described_class).to receive(:query!).and_return(nil)
    expect(described_class.current_version).to be false
  end

  describe "organization views" do
    let!(:organization) { create(:source).organization }

    it "#all_organization_views lists all organization views" do
      expect(described_class.all_organization_views.sort).
        to include "prospect_test#{test_number}_prospect_organization_#{organization.id}.data_custom",
                   "prospect_test#{test_number}_prospect_organization_#{organization.id}.data_test"
    end

    it "#all_organization_views can filter for data category" do
      expect(described_class.all_organization_views(data_category: :test)).
        to eq ["prospect_test#{test_number}_prospect_organization_#{organization.id}.data_test"]
    end

    it "#drop_all_organization_views! drops all views" do
      described_class.drop_all_organization_views!

      expect(described_class.all_organization_views).to be_empty
    end

    it "#drop_all_organization_views! can filter for data category DEPRECATED" do
      described_class.drop_all_organization_views! :test

      expect(described_class.all_organization_views).
        not_to include "prospect_test#{test_number}_prospect_organization_#{organization.id}.data_test"
      expect(described_class.all_organization_views).to be_empty
    end
  end

  describe "timescale" do
    it "check if timescale is available" do
      expect(described_class.timescale_available?).to be true
    end

    it "check if timescale is enabled" do
      expect(described_class.timescale_enabled?).to be true
    end
  end

  it "#after_migration! runs without complaining" do
    expect { described_class.after_migration! }.not_to raise_error
  end

  it "#after_migration! outputs nothing when it works" do
    expect { described_class.after_migration! }.not_to output(/Please run manually/).to_stdout
  end

  it "#after_migration! prints instructions when failing" do
    allow(described_class).to receive(:setup_all_organizations!).and_raise("oops")
    expect { described_class.after_migration! }.to output(/Please run manually/).to_stdout
  end

  it "can safely update views with changing schema" do
    described_class.drop_all_custom_views!

    expect(described_class.all_custom_views_everywhere).to be_empty

    described_class.create_view_safe! "CREATE OR REPLACE VIEW custom_test_view AS SELECT id AS someid, name AS bla FROM organizations"
    expect(described_class.all_custom_views_everywhere).to eq %w[public.custom_test_view]

    described_class.create_view_safe! "CREATE OR REPLACE VIEW custom_test_view AS SELECT name AS bla, created_at AS keks, id AS someid FROM organizations"
    expect(described_class.all_custom_views_everywhere).to eq %w[public.custom_test_view]

    described_class.create_view_safe! "CREATE OR REPLACE VIEW custom_test_view2 AS SELECT id AS renamedid FROM organizations"
    expect(described_class.all_custom_views_everywhere.sort).to eq %w[public.custom_test_view public.custom_test_view2]

    described_class.create_view_safe! "CREATE OR REPLACE VIEW custom_test_view AS SELECT id AS renamedid FROM organizations"
    expect(described_class.all_custom_views_everywhere.sort).to eq %w[public.custom_test_view public.custom_test_view2]

    # we mess with transactions, so we clean up manually
    described_class.drop_all_custom_views!
  end

  describe "#migration_without_views" do
    let!(:source) { create :source, :custom } # rubocop:disable RSpec/LetSetup
    let!(:count_cv) { described_class.all_custom_views_everywhere.count }
    let!(:count_ov) { described_class.all_organization_views.count + 2 }

    it "can drop all views completely" do
      expect(described_class.all_custom_views_everywhere).not_to be_empty
      expect(described_class.all_organization_views).not_to be_empty

      described_class.migration_without_views do
        expect(described_class.all_custom_views_everywhere).to be_empty
        expect(described_class.all_organization_views).to be_empty
      end

      expect(described_class.all_custom_views_everywhere).not_to be_empty
      expect(described_class.all_organization_views).not_to be_empty
      expect(described_class.all_custom_views_everywhere.count).to eq count_cv
      expect(described_class.all_organization_views.count).to eq count_ov
    end

    it "can drop multiple data_category views only" do
      expect(described_class.all_custom_views_everywhere).not_to be_empty
      expect(described_class.all_organization_views).to include(/data_grids_ts/)
      expect(described_class.all_organization_views).to include(/data_meters$/)

      described_class.migration_without_views :grids_ts, :meters do # DEPRECATED
        expect(described_class.all_custom_views_everywhere).to be_empty
      end

      expect(described_class.all_custom_views_everywhere.count).to eq count_cv
      expect(described_class.all_organization_views.count).to eq count_ov
      expect(described_class.all_organization_views).to include(/data_grids_ts/)
      expect(described_class.all_organization_views).to include(/data_meters$/)
    end

    it "drops custom views if data_category is custom" do
      expect(described_class.all_custom_views_everywhere).not_to be_empty
      expect(described_class.all_organization_views).to include(/data_custom/)

      described_class.migration_without_views :meters, :custom do # DEPRECATED
        expect(described_class.all_custom_views_everywhere).to be_empty
      end

      expect(described_class.all_custom_views_everywhere.count).to eq count_cv
      expect(described_class.all_organization_views.count).to eq count_ov
      expect(described_class.all_organization_views).to include(/custom/)
    end
  end

  describe "custom views" do
    let!(:source) { create :source, :custom }

    it "#drop_view" do
      expect(described_class).to receive(:query!).with("DROP VIEW IF EXISTS some_view CASCADE")
      described_class.drop_view! "some_view"
    end

    it "#drop_all_custom_views! is brutal" do
      expect(described_class).to receive(:drop_view!).
        with(Naming.db_visibility_custom_view(source.project.default_visibility, source))
      described_class.drop_all_custom_views!
    end

    it "#query_drop_view builds a nice query" do
      expect(described_class.query_drop_view("some_view")).to eq "DROP VIEW IF EXISTS some_view CASCADE"
    end
  end

  describe "as organization" do
    let(:visibility) { create :visibility }
    let(:organization) { visibility.organization }

    it "#count_as_organization raises error if neither data_category nor custom_view" do
      expect { described_class.count_as_organization organization }.to raise_error "Either data_category or custom_view must be given"
    end

    it "#select_as_organization raises error if neither data_category nor custom_view" do
      expect { described_class.select_as_organization organization, "*" }.to raise_error "Either data_category or custom_view must be given"
    end
  end
end
