# frozen_string_literal: true

# rubocop:disable RSpec/NestedGroups

require "rails_helper"

describe Postgres::DataTable do
  Postgres::RemoteControl.data_categories.each do |cat|
    data_table = described_class.new cat

    describe "DataTable #{cat}" do
      describe "proper yaml documentation" do
        it "gives description" do
          expect(data_table.structure[:desc]).to be_present
        end

        it "has uid as first column" do
          expect(data_table.columns.first.name).to match(/.*uid/)
        end

        %i[type desc example].each do |field|
          it "gives #{field} for all columns" do
            expect(data_table.columns.map(&field)).to all be_present
          end
        end

        data_table.columns.select { _1.type == "enum" }.each do |c|
          it "check if enum column #{cat}.#{c.name} has values defined" do
            expect(c.values).not_to be_nil
            expect(c.values.count).not_to be_zero
          end
        end

        describe "p14n attributes" do
          it "only allows certain values for pseudonymization" do
            expect(data_table.columns.map(&:pseudonymized)).to all be_in([nil, "text", "phone_number", "geo"])
          end

          it "makes sure pseudonymized phone columns use phone p14n" do
            p14n_phone_columns = data_table.columns.select(&:pseudonymized?).
                                 select { |c| c.name.include?("phone") }
            expect(p14n_phone_columns.map(&:pseudonymized)).to all eq "phone_number"
          end

          it "makes sure lat lon columns use geo p14n" do
            p14n_geo_columns = data_table.columns.select(&:pseudonymized?).
                               select { |c| c.name.include?("_lat") || c.name.include?("_lon") }
            expect(p14n_geo_columns.map(&:pseudonymized)).to all eq "geo"
          end

          it "makes sure geo p14n col name always end with latitude or longitude" do
            p14n_geo_types = data_table.columns.map(&:p14n_geo_attribs).compact.pluck(:type)
            expect(p14n_geo_types).to all be_in(%i[latitude longitude])
          end

          it "makes sure all latitude fields have a matching longitude field" do
            p14n_geo_attribs = data_table.columns.map(&:p14n_geo_attribs).compact
            types = p14n_geo_attribs.pluck(:type).tally
            prefixes = p14n_geo_attribs.pluck(:prefix).tally

            expect(types[:latitude]).to eq types[:longitude]
            expect(prefixes.values).to all eq 2
          end
        end

        describe "encryption attributes" do
          it "only allows certain values for encryption" do
            expect(data_table.columns.map(&:encrypted)).to all be_in([nil, "text", "phone_number", "geo"])
          end

          it "makes sure encrypted phone columns use standard text encryption" do
            enc_phone_columns = data_table.columns.select(&:encrypted?).select { |c| c.name.include?("phone") }
            expect(enc_phone_columns.map(&:encrypted)).to all eq "text"
          end

          it "makes sure lat lon columns use geo encryption" do
            enc_geo_columns = data_table.columns.select(&:encrypted?).
                              select { |c| c.name.include?("_lat") || c.name.include?("_lon") }
            expect(enc_geo_columns.map(&:encrypted)).to all eq "geo"
          end

          it "makes sure geo encryption col name always end with latitude or longitude" do
            enc_geo_types = data_table.columns.map(&:encrypt_geo_attribs).compact.pluck(:type)
            expect(enc_geo_types).to all be_in(%i[latitude longitude])
          end

          it "makes sure all encrypted latitude fields have a matching longitude field" do
            encrypt_geo_attribs = data_table.columns.map(&:encrypt_geo_attribs).compact
            types = encrypt_geo_attribs.pluck(:type).tally
            prefixes = encrypt_geo_attribs.pluck(:prefix).tally

            expect(types[:latitude]).to eq types[:longitude]
            expect(prefixes.values).to all eq 2
          end
        end
      end

      describe "matches postgres table structure" do
        it "has all columns described in config" do
          actual_columns = ActiveRecord::Base.connection.columns(data_table.table_name).map(&:name)

          expect(data_table.sql_columns).to match_array actual_columns
        end

        described_class.new(cat).columns.each do |col|
          describe col.to_s do
            if col.sql_column
              describe "stored plainly" do
                it "has base column" do
                  expect(ActiveRecord::Base.connection).
                    to be_column_exists(data_table.table_name, col.sql_column)
                end

                it "has correct type" do
                  expect(ActiveRecord::Base.connection).
                    to be_column_exists(data_table.table_name, col.sql_column, col.type)
                end

                it "is not nullable if required" do
                  if col.required?
                    expect(ActiveRecord::Base.connection).
                      to be_column_exists(data_table.table_name, col.sql_column, null: false)
                  end
                end

                it "is nullable if not required" do
                  unless col.required?
                    expect(ActiveRecord::Base.connection).
                      to be_column_exists(data_table.table_name, col.sql_column, null: true)
                  end
                end
              end
            end

            it "has blurred _b float column if blurred" do
              if col.sql_column_blurred
                expect(ActiveRecord::Base.connection).
                  to be_column_exists(data_table.table_name, col.sql_column_blurred, :float)
              end
            end

            it "has p14 _p string column if pseudonymized" do
              if col.sql_column_pseudonymized
                expect(ActiveRecord::Base.connection).
                  to be_column_exists(data_table.table_name, col.sql_column_pseudonymized, :string)
              end
            end

            it "has encrypted _e string column if encrypted" do
              if col.sql_column_encrypted
                expect(ActiveRecord::Base.connection).
                  to be_column_exists(data_table.table_name, col.sql_column_encrypted, :string)
              end
            end

            it "is primary if marked as one" do
              if col.primary_key?
                expect(ActiveRecord::Base.connection.primary_keys(data_table.table_name)).
                  to(be_any { |i| Array(i).include?(col.name) })
              end
            end

            it "is indexed uniquely if name is uid" do
              if col.name == "uid"
                indexed = ActiveRecord::Base.connection.indexes(data_table.table_name).
                          any? { |i| i.columns.include?(col.name) && i.unique }
                primary_key = ActiveRecord::Base.connection.primary_keys(data_table.table_name).
                              any? { |i| Array(i).include?(col.name) }

                expect(indexed || primary_key).to be_truthy
              end
            end

            describe "uid column" do
              if "uid".in?(col.name)
                it "is uniquely indexed or primary key" do
                  indexed = ActiveRecord::Base.connection.indexes(data_table.table_name).
                            any? { |i| i.columns.include?(col.name) }
                  primary_key = ActiveRecord::Base.connection.primary_keys(data_table.table_name).
                                any? { |i| Array(i).include?(col.name) }

                  expect(indexed || primary_key).to be_truthy
                end

                it "is uid?" do
                  expect(col).to be_uid
                end

                it "non manual_uid has uid parts" do
                  expect(col.uid_parts).to be_present unless col.manual_uid?
                end

                it "manual uis has no uid parts" do
                  expect(col.uid_parts).to be_blank if col.manual_uid?
                end

                col.uid_parts.each do |uid_part|
                  it "uid part #{uid_part} exists as column" do
                    expect(ActiveRecord::Base.connection).
                      to be_column_exists(data_table.table_name, uid_part)
                  end
                end

                it "is excluded from ingress" do
                  expect(col).to be_exclude_from_ingress
                end
              end
            end

            describe "non uid column" do
              unless "uid".in?(col.name)
                it "is not uid?" do
                  expect(col).not_to be_uid
                end

                it "has no uid parts" do
                  expect(col.uid_parts).to eq []
                end
              end
            end
          end
        end
      end

      describe "update triggers and history" do
        let(:table_triggers) do
          r = Postgres::RemoteControl.query! <<~SQL.squish
            SELECT trigger_name, action_timing, event_manipulation
            FROM information_schema.triggers
            WHERE event_object_table = '#{data_table.table_name}'
              AND trigger_name != 'ts_insert_blocker'
          SQL
          r.values
        end

        if data_table.history?
          describe "#history? == true" do
            it "has 3 triggers" do
              expect(table_triggers.count).to eq 3
            end

            it "triggers track_history_#{cat} AFTER UPDATE and DELETE" do
              expect(table_triggers).to include %W[track_history_#{cat} AFTER UPDATE]
              expect(table_triggers).to include %W[track_history_#{cat} AFTER DELETE]
            end

            it "triggers before_update_checks_#{cat} BEFORE UPDATE" do
              expect(table_triggers).to include %W[before_update_checks_#{cat} BEFORE UPDATE]
            end
          end
        else
          describe "#history? == false" do
            it "has no triggers" do
              expect(table_triggers).to be_empty
            end
          end
        end
      end
    end
  end

  describe "class methods" do
    it "#all" do
      expect(described_class.all).to be_an Array
      expect(described_class.first).to be_a described_class
    end

    it "#find" do
      expect(described_class.find("test").data_category).to eq "test"
    end

    it "#non_custom" do
      expect(described_class.non_custom.map(&:data_category)).not_to include "custom"
    end
  end

  describe "active record abstration classes" do
    let(:organization1) { create :organization }
    let(:project1) { create :project, organization: organization1 }
    let(:data_source1) { create :source, project: project1 }
    let(:import1) do
      create :import, source: data_source1,
                      file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                      ingestion_finished_at: Time.zone.now,
                      ingestion_started_at: Time.zone.now
    end

    let(:organization2) { create :organization }
    let(:project2) { create :project, organization: organization2 }
    let(:data_source2) { create :source, project: project2 }
    let(:import2) do
      create :import, source: data_source2,
                      file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                      ingestion_finished_at: Time.zone.now,
                      ingestion_started_at: Time.zone.now
    end

    it "queries only the organization data" do
      test_example_insert import1, "test"

      entry = Data::Test.first.as_json
      entry.delete "id"
      entry["uid"] = "Some other uid"
      entry["organization_id"] = organization2.id
      entry["import_id"] = import2.id
      entry["source_id"] = data_source2.id

      # https://dev.to/truemark/fix-rails-auto-increment-id-postgres-error-id
      # for whatever reason we otherwise it tries to insert with id 1
      ActiveRecord::Base.connection.reset_pk_sequence! "data_test"

      Data::Test.create! entry

      ar1 = described_class.ar("test", organization1)
      ar2 = described_class.ar("test", organization2)

      expect(ar1.count).to eq 1
      expect(ar2.count).to eq 1

      expect(ar1.first.uid).to start_with "The UUID - "
      expect(ar2.first.uid).to eq "Some other uid"

      expect(ar1.table_name.split(".").first).to eq Naming.db_organization_schema organization1
      expect(ar2.table_name.split(".").first).to eq Naming.db_organization_schema organization2

      par = described_class.public_ar("test")
      expect(par.count).to eq 2
    end
  end

  describe "test table" do
    let(:data_table) { described_class.new "test" }

    it "has a string representation" do
      expect(data_table.to_s).to eq "Postgres::DataTable test"
    end

    describe "examples" do
      it "can give us example data for all columns" do
        examples = data_table.examples
        expect(examples.values).to all be_present
        expect(examples.keys.first).to eq "int_column"
        expect(examples["float_column"]).to eq 3.14
      end

      it "can give us an example for a certain column" do
        expect(data_table.example(:int_column)).to eq 5
      end
    end

    describe "config" do
      it "#desc" do
        expect(data_table.desc).to match(/This is the description of the table/)
      end

      it "#expose_to_out_api?" do
        expect(data_table).to be_exposed_to_in_api
      end

      it "#expose_to_in_api?" do
        expect(data_table).to be_exposed_to_out_api
      end
    end

    describe "input caster" do
      it "can build a input caster" do
        expect(data_table.caster([{}])).to be_a Postgres::DataTable::Caster
      end
    end

    describe "p14n" do
      it "builds us a p14n converter" do
        expect(data_table.p14ner).to be_a Postgres::DataTable::P14ner
      end
    end

    describe "rsaer" do
      it "builds us a rsa encryption converter" do
        expect(data_table.rsaer).to be_a Postgres::DataTable::RsaEr
      end
    end
  end

  describe "for custom data" do
    let(:data_table) { described_class.new "custom" }

    it "#castable_as_timestamp?" do
      expect(data_table).to be_castable_as_timestamp("2022-04-13 17:18:33")
      expect(data_table).to be_castable_as_timestamp("2022-04-13")
      expect(data_table).to be_castable_as_timestamp("")
      expect(data_table).to be_castable_as_timestamp(nil)

      expect(data_table).not_to be_castable_as_timestamp("2022-04-35")
      expect(data_table).not_to be_castable_as_timestamp("2022-04-35 25:61:61")
      expect(data_table).not_to be_castable_as_timestamp("3 Dez 1990")
      expect(data_table).not_to be_castable_as_timestamp("something different")
      expect(data_table).not_to be_castable_as_timestamp("123")
    end

    it "#castable_as_numeric?" do
      expect(data_table).to be_castable_as_numeric("123")
      expect(data_table).to be_castable_as_numeric("456.789")
      expect(data_table).to be_castable_as_numeric("")
      expect(data_table).to be_castable_as_numeric(nil)

      expect(data_table).not_to be_castable_as_numeric("something different")
      expect(data_table).not_to be_castable_as_numeric("2022-04-35")
      expect(data_table).not_to be_castable_as_numeric("a")
    end

    describe "#custom_column_conf" do
      it "keeps out the fixed db columns" do
        data = [{ "external_id" => "abc", "source_id" => 5, "col" => "abc" }]
        expect(data_table.custom_column_conf(data)).to eq [%w[col text]]
      end

      it "only date, datetime and blank becomes timestamp" do
        data = ["", "2021-01-07 17:15:13", "", "2022-02-01", ""].
               map { |v| { "col" => v } }
        expect(data_table.custom_column_conf(data)).to eq [%w[col timestamp]]
      end

      it "prevents timestamp if any value is not parseable" do
        data = ["", "2021-01-07 17:15:13", "", "1", "2022-02-01", ""].
               map { |v| { "col" => v } }
        expect(data_table.custom_column_conf(data)).to eq [%w[col text]]
      end

      it "only int or float or blank becomes numeric" do
        data = ["", "5", "", "20.337", "50123987", nil].
               map { |v| { "col" => v } }
        expect(data_table.custom_column_conf(data)).to eq [%w[col numeric]]
      end

      it "makes only blank columns text" do
        data = ["", "", "", "", nil, ""].
               map { |v| { "col" => v } }
        expect(data_table.custom_column_conf(data)).to eq [%w[col text]]
      end

      it "defaults other mixed things as text" do
        data = ["", "2021-01-07 17:15:13", "", "123", "2022-02-01", ""].
               map { |v| { "col" => v } }
        expect(data_table.custom_column_conf(data)).to eq [%w[col text]]
      end
    end
  end

  describe "triggers and functions" do
    before do
      q = <<~SQL.squish
        INSERT INTO data_test
          (uid, int_column, float_column, personal_column, created_at, updated_at, source_id, custom, data_origin, import_id, organization_id)
        VALUES
          ('some_uid_1', 7, 8.9, 'personal name', '2022-02-02 02:22:22', '2022-02-02 02:22:22', 2, '{"some": {"nested": "value"}}', 'api', 3, 1)
      SQL
      Postgres::RemoteControl.query! q
    end

    def history_entries_count
      Postgres::RemoteControl.query!("SELECT COUNT(*) FROM history").values.first.first
    end

    def expect_history_entries(amount)
      old_count = history_entries_count
      yield
      expect(history_entries_count).to eq(old_count + amount)
    end

    def update_test_data(set_part)
      Postgres::RemoteControl.query! "UPDATE data_test SET #{set_part} WHERE uid = 'some_uid_1'"
    end

    def last_test_data
      Postgres::RemoteControl.query!("SELECT * FROM data_test ORDER BY uid DESC LIMIT 1").
        to_a.first.with_indifferent_access
    end

    describe "before update update_checks_and_deduplication()" do
      it "does nothing when values except meta are the same" do
        update_test_data <<-SQLSET
            created_at = '1992-03-07 17:18:19', updated_at = '1992-03-07 17:18:19',
            source_id = 42, import_id = 42, int_column = 7
        SQLSET

        expect(last_test_data[:created_at]).to be < 1.year.ago
        expect(last_test_data[:updated_at]).to be < 1.year.ago
        expect(last_test_data[:source_id]).to eq 2
        expect(last_test_data[:import_id]).to eq 3
      end

      it "raises when trying to update the organization_id" do
        expect { update_test_data "organization_id = 77" }.to raise_error(/ORGUP/)
      end

      it "keeps original created_at, source_id, and import_id and writes new into last_" do
        update_test_data <<-SQLSET
            created_at = '1992-03-07 17:18:19', updated_at = '1992-03-07 17:18:19',
            source_id = 42, import_id = 43, int_column = 99
        SQLSET

        # keeping the original
        expect(last_test_data[:created_at]).to be < 1.year.ago
        expect(last_test_data[:source_id]).to eq 2
        expect(last_test_data[:import_id]).to eq 3

        # putting new import and source into the last_... fields
        expect(last_test_data[:last_source_id]).to eq 42
        expect(last_test_data[:last_import_id]).to eq 43

        # just for good measure we actually update data :D
        expect(last_test_data[:updated_at]).not_to eq last_test_data[:created_at]
        expect(last_test_data[:int_column]).to eq 99
      end
    end

    describe "after update track_changes_in_history()" do
      it "tracks changes of some normal column" do
        expect_history_entries(1) do
          update_test_data "int_column = 5"
        end
      end

      it "tracks changes on updated custom json" do
        expect_history_entries(1) do
          update_test_data %q(custom = '{"some": {"nested": "different_value"}}')
        end
      end

      it "does not track changes when nothing changes" do
        expect_history_entries(0) do
          update_test_data %q(custom = '{"some": {"nested": "value"}}', int_column = 7)
        end
      end

      it "does not track update when only timestamp or source and import would get changed" do
        expect_history_entries(0) do
          update_test_data <<-SQLSET
            created_at = '1992-03-07 17:18:19', updated_at = '1992-03-07 17:18:19',
            source_id = 42, import_id = 42
          SQLSET
        end
      end

      it "works with history and update checks across multiple updates" do
        expect_history_entries(2) do
          update_test_data "import_id = 111, int_column = 100"
          update_test_data "import_id = 222, int_column = 200"
        end

        expect(last_test_data[:import_id]).to eq 3
        expect(last_test_data[:last_import_id]).to eq 222
        expect(last_test_data[:int_column]).to eq 200
      end

      it "creates row in history with the right properties" do
        ## a very long and explicit test! ;)
        #
        # Testing what shows up in history table for each and every row
        # So that there is really no stupid special case on some stupid column

        expect_history_entries(1) do
          update_test_data <<-SQLSET
            created_at = '1992-03-07 17:18:19', updated_at = '1992-03-07 17:18:19',
            source_id = 42, import_id = 43,
            int_column = 5, float_column = 3.33, custom = '{"some": {"nested": "different_value"}}',
            bool_column = TRUE, enum_column = 'b', time_column = '1990-03-10 10:03:19',
            name_column_p = 'who is schnacki', something_uid = 'uid_88745', personal_column = 'expose yourself'
          SQLSET
        end

        h = Postgres::RemoteControl.query!("SELECT * FROM HISTORY ORDER BY uid DESC LIMIT 1").
            to_a.first.with_indifferent_access

        # meta columns
        expect(h[:created_at]).to be > 5.seconds.ago
        expect(h[:reverted_at]).to be_nil
        expect(h[:uid]).to eq "some_uid_1"

        expect(h[:organization_id]).to eq 1
        expect(h[:source_id]).to eq 42
        expect(h[:import_id]).to eq 43

        # now check changes jsonb...
        o = JSON.parse(h[:old_row]).with_indifferent_access
        n = JSON.parse(h[:new_row]).with_indifferent_access

        # ...staying the same
        expect(o[:id]).to eq n[:id]
        expect(o[:created_at]).to eq n[:created_at] # stays the same

        expect(o[:uid]).to eq "some_uid_1"
        expect(n[:uid]).to eq "some_uid_1"

        expect(o[:organization_id]).to eq 1
        expect(n[:organization_id]).to eq 1

        expect(o[:source_id]).to eq 2
        expect(n[:source_id]).to eq 2

        expect(o[:import_id]).to eq 3
        expect(n[:import_id]).to eq 3

        expect(o[:data_origin]).to eq "api"
        expect(n[:data_origin]).to eq "api"

        # ...or changing
        expect(o[:custom]).to eq({ "some" => { "nested" => "value" } })
        expect(n[:custom]).to eq({ "some" => { "nested" => "different_value" } })

        expect(o[:int_column]).to eq 7
        expect(n[:int_column]).to eq 5

        expect(o[:updated_at]).not_to eq n[:updated_at]
        expect(n[:updated_at]).to be > 5.seconds.ago.to_s

        expect(o[:bool_column]).to be_nil
        expect(n[:bool_column]).to be_truthy

        expect(o[:enum_column]).to be_nil
        expect(n[:enum_column]).to eq "b"

        expect(o[:time_column]).to be_nil
        expect(n[:time_column]).to eq "1990-03-10T10:03:19"

        expect(o[:float_column]).to eq 8.9
        expect(n[:float_column]).to eq 3.33

        expect(o[:name_column_p]).to be_nil
        expect(n[:name_column_p]).to eq "who is schnacki"

        expect(o[:something_uid]).to be_nil
        expect(n[:something_uid]).to eq "uid_88745"

        expect(o[:last_source_id]).to be_nil
        expect(n[:last_source_id]).to eq 42

        expect(o[:last_import_id]).to be_nil
        expect(n[:last_import_id]).to eq 43

        expect(o[:personal_column]).to eq "personal name"
        expect(n[:personal_column]).to eq "expose yourself"
      end
    end

    describe "deletion" do
      let(:delete_record!) { Postgres::RemoteControl.query! "DELETE FROM data_test WHERE uid = 'some_uid_1'" }

      it "removes record" do
        old_cnt = Postgres::RemoteControl.query!("SELECT * FROM data_test").ntuples
        delete_record!
        expect(Postgres::RemoteControl.query!("SELECT * FROM data_test").ntuples).to eq old_cnt - 1
      end

      it "creates a history entry" do
        old_cnt = Postgres::RemoteControl.query!("SELECT * FROM history").ntuples
        delete_record!
        expect(Postgres::RemoteControl.query!("SELECT * FROM history").ntuples).to eq old_cnt + 1
      end

      it "has history entry with correct values" do
        delete_record!
        h = Postgres::RemoteControl.query!("SELECT * FROM history").to_a.first.with_indifferent_access
        o = JSON.parse(h[:old_row]).with_indifferent_access

        expect(h[:id]).to be_present
        expect(h[:created_at]).to be > 5.seconds.ago
        expect(h[:data_table]).to eq "data_test"
        expect(h[:import_id]).to eq 3
        expect(h[:new_row]).to be_nil
        expect(h[:organization_id]).to eq 1
        expect(h[:reverted_at]).to be_nil
        expect(h[:uid]).to eq "some_uid_1"

        # just cursory checks so we know old_row is there
        expect(o[:uid]).to eq "some_uid_1"
        expect(o[:custom]).to eq({ "some" => { "nested" => "value" } })
        expect(o[:import_id]).to eq 3
        expect(o[:int_column]).to eq 7
      end
    end
  end
end

# rubocop:enable RSpec/NestedGroups
