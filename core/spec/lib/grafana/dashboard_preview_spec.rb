# frozen_string_literal: true

require "rails_helper"

describe Grafana::DashboardPreview do
  it "fetches preview image, saves it and provides it" do
    VCR.use_cassette :grafana_preview_fetch_image do
      allow(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).and_call_original
      org = create :organization, id: 12_345, grafana_org_id: 12_345

      list = Grafana::Client.new.dashboard_list(org_id: org.grafana_org_id)
      expect(list).not_to be_empty

      db = list.first

      result = described_class.create_grafana_dashboard_preview(db[:url], org.grafana_org_id)

      # png magic number
      # https://www.w3.org/TR/PNG-Structure.html
      sign_dec = [137, 80, 78, 71, 13, 10, 26, 10]

      expect(result[0..7].unpack("C*")).to eq sign_dec

      described_class.save_preview(result, db[:uid], org.grafana_org_id)

      expect(Dir).to exist(described_class::PATH)
      # 12345 the grafana_org_id as preview image prefix
      expect(Dir["#{described_class::PATH}/12345_your-data.png"].count).to be 1

      image = described_class.get_preview(db[:uid], org.grafana_org_id)

      expect(image).to eq "/dashboard_preview/12345_your-data.png"
    end
  end
end
