# frozen_string_literal: true

require "rails_helper"
require "json"

describe "generic /api/v1/in", type: :request do
  def make_post!(data, auth, endpoint = "/api/v1/in/test")
    h = { "Authorization" => auth }
    post endpoint, params: { data: [data] }, headers: h, as: :json
  end

  let!(:source) { create :source, kind: "api_push", data_category: "test" }
  let!(:shs_source) { create :source, kind: "api_push", data_category: "shs" }
  let(:data_good) { { float_column: "1.100", name_column: "  Meter Paffay  ", something_else: 4 } }

  context "when we expect a negative result" do
    it "returns 401 on wrong auth params" do
      make_post! data_good, "dsfsdf"
      expect(response).to have_http_status(:unauthorized)
    end

    it "returns 401 on missing auth params" do
      make_post! data_good, nil
      expect(response).to have_http_status(:unauthorized)
    end

    it "returns 400 on missing params" do
      data = data_good.merge float_column: nil
      make_post! data, source.secret

      expect(response).to have_http_status(:bad_request)
    end

    it "returns 400 on wrong enum values" do
      data = data_good.merge enum_column: "aa"
      make_post! data, source.secret

      expect(response.body).to include "data[enum_column] does not have a valid value"
      expect(response).to have_http_status(:bad_request)
    end

    it "returns 400 on forbidden param" do
      data = data_good.merge uid: "aa"
      make_post! data, source.secret

      expect(response.body).to include "data[uid] is forbidden and must not be present"
      expect(response).to have_http_status(:bad_request)
    end
  end

  context "when we expect a positive result" do
    describe "inactive source - demo mode" do
      it "returns 202" do
        source.update! activated_at: nil
        make_post! data_good, source.secret
        expect(response).to have_http_status(:accepted)
      end

      it "returns the result in the response" do
        source.update! activated_at: nil
        make_post! data_good, source.secret
        r = JSON.parse response.body

        expect(r["status"]).to match(/demo mode/i)
        expect(r.dig("result", "data")).to be_present
        expect(r.dig("result", "error")).to be_nil
        expect(r.dig("result", "format")).to eq "json"
        expect(r.dig("result", "hint")).to be_nil
        expect(r.dig("result", "protocol")).to match(/SUCCESS/)
      end
    end

    it "returns a 201 when all is good" do
      make_post! data_good, source.secret
      expect(response).to have_http_status(:created)
    end

    it "returns import data in body" do
      make_post! data_good, source.secret
      expect(response.body).to include "Import started"
    end

    it "creates an Import" do
      expect { make_post!(data_good, source.secret) }.to change(Import, :count).by(1)
    end

    it "leaves import in the right state" do
      make_post! data_good, source.secret
      expect(Import.last).to be_ingestion_finished
    end

    it "stores correct data" do
      make_post! data_good, source.secret

      stored_json = JSON.parse Import.last.file.download

      expected_json = [{ "float_column" => 1.1,
                         "name_column_p" => P14n.text("meter paffay"),
                         "something_else" => 4 }]

      expect(stored_json["test"].first["float_column"].to_f).to eq expected_json.first["float_column"]
      expect(stored_json["test"].first["name_column_p"]).to eq expected_json.first["name_column_p"]
      expect(stored_json["test"].first["something_else"]).to eq expected_json.first["something_else"]
      expect(stored_json["test"].first["name_column_e"]).not_to be_empty
    end

    it "the geodata is transformed correctly" do
      data = {
        device_external_id: "12345",
        account_external_id: "12345",
        serial_number: "12345",
        manufacturer: "Test",
        customer_external_id: "12345",
        customer_latitude: 1.234567,
        customer_longitude: 1.234567,
        seller_latitude: 2.345678,
        seller_longitude: 2.345678
      }

      make_post!(data, shs_source.secret, "/api/v1/in/shs")
      stored_json = JSON.parse Import.last.file.download

      keys = %w[customer_latitude_b customer_longitude_b
                customer_latitude_p customer_longitude_p
                seller_latitude_b seller_longitude_b
                seller_latitude_p seller_longitude_p]

      expect(stored_json["shs"].first.keys).to include(*keys)

      expect(stored_json["shs"].first["customer_latitude_b"]).to eq 1.235
      expect(stored_json["shs"].first["customer_longitude_b"]).to eq 1.235
      expect(stored_json["shs"].first["seller_latitude_b"]).to eq 2.346
      expect(stored_json["shs"].first["seller_longitude_b"]).to eq 2.346
    end
  end
end
