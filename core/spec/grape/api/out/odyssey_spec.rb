# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/out/odyssey", type: :request do
  let(:organization) { create :organization }
  let(:params) { { FROM: 1.day.ago.to_s, TO: Time.zone.now.to_s } }
  let(:device_external_id) { "device_some_ext_id_1" }
  let(:source) { create :source, project: create(:project, organization:) }
  let(:import) { create :import, source: }
  let(:serial) { "SERIAL_123" }
  let(:account_external_id) { "ACC_EXT_ID_123" }

  # shs table is used by both API calls
  before do
    # shs table
    # insert only required fields: null:false or required by odyssey
    q = <<-SQL.squish
        INSERT INTO data_shs
          (customer_external_id, customer_name_p, customer_phone_p, customer_latitude_b, customer_longitude_b, customer_category, account_external_id,#{' '}
          device_external_id, serial_number, manufacturer, created_at, updated_at, source_id, import_id, organization_id, data_origin,#{' '}
          uid, device_uid, customer_uid, account_uid)
        VALUES
          ('customer_external_1', 'p14n glibberish', 'this was a phone number once', 12.1, 50.2, 'household', '#{account_external_id}',#{' '}
          '#{device_external_id}', '#{serial}', 'Some good brand', NOW(), NOW(), #{source.id}, #{import.id}, #{organization.id}, 'origin',#{' '}
          'uid_1', 'device_uid','customer_uid_1','account_uid_1')
    SQL
    Postgres::RemoteControl.query! q
  end

  describe "/readings" do
    describe "auth" do
      it "responds with 401 if unauthorized" do
        get "/api/v1/out/odyssey/readings", params:, headers: { Authorization: nil }
        expect(response).to have_http_status(:unauthorized)
      end

      it "responds with 200 if authorized" do
        get "/api/v1/out/odyssey/readings", params:, headers: { Authorization: organization.api_key }
        expect(response).to have_http_status(:ok)
      end
    end

    describe "result" do
      let(:energy_lifetime_wh) { 20_222.1 }
      let(:energy_interval_wh) { 22.1 }

      before do
        # shs_ts table
        q = <<-SQL.squish
            INSERT INTO data_shs_ts
              (device_uid, metered_at, output_energy_lifetime_wh, output_energy_interval_wh, serial_number, manufacturer, created_at, updated_at, source_id, import_id, organization_id, data_origin)
            VALUES
              ('device_uid', '#{1.hour.ago.to_fs(:db)}', #{energy_lifetime_wh}, #{energy_interval_wh}, 'serial123', 'A2EI', NOW(), NOW(), #{source.id}, #{import.id}, #{organization.id}, 'test')
        SQL
        Postgres::RemoteControl.query! q
      end

      it "responds with no errors" do
        get "/api/v1/out/odyssey/readings", params:, headers: { Authorization: organization.api_key }
        expect(JSON.parse(response.body)["errors"]).to be_empty
      end

      it "responds with meter readings" do
        get "/api/v1/out/odyssey/readings", params:, headers: { Authorization: organization.api_key }
        dataset = JSON.parse(response.body, symbolize_names: true)

        expect(dataset[:readings].length).to eq 1

        reading = dataset[:readings].first
        # check one value from each table
        expect(reading[:energyReadingKwh]).to eq energy_lifetime_wh / 1000
        expect(reading[:customerName]).to eq "p14n glibberish"
      end

      it "works as well with meter_id set" do
        params_with_meter_id = params.merge({ "id" => device_external_id })

        get "/api/v1/out/odyssey/readings", params: params_with_meter_id, headers: { Authorization: organization.api_key }
        dataset = JSON.parse(response.body, symbolize_names: true)

        expect(dataset[:readings].length).to eq 1
      end

      it "no result with non existing meter_id" do
        params_with_meter_id = params.merge({ "id" => "does not exist" })

        get "/api/v1/out/odyssey/readings", params: params_with_meter_id, headers: { Authorization: organization.api_key }
        dataset = JSON.parse(response.body, symbolize_names: true)

        expect(dataset[:readings].length).to eq 0
      end
    end
  end

  describe "/payments" do
    describe "auth" do
      it "responds with 401 if unauthorized" do
        get "/api/v1/out/odyssey/payments", params:, headers: { Authorization: nil }
        expect(response).to have_http_status(:unauthorized)
      end

      it "responds with 200 if authorized" do
        get "/api/v1/out/odyssey/payments", params:, headers: { Authorization: organization.api_key }
        expect(response).to have_http_status(:ok)
      end
    end

    describe "result" do
      let(:amount) { 20_222.1 }

      before do
        # shs_ts table
        q = <<-SQL.squish
            INSERT INTO data_payments_ts
              (uid, payment_external_id, paid_at, amount, currency, account_external_id, account_uid, account_origin, custom, created_at, updated_at, source_id, import_id, organization_id, data_origin)
            VALUES
              ('device_some_uid_1', 'ABC-1', '#{1.hour.ago.to_fs(:db)}', #{amount}, 'XXX', '#{account_external_id}', '1234', 'shs', '{"type":"Contract Payment"}', NOW(), NOW(), #{source.id}, #{import.id}, #{organization.id}, 'test')
        SQL
        Postgres::RemoteControl.query! q
      end

      it "responds with no errors" do
        get "/api/v1/out/odyssey/payments", params:, headers: { Authorization: organization.api_key }
        expect(JSON.parse(response.body)["errors"]).to be_empty
      end

      it "responds with payments" do
        get "/api/v1/out/odyssey/payments", params:, headers: { Authorization: organization.api_key }
        dataset = JSON.parse(response.body, symbolize_names: true)

        expect(dataset[:payments].length).to eq 1

        payment = dataset[:payments].first
        # check one value from each table
        expect(payment[:amount]).to eq amount
        expect(payment[:serialNumber]).to eq serial

        # check value mappings
        expect(payment[:transactionType]).to eq "INSTALLMENT_PAYMENT"
        expect(payment[:customerCategory]).to eq "Residential"
      end
    end
  end
end
