# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/out/sources", type: :request do
  let(:auth_token) { "dasisteintesttoken" }
  let(:organization) { create :organization, api_key: auth_token }
  let!(:project) { create :project, organization: }
  let!(:source) { create :source, project: }
  let(:another_organization) { create :organization }
  let!(:another_source) { create :source, project: create(:project, organization: another_organization) }

  describe "authentication" do
    it "responds with 401 if unauthorized if no header given" do
      get "/api/v1/out/sources/#{source.id}", headers: { Authorization: nil }
      expect(response).to have_http_status(:unauthorized)
    end

    it "responds with 401 if wrong auth token given" do
      get "/api/v1/out/sources/#{source.id}", headers: { Authorization: "whatever" }
      expect(response).to have_http_status(:unauthorized)
    end

    it "responds with 200 if authorized" do
      get "/api/v1/out/sources/#{source.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET /sources/:id" do
    it "returns the correct body" do
      get "/api/v1/out/sources/#{source.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body[:kind]).to eq source.kind
      expect(body[:name]).to eq source.name
      expect(body[:project_id]).to eq source.project.id
    end

    it "returns unauthorized if source belongs to a different organization" do
      get "/api/v1/out/sources/#{another_source.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "GET /sources" do
    before { get "/api/v1/out/sources", params: { organization_id: organization.id, page: 1, size: 1 }, headers: { Authorization: auth_token } }

    it "returns a list of sources" do
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body).to be_an(Array)
      expect(body.count).to eq 1
    end

    it "does not return sources of another organization" do
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body.pluck(:id)).not_to include another_source.id
    end
  end
end
