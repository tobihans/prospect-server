# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/out/imports", type: :request do
  let(:auth_token) { "dasisteintesttoken" }
  let(:organization) { create :organization, api_key: auth_token }
  let!(:project) { create :project, organization: }
  let!(:source) { create :source, project: }
  let!(:import) { create :import, source: }
  let(:another_organization) { create :organization }
  let!(:another_source) { create :source, project: create(:project, organization: another_organization) }
  let!(:another_import) { create :import, source: another_source }
  let(:default_params) do
    { page: 1,
      size: 10,
      filter: {
        created_at_gteq: 24.hours.ago,
        created_at_lteq: 1.hour.from_now
      },
      order: "created_at DESC" }
  end

  describe "authentication" do
    it "responds with 401 if unauthorized if no header given" do
      get "/api/v1/out/imports/#{import.id}", headers: { Authorization: nil }
      expect(response).to have_http_status(:unauthorized)
    end

    it "responds with 401 if wrong auth token given" do
      get "/api/v1/out/imports/#{import.id}", headers: { Authorization: "whatever" }
      expect(response).to have_http_status(:unauthorized)
    end

    it "responds with 200 if authorized" do
      get "/api/v1/out/imports/#{import.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET /imports/:id" do
    before do
      create :import, source:
    end

    it "returns the correct body" do
      get "/api/v1/out/imports/#{import.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body[:created_at]).not_to be_nil
      expect(body[:source_id]).to eq source.id
    end

    it "returns unauthorized if import belongs to a different organization" do
      get "/api/v1/out/imports/#{another_import.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "DELETE /imports/:id" do
    before do
      5.times { create :import, source: }
    end

    it "deletes only one import" do
      last_i = Import.last
      expect(Import.count).to eq 7

      expect_any_instance_of(Import).to receive(:delete_data!).once

      delete "/api/v1/out/imports/#{last_i.id}", headers: { Authorization: auth_token }

      expect(response).to have_http_status(:ok)
      expect(Import.count).to eq 6
      expect(Import.all.map(&:id)).not_to include(last_i.id)
    end

    it "returns unauthorized if import belongs to a different organization" do
      delete "/api/v1/out/imports/#{another_import.id}", headers: { Authorization: auth_token }
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "GET /imports" do
    before do
      5.times { create :import, source: }
    end

    it "returns a list of imports" do
      get "/api/v1/out/imports", params: default_params, headers: { Authorization: auth_token }
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body).to be_an(Array)
      expect(body.count).to eq 6
    end

    it "does not return imports of another organization" do
      get "/api/v1/out/imports", params: default_params, headers: { Authorization: auth_token }
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body.pluck(:source_id)).not_to include another_source.id
    end

    it "paginates" do
      get "/api/v1/out/imports", params: default_params.merge(size: 2), headers: { Authorization: auth_token }
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body).to be_an(Array)
      expect(body.count).to eq 2
    end

    it "filters" do
      default_params[:filter].merge!(created_at_lteq: 10.minutes.ago)
      get "/api/v1/out/imports", params: default_params, headers: { Authorization: auth_token }
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body).to be_an(Array)
      expect(body.count).to eq 0
    end
  end

  describe "DELETE /imports/:import_ids" do
    before do
      5.times { create :import, source: }
    end

    it "deletes the records" do
      to_be_deleted_ids = [Import.first.id, Import.last.id]
      expect(Import.count).to eq 7
      expect(Import.all.map(&:id)).to include(*to_be_deleted_ids)
      delete "/api/v1/out/imports", params: { import_ids: to_be_deleted_ids }, headers: { Authorization: auth_token }
      JSON.parse(response.body, symbolize_names: true)
      expect(Import.count).to eq 5
      expect(Import.all.map(&:id)).not_to include(*to_be_deleted_ids)
    end

    it "does not allow the deletion of an import from another org" do
      to_be_deleted_id = another_import.id
      expect(Import.count).to eq 7
      expect(Import.all.map(&:id)).to include(*to_be_deleted_id)
      delete "/api/v1/out/imports", params: { import_ids: [to_be_deleted_id] }, headers: { Authorization: auth_token }
      expect(response).to have_http_status(:unauthorized)
      expect(Import.count).to eq 7
      expect(Import.all.map(&:id)).to include(*to_be_deleted_id)
    end
  end

  describe "GET /sources/:id/imports" do
    before do
      5.times { create :import, source: }
      get "/api/v1/out/sources/#{source.id}/imports", params: default_params, headers: { Authorization: auth_token }
    end

    it "returns a list of imports" do
      expect(source.imports.count).to eq(6)
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body).to be_an(Array)
      expect(body.count).to eq 6
    end

    it "does not return imports of another organization" do
      body = JSON.parse(response.body, symbolize_names: true)
      expect(body.pluck(:source_id)).not_to include another_source.id
    end
  end
end
