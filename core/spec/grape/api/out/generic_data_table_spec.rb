# frozen_string_literal: true

require "rails_helper"
require "json"

describe "generic /api/v1/out", type: :request do
  let(:visibility) { create :visibility }
  let(:organization) { visibility.organization }

  let(:headers) { { "Authorization" => organization.api_key } }

  let(:source) { create :source, { kind: "api_push", organization:, project: visibility.project } }
  let(:import) { create :import, source: }

  context "when we expect a negative result" do
    it "returns 401 on wrong auth params" do
      get "/api/v1/out/test", params: { uid_from: "0", uid_to: "z", size: 25, page: 1 },
                              headers: { "Authorization" => "something_not_valid" }
      expect(response).to have_http_status(:unauthorized)
    end

    it "returns 401 on missing auth params" do
      get "/api/v1/out/test", params: { size: 25, page: 1 }
      expect(response).to have_http_status(:unauthorized)
    end
  end

  context "when parameter q set" do
    it "search with ransack param" do
      test_example_insert import, "test"

      # search for uid containg 'UUID', test data should supply that.
      get("/api/v1/out/test", params: { size: 25, page: 1, q: { uid_cont: "UUID" } }, headers:)

      expect(response).to have_http_status(:ok)

      data = JSON.parse response.body
      expect(data["data"]).not_to be_empty
    end
  end

  Postgres::DataTable.each do |data_table|
    context "when we expect a positive result, #{data_table.data_category}" do
      # use at least one filter, that all tables have
      let(:params) { { updated_at_from: 1.day.ago.to_s, updated_at_to: Time.zone.now.to_s, size: 25, page: 1 } }
      let(:data) { JSON.parse response.body }

      before do
        test_example_insert import, data_table.data_category

        get("/api/v1/out/#{data_table.data_category}", params:, headers:)
      end

      it "returns 200" do
        expect(data["errors"]).to be_empty
        expect(response).to have_http_status(:ok)
      end

      it "returns the result in the response" do
        expect(data["data"]).not_to be_empty
      end
    end
  end
end
