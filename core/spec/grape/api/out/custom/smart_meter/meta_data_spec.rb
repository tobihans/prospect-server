# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/meter/meta_data", type: :request do
  original_params = { id: 1 }
  let(:params_incomplete) { original_params.merge(id: nil) }
  let(:params_id_wrong) { original_params.merge(id: "string") }
  let(:params_complete) { original_params }
  let(:metadataset) { build :sm_meta_data }

  context "when we expect a negative result" do
    let(:correct_token) { ENV["JRC_TOKEN"] = "dasisteintesttoken" }

    context "with missing params" do
      it "returns 400 on missing params" do
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_incomplete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with missing auth header" do
      it "returns 401 on missing auth header token" do
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_complete, headers: { Authorization: nil }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "with wrong method" do
      it "returns 405 on wrong http method (post instead of get)" do
        post "/api/v1/out/custom/smart_meter/meta_data", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:method_not_allowed)
      end
    end

    context "with invalid params" do
      it "returns 400 on wrong id param format" do
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_id_wrong, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with incorrect auth header" do
      it "returns 401 on wrong auth header token" do
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_complete, headers: { Authorization: "incorrect_token" }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  context "when we expect positive results" do
    let(:correct_token) { ENV["ODYSSEY_TOKEN"] = "dasisteintesttokenodyssey" }

    context "with valid params and header" do
      it "returns 200 on correct auth header and params" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return([metadataset])
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:ok)
      end

      it "returns valid meta data" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return([metadataset])
        get "/api/v1/out/custom/smart_meter/meta_data", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset[:meter_id]).to eq metadataset.meter_id
      end
    end
  end
end
