# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/meter/load_profile", type: :request do
  original_params = { id: 1, resolution: :month }
  let(:params_incomplete) { original_params.merge(id: nil) }
  let(:params_id_wrong) { original_params.merge(id: "string") }
  let(:params_complete) { original_params }
  let(:correct_token) { ENV["JRC_TOKEN"] = "dasisteintesttoken" }

  let(:query_result) do
    [
      { meter_id: 1, hour: 5, avg_power: 27.2, std_power: 4.7, max_power: 45.1, min_power: 12.8, datapoints: 57 }.with_indifferent_access,
      { meter_id: 2, hour: 6, avg_power: 29.1, std_power: 5.1, max_power: 41.2, min_power: 11.9, datapoints: 58 }.with_indifferent_access,
      { meter_id: 3, hour: 7, avg_power: 26.7, std_power: 3.2, max_power: 46.1, min_power: 12.3, datapoints: 45 }.with_indifferent_access
    ]
  end

  context "when we expect a negative result" do
    context "with missing params" do
      it "returns 400 on missing params" do
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_incomplete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with missing auth header" do
      it "returns 401 on missing auth header token" do
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: nil }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "with wrong method" do
      it "returns 405 on wrong http method (post instead of get)" do
        post "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:method_not_allowed)
      end
    end

    context "with invalid params" do
      it "returns 400 on wrong id param format" do
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_id_wrong, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with incorrect auth header" do
      it "returns 401 on wrong auth header token" do
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: "incorrect_token" }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  context "when we expect positive results" do
    context "with valid params and header" do
      it "returns 200 on correct auth header and params" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:ok)
      end

      it "returns valid meta data lenght" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.length).to eq query_result.length
      end

      it "returns valid hour" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:hourOfTheDay]).to eq query_result.first[:hour]
      end

      it "returns valid power average" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:averagePower]).to eq query_result.first[:avg_power]
      end

      it "returns valid power standard deviation" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:stdDevPower]).to eq query_result.first[:std_power]
      end

      it "returns valid power maximum" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:maxPower]).to eq query_result.first[:max_power]
      end

      it "returns valid power minimum" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:minPower]).to eq query_result.first[:min_power]
      end

      it "returns valid number of datapoints" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/load_profile", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:numberDatapoints]).to eq query_result.first[:datapoints]
      end
    end
  end
end
