# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/meter/meters", type: :request do
  let(:correct_token) { ENV["JRC_TOKEN"] = "dasisteintesttoken" }
  let(:meters_response) { [{ meter_id: 1375 }, { meter_id: 1377 }] }

  context "when we expect a negative result" do
    it "responds with 401 if unauthorized" do
      get "/api/v1/out/custom/smart_meter/meters", headers: { Authorization: nil }
      expect(response).to have_http_status(:unauthorized)
    end
  end

  context "when we expect a positive result" do
    it "responds with 200 if authorized" do
      allow(Service::RdsConnection.connection).to receive(:execute).and_return(meters_response)
      get "/api/v1/out/custom/smart_meter/meters", headers: { Authorization: correct_token }
      expect(response).to have_http_status(:ok)
    end

    it "responds with list of meters" do
      allow(Service::RdsConnection.connection).to receive(:execute).and_return(meters_response)
      get "/api/v1/out/custom/smart_meter/meters", headers: { Authorization: correct_token }
      dataset = JSON.parse(response.body, symbolize_names: true)
      expect(dataset.length).to eq 2
    end
  end
end
