# frozen_string_literal: true

require "rails_helper"
require "json"

describe "/api/v1/out/custom/smart_meter/technical_data", type: :request do
  original_params = { id: 1, resolution: :month, from: 1.year.ago, to: 1.month.ago }
  let(:params_incomplete) { original_params.merge(id: nil) }
  let(:params_id_wrong) { original_params.merge(id: "string") }
  let(:params_complete) { original_params }
  let(:correct_token) { ENV["JRC_TOKEN"] = "dasisteintesttoken" }

  let(:query_result) do
    [
      { customer: 1, start_period: 2.years.ago, metered_power: 26.0, energy_consumed_total_kwh: 13.19, energy_consumed_in_interval_kwh: 13.19 }.with_indifferent_access,
      { customer: 2, start_period: 2.years.ago, metered_power: 30.0, energy_consumed_total_kwh: 33.72, energy_consumed_in_interval_kwh: 33.72 }.with_indifferent_access,
      { customer: 3, start_period: 2.years.ago, metered_power: 32.0, energy_consumed_total_kwh: 58.42, energy_consumed_in_interval_kwh: 58.42 }.with_indifferent_access
    ]
  end

  context "when we expect a negative result" do
    context "with missing params" do
      it "returns 400 on missing params" do
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_incomplete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with missing auth header" do
      it "returns 401 on missing auth header token" do
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: nil }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "with wrong method" do
      it "returns 405 on wrong http method (post instead of get)" do
        post "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:method_not_allowed)
      end
    end

    context "with invalid params" do
      it "returns 400 on wrong id param format" do
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_id_wrong, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:bad_request)
      end
    end

    context "with incorrect auth header" do
      it "returns 401 on wrong auth header token" do
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: "incorrect_token" }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  context "when we expect positive results" do
    context "with valid params and header" do
      it "returns 200 on correct auth header and params" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        expect(response).to have_http_status(:ok)
      end

      it "returns valid meta data lenght" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.length).to eq query_result.length
      end

      it "returns valid metered_power" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:metered_power]).to eq query_result.first[:metered_power]
      end

      it "returns valid energy consumed total kwh" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:energyReadingKwh]).to eq query_result.first[:energy_consumed_total_kwh]
      end

      it "returns valid energy consumed in interval kwh" do
        allow(Service::RdsConnection.connection).to receive(:execute).and_return(query_result)
        get "/api/v1/out/custom/smart_meter/technical_data", params: params_complete, headers: { Authorization: correct_token }
        dataset = JSON.parse(response.body, symbolize_names: true)
        expect(dataset.first[:energyConsumptionKwh]).to eq query_result.first[:energy_consumed_in_interval_kwh]
      end
    end
  end
end
