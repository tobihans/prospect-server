# frozen_string_literal: true

require "rails_helper"

RSpec.describe UserMailer do
  let(:user) { create :user }

  describe "notify" do
    let(:inviting_user) { create :user }
    let(:mail) { described_class.with(user:, inviting_user:).invite }
    let(:mail_without_inviter) { described_class.with(user:).invite }

    it "renders the headers" do
      expect(mail.subject).to match "Invitation to PROSPECT"
      expect(mail.to).to eq([user.email])
    end

    it "sends invite code in body" do
      expect(mail.body.encoded).to match user.invite_code
    end

    it "sends inviting user in body" do
      expect(mail.body.encoded).to match inviting_user.to_s
    end

    it "sends organization in body" do
      expect(mail.body.encoded).to match user.organization.to_s
    end

    it "also when there is no inviting user" do
      expect(mail_without_inviter.body.encoded).not_to match inviting_user.to_s
      expect(mail_without_inviter.body.encoded).to match user.invite_code
    end
  end

  describe "welcome" do
    let(:mail) { described_class.with(user:).welcome }

    it "renders the headers" do
      expect(mail.subject).to match "Welcome"
      expect(mail.to).to eq([user.email])
    end

    it "sends user in body" do
      expect(mail.body.encoded).to match user.to_s
    end

    it "says welcome in body" do
      expect(mail.body.encoded).to match "Welcome"
    end

    it "sends a url" do
      expect(mail.body.encoded).to match "http"
    end
  end

  describe "invitation" do
    let(:mail) { described_class.with(user:, organization: user.organization).organization_invite }

    it "renders the headers" do
      expect(mail.subject).to match "Invitation to PROSPECT"
      expect(mail.to).to eq([user.email])
    end

    it "sends user in body" do
      expect(mail.body.encoded).to match user.to_s
    end

    it "sends a url" do
      expect(mail.body.encoded).to match "http"
    end

    it "sends organization name" do
      expect(mail.body.encoded).to match user.organization.to_s
    end
  end

  describe "reset_password_email" do
    let(:user) { create :user, reset_password_token: "EXAMPLETOKEN" }
    let(:mail) { described_class.reset_password_email(user) }

    it "has right content" do
      expect(mail.subject).to include "Your password has been reset"
      expect(mail.to).to eq([user.email])
      expect(mail.body.encoded).to include "password_resets/EXAMPLETOKEN/edit"
    end
  end

  describe "reset_password_other_auth" do
    let(:user) { create :user, :google_auth }
    let(:mail) { described_class.reset_password_other_auth(user) }

    it "has right content" do
      expect(mail.subject).to include "Password Reset - Login via"
      expect(mail.to).to eq([user.email])
      expect(mail.body.encoded).to include "Google"
    end
  end
end
