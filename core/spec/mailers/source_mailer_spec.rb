# frozen_string_literal: true

require "rails_helper"

RSpec.describe SourceMailer do
  describe "notify_recorded_upload" do
    let(:source) { create :source }

    let(:mail) do
      described_class.
        with(source:, code: "ABCDEF", code_expiry: 3.days, email: "a@b.c").
        notify_recorded_upload
    end

    it "has proper headers" do
      expect(mail.subject).to match "PROSPECT: Please upload data for #{source.name}"
      expect(mail.to).to eq(["a@b.c"])
    end

    it "sends link to upload with code" do
      expect(mail.body.encoded).to match "/sources/public_recorded_upload/ABCDEF"
    end

    it "sends expiry in body" do
      expect(mail.body.encoded).to match "3 days"
    end

    it "sends source name in body" do
      expect(mail.body.encoded).to match source.name
    end
  end
end
