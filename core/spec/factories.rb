# frozen_string_literal: true

def mock_grafana
  allow_any_instance_of(Organization).to receive(:setup_grafana!).and_return(true)
  allow_any_instance_of(User).to receive(:create_grafana_user).and_return(true)
  allow_any_instance_of(OrganizationUser).to receive(:update_grafana_rights).and_return(true)
end

def unmock_grafana
  allow_any_instance_of(Organization).to receive(:setup_grafana!).and_call_original
  allow_any_instance_of(User).to receive(:create_grafana_user).and_call_original
  allow_any_instance_of(OrganizationUser).to receive(:update_grafana_rights).and_call_original
end

FactoryBot.define do
  factory :sm_meta_data, class: OpenStruct do
    sequence(:meter_id) { |n| n }
    # meter_id { 3 }
    latitude { "1.0" }
    longitude { "2.0" }
    installed_at { 2.years.ago }
    name { "Test Meter" }
    district { "Test District" }
    type_of_network { "Test Network" }
    type_of_smartmeter { "Test Smartmeter" }
    power_reading { "Test Power Reading" }
    appliances { "Test Appliances" }
    facilities { "Test Facilities" }
  end

  factory :user do
    before(:create) do
      mock_grafana
    end

    after(:create) do
      unmock_grafana
    end

    #
    # test helper to allow for easy user creation with certain privileges
    #
    transient do
      privileges { [] }
    end

    after(:create) do |user, evaluator|
      evaluator.privileges.each do |name|
        user.current_organization_user.privileges.create name:, user: (create :user, :admin)
      end
    end

    sequence(:username) { |n| "Jonny #{n}" }
    email { "#{username.parameterize}@example.com" }
    password { "very_secret_test_password" }
    salt { "asdasdastr4325234324sdfds" }
    active_at { Time.zone.now }
    crypted_password do
      Sorcery::CryptoProviders::BCrypt.encrypt(
        password, "asdasdastr4325234324sdfds"
      )
    end
    organization
    terms_of_use do
      { accepted: true,
        version: Rails.configuration.current_terms_version[:terms_of_use] }
    end
    data_protection_policy do
      { accepted: true,
        version: Rails.configuration.current_terms_version[:data_protection_policy] }
    end
    code_of_conduct do
      { accepted: true,
        version: Rails.configuration.current_terms_version[:code_of_conduct] }
    end

    trait :invited do
      username { nil }
      sequence(:email) { |n| "freshi_#{n}@blake.ks" }
      password { nil }
      salt { nil }
      active_at { nil }
    end

    trait :archived do
      archived_at { Time.zone.now }
    end

    trait :admin do
      admin { true }
    end

    trait :google_auth do
      authentications { [association(:authentication)] }
    end
  end

  factory :authentication do
    provider { "google" }
    sequence(:uid) { |n| "uid#{n}" }
    user
  end

  factory :organization do
    before(:create) do
      mock_grafana
    end

    after(:create) do
      unmock_grafana
    end

    sequence(:name) { |n| "The Test Organization #{n}" }
    public_key do
      %( MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4NGzA0Lt6L96rbP2Wq9vvGtXoZv1knYrlRM
        jFqene4w4yCr3CUCy0NB0cqe7a4db7sju3D17YvlV76v7UoL8UWDOYx5ctu3a3DGwy1zSQALTpwlQoUm
        aLC8PiJKMuhoY05IJ+fkQli1fW/1E+UQ9pKG/zGYBzkyx90BrsqaQvXRwMya7zneIC6e1nUitZlIZM4b
        Qe2LV4Ufp2UDwV87kGyevR+ZyCLFQ2m9nxRhtuxuiLoNXqT1qxilOXe9OASZZ2bO6wl2mHr8eZfVccgF
        5ByonrRkMTICoxWqw6hdA/pzRlTO1AR7nHMBuNQpC4Z/qqEYiGjIuW5yO2BK0o4yTowIDAQAB ).squish
    end
    iv { "1234567890123456" }
    salt { "1234567890123456" }
    crypto_added_at { 1.month.ago }
    wrapped_key do
      %( "tpfVcq7kZ+tMMnJuK7ofWYpxzLNokmGP9sLzAQuzgUGis9ax5snCVm3w5bA/vy1FcybDOKX7CYy1V3
        z9FLhVpzuKzpdEEKp5zGXA6ZK+RjuQxSGSE5gRhHiEBpFNG3J659F102oHTs1TJQ81u6oUaRX/u78vb0
        V/h8ao/RHf8skSLNrSy84o/rgRwyeCLjIoh8N6aCUz0NkpdvMq/CG6fVxctIGA1C8FqKLV71i4YqMsZq
        CMLahVIY3yRroIGbYAXw3B2lefLwBHeySNI8FM241PvXYsW9sSZkMQKP9oH8HeRtqK16oP2Wr/kzzuj6
        dhO+hq1Slh1ALCFxC3OxtwfyLt1Cld8sdHrXBPGNqaykz9qhY1w59tAY9dDqTsvZzn3jjS9b3nuzNBe0
        KTUgjrYbYNf+SKLqRJYt+9iqojk7QtPaJWoxpqRRQJxgb9VRAJ1Y7PQGXNA7ZQoCvRjqobJDzlU8DmKR
        scj44UU8JiOPU3ybEjx5Zhcq676GdaoofblW0fo2TnfUNHPa3vnrpC/HDu0eKjfoBIF2g6VgAljXaQVU
        C9h3Zs1Fed3elobhJFxeUhA64e6b4EhOqVxi2y4LY0NWw/GfOuNhbLONuKBTP0HSmncb6Ywa4kl4tak+
        7NWM2va+8sNP3FRVmSnaa62cREUuedxUDkRREk5GRkxGc2To27Q8mlvekFwYa27Jq5YIA4HIx66bhqa5
        U6mFZr4Rl0sM5AFXLwCP9n8ZFUW+gHXqR6V04wtdnmpJWy4YSQ/fomknSo9DkxP6Ku5ciw21zia3hVro
        bIjEUOv5mRjv2MEfB/bhpHmTjUZi1ZGE/UH3FrScU3pTLuoEB0M75Q8HwwPA+9ak3xyhYuNSzaivNLAQ
        /SeESnzMxKqtMUTlzX61A4vh+wFRdiwOMPYMJYCmnrW2j0/zdOFxW8IECYBieBNUOFy2OgCFE3KSXr1Z
        Ph/og8mwo4OFpaZVhx5mcl5n2jqZ/7lAr3/5LpInZkiFAsf/RnzigDIsAnyOduHYiNiKr4vjz+3oKg2s
        INXZORxbytowHu6+9L6h6LaF1foZInnOnFFiRPXKsX+CdxZhdm4OrjDBe7OC0qlAnwNmLnTSKClO96cq
        cNomzBL1wZYXpP0sgc9gVcpELH6AjH05Rn5b493bY7uNLUJP8riWnLnL+jltA68BrYAI+bjsT5pujZnA
        K0f+H6zFgt9xdxAF+zG5FbiP/HYQjFfaM8n2XutmMhtAS4g5jC5IarFLopuiHQy8UUlnOBPal557i82P
        tbMY0u++ALOOQWvsjNTXy8uu9SmPSts+cZ+1Y7xnDrjxN9b7BtwWKW+rmzBwpU9iKZ6ZsFa8IZFTrpTU
        EamEH4SBnI8XzQQC4vjGTRY7xk7QVbmbTjfZhryOJkvizG4oYa/m/dTVuKiRnjZQn1roN88kM6TSFyM4
        QFvPaMdZi4YPvsnvng2Z6qcii3U6veOUQtfgGvC1br0TJm3ftsgZXMlhTAC2ZOt4XLi8lmnMu2SuKDWk
        zwBuZnTbsFeXrB9XhRvWCuYBBG1fWZEGofFf+lNPFuBLeZZK1/xr+pRT4galfpuI0+WR7GhLI0b6sonz
        o82DO4O+9uWTXQqNDaHKIgH9R+KFjlImwGKeUgrc9Er3U=").squish
    end
  end

  factory :organization_group do
    sequence(:name) { |n| "Test Organization Group 1 #{n}" }
  end

  factory :organization_group_member do
    organization
    organization_group
  end

  factory :organization_user do
    user
    organization
  end

  factory :privilege do
    organization_user
    name { Privilege::PRIVILEGES.sample }
    user # creator
  end

  factory :project do
    sequence(:name) { |n| "Test Project #{n}" }
    organization
  end

  factory :source do
    sequence(:name) { |n| "Test Source#{n}" }
    project
    kind { "manual_upload" }
    activated_at { Time.zone.now }
    secret { "test_secret" }

    # default factory can not just autofill the data_category. But we cant just set one for all source kinds...
    # So we treat the symptom here...
    after :build do |s|
      s.data_category ||= "shs" if s.data_schema == "generic"
      s.data_category ||= s.adapter.data_categories.first
    end

    trait :s3_bucket do
      kind { "s3_bucket" }
      data_category { "grids" }
      details do
        { "access_key" => "SOME_ACCESS_KEY",
          "region" => "us-west1",
          "bucket_name" => "bucket",
          "file_format" => "json",
          "frequency" => "monthly",
          "order_by" => "last_modified" }
      end
    end

    trait :victron do
      kind { "api/victron" }
      data_category { "grids" }
      details { { "username" => "some_username" } }
    end

    trait :supamoto do
      kind { "api/supamoto" }
      details { { "username" => "some_username" } }
    end

    trait :custom do
      kind { :manual_upload }
      data_category { :custom }
      details { { "custom_columns" => [%w[txt_col text], %w[ts_col timestamp], %w[num_col numeric]] } }
    end

    trait :rbf_ueccc_ogs do
      transient do
        dyn_form { create :dynamic_form, :ogs }
      end

      kind { "manual_input/ueccc" }
      sequence(:details) do |n|
        { form: dyn_form.id,
          bulk_upload: "true",
          public_access: "true",
          slug: "slug_#{n}" }
      end

      after(:create) do |s, evaluator|
        RbfOrganizationConfig.create! organization: s.organization,
                                      rbf_claim_template: evaluator.dyn_form.rbf_claim_template,
                                      code: "RBF/OGS/ESCO/#{s.organization_id}",
                                      total_grant_allocation: 5_000_000
      end
    end

    trait :rbf_ueccc_ccs do
      transient do
        dyn_form { create :dynamic_form, :ccs }
      end

      kind { "manual_input/ueccc" }
      sequence(:details) do |n|
        { form: dyn_form.id,
          bulk_upload: "true",
          public_access: "true",
          slug: "slug_#{n}" }
      end

      after(:create) do |s, evaluator|
        RbfOrganizationConfig.create! organization: s.organization,
                                      rbf_claim_template: evaluator.dyn_form.rbf_claim_template,
                                      code: "RBF/CCS/ESCO/#{s.organization_id}",
                                      total_grant_allocation: 5_000_000
      end
    end

    trait :rbf_ueccc_pue do
      transient do
        dyn_form { create :dynamic_form, :pue }
      end

      kind { "manual_input/ueccc" }
      sequence(:details) do |n|
        { form: dyn_form.id,
          bulk_upload: "true",
          public_access: "true",
          slug: "slug_#{n}" }
      end

      after(:create) do |s, evaluator|
        RbfOrganizationConfig.create! organization: s.organization,
                                      rbf_claim_template: evaluator.dyn_form.rbf_claim_template,
                                      code: "RBF/PUE/ESCO/#{s.organization_id}",
                                      total_grant_allocation: 5_000_000
      end
    end

    trait :rbf_ueccc_payments do
      transient do
        dyn_form { create :dynamic_form, :payments }
      end

      kind { "manual_input/ueccc" }
      sequence(:details) do |n|
        { form: dyn_form.id,
          bulk_upload: "true",
          public_access: "true",
          slug: "slug_#{n}" }
      end

      after(:create) do |s, _evaluator|
        rct = create :rbf_claim_template
        RbfOrganizationConfig.create! organization: s.organization,
                                      rbf_claim_template: rct,
                                      code: "RBF/PUE/ESCO/#{s.organization_id}",
                                      total_grant_allocation: 5_000_000
      end
    end
  end

  factory :import do
    source

    trait :finished do
      ingestion_started_at { Time.zone.parse "2024-05-06 07:08:09" }
      ingestion_finished_at { Time.zone.parse "2024-05-06 07:08:10" }
      processing_started_at { Time.zone.parse "2024-05-06 07:08:11" }
      processing_finished_at { Time.zone.parse "2024-05-06 07:08:12" }
    end
  end

  factory :visibility do
    project
    organization

    trait :test_column do
      columns { { data_test: %w[uid] } }
    end
  end

  factory :org_device_mapper do
    organization { create :organization }
    device_type { "hop_meter" }
    device_id { 100 }

    trait :aam_v3 do
      device_type { "aam_v3" }
    end
  end

  factory :session, class: "ActiveRecord::SessionStore::Session" do
    data { { "test" => "data" } }
    session_id { SecureRandom.hex(16) }
  end

  factory :rbf_claim_template do
    supervising_organization { create :organization }
    managing_organization { create :organization }
    verifying_organization { create :organization }
    organization_group { create :organization_group }
    currency { "UGX" }
    user

    ogs # default trait

    trait :ogs do
      name { "OGS" }
      trust_trace_check { "rbf_easp_ogs_is_eligible" }
    end

    trait :ccs do
      name { "CCS" }
      trust_trace_check { "rbf_easp_ccs_is_eligible" }
    end

    trait :pue do
      name { "OGS" }
      trust_trace_check { "rbf_easp_pue_is_eligible" }
    end

    after(:build) do |rct|
      [rct.supervising_organization, rct.managing_organization, rct.verifying_organization].each do |org|
        create :organization_group_member, organization: org, organization_group: rct.organization_group
      end
      rct.reload_organization_group
    end
  end

  factory :rbf_product do
    rbf_claim_template
    technology { "Solar Home System" }
    manufacturer { "Manufacturer" }
    sequence(:model) { "Model #{_1}" }
    rbf_category { "B" }
    after(:build) { _1.organization ||= _1.rbf_claim_template.non_submitting_organizations.first }
  end

  factory :rbf_product_price do
    original_sales_price { 200_000 }
    base_subsidized_sales_price { 80_000 }
    base_subsidy_amount { 120_000 }
    category { "paygo" }
    sequence(:valid_from) { Date.parse("2024-05-06") - _1.days }
    rbf_product
  end

  factory :rbf_organization_config do
    organization { create :organization }
    rbf_claim_template { create :rbf_claim_template }
    sequence(:code) { "RBF/CODE/#{_1}" }
    total_grant_allocation { 5_000_000 }
  end

  factory :rbf_claim do
    transient do
      org_conf { create :rbf_organization_config }
    end
    rbf_claim_template { org_conf.rbf_claim_template }
    organization { org_conf.organization }
  end

  factory :dynamic_form do
    ogs # default trait

    trait :ogs do
      name { "OGS Sales Form" }
      rbf_claim_template { create :rbf_claim_template, :ogs }
      form_data { Ueccc::FormGenerator.sales_form_data "ogs" }
    end

    trait :ccs do
      name { "CCS Sales Form" }
      rbf_claim_template { create :rbf_claim_template, :ccs }
      form_data { Ueccc::FormGenerator.sales_form_data "ccs" }
    end

    trait :pue do
      name { "PUE Sales Form" }
      rbf_claim_template { create :rbf_claim_template, :pue }
      form_data { Ueccc::FormGenerator.sales_form_data "pue" }
    end

    trait :payments do
      name { "Payments Form" }
      rbf_claim_template { nil }
      form_data { Ueccc::FormGenerator.payment_form_data }
    end
  end
end
