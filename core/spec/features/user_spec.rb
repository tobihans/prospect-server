# frozen_string_literal: true

require "rails_helper"

describe "User" do
  let!(:invited_user) do
    create(
      :user,
      invite_code: 1234,
      created_at: 6.minutes.ago,
      active_at: nil
    )
  end

  let(:recently_invited_user) do
    create(
      :user,
      invite_code: 1234,
      created_at: 1.minute.ago,
      active_at: nil
    )
  end

  before do
    sign_in(user)
  end

  context "when user is admin" do
    let(:user) { create :user, admin: true }

    it "show me" do
      visit "/users/me"

      expect(page.text).to include user.email
      expect(page.text).to include user.username
    end

    it "shows invitation url of invited_user" do
      visit user_path(invited_user)
      expect(page.text).to include invited_user.invite_code
    end

    it "does not show invitation url of recently invited user" do
      visit user_path(recently_invited_user)
      expect(page.text).not_to include recently_invited_user.invite_code
    end

    it "forces to accepct terms and conditions" do
      user.update! terms_of_use: {}, data_protection_policy: {}, code_of_conduct: {}

      visit "/users/me"

      expect(page.text).to include "Before continuing, please accept the terms and conditions."
      check :user_terms_of_use
      find("input[type=submit]").click
      expect(page.text).to include "You have to accept all terms and conditions"

      check :user_terms_of_use
      check :user_data_protection_policy
      check :user_code_of_conduct
      find("input[type=submit]").click

      expect(page.text).to include "Terms and Conditions accepted"

      visit "/users/me"
      expect(page.text).to include user.email
      expect(page.text).to include user.username
    end

    describe "organization invite" do
      it "accept" do
        allow(Grafana::RemoteControl).to receive_messages(update_user_permission: {})

        VCR.use_cassette :grafana_accept_invite do
          Grafana::RemoteControl.instance_variable_set("@client", Grafana::Client.new)
          organization2 = create :organization, id: 123_45
          VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
            organization2.setup_grafana!
          end
          user2 = create :user, id: 123_45
          user2.create_grafana_user

          visit new_user_path

          fill_in "Email", with: user2.email
          select :manager
          select organization2.name

          expect do
            click_link_or_button "Invite"
            # the above line doesn't seem to wait correctly for turbo frames, so we use find to wait for the expected next page
            find("a[href*='#{new_user_path}']")
          end.to have_enqueued_job.exactly(:once).and have_enqueued_job(ActionMailer::MailDeliveryJob)

          expect(page.text).to include "Invitation created"

          user2.reload
          expect(user2.organization_users.count).to be 2

          invitation = user2.organization_users.find_by(organization: organization2)
          expect(invitation.activated_at).to be_nil
          expect(invitation).to be_privilege("user.manage")

          expect(user2.organization_id).not_to be organization2.id

          sign_out
          sign_in(user2)

          visit "/"
          expect(page.body).to include("Pending Invitations")

          visit "/users/invitations"
          expect(page.text).to match(/#{organization2.name}/)

          allow(Grafana::RemoteControl).to receive(:add_user_to_org).and_call_original

          find("a[href*='accept']").click
          expect(page.text).to include("Invitation accepted")
          expect(user2.organization_users.find_by(organization: organization2).activated_at).not_to be_nil
          find("div[data-controller='dropdown']").click
          find("a[href='#{switch_organization_users_path(organization_id: organization2.id)}']").click

          organization2.reload

          client = Grafana::Client.new
          grafana_user = client.search_for_users_by(login: Naming.grafana_user(user2))
          user_info = client.user_by_id(grafana_user.first[:id])

          expect(user_info[:orgId]).to eq organization2.grafana_org_id

          user2.reload
          expect(user2.organization_id).to eq organization2.id
          expect(user2.organization).to eq organization2
        end
      end

      it "reject" do
        allow(Grafana::RemoteControl).to receive_messages(update_user_permission: {})

        VCR.use_cassette :grafana_reject_invite do
          organization2 = create :organization, id: 123_45
          VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
            organization2.setup_grafana!
          end
          user2 = create :user, id: 123_45
          user2.create_grafana_user

          visit new_user_path

          fill_in "Email", with: user2.email
          select :manager
          select organization2.name

          expect do
            click_link_or_button "Invite"
          end.to have_enqueued_job.exactly(:once).and have_enqueued_job(ActionMailer::MailDeliveryJob)

          expect(page.text).to include "Invitation created"

          user2.reload
          expect(user2.organization_users.count).to be 2

          invitation = user2.organization_users.find_by(organization: organization2)
          expect(invitation.activated_at).to be_nil
          expect(invitation).to be_privilege("user.manage")

          expect(user2.organization_id).not_to be organization2.id

          visit new_user_path

          fill_in "Email", with: user2.email
          select :manager
          select organization2.name

          click_link_or_button "Invite"
          expect(page.text).to include "already a pending invitation"

          sign_out
          sign_in(user2)

          visit "/"
          expect(page.body).to include("Pending Invitations")

          visit "/users/invitations"
          expect(page.text).to match(/#{organization2.name}/)

          find("a[href*='reject']").click
          expect(page.text).to include("Invitation rejected")
          expect(user2.organization_users.find_by(organization: organization2)).to be_nil
        end
      end
    end
  end

  context "when user is manager and not admin" do
    let(:user) { create :user, privileges: ["user.manage"], admin: false, organization: invited_user.organization }

    it "shows no invitation url" do
      visit user_path(invited_user)
      expect(page.text).not_to include invited_user.invite_code
    end
  end

  context "when user is neither manager nor admin" do
    let(:user) { create :user, admin: false, organization: invited_user.organization }

    it "shows no invitation url" do
      visit user_path(invited_user)
      expect(page.text).not_to include invited_user.invite_code
    end
  end
end
