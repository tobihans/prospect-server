# frozen_string_literal: true

require "rails_helper"

describe "import" do
  let(:project) { create :project }

  describe "admin" do
    let(:user) { create :user, organization: project.organization, admin: true }
    let(:data_source) { create :source, project: }
    let(:import) do
      create :import, source: data_source,
                      file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                      ingestion_finished_at: Time.zone.now,
                      ingestion_started_at: Time.zone.now
    end

    before do
      sign_in(user)
    end

    it "reprocess", :js do
      test_example_insert import, "test"

      visit import_path(import)

      expect(Data::Test.where(import_id: import.id).count).to eq 1

      expect do
        accept_prompt do
          click_on "Reprocess"
        end
      end.to change { Faktory::Queues["etl"].size }.by 1

      expect(page).to have_css("#flash-notice")
      expect(page.text).to include "Reprocessing triggered"
      expect(Data::Test.where(import_id: import.id).count).to eq 0
    end

    it "deletes", :js do
      test_example_insert import, "test"

      visit import_path(import)

      expect(Data::Test.where(import_id: import.id).count).to eq 1

      find_link("Delete") # trying this line to avoid flaky test... if it is still flaky, we can remove it.
      accept_prompt do
        click_on "Delete"
      end

      expect(page).to have_css("#flash-notice")
      expect(page.text).to include "Import deleted"
      expect(data_source.imports.reload.count).to eq 0
      expect(Data::Test.where(import_id: import.id).count).to eq 0
    end
  end
end
