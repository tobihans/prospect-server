# frozen_string_literal: true

require "rails_helper"

describe "Docs" do
  let(:org) { create :organization, id: 12_345 }
  let(:user) { create :user, admin: true, organization: org, id: 12_345 }

  before do
    sign_in(user)
  end

  it "visit all the docs pages" do
    Postgres::DataTable.all.sort_by(&:data_category).each do |dt|
      visit doc_path(dt.data_category)
      expect(page.body).to include "Documentation: #{dt.data_category}"
    end
  end
end
