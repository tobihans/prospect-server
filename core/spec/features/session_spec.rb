# frozen_string_literal: true

require "rails_helper"

describe "render all the pages" do
  it "renders welcome page" do
    Rails.application.config.google_auth = true
    visit "/"
    expect(page.body).to include "sign_in_with_google"
    expect(page.body).to include "symbollockup_signin_light"
  end

  describe "the signin process" do
    let(:user) { create :user }

    it "signs me in and out" do
      visit "/"
      within("form") do
        fill_in "email", with: user.email
        fill_in "password", with: "very_secret_test_password"
      end
      click_link_or_button "Login"
      expect(page.text).to include "Login successful"
      find("a[href='#{logout_users_path}']").click
      expect(page.text).to include "Logged out!"
    end

    it "fails with wrong password" do
      visit "/"
      within("form") do
        fill_in "email", with: user.email
        fill_in "password", with: "wrong password"
      end
      click_link_or_button "Login"
      expect(page.text).to include "Login failed"
    end
  end
end
