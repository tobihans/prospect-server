# frozen_string_literal: true

require "rails_helper"

describe "Organization" do
  context "when a user has admin rights" do
    let(:user) { create :user, :admin }
    let(:creator) { create :user }
    let!(:invited_user) { create :user, :invited, organization: user.organization, active_at: nil, email: "dings@bums.com" }

    before do
      sign_in(user)
      user.organization.user_id = creator.id
    end

    it "list team users" do
      visit "/organizations/mine"
      expect(page.text).to include "The Test Organization"
      expect(page.text).to include "Jonny"
      expect(page.text).to include "admin" # priv summary

      find("a[href='/users/#{user.id}']").click
      # find("a[href='/users/#{user.id}']").click

      expect(page.text).to include user.email
      expect(page.text).to include user.username
    end

    it "shows api key", :js do
      visit organization_path(user.organization)
      find("#show_org_api_token a").click
      expect(page.text).to match(/#{user.organization.api_key}/)
    end

    it "shows edit button" do
      visit "/organizations/mine"
      expect(page).to have_selector(:link_or_button, "Edit")
    end

    it "shows the creator" do
      visit "/organizations/mine"
      expect(page.text).to include "Creator"
      expect(page.text).to include user.organization.creator.username
    end

    it "shows the resend invite button and triggers a mail on click" do
      visit "/organizations/mine"

      expect(page.text).to include invited_user.email
      expect(page).to have_selector(:link_or_button, "Resend invite")
      first("a[href*='/users/#{invited_user.id}/resend_invitation']").click
      expect(page.text).to match(/The invitation has been resent./)
    end
  end

  context "when a non-admin manager is user" do
    let(:user) { create :user, privileges: Privilege::PRIVILEGES }

    before { sign_in(user) }

    it "shows mine" do
      visit "/organizations/mine"
      expect(page.text).to include "The Test Organization"
      expect(page.text).to include "Jonny"
      expect(page.text).to include "manager" # priv summary
    end

    it "shows edit button" do
      visit "/organizations/mine"
      expect(page).to have_selector(:link_or_button, "Edit")
    end
  end

  context "when a non-admin is user" do
    let(:user) { create :user }

    before { sign_in(user) }

    it "shows mine" do
      visit "/organizations/mine"
      expect(page.text).to include "The Test Organization"
      expect(page.text).to include "Jonny"
    end

    it "doesn't show the edit button" do
      visit "/organizations/mine"
      expect(page).to have_no_selector(:link_or_button, "Edit")
    end
  end

  context "when downloading encrypted data", :js do
    let!(:organization) { create :organization, public_key: nil, wrapped_key: nil, iv: nil, salt: nil, crypto_added_at: nil }
    let!(:user) { create :user, admin: true, organization: }
    let!(:project) { create :project, name: "Test Project", organization: user.organization }

    before { sign_in(user) }

    it "decrypts data" do
      visit "/organizations/#{user.organization.id}/edit"

      fill_in "organization[message]", with: "super_secret_encryption_password"
      fill_in "organization[message-confirmation]", with: "super_secret_encryption_password"
      accept_prompt do
        find_by_id("crypt-create-key").click
      end

      find('input[type="submit"]').click

      expect(organization.reload).to be_encryption_keys
      source = Source.create!(data_category: "test", details: { "file_format" => "csv" },
                              kind: "manual_upload",
                              name: "en/decryption test",
                              activated_at: Time.zone.now,
                              project:)

      source.trigger_import! "float_column,name_column\n1.0,Customer Name"

      import = source.imports.reload.first
      data = JSON.parse(import.file.download)

      entry = Data::Test.create!(uid: "some_uid",
                                 float_column: data["test"].first["float_column"],
                                 name_column_e: data["test"].first["name_column_e"],
                                 organization_id: user.organization.id,
                                 data_origin: "manual_upload",
                                 import_id: import.id,
                                 source_id: source.id)

      expect(entry.name_column_e).to be_present
      expect(entry.name_column_e).not_to eq "Customer Name"

      file = "#{page.driver.browser.options.save_path}/data.csv"

      # remove the download file if it exists already
      FileUtils.rm_f file

      visit exports_path

      click_on "decrypted csv"
      fill_in :decrypt_password, with: "super_secret_encryption_password"
      click_on "Open Vault"

      max_wait = 3
      while !File.exist?(file) && max_wait > 0
        sleep(0.1)
        max_wait -= 0.1
      end

      data = CSV.parse(File.read(file), headers: true).first
      expect(data["name_column"]).to eq "Customer Name"
      expect(Data::Test.first.name_column_e).not_to be_nil
    end
  end

  context "with rbf" do
    let!(:source) { create :source, :rbf_ueccc_ogs }
    let!(:organization) { source.organization }

    it "#submittable_rbf_claim_templates are present" do
      expect(organization.submittable_rbf_claim_templates).to be_present
    end

    it "#rbf_submitting? is true" do
      expect(organization).to be_rbf_submitting
    end

    it "#rbf_claim_templates are present" do
      expect(organization.rbf_claim_templates).to be_present
    end

    context "without rbf" do
      let!(:source) { create :source }
      let!(:organization) { source.organization }

      it "#submittable_rbf_claim_templates are not present" do
        expect(organization.submittable_rbf_claim_templates).not_to be_present
      end

      it "#rbf_submitting? is false" do
        expect(organization).not_to be_rbf_submitting
      end

      it "#rbf_claim_templates are not present" do
        expect(organization.rbf_claim_templates).not_to be_present
      end
    end
  end
end
