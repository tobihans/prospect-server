# frozen_string_literal: true

require "rails_helper"

describe "Dashboard" do
  let(:org) { create :organization, id: 12_345 }
  let(:user) { create :user, admin: true, organization: org, id: 12_345 }

  before do
    sign_in(user)
  end

  it "visit dashboard" do
    VCR.use_cassette(:grafana_dashboards) do
      Grafana::RemoteControl.instance_variable_set("@client", nil)

      VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
        org.setup_grafana!
      end

      # returns 1 for first call and 0 for all afterwards
      allow(Postgres::RemoteControl).to receive(:count_as_organization).and_return(1, 0)

      visit "/analytics"
      page.driver.wait_for_network_idle

      expect(page.text).to include "Your Dashboards"
      find_all("#dashboard_tile a").first.trigger("click")

      # Current object resets after page visit
      Current.user = user

      expect(page.body).to include Current.url_login_token.split(".").first
    end
  end
end
