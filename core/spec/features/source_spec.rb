# frozen_string_literal: true

require "rails_helper"

describe "build source" do
  let(:project) { create :project }
  let(:another_project) { create :project }

  describe "user" do
    let(:user) { create :user, organization: project.organization }

    before do
      sign_in(user)
    end

    it "can not find button to create source" do
      visit project_path(project)
      expect(page.body).not_to include "Create New Data Source"
    end
  end

  describe "priv manage sources" do
    let(:user) { create :user, privileges: ["source.manage"], organization: project.organization }

    before do
      sign_in(user)
    end

    it "build pull API" do
      create :organization

      visit project_path(project)

      first(:link, nil, href: %r{/build_source/}).click
      fill_in :source_name, with: "Test Source"
      choose("source_kind_api_push")
      find("input[value='Next']").click
      choose("source_data_category_shs")
      find("input[value='Next']").click

      expect(page.text).to include "Please POST your data to"

      find("input[value='Next']").click

      find_by_id("source_active").check
      find("input[value='Finish']").click

      source = Source.find_by(name: "Test Source")
      expect(source).not_to be_nil

      expect(source.state).to eq "active"
      expect(source.to_s).to eq "Test Source (SHS)"

      visit project_path(project)
      expect(page.text).to match(/Test Source\nSHS\nApi Push\nactiv/)

      visit source_path(source)
      expect(page.text).to match(/Test Source/)
      expect(page.text).to match(/Category shs/)
      expect(page.text).to match(/Kind api_push/)
      expect(page.text).to match(/The Test Organization/)
    end

    it "build push API" do
      visit project_path(project)

      first(:link, nil, href: %r{/build_source/}).click
      fill_in :source_name, with: "Test Source"

      choose("source_kind_api_push")
      find("input[value='Next']").click
      choose("source_data_category_meters")
      find("input[value='Next']").click
      find("input[value='Next']").click

      source = Source.find_by(name: "Test Source")
      expect(source).not_to be_nil

      expect(source.state).to eq "inactive"
      expect(source.to_s).to eq "Test Source (Meters)"
    end
  end

  describe "admin" do
    let(:user) { create :user, admin: true, organization: project.organization }
    let(:data_source) { create :source, project: }
    let(:another_data_source) { create :source, project: another_project }
    let(:s3_data_source) do
      create :source,
             kind: "s3_bucket",
             secret: "some_secret",
             project:,
             details: { bucket_name: "bucket_name",
                        region: "us-east-1",
                        access_key: "ASDFSFA",
                        file_format: "json",
                        frequency: "monthly",
                        order_by: "lexicographical" }
    end

    before do
      WebMock.disable_net_connect!(allow_localhost: true)
      sign_in(user)

      # This seems to set stub_responses: true on any
      # AWS Client that will be generated.
      #
      # The AWS stubbing provides empty default responses,
      # we just overwrite the ones we need.
      Aws.config[:s3] = {
        stub_responses: {
          # list_buckets: { buckets: [{name: 'my-bucket' }] },
          list_objects_v2: { contents: [{ key: "an_object_key" }] }
        }
      }
    end

    it "deletes source" do
      visit source_path(data_source)
      find_by_id("delete_source").click
      expect(page.text).to include "Data Source deleted"
    end

    it "deletes all data and imports", :js do
      import = Import.create source: data_source
      test_example_insert import, "test"

      expect(Data::Test.where(import_id: import.id).count).to eq 1

      visit source_path(data_source)

      expect(page.text).to include "Imports (1)"

      accept_confirm do
        click_on("Delete all data and imports")
      end

      expect(page).to have_css("#flash-notice")
      expect(page.text).to include "All source data and imports deleted"
      expect(data_source.imports.reload.count).to eq 0
      expect(Data::Test.where(import_id: import.id).count).to eq 0
    end

    it "triggers reprocessing of all imports", :js do
      import = Import.create source: data_source,
                             file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                             ingestion_finished_at: Time.zone.now,
                             ingestion_started_at: Time.zone.now
      test_example_insert import, "test"

      expect(Data::Test.where(import_id: import.id).count).to eq 1

      visit source_path(data_source)
      expect(page.text).to include "Imports (1)"

      expect do
        accept_confirm do
          click_on("Reprocess")
        end
      end.to change { Faktory::Queues["etl"].size }.by 1

      expect(data_source.imports.reload.count).to eq 1
      expect(Data::Test.where(import_id: import.id).count).to eq 0
    end

    context "when viewing import index" do
      it "has index link and paginates with more than 30 imports" do
        31.times do
          i = Import.create source: data_source,
                            file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                            ingestion_finished_at: Time.zone.now,
                            ingestion_started_at: Time.zone.now

          test_example_insert i, "test"
        end

        visit source_path(data_source)
        expect(page.text).to include "Imports (31)"
        click_on("Show More")
        expect(page).to have_css("a[href='#{import_path(data_source.imports.last)}']")
        expect(page).to have_css("nav.pagy", visible: :visible)
      end

      it "does not show more link with less than 10 imports" do
        9.times do
          i = Import.create source: data_source,
                            file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                            ingestion_finished_at: Time.zone.now,
                            ingestion_started_at: Time.zone.now

          test_example_insert i, "test"
        end

        visit source_path(data_source)
        expect(page.text).to include "Imports (9)"
        expect(page).to have_no_text("Show More")
      end

      it "does not show imports from another organization/source" do
        i = Import.create source: data_source,
                          file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                          ingestion_finished_at: Time.zone.now,
                          ingestion_started_at: Time.zone.now

        test_example_insert i, "test"

        i2 = Import.create source: another_data_source,
                           file: Rack::Test::UploadedFile.new(Tempfile.new("data.csv")),
                           ingestion_finished_at: Time.zone.now,
                           ingestion_started_at: Time.zone.now

        test_example_insert i2, "test"

        visit source_imports_path(data_source)
        expect(page).to have_css("a[href='#{import_path(data_source.imports.first)}']")
        expect(page).to have_no_css("a[href='#{import_path(another_data_source.imports.first)}']")
      end
    end

    it "check S3 validations" do
      data_source

      visit project_path(project)

      find_link(href: %r{/build_source/}, title: "Add new Datasource").click
      fill_in :source_name, with: data_source.name
      find("input[value='Next']").click

      expect(page.text).to include "Kind can't be blank"
      expect(page.text).to include "Name has already been taken"

      choose("source_kind_s3_bucket")
      fill_in :source_name, with: "Way better Source name"
      find("input[value='Next']").click

      # now we are on the connection page
      find("input[value='Next']").click
      expect(page.text).to include "Data category can't be blank"

      choose("source_data_category_grids_ts")
      find("input[value='Next']").click

      expect(page.text).to include "Secret can't be blank"
      expect(page.text).to include "Region must be present"
      expect(page.text).to include "Access key must be present"
      expect(page.text).to include "Bucket name must be present"
      expect(page.text).to include "Order by must be present"
      fill_in :source_secret, with: "super_secret_aws_access_key"
      fill_in :source_details_region, with: "some_region"
      fill_in :source_details_access_key, with: "XKYJVK1234"
      fill_in :source_details_bucket_name, with: "my_bucket"
      choose("source_details_order_by_lexicographical")

      find("input[value='Next']").click

      expect(page.text).to include "Checking AWS credentials ... ERROR"
    end

    it "edit meta" do
      visit build_source_path(Source::BUILD_STEPS.first, source_id: s3_data_source.id)
      expect(find_by_id("source_name").value).to include s3_data_source.name
      fill_in :source_name, with: "Way better Source name"
      find("input[value='Next']").click
      expect(s3_data_source.reload.name).to eq "Way better Source name"
    end

    it "trigger test" do
      visit build_source_path(Source::BUILD_STEPS.third, source_id: s3_data_source.id)
      expect(page.text).to include ">>> ERROR"
      expect(page.text).to include "No data"
    end

    it "shows secret", :js do
      visit source_path(s3_data_source)
      find("#show_source_secret a").click
      expect(page.text).to match(/#{s3_data_source.secret}/)
    end

    context "when source is of kind api/stellar_multi_org_parent" do
      let(:stellar_source) { create :source, kind: "api/stellar_multi_org_parent", project: }

      it "lets admin configure frequency" do
        visit build_source_path(Source::BUILD_STEPS[1], source_id: stellar_source.id)
        allow_any_instance_of(Source::Api::StellarMultiOrgParent).to receive(:create_imports).and_return(nil)

        fill_in :source_secret, with: "some_secret"
        select "5 minutes", from: "Frequency"
        click_on "Next"
        expect(stellar_source.reload.details["frequency"]).to eq "5 minutes"
      end
    end

    context "when source is of kind api/angaza" do
      custom_attributes = [
        "Full Name", "Primary Phone",
        "NRC Number", "Does The Client Have An Alternative Name (Nickname)",
        "Age (Years)", "Gender", "Client Photo"
      ]
      let(:angaza_source) do
        create :source,
               project:,
               kind: "api/angaza",
               secret: "some_secret",
               details: { username: "test@a2ei.org" }
      end
      let(:adapter) { Source::Api::Angaza.new(angaza_source) }

      let(:result) do
        VCR.use_cassette :api_connector_angaza_test do
          adapter.validation_result
        end
      end

      it "lets user configure attribute mapping" do
        create :organization

        visit project_path(project)
        first(:link, nil, href: %r{/build_source/}).click
        fill_in :source_name, with: "Test Angaza Source"
        choose("source_kind_apiangaza")
        find("input[value='Next']").click
        allow_any_instance_of(Source::Api::Angaza).to receive_messages(validation_result: result, fetch: result)
        allow_any_instance_of(Source::Api::Angaza).to receive_messages(custom_attributes:)

        fill_in :source_secret, with: "some_secret"
        fill_in :source_details_username, with: "username"
        find("input[value='Next']").click
        expect(page.text).to include ">>> SUCCESS"
        find("input[value='Next']").click

        expect(page.text).to include "To import data from your organization" # some of the explanation
        expect(page.text).to include "Full Name"

        page.select "customer_name", from: "Full Name"
        page.select "customer_phone", from: "Primary Phone"
        page.select "ignore", from: "NRC Number"
        page.select "custom_anonymize", from: "Does The Client Have An Alternative Name (Nickname)"
        page.select "custom", from: "Age (Years)"
        page.select "customer_gender", from: "Gender"
        page.select "custom_anonymize", from: "Client Photo"

        find("input[value='Next']").click

        find_by_id("source_active").check
        find("input[value='Finish']").click

        expect(angaza_source.reload.details["username"]).to eq "test@a2ei.org"
      end

      it "doesn't let user configure attribute mapping with duplicate attributes" do
        create :organization

        visit project_path(project)
        first(:link, nil, href: %r{/build_source/}).click
        fill_in :source_name, with: "Test Angaza Source"
        choose("source_kind_apiangaza")
        find("input[value='Next']").click
        allow_any_instance_of(Source::Api::Angaza).to receive_messages(validation_result: result, fetch: result)
        allow_any_instance_of(Source::Api::Angaza).to receive_messages(custom_attributes:)

        fill_in :source_secret, with: "some_secret"
        fill_in :source_details_username, with: "username"
        find("input[value='Next']").click
        expect(page.text).to include ">>> SUCCESS"
        find("input[value='Next']").click

        expect(page.text).to include "To import data from your organization" # some of the explanation
        expect(page.text).to include "Full Name"

        page.select "customer_name", from: "Full Name"
        page.select "customer_name", from: "Primary Phone"

        find("input[value='Next']").click
        expect(page.text).to include "attribute customer_name is used more than once"
        expect(page.text).to include "Full Name"
        expect(page.text).to include "Primary Phone"
      end
    end
  end
end
