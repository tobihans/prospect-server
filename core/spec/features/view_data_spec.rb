# frozen_string_literal: true

require "rails_helper"

describe "View Data" do
  let(:org) { create :organization, id: 12_345 }
  let(:user) { create :user, admin: true, organization: org, id: 12_345 }

  before do
    sign_in(user)
  end

  it "only loads images if we use the button", :js do
    source = create :source, organization: org
    source.project.create_default_visibility

    import = create(:import, source:)
    test_example_insert import, "test"
    empty_png = [137, 80, 78, 71, 13, 10, 26, 10].pack("C*")
    code = EncryptedBlob.store_data! empty_png, import.organization, import

    data = Data::Test.where(import_id: import.id).first
    data.custom[:image] = code
    data.save!

    visit source_view_data_path(import.source.id)

    max_wait = 3

    while (page.driver.network_traffic.last.url.match(%r{api/v1/out/test}).nil? || !page.driver.network_traffic.last.finished?) && max_wait > 0
      sleep(0.1)
      max_wait -= 0.1
    end

    # TODO: figure out how to set a format without mapper
    expect(page.text).to match(/http.*encrypted_blob/)
  end
end
