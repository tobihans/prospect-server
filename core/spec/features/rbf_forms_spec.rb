# frozen_string_literal: true

require "rails_helper"

describe "rbf_forms" do
  let(:purchase_date) { Time.zone.today.to_s }
  let(:payment_date) { (Time.zone.today - 6.days).to_s }

  describe "ogs" do
    let!(:source) { create :source, :rbf_ueccc_ogs }
    let!(:rbf_product) do
      create :rbf_product,
             rbf_claim_template: source.adapter.rbf_claim_template,
             organization: source.organization
    end
    let!(:rbf_product_price) { create :rbf_product_price, rbf_product: }

    it "creates processed import", :js do
      visit dynamic_form_sources_path(slug: source.details["slug"])

      expect(page.text).to include "OGS Sales Form"

      fill_in :given_name, with: "Peter"
      fill_in :surname, with: "Pan"

      select "National ID", from: :national
      fill_in :id_number, with: "123NATIONALID3"

      select "Female", from: :gender
      select "true", from: :household_head

      fill_in "contact_phone", with: "0123456789"
      fill_in "alternative_contact_phone", with: "0987654321"

      execute_script 'document.getElementById("gps_location").value = "52.4885925,13.4099301"'
      expect(find(id: "gps_location").value).to eq "52.4885925,13.4099301"

      find(id: "location_query-ts-control").native.send_keys "Bila"
      find(id: "location_query-opt-1").click

      select rbf_product.manufacturer, from: :product_brand
      select rbf_product.model, from: :product_model
      fill_in :product_serial_number, with: "SERIAL-123"
      fill_in :product_secondary_serial_number, with: "SERIAL-2-123"

      select "PAYGO", from: :paygo_or_cash_customer
      # BUG; select does only trigger change but not input - https://github.com/rubycdp/cuprite/issues/223
      # so we fill in after we select above to trigger it
      fill_in :date, with: purchase_date

      expect(find(id: "price").value.to_f).to eq rbf_product_price.base_subsidized_sales_price

      fill_in :payment_account_id, with: "ACCOUNT-1234"

      fill_in :down_payment, with: rbf_product_price.base_subsidized_sales_price + 1
      expect(page.text).to include "Should not be higher than"

      fill_in :down_payment, with: rbf_product_price.base_subsidized_sales_price
      expect(page.text).not_to include "Should not be higher than"

      fill_in :duration, with: 365

      find(id: "time_of_transaction").native.send_keys "1155AM"
      fill_in :payment_amount, with: 90_000
      select "Mobile Money Airtel", from: :type_of_payment
      fill_in :provider_transaction_id, with: "TXID007"

      attach_file("photo_upload", File.absolute_path("spec/fixtures/test.png"), make_visible: true)

      old_imports_count = Import.count

      click_on "Submit"
      find(id: "confirmSubmitButton").click

      page.driver.wait_for_network_idle

      expect(Import.count).to eq(old_imports_count + 1)

      i = Import.last
      expect(i.error).to be_nil
      expect(i).to be_ingestion_finished

      # Check the data gets converted correctly
      stored_data = JSON.parse i.file.download
      expect(stored_data["shs"].count).to eq 1
      expect(stored_data["payments_ts"].count).to eq 1

      device_data = stored_data["shs"].first

      expected_device = {
        "account_external_id" => "ACCOUNT-1234",
        "customer_external_id" => "ugx_unknown",
        "customer_gender" => "F",
        "is_household_head" => "true",
        "customer_id_type" => "National ID",
        "customer_location_area_1" => "Northern",
        "customer_location_area_2" => "Agago",
        "customer_location_area_3" => "Arum",
        "customer_location_area_4" => "Alela",
        "customer_location_area_5" => "Bila",
        "device_external_id" => "SERIAL-123",
        "manufacturer" => "Manufacturer",
        "model" => rbf_product.model,
        "payment_plan_type" => "paygo",
        "payment_plan_currency" => "UGX",
        "payment_plan_days" => 365.0,
        "payment_plan_down_payment" => 80_000.0,
        "purchase_date" => purchase_date,
        "serial_number" => "SERIAL-123",
        "serial_number_2" => "SERIAL-2-123",
        "total_price" => rbf_product_price.base_subsidized_sales_price,
        "customer_latitude_b" => 52.489,
        "customer_longitude_b" => 13.41
      }

      check_subhash device_data, expected_device
      check_p14n_values device_data

      pseudo_keys =
        %w[customer_firstname_e customer_surname_e customer_id_number_e customer_id_number_p
           customer_name_e customer_name_p customer_phone_e customer_phone_p customer_phone_2_e customer_phone_2_p
           customer_latitude_e customer_latitude_p customer_longitude_e customer_longitude_p gps_location_e customer_id_image_e]
      check_no_plaintext device_data, pseudo_keys
      check_has_no_more_keys device_data, (expected_device.keys + pseudo_keys)

      expected_payment = {
        "account_external_id" => "ACCOUNT-1234",
        "account_origin" => "shs",
        "amount" => 90_000.0,
        "currency" => "UGX",
        "paid_at" => "#{purchase_date} 11:55:00 UTC",
        "paid_at_date" => purchase_date,
        "paid_at_time" => "11:55",
        "payment_external_id" => "ugx_unknown",
        "provider_name" => "airtel",
        "provider_transaction_id" => "TXID007"
      }
      expect(stored_data["payments_ts"].first).to eq expected_payment
    end

    # NOW break it! #theAxeInTheWoods
    describe "ogs Crash Test Lab" do
      it "validates required_if fields", :js do
        visit dynamic_form_sources_path(slug: source.details["slug"])

        expect(page.text).to include "OGS Sales Form"
        select rbf_product.manufacturer, from: :product_brand
        select rbf_product.model, from: :product_model
        fill_in :date, with: purchase_date
        select "PAYGO", from: :paygo_or_cash_customer
        select "Mobile Money Airtel", from: :type_of_payment

        click_on "Submit"
        page.driver.wait_for_network_idle

        expect(page.text).to include "Please enter value if PAYGO OR CASH CUSTOMER is filled with paygo"
        expect(page.text).to include "Please enter value if TYPE OF PAYMENT is filled with mtn, airtel"
      end
    end
  end

  describe "ccs" do
    let!(:source) { create :source, :rbf_ueccc_ccs }
    let!(:rbf_product) do
      create :rbf_product,
             rbf_claim_template: source.adapter.rbf_claim_template,
             organization: source.organization
    end
    let!(:rbf_product_price) { create :rbf_product_price, rbf_product: }

    it "creates processed import", :js do
      visit dynamic_form_sources_path(slug: source.details["slug"])
      expect(page.text).to include "CCS Sales Form"

      fill_in :given_name, with: "Peter"
      fill_in :surname, with: "Pan"

      select "National ID", from: :national
      fill_in :id_number, with: "123NATIONALID3"

      select "Male", from: :gender

      fill_in "contact_phone", with: "0123456789"
      fill_in "alternative_contact_phone", with: "0987654321"

      execute_script 'document.getElementById("gps_location").value = "52.4885925,13.4099301"'
      expect(find(id: "gps_location").value).to eq "52.4885925,13.4099301"

      find(id: "location_query-ts-control").native.send_keys "Bila"
      find(id: "location_query-opt-1").click

      select rbf_product.manufacturer, from: :product_brand
      select rbf_product.model, from: :product_model
      fill_in :product_serial_number, with: "SERIAL-123"
      fill_in :product_secondary_serial_number, with: "SERIAL-2-123"
      select "PAYGO", from: :paygo_or_cash_customer

      # BUG; select does only trigger change but not input - https://github.com/rubycdp/cuprite/issues/223
      # so we fill in after we select above to trigger it
      fill_in :date, with: purchase_date
      page.execute_script("document.getElementById('date').dispatchEvent(new Event('change'));")

      expect(find(id: "date_of_transaction").value).to eq purchase_date

      expect(find(id: "price").value.to_f).to eq rbf_product_price.base_subsidized_sales_price

      fill_in :payment_account_id, with: "ACCOUNT-1234"

      fill_in :down_payment, with: 80_000
      fill_in :duration, with: 365

      # fill_in :date_of_transaction, with: payment_date
      find(id: "time_of_transaction").native.send_keys "0706AM"
      fill_in :payment_amount, with: 90_000
      select "Mobile Money Airtel", from: :type_of_payment
      fill_in :provider_transaction_id, with: "TXID007"

      attach_file("photo_upload", File.absolute_path("spec/fixtures/test.png"), make_visible: true)

      old_imports_count = Import.count

      click_on "Submit"
      find(id: "confirmSubmitButton").click

      page.driver.wait_for_network_idle

      expect(Import.count).to eq(old_imports_count + 1)

      i = Import.last
      expect(i.error).to be_nil
      expect(i).to be_ingestion_finished

      # Check the data gets converted correctly
      stored_data = JSON.parse i.file.download
      expect(stored_data["meters"].count).to eq 1
      expect(stored_data["payments_ts"].count).to eq 1

      device_data = stored_data["meters"].first

      expected_device = {
        "account_external_id" => "ACCOUNT-1234",
        "customer_external_id" => "ugx_unknown",
        "customer_gender" => "M",
        "is_household_head" => "false",
        "customer_id_type" => "National ID",
        "customer_location_area_1" => "Northern",
        "customer_location_area_2" => "Agago",
        "customer_location_area_3" => "Arum",
        "customer_location_area_4" => "Alela",
        "customer_location_area_5" => "Bila",
        "device_external_id" => "SERIAL-123",
        "manufacturer" => "Manufacturer",
        "model" => rbf_product.model,
        "payment_plan" => "paygo",
        "payment_plan_currency" => "UGX",
        "payment_plan_days" => 365.0,
        "payment_plan_down_payment" => 80_000.0,
        "purchase_date" => purchase_date,
        "serial_number" => "SERIAL-123",
        "serial_number_2" => "SERIAL-2-123",
        "total_price" => rbf_product_price.base_subsidized_sales_price,
        "customer_latitude_b" => 52.489,
        "customer_longitude_b" => 13.41
      }
      check_subhash device_data, expected_device
      check_p14n_values device_data

      new_keys =
        %w[customer_firstname_e customer_surname_e customer_id_number_e customer_id_number_p customer_name_e
           customer_name_p customer_phone_e customer_phone_p customer_phone_2_e customer_phone_2_p
           customer_latitude_e customer_latitude_p customer_longitude_e customer_longitude_p gps_location_e customer_id_image_e]
      check_no_plaintext device_data, new_keys

      check_has_no_more_keys device_data, (expected_device.keys + new_keys)

      expected_payment = {
        "account_external_id" => "ACCOUNT-1234",
        "account_origin" => "shs",
        "amount" => 90_000.0,
        "currency" => "UGX",
        "paid_at" => "#{purchase_date} 07:06:00 UTC",
        "paid_at_date" => purchase_date,
        "paid_at_time" => "07:06",
        "payment_external_id" => "ugx_unknown",
        "provider_name" => "airtel",
        "provider_transaction_id" => "TXID007"
      }
      expect(stored_data["payments_ts"].first).to eq expected_payment
    end

    describe "ccs Crash Test Lab" do
      it "validates required_if fields", :js do
        visit dynamic_form_sources_path(slug: source.details["slug"])

        expect(page.text).to include "CCS Sales Form"
        select rbf_product.manufacturer, from: :product_brand
        select rbf_product.model, from: :product_model
        fill_in :date, with: purchase_date
        select "PAYGO", from: :paygo_or_cash_customer
        select "Mobile Money Airtel", from: :type_of_payment

        click_on "Submit"
        page.driver.wait_for_network_idle

        expect(page.text).to include "Please enter value if PAYGO OR CASH CUSTOMER is filled with paygo"
        expect(page.text).to include "Please enter value if TYPE OF PAYMENT is filled with mtn, airtel"
      end
    end
  end

  describe "pue" do
    let!(:source) { create :source, :rbf_ueccc_pue }
    let!(:rbf_product) do
      create :rbf_product,
             rbf_claim_template: source.adapter.rbf_claim_template,
             organization: source.organization
    end
    let!(:rbf_product_price) { create :rbf_product_price, rbf_product: }

    it "creates processed import", :js do
      visit dynamic_form_sources_path(slug: source.details["slug"])
      expect(page.text).to include "PUE Sales Form"

      fill_in :given_name, with: "Peter"
      fill_in :surname, with: "Pan"

      select "National ID", from: :national
      fill_in :id_number, with: "123NATIONALID3"

      select "Female", from: :gender

      select "Micro or Small Enterprise (MSE)", from: :customer_category
      select "true", from: :is_below_50_employees_and_100mio_ugx_assets

      fill_in "contact_phone", with: "0123456789"
      fill_in "alternative_contact_phone", with: "0987654321"

      execute_script 'document.getElementById("gps_location").value = "52.4885925,13.4099301"'
      expect(find(id: "gps_location").value).to eq "52.4885925,13.4099301"

      find(id: "location_query-ts-control").native.send_keys "Bila"
      find(id: "location_query-opt-1").click

      select rbf_product.manufacturer, from: :product_brand
      select rbf_product.model, from: :product_model
      fill_in :product_serial_number, with: "SERIAL-123"
      fill_in :product_secondary_serial_number, with: "SERIAL-2-123"
      fill_in :product_tertiary_serial_number, with: "SERIAL-3-123"

      select "PAYGO", from: :paygo_or_cash_customer

      select "false", from: :female_owned_business
      fill_in :date, with: purchase_date # BUG; select does only trigger change but not input - https://github.com/rubycdp/cuprite/issues/223 - so we fill in after we select above to trigger it
      expect(find(id: "price").value.to_f).to eq rbf_product_price.base_subsidized_sales_price

      select "true", from: :female_owned_business
      fill_in :date, with: purchase_date # BUG; select does only trigger change but not input - https://github.com/rubycdp/cuprite/issues/223 - so we fill in after we select above to trigger it
      expect(find(id: "price").value.to_f).to eq rbf_product_price.base_subsidized_sales_price - 50_000

      fill_in :payment_account_id, with: "ACCOUNT-1234"

      fill_in :down_payment, with: 20_000
      fill_in :duration, with: 365

      find(id: "time_of_transaction").native.send_keys "1155AM"
      fill_in :payment_amount, with: 90_000
      select "Mobile Money Airtel", from: :type_of_payment
      fill_in :provider_transaction_id, with: "TXID007"

      attach_file("photo_upload", File.absolute_path("spec/fixtures/test.png"), make_visible: true)

      old_imports_count = Import.count

      click_on "Submit"
      find(id: "confirmSubmitButton").click

      page.driver.wait_for_network_idle

      expect(Import.count).to eq(old_imports_count + 1)

      i = Import.last
      expect(i.error).to be_nil
      expect(i).to be_ingestion_finished

      # Check the data gets converted correctly
      stored_data = JSON.parse i.file.download

      expect(stored_data["meters"].count).to eq 1
      expect(stored_data["payments_ts"].count).to eq 1

      device_data = stored_data["meters"].first

      expected_device = {
        "account_external_id" => "ACCOUNT-1234",
        "customer_category" => "mse",
        "customer_external_id" => "ugx_unknown",
        "customer_gender" => "F",
        "customer_id_type" => "National ID",
        "customer_location_area_1" => "Northern",
        "customer_location_area_2" => "Agago",
        "customer_location_area_3" => "Arum",
        "customer_location_area_4" => "Alela",
        "customer_location_area_5" => "Bila",
        "device_external_id" => "SERIAL-123",
        "is_below_50_employees_and_100mio_ugx_assets" => "true",
        "is_female_owned" => "true",
        "manufacturer" => "Manufacturer",
        "model" => rbf_product.model,
        "payment_plan" => "paygo",
        "payment_plan_currency" => "UGX",
        "payment_plan_days" => 365.0,
        "payment_plan_down_payment" => 20_000.0,
        "purchase_date" => purchase_date,
        "serial_number" => "SERIAL-123",
        "serial_number_2" => "SERIAL-2-123",
        "serial_number_3" => "SERIAL-3-123",
        "total_price" => rbf_product_price.base_subsidized_sales_price - 50_000,
        "customer_latitude_b" => 52.489,
        "customer_longitude_b" => 13.41
      }
      check_subhash device_data, expected_device
      check_p14n_values device_data

      new_keys = %w[customer_firstname_e customer_surname_e customer_id_number_e customer_id_number_p customer_name_e
                    customer_name_p customer_phone_e customer_phone_p customer_phone_2_e customer_phone_2_p
                    customer_latitude_e customer_latitude_p customer_longitude_e customer_longitude_p gps_location_e customer_id_image_e]
      check_no_plaintext device_data, new_keys
      check_has_no_more_keys device_data, (expected_device.keys + new_keys)

      expected_payment = {
        "account_external_id" => "ACCOUNT-1234",
        "account_origin" => "shs",
        "amount" => 90_000.0,
        "currency" => "UGX",
        "paid_at" => "#{purchase_date} 11:55:00 UTC",
        "paid_at_date" => purchase_date,
        "paid_at_time" => "11:55",
        "payment_external_id" => "ugx_unknown",
        "provider_name" => "airtel",
        "provider_transaction_id" => "TXID007"
      }
      expect(stored_data["payments_ts"].first).to eq expected_payment
    end

    describe "pue Crash Test Lab" do
      it "validates required_if fields", :js do
        visit dynamic_form_sources_path(slug: source.details["slug"])

        expect(page.text).to include "PUE Sales Form"
        select rbf_product.manufacturer, from: :product_brand
        select rbf_product.model, from: :product_model
        fill_in :date, with: purchase_date
        select "PAYGO", from: :paygo_or_cash_customer
        select "Mobile Money Airtel", from: :type_of_payment

        click_on "Submit"
        page.driver.wait_for_network_idle

        expect(page.text).to include "Please enter value if PAYGO OR CASH CUSTOMER is filled with paygo"
        expect(page.text).to include "Please enter value if TYPE OF PAYMENT is filled with mtn, airtel"
      end
    end
  end

  describe "payments" do
    let!(:source) { create :source, :rbf_ueccc_payments }

    it "creates processed import", :js do
      visit dynamic_form_sources_path(slug: source.details["slug"])
      expect(page.text).to include "Payments Form"

      fill_in :payment_account_id, with: "PAYACC123"

      fill_in :date_of_transaction, with: payment_date
      find(id: "time_of_transaction").native.send_keys "1155AM"
      fill_in :payment_amount, with: 90_000
      select "Mobile Money Airtel", from: :type_of_payment
      fill_in :provider_transaction_id, with: "TXID007"

      old_imports_count = Import.count

      click_on "Submit"
      find(id: "confirmSubmitButton").click

      page.driver.wait_for_network_idle

      expect(Import.count).to eq(old_imports_count + 1)

      i = Import.last
      expect(i.error).to be_nil
      expect(i).to be_ingestion_finished

      # Check the data gets converted correctly
      stored_data = JSON.parse i.file.download
      expected = {
        "payments_ts" => [
          {
            "account_external_id" => "PAYACC123",
            "account_origin" => "shs",
            "amount" => 90_000.0,
            "currency" => "UGX",
            "paid_at" => "#{payment_date} 11:55:00 UTC",
            "paid_at_date" => payment_date,
            "paid_at_time" => "11:55",
            "payment_external_id" => "ugx_unknown",
            "provider_name" => "airtel",
            "provider_transaction_id" => "TXID007"
          }
        ]
      }
      expect(stored_data).to eq expected
    end
  end

  def check_subhash(given, expected)
    expected.each do |k, v|
      given_val = given[k]
      expect(given_val).to eq(v), "key: #{k} - given: #{given_val.inspect} - but expected #{v.inspect}"
    end
  end

  def check_p14n_values(device_data)
    expected_pseudo = {
      "customer_id_number_p" => "123NATIONALID3",
      "customer_name_p" => "Peter Pan",
      "customer_phone_p" => "0123456789",
      "customer_phone_2_p" => "0987654321"
    }
    expected_pseudo.each do |key, plain|
      expected_p14 = P14n.text(plain)
      given_p14 = device_data[key]

      expect(given_p14).to eq(expected_p14), "#{key} does not match p14 of #{plain} (given: #{given_p14}, expected: #{expected_p14}"
    end
  end

  def check_has_no_more_keys(hash, expected_keys)
    expect(hash.compact.keys).to match_array(expected_keys)
  end

  def check_no_plaintext(hash, converted_keys)
    # collect here all possible plain text values from tests above which should be converted
    plain_text_values =
      %w[Peter Pan 123NATIONALID3 12345678 87654321 123456789 987654321 0987654321 0123456789
         52.4885925,13.4099301 52.488 13.409]

    # collect here all possible plain text keys from tests above which should be converted
    plain_text_keys =
      %w[customer_firstname customer_id_number customer_name customer_surname customer_phone
         alternative_contact_phone gps_location]

    # check that plain text keys are all gone and converted
    plain_text_keys.each do |ptk|
      expect(hash.keys.map(&:to_s)).not_to include ptk
    end

    # check that encrypted / p14n keys are all there and dont contain any of our known plaintext keys
    converted_keys.each do |ck|
      expect(hash[ck]).to be_present, "converted key #{ck} is not present but should be"
    end

    # check that none of the plain text values is in no value
    plain_text_values.each do |ptv|
      hash.each do |k, v|
        includes_ptv = v.to_s.downcase.include?(ptv.downcase) && v.to_s.length < 100
        expect(includes_ptv).to be_falsey, "#{k} should not contain the plain text value #{ptv}, given: #{v}"
      end
    end
  end
end
