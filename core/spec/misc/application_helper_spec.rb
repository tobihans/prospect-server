# frozen_string_literal: true

require "rails_helper"

describe ApplicationHelper do
  include described_class

  describe "#tw_submit_prominent_args" do
    it "includes some classes" do
      expect(tw_submit_prominent_args[:class]).to include "uppercase"
    end

    it "Joins stuff" do
      base_str = tw_submit_prominent_args[:class]
      expect(tw_submit_prominent_args(%w[String1 String2])[:class]).to eq "#{base_str} String1 String2"
    end

    it "can also remove classes" do
      class_str = tw_submit_prominent_args(%w[String1 String2], without: %w[uppercase])[:class]
      expect(class_str).to include "String1"
      expect(class_str).to include "String2"
      expect(class_str).not_to include "uppercase"
    end

    it "raises on wrong argument type" do
      expect { tw_submit_prominent_args DateTime.now }.to raise_exception RuntimeError
    end

    it "raises on unhandled color" do
      expect { tw_submit_prominent_args(color: :unhandled) }.to raise_error "unhandled color unhandled!"
    end
  end

  describe "tw_link_prominent_args" do
    it "is a mixture with prominent submit" do
      expect(tw_link_prominent_args[:class]).to include "hover:animate-pulse"
      expect(tw_link_prominent_args[:class]).to include "rounded-full"
    end

    it "can be configured" do
      class_str = tw_link_prominent_args(
        %w[custom-class],
        color: :bad,
        without: %w[uppercase shadow]
      )[:class]

      expect(class_str).to include "custom-class"
      expect(class_str).to include "from-pink"
      expect(class_str).not_to include "uppercase"
      expect(class_str).not_to include "shadow"
    end
  end

  describe "privilege helpers" do
    let(:user) { create :user }

    before do
      Current.user = user
    end

    it "can? no" do
      expect(user_can?(Privilege::PRIVILEGES.first)).to be false
    end

    it "can? yes" do
      Current.user.current_organization_user.add_privilege Privilege::PRIVILEGES.first
      expect(user_can?(Privilege::PRIVILEGES.first)).to be true
    end
  end

  describe "#percentage_s" do
    it "also works with countable objects" do
      expect(percentage_s([9, 9, 9], 5)).to eq "60%"
      expect(percentage_s([700, 20, 7.7, 33, 0, 0, 0, 0], 5)).to eq "160%"
      expect(percentage_s([700, 20, 7.7, 33, 0, 0, 0, 0], [1, 2, 3])).to eq "266.7%"
    end

    it "calculates properly" do
      expect(percentage_s(1, 1)).to eq "100%"
      expect(percentage_s(5, 5)).to eq "100%"
      expect(percentage_s(30, 30)).to eq "100%"
      expect(percentage_s(200, 200)).to eq "100%"

      expect(percentage_s(2, 1)).to eq "200%"
      expect(percentage_s(15, 5)).to eq "300%"
      expect(percentage_s(45, 30)).to eq "150%"
      expect(percentage_s(450, 200)).to eq "225%"

      expect(percentage_s(0, 5)).to eq "0%"
      expect(percentage_s(1, 5)).to eq "20%"
      expect(percentage_s(2, 5)).to eq "40%"
      expect(percentage_s(3, 5)).to eq "60%"
      expect(percentage_s(4, 5)).to eq "80%"
      expect(percentage_s(5, 5)).to eq "100%"
    end

    it "can round differently" do
      expect(percentage_s(1, 3)).to eq "33.3%"
      expect(percentage_s(1, 3, precision: 0)).to eq "33%"
      expect(percentage_s(1, 3, precision: 1)).to eq "33.3%"
      expect(percentage_s(1, 3, precision: 2)).to eq "33.33%"
      expect(percentage_s(1, 3, precision: 3)).to eq "33.333%"
    end

    it "handles division by 0" do
      expect(percentage_s(70, 0)).to eq "-%"
    end

    it "handles nan values" do
      expect(percentage_s(0, 0)).to eq "0%"
    end
  end
end
