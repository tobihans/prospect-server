# frozen_string_literal: true

require "rails_helper"

describe Current do
  let(:user) { create :user, admin: true }

  before do
    described_class.user = user
  end

  it "url login token" do
    expect(described_class.url_login_token).not_to be_nil
    expect do
      JWT.decode(described_class.url_login_token, Rails.application.config.jwk.keypair, true, { algorithm: "ES256" })
    end.not_to raise_exception
  end

  it "user organization" do
    expect(described_class.organization).to eq user.organization
  end
end
