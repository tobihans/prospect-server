# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id              :bigint           not null, primary key
#  description     :string
#  name            :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer          not null
#  user_id         :integer
#
# Indexes
#
#  index_projects_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_projects_on_organization_id           (organization_id)
#  index_projects_on_user_id                   (user_id)
#

require "rails_helper"

describe Project do
  let(:project) { create :project }

  it "#to_s is name" do
    expect(project.to_s).to eq project.name
  end

  it "cleans up name and description" do
    project.update name: "   \n hello ", description: " what 's UP   "
    expect(project.name).to eq "hello"
    expect(project.description).to eq "what 's UP"
  end

  it "has a default visibility" do
    expect(project.default_visibility).to be_present
  end

  it "default visibility can only be created once" do
    expect { project.create_default_visibility }.to raise_error ActiveRecord::RecordInvalid
  end

  describe "visibility aspect" do
    let(:organization) { project.organization }
    let(:another_organization) { create :organization }
    let!(:visible_to_another) { create :visibility, project:, organization: another_organization } # rubocop:disable RSpec/LetSetup

    it "#visible_to_other_organizations shows non self orgs who can see something" do
      expect(project.visible_to_other_organizations).to eq [another_organization]
    end

    it "#visible_for_organizations shows all who can see it" do
      expect(project.visible_for_organizations).to contain_exactly(organization, another_organization)
    end

    it "#visibilities shows all visibilities" do
      expect(project.visibilities.pluck(:organization_id)).to contain_exactly(organization.id, another_organization.id)
    end

    it "#visibilites_for_others shows non default visibilities" do
      expect(project.visibilities_for_others.pluck(:organization_id)).to eq [another_organization.id]
    end

    it "scope visible_for_organization shows all projects visible to them" do
      expect(described_class.visible_for_organization(organization)).to eq [project]
      expect(described_class.visible_for_organization(another_organization)).to eq [project]
    end
  end
end
