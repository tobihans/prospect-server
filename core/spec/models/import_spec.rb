# frozen_string_literal: true

# == Schema Information
#
# Table name: imports
#
#  id                     :bigint           not null, primary key
#  error                  :string
#  ingestion_finished_at  :datetime
#  ingestion_started_at   :datetime
#  last_imported_hint     :string
#  processing_finished_at :datetime
#  processing_started_at  :datetime
#  protocol               :text
#  rows_duplicated        :integer
#  rows_failed            :integer
#  rows_inserted          :integer
#  rows_updated           :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  source_id              :bigint
#
# Indexes
#
#  index_imports_on_source_id  (source_id)
#

require "rails_helper"

describe Import do
  let(:import) { create :import }

  it "can set and persist the error" do
    import.failed! "Error Message"
    import.reload
    expect(import.error).to eq "Error Message"
    expect(import).to be_failed
  end

  it "can have no data" do
    import.failed! "no_data"
    import.reload
    expect(import).to be_no_data
    expect(import).not_to be_failed # no data is no error
  end

  describe "states" do
    let(:stages) { %w[ingestion_started ingestion_finished processing_started processing_finished] }
    let(:failed_stages) { stages.map { |s| "#{s}_failed" } }

    it "works with initial pending state - nothing overlaps" do
      expect(import).to be_pending
      expect(import.state).to eq "pending"
      expect(described_class.pending).to include import

      stages.each do |other_stage|
        expect(import.send("#{other_stage}?")).to be_falsey
        expect(described_class.send(other_stage)).not_to include import
      end

      failed_stages.each do |other_stage|
        expect(import.send("#{other_stage}?")).to be_falsey
        expect(described_class.send(other_stage)).not_to include import
      end

      import.failed! "error message"

      expect(import).to be_pending_failed
      expect(import.state).to eq "pending_failed"
      expect(described_class.pending_failed).to include import

      stages.each do |other_stage|
        expect(import.send("#{other_stage}?")).to be_falsey
        expect(described_class.send(other_stage)).not_to include import
      end

      failed_stages.each do |other_stage|
        expect(import.send("#{other_stage}?")).to be_falsey
        expect(described_class.send(other_stage)).not_to include import
      end
    end

    it "also works when setting timestamps - nothing overlaps" do
      stages.each do |stage|
        stage_failed = "#{stage}_failed"

        import.send "#{stage}!"
        expect(import.state).to eq stage
        expect(import.send("#{stage}_at")).to be_present

        expect(import.send("#{stage}?")).to be_truthy
        expect(described_class.send(stage)).to include import

        stages.without(stage).each do |other_stage|
          expect(import.send("#{other_stage}?")).to be_falsey
          expect(described_class.send(other_stage)).not_to include import
        end

        failed_stages.each do |other_stage|
          expect(import.send("#{other_stage}?")).to be_falsey
          expect(described_class.send(other_stage)).not_to include import
        end

        import.failed! "bad things happen!"

        expect(import.state).to eq stage_failed
        expect(import.send("#{stage}_at")).to be_present

        expect(import.send("#{stage_failed}?")).to be_truthy
        expect(described_class.send(stage_failed)).to include import

        stages.each do |other_stage|
          expect(import.send("#{other_stage}?")).to be_falsey
          expect(described_class.send(other_stage)).not_to include import
        end

        failed_stages.without(stage_failed).each do |other_stage|
          expect(import.send("#{other_stage}?")).to be_falsey
          expect(described_class.send(other_stage)).not_to include import
        end

        # reset for next iteration
        import.update! error: nil
      end
    end

    describe "#state_symbol" do
      it "failed" do
        import.error = "some error"
        expect(import.state_symbol).to eq "⨯"
      end

      it "is finished" do
        allow(import).to receive(:processing_finished?).and_return(true)
        expect(import.state_symbol).to eq "✔"
      end

      it "is neither failed or finished" do
        expect(import.state_symbol).to eq "…"
      end
    end

    describe "#time_s" do
      it "just shows creation time when not finished" do
        expect(import.time_s).to eq "#{import.created_at} ..."
      end

      it "shows finished time too when finished" do
        import.processing_finished_at = Time.zone.now
        expect(import.time_s).to eq "#{import.created_at} ... #{import.processing_finished_at}"
      end
    end
  end

  it "properly calculates state progress" do
    expect(import.state_int).to eq 0
    expect(import.state_percent).to eq 0
    expect(import.state_int_of).to eq "0/4"

    import.ingestion_started_at = Time.zone.now

    expect(import.state_int).to eq 1
    expect(import.state_percent).to eq 25
    expect(import.state_int_of).to eq "1/4"

    import.ingestion_finished_at = Time.zone.now

    expect(import.state_int).to eq 2
    expect(import.state_percent).to eq 50
    expect(import.state_int_of).to eq "2/4"

    import.processing_started_at = Time.zone.now

    expect(import.state_int).to eq 3
    expect(import.state_percent).to eq 75
    expect(import.state_int_of).to eq "3/4"

    import.processing_finished_at = Time.zone.now

    expect(import.state_int).to eq 4
    expect(import.state_percent).to eq 100
    expect(import.state_int_of).to eq "4/4"
  end

  context "with rbf" do
    it "will not trigger post processing if there is no rbf" do
      source = create :source
      import = create(:import, source:)
      expect(ImportPostProcessingWorker).not_to receive :enqueue_for_import
      import.notify_processing(force: true)
    end

    it "will trigger post processing worker for rbf source" do
      source = create :source, :rbf_ueccc_ogs
      import = create(:import, source:)
      expect(ImportPostProcessingWorker).to receive :enqueue_for_import
      import.notify_processing(force: true)
    end
  end
end
