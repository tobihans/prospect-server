# frozen_string_literal: true

# == Schema Information
#
# Table name: encrypted_blobs
#
#  id         :bigint           not null, primary key
#  error      :text
#  url_e      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  import_id  :bigint           not null
#
# Indexes
#
#  index_encrypted_blobs_on_import_id  (import_id)
#
require "rails_helper"
require "faktory/testing"

describe EncryptedBlob do
  let(:import) { create :import }
  let(:blob) { described_class.create! import: }
  let(:empty_png) { [137, 80, 78, 71, 13, 10, 26, 10].pack("C*") }

  it "stores a blob plainly" do
    blob.attach_blob! empty_png

    expect(blob.blob).to be_attached
    expect(blob.blob.download).not_to be_nil
    expect(blob.error).to be_nil
    expect(blob.blob.download).to eq empty_png
    expect(blob.key).to match(%r{encrypted_blobs/\d*/\d*/\d*})
  end

  it "encrypts and stores a image" do
    blob.encrypt_attach_blob! empty_png, content_type: "image/png"

    expect(blob.blob).to be_attached
    expect(blob.blob.download).not_to be_nil
    expect(blob.error).to be_nil
    expect(blob.blob.download).to be_start_with "#{blob.organization.id}:"
  end

  it "coverts to data url when encrypting and storing an image" do
    expect(RsaOaepEncrypt).to receive(:encrypt).with("data:image/png;base64,iVBORw0KGgo=", blob.organization)
    blob.encrypt_attach_blob! empty_png, content_type: "image/png"
  end

  it "can convert an image to a data uri" do
    expect(blob.image_to_data_uri(empty_png, "image/png")).to eq "data:image/png;base64,iVBORw0KGgo="
  end

  it "attach blob encrypts data" do
    code = described_class.store_data! empty_png, import.organization, import

    expect(code).not_to be_nil
    expect(code).to match(%r{http://.*/encrypted_blob/\d})
    new_blob = described_class.find(code.split("/").last)
    expect(new_blob).not_to be_nil
    expect(new_blob.blob).to be_attached
    expect(new_blob.blob.download).not_to eq empty_png
  end

  it "schedules worker if the blob is an url" do
    expect { described_class.store_data! "http://example.com/text.png", import.organization, import }.
      to change { PullBlobWorker.jobs.size }.by 1
  end
end
