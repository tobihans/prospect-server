# frozen_string_literal: true

# == Schema Information
#
# Table name: visibilities
#
#  id              :bigint           not null, primary key
#  columns         :jsonb            not null
#  conditions      :jsonb            not null
#  data_from       :datetime
#  data_until      :datetime
#  license         :string
#  resharing       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer          not null
#  project_id      :bigint           not null
#  user_id         :integer
#
# Indexes
#
#  index_visibilities_on_organization_id                 (organization_id)
#  index_visibilities_on_project_id_and_organization_id  (project_id,organization_id) UNIQUE
#  index_visibilities_on_user_id                         (user_id)
#

require "rails_helper"

describe Visibility do
  let!(:import) { create :import }
  let!(:source) { import.source }
  let(:project) { import.project }
  let(:organization) { import.organization } # owning
  let(:another_organization) { create :organization } # uninvited
  let(:organization3) { create :organization } # has visibility below

  before do
    q = <<-SQL.squish
        INSERT INTO data_test
        (uid, int_column, float_column, personal_column, created_at, updated_at, source_id, custom, data_origin, import_id, organization_id)
        VALUES
        ('some_uid_1', 7, 8.9, 'personal name', NOW(), NOW(), #{import.source.id}, '{"some": "value"}', 'api', #{import.id}, #{import.organization.id})
    SQL
    Postgres::RemoteControl.query! q
  end

  it "project has default visibility" do
    expect(project.default_visibility).to be_present
  end

  it "default visibility can see all data" do
    expect(project.default_visibility).to be_all_data
  end

  it "by default gives full access to source organization" do
    r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_data_view(organization, :test)}"
    expect(r.ntuples).to eq 1
    data = r.to_a.first.with_indifferent_access

    expect(data[:source_id]).to eq source.id
    expect(data[:import_id]).to eq import.id
    expect(data[:source_project_id]).to eq project.id
    expect(data[:source_name]).to eq source.name
    expect(data[:source_organization_name]).to eq organization.name
    expect(data[:source_project_name]).to eq project.name
    expect(data[:created_at]).to be_present
    expect(data[:updated_at]).to be_present
    expect(data[:organization_id]).to eq organization.id

    expect(data[:last_import_id]).to be_nil
    expect(data[:last_source_id]).to be_nil

    expect(data[:data_origin]).to eq "api"
    expect(data[:uid]).to eq "some_uid_1"
    expect(data[:int_column]).to eq 7
    expect(data[:float_column]).to eq 8.9
    expect(data[:personal_column]).to eq "personal name"
    expect(JSON.parse(data[:custom])).to eq({ "some" => "value" })
    expect(data[:bool_column]).to be_nil
    expect(data[:time_column]).to be_nil
    expect(data[:name_column_p]).to be_nil
    expect(data[:name_column_e]).to be_nil
    expect(data[:something_uid]).to be_nil
    expect(data[:enum_column]).to be_nil
  end

  it "also gives access if source is inactive" do
    source.active = false
    source.save!

    described_class.full_views_refresh!

    r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_data_view(organization, :test)}"
    expect(r.ntuples).to eq 1
    expect(r.to_a.first["source_id"]).to eq source.id
  end

  describe "sanitizes before save" do
    it "time-range" do
      t = Time.zone.now
      v = described_class.create! project:, organization: another_organization,
                                  data_from: t, data_until: t
      expect(v.data_from).to eq Time.zone.today.beginning_of_day
      expect(v.data_until).to be_within(1.second).of(Time.zone.today.end_of_day)
      # Otherwise we get:
      #   expected: 2023-12-20 23:59:59.999999999 +0000
      #   got: 2023-12-20 23:59:59.999999000 +0000
    end

    it "columns" do
      v = described_class.create! project:, organization: another_organization,
                                  columns: { tbl_arr: %w[uid customer_name],
                                             tbl_form_hash: { col: "1", no_way: "0", ok: 1 },
                                             not_me: [], also_me_not: {}, forget_it: nil }
      expect(v.columns).to eq({ "tbl_arr" => %w[uid customer_name], "tbl_form_hash" => %w[col ok] })
    end

    it "fails for columns with bad data" do
      expect do
        described_class.create! project:, organization: another_organization,
                                columns: { data_meters: 5 }
      end.to raise_error("Can not sanitize column data of type Integer")
    end
  end

  it "can create share everything visibility" do
    v = described_class.new project:, organization: another_organization
    v.share_everything
    expect(v).to be_share_everything
    v.save!
    expect(v).to be_share_everything

    r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_data_view(another_organization, :test)}"
    expect(r.ntuples).to eq 1
    data = r.to_a.first.with_indifferent_access

    expect(data[:data_origin]).to eq "api"
    expect(data[:uid]).to eq "some_uid_1"
    expect(data[:int_column]).to eq 7
    expect(data[:float_column]).to eq 8.9
    expect(data[:personal_column]).to eq "personal name"
    expect(JSON.parse(data[:custom])).to eq({ "some" => "value" })
  end

  it "can create restricted visibility" do
    described_class.create! project:, organization: another_organization,
                            columns: { "data_test" => %w[uid int_column] }

    r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_data_view(another_organization, :test)}"
    expect(r.ntuples).to eq 1
    data = r.to_a.first.with_indifferent_access

    expect(data[:source_id]).to eq source.id
    expect(data[:import_id]).to eq import.id
    expect(data[:source_project_id]).to eq project.id
    expect(data[:source_name]).to eq source.name
    expect(data[:source_organization_name]).to eq organization.name
    expect(data[:source_project_name]).to eq project.name
    expect(data[:created_at]).to be_present
    expect(data[:updated_at]).to be_present
    expect(data[:organization_id]).to eq organization.id

    expect(data[:last_import_id]).to be_nil
    expect(data[:last_source_id]).to be_nil

    expect(data[:data_origin]).to eq "api"

    expect(data[:uid]).to eq "some_uid_1"
    expect(data[:int_column]).to eq 7
    expect(data[:float_column]).to be_nil
    expect(data[:personal_column]).to be_nil
    expect(data[:custom]).to be_nil
    expect(data[:bool_column]).to be_nil
    expect(data[:time_column]).to be_nil
    expect(data[:name_column_p]).to be_nil
    expect(data[:name_column_e]).to be_nil
    expect(data[:something_uid]).to be_nil
    expect(data[:enum_column]).to be_nil
  end

  it "filters data by created_at" do
    row_count = lambda {
      Postgres::RemoteControl.query!(
        "SELECT * FROM #{Naming.db_organization_data_view(another_organization, :test)}"
      ).ntuples
    }

    vis = described_class.create! project:, organization: another_organization,
                                  columns: { "data_test" => %w[uid int_column] }
    expect(row_count.call).to eq 1

    vis.update! data_from: 1.week.ago, data_until: 2.weeks.after
    expect(row_count.call).to eq 1

    vis.update! data_from: nil, data_until: 2.weeks.after
    expect(row_count.call).to eq 1

    vis.update! data_from: 1.week.ago, data_until: nil
    expect(row_count.call).to eq 1

    vis.update! data_from: 1.week.after, data_until: 2.weeks.after
    expect(row_count.call).to eq 0

    vis.update! data_from: 2.weeks.ago, data_until: 1.week.ago
    expect(row_count.call).to eq 0

    vis.update! data_from: 1.week.after, data_until: nil
    expect(row_count.call).to eq 0

    vis.update! data_from: nil, data_until: 1.week.ago
    expect(row_count.call).to eq 0
  end

  describe "#to_as_columns" do
    it "is correct for default visibility" do
      vis = project.default_visibility
      expect(vis.to_s_columns).to eq "All data"
    end

    it "is correct for partial visibility" do
      vis = described_class.create! project:, organization: another_organization,
                                    columns: { "test" => %w[uid int_column] }
      expect(vis.to_s_columns).to eq "2 columns from test"
    end

    it "is correct for nothing visibility" do
      vis = described_class.create! project:, organization: another_organization
      expect(vis.to_s_columns).to eq "No columns shared"
    end
  end

  describe "scopes" do
    let(:another_project) { create :project, organization: another_organization }
    let!(:another_visibility) do
      create :visibility, project: another_project, organization:, columns: { "data_test" => %w[uid int_column] }
    end

    it "for_organization shows all visible" do
      expect(described_class.for_organization(organization).to_a).
        to contain_exactly(project.default_visibility, another_visibility)
    end

    it "shared_to_organization only shows shared from other organizations" do
      expect(described_class.shared_to_organization(organization)).
        to eq [another_visibility]
    end
  end

  describe "change tracking" do
    it "only has views for one default visibility" do
      expected_props = {
        organization_id: organization.id,
        project_id: project.id,
        source_id: source.id
      }
      expect(Postgres::RemoteControl.all_visibility_views(parse: true)).
        to all include expected_props
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count
    end

    it "has only views for organization after adding another source" do
      create(:source, project:)
      expected_props = {
        organization_id: organization.id,
        project_id: project.id
      }
      expect(Postgres::RemoteControl.all_visibility_views(parse: true)).
        to all include expected_props
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count * 2
    end

    it "updates after a source has been deleted" do
      new_source = create(:source, project:)
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq(Postgres::DataTable.count * 2)
      new_source.destroy!
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count
    end

    it "reflects when a visibility is added to another organization" do
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count

      create :visibility, :test_column, project:, organization: another_organization

      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count + 1
      expect(Postgres::RemoteControl.all_visibility_views(parse: true).pluck(:organization_id).uniq).
        to contain_exactly(organization.id, another_organization.id)
    end

    it "does not care if visibility without columns is added" do
      expect do
        create(:visibility, project:, organization: another_organization, columns: {})
      end.not_to(change { Postgres::RemoteControl.all_visibility_views.count })
    end

    it "reflects multiple sources and multiple visibilities" do
      create :visibility, :test_column, project:, organization: another_organization

      initial_vis_count = Postgres::DataTable.count + 1
      expect(Postgres::RemoteControl.all_visibility_views.count).to eq(initial_vis_count)

      create(:source, project:)

      expect(Postgres::RemoteControl.all_visibility_views.count).to eq(initial_vis_count * 2)
    end

    it "reflects when source is deleted" do
      create :visibility, :test_column, project:, organization: another_organization
      initial_vis_count = Postgres::DataTable.count + 1
      expect(Postgres::RemoteControl.all_visibility_views.count).to eq(initial_vis_count)
      new_source = create(:source, project:)
      expect(Postgres::RemoteControl.all_visibility_views.count).to eq(initial_vis_count * 2)

      new_source.destroy!
      expect(Postgres::RemoteControl.all_visibility_views.count).to eq(initial_vis_count)
    end

    it "reflects when a visibility is deleted" do
      new_visibility = create :visibility, :test_column, project:, organization: another_organization
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count + 1
      new_visibility.destroy!
      expect(Postgres::RemoteControl.all_visibility_views.count).
        to eq Postgres::DataTable.count
      expect(Postgres::RemoteControl.all_visibility_views(parse: true).pluck(:organization_id).uniq).
        to eq [organization.id]
    end
  end

  it "fresh organization without visibilities can access the data views" do
    org = create :organization
    expect(org.visibilities).to be_empty

    Postgres::DataTable.all.map(&:data_category).each do |dc|
      data_view = Naming.db_organization_data_view(org, dc)
      expect do
        Postgres::RemoteControl.query!("SELECT * FROM #{data_view}").to_a
      end.not_to raise_error
    end
  end

  describe "shared view" do
    before { create(:visibility, project:, organization: organization3) }

    it "provides data to owning organization" do
      r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_shared_visibilities_view(organization, prefix: false)}"
      expect(r.ntuples).to eq 1

      data = r.to_a.first.with_indifferent_access
      expect(data[:source_id]).to eq source.id
      expect(data[:project_id]).to eq project.id
      expect(data[:org_id]).to eq organization.id
    end

    it "provides data to organization with project visibility" do
      r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_shared_visibilities_view(organization3, prefix: false)}"
      expect(r.ntuples).to eq 1

      data = r.to_a.first.with_indifferent_access
      expect(data[:source_id]).to eq source.id
      expect(data[:project_id]).to eq project.id
      expect(data[:org_id]).to eq organization.id
    end

    it "does not share data with another organization with no visibility for the project" do
      r = Postgres::RemoteControl.query! "SELECT * FROM #{Naming.db_organization_shared_visibilities_view(another_organization, prefix: false)}"
      expect(r.ntuples).to eq 0
    end
  end

  describe "custom sources" do
    let!(:custom_source) { create :source, :custom, project: }
    let!(:custom_import) { create :import, source: custom_source }

    before do
      raw_db_insert data_custom: [
        {
          uid: "some_uid_1",
          external_id: "schnippi_schnappi_5000",
          custom: { txt_col: "hallo text",
                    num_col: 1234,
                    ts_col: "2022-02-22 02:22:22",
                    invisible_col: "mi-au" }.to_json,
          data_origin: "api",
          created_at: { raw: "NOW()" },
          updated_at: { raw: "NOW()" },
          import_id: custom_import.id,
          source_id: custom_source.id,
          organization_id: custom_source.organization.id
        }
      ]
    end

    it "gives all data through default visibility" do
      v = Naming.db_organization_custom_view organization, custom_source.custom_view
      r = Postgres::RemoteControl.query!("SELECT * FROM #{v}").to_a
      expect(r.count).to eq 1
      d = r.first
      expect(d["source_name"]).to eq custom_source.name
      expect(d["source_id"]).to eq custom_source.id
      expect(d["source_organization_name"]).to eq custom_source.organization.name
      expect(d["organization_id"]).to eq custom_source.organization.id
      expect(d["source_project_name"]).to eq project.name
      expect(d["source_project_id"]).to eq project.id
      expect(d["import_id"]).to eq custom_import.id

      expect(d["created_at"]).to be_present
      expect(d["updated_at"]).to be_present

      expect(d["custom"]).to be_present
      expect(d["data_origin"]).to eq "api"
      expect(d["last_import_id"]).to be_nil
      expect(d["last_source_id"]).to be_nil

      expect(d["uid"]).to eq "some_uid_1"
      expect(d["external_id"]).to eq "schnippi_schnappi_5000"

      expect(d["txt_col"]).to eq "hallo text"
      expect(d["ts_col"]).to eq Time.zone.parse("2022-02-22 02:22:22 UTC")
      expect(d["num_col"]).to eq 1234
    end

    it "also gives access if source is inactive" do
      custom_source.active = false
      custom_source.save!

      described_class.full_views_refresh!

      v = Naming.db_organization_custom_view organization, custom_source.custom_view
      r = Postgres::RemoteControl.query!("SELECT * FROM #{v}").to_a

      expect(r.count).to eq 1
      expect(r.to_a.first["source_id"]).to eq custom_source.id
    end

    it "does not create even view if nothing is shared" do
      create :visibility, project:, organization: another_organization
      v = Naming.db_organization_custom_view another_organization, custom_source.custom_view
      expect { Postgres::RemoteControl.query!("SELECT * FROM #{v}") }.to raise_error(/PG::UndefinedTable/)
    end

    it "gives selected access to another organization" do
      shared_cols = %w[external_id num_col]
      create :visibility, project:, organization: another_organization,
                          columns: { custom_source.custom_view => shared_cols }
      v = Naming.db_organization_custom_view another_organization, custom_source.custom_view
      r = Postgres::RemoteControl.query!("SELECT * FROM #{v}").to_a

      expect(r.count).to eq 1
      d = r.first

      expect(d["uid"]).to be_nil
      expect(d["external_id"]).to eq "schnippi_schnappi_5000"

      expect(d["txt_col"]).to be_nil
      expect(d["ts_col"]).to be_nil
      expect(d["num_col"]).to eq 1234

      expect(d["source_name"]).to eq custom_source.name
      expect(d["source_id"]).to eq custom_source.id
      expect(d["source_organization_name"]).to eq custom_source.organization.name
      expect(d["organization_id"]).to eq custom_source.organization.id
      expect(d["source_project_name"]).to eq project.name
      expect(d["source_project_id"]).to eq project.id
      expect(d["import_id"]).to eq custom_import.id

      expect(d["created_at"]).to be_present
      expect(d["updated_at"]).to be_present

      expect(d["custom"]).to be_nil
      expect(d["data_origin"]).to eq "api"
      expect(d["last_import_id"]).to be_nil
      expect(d["last_source_id"]).to be_nil
    end
  end
end

# rubocop:enable RSpec/MultipleMemoizedHelpers
