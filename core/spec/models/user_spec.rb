# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                                  :bigint           not null, primary key
#  access_count_to_reset_password_page :integer          default(0)
#  active_at                           :datetime
#  admin                               :boolean          default(FALSE), not null
#  archived_at                         :datetime
#  code_of_conduct                     :json
#  crypted_password                    :string
#  data_protection_policy              :json
#  email                               :string           not null
#  invite_code                         :string
#  reset_password_email_sent_at        :datetime
#  reset_password_token                :string
#  reset_password_token_expires_at     :datetime
#  salt                                :string
#  terms_of_use                        :json
#  username                            :string
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  organization_id                     :integer
#
# Indexes
#
#  index_users_on_active_at             (active_at)
#  index_users_on_archived_at           (archived_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_invite_code           (invite_code) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token)
#  index_users_on_username              (username)
#

require "rails_helper"

describe User do
  let(:user) { create :user }
  let(:organization) { create :organization }

  it "user jwt token" do
    token = user.jwt_json
    expect(token).not_to be_nil
    expect(token).to have_key :sub
    expect(token).to have_key :username
    expect(token[:username]).to eq Naming.grafana_user(user)
  end

  it "#to_s returns username if present, otherwise email" do
    expect(user.to_s).to match(/Jonny \d+/)
    user.username = nil
    expect(user.to_s).to match(/jonny-\d+@/)
  end

  it "normalizes email address" do
    u = create(:user, email: " GARBELgibS@bluBB.dE  \n  ", organization:)
    expect(u.email).to eq "garbelgibs@blubb.de"
  end

  describe "admin" do
    it "checks admin" do
      expect(user.admin?).to be false
    end
  end

  it "sets states by timestamp columns" do
    u = create :user, active_at: nil

    expect(u).to be_inactive
    expect(u).to be_invited
    expect(u).not_to be_active
    expect(u).not_to be_archived
    expect(described_class.invited.pluck(:id)).to include u.id
    expect(described_class.active.pluck(:id)).not_to include u.id
    expect(described_class.archived.pluck(:id)).not_to include u.id

    u.activate
    u.save!

    expect(u).not_to be_inactive
    expect(u).not_to be_invited
    expect(u).to be_active
    expect(u).not_to be_archived
    expect(described_class.invited.pluck(:id)).not_to include u.id
    expect(described_class.active.pluck(:id)).to include u.id
    expect(described_class.archived.pluck(:id)).not_to include u.id

    u.archive
    u.save!

    expect(u).to be_inactive
    expect(u).not_to be_invited
    expect(u).not_to be_active
    expect(u).to be_archived
    expect(described_class.invited.pluck(:id)).not_to include u.id
    expect(described_class.active.pluck(:id)).not_to include u.id
    expect(described_class.archived.pluck(:id)).to include u.id
  end

  it "can? has privilege" do
    priv = Privilege::PRIVILEGES.sample
    user = create :user, privileges: [priv]

    expect(user.can?(priv)).to be true
  end

  it "can? not" do
    priv = Privilege::PRIVILEGES.sample
    user = create :user, privileges: []

    expect(user.can?(priv)).to be false
  end

  it "can? has privilege and record same org" do
    priv = Privilege::PRIVILEGES.sample
    user = create :user, privileges: [priv]
    project = create :project, organization: user.organization

    expect(user.can?(priv, record: project)).to be true
  end

  it "can? has privilege and record different org" do
    priv = Privilege::PRIVILEGES.sample
    user = create :user, privileges: [priv]
    project = create :project, organization: (create :organization)

    expect(user.can?(priv, record: project)).to be false
  end

  it "can? not and record same org" do
    priv = Privilege::PRIVILEGES.sample
    user = create :user, privileges: []
    project = create :project, organization: user.organization

    expect(user.can?(priv, record: project)).to be false
  end

  describe "invitation" do
    it "creates invitation code for new users" do
      u = create(:user, email: "somemail@blubs.com", organization:)
      expect(u.invite_code).to be_present
    end

    it "sends email with invitation code to new users" do
      VCR.use_cassette :user_create_invite_email do
        expect do
          described_class.create!(email: "garbelgarbel@blub.de", organization:)
        end.to have_enqueued_job.with "UserMailer", "invite", any_args
      end
    end
  end

  describe "welcoming a new user" do
    it "sends a welcome mail" do
      expect { user.mail_welcome }.
        to have_enqueued_job.with "UserMailer", "welcome", any_args
    end
  end

  it "calculates grafana role properly" do
    VCR.use_cassette :grafana_update_roles do
      user.update! admin: false
      expect(user.grafana_role).to eq "Viewer"

      user.update! admin: true
      expect(user.grafana_role).to eq "Editor"

      user.update! admin: false
      Current.user = user
      user.current_organization_user.add_privilege "grafana.edit"
      expect(user.grafana_role).to eq "Editor"
    end
  end

  describe "callbacks" do
    it "updates grafana role" do
      VCR.use_cassette :grafana_user_update_callback do
        client = Grafana::RemoteControl.client
        expect(Grafana::RemoteControl).to receive(:update_user_permission).at_least(3).times.and_call_original

        organization = create :organization, id: 12_345
        VCR.use_cassette(:grafana_default_dashboards, erb: default_dashboard_infos) do
          organization.setup_grafana!
        end
        user = create :user, id: 12_345, organization:, admin: false
        user.create_grafana_user

        expect(client).to receive(:update_user_permissions).with(2, "Editor", 2)
        user.admin = true
        user.save! # first call on update_user_permission

        expect(client).to receive(:update_user_permissions).with(2, "Viewer", 2)
        user.admin = false
        user.save!

        expect(client).to receive(:update_user_permissions).with(2, "Editor", 2)
        Current.user = user
        user.current_organization_user.add_privilege "grafana.edit"

        # TODO: this generates some VCR error
        # expect(client).to receive(:update_user_permissions).with(2, "Viewer", 2)
        # user.current_organization_user.remove_privilege "grafana.edit"
      end
    end
  end

  describe "terms and conditions" do
    it "checks if user accepted them" do
      user.update! terms_of_use: {}, data_protection_policy: {}, code_of_conduct: {}

      expect(user).not_to be_all_terms_accepted

      user.update! terms_of_use: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:terms_of_use]
                   },
                   data_protection_policy: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:data_protection_policy]
                   },
                   code_of_conduct: {
                     accepted: true,
                     version: Rails.configuration.current_terms_version[:code_of_conduct]
                   }

      expect(user).to be_all_terms_accepted

      user.update! terms_of_use: {
        accepted: true,
        version: "Old Version some time ago"
      }

      expect(user).not_to be_all_terms_accepted
    end
  end
end
