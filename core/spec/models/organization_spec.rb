# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations
#
#  id                :bigint           not null, primary key
#  api_key           :string(1000)
#  crypto_added_at   :datetime
#  iv                :text
#  name              :string           not null
#  postgres_password :string(1000)
#  public_key        :text
#  salt              :text
#  wrapped_key       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  grafana_org_id    :integer
#  user_id           :integer
#
# Indexes
#
#  index_organizations_on_name     (name) UNIQUE
#  index_organizations_on_user_id  (user_id)
#

require "rails_helper"

describe Organization do
  let(:source) { create :source }
  let(:organization) { source.organization }

  it "creates valid organization" do
    expect(organization.to_s).to match "The Test Organization"
  end

  describe "/api/v1/out/odyssey", type: :request do
    let(:organization_api) { create :organization }

    it "sends the wrapped key via API" do
      get "/api/v1/out/organization/wrapped_key", headers: { Authorization: organization_api.api_key }
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body).with_indifferent_access).to eq ({ id: organization_api.id }.merge(organization_api.crypto_options)).with_indifferent_access
    end
  end

  it "auto generates an api key" do
    p = described_class.new name: "bla cake apikey 11"
    expect(p.api_key).not_to be_present
    allow_any_instance_of(Grafana::Client).to receive(:all_orgs).and_return([{ name: "prospect_organization", id: 1 }])
    allow(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).and_return(true)
    allow(Naming).to receive(:grafana_organization_org).and_return("prospect_organization")
    allow_any_instance_of(described_class).to receive(:setup_grafana!).and_return(true)
    p.save!
    expect(p.reload.api_key).to be_present
  end

  it "has many users through OrganizationUser" do
    create(:user, organization:)
    expect(organization.users.count).to eq 1
    expect { organization.users << create(:user) }.to change(OrganizationUser, :count).by(1)
  end

  it "removes all organization users when destroyed" do
    create(:user, organization:)
    expect { organization.destroy! }.to change(OrganizationUser, :count).by(-1)
  end

  describe "postgres stuff is auto created" do
    it "gets a password" do
      p = described_class.new name: "bla cake postgres pw 22"
      expect(p.postgres_password).not_to be_present
      allow(Grafana::RemoteControl).to receive(:default_dashboards_for_organization!).and_return(true)
      allow_any_instance_of(described_class).to receive(:setup_grafana!).and_return(true)
      p.save!
      expect(p.reload.postgres_password).to be_present
    end

    it "gets a user" do
      q = "SELECT 1 FROM pg_roles WHERE rolname='#{Naming.db_organization_user(organization)}'"
      expect(Postgres::RemoteControl.query!(q).ntuples).to eq 1
    end

    it "gets a schema" do
      q = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = '#{Naming.db_organization_schema(organization)}'"
      expect(Postgres::RemoteControl.query!(q).ntuples).to eq 1
    end

    it "gets the test data view" do
      q = <<-SQL.squish
        SELECT table_name FROM information_schema.tables
        WHERE  table_schema = '#{Naming.db_organization_schema(organization)}'
        AND    table_name   = 'data_test'
      SQL
      expect(Postgres::RemoteControl.query!(q).ntuples).to eq 1
    end
  end

  describe "saves a creator" do
    before { Current.user = create :user }

    let(:users) { create_list :user, 2 }
    let(:organization) { create :organization, users: }

    it "saves the creator" do
      expect(organization.users).to eq users
      expect(organization.creator).to eq Current.user
    end
  end

  describe "custom views" do
    let!(:source) { create :source, :custom }
    let!(:organization) { source.organization }
    let!(:custom_view_name) { source.custom_view }

    it "#custom_views lists all custom views of organization" do
      expect(organization.custom_views).to eq [custom_view_name]
    end

    it "#custom_views_counts lists counts of data in all custom views" do
      expect(organization.custom_views_counts).to eq({ custom_view_name => 0 })
    end

    it "#count_for_custom_view counts data in a certain custom view" do
      expect(organization.count_for_custom_view(custom_view_name)).to eq 0
    end
  end

  describe "callbacks" do
    it "creates grafana organization and datasource" do
      expect_any_instance_of(described_class).to receive(:setup_grafana!).once
      organization
    end
  end

  it "main_organization?" do
    ENV["MAIN_ORG_ID"] = organization.id.to_s
    expect(organization).to be_main_organization

    ENV["MAIN_ORG_ID"] = (organization.id + 1).to_s
    expect(organization).not_to be_main_organization
  end

  describe "crypto" do
    it "factory organization has proper encryption keys" do
      expect(organization).to be_encryption_keys
      expect(organization).not_to be_encryption_keys_blank

      expect(organization.crypto_added_at).to be_present
      expect(organization.public_key).to be_present
      expect(organization.wrapped_key).to be_present
      expect(organization.iv).to be_present
      expect(organization.salt).to be_present
    end

    it "removes encryption keys and gives right state" do
      organization.remove_encryption_keys!

      expect(organization).not_to be_encryption_keys
      expect(organization).to be_encryption_keys_blank

      expect(organization.crypto_added_at).to be_nil
      expect(organization.public_key).to be_nil
      expect(organization.wrapped_key).to be_nil
      expect(organization.iv).to be_nil
      expect(organization.salt).to be_nil
    end

    it "removes timestamp if keys are removed individually" do
      organization.update! public_key: nil,
                           wrapped_key: nil,
                           iv: nil,
                           salt: nil

      expect(organization.crypto_added_at).to be_nil
      expect(organization.public_key).to be_nil
      expect(organization.wrapped_key).to be_nil
      expect(organization.iv).to be_nil
      expect(organization.salt).to be_nil
    end

    it "does not change timestamp if keys stay same" do
      t = 1.day.ago
      organization.update_columns crypto_added_at: t # rubocop:disable Rails/SkipsModelValidations

      organization.save!

      expect(organization).to be_encryption_keys
      expect(organization.crypto_added_at).to eq t
    end

    it "cleans up keys with just whitespace" do
      organization.update! public_key: " ",
                           wrapped_key: "",
                           iv: "\t",
                           salt: nil

      expect(organization).to be_encryption_keys_blank
      expect(organization.crypto_added_at).to be_nil
    end

    it "adds timestamp if keys are added" do
      old_add_at = organization.crypto_added_at
      organization.remove_encryption_keys!

      organization.update! public_key: "abc",
                           wrapped_key: "abc",
                           iv: "abc",
                           salt: "abc"

      expect(organization.crypto_added_at).to be_present
      expect(organization.crypto_added_at).not_to eq old_add_at
    end

    it "complains if keys are set partially" do
      expect do
        organization.update! public_key: "abc",
                             wrapped_key: "abc",
                             iv: "abc",
                             salt: nil
      end.to raise_error(/Crypto keys complete or not set must be accepted/)
    end
  end
end
