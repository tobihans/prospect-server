# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations_users
#
#  id              :bigint           not null, primary key
#  activated_at    :datetime
#  organization_id :bigint           not null
#  user_id         :bigint           not null
#
# Indexes
#
#  index_organizations_users_on_organization_id              (organization_id)
#  index_organizations_users_on_user_id                      (user_id)
#  index_organizations_users_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
require "rails_helper"

describe OrganizationUser do
  let(:user) { create :user }
  let(:ou) { user.current_organization_user }
  let(:priv) { Privilege::PRIVILEGES.first }

  let(:current_user) { create :user, :admin }

  before do
    Current.user = current_user

    allow(Grafana::RemoteControl).to receive_messages(update_user_permission: {})
  end

  it "add_privilege" do
    ou.add_privilege priv

    expect(ou.privilege?(priv)).to be true
  end

  it "remove_privilege" do
    ou.add_privilege priv
    ou.remove_privilege priv

    expect(ou.privilege?(priv)).to be false
  end

  it "add_manager_privileges" do
    ou.apply_privilege_template("manager")

    expect(ou.privileges.map(&:name)).to match_array(Privilege::PRIVILEGES)
  end

  it "add_user_privileges" do
    ou.apply_privilege_template("user")

    expect(ou.privileges.map(&:name)).to match_array(Privilege::USER_PRIVILEGES)
  end

  it "add_viewer_privileges" do
    ou.apply_privilege_template("viewer")

    expect(ou.privileges.map(&:name)).to match_array(Privilege::VIEWER_PRIVILEGES)
  end

  [
    [[], "no priv. template"],
    [["data.download"], "no priv. template+"],
    [Privilege::PRIVILEGES, "manager"],
    [Privilege::USER_PRIVILEGES, "user"],
    [Privilege::USER_PRIVILEGES + ["user.manage"], "user+"]
  ].each do |privs, result|
    it "privilege_summary" do
      ou.privilege_names = privs

      expect(ou.privilege_summary).to eq result
    end

    it "privilege_summary admin" do
      ou.user.admin = true

      expect(ou.privilege_summary).to eq "admin"
    end
  end
end
