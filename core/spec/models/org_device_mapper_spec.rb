# frozen_string_literal: true

# == Schema Information
#
# Table name: org_device_mappers
#
#  id              :bigint           not null, primary key
#  device_type     :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  device_id       :bigint           not null
#  organization_id :bigint           not null
#
# Indexes
#
#  index_org_device_mappers_on_device_type_and_device_id        (device_type,device_id) UNIQUE
#  index_org_device_mappers_on_device_type_and_organization_id  (device_type,organization_id)
#  index_org_device_mappers_on_organization_id                  (organization_id)
#

require "rails_helper"

describe OrgDeviceMapper do
  let(:org_device_mapper) { create :org_device_mapper }

  it "#to_csv" do
    org_device_mapper

    csv = described_class.to_csv
    expect(csv).to include "device_id,device_type,organization_name"
    expect(csv).to include "100,hop_meter,The Test Organization "
  end

  describe "#import_csv!" do
    let(:example_csv) { Rails.root.join("spec/fixtures/org_device_mappers_example.csv").read }

    before do
      create :organization, name: "Org 1"
      create :organization, name: "Org 2"
      create :org_device_mapper, device_id: "111",
                                 device_type: "aam_v3",
                                 organization: Organization.find_by!(name: "Org 2")
    end

    it "creates a device mapper" do
      expect do
        described_class.import_csv! example_csv
      end.to change(described_class, :count).by 1
    end

    it "creates device mapper with correct data" do
      described_class.import_csv! example_csv
      odm = described_class.find_by device_id: "222"

      expect(odm.device_type).to eq "hop_meter"
      expect(odm.organization_name).to eq "Org 2"
    end

    it "can update an exiting device" do
      odm = described_class.find_by(device_id: "111")
      expect(odm.organization_name).to eq "Org 2"
      expect(odm.device_type).to eq "aam_v3"

      described_class.import_csv! example_csv

      odm.reload
      expect(odm.organization_name).to eq "Org 1"
      expect(odm.device_type).to eq "aam_v3"
    end

    it "logs import result" do
      log = described_class.import_csv! example_csv

      expect(log).to include "Total rows: 4"
      expect(log).to include "1 aam_v3 - 111: OK - UPDATED"
      expect(log).to include "2 hop_meter - 222: OK - SAVED"
      expect(log).to include "3 bad_device - 333: ERR - Device type is not included in the list"
      expect(log).to include "4 aam_v3 - 444: ERR - Organization must exist"
    end
  end
end
