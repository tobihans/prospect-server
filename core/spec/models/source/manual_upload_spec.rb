# frozen_string_literal: true

require "rails_helper"

describe Source::ManualUpload do
  let(:source) { create :source, kind: :manual_upload, data_category: :test, details: { file_format: "csv" } }
  let(:adapter) { source.adapter }

  let(:csv) do
    <<~CSV
      int_column,bool_column,float_column,time_column,name_column,personal_column,enum_column
      5,false,5.67,2021-02-22 13:34:56 +0100,pater paul stored as fe42o3p2,Personal stuff  ,b
    CSV
  end

  let(:bad_csv) do
    <<~CSV
      int_column,bool_column,float_column,time_column,name_column,personal_column,enum_column
      5,true,3.14,2021-02-22 13:34:56 +0100,pater paul stored as fe42o3p2,Personal Text,x
    CSV
  end

  describe "#ingest" do
    describe "complaints" do
      it "logs errors properly" do
        adapter.ingest({ bla: :data }.to_json)

        expect(adapter.protocol).to include(/\.\.\. ERROR/i)
        expect(adapter.protocol).to include(">>> ERROR")
        expect(adapter.protocol).not_to include(/OK/i)
        expect(adapter.protocol).not_to include(/SUCCESS/i)
        expect(adapter).to be_failed
      end

      it "does not like wrong file format" do
        adapter.ingest({ bla: :data }.to_json)

        expect(adapter.protocol).to include "Checking data file format: csv ... ERROR"
        expect(adapter).to be_failed
      end

      it "complains about invalid data" do
        adapter.ingest bad_csv

        expect(adapter.protocol).to include "Checking schema is conforming to: test ... ERROR"
        expect(adapter.protocol).to include "  Line 1: enum_column can not be casted as enum with allowed values a, b, c"
        expect(adapter).to be_failed
      end

      it "complains about failing p14n" do
        allow(P14n).to receive(:text_key).and_raise("WTF!")
        adapter.ingest csv

        expect(adapter.protocol).
          to eq ["Checking data file format: csv ... OK",
                 "Checking schema is conforming to: test ... OK",
                 "Applying cleaning ... ERROR",
                 "  WTF!",
                 ">>> ERROR"]
        expect(adapter).to be_failed
      end

      it "#validation_result without data" do
        expect(adapter.validation_result.protocol).to eq "No data\n>>> ERROR"
        expect(adapter).to be_failed
      end

      it "#validation_result with data" do
        adapter.raw_data = bad_csv
        exp_prot = <<~TXT.strip
          Checking data file format: csv ... OK
          Checking schema is conforming to: test ... ERROR
            Line 1: enum_column can not be casted as enum with allowed values a, b, c
          >>> ERROR
        TXT
        expect(adapter.validation_result.protocol).to eq exp_prot
        expect(adapter).to be_failed
      end
    end

    describe "happy path" do
      it "does not protocol ERROR" do
        adapter.process_data csv
        expect(adapter.protocol).not_to include(/error/i)
      end

      it "protocols success" do
        adapter.process_data csv
        expect(adapter.protocol).to include(/ok/i)
        expect(adapter.protocol).to include ">>> SUCCESS"
      end

      it "returns the correct format" do
        result = adapter.process_data csv
        expect(result.format).to eq :json
      end

      it "returns the proper data" do
        result = adapter.process_data csv
        expected_data = [
          { "bool_column" => false,
            "enum_column" => "b",
            "float_column" => 5.67,
            "int_column" => 5,
            "name_column_p" => P14n.p("pater paul stored as fe42o3p2"),
            "personal_column" => "Personal stuff",
            "time_column" => "2021-02-22 12:34:56 UTC" }
        ]
        expect(result.data["test"].first["name_column_e"]).not_to be_nil
        result.data["test"].first.delete("name_column_e")
        expect(result.data["test"]).to eq expected_data
      end

      it "#validation_result" do
        adapter.raw_data = csv
        exp_prot = <<~TXT.strip
          Checking data file format: csv ... OK
          Checking schema is conforming to: test ... OK
          Applying cleaning ... OK
          Checking data presence ... OK
            Counted 1 rows
          >>> SUCCESS
        TXT
        expect(adapter.validation_result.protocol).to eq exp_prot
      end
    end
  end

  describe "excel" do
    let(:source) { create :source, kind: :manual_upload, data_category: :test, details: { file_format: "xlsx" } }
    let(:excel_data) { Rails.root.join("spec/fixtures/data_table_test_example.xlsx").read(mode: "rb") }
    let(:process_excel!) { adapter.process_data excel_data }

    it "does not complain" do
      result = process_excel!
      expect(result.protocol).not_to match(/error/i)
      expect(result.protocol).to match(/success/i)
    end

    it "parses the right data" do
      result = process_excel!
      exp =
        [{ "bool_column" => true,
           "enum_column" => "b",
           "float_column" => 3.14,
           "int_column" => 5,
           "name_column_p" => P14n.p("pater paul stored as fe42o3p2"),
           "personal_column" => "Personal Text",
           "time_column" => "2021-02-22 12:34:56 UTC" },
         { "bool_column" => true,
           "enum_column" => "c",
           "float_column" => 5.22,
           "int_column" => 7,
           "name_column_p" => P14n.p("pater paul stored as fe42o3p2"),
           "personal_column" => "Personal Text",
           "time_column" => "2021-02-22 12:34:56 UTC" }]

      expect(result.data["test"].first["name_column_e"]).not_to be_nil
      result.data["test"].first.delete("name_column_e")
      result.data["test"].second.delete("name_column_e")
      expect(result.data["test"]).to eq exp
    end
  end
end
