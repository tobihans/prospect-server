# frozen_string_literal: true

require "rails_helper"

describe Source::AamParent do
  #
  # AamParent inherits from S3, so we test only the overwritten behaviour
  #
  describe "metadata and source integration" do
    let(:source) { create :source, kind: "aam_parent" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "aam_v3"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "aam"
    end
  end

  describe "Mocked requests" do
    before do
      # The AWS stubbing provides empty default responses,
      # we just overwrite the ones we need.
      Aws.config[:s3] = {
        stub_responses: {
          # list_buckets: { buckets: [{name: 'my-bucket' }] },
          list_objects_v2: { contents: [{ key: "an_object_key" }, { key: "b_an_object_key" }] }
        }
      }
    end

    describe "api calls" do
      let(:response) do
        source = create :source
        adapter = described_class.new source
        data, _more_data, _hint = adapter.fetch(nil)
        data
      end

      it "stubbed object download" do
        expect(response).to be_nil
      end
    end

    describe "UI" do
      let(:source) do
        create :source,
               secret: "SECRET_KEY",
               kind: "aam_parent",
               details: {
                 "access_key" => "SOME_ACCESS_KEY",
                 "region" => "us-east-1",
                 "bucket_name" => "my_bucket"
               }
      end
      let(:adapter) { source.adapter }
      let(:good_response) { [source.data_table.examples].to_json }

      it "validation_result success" do
        Aws.config[:s3][:stub_responses][:get_object] = { body: good_response }
        x = adapter.validation_result.protocol

        expect(x).to include ">>> SUCCESS"
        expect(x).not_to match(/error/i)
      end

      it "validation_result error - bad region" do
        source.details["region"] = "unknown_region"
        x = adapter.validation_result.protocol

        expect(x).not_to match(/success/i)
        expect(x).to match(/error/i) # AWS reports non existing regions
        expect(x).to match(/Not every service is available in every region/) # AWS reports non existing regions
      end

      it "validation_result error - no file" do
        Aws.config[:s3][:stub_responses][:list_objects_v2] = {}
        x = adapter.validation_result.protocol

        expect(x).to match(/success/i)
        expect(x).to match(/0 object/)
      end
    end
  end

  describe "parent specifics" do
    let(:meter_mapping) do
      create :org_device_mapper, :aam_v3
    end

    let(:meta) do
      Data.define(:key, :last_modified, :etag).new("#{meter_mapping.device_id}/a2ei_123456789.log", Time.parse("2023-09-01 04:30:41 UTC"), "some_e_tag")
    end

    # we need an active child source for device mapping
    let(:source) { create :source, kind: "aam_child", organization: meter_mapping.organization }
    let(:parent) { described_class.new(create(:source, kind: "aam_parent")) }

    before do
      source # use it, because we need it for every test
    end

    it "create_imports generates one import" do
      expect do
        parent.create_import "some data", meta
      end.to change(Import, :count).by(1)
    end

    it "create_imports creates meta data" do
      parent.create_import "some data", meta

      i = source.imports.first
      import_data = i.file.download
      line_iter = import_data.each_line

      expect(line_iter.next).to eq "{\"last_modified\":\"2023-09-01 04:30:41 UTC\",\"key\":\"#{meta.key}\",\"etag\":\"#{meta.etag}\"}\n"
      expect(line_iter.next).to eq "---\n"
      expect(line_iter.next).to eq "some data"
    end

    it "filter_object_list" do
      filtered = parent.filter_object_list [{ key: "null/this_should_be_filtered_out" }, { key: meta.key }, { key: "1234/this_should_have_no_mapping" }]
      expect(filtered.size).to be 1
      expect(filtered.first[:key]).to eq meta.key
    end

    it "retrieve_data" do
      Aws.config[:s3] = {
        stub_responses: {
          list_objects_v2: [{ contents: [{ key: "an_object_key" }, { key: "b_an_object_key" }] },
                            { contents: [{ key: "b_an_object_key" }] }],
          get_object: [{ body: {} }, { body: {} }]
        }
      }
      result = parent.retrieve_data

      # smoke test
      expect(result).to be_a Source::Adapter::Result
    end

    it "filter object keys" do
      object_list = [{ key: "#{meter_mapping.device_id}/xyz_1234566778.log" }, { key: "#{meter_mapping.device_id}/a2ei_1234566778.log" }]
      filtered = parent.filter_object_list object_list

      expect(filtered.size).to eq 1
      expect(filtered.first[:key]).to eq "#{meter_mapping.device_id}/a2ei_1234566778.log"
    end
  end
end
