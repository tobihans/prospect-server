# frozen_string_literal: true

require "rails_helper"

describe Source::Api::Upya do
  let(:source) { create :source, kind: "api/upya" }

  describe "stubbed requests" do
    before do
      responses = {
        contracts: [
          {
            assets: [],
            agents: [],
            users: [],
            teams: [],
            agent: {
              profile: {
                firstName: "Someone",
                gender: "Female",
                lastName: "Musterfrau"
              },
              contact: {
                mobile: 7_680_123_456,
                fullNumber: nil,
                countryCode: "+260",
                address: nil,
                email: nil,
                secondaryMobile: nil
              },
              _id: "12323456ce0012159211",
              agentNumber: "HH42U"
            },
            country: "Zambia",
            status: "ENABLED",
            lastStatusUpdate: "2023-08-02T01:00:00.000Z",
            totalDaysActivated: 3,
            deal: {
              dealNumber: "XH0HY"
            },
            product: {
              category: "Energy",
              productReference: "Home200x",
              name: "P1 Ogress Sunking Home 200x"
            },
            dealName: "SunKing Home 200x on Loan",
            totalCost: 2500,
            remainingDebt: 1234,
            debt: 1234,
            backPay: "Off",
            totalPaid: 2500 - 1234,
            ccy: "ZMW",
            recorder: "HH42U",
            respAgent: "HH42U",
            agentSince: "2022-01-01T13:00:00.000Z",
            type: "PAYG",
            client: {
              profile: {
                firstName: "Frida",
                lastName: "Ganzi",
                birthday: "1941-10-10T10:00:00.000Z",
                gender: "Female",
                village: "Mwampila, Kakoma, Keembe, chibombo, central",
                county: "Zambia"
              },
              contact: {
                countryCode: "+260",
                mobile: "960675251",
                fullNumber: "+260+260",
                neighbour: "961230557",
                secondaryMobile: nil
              },
              clientNumber: "C12345678"

            },
            entryDate: "2022-01-01T01:01:01.051Z",
            submissionDate: "2022-01-01T01:01:01.051Z",
            lockable: true,
            onboardingStatus: "Signed",
            contractNumber: "A5231234567",
            updatedAt: "2022-01-01T01:01:01.051Z",
            creditDecisionDate: "2022-01-01T01:01:01.051Z",
            creditDecisionUserId: "U0234324234",
            asset: {

              assetNumber: "123456789098",

              paygNumber: "123456554"

            },
            paygNumber: "123456554",
            signingDate: "2022-01-01T01:01:01.051Z",
            nextStatusUpdate: "2023-01-01T01:01:01.051Z"
          }
        ],

        clients: [{
          profile: {
            gps: {
              longitude: "30.123456789",
              latitude: "-14.1234567"
            },
            firstName: "Frida",
            lastName: "Musterfrau",
            birthday: "1982-01-01T01:01:01.000Z",
            address: "some area"
          },
          contact: {
            countryCode: "+260",
            mobile: "975123456",
            fullNumber: "260975123456"
          },
          contracts: [
            {
              _id: "123ea782c",
              deal: {
                _id: "123c087",
                dealName: "Sunking 32",
                dealNumber: "X5FUU"
              },
              dealName: "Sunking 32",
              contractNumber: "A7099123456"
            }
          ],
          _id: "123ea7818",
          clientNumber: "C012345667789",
          agent: {
            agentNumber: "912345"
          },
          entryDate: "2023-01-01T09:01:01.000Z",
          status: "Pending",
          recorder: "912345",
          updatedAt: "2023-01-01T09:01:01.000Z"
        }],

        payments: [
          {
            contract: {
              contractNumber: "A1234567"
            },
            agent: {
              profile: {
                firstName: "MAx",
                gender: "Male",
                birthday: nil,
                lastName: "Musterman"
              },
              agentNumber: "OO7"
            },
            contractNumber: "A1234567",
            operator: "Primenet",
            type: "MM",
            date: "2023-01-01T00:01:01.000Z",
            amount: 200,
            paymentReference: "123456789",
            ccy: "ZMW",
            mobile: "260974123456",
            status: "ACCEPTED",
            paymentCode: "PAYMENT_SUCCESS",
            transactionId: "PNZ12345678P1234",
            message: {
              text: "Thank you...",
              status: "PENDING"
            }
          }
        ],
        assets: [{
          distributed: {
            status: true,
            date: "2022-04-17T10:00:00.000Z"
          },
          serialNumber: "20077236",
          dateAdded: "2022-01-1T10:00:00.000Z",
          lastUpdate: "2022-01-1T10:00:00.000Z",
          status: "DEPLOYED",
          product: {
            productReference: "Product100",
            name: "Product100"
          },
          paygNumber: "20012345",
          batchNumber: "10.3.2022",
          updatedAt: "2023-01-1T10:00:00.000Z",
          entity: nil,
          heldBy: nil,
          heldSince: "2022-01-1T10:00:00.000Z",
          contract: {
            contractNumber: "A12345678"
          },
          deployDate: "2022-04-17T10:00:00.000Z",
          ownedBy: {
            profile: {
              firstName: "Max ",
              lastName: "Musterman"
            },
            contact: {
              countryCode: "+260",
              mobile: "967123456"
            },
            clientNumber: "C20123456"
          }
        }],
        "contract-events": [{
          _id: "6229bb5b8fa68800137c68a9",
          type: "SIGNED",
          additionalInfo: nil,
          contract: {
            dealName: "TEST",
            contractNumber: "A90201219",
            asset: {
              product: {
                productReference: "TEST"
              }
            }
          },
          date: "2022-03-02T12:00:00.000Z",
          info: nil,
          user: {
            profile: {
              firstName: "Upya",
              lastName: "Support"
            },
            userNumber: "CO123456789878765"
          },
          createdAt: "2022-03-10T08:48:27.069Z"
        }],
        products: [{
          name: "P2 Ogress Sunking Home 500 x",
          description: "Sunking Home 500 x comes with 21 and 32 inch TVs . ",
          category: "Energy",
          lockable: true,
          productReference: "Home500x",
          manufacturer: "GLP"
        }],
        deals: [{
          dealName: "P1 Ogress Wow 60 Loan sale",
          dealNumber: "X1FZX",
          type: "PAYG",
          totalCost: 1700,
          status: "Active",
          ccy: "ZMW",
          backPay: "Off",
          lockable: true,
          questionnaire: "Onboarding",
          eligibleProduct: {
            name: "P1 Ogress Wow 60",
            productReference: "Wow60",
            category: "Energy"
          },
          pricingSchedule: {
            recurring: 117,
            days: 30,
            upfront: 300,
            upfrontDays: 7,
            minPayment: 4
          },
          topUpSchedule: {},
          setUpOn: "2022-05-02T10:32:10.183Z",
          restricted: false,
          restrictedTo: []
        }]
      }

      described_class::ENDPOINTS.each do |key, ep|
        stub_request(:post, "#{described_class::URL}#{ep[:path]}").
          to_return(status: 200, body: responses[key].to_json, headers: { content_type: "application/json" })
      end
    end

    let(:result) do
      s = described_class.new create :source
      s.retrieve_data
    end

    let(:response) do
      JSON.parse result.data
    end

    it "some data" do
      described_class::ENDPOINTS.each_key do |key|
        expect(response).to have_key(key.to_s)
      end
    end

    it "cleaning" do
      # test at least one random field for each endpoint
      expect(response["contracts"].first["client"]["profile"]).to have_key("firstName_p")
      expect(response["contracts"].first["client"]["profile"]).not_to have_key("firstName")

      expect(response["clients"].first["profile"]).to have_key("address_p")
      expect(response["clients"].first["profile"]).not_to have_key("address")
      expect(response["clients"].first["profile"]).not_to have_key("birthday") # birthday case
      expect(response["clients"].first["profile"]).to have_key("birthday_b")
      expect(response["clients"].first["profile"]["gps"]).not_to have_key("longitude")
      expect(response["clients"].first["profile"]["gps"]).not_to have_key("latitude")

      expect(response["payments"].first).to have_key("mobile_p")
      expect(response["payments"].first).not_to have_key("mobile")

      expect(response["assets"].first["ownedBy"]["profile"]).to have_key("firstName_p")
      expect(response["assets"].first["ownedBy"]["profile"]).not_to have_key("firstName")

      expect(response["contract-events"].first["user"]["profile"]).to have_key("firstName_p")
      expect(response["contract-events"].first["user"]["profile"]).not_to have_key("firstName")
    end

    it "empty repsonse" do
      stub_request(:post, "#{described_class::URL}#{described_class::ENDPOINTS.first[1][:path]}").
        to_return(status: 200, body: [].to_json, headers: { content_type: "application/json" })

      expect(response).to be_present
    end
  end

  describe "failing request" do
    it "import fails" do
      stub_request(:post, "#{described_class::URL}#{described_class::ENDPOINTS.first[1][:path]}").
        to_return(status: 503, body: nil, headers: { content_type: "application/json" })

      s = described_class.new create :source
      result = s.retrieve_data

      expect(result.data).to be_nil
    end
  end

  describe "metadata and source integration" do
    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "api/upya"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "upya"
    end
  end
end
