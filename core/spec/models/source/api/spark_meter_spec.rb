# frozen_string_literal: true

require "rails_helper"

describe Source::Api::SparkMeter do
  describe "Mocked requests" do
    before do
      response = {
        "customers" => [
          { "code" => "",
            "credit_balance" => 123.4567890123,
            "debt_balance" => 0.0,
            "ground" => { "id" => "123e4567-e89b-12d3-a456-426614174000", "name" => "xyz-location001" },
            "id" => "223e4567-e89b-12d3-a456-426614174001",
            "meters" =>
             [{ "active" => true,
                "address" => "A-1, xxxxxxx, North Western Province, Zambia",
                "bootloader" => "0100020000",
                "city" => "Mwinilunga",
                "coords" => "",
                "country" => "Zambia",
                "current_daily_energy" => nil,
                "current_tariff_name" => "Medium Consumption",
                "firmware" => "0100050000",
                "is_running_plan" => true,
                "last_config_datetime" => "2022-11-17T06:15:26.406849",
                "last_cycle_start" => "2022-11-01T19:00:00",
                "last_energy" => 1999.9123456,
                "last_energy_datetime" => "2022-11-20T23:30:00",
                "last_meter_state_code" => 1,
                "last_plan_expiration_date" => "2022-11-21T06:15:00",
                "last_plan_payment_date" => "2022-11-20T06:15:00",
                "model" => "SM60R",
                "operating_mode" => 2,
                "plan_balance" => 6.74597901785486,
                "postalcode" => "",
                "serial" => "SM60R-07-000123456",
                "state" => "North Western Province",
                "street1" => "A-1",
                "street2" => "",
                "tags" => [],
                "total_cycle_energy" => 226.718437500002 }],
            "name" => "Max Musterman",
            "phone_number" => "+260123456789",
            "phone_number_verified" => false }
        ],
        "status" => "success",
        "errors" => nil
      }
      stub_request(:get, "https://example.com/customers").
        to_return(status: 200, body: response.to_json, headers: { content_type: "application/json" })
    end

    describe "api calls" do
      let(:response) do
        s = described_class.new create :source
        s.fetch("https://example.com", "")
      end

      it "returns correctly some data" do
        expect(response).to have_key("status")
      end

      it "p14n name field" do
        expect(response["customers"].first).not_to have_key("name")
        expect(response["customers"].first).to have_key("name_p")
        expect(response["customers"].first["name_p"]).not_to match(/^Max$/)
      end

      it "encrypts name field" do
        expect(response["customers"].first).to have_key("name_e")
        expect(response["customers"].first["name_e"]).not_to be_empty
        expect(response["customers"].first["name_e"]).not_to match(/^Max$/)
      end

      it "encrypts phone_number field" do
        expect(response["customers"].first).to have_key("phone_number_e")
        expect(response["customers"].first["phone_number_e"]).not_to be_empty
        expect(response["customers"].first["phone_number_e"]).not_to match(/260123456789/)
      end
    end

    it "API fail" do
      stub_request(:get, "https://example.com/customers").
        to_return(status: 400, body: nil, headers: { content_type: "application/json" })

      s = described_class.new create :source
      s.fetch("https://example.com", "")

      expect(s.protocol.last).to match(/ERROR/)
    end

    describe "UI" do
      let(:source) do
        s = Source.new
        s.details["url"] = "https://example.com"
        s
      end

      it "validation_result success" do
        s = described_class.new source
        expect(s.validation_result.protocol).to match(/SUCCESS/)
      end

      it "validation_result error" do
        s = described_class.new create :source # empty source
        expect(s.validation_result.protocol).to match(/ERROR/)
      end

      it "retrieve_data" do
        s = described_class.new source
        expect(s.retrieve_data.format).to be :json
      end
    end
  end

  describe "metadata and source integration" do
    let(:source) { create :source, kind: "api/spark_meter" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "api/spark_meter"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "spark_meter"
    end
  end
end
