# frozen_string_literal: true

require "rails_helper"

describe Source::Api::Angaza do
  import_hint = { "status" => "normal",
                  "endpoints" => { "accounts" => "2024-07-01T10:10:10Z", "clients" => "CL000001", "payments" => "2020-04-14T19:32:21Z", "replacements" => "2020-04-14T19:00:00Z", "users" => nil,
                                   "pricing_groups" => nil } }

  let(:source) do
    create :source,
           kind: "api/angaza",
           secret: "some_secret",
           details: { username: "test@a2ei.org" }
  end
  let(:import) { create :import, source: }
  let(:source_adapter) { described_class.new(source) }

  context "when first import" do
    let(:result) do
      VCR.use_cassette :api_connector_angaza_test do
        source_adapter.retrieve_data(nil)
      end
    end

    before do
      import
      source_adapter.validation_mode = true
    end

    it "API call works" do
      expect(source_adapter.validation_hint).to eq ""
      expect(source_adapter.data_categories).to eq ["payments_ts"]
      expect(source_adapter.data_origin).to eq "angaza"

      expect(result.data.keys).to match_array(described_class::ENDPOINTS.keys)

      described_class::ENDPOINTS.each_key do |key|
        expect(result.data[key]).not_to be_empty
      end
    end

    it "retrieves all existing data" do
      expect(result.data["payments"]["_embedded"]["item"].size).to eq 1
      expect(source_adapter.validation_hint).to eq ""
      expect(result.more_data).to be true # we are in INITIAL_BACKFILL mode because import_hint is not set
    end

    it "API call retrieves data" do
      expect(result.protocol).to include "SUCCESS"

      # cleaning the PII data
      result.data["clients"]["_embedded"]["item"].each do |client|
        expect(client.keys).not_to include "name"
        expect(client.keys).not_to include "primary_phone"

        expect(client.keys).to include "name_p"
        expect(client.keys).to include "name_e"
        expect(client.keys).to include "primary_phone_p"
        expect(client.keys).to include "primary_phone_e"
      end

      result.data["payments"]["_embedded"]["item"].each do |payment|
        expect(payment.keys).not_to include "msisdn"
        expect(payment.keys).to include "msisdn_p"
        expect(payment.keys).to include "msisdn_e"
      end
    end

    it "removes photo attributes" do
      expect(result.protocol).to include "SUCCESS"

      described_class::ENDPOINTS.each_key do |_key|
        endpoint_data = result.data["clients"]
        endpoint_data["_embedded"]["item"].each do |item|
          item["attribute_values"].each do |a|
            expect(a["type"]).not_to include "PHOTO"
          end
        end
      end
    end

    it "correct next import hint" do
      expect(JSON.parse(result.hint)).to eq(import_hint)
    end

    context "when there is an attribute mapping" do
      let(:attribute_mapping) do
        { "City" => "custom",
          "Full Name" => "customer_name",
          "Primary Phone" => "customer_phone",
          "NRC Number" => "custom_anonymize",
          "Client Phone 2" => "custom_anonymize",
          "Does The Client Have An Alternative Name (Nickname)" => "custom_anonymize",
          "Gender" => "customer_gender",
          "Age (Years)" => "custom_anonymize",
          "Client Photo" => "custom_anonymize",
          "Clients Province" => "customer_location_area_1",
          "Name Of Clients District Where The System Will Be Installed" => "customer_location_area_2",
          "Photo Of The Outside Of The House Where The Client Stays" => "custom_anonymize",
          "Clients Physical Road Address or Town/Village" => "customer_address",
          "What Is The Nearest Landmark To The Clients House?" => "custom",
          "Local Chief Full Name" => "custom_anonymize",
          "Clients Language Preference" => "custom",
          "Name of 1st Family Relative" => "custom_anonymize",
          "Clients Relation with 1st Family Relative" => "custom",
          "Physical Address of 1st Family Relative" => "custom_anonymize",
          "Phone Number of 1st Family Relative" => "custom_anonymize",
          "Name of 2nd Family Relative (NOT SPOUSE OR CHILD)" => "custom_anonymize",
          "Clients Relation with 2nd Family Relative" => "custom",
          "Physical Address of 2nd Family Relative" => "custom_anonymize",
          "Phone Number of 2nd Family Relative" => "custom_anonymize",
          "How Reliable Is The Clients Network Coverage At Home" => "custom",
          "Clients Occupation" => "customer_profession",
          "Clients Monthly Salary from Contracted Employment (EXCLUDING PERSONAL BUSINESS INCOME)" => "custom_anonymize",
          "What is the name of the Clients workplace?" => "custom_anonymize",
          "When does the Client usually receive their Contracted Employment Salary?" => "custom",
          "Clients Monthly Income from Personal/Family business (EXCLUDING SALARY from Contracted Employment)" => "custom_anonymize",
          "Clients Monthly Household Rent" => "custom_anonymize",
          "What Is The Roofing Material On The Clients Home?" => "custom",
          "How many dependents does the Client have?" => "custom",
          "How many people live in the clients house and will use the product?" => "customer_household_size",
          "How much in TOTAL does the Client pay for School Fees per Year? (For all Children)" => "custom_anonymize",
          "Has the Client used any other Solar Home System?" => "custom",
          "Does the Client currently have electricity supply at their home?" => "custom",
          "For what purpose will the client be using the Product?" => "primary_use",
          "Does the client own any Farming land used for crops?" => "custom",
          "What is the Clients main Harvested Crop?" => "custom",
          "How Much Did The Client Spend On Seed And Fertilizer Last Season" => "custom",
          "Does the client own any livestock?" => "custom",
          "What was the income from the Clients last harvest" => "ignore",
          "What was the Income generated from the Clients livestock last year" => "custom",
          # one of the attributes we delete, because we can't access foto links anyway
          # "Front of Zambian NRC/Passport" => "custom_anonymize",
          "Back of Zambian NRC/Passport" => "ignore",
          "Does the Client own a Smartphone?" => "custom",
          "Does the Client own any Motorized Vehicles?" => "custom",
          "How Far Is The Nearest Medical Facility To The Clients House" => "custom",
          "What product does the Client want RDG to offer next?" => "custom" }
      end
      let(:source) do
        create :source,
               kind: "api/angaza",
               secret: "some  secret",
               details: { username: "test@a2ei.de", attribute_mapping: }
      end

      before do
        allow(source_adapter).to receive(:fetch).and_return([result.data, nil, ""])
      end

      it "replaces the attribute names" do
        attribute_names = result.data["clients"]["_embedded"]["item"].first["attribute_values"].pluck("name")
        expect(attribute_names).to include("customer_profession")
        expect(attribute_names).to include("customer_gender")
        expect(attribute_names).not_to include("Clients Occupation")
        expect(attribute_names).not_to include("Gender")
        expect(attribute_names).to include("customer_phone")
        expect(attribute_names).to include("customer_location_area_1")
        expect(attribute_names).to include("customer_location_area_2")
        expect(attribute_names).to include("customer_address")
        expect(attribute_names).to include("customer_household_size")
        expect(attribute_names).to include("primary_use")
        expect(attribute_names).not_to include("custom")
        expect(attribute_names).not_to include("custom_anonymize")
      end

      it "cleans anonymized attributes" do
        result.data["clients"]["_embedded"]["item"].first["attribute_values"].each do |attribute|
          next unless attribute["type"] == "custom_anonymize" || described_class.private_attributes.include?(attribute["name"])

          expect(attribute.keys).not_to include "value"
          expect(attribute.keys).to include "value_p"
          expect(attribute.keys).to include "value_e"
          expect(attribute["value_p"]).not_to be_empty
          expect(attribute["value_e"]).not_to be_empty
        end
      end

      it "removes ignored attributes" do
        attribute_names = result.data["clients"]["_embedded"]["item"].first["attribute_values"].pluck("name")
        expect(attribute_names).not_to include("What was the income from the Clients last harvest")
      end

      it "cleans whitelisted users attributes" do
        result.data["users"]["_embedded"]["item"].first["attribute_values"].each do |attribute|
          expect(attribute.keys).not_to include "value"
          expect(attribute.keys).to include "value_p"
          expect(attribute.keys).to include "value_e"
          expect(attribute["value_p"]).not_to be_empty
          expect(attribute["value_e"]).not_to be_empty
        end
      end

      it "keeps only whitelisted users attributes" do
        result.data["users"]["_embedded"]["item"].first["attribute_values"].each do |attribute|
          expect(described_class::USERS_ATTRIBUTE_WHITELIST.include?(attribute["attribute_qid"])).to be true
        end
      end
    end
  end

  context "when last import hint is set - normal mode" do
    let(:result_with_hint) do
      VCR.use_cassette :api_connector_angaza_test do
        source_adapter.retrieve_data(import_hint.to_json)
      end
    end

    it "stops when hitting item with hint before last_import_hint" do
      expect(JSON.parse(result_with_hint.hint)).to eq(import_hint)
      expect(result_with_hint.protocol).to include "Found item with"
    end
  end

  context "when last import hint is set - initial backfill mode" do
    import_hint_backfill = { "status" => "initial_backfill",
                             "endpoints" => { "accounts" => 0, "clients" => 0, "payments" => 0, "replacements" => 0, "users" => 0,
                                              "pricing_groups" => 0 } }
    let(:result_with_hint) do
      VCR.use_cassette :api_connector_angaza_test do
        source_adapter.retrieve_data(import_hint_backfill.to_json)
      end
    end

    it "sets first_run_hint on first_run" do
      # should stop import after first request
      # this simulates the first import of many in initial_backfill mode
      stub_const("Source::Api::Angaza::MAX_ENTRIES_PER_ENDPOINT", Source::Api::Angaza::PAGE_SIZE)
      parsed_hint = JSON.parse(result_with_hint.hint)
      expect(parsed_hint["status"]).to eq "initial_backfill"
      expect(parsed_hint["endpoints"]).to eq(import_hint_backfill["endpoints"])
      expect(parsed_hint["first_run_hint"]).to eq(import_hint["endpoints"])
    end

    it "stops when all endpoints reach last page" do
      parsed_hint = JSON.parse(result_with_hint.hint)
      expect(parsed_hint["status"]).to eq "normal"
      expect(parsed_hint["endpoints"]).to eq(import_hint["endpoints"])
    end
  end

  context "when credentials are wrong" do
    let(:result) do
      VCR.use_cassette :no_credentials_angaza_test do
        source_adapter.validation_result
      end
    end

    before { source_adapter.validation_mode = true }

    it "returns an error" do
      expect(result.error).to eq "Connecting to Angaza API: wrong credentials provided"
    end
  end

  it "initial last_import_hint" do
    hint = Source::Api::Angaza::ImportHint.new(nil, Source::Api::Angaza::ENDPOINTS)
    expect(hint.endpoints).to eq({ "accounts" => 0, "clients" => 0, "payments" => 0, "pricing_groups" => 0, "replacements" => 0, "users" => 0 })
    expect(hint.status).to eq("initial_backfill")
  end

  describe "attribute loading related smoke tests" do
    it "load_table_attributes" do
      attr, priv_attr = described_class.load_table_attributes

      expect(attr).not_to be_empty
      expect(priv_attr).not_to be_empty
    end

    it "ui_attributes" do
      attr = described_class.ui_attributes

      expect(attr).not_to be_empty
    end
  end
end
