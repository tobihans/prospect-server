# frozen_string_literal: true

require "rails_helper"

describe Source::Api::SparkmeterKoios do
  let(:source) { create :source, kind: "api/sparkmeter_koios" }
  let(:response_data) do
    {
      cursor: "LONG_CURSOR",
      errors: [],
      data: [{
        name: "Max Musterman",
        code: "1941",
        phone_number: "123123132",
        id: "52e6d871-9353-4f53-b1e7-c13417a4fdbc",
        balances: {
          credit: {
            value: "8.97",
            currency: "ZMW"
          },
          plan: {
            value: "0.00",
            currency: "ZMW"
          },
          technical_debt: {
            value: "0.00",
            currency: "ZMW"
          }
        },
        meters: [
          {
            address: "Somewhere, Someplace",
            coordinates: nil,
            operating_mode: "auto",
            tariff_id: "52e6d871-9353-4f53-b1e7-c13417a4fdbc",
            pole_id: nil,
            meter_phase: nil,
            tariff: {
              name: "Residential",
              electricity_rate_type: "flat_rate",
              block_rate: nil,
              rate_amount: {
                value: "1.0000",
                numerator: "ZMW",
                denominator: "kWh"
              },
              time_of_use: nil,
              load_limit_type: "flat",
              load_limit: {
                value: 25_000,
                unit: "Watts"
              },
              plan_type: nil,
              low_balance_threshold: {
                amount: "0.0000",
                currency: "ZMW"
              },
              inrush_current_protection_disabled: false,
              block_rate_cycle_reset_energy: nil,
              last_block_rate_cycle_reset_at: nil
            },
            id: "52e6d871-9353-4f53-b1e7-c13417a4fdbc",
            latest_reading: {
              apparent_power_avg: 0,
              current_avg: 0,
              current_max: 0,
              current_min: 0,
              energy: 0.849,
              frequency: 49,
              heartbeat_end: "2024-01-22T16:30:00",
              heartbeat_start: "2024-01-22T16:15:00",
              power_factor_avg: 0,
              state: "on",
              true_power_avg: 0,
              true_power_inst: 0,
              uptime_secs: 298_851,
              user_power_limit: 10_640,
              voltage_avg: 235.25,
              voltage_max: 237.30999755859375,
              voltage_min: 0
            },
            serial: "SMRSD-04-123456789"
          },
          {
            address: "Somewhere, Someplace",
            coordinates: {
              latitude: "-12.5214",
              longitude: "25.1234"
            },
            operating_mode: "auto",
            tariff_id: "52e6d871-9353-4f53-b1e7-c13417a4fdbc",
            pole_id: nil,
            meter_phase: nil,
            tariff: {
              name: "Residential",
              electricity_rate_type: "flat_rate",
              block_rate: nil,
              rate_amount: {
                value: "1.0000",
                numerator: "ZMW",
                denominator: "kWh"
              },
              time_of_use: nil,
              load_limit_type: "flat",
              load_limit: {
                value: 25_000,
                unit: "Watts"
              },
              plan_type: nil,
              low_balance_threshold: {
                amount: "0.0000",
                currency: "ZMW"
              },
              inrush_current_protection_disabled: false,
              block_rate_cycle_reset_energy: nil,
              last_block_rate_cycle_reset_at: nil
            },
            id: "52e6d871-9353-4f53-b1e7-c13417a4fdbd",
            latest_reading: {
              apparent_power_avg: 0,
              current_avg: 0,
              current_max: 0,
              current_min: 0,
              energy: 0.849,
              frequency: 49,
              heartbeat_end: "2024-01-22T16:30:00",
              heartbeat_start: "2024-01-22T16:15:00",
              power_factor_avg: 0,
              state: "on",
              true_power_avg: 0,
              true_power_inst: 0,
              uptime_secs: 298_851,
              user_power_limit: 10_640,
              voltage_avg: 235.25,
              voltage_max: 237.30999755859375,
              voltage_min: 0
            },
            serial: "SMRSD-04-1234567890"
          }
        ],
        last_plan_renewal: nil,
        next_plan_renewal: nil,
        site_id: "52e6d871-9353-4f53-b1e7-c13417a4fdbc"
      }]
    }
  end

  describe "stubbed requests" do
    before do
      stub_request(:get, /#{described_class::URL}customers.*/).
        to_return({ status: 200, body: response_data.to_json }, { status: 200, body: response_data.to_json }, { status: 200, body: nil })
    end

    let(:response) do
      s = described_class.new create :source
      data, _more_data, _hint = s.fetch(nil, nil)
      JSON.parse data
    end

    it "some data" do
      expect(response).to have_key("customers")
    end

    it "cleaning" do
      # test at least some fields ...
      expect(response["customers"].first).to have_key("name_p")
      expect(response["customers"].first["meters"].first).to have_key("address_p")

      # coordinates
      coords = response["customers"].first["meters"].second["coordinates"]
      expect(coords).to have_key("lat_p")
      expect(coords).to have_key("lon_p")
      expect(coords).to have_key("lat_b")
      expect(coords).to have_key("lon_b")
      expect(coords).not_to have_key("latitude")
      expect(coords).not_to have_key("longitude")
    end

    it "paging" do
      expect(response["customers"].length).to eq 2
    end

    it "validation_result" do
      s = described_class.new create :source
      response = JSON.parse s.validation_result.data

      expect(response["customers"].length).to eq 1 # don't load 2. page
    end
  end

  describe "retry http request" do
    it "retry succeeds" do
      stub_request(:get, /#{described_class::URL}customers.*/).to_raise("some error").times(3).
        to_return({ status: 200, body: response_data.to_json }, { status: 200, body: nil })

      s = described_class.new create :source
      data, _more_data, _hint = s.fetch(nil, nil)
      response = JSON.parse data

      expect(response).to have_key("customers")
    end

    it "retries exhausted" do
      stub_request(:get, /#{described_class::URL}customers.*/).to_raise("some error").times(4).
        to_return({ status: 200, body: response_data.to_json }, { status: 200, body: nil })

      s = described_class.new create :source
      data, _more_data, _hint = s.fetch(nil, nil)
      expect(data).to be_nil
    end
  end

  describe "metadata and source integration" do
    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "api/sparkmeter_koios"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "sparkmeter"
    end
  end
end
