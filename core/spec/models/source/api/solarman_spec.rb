# frozen_string_literal: true

require "rails_helper"
require "timecop"

describe Source::Api::Solarman do
  describe "Mocked requests" do
    before do
      app_id = "12345"
      response_auth_login = {
        code: nil,
        msg: nil,
        success: true,
        requestId: "b07f1630999db2b4",
        access_token: "SAMPLE_TOKEN",
        token_type: "bearer",
        refresh_token: "SAMPLE_TOKEN",
        expires_in: "5183999",
        scope: nil,
        uid: 13_307_020
      }

      response_plants = {
        code: nil,
        msg: nil,
        success: true,
        requestId: "6c113d6ed6d8704d",
        total: 1,
        stationList: [
          {
            id: 60_993_186,
            name: "SAM",
            locationLat: 7.1758639,
            locationLng: 2.1525448,
            locationAddress: "Samionta ",
            regionNationId: 20,
            regionLevel1: 456,
            regionLevel2: nil,
            regionLevel3: nil,
            regionLevel4: nil,
            regionLevel5: nil,
            regionTimezone: "Europe/Amsterdam",
            type: "GROUND",
            gridInterconnectionType: "BATTERY_BACKUP",
            installedCapacity: 17.48,
            startOperatingTime: 1_688_762_734.000000000,
            stationImage: "https://img1.solarmanpv.com/default/3fce41ab612144a2a2f1a4555d67ea851696346430577.jpg",
            createdDate: 1_688_762_775.000000000,
            batterySoc: 100.0,
            networkStatus: "NORMAL",
            generationPower: 1414.0,
            lastUpdateTime: 1_699_029_886.000000000,
            contactPhone: nil,
            ownerName: nil
          }
        ]
      }
      response_historic_data = {
        code: nil,
        msg: nil,
        success: true,
        requestId: "d2c4a222d23a88b3",
        total: 284,
        stationDataItems: [
          {
            generationPower: 0.0,
            usePower: 1484.0,
            gridPower: nil,
            purchasePower: nil,
            wirePower: 0.0,
            chargePower: nil,
            dischargePower: 1639.0,
            batteryPower: 1639.0,
            batterySoc: 73.0,
            irradiateIntensity: nil,
            generationValue: nil,
            generationRatio: 0.0,
            gridRatio: nil,
            chargeRatio: nil,
            useValue: nil,
            useRatio: nil,
            buyRatio: nil,
            useDischargeRatio: nil,
            gridValue: nil,
            buyValue: nil,
            chargeValue: nil,
            dischargeValue: nil,
            fullPowerHours: nil,
            irradiate: nil,
            theoreticalGeneration: nil,
            pr: nil,
            cpr: nil,
            dateTime: 1_698_793_200.000000000,
            year: 2023,
            month: 11,
            day: 1
          },
          {
            generationPower: 0.0,
            usePower: 1584.0,
            gridPower: nil,
            purchasePower: nil,
            wirePower: 0.0,
            chargePower: nil,
            dischargePower: 1739.0,
            batteryPower: 1739.0,
            batterySoc: 83.0,
            irradiateIntensity: nil,
            generationValue: nil,
            generationRatio: 0.0,
            gridRatio: nil,
            chargeRatio: nil,
            useValue: nil,
            useRatio: nil,
            buyRatio: nil,
            useDischargeRatio: nil,
            gridValue: nil,
            buyValue: nil,
            chargeValue: nil,
            dischargeValue: nil,
            fullPowerHours: nil,
            irradiate: nil,
            theoreticalGeneration: nil,
            pr: nil,
            cpr: nil,
            dateTime: 1_698_793_200.000000000,
            year: 2023,
            month: 11,
            day: 2
          }
        ]
      }

      Timecop.freeze(Time.zone.at(1_675_729_062)) # we need to mock api calls with timestamps in the URL

      stub_request(:post, "#{described_class::URL}account/v1.0/token?appId=#{app_id}").
        to_return(status: 200, body: response_auth_login.to_json, headers: { content_type: "application/json" })

      stub_request(:post, "#{described_class::URL}station/v1.0/list?language=en").
        to_return(status: 200, body: response_plants.to_json, headers: { content_type: "application/json" })

      stub_request(:post, "#{described_class::URL}station/v1.0/history?language=en").
        to_return(status: 200, body: response_historic_data.to_json, headers: { content_type: "application/json" })
    end

    after do
      Timecop.return
    end

    describe "api calls" do
      # bundle exec rspec spec/models/source/api/solarman_spec.rb
      let(:response) do
        s = described_class.new create :source
        s.details["email"] = "default@example.com"
        s.source.secret = "dfsfsdf"
        s.details["app_id"] = "12345"
        s.details["app_secret"] = "dasdasdas"

        data, _more_data, _hint = s.fetch
        data
      end

      it "returns correctly some data" do
        expect(response.size).to be 1
        expect(response.first["stats_frame_detail_data"].size).to eq 62 # due to built in iteration the mock is called 31 times
        expect(response.first["stats_daily_aggregated_data"].size).to eq 2 # 2 entries to mocked response
      end
    end

    describe "Result fetching" do
      let(:result) do
        s = described_class.new create :source
        s.details["email"] = "default@example.com"
        s.source.secret = "dfsfsdf"
        s.details["app_id"] = "12345"
        s.details["app_secret"] = "dasdasdas"
        s.retrieve_data # => Result
      end

      it "returns ok" do
        expect(result).to be_ok
        expect(result.error).to be_nil
        expect(result.protocol).to match(/SUCCESS/)
        expect(result.data).to be_present
      end
    end

    describe "UI" do
      let(:source) do
        s = described_class.new create :source
        s.details["email"] = "default@example.com"
        s.source.secret = nil
        s.details["app_id"] = "12345"
        s.details["app_secret"] = "dasdasdas"
        s
      end

      it "validation_result error" do
        expect(source.validation_result.protocol).to match(/ERROR/)
      end

      it "validation_result ok" do
        source.source.secret = "fwrwerer"
        expect(source.validation_result.protocol).to match(/SUCCESS/)
      end
    end

    describe "metadata and source integration" do
      let(:source) { create :source, kind: "api/solarman" }

      it "has correct class for source" do
        expect(source.adapter.class).to eq described_class
      end

      it "generates correct data_schema" do
        expect(source.data_schema).to eq "api/solarman"
      end

      it "generates correct data_origin" do
        expect(source.data_origin).to eq "solarman"
      end
    end
  end
end
