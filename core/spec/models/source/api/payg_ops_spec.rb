# frozen_string_literal: true

require "rails_helper"

describe Source::Api::PaygOps do
  base64_string = %r{[-A-Za-z0-9+/]*={0,3}}
  encrypted_message = /^[0-9]+:#{base64_string}:#{base64_string}$/

  it "rescues time out" do
    allow(HTTParty).to receive(:post).and_raise(Net::ReadTimeout)

    source = described_class.new create :source,
                                        kind: "api/payg_ops",
                                        secret: "secret",
                                        details: { username: "user", url: "https://instance.paygops.com/", test_tags: ["Test"] }

    expect { source.validation_result }.not_to raise_error
    expect(source.protocol).to be_any(match(/Net::ReadTimeout/))
  end

  it "API call works" do
    source = described_class.new create :source,
                                        kind: "api/payg_ops",
                                        secret: "secret",
                                        details: { username: "user", url: "https://instance.paygops.com/", test_tags: ["Test"] }

    expect(source.validation_hint).to eq "Make sure your user role allows to generate API keys. (Create API Token Permission)"
    expect(source.data_categories).to eq ["payments_ts"]
    expect(source.data_origin).to eq "payg_ops"

    endpoints = Source::Api::PaygOps::ENDPOINTS.keys | %w[payment_export test_tags]

    VCR.use_cassette :api_connector_payg_ops_test do
      result = source.validation_result

      expect(result.data.keys).to match_array(endpoints)

      endpoints.each do |key|
        expect(result.data[key]).not_to be_empty
      end
    end

    VCR.use_cassette :api_connector_payg_ops do
      result = source.retrieve_data(nil, test: false)

      expect(result.data.keys).to match_array(endpoints)

      expect(result.protocol).to include "SUCCESS"

      # cleaning the PII data
      result.data["clients"].each do |obj|
        expect(obj.keys).not_to include "gps_latitude"
        expect(obj.keys).not_to include "gps_longitude"
        expect(obj.keys).to include "lat_p"
        expect(obj.keys).to include "lon_p"
        expect(obj.keys).to include "lat_e"
        expect(obj.keys).to include "lon_e"
        expect(obj.keys).to include "lat_b"
        expect(obj.keys).to include "lon_b"

        expect(obj.keys).not_to include "name"
        expect(obj.keys).to include "name_p"
        expect(obj.keys).to include "name_e"
        expect(obj["name_e"]).to match(encrypted_message)

        expect(obj.keys).not_to include "surname"
        expect(obj.keys).not_to include "surname_p"
        expect(obj.keys).not_to include "surname_e"

        expect(obj.keys).not_to include "preferred_phone_number"
        expect(obj.keys).to include "preferred_phone_number_p"
        expect(obj.keys).to include "preferred_phone_number_e"
        expect(obj["preferred_phone_number_e"]).to match(encrypted_message)

        expect(obj["phone_numbers"]).to all(match(encrypted_message))
      end

      result.data["payments"].each do |obj|
        expect(obj.keys).not_to include "wallet_name"
        expect(obj.keys).to include "wallet_name_p"
        expect(obj.keys).to include "wallet_name_e"
      end

      result.data["lead_generators"].each do |obj|
        expect(obj.keys).not_to include "name"

        expect(obj.keys).not_to include "name"
        expect(obj.keys).to include "name_p"
        expect(obj.keys).to include "name_e"
        expect(obj["name_e"]).to match(encrypted_message)

        expect(obj.keys).not_to include "surname"
        expect(obj.keys).not_to include "surname_p"
        expect(obj.keys).not_to include "surname_e"

        expect(obj.keys).not_to include "preferred_phone_number"
        expect(obj.keys).to include "preferred_phone_number_p"
        expect(obj.keys).to include "preferred_phone_number_e"
        expect(obj["preferred_phone_number_e"]).to match(encrypted_message)

        expect(obj["phone_numbers"]).to all(match(encrypted_message))
      end

      result.data["leads"].each do |obj|
        expect(obj.keys).not_to include "gps_latitude"
        expect(obj.keys).not_to include "gps_longitude"
        expect(obj.keys).to include "lat_p"
        expect(obj.keys).to include "lon_p"
        expect(obj.keys).to include "lat_e"
        expect(obj.keys).to include "lon_e"
        expect(obj.keys).to include "lat_b"
        expect(obj.keys).to include "lon_b"

        expect(obj.keys).not_to include "name"
        expect(obj.keys).to include "name_p"
        expect(obj.keys).to include "name_e"
        expect(obj["name_e"]).to match(encrypted_message)

        expect(obj.keys).not_to include "surname"
        expect(obj.keys).not_to include "surname_p"
        expect(obj.keys).not_to include "surname_e"

        expect(obj.keys).not_to include "preferred_phone_number"
        expect(obj.keys).to include "preferred_phone_number_p"
        expect(obj.keys).to include "preferred_phone_number_e"
        expect(obj["preferred_phone_number_e"]).to match(encrypted_message)

        expect(obj["phone_numbers"]).to all(match(encrypted_message))
      end
    end
  end
end
