# frozen_string_literal: true

require "rails_helper"

describe Source::Api::Supamoto do
  let(:source) { create :source, :supamoto }

  describe "stubbed requests" do
    before do
      responses = {
        stoves: {
          method: :get,
          response: {
            "content" => [
              {
                "deviceId" => 332_419_882,
                "model" => "MIMIMOTO",
                "status" => "ACTIVE",
                "country" => "MW",
                "latitude" => 23.493727,
                "longitude" => -49.282822,
                "certificateCid" => "bafkreidg212salalxzowqpooowe5yt7igfpo12p",
                "registrationDateTime" => "2022-09-13T10:15:30",
                "nftCollectionId" => 1,
                "nftStartDate" => "2022-08-17T02:50:17",
                "customer" => {
                  "agreementPolicy" => "PRIVATE",
                  "profilePictureUrl" => "https://cdn.supamoto.app/customers/example.jpg",
                  "firstName" => "Joanna",
                  "did" => "user-did"
                }
              }
            ],
            "totalPages" => 3,
            "totalElements" => 30,
            "hasNextPage" => true,
            "hasPreviousPage" => false
          }
        },
        orders: {
          method: :post,
          response: {
            "content" => [
              {
                "transactionId" => "SM231017.153626.TbcbVFL9RAQ",
                "amount" => 120.00,
                "currency" => "ZMW",
                "orderType" => "CONTRACT",
                "deviceId" => 332_419_882,
                "dateTime" => "2023-10-17T15:36:26.912618"
              },
              {
                "transactionId" => "SM231017.153746.Muly5FldSaQ",
                "amount" => 130.00,
                "currency" => "ZMW",
                "pelletsAmount" => 30,
                "pelletsAmountUnits" => "kg",
                "orderType" => "PELLETS",
                "deviceId" => 332_419_882,
                "dateTime" => "2023-10-17T15:37:46.042136"
              }
            ],
            "totalPages" => 3,
            "totalElements" => 30,
            "hasNextPage" => true,
            "hasPreviousPage" => false
          }
        },
        payments: {
          method: :post,
          response: {
            "content" => [
              {
                "transactionId" => "MP220804.0000.D00000",
                "amount" => 250.00,
                "currency" => "ZMW",
                "serviceProvider" => "airtel",
                "deviceId" => 332_419_882,
                "dateTime" => "2023-10-16T15:18:00"
              }
            ],
            "totalPelletsAmount" => 100,
            "totalPages" => 3,
            "totalElements" => 30,
            "hasNextPage" => true,
            "hasPreviousPage" => false
          }
        }
      }

      stub_reqs = described_class::ENDPOINTS.map do |key, path|
        resp = responses[key]
        last_page = resp[:response].merge({ "content" => [] })
        stub_request(resp[:method], "#{described_class::URL}#{path}").with(query: hash_including({})). # any query...
          to_return(status: 200, body: resp[:response].to_json, headers: { content_type: "application/json" }).then.
          to_return(status: 200, body: last_page.to_json, headers: { content_type: "application/json" })
      end
      stub_reqs
    end

    let(:result_data) do
      result = source.adapter.retrieve_data
      result.data
    end

    described_class::ENDPOINTS.each_key do |key|
      it "fetched data for #{key}" do
        expect(result_data[key].pluck("deviceId").uniq).to eq [332_419_882]
      end
    end

    it "has proper data" do
      expect(result_data[:stoves].first["model"]).to eq "MIMIMOTO"
      expect(result_data[:orders].first["transactionId"]).to eq "SM231017.153626.TbcbVFL9RAQ"
      expect(result_data[:payments].first["transactionId"]).to eq "MP220804.0000.D00000"
    end

    it "cleans customer data" do
      stove = result_data[:stoves].first

      expect(stove.keys).not_to include(*%w[latitude longitude])
      expect(stove.keys).to include(*%w[lat_b lat_p lat_e lon_b lon_p lon_e])

      stove_cust = stove["customer"]
      expect(stove_cust.keys).not_to include "firstName"
      expect(stove_cust.keys).to include(*%w[firstName_e firstName_p])
    end

    describe "#from_to" do
      it "leaves proper import hint" do
        result = source.adapter.retrieve_data
        expect(result.hint).to eq 2.days.ago.to_date.to_s
      end

      it "has nice default range" do
        f, t = source.adapter.from_to nil
        expect(f).to eq Date.parse("2023-08-01")
        expect(t).to eq 2.days.ago.to_date
      end

      it "get the past till almost now" do
        f, t = source.adapter.from_to 6.months.ago.to_date.to_s
        expect(f).to eq (6.months.ago - 2.days).to_date
        expect(t).to eq 2.days.ago.to_date
      end

      it "always adds an overlap" do
        f, t = source.adapter.from_to 3.days.ago.to_date.to_s
        expect(f).to eq 5.days.ago.to_date
        expect(t).to eq 2.days.ago.to_date
      end

      it "will fail if it would calculate a bad date range" do
        result = source.adapter.retrieve_data(Time.zone.today.to_s)
        expect(result.error).to match(/Nothing to fetch in this date range/)
      end
    end
  end

  describe "exception handling" do
    before do
      allow(source.adapter).to receive(:fetch_stoves).and_raise("ouch")
    end

    it "raises in normal mode" do
      expect { source.adapter.retrieve_data }.to raise_error "ouch"
    end

    it "just tells about the error when validating" do
      r = source.adapter.validation_result
      expect(r).not_to be_ok
      expect(r.error).to eq "Fetching Stoves: Failed with error: ouch"
    end
  end

  it "pagination_works" do
    results = [3, 2, 1, 0]
    data = source.adapter.paginated do |page_query|
      r = results.pop
      expect(page_query).to eq "page=#{r}&pageSize=#{described_class::PAGE_SIZE}"
      r < 3 ? [r] : []
    end
    expect(data).to eq [0, 1, 2]
  end

  describe "metadata and source integration" do
    it "is valid source" do
      expect(source).to be_valid
    end

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "api/supamoto"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "supamoto"
    end
  end

  describe "validation" do
    it "has empty #validation_hint" do
      expect(source.adapter.validation_hint).to be_blank
    end

    it "gives a Result when doing validation" do
      expect(source.adapter.validation_result).to be_a Source::Adapter::Result
    end
  end
end
