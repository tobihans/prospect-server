# frozen_string_literal: true

require "rails_helper"

describe Source::Api::StellarMultiOrgChild do
  describe "Mocked requests" do
    describe "UI" do
      let(:source) do
        described_class.new Source.new
      end

      it "validation_hint" do
        expect(source.validation_hint).to match(/Found/)
      end

      it "validation_result success" do
        expect(source.validation_result.protocol).to match(/SUCCESS/)
      end

      it "retrieve_data" do
        s = described_class.new source
        expect { s.retrieve_data }.to raise_error(RuntimeError)
      end
    end

    describe "metadata and source integration" do
      let(:source) { create :source, kind: "api/stellar_multi_org_child" }

      it "has correct class for source" do
        expect(source.adapter.class).to eq described_class
      end

      it "generates correct data_schema" do
        expect(source.data_schema).to eq "api/stellar_v2"
      end

      it "generates correct data_origin" do
        expect(source.data_origin).to eq "stellar"
      end
    end
  end
end
