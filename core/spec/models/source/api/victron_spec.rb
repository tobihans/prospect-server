# frozen_string_literal: true

require "rails_helper"
require "timecop"

describe Source::Api::Victron do
  describe "Mocked requests" do
    before do
      user_id = 12_345
      response_auth_login =
        { "token" => "aLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONGSTriiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiing",
          "idUser" => user_id,
          "verification_mode" => "password",
          "verification_sent" => false }

      inst_id = 12_123
      response_installations = {
        "success" => true,
        "records" =>
        [
          { "idSite" => inst_id,
            "accessLevel" => 0,
            "owner" => true,
            "is_admin" => false,
            "name" => "Max Musterman",
            "identifier" => "1234b8f7f44b",
            "idUser" => 123_123,
            "pvMax" => 0,
            "timezone" => "Africa/Lusaka",
            "phonenumber" => "+2504535433543",
            "notes" => nil,
            "geofence" => nil,
            "geofenceEnabled" => false,
            "realtimeUpdates" => true,
            "hasMains" => 0,
            "hasGenerator" => 0,
            "noDataAlarmTimeout" => nil,
            "alarmMonitoring" => 1,
            "invalidVRMAuthTokenUsedInLogRequest" => 0,
            "syscreated" => 1_583_408_113,
            "grafanaEnabled" => 0,
            "isPaygo" => 0,
            "paygoCurrency" => nil,
            "paygoTotalAmount" => nil,
            "inverterChargerControl" => 0,
            "shared" => false,
            "device_icon" => "battery",
            extended: [
              {
                idDataAttribute: 4,
                code: "lt",
                description: "Latitude",
                formatWithUnit: "%.5F LAT",
                dataType: "float",
                idDeviceType: 0,
                textValue: nil,
                instance: "0",
                timestamp: nil,
                dbusServiceType: "gps",
                dbusPath: "/Position/Latitude",
                rawValue: "-10.978",
                formattedValue: "-10.97800 LAT"
              },
              {
                idDataAttribute: 8,
                code: "ll",
                description: "Random",
                formatWithUnit: "%.5F OOO",
                dataType: "float",
                idDeviceType: 0,
                textValue: nil,
                instance: "0",
                timestamp: nil,
                dbusServiceType: "any",
                dbusPath: "/ANYTHING",
                rawValue: "7.6",
                formattedValue: "7,6 OOO"
              },
              {
                idDataAttribute: 4,
                code: "lt",
                description: "Longitude",
                formatWithUnit: "%.5F LON",
                dataType: "float",
                idDeviceType: 0,
                textValue: nil,
                instance: "0",
                timestamp: nil,
                dbusServiceType: "gps",
                dbusPath: "/Position/Longitude",
                rawValue: "-2.978",
                formattedValue: "-2.97800 LON"
              }
            ] }

        ]
      }
      response_stats = { "success" => true,
                         "records" =>
        { "Bc" =>
          [[1_673_280_808_000, 0.473297119140625],
           [1_673_363_608_000, 0.104034423828125]],
          "Pc" =>
          [[1_673_320_408_000, 0.010009765625],
           [1_673_363_608_000, 0.13262939453125]],
          "Pb" =>
          [[1_673_324_008_000, 0.0772705078125],
           [1_673_363_608_000, 0.00726318359375]],
          "bs" =>
          [[1_673_280_808_000, 99.07142857142857, 98.5, 99.5],
           [1_673_363_608_000, 99.625, 99.5, 100]] },
                         "totals" => { "Bc" => 6.034210205078125, "Pc" => 2.6092529296875, "Pb" => 8.6707763671875, "bs" => 2178.567857142857 } }

      Timecop.freeze(Time.zone.at(1_675_729_062)) # we need to mock api calls with timestamps in the URL

      stub_request(:post, "#{described_class::URL}auth/login").
        to_return(status: 200, body: response_auth_login.to_json, headers: { content_type: "application/json" })

      stub_request(:get, "#{described_class::URL}users/#{user_id}/installations?extended=1   ").
        to_return(status: 200, body: response_installations.to_json, headers: { content_type: "application/json" })

      stats_parameters = "?attributeCodes%5B%5D=Bc&attributeCodes%5B%5D=Pb&attributeCodes%5B%5D=Pc&attributeCodes%5B%5D=bs&end=1675728900&interval=15mins&start=1675642500&type=custom"
      stub_request(:get, "#{described_class::URL}installations/#{inst_id}/stats#{stats_parameters}").
        to_return(status: 200, body: response_stats.to_json, headers: { content_type: "application/json" })
    end

    after do
      Timecop.return
    end

    describe "api calls" do
      let(:response) do
        s = described_class.new create :source, :victron
        data, _more_data, _hint = s.fetch(described_class::URL, nil)
        JSON.parse data
      end

      it "returns correctly some data" do
        expect(response).to have_key("success")
      end

      it "p14n fields" do
        expect(response["records"].first).not_to have_key("phonenumber")
        expect(response["records"].first).to have_key("phonenumber_p")
        expect(response["records"].first).to have_key("phonenumber_e")
        expect(response["records"].first).not_to have_key("latitude")
        expect(response["records"].first).to have_key("lat_p")
        expect(response["records"].first).to have_key("lat_e")
        expect(response["records"].first).to have_key("lat_b")
        expect(response["records"].first).not_to have_key("longitude")
        expect(response["records"].first).to have_key("lon_p")
        expect(response["records"].first).to have_key("lon_b")
        expect(response["records"].first).to have_key("lon_e")
      end

      it "blurs geo fields" do
        expect(response["records"].first["lat_b"]).to eq(-10.978)
        expect(response["records"].first["lon_b"]).to be(-2.978)
      end

      it "encrypts fields" do
        expect(response["records"].first).to have_key("phonenumber_e")
        expect(response["records"].first["phonenumber_e"]).not_to be_empty
        expect(response["records"].first["lat_e"]).not_to be_empty
        expect(response["records"].first["lon_e"]).not_to be_empty
      end

      it "has no Latitude or Longitude key in extended anymore" do
        expect(response["records"].first).to have_key("extended")
        response["records"].each do |record|
          record.each do |key, val|
            next unless key == "extended"

            val.each do |k|
              expect(k["description"]).not_to match(/Latitude/)
              expect(k["description"]).not_to match(/Longitude/)
              expect(k["description"]).to match(/Random/)
            end
          end
        end
      end
    end

    it "fails to login" do
      stub_request(:post, "#{described_class::URL}auth/login").
        to_return(status: 401, body: { success: false, errors: "Token is invalid", error_code: "invalid_token" }.to_json, headers: { content_type: "application/json" })

      s = described_class.new create :source, :victron

      expect(s.validation_result.protocol).to match(/Failed to login/)
    end

    it "correct data for skipped import" do
      now_timestamp = Time.zone.now.to_i
      s = described_class.new create :source, :victron
      data, more_data, hint = s.fetch(described_class::URL, now_timestamp)

      expect(data).to be_nil
      expect(more_data).to be false
      expect(hint).to eq(now_timestamp)
    end

    describe "UI" do
      let(:source) do
        create :source, :victron
      end

      it "validation_result success" do
        s = described_class.new source
        expect(s.validation_result.protocol).to match(/SUCCESS/)
      end

      # there is currently no way this fails because of UI input validation

      it "retrieve_data" do
        s = described_class.new source
        expect(s.retrieve_data.format).to be :json
      end
    end

    it "floor_15min" do
      expecteds = {
        "2022-02-02 17:14:59" => "2022-02-02 17:00:00",
        "2022-02-02 17:16:30" => "2022-02-02 17:15:00",
        "2022-02-02 17:31:59" => "2022-02-02 17:30:00",
        "2022-02-02 17:46:59" => "2022-02-02 17:45:00",

        "2022-02-02 17:15:00" => "2022-02-02 17:15:00" # alread floored
      }

      expecteds.each do |orig, floored|
        expect(described_class.floor_15min(Time.zone.parse(orig))).to eq Time.zone.parse(floored)
      end
    end

    describe "metadata and source integration" do
      let(:source) { create :source, :victron }

      it "has correct class for source" do
        expect(source.adapter.class).to eq described_class
      end

      it "generates correct data_schema" do
        expect(source.data_schema).to eq "api/victron_grids"
      end

      it "generates correct data_origin" do
        expect(source.data_origin).to eq "victron"
      end

      it "has the right data_category" do
        expect(source.data_category).to eq "grids"
      end

      it "generates a valid source" do
        expect(source).to be_valid
      end
    end

    describe "data_category decides which kind of data_schema adapter has" do
      it "works for shs" do
        s = create :source, kind: "api/victron", data_category: "shs"
        expect(s.data_schema).to eq "api/victron_shs"
      end

      it "works for grids" do
        s = create :source, kind: "api/victron", data_category: "grids"
        expect(s.data_schema).to eq "api/victron_grids"
      end

      it "fails for a different data_category" do
        expect do
          create :source, kind: "api/victron", data_category: "meters"
        end.to raise_error(/Validation failed: Data category must be in grids, shs/)
      end

      it "fails for unspecified data_category" do
        expect do
          source = create :source, kind: "api/victron"
          source.data_category = nil # factory sets data_category, we need to unset it...
          source.save!
        end.to raise_error(/Validation failed: Data category can't be blank, Data category/)
      end
    end
  end
end
