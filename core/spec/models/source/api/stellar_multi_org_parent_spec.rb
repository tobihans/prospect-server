# frozen_string_literal: true

require "rails_helper"

describe Source::Api::StellarMultiOrgParent do
  #
  # StellarMultiOrgParent inherits from Stellar, so we test only the overwritten behaviour
  #
  describe "metadata and source integration" do
    let(:source) { create :source, kind: "api/stellar_multi_org_parent" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "api/stellar_multi_org_parent"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "stellar_multi_org_parent"
    end
  end

  describe "multi org parent specifics" do
    let(:meter_mapping) do
      create :org_device_mapper
    end

    let(:response) do
      {
        "status" => "success",
        "meta" => {
          "time" => "2023-03-06T15:15:05.665Z",
          "instance" => 644_058,
          "configUpdatedAt" => "2023-02-28T08:08:35.085Z",
          "count" => 1748
        },
        "data" =>
     [
       { "uid" => 123,
         "contactName" => "some name",
         "phoneNumber1" => "+2312345678912",
         "phoneNumber2" => "+2312345678913",
         "meterId" => 541_234,
         "connectionDate" => "2019-01-02",
         "application" => "generator",
         "latitude" => 9.12312333,
         "longitude" => 7.123453333,
         "siteType" => "office equipment",
         "market" => "wuse",
         "address" =>
         "block Z1 shop 1, some company name ltd, old market",
         "operatorId" => "a2ei",
         "country" => "NG",
         "timezone" => "Africa/Lagos",
         "powerSystemView" => { "regionId" => "nigeria" } },
       { "uid" => 123,
         "contactName" => "some name",
         "phoneNumber1" => "+2312345678912",
         "phoneNumber2" => "+2312345678913",
         "meterId" => meter_mapping.device_id }
     ]
      }
    end

    # we need an active child source for device mapping
    let(:source) { create :source, kind: "api/stellar_multi_org_child", organization: meter_mapping.organization }
    let(:parent) { described_class.new(create(:source, kind: "api/stellar_multi_org_parent")) }

    before do
      source # use it, because we need it for every test
    end

    it "create_imports generates one import" do
      expect do
        parent.create_imports response.to_json
      end.to change(Import, :count).by(1)
    end

    it "create_imports selects correct meter" do
      parent.create_imports response.to_json

      i = source.imports.first
      import_data = JSON.parse(i.file.download)["data"]
      expect(import_data.size).to eq 1
      expect(import_data.first["meterId"]).to eq(meter_mapping.device_id)
    end
  end
end
