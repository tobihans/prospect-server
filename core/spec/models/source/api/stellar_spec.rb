# frozen_string_literal: true

require "rails_helper"

describe Source::Api::Stellar do
  describe "Mocked requests" do
    before do
      uid = "0123"

      response_sites = {
        "status" => "success",
        "meta" => {
          "time" => "2023-03-06T15:15:05.665Z",
          "instance" => 644_058,
          "configUpdatedAt" => "2023-02-28T08:08:35.085Z",
          "count" => 1748
        },
        "data" =>
     [
       { "uid" => uid,
         "contactName" => "some name",
         "phoneNumber1" => "+2312345678912",
         "phoneNumber2" => "+2312345678913",
         "meterId" => 541_234,
         "connectionDate" => "2019-01-02",
         "application" => "generator",
         "latitude" => 9.12312333,
         "longitude" => 7.123453333,
         "siteType" => "office equipment",
         "market" => "wuse",
         "address" =>
         "block Z1 shop 1, some company name ltd, old market",
         "operatorId" => "a2ei",
         "country" => "NG",
         "timezone" => "Africa/Lagos",
         "powerSystemView" => { "regionId" => "nigeria" } }
     ]
      }

      response_stats =
        { "status" => "success",
          "meta" => { "time" => "2023-03-06T15:26:03.877Z" },
          "data" =>
           [{ "_id" => "6404baf3c2a262774c9a601d",
              "deviceId" => "a2ei_020_mtr",
              "time" => "2023-03-05T15:40:00.000Z",
              "createdOn" => "2023-03-05T15:53:23.669Z",
              "currentA" => 2.317,
              "frequency" => 47.81,
              "lifetimeEnergy" => 2737.17,
              "measurementTime" => "2023-03-05T15:40:00.000Z",
              "meteredPower" => 505.8,
              "meteredPowerA" => 505.8,
              "meteredVoltageA" => 226.7,
              "powerFactor" => 0.941,
              "powerFactorA" => 0.941,
              "signalStrength" => 21,
              "sourceCreatedAt" => "2023-03-05T15:47:55.118Z" }] }
      Timecop.freeze(Time.zone.at(1_675_729_062)) # we need to mock api calls with timestamps in the URL

      stub_request(:get, "#{described_class::URL}sites").
        to_return(status: 200, body: response_sites.to_json, headers: { content_type: "application/json" })

      # stats calls are done via typhoeus
      response_stats_t = Typhoeus::Response.new(code: 200, body: response_stats.to_json)

      # check VCR configuration in case you change the stubbing here
      Typhoeus.stub(/#{described_class::URL}/).and_return(response_stats_t)
    end

    after do
      Timecop.return
    end

    describe "api calls" do
      let(:response) do
        s = described_class.new create :source
        data, _more_data, _hint = s.fetch(nil, nil)
        JSON.parse data
      end

      it "returns correctly some data" do
        expect(response).to have_key("status")
        expect(response["status"]).to eq("success")
      end

      it "p14n fields" do
        expect(response["data"].first).not_to have_key("contactName")
        expect(response["data"].first).to have_key("contactName_p")

        expect(response["data"].first).not_to have_key("phoneNumber1")
        expect(response["data"].first).to have_key("phoneNumber1_p")

        expect(response["data"].first).not_to have_key("phoneNumber2")
        expect(response["data"].first).to have_key("phoneNumber2_p")

        expect(response["data"].first).not_to have_key("latitude")
        expect(response["data"].first).to have_key("lat_b")
        expect(response["data"].first).to have_key("lat_p")

        expect(response["data"].first).not_to have_key("longitude")
        expect(response["data"].first).to have_key("lon_b")
        expect(response["data"].first).to have_key("lon_p")

        expect(response["data"].first).not_to have_key("address")
        expect(response["data"].first).to have_key("address_p")
      end
    end

    it "timerange" do
      from, to, more_data = described_class.timerange nil

      expect(from).to eq 1.day.ago
      expect(to).to eq Time.zone.parse("2023-02-07 00:16:42")
      expect(more_data).to be false
    end

    it "timerange week" do
      five_weeks_ago = 5.weeks.ago
      from, to, more_data = described_class.timerange five_weeks_ago.to_s

      expect(from).to eq five_weeks_ago
      expect(to).to eq(five_weeks_ago + described_class::MAX_DATE_RANGE) # max 30 days per call
      expect(more_data).to be true # true because there is newer data we haven't pulled yet...
    end

    it "timerange receivedAfter time limit" do
      week_ago = 1.week.ago
      from, to, more_data = described_class.timerange week_ago.to_s

      expect(from).to eq week_ago
      expect(to).to eq(4.days.ago + 1.hour) # max 30 days per call
      expect(more_data).to be true # true because there is newer data we haven't pulled yet...
    end

    it "parse_jsonl" do
      parsed_lines = described_class.parse_jsonl "{}\n{\"a\":\"some_data\"}"

      expect(parsed_lines.size).to eq 2
      expect(parsed_lines[1]["a"]).to eq "some_data"
    end

    describe "UI" do
      let(:source) do
        s = Source.new
        s.secret = "some secret"
        s
      end

      it "validation_result success" do
        s = described_class.new source
        expect(s.validation_result.protocol).to match(/SUCCESS/)
      end

      it "validation_result error" do
        s = described_class.new create :source

        stub_request(:get, "#{described_class::URL}sites").
          to_return(status: 500, headers: { content_type: "application/json" })

        expect(s.validation_result.protocol).to match(/ERROR/)
      end

      it "retrieve_data" do
        s = described_class.new source
        result = s.retrieve_data
        expect(result.format).to be :json
        expect(result.hint).to eq "2023-02-07 00:16:42 UTC"
      end
    end

    describe "metadata and source integration" do
      let(:source) { create :source, kind: "api/stellar" }

      it "has correct class for source" do
        expect(source.adapter.class).to eq described_class
      end

      it "generates correct data_schema" do
        expect(source.data_schema).to eq "api/stellar_v2"
      end

      it "generates correct data_origin" do
        expect(source.data_origin).to eq "stellar"
      end
    end
  end
end
