# frozen_string_literal: true

require "rails_helper"

describe Source::CsvUpdate do
  let(:source) { create :source, kind: :manual_upload, data_category: :test, details: { file_format: "csv" } }
  let(:adapter) { described_class.new source }

  let(:csv) do
    <<~CSV
      int_column,bool_column,float_column,time_column,personal_column,enum_column,custom,uid,organization_id,source_organization_name,something_different
      5,false,5.67,2021-02-22 13:34:56 +0100,Personal stuff  ,b,"{"bla":4}",some_uid_to_forget,5,forget_me,keep_me
    CSV
  end

  describe "working" do
    let(:result) { adapter.process_data csv, data_category: :test }

    it "just works" do
      expect(result).to be_ok
    end

    it "gives right protocol" do
      expect(result.protocol).to include "Manual Update from CSV for data category test"
      expect(result.protocol).to include ">>> SUCCESS"
    end

    it "returns right result" do
      exp =
        [{ "bool_column" => false,
           "enum_column" => "b",
           "float_column" => 5.67,
           "int_column" => 5,
           "personal_column" => "Personal stuff",
           "time_column" => "2021-02-22 12:34:56 UTC",
           "name_column_e" => nil,
           "name_column_p" => nil,
           "something_different" => "keep_me" }]
      expect(result.data[:test]).to eq exp
    end
  end
end
