# frozen_string_literal: true

require "rails_helper"

describe Source::Yaka do
  describe "UI" do
    let(:source) do
      create :source,
             secret: "SECRET_KEY",
             data_category: "test",
             kind: "yaka",
             details: {
               "folder_id" => "SOME_FOLDER_ID",
               "file_format" => "json"
             }
    end
    let(:adapter) { source.adapter }

    let(:good_response) do
      <<~YAKACSV.strip
        "POS ID","POS Name","OPERATOR_NAME","Tariff Program","Order No","Vendor_TransactionID","Transaction Date Time","Customer No","Customer Name","Region","District","Tax Excempt","Meter No","TOKEN","Cash Tendered","Refunds Amount","Arrears Amount","Service Charge","Additional Charge","Fuel Adjustment","Inflation Adjustment","Forex Adjustment","Lifeline Unit","Above Lifeline Unit","Cost of Lifeline Unit","Cost of Above Lifeline Unit","Cost of Energy","VAT","Energy","Account Pay Amount","Account Save Amount"
        "12345",Uganda,Uganda,domestic,"0000000123456789","22123455667",2022-11-01 01:01:00.000,"2012345","Musterman,",KAMPALA_EAST,NAALYA,NO,"123455678","01111111111111111111",10000,0,"0",3360,0,0,0,0,15,64.9,3750,52244.5,55994.5,10683.81,79.9,"46.5","8.19"
        "12345",Uganda,Uganda,domestic,"0000000234567891","22243243434",2022-11-01 01:01:00.000,"2012345",Max Musterman,KAMPALA_WEST,NAKULABYE,NO,"12345678","62222222222222222222",10000,0,"0",3360,0,0,0,0,15,12.2,3750,9821,13571,3047.58,27.2,"26.58","48"
      YAKACSV
    end

    before do
      allow(adapter).to receive(:find_latest_files) {
        [Google::Apis::DriveV3::File.new(name: "a", id: "1", modified_time: "2022-01-01")]
      }
      allow(adapter).to receive(:download_file) do |_file_id, data_param|
        data_param.write(good_response)
      end
      allow(adapter).to receive_messages(drive_service: nil, check_access_to_folder: true)

      ENV["GOOGLE_DRIVE_CREDENTIAL_FILE"] = "just_point_away_from_any_real_file"
    end

    it "validation_result success" do
      x = adapter.validation_result.protocol

      expect(x).to include ">>> SUCCESS"
      expect(x).not_to match(/error/i)
    end

    it "fetch" do
      data, more_data, hint = adapter.fetch nil

      expect(more_data).to be_falsy
      expect(hint).to eq("2022-01-01;a")

      data = data["data"]
      expect(data.size).to eq 2

      # one data field content
      expect(data.first["Customer No"]).to eq "2012345"

      # p14n field
      expect(data.first).to have_key("Customer Name_p")
      expect(data.first).not_to have_key("Customer Name")
    end

    it "validation_result error - bad data" do
      allow(adapter).to receive(:download_file) do |_file_id, data_param|
        data_param.write("this is no parsable CSV")
      end

      x = adapter.validation_result.protocol

      expect(x).to include "Didn't find any data rows"
      expect(x).not_to match(/success/i)
      expect(x).to include ">>> ERROR"
    end
  end

  describe "metadata and source integration" do
    let(:source) { create :source, kind: "yaka", data_category: "shs" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "yaka"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "yaka"
    end

    it "adapter_constraints" do
      # we dont't allow two sources with the same folder_id
      create :source, kind: "yaka", data_category: "shs", details: { folder_id: "MY_FOLDER" }

      expect do
        create :source, kind: "yaka", details: { folder_id: "MY_FOLDER" }
      end.to raise_error ActiveRecord::RecordInvalid
    end
  end
end
