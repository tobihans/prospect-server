# frozen_string_literal: true

require "rails_helper"

describe Source::Sunking do
  #
  # Sunking inherits from S3, so we test only the overwritten behaviour
  #
  describe "metadata and source integration" do
    let(:source) { create :source, kind: "sunking" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "sunking"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "sunking"
    end
  end

  describe "Mocked requests" do
    before do
      # The AWS stubbing provides empty default responses,
      # we just overwrite the ones we need.
      Aws.config[:s3] = {
        stub_responses: {
          # list_buckets: { buckets: [{name: 'my-bucket' }] },
          list_objects_v2: { contents: [
            { key: "Energy_Explorer_Energy_Explorer_Gsm_Api_Data_2024-06-10T0900_ZDkWxyy.csv" },
            { key: "Energy_Explorer_Energy_Explorer_Gsm_Api_Data_2024-06-11T0900_ZDkWxyy.csv" }
          ] }
        }
      }
    end

    describe "api calls" do
      let(:response) do
        source = create :source
        adapter = described_class.new source
        data, _more_data, _hint = adapter.fetch(nil)
        data
      end

      it "stubbed object download" do
        expect(response).to be_nil
      end
    end

    describe "UI" do
      let(:source) do
        create :source,
               secret: "SECRET_KEY",
               kind: "sunking",
               details: {
                 "access_key" => "SOME_ACCESS_KEY",
                 "region" => "us-east-1",
                 "bucket_name" => "my_bucket"
               }
      end

      let(:adapter) { source.adapter }
      let(:good_response) do
        <<~FIRST_LINES
          Battery ID,Data Interval,Version,Pv Voltage,Pv Current,Ac In Voltage,Ac Load Watt,Fuel Ratio,Energy Charged,Energy Discharged,Timestamp Time
          142806611,30,B01F0504,0,0,0,0,6.7,0,1,2024-06-09 20:19:38
          142806611,30,B01F0504,0,0,0,0,6.7,0,0,2024-06-09 19:49:38
        FIRST_LINES
      end

      it "validation_result success" do
        Aws.config[:s3][:stub_responses][:get_object] = { body: good_response }
        x = adapter.validation_result.protocol

        expect(x).to include ">>> SUCCESS"
        expect(x).not_to match(/error/i)
      end

      it "retriev_data, fetch" do
        Aws.config[:s3][:stub_responses][:get_object] = { body: good_response }
        result = adapter.retrieve_data

        expect(result.format).to be :json
        expect(result.more_data).to be true
        expect(result.hint).to eq "Energy_Explorer_Energy_Explorer_Gsm_Api_Data_2024-06-10T0900_ZDkWxyy.csv"
        expect(result.data).to be_present

        expect(result.data.size).to be 2
        expect(result.data.first).to have_key "Battery ID"
        expect(result.data.first["Battery ID"]).to eq "142806611"
      end

      it "validation_result error - bad region" do
        source.details["region"] = "unknown_region"
        x = adapter.validation_result.protocol

        expect(x).not_to match(/success/i)
        expect(x).to match(/error/i) # AWS reports non existing regions
        expect(x).to match(/Not every service is available in every region/) # AWS reports non existing regions
      end

      it "validation_result error - no file" do
        Aws.config[:s3][:stub_responses][:list_objects_v2] = {}
        x = adapter.validation_result.protocol

        expect(x).to match(/success/i)
        expect(x).to match(/0 object/)
      end
    end
  end
end
