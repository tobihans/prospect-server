# frozen_string_literal: true

require "rails_helper"

describe Source::RecordedUpload::AirtelZm do
  describe "UI" do
    let(:source) do
      create :source,
             secret: nil,
             kind: "recorded_upload/airtel_zm",
             details: {
               "email" => "some@em.ail",
               "frequency" => "weekly"
             }
    end

    let(:adapter) { source.adapter }

    let(:raw_csv) do
      <<~CSV
        	,,,Customer_Transaction_Report
        	Selection Criteria :MFS Provider: Airtel
        From Date :Jan 08  2024
        To Date :Feb 06  2024
        Record No,Transaction ID,Reference No.,Transaction Date & Time,Payer MFS Provider,Payer Payment Instrument,Payer Wallet Type/Linked Bank,Payer Bank Account No/Mobile No,Payer User Name,Sender Grade,Payer Nick Name,Payer Mobile Number,Payer Category,Payee MFS Provider,Payee Payment Instrument,Payee Wallet Type/Linked Bank,Receiver Mobile Number,Payee Bank Account No/Mobile No,Receiver Category,Receiver Grade,Payee User Name,Payee Nick Name,Service Type,Transaction Status,Transaction Amount,Payer Previous Balance,Payer Post Balance,Payee Pre Balance,Payee Post Balance,Total Service Charge
        1,MP240108.0621.12345,C297050015,08-JAN-2024  06:21:01,Airtel,WALLET,Normal,977250739,SOME NAME,Gold Subscriber,,977250739,Subscriber,Airtel,WALLET,Normal,889073368,889073368,Head Merchant,2PercentMerchant,EMERGING COOKING SOLUTIONS,XXXXMOTO,Merchant Payment,Transaction Success,130,NA,NA,.25,130.25,1
        2,MP240108.0635.12345,C301250015,08-JAN-2024  06:35:38,Airtel,WALLET,Normal,974353581,another name,Gold Subscriber,XXXXX3581,974353581,Subscriber,Airtel,WALLET,Normal,889073368,889073368,Head Merchant,2PercentMerchant,EMERGING COOKING SOLUTIONS,XXXXMOTO,Merchant Payment,Transaction Success,130,NA,NA,130.25,260.25,1
        2104,MP240131.1248.12345git288620016,31-JAN-2024  12:48:21,Airtel,WALLET,Normal,979791678,JOHN BANDA,Gold Subscriber,XXXXXXXX1678,979791678,Subscriber,Airtel,WALLET,Normal,889073368,889073368,Head Merchant,2PercentMerchant,EMERGING COOKING SOLUTIONS,XXXXMOTO,Merchant Payment,Transaction Success,130,NA,NA,10023.25,10153.25,1
        2503,MP240203.1633.12345,C,54321,03-FEB-2024  16:33:55,Airtel,WALLET,Normal,979791678,JOHN BANDA,Gold Subscriber,XXXXXXXX1678,979791678,Subscriber,Airtel,WALLET,Normal,889073368,889073368,Head Merchant,2PercentMerchant,EMERGING COOKING SOLUTIONS,XXXXMOTO,Merchant Payment,Transaction Success,130,NA,NA,8717.25,8847.25,1
      CSV
    end

    it "validation works" do
      adapter.raw_data = raw_csv
      result = adapter.validation_result

      expect(result).to be_ok
      expect(result.protocol).to include ">>> SUCCESS"
      expect(result.protocol).not_to match(/error/i)
    end

    describe "recorded upload adapter" do
      it "#requires_recording?" do
        expect(adapter).to be_requires_recording
      end

      it "#download_url" do
        expect(adapter.download_url).to be_present
      end

      it "#download_hint" do
        expect(adapter.download_hint).to eq "Customer Transaction Report"
      end

      it "#from_hint when fresh goes into the past" do
        expect(adapter.from_hint).to be_between(5.months.ago, 2.weeks.ago)
      end

      it "#from_hint when for subsequent overlaps a bit" do
        allow(source).to receive(:last_imported_hint).and_return(1.week.ago.to_s)
        expect(adapter.from_hint).to be_between(2.weeks.ago, 1.week.ago)
      end

      it "#until_hint gives a bit of delay for payments correction" do
        expect(adapter.until_hint).to be_between(1.week.ago, 1.day.ago)
      end

      it "can build a #next_import_code" do
        code = adapter.next_import_code

        sid, iid = VerifiedCode.parse(code).split
        expect(sid).to eq source.id.to_s
        expect(iid).to eq 0.to_s
      end

      it "#notify! generates an email" do
        expect do
          adapter.notify!
        end.to have_enqueued_job.with "SourceMailer", "notify_recorded_upload", any_args
      end
    end

    describe "processing" do
      let(:result) { adapter.process_data raw_csv }
      let(:result_data) { result.data }

      it "processes without error" do
        expect(result).to be_ok
      end

      it "creates data in proper format" do
        expect(result_data.count).to eq 4
        expect(result_data.pluck("Record No")).to eq %w[1 2 2104 2503]
        expect(result_data.pluck("Total Service Charge")).to eq(%w[1] * 4)
      end

      it "cleans personal data" do
        original_keys = [
          "Payer Bank Account No/Mobile No",
          "Payer User Name",
          "Payer Nick Name",
          "Payer Mobile Number"
        ]

        result_data.each do |d|
          original_keys.each do |k|
            expect(d).not_to be_key(k)
            expect(d.key?("#{k}_p")).to be_present
            expect(d.key?("#{k}_e")).to be_present
          end
        end
      end

      it "manages missing or splitted payment reference" do
        expected = ["C297050015", "C301250015", nil, "C_54321"]
        expect(result_data.pluck("Reference No.")).to eq expected
      end
    end

    describe "properties" do
      it "has correct data_origin" do
        expect(adapter.data_origin).to eq "airtel_zm"
      end

      it "has correct data_schema" do
        expect(adapter.data_schema).to eq "airtel_zm_customer_transactions"
      end
    end
  end
end
