# frozen_string_literal: true

require "rails_helper"

describe Source::RecordedUpload::MtnZm do
  describe "UI" do
    let(:source) do
      create :source,
             secret: nil,
             kind: "recorded_upload/mtn_zm",
             details: {
               "email" => "some@em.ail",
               "frequency" => "weekly"
             }
    end

    let(:adapter) { source.adapter }

    let(:raw_csv) do
      <<~CSV
        "Id","External id","Date","Status","Type","Provider Category","Information","To message","From","From Name","From Handler Name","To","To Name","To Handler Name","Initiated By","On Behalf Of","Amount","Currency","External Amount","Currency","External FX Rate","External Service Provider","Fee","Currency","Discount","Currency","Promotion","Currency","Coupon","Currency","Balance","Currency"
        "1111","8d4c7e17-d356-4fee-990d-abcdefg","2023-09-30 16:42:41","Successful","Debit","2.00","","Merchant: Supamoto","FRI:260123456789/MSISDN","CUSTOMER NAME","CUSTOMER NAME","FRI:33445555/MM","Supamoto Solar Payment","Supamoto Solar Payment","","","130.00","ZMW","","","","","2.60","ZMW","","","","","","","",""
        "2222","a032a08b-8369-4099-8bbf-abcdefg","2023-09-30 14:12:53","Successful","Debit","2.00","","Merchant: Supamoto","FRI:260123456789/MSISDN","CUSTOMER NAME 2","CUSTOMER NAME 2","FRI:33445555/MM","Supamoto Solar Payment","Supamoto Solar Payment","","","130.00","ZMW","","","","","2.60","ZMW","","","","","","","",""
        "3333","3c0bf187-011a-4a88-9a94-abcdefg","2023-09-30 13:39:39","Successful","Debit","2.00","","Merchant: Supamoto","FRI:260123456789/MSISDN","Customer Name 3","Customer Name 3","FRI:33445555/MM","Supamoto Solar Payment","Supamoto Solar Payment","","","130.00","ZMW","","","","","2.60","ZMW","","","","","","","",""
        "4444","a9f8048b-124a-4dc4-9b93-abcdefg","2023-09-30 12:53:02","Successful","Debit","2.00","","Merchant: Supamoto","FRI:260123456789/MSISDN","customer name 4","customer name 4","FRI:33445555/MM","Supamoto Solar Payment","Supamoto Solar Payment","","","130.00","ZMW","","","","","2.60","ZMW","","","","","","","",""
      CSV
    end

    it "validation works" do
      adapter.raw_data = raw_csv
      result = adapter.validation_result

      expect(result).to be_ok
      expect(result.protocol).to include ">>> SUCCESS"
      expect(result.protocol).not_to match(/error/i)
    end

    describe "recorded upload adapter" do
      it "#requires_recording?" do
        expect(adapter).to be_requires_recording
      end

      it "#download_url" do
        expect(adapter.download_url).to be_present
      end

      it "#download_hint" do
        expect(adapter.download_hint).to eq "Payments Statement"
      end

      it "#from_hint when fresh goes into the past" do
        expect(adapter.from_hint).to be_between(5.months.ago, 2.weeks.ago)
      end

      it "#from_hint when for subsequent overlaps a bit" do
        allow(source).to receive(:last_imported_hint).and_return(1.week.ago.to_s)
        expect(adapter.from_hint).to be_between(2.weeks.ago, 1.week.ago)
      end

      it "#until_hint gives a bit of delay for payments correction" do
        expect(adapter.until_hint).to be_between(1.week.ago, 1.day.ago)
      end

      it "can build a #next_import_code" do
        code = adapter.next_import_code

        sid, iid = VerifiedCode.parse(code).split
        expect(sid).to eq source.id.to_s
        expect(iid).to eq 0.to_s
      end

      it "#notify! generates an email" do
        expect do
          adapter.notify!
        end.to have_enqueued_job.with "SourceMailer", "notify_recorded_upload", any_args
      end
    end

    describe "processing" do
      let(:result) { adapter.process_data raw_csv }
      let(:result_data) { result.data }

      it "processes without error" do
        expect(result).to be_ok
      end

      it "creates data in proper format" do
        expect(result_data.count).to eq 4
        expect(result_data.pluck("Id")).to eq %w[1111 2222 3333 4444]
        expect(result_data.pluck("Amount")).to eq(%w[130.00] * 4)
      end

      it "cleans personal data" do
        original_keys = [
          "From",
          "From Name",
          "From Handler Name"
        ]

        result_data.each do |d|
          original_keys.each do |k|
            expect(d).not_to be_key(k)
            expect(d.key?("#{k}_p")).to be_present
            expect(d.key?("#{k}_e")).to be_present
          end
        end
      end
    end

    describe "properties" do
      it "has correct data_origin" do
        expect(adapter.data_origin).to eq "mtn_zm"
      end

      it "has correct data_schema" do
        expect(adapter.data_schema).to eq "mtn_zm_payments_statement"
      end
    end
  end
end
