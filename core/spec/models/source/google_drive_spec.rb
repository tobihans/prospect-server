# frozen_string_literal: true

require "rails_helper"

describe Source::GoogleDrive do
  describe "UI" do
    let(:source) do
      create :source,
             secret: "SECRET_KEY",
             data_category: "test",
             kind: "google_drive",
             details: {
               "folder_id" => "SOME_FOLDER_ID",
               "file_format" => "json"
             }
    end
    let(:adapter) { source.adapter }
    let(:good_response) { [source.data_table.examples].to_json }

    before do
      allow(adapter).to receive(:find_latest_files) {
        [Google::Apis::DriveV3::File.new(name: "a", id: "1", modified_time: "")]
      }
      allow(adapter).to receive(:download_file) do |_file_id, data_param|
        data_param.write([source.data_table.examples].to_json)
      end
      allow(adapter).to receive_messages(drive_service: nil, check_access_to_folder: true)

      ENV["GOOGLE_DRIVE_CREDENTIAL_FILE"] = "just_point_away_from_any_real_file"
    end

    it "validation_result success" do
      x = adapter.validation_result.protocol

      expect(x).to include ">>> SUCCESS"
      expect(x).not_to match(/error/i)
    end

    it "validation_result error - no access to folder" do
      allow(adapter).to receive(:check_access_to_folder).and_return(false)
      x = adapter.validation_result.protocol

      expect(x).not_to match(/success/i)
      expect(x).to match(/error/i)
      expect(x).to match(/No read permission for folder_id/)
    end

    it "validation_result - no file" do
      allow(adapter).to receive(:find_latest_files).and_return([])

      x = adapter.validation_result.protocol

      expect(x).to match(/success/i)
      expect(x).to match(/0 new files/)
    end

    it "validation_result error - bad data" do
      allow(adapter).to receive(:download_file) do |_file_id, data_param|
        data_param.write([source.data_table.examples.without("float_column")].to_json)
      end

      x = adapter.validation_result.protocol

      expect(x).to include "  Line 1: float_column is required"
      expect(x).to match(/error/i)
      expect(x).not_to match(/success/i)
      expect(x).to include ">>> ERROR"
    end

    it "retrieve_data with continuation" do
      allow(adapter).to receive(:find_latest_files) {
        [
          Google::Apis::DriveV3::File.new(name: "a", id: "1", modified_time: DateTime.parse("2022-01-01T01:00:00")),
          Google::Apis::DriveV3::File.new(name: "b", id: "1", modified_time: DateTime.parse("2022-01-02T01:00:00"))
        ]
      }

      result = adapter.retrieve_data

      expect(result.format).to be :json
      expect(result.more_data).to be true
      expect(result.hint).to eq "2022-01-01T01:00:00+00:00;a"
      expect(result.data).to be_present

      allow(adapter).to receive(:find_latest_files) {
        [
          Google::Apis::DriveV3::File.new(name: "b", id: "1", modified_time: DateTime.parse("2022-01-02T01:00:00"))
        ]
      }

      result = adapter.retrieve_data "2022-01-01T01:00:00;some_file"

      expect(result.format).to be :json
      expect(result.more_data).to be false
      expect(result.hint).to eq "2022-01-02T01:00:00+00:00;b"
      expect(result.data).to be_present
    end

    it "find_files_wrapped" do
      allow(adapter).to receive(:find_latest_files) {
        [
          # same modified date!
          Google::Apis::DriveV3::File.new(name: "aa", id: "1", modified_time: DateTime.parse("2022-01-01T01:10:00")),
          Google::Apis::DriveV3::File.new(name: "bb", id: "1", modified_time: DateTime.parse("2022-01-01T01:10:00"))
        ]
      }

      files = adapter.find_files_wrapped "2022-01-01T01:10:00;a"
      expect(files.count).to eq 2
      expect(files.first.name).to eq "aa"

      files = adapter.find_files_wrapped "2022-01-01T01:10:00;aa"
      expect(files.count).to eq 1
      expect(files.first.name).to eq "bb"

      files = adapter.find_files_wrapped "2022-01-01T01:10:00;bb"
      expect(files.count).to eq 0

      files = adapter.find_files_wrapped nil
      expect(files.count).to eq 2
    end

    it "find_files_wrapped unparsable" do
      expect do
        adapter.find_files_wrapped "2022-01-01XXX"
      end.to raise_error RuntimeError
    end

    describe "API" do
      it "service_account_email" do
        email = "someone@example.com"
        allow(File).to receive(:read).with(ENV.fetch("GOOGLE_DRIVE_CREDENTIAL_FILE")) {
          { client_email: email }.to_json
        }

        expect(described_class.service_account_email).to eq email
      end

      it "check_access_to_folder" do
        drive_service = instance_double(Google::Apis::DriveV3::DriveService)
        allow(drive_service).to receive(:list_permissions) {
          Google::Apis::DriveV3::PermissionList.new(permissions:
          [Google::Apis::DriveV3::Permission.new(role: "reader")])
        }

        allow(adapter).to receive(:drive_service) {
          drive_service
        }

        # unstub
        allow(adapter).to receive(:check_access_to_folder).and_call_original

        expect(adapter.check_access_to_folder("some_folder_id")).to be true
      end
    end
  end

  describe "metadata and source integration" do
    let(:source) { create :source, kind: "google_drive", data_category: "shs" }

    it "has correct class for source" do
      expect(source.adapter.class).to eq described_class
    end

    it "generates correct data_schema" do
      expect(source.data_schema).to eq "generic"
    end

    it "generates correct data_origin" do
      expect(source.data_origin).to eq "generic"
    end

    it "adapter_constraints" do
      # we dont't allow two sources with the same folder_id
      create :source, kind: "google_drive", details: { folder_id: "MY_FOLDER" }

      expect do
        create :source, kind: "google_drive", details: { folder_id: "MY_FOLDER" }
      end.to raise_error ActiveRecord::RecordInvalid
    end
  end
end
