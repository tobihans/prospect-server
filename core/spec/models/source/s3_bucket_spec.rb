# frozen_string_literal: true

require "rails_helper"

describe Source::S3Bucket do
  describe "Mocked requests" do
    before do
      # The AWS stubbing provides empty default responses,
      # we just overwrite the ones we need.
      Aws.config[:s3] = {
        stub_responses: {
          # list_buckets: { buckets: [{name: 'my-bucket' }] },
          list_objects_v2: { contents: [{ key: "an_object_key" }, { key: "b_an_object_key" }] }
        }
      }
    end

    describe "api calls" do
      let(:response) do
        source = create :source
        adapter = described_class.new source
        data, _more_data, _hint = adapter.fetch(nil)
        data
      end

      it "stubbed object download" do
        expect(response).to be_nil
      end
    end

    describe "UI" do
      let(:source) do
        create :source,
               secret: "SECRET_KEY",
               data_category: "test",
               kind: "s3_bucket",
               details: {
                 "access_key" => "SOME_ACCESS_KEY",
                 "region" => "us-east-1",
                 "bucket_name" => "my_bucket",
                 "file_format" => "json",
                 "order_by" => "lexicographical"
               }
      end
      let(:adapter) { source.adapter }
      let(:good_response) { [source.data_table.examples].to_json }

      it "validation_result success" do
        Aws.config[:s3][:stub_responses][:get_object] = { body: good_response }
        x = adapter.validation_result.protocol

        expect(x).to include ">>> SUCCESS"
        expect(x).not_to match(/error/i)
      end

      it "validation_result error - bad region" do
        source.details["region"] = "unknown_region"
        x = adapter.validation_result.protocol

        expect(x).not_to match(/success/i)
        expect(x).to match(/error/i) # AWS reports non existing regions
        expect(x).to match(/Not every service is available in every region/) # AWS reports non existing regions
      end

      it "validation_result error - no file" do
        Aws.config[:s3][:stub_responses][:list_objects_v2] = {}
        x = adapter.validation_result.protocol

        expect(x).to match(/success/i)
        expect(x).to match(/0 object/)
      end

      it "validation_result error - bad data" do
        Aws.config[:s3][:stub_responses][:get_object] =
          { body: [source.data_table.examples.without("float_column")].to_json }
        x = adapter.validation_result.protocol

        expect(x).to include "  Line 1: float_column is required"
        expect(x).to match(/error/i)
        expect(x).not_to match(/success/i)
        expect(x).to include ">>> ERROR"
      end

      it "validation_result error - bucket access" do
        Aws.config[:s3][:stub_responses][:head_bucket] = { status_code: 404, headers: {}, body: "" }
        x = adapter.validation_result.protocol

        expect(x).to include "Checking Bucket Access ... ERROR"
        expect(x).to include "  Aws::S3::Errors::NotFound"
        expect(x).to match(/error/i)
        expect(x).not_to match(/success/i)
        expect(x).to include ">>> ERROR"
      end

      it "retrieve_data with continuation" do
        Aws.config[:s3] = {
          stub_responses: {
            list_objects_v2: [{ contents: [{ key: "an_object_key" }, { key: "b_an_object_key" }] },
                              { contents: [{ key: "b_an_object_key" }] }],
            get_object: { body: [source.data_table.examples].to_json }
          }
        }

        result = adapter.retrieve_data

        expect(result.format).to be :json
        expect(result.more_data).to be true
        expect(result.hint).to eq "an_object_key"
        expect(result.data).to be_present

        result = adapter.retrieve_data "an_object_key"

        expect(result.format).to be :json
        expect(result.more_data).to be false
        expect(result.hint).to eq "b_an_object_key"
        expect(result.data).to be_present
      end

      it "test data processing" do
        Aws.config[:s3][:stub_responses][:get_object] = { body: good_response }
        data = adapter.retrieve_data.data

        expected_data = { "bool_column" => true,
                          "enum_column" => "b",
                          "float_column" => 3.14,
                          "int_column" => 5,
                          "name_column_p" => P14n.p("pater paul stored as fe42o3p2"),
                          "personal_column" => "Personal Text",
                          "time_column" => "2021-02-22 12:34:56 UTC" }
        expect(data.first["name_column_e"]).not_to be_nil
        data.first.delete("name_column_e")
        expect(data.first).to eq expected_data
      end
    end

    describe "metadata and source integration" do
      let(:source) { create :source, kind: "s3_bucket", data_category: "shs" }

      it "has correct class for source" do
        expect(source.adapter.class).to eq described_class
      end

      it "generates correct data_schema" do
        expect(source.data_schema).to eq "generic"
      end

      it "generates correct data_origin" do
        expect(source.data_origin).to eq "generic"
      end

      it "reset last_imported_hint when order_by changes" do
        source.details["order_by"] = "last_modified"
        source.save!
        i = Import.new last_imported_hint: "something", ingestion_finished_at: Time.zone.now
        i.source = source
        i.save!

        expect(source.last_imported_hint).to eq "something"

        # let's change the order_by
        source.details["order_by"] = "lexicographical"
        source.save! # this should reset the last_import_hint

        expect(source.last_imported_hint).to be_nil
      end
    end
  end

  describe "Source#filter_object_list" do
    let(:source) do
      create :source,
             secret: "SECRET_KEY",
             data_category: "test",
             kind: "s3_bucket",
             details: {
               "access_key" => "SOME_ACCESS_KEY",
               "region" => "us-east-1",
               "bucket_name" => "my_bucket",
               "file_format" => "json",
               "order_by" => "lexicographical",
               "filter" => nil
             }
    end

    let(:object_list) { [{ key: "some_random_key" }, { key: "folder/some_file.log" }] }

    it "empty filter" do
      filtered = source.adapter.filter_object_list object_list
      expect(filtered).to eq object_list
    end
  end

  describe "Source#trigger_import!" do
    let(:source) do
      create :source,
             secret: "SECRET_KEY",
             data_category: "test",
             kind: "s3_bucket",
             details: {
               "access_key" => "SOME_ACCESS_KEY",
               "region" => "us-east-1",
               "bucket_name" => "my_bucket",
               "file_format" => "json",
               "order_by" => "lexicographical"
             }
    end
    let(:response) { [source.data_table.examples].to_json }
    let(:another_response) { [source.data_table.examples.merge({ "float_column" => 42.22 })].to_json }
    let(:import!) { source.trigger_import! }

    describe "single file in bucket" do
      before do
        Aws.config[:s3] = {
          stub_responses: {
            list_objects_v2: { contents: [{ key: "an_object_key" }] },
            get_object: [{ body: response }]
          }
        }
      end

      it "just creates one import" do
        expect { import! }.to change(Import, :count).by 1
      end
    end

    describe "multiple files in bucket, lexicographical strategy" do
      before do
        Aws.config[:s3] = {
          stub_responses: {
            list_objects_v2: [{ contents: [{ key: "an_object_key" }, { key: "b_an_object_key" }] },
                              { contents: [{ key: "b_an_object_key" }] }],
            get_object: [{ body: response }, { body: another_response }]
          }
        }
      end

      it "creates two imports" do
        expect { import! }.to change(Import, :count).by 2
      end

      it "creates imports in right state" do
        import!
        expect(Import.last(2)).to all be_ingestion_finished
      end

      it "creates the right hints" do
        import!
        expect(Import.last(2).map(&:last_imported_hint)).to eq %w[an_object_key b_an_object_key]
      end

      it "sets all relevant fields" do
        import!
        expect(Import.last(2).map(&:protocol)).to all be_present
      end

      it "stores the right data" do
        import!
        stored = Import.last(2).map { |i| JSON.parse i.file.download }

        expect(stored.first.first["float_column"]).to eq 3.14
        expect(stored.second.first["float_column"]).to eq 42.22
      end
    end

    describe "multiple files in bucket, last_modified strategy" do
      before do
        source.details["order_by"] = "last_modified"
        time1 = Time.zone.parse("2020-01-01T01:01:01.000Z")
        time2 = time1 + 1.day
        Aws.config[:s3] = {
          stub_responses: {
            list_objects_v2: [{ contents: [{ key: "an_object_key", last_modified: time1 }, { key: "b_an_object_key", last_modified: time2 }] },
                              # in last_modified strategy all calls give the same
                              { contents: [{ key: "b_an_object_key", last_modified: time2 }, { key: "an_object_key", last_modified: time1 }] }],
            get_object: [{ body: response }, { body: another_response }]
          }
        }
      end

      it "creates two imports" do
        expect { import! }.to change(Import, :count).by 2
      end

      it "creates imports in right state" do
        import!
        expect(Import.last(2)).to all be_ingestion_finished
      end

      it "creates the right hints" do
        import!
        expect(Import.last(2).map(&:last_imported_hint)).to eq ["2020-01-01 01:01:01 UTC", "2020-01-02 01:01:01 UTC"]
      end

      it "sets all relevant fields" do
        import!
        expect(Import.last(2).map(&:protocol)).to all be_present
      end

      it "stores the right data" do
        import!
        stored = Import.last(2).map { |i| JSON.parse i.file.download }

        expect(stored.first.first["float_column"]).to eq 3.14
        expect(stored.second.first["float_column"]).to eq 42.22
      end
    end
  end
end
