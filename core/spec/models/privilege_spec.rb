# frozen_string_literal: true

# == Schema Information
#
# Table name: privileges
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  organization_user_id :bigint           not null
#  user_id              :bigint           not null
#
# Indexes
#
#  index_privileges_on_name                           (name)
#  index_privileges_on_organization_user_id           (organization_user_id)
#  index_privileges_on_organization_user_id_and_name  (organization_user_id,name) UNIQUE
#  index_privileges_on_user_id                        (user_id)
#
require "rails_helper"

describe Privilege do
  describe "documentation" do
    Privilege::PRIVILEGES.each do |p|
      it "translates #{p}" do
        expect(I18n.t("privileges.docu.#{p}").downcase).not_to include "translation missing"
      end
    end
  end

  it "no duplicates" do
    expect(Privilege::PRIVILEGES.uniq.length).to eq Privilege::PRIVILEGES.length
  end

  it "user privs are subset" do
    expect(Privilege::USER_PRIVILEGES.difference(Privilege::PRIVILEGES)).to be_empty
  end

  it "viewer privs are subset of user privs" do
    expect(Privilege::VIEWER_PRIVILEGES.difference(Privilege::USER_PRIVILEGES)).to be_empty
  end

  it "cleanup" do
    create :privilege, name: "non existent privilege"

    expect do
      described_class.cleanup_privileges
    end.to change(described_class, :count).by(-1)
  end

  it "match_templates USER exact" do
    r = described_class.match_templates(Privilege::USER_PRIVILEGES)

    expect(r[:template]).to eq("user")
    expect(r[:more_privileges]).to be false
  end

  it "match_templates USER plus extra priv" do
    r = described_class.match_templates(Privilege::USER_PRIVILEGES + ["user.manage"])

    expect(r[:template]).to eq("user")
    expect(r[:more_privileges]).to be true
  end

  it "match_templates no template match" do
    r = described_class.match_templates(["user.manage"])

    expect(r[:template]).to eq("no priv. template")
    expect(r[:more_privileges]).to be true
  end

  it "match_templates empty privs" do
    r = described_class.match_templates([])

    expect(r[:template]).to eq("no priv. template")
    expect(r[:more_privileges]).to be false
  end

  it "match_templates all privs" do
    r = described_class.match_templates(Privilege::PRIVILEGES)

    expect(r[:template]).to eq("manager")
    expect(r[:more_privileges]).to be false
  end
end
