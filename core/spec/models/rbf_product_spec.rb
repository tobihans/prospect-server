# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_products
#
#  id                    :bigint           not null, primary key
#  manufacturer          :string           not null
#  model                 :string           not null
#  rated_power_w         :integer
#  rbf_category          :string           not null
#  technology            :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  organization_id       :bigint           not null
#  rbf_claim_template_id :bigint           not null
#  user_id               :bigint
#
# Indexes
#
#  idx_on_model_organization_id_manufacturer_c8532bbe4b  (model,organization_id,manufacturer) UNIQUE
#  index_rbf_products_on_organization_id                 (organization_id)
#  index_rbf_products_on_rbf_claim_template_id           (rbf_claim_template_id)
#  index_rbf_products_on_user_id                         (user_id)
#

require "rails_helper"

describe RbfProduct do
  subject(:rbf_product) { create :rbf_product }

  describe "callbacks" do
    it "#clean_whitespace" do
      rbf_product.manufacturer = "   Gap Gap   "
      rbf_product.model = "   Mod Mod   "
      rbf_product.technology = "   "
      rbf_product.save
      expect(rbf_product.manufacturer).to eq "Gap Gap"
      expect(rbf_product.model).to eq "Mod Mod"
      expect(rbf_product.technology).to be_nil
    end
  end

  describe "validation of rbf category" do
    it "is valid from factory" do
      expect(rbf_product).to be_valid
    end

    it "lets unconfigured rbf_claim_template_check pass" do
      rbf_product.rbf_claim_template.trust_trace_check = "some unknown check"
      expect(rbf_product).to be_valid
    end

    it "complains about bad data category" do
      rbf_product.rbf_category = "C"
      expect(rbf_product).not_to be_valid
      expect(rbf_product.errors.to_a.first).to eq "Rbf category must be one of: A, B"
    end
  end
end
