# frozen_string_literal: true

require "rails_helper"

describe DynamicForm::Validator do
  describe "Rules" do
    let(:template) do
      {
        "tests" => {
          "fields" => {
            "test" => { "data_column" => "some_column" },
            "test2" => { "data_column" => "some_column" }
          }
        }
      }
    end

    it "checks for fields that should not be bigger than others" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["lesser_than:price"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "10,000", "price" => "10,000" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "10,000", "price" => "9,000" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/should not be bigger than/i)
    end

    it "checks max length" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["max:5"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "abc" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "abcdef" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/maximum length for/)
    end

    it "checks min length" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["min:5"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "abc" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/minimum length for/)

      result = validator.validate({ "test" => "abcdef" })
      expect(result.valid).to be true
    end

    it "checks maximum date" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["max_date:today"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => Time.zone.today.to_s })
      expect(result.valid).to be true

      result = validator.validate({ "test" => Date.tomorrow.to_s })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/must not be futher in the future than/)

      result = validator.validate({ "test" => "" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/invalid/)

      form["tests"]["fields"]["test"]["validation_rules"] = ["max_date:#{Time.zone.tomorrow}"]
      validator = described_class.new(form)
      result = validator.validate({ "test" => Time.zone.today.to_s })
      expect(result.valid).to be true
    end

    it "checks minimum date" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["min_date:2024-11-01"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "2024-11-05" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "2024-10-01" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/should not be earlier than/)

      result = validator.validate({ "test" => "" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/invalid/)
    end

    it "checks field required if other is set to specific value" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["required_if:test2=abc|xyz"]

      validator = described_class.new(form)
      result = validator.validate({ "test2" => "abc", "test" => "" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/is required if/)

      result = validator.validate({ "test2" => "xyz", "test" => "" })
      expect(result.valid).to be false

      result = validator.validate({ "test2" => "abc", "test" => "def" })
      expect(result.valid).to be true

      result = validator.validate({ "test2" => "xyz", "test" => "def" })
      expect(result.valid).to be true

      result = validator.validate({ "test2" => "afd", "test" => "def" })
      expect(result.valid).to be true

      result = validator.validate({ "test2" => "def" })
      expect(result.valid).to be true
    end

    it "checks for a required field" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["required"]
      validator = described_class.new(form)

      result = validator.validate({ "test" => "" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/is required/)

      result = validator.validate({ "test" => "abc" })
      expect(result.valid).to be true
    end

    it "checks regex on a field" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["regex:^[a-z]{3}[0-9]{2}[A-Z]{1}$"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "abc12X" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "X12C" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/does not match the required format/)
    end

    it "checks format of a field" do
      form = template.dup
      form["tests"]["fields"]["test"]["validation_rules"] = ["format:uppercase"]

      validator = described_class.new(form)
      result = validator.validate({ "test" => "ABC" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "abc" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/is not in the required format/)

      form["tests"]["fields"]["test"]["validation_rules"] = ["format:unknown"]
      validator = described_class.new(form)
      result = validator.validate({ "test" => "abc" })
      expect(result.valid).to be true
    end

    it "checks select fields predefined values" do
      form = template.dup
      form["tests"]["fields"]["test"]["type"] = "select"
      form["tests"]["fields"]["test"]["options"] = [{ "value" => "Correct Value" }]

      validator = described_class.new(form)

      result = validator.validate({ "test" => "Correct Value" })
      expect(result.valid).to be true

      result = validator.validate({ "test" => "Wrong value" })
      expect(result.valid).to be false
      expect(result.errors.first.second).to match(/Wrong value for test not in list of allowed values: Correct Value/)
    end

    it "survives nil" do
      expect do
        described_class::Rules::MaxLen.new(amount: 10).check(nil, {})
        described_class::Rules::MinLen.new(amount: 10).check(nil, {})
        described_class::Rules::Currency.new.check(nil, {})
        described_class::Rules::MaxDate.new(date: Time.zone.now).check(nil, {})
        described_class::Rules::RequiredIf.new("key", "value").check(nil, {})
        described_class::Rules::Required.new.check(nil, {})
        described_class::Rules::Regex.new(/.*/).check(nil, {})
        described_class::Rules::Format.new("uppercase").check(nil, {})
      end.not_to raise_error
    end
  end
end
