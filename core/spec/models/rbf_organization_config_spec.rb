# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_organization_configs
#
#  id                     :bigint           not null, primary key
#  code                   :string           not null
#  total_grant_allocation :float            default(0.0), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  organization_id        :integer          not null
#  rbf_claim_template_id  :integer          not null
#  user_id                :integer
#
# Indexes
#
#  idx_on_code_rbf_claim_template_id_967e2caa74             (code,rbf_claim_template_id) UNIQUE
#  idx_on_organization_id_rbf_claim_template_id_2d999cb87a  (organization_id,rbf_claim_template_id) UNIQUE
#

require "rails_helper"

describe RbfOrganizationConfig do
  subject(:price) { create :rbf_organization_config }

  it "stringifies the currency amount" do
    expect(price.total_grant_allocation_s).to eq "UGX5,000,000"
  end

  it "automatically moves organization into proper group" do
    org = create :organization
    rct = create :rbf_claim_template

    expect(org.organization_groups).to be_empty

    described_class.create! rbf_claim_template: rct, organization: org,
                            code: "BLAKEKS", total_grant_allocation: 1000

    expect(org.reload.organization_groups).to eq [rct.organization_group]
  end
end
