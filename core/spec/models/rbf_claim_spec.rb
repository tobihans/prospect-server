# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_claims
#
#  id                         :bigint           not null, primary key
#  aasm_state                 :string
#  approved_at                :datetime
#  claimed_data               :jsonb            not null
#  claimed_subunit            :integer
#  code                       :string
#  paid_out_at                :datetime
#  rejected_at                :datetime
#  submitted_to_program_at    :datetime
#  verification_at            :datetime
#  verified_data              :jsonb            not null
#  verified_subunit           :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  approved_by_id             :bigint
#  assigned_to_verifier_id    :bigint
#  organization_id            :bigint           not null
#  paid_out_by_id             :bigint
#  rbf_claim_template_id      :bigint           not null
#  rejected_by_id             :bigint
#  submitted_to_program_by_id :bigint
#  user_id                    :bigint           not null
#  verification_by_id         :bigint
#
# Indexes
#
#  index_rbf_claims_on_aasm_state             (aasm_state)
#  index_rbf_claims_on_code                   (code) UNIQUE
#  index_rbf_claims_on_organization_id        (organization_id)
#  index_rbf_claims_on_rbf_claim_template_id  (rbf_claim_template_id)
#  index_rbf_claims_on_user_id                (user_id)
#

require "rails_helper"

describe RbfClaim do
  subject(:rbf_claim) { create :rbf_claim }

  before do
    Current.user = create :user
  end

  it "creates the code" do
    expect(rbf_claim.number).to eq 1
    expect(rbf_claim.code).to match(%r{RBF/CODE/\d+/001})

    rbf_claim.update! aasm_state: "rejected" # otherwise we could not create a new one

    # counts up for next claim
    next_claim = described_class.create! organization: rbf_claim.organization, rbf_claim_template: rbf_claim.rbf_claim_template
    expect(next_claim.number).to eq 2
    expect(next_claim.code).to match(%r{RBF/CODE/\d+/002})

    # and even works for nonpersisted when we have all the info
    new_claim = described_class.new

    expect(new_claim.number).to eq 1 # no org / template info yet
    expect(new_claim.generate_code).to be_nil

    new_claim.organization = rbf_claim.organization
    new_claim.rbf_claim_template = rbf_claim.rbf_claim_template

    expect(new_claim.number).to eq 3
    expect(new_claim.generate_code).to match(%r{RBF/CODE/\d+/003})
  end
end
