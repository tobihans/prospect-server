# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_product_prices
#
#  id                          :bigint           not null, primary key
#  base_subsidized_sales_price :integer          not null
#  base_subsidy_amount         :integer          not null
#  category                    :string           not null
#  original_sales_price        :float            not null
#  valid_from                  :date             not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  rbf_product_id              :bigint           not null
#  user_id                     :bigint
#
# Indexes
#
#  idx_on_rbf_product_id_category_valid_from_73af8c87d8  (rbf_product_id,category,valid_from) UNIQUE
#  index_rbf_product_prices_on_rbf_product_id            (rbf_product_id)
#

require "rails_helper"

describe RbfProductPrice do
  subject(:price) { create :rbf_product_price }

  it "validates no same two prices at same time for same category" do
    other = described_class.new rbf_product_id: price.rbf_product_id,
                                category: "paygo",
                                valid_from: price.valid_from,
                                original_sales_price: 10_000,
                                base_subsidized_sales_price: 5_000,
                                base_subsidy_amount: 5_000
    expect(other).not_to be_valid
    expect(other.errors.to_a).to include("Valid from has already been taken")

    other.category = "cash"
    expect(other).to be_valid
  end

  describe "#price_for_at" do
    before do
      5.times do |n|
        day_diff = ((n + 1) * 3).days
        %i[+ -].each do |direction|
          %w[paygo cash].each do |category|
            original_sales_price = ((n + 1) * 1000).send(direction, 100)
            base_subsidized_sales_price = base_subsidy_amount = original_sales_price / 2
            valid_from = price.valid_from.send(direction, day_diff)

            create(:rbf_product_price, rbf_product_id: price.rbf_product_id,
                                       category:, valid_from:, original_sales_price:,
                                       base_subsidized_sales_price:, base_subsidy_amount:)
          end
        end
      end
    end

    # bad style all in one test, but i dont wanna repeat the setup above all the time...
    it "find the correct prices" do
      # purchase date = validity date
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from)
      ).to eq price

      # next day its the same
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from + 1.day)
      ).to eq price

      # and the day after too
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from + 2.days)
      ).to eq price

      # but now here comes the next validity
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from + 3.days).original_sales_price
      ).to eq 1_100

      # the past has another validity
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from - 1.day).original_sales_price
      ).to eq 900

      # the future will always have something
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from + 100.days).original_sales_price
      ).to eq 5_100

      # far in past there is a big nothingness
      expect(
        described_class.price_for_at(price.rbf_product, :paygo, price.valid_from - 100.days)
      ).to be_nil
    end
  end
end
