# frozen_string_literal: true

require "rails_helper"

describe "Adapter" do
  key = "username"
  # take one kind as an example for all implementing classes
  let(:source) { create :source, { kind: "api/angaza", details: { key => "a_username" } } }

  it "no duplicate source" do
    expect(source).to be_valid
  end

  it "duplicate source" do
    other_source = build :source, { project: source.project, kind: source.kind }
    other_source.details[key] = "a_username"

    expect do
      other_source.save!
    end.to raise_error(ActiveRecord::RecordInvalid)
  end

  it "capability? existing" do
    expect(source.adapter.capability?(:backfill_completely)).to be true
  end

  it "capability? false" do
    # maybe this should rather throw an error, as this capability does not exist... vs set to false
    expect(source.adapter.capability?(:backfill_xyz)).to be false

    expect(source.adapter.capability?(:backfill_from_date)).to be false
  end
end
