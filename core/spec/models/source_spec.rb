# frozen_string_literal: true

# == Schema Information
#
# Table name: sources
#
#  id            :bigint           not null, primary key
#  activated_at  :datetime
#  data_category :string
#  details       :jsonb            not null
#  kind          :string           not null
#  name          :string           not null
#  secret        :string(1000)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  project_id    :bigint           not null
#  user_id       :integer
#
# Indexes
#
#  index_sources_on_activated_at         (activated_at)
#  index_sources_on_kind                 (kind)
#  index_sources_on_name_and_project_id  (name,project_id) UNIQUE
#  index_sources_on_project_id           (project_id)
#  index_sources_on_user_id              (user_id)
#

require "rails_helper"

describe Source do
  describe "activate inactive" do
    let(:source) { create :source }

    it "is active when activated_at is set" do
      source.activated_at = Time.zone.now
      expect(source).to be_active
      expect(source).not_to be_inactive
    end

    it "is active when activated_at is null" do
      source.activated_at = nil
      expect(source).not_to be_active
      expect(source).to be_inactive
    end

    it "triggers import if activated" do
      expect { create :source, activated_at: Time.zone.now }.to change { ImportWorker.jobs.count }.by 1
      expect { create :source, activated_at: nil }.not_to(change { ImportWorker.jobs.count })
    end
  end

  describe "user" do
    before { Current.user = create :user }

    let(:source) { create :source }

    it "gets saved" do
      expect(source.user).to eq Current.user
    end
  end

  describe "custom data" do
    let(:source) { create :source, data_category: :custom, name: "Peter & Lustig" }

    before do
      allow(source).to receive(:id).and_return(33)
    end

    it "#custom_data?" do
      expect(source).to be_custom_data
    end

    it "gives name part for custom view" do
      expect(source.custom_view).to eq "custom_33_peter___lustig"
    end

    it "gives us the custom structure as a string" do
      expect(source.custom_structure_s).to eq ""
      source.details["custom_columns"] = [%w[some_col text], %w[another numeric]]
      expect(source.custom_structure_s).to eq "some_col => text, another => numeric"
    end

    it "gives us the custom structure as a hash" do
      expect(source.custom_structure_h).to eq({})
      source.details["custom_columns"] = [%w[some_col text], %w[another numeric]]
      expect(source.custom_structure_h).to eq({ "some_col" => "text", "another" => "numeric" })
    end
  end

  describe "#last import and #last_import hint" do
    let(:source) { create :source, :victron }

    it "returns #last_import no matter the timestamps" do
      Import.create!(source:)
      i = Import.create!(source:)

      expect(source.imports.last).to eq i
    end

    it "#last import hint returns last import hint for last ingestion_finished import" do
      3.times do |n|
        Import.create! source:, last_imported_hint: "import hint #{n}",
                       ingestion_finished_at: n < 2 ? Time.zone.now : nil
      end
      expect(source.last_imported_hint).to eq("import hint 1")
    end
  end

  describe "common for all kinds" do
    let(:source) { create :source }

    it "#to_s_download cleans good" do
      source.name = "a B \t && xx55667 67!"
      expect(source.to_s_download).to eq "a_b______xx55667_67_"
    end
  end

  describe "check for each kinds" do
    Source::KINDS.each do |k|
      describe k do
        let(:source) { create :source, kind: k }

        before do
          source.kind = k
        end

        it "has an I18n key" do
          expect(I18n.t("build.kinds.#{k}").downcase).not_to include "translation missing"
        end

        it "is a valid source" do
          expect(source).to be_valid
        end

        it "gives array of possible data categories" do
          expect(source.adapter.data_categories).to be_an Array
          expect(source.adapter.data_categories.first).to be_a String
        end

        it "has data schema for kind #{k}" do
          expect(source.data_schema).to be_present
        end

        it "has data origin for kind #{k}" do
          expect(source.data_origin).to be_present
        end

        it "is either push or pull" do
          expect(source.adapter.pull? || source.adapter.push?).to be_truthy
        end

        # this is hard to implement in a generic way, we would need to mock each request?
        #
        # it "returns a result on method for pull adapters" do
        #   expect(source.adapter.retrieve_data).to be_a Source::Adapter::Result if source.adapter.pull?
        # end

        # it "returns a result on method for push adapters" do
        #  expect(source.adapter.process_data("SomeCSV,data")).to be_a Source::Adapter::Result if source.adapter.push?
        # end
        # it "has a validation hint" do
        #  expect(source.adapter.validation_hint).to be_a String
        # end

        # it "gives array as validation protocol" do
        #  expect(source.adapter.validation_result).to be_an Source::Adapter::Result
        # end

        it "implements backfill the correct way" do
          expect(source.adapter).to respond_to(:backfill_completely_import_hint) if source.adapter.capability? :backfill_completely
          from_date = 1.day.ago
          # normally the input hint contains the given date as a string... but this doesn't have to be always like that. Adapt test if neccesary
          expect(source.adapter.backfill_from_date_import_hint(from_date)).to include(from_date.to_s) | include(from_date.to_i.to_s) if source.adapter.capability? :backfill_from_date
        end
      end
    end
  end

  describe "#trigger_import!" do
    it "#works for pushed source with data" do
      source = create :source, kind: :manual_upload, details: { "file_format" => "csv" }
      expect(source.adapter).to be_push
      expect(source.trigger_import!(source.data_table.examples_csv)).to be_a Import
    end

    it "#works for pushed source without data" do
      source = create :source, kind: :api_push
      expect(source.adapter).to be_push
      expect(source.trigger_import!).to eq 42
    end

    it "uses given import hint" do
      source = create :source, :victron # we need a pull adapter

      allow(source.adapter).to receive(:retrieve_data).
        and_return(
          Source::Adapter::Result.new(format: :csv, data: "nothing", protocol: "", error: nil, hint: "", more_data: false)
        )

      source.trigger_import! nil, use_db_import_hint: false, import_hint: "my_import_hint"

      expect(source.adapter).to have_received(:retrieve_data).with("my_import_hint") # rubocop:disable RSpec/MessageSpies
    end

    describe "good data" do
      let(:source) { create :source, kind: "manual_upload", data_category: "test", details: { "file_format" => "csv" } }
      let(:good_data) { source.data_table.examples_csv }
      let(:import!) { source.trigger_import!(good_data) }

      it "creates one import" do
        expect { import! }.to change(Import, :count).by 1
      end

      it "import is not failed" do
        import!
        expect(Import.last).not_to be_failed
      end

      it "import is ingestion_finished" do
        import!
        expect(Import.last).to be_ingestion_finished
      end

      it "does not set last_import_hint" do
        import!
        expect(Import.last.last_imported_hint).to be_nil
      end

      it "stores the data" do
        import!
        exp =
          [{
            "bool_column" => true,
            "enum_column" => "b",
            "float_column" => 3.14,
            "int_column" => 5,
            "name_column_p" => P14n.p("pater paul stored as fe42o3p2"),
            "personal_column" => "Personal Text",
            "time_column" => "2021-02-22 12:34:56 UTC"
          }]
        expect(JSON.parse(Import.last.file.download)["test"].first["name_column_e"]).not_to be_empty
        f = JSON.parse(Import.last.file.download)["test"]
        f.first.delete("name_column_e")
        expect(f).to eq exp
      end

      it "has a protocol" do
        import!
        exp_protocol = <<~TXT.strip
          Checking data file format: csv ... OK
          Checking schema is conforming to: test ... OK
          Applying cleaning ... OK
          Checking data presence ... OK
            Counted 1 rows
          >>> SUCCESS
        TXT

        expect(Import.last.protocol).to eq exp_protocol
      end

      it "save protocol in case of error in import" do
        # lets make our own adapter that logs and raises an error
        stub_const "Source::RaiseAdapter", Class.new
        Source::RaiseAdapter.class_eval do
          include Source::Adapter::Api

          def pull?
            true
          end

          def retrieve_data(_since)
            log "A very important log message"
            raise "unexpected error in importer"
          end

          def data_categories
            ["shs"]
          end
        end

        # create a source with our new adapter
        source = described_class.new
        source.kind = :raise_adapter
        expect(source.adapter.class).to be Source::RaiseAdapter

        # start the import
        imp = source.trigger_import!

        # check that we get everything in the protocol
        expect(imp.protocol).to match(/A very important log message/)
        expect(imp.protocol).to match(/unexpected error in importer/)
        expect(imp).to be_failed
      end
    end

    describe "bad data" do
      let(:source) { create :source, kind: "manual_upload", data_category: "test", details: { "file_format" => "csv" } }
      let(:bad_data) { source.data_table.examples_csv.gsub("3.14", "") }
      let(:import!) { source.trigger_import!(bad_data) }

      it "creates one import" do
        expect { import! }.to change(Import, :count).by 1
      end

      it "import is failed" do
        import!
        expect(Import.last).to be_failed
      end

      it "import is ingestion_finished_failed" do
        import!
        expect(Import.last).to be_ingestion_started_failed
      end

      it "does not set last_import_hint" do
        import!
        expect(Import.last.last_imported_hint).to be_nil
      end

      it "does not store the data" do
        import!
        expect(Import.last.file).not_to be_attached
      end

      it "has a protocol" do
        import!
        exp_protocol = <<~TXT.strip
          Checking data file format: csv ... OK
          Checking schema is conforming to: test ... ERROR
            Line 1: float_column is required
          >>> ERROR
        TXT

        expect(Import.last.protocol).to eq exp_protocol
      end
    end

    describe "no data" do
      let(:source) { create :source, kind: "google_drive", data_category: "test", details: { "file_format" => "csv" } }

      let(:import!) { source.trigger_import! }

      before do
        allow(source.adapter).to receive(:retrieve_data).and_return(
          Source::Adapter::Result.new(format: :csv, data: nil, protocol: "This is a test", error: nil, hint: "some_hint", more_data: false)
        )
      end

      it "creates one import" do
        expect { import! }.to change(Import, :count).by 1
      end

      it "import in state no_data" do
        import!
        expect(Import.last.no_data?).to be true
      end
    end
  end

  describe "adapter concerns" do
    it "Source::Adapter makes sure that several methods get overriden" do
      adapter_class = Class.new { include Source::Adapter }
      source = create :source, kind: :api_push
      ac = adapter_class.new source

      expect { ac.data_origin }.to raise_error(/Implement ME/)
      expect { ac.data_schema }.to raise_error(/Implement ME/)
      expect { ac.retrieve_data(nil) }.to raise_error(/Implement ME/)
      expect { ac.process_data(nil) }.to raise_error(/Implement ME/)
      expect { ac.ingest(nil) }.to raise_error(/Implement ME/)
      expect { ac.fetch(nil) }.to raise_error(/Implement ME/)
      expect { ac.data_categories }.to raise_error(/Implement ME/)
    end

    describe "Source::Adapter::Generic" do
      let(:custom_source) { create :source, :custom, details: { "file_format" => "json" } }
      let(:custom_adapter) do
        adapter_class = Class.new { include Source::Adapter::Generic }
        adapter_class.new custom_source
      end

      it "can give the data table for generic adapters" do
        source = create :source, kind: :manual_upload, data_category: :shs
        expect(source.adapter.data_table.data_category).to eq "shs"
      end

      it "#convert_to_hashes still raises for unknown format" do
        adapter_class = Class.new { include Source::Adapter::Generic }
        source = create :source, kind: :api_push, details: { "file_format" => "abcd" }
        ac = adapter_class.new source

        expect { ac.convert_to_hashes("") }.to raise_error "Failed to parse file: abcd is not supported"
      end

      it "can build custom column conf" do
        data = [{ "ts_col" => "2022-02-22 02:22:22",
                  "num_col" => "3.14",
                  "txt_col" => "blabla",
                  "blnk_col" => "" }].to_json

        custom_adapter.ingest data

        expect(custom_source.reload.details["custom_columns"]).
          to eq [%w[ts_col timestamp], %w[num_col numeric], %w[txt_col text], %w[blnk_col text]]
      end

      it "complains about bad custom columns: empty name" do
        custom_adapter.ingest [{ " " => 5 }].to_json
        expect(custom_adapter.error).to eq "Persisting custom structure: Column without title detected."
      end

      it "complains about bad custom columns: weird characters" do
        custom_adapter.ingest [{ "%hallo" => 5 }].to_json
        expect(custom_adapter.error).
          to eq "Persisting custom structure: Column title can only consist of letter, number, underscore, dash, space: '%hallo'."
      end

      it "complains about bad custom columns: start with space" do
        custom_adapter.ingest [{ " blabla" => 5 }].to_json
        expect(custom_adapter.error).
          to eq "Persisting custom structure: Column title can not start or end with whitespace: ' blabla'."
      end

      it "complains about bad custom columns: end with space" do
        custom_adapter.ingest [{ "miau " => 5 }].to_json
        expect(custom_adapter.error).to eq "Persisting custom structure: Column title can not start or end with whitespace: 'miau '."
      end
    end
  end

  describe "clockwork" do
    let(:source) { create :source }

    it "uses UTC" do
      expect(source.tz).to eq "UTC"
    end

    it "skips first run if imports exist" do
      expect(source.skip_first_run).to be_falsey
      create(:import, source:)
      expect(source.skip_first_run).to be_falsey
    end

    it "checks daily unless the frequency is hourly" do
      %w[weekly daily monthly quarterly].each do |freq|
        source.details["frequency"] = freq
        expect(source.frequency).to eq 1.day
      end

      source.details["frequency"] = "hourly"
      expect(source.frequency).to eq 1.hour
    end

    it "check minute frequency" do
      source.details["frequency"] = "10.minutes"
      expect(source.frequency).to eq 10.minutes
      expect(source.at).to be_nil
    end

    it "creates proper at string" do
      %w[weekly daily monthly quarterly].each do |freq|
        source.details["frequency"] = freq

        expect(source.at).to start_with "03:"
      end
    end

    describe "#if?" do
      let(:random_day) { Time.zone.parse("2222-02-02") } # not monday, not first, just some casual day

      before do
        source.details["frequency"] = "hourly"
      end

      it "fails unless active with set frequency" do
        source.details["frequency"] = nil
        source.activated_at = nil

        expect(source).not_to be_if

        source.details["frequency"] = "hourly"

        expect(source).not_to be_if

        source.activated_at = Time.zone.now

        expect(source).to be_if
      end

      it "fails with unknown frequency" do
        expect(source).to be_if
        source.details["frequency"] = "decadely"
        expect(source).not_to be_if
        expect(source).not_to be_if(random_day)
        expect(source).not_to be_if(Time.zone.now.beginning_of_month)
        expect(source).not_to be_if(Time.zone.now.beginning_of_week)
      end

      it "monthly only on 1st" do
        source.details["frequency"] = "monthly"
        expect(source).not_to be_if(random_day)
        expect(source).to be_if(Time.zone.now.beginning_of_month)
      end

      it "weekly only on sunday" do
        source.details["frequency"] = "weekly"
        expect(source).not_to be_if(random_day)
        expect(source).to be_if(Time.zone.now.beginning_of_week)
      end

      %w[daily hourly].each do |freq|
        it "#{freq} is always happy" do
          source.details["frequency"] = freq
          expect(source).to be_if
          expect(source).to be_if(random_day)
          expect(source).to be_if(Time.zone.now.beginning_of_month)
          expect(source).to be_if(Time.zone.now.beginning_of_week)
        end
      end

      it "has proper at for hourly" do
        source.details["frequency"] = "hourly"
        expect(source.at).to match(/\*\*:\d\d/)
      end

      it "minutely" do
        source.details["frequency"] = "10.minutes"
        expect(source).to be_if
        expect(source.at).to be_nil
      end
    end
  end

  it "visible_kinds" do
    org = create :organization

    ENV["MAIN_ORG_ID"] = org.id.to_s
    expect(described_class.visible_kinds(org)).to include("api/stellar_multi_org_parent")
    expect(described_class.visible_kinds(org)).to include("aam_parent")

    ENV["MAIN_ORG_ID"] = "-1"
    expect(described_class.visible_kinds(org)).not_to include("api/stellar_multi_org_parent")
    expect(described_class.visible_kinds(org)).not_to include("aam_parent")
  end

  describe "data manipulation" do
    let(:source) { create :source }
    let(:import) { create :import, source: }

    def source_data_count(source)
      q = "SELECT * FROM data_test WHERE source_id = #{source.id}"
      Postgres::RemoteControl.query!(q).ntuples
    end

    before do
      data = {
        import_id: import.id, source_id: source.id, organization_id: source.organization.id,
        created_at: Time.zone.now, updated_at: Time.zone.now, data_origin: "here",
        enum_column: "a", float_column: 1.23, uid: "uiuiui"
      }
      raw_db_insert data_test: [data]
    end

    it "has some data" do
      expect(source_data_count(source)).to eq 1
    end

    it "can delete the data" do
      source.delete_data!
      expect(source_data_count(source)).to be_zero
    end

    it "can move the data" do
      source2 = create :source, project: source.project
      expect(source_data_count(source)).to eq 1
      expect(source_data_count(source2)).to eq 0

      source.move_data_to_source! source2, i_know_what_i_do: true

      expect(source_data_count(source)).to eq 0
      expect(source_data_count(source2)).to eq 1
    end
  end
end
