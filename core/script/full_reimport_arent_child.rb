# frozen_string_literal: true

def full_reimport_parent_child!(parent_kind:, child_kind:)
  parent = Source.active.find_by! kind: parent_kind
  children = Source.active.where kind: child_kind

  all_sources = [parent, children].flatten

  all_sources.each(&:delete_data!)
  all_sources.each { |s| s.imports.each(&:destroy) }

  parent.trigger_import!
end

## trigger full reimport
#   full_reimport_parent_child!(parent_kind: :aam_parent, child_kind: :aam_child)
## check imports generated state
#   Source.where(kind: :aam_child).to_h{|s| [s.id, s.imports.map(&:state).tally] }
