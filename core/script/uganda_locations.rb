# frozen_string_literal: true

csv = CSV.open("uganda.csv", headers: true)

locations = {}
current = {}

keys = ["REGION", "DISTRICT", "SCOUNTY/TOWN COUNCIL/DIVISION", "PARISH / WARD", "VILLAGE/CELL"]

csv.map do |row|
  loc = row.to_h

  keys.each do |key|
    value = loc[key] || current[key]

    value = if value.match(/ - /)
              value.split(" - ").map { _1.humanize.titleize }.join(" - ")
            elsif value.match(/-/)
              value.split("-").map { _1.humanize.titleize }.join("-")
            else
              value.humanize.titleize
            end

    current[key] = value.squish!
  end

  roman_numbers = / (?=[MDCLXVI])M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})( |$)/i

  # handle roman numbers...
  match = current["PARISH / WARD"].match(roman_numbers)
  if match
    match.captures.reject(&:empty?).each do |c|
      current["PARISH / WARD"].gsub!(c, c.upcase)
    end
  end

  # titalize eats dashes...
  village = if loc["VILLAGE/CELL"].match(/ - /)
              loc["VILLAGE/CELL"].split(" - ").map { _1.humanize.titleize }.join(" - ")
            elsif loc["VILLAGE/CELL"].match(/-/)
              loc["VILLAGE/CELL"].split("-").map { _1.humanize.titleize }.join("-")
            else
              loc["VILLAGE/CELL"].split("'").map { _1.humanize.titleize }.join("'")
            end

  # handle roman numbers...
  match = village.match(roman_numbers)

  if match
    match.captures.reject(&:empty?).each do |c|
      village.gsub!(c, c.upcase)
    end
    village.gsub!("MIX", "Mix") # I'm special
  end

  village.gsub!(/'+ ?(.{1})'*( |$|[A-Z])/, '\1') # Name 'B' => Name B
  village.gsub!(/"(.{1})"/, '\1') # Name "A" => Name A
  village.gsub!(/\((.{1})\)/, '\1') # Name (C) => Name C
  village.squish!

  locations[current["REGION"]] ||= {}
  locations[current["REGION"]][current["DISTRICT"]] ||= {}
  locations[current["REGION"]][current["DISTRICT"]][current["SCOUNTY/TOWN COUNCIL/DIVISION"]] ||= {}
  locations[current["REGION"]][current["DISTRICT"]][current["SCOUNTY/TOWN COUNCIL/DIVISION"]][current["PARISH / WARD"]] ||= []
  locations[current["REGION"]][current["DISTRICT"]][current["SCOUNTY/TOWN COUNCIL/DIVISION"]][current["PARISH / WARD"]] << village

  "#{current['REGION']} ~ #{current['DISTRICT']} ~ #{current['SCOUNTY/TOWN COUNCIL/DIVISION']} ~ #{current['PARISH / WARD']} ~ #{village}"
  # village unless village.gsub("^[A-Z][a-z]+$", "").empty?
  # village unless village.match(/^([A-Z][a-z]+ ?)+$/)
  # current["PARISH / WARD"]# unless current["PARISH / WARD"].match(/^[A-Z][a-z]+$/)
  # current["SCOUNTY/TOWN COUNCIL/DIVISION"]# unless current["PARISH / WARD"].match(/^[A-Z][a-z]+$/)
end
