# execute me with docker exec, we need a bit of debugging and clean up somethings
# otherwise we sometimes get some annoyances
#
# called like:
# docker compose exec core bash -c '/core/script/prepare_docker_for_rubymine.sh'

# clear orphaned servers or debuggers
pkill -f puma
pkill -f rdebug

# we just dont care ;)
exit 0

