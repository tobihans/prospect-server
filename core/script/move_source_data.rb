# frozen_string_literal: true

##
# Move data between datasources of different organizations
# tries to recreate uids.
# might get outdated easily if dat  structure changes...
# main entrypoint below: def move_data_source

def query(query, run:)
  puts query
  if run
    Postgres::RemoteControl.query! query
  else
    OpenStruct.new to_a: [], ntuples: 0
  end
end

def change_uid(uid_str, oo_id, no_id)
  uid_str.sub(/^#{oo_id}_/, "#{no_id}_")
end

def change_uid_pair(uid_str, oo_id, no_id)
  [uid_str, change_uid(uid_str, oo_id, no_id)]
end

def build_attr_set_sql(changes, *slices)
  changes = changes.slice(*slices) if slices.present?
  changes = changes.slice(*slices) if slices.present?

  changes.map do |col, chg|
    "#{col} = #{chg.last}"
  end.join(", ")
end

def move_data_source_diff_org(os_id, oo_id, ns_id, no_id, _run:)
  where_old_source = "WHERE source_id = #{os_id}"
  default_changes = { "source_id" => [os_id, ns_id],
                      "organization_id" => [oo_id, ns_id] }

  meters_queries = []
  meters_ts_queries = []
  grids_queries = []
  grids_ts_queries = []
  shs_queries = []
  shs_ts_queries = []
  payments_ts_queries = []
  custom_queries = []

  ## meters => meters_ts

  meters_result = query("SELECT * FROM data_meters #{where_old_source}", run: true)
  puts "Found #{meters_result.ntuples} meters"

  meter_values = meters_result.pluck "uid", "account_uid", "customer_uid", "device_uid", "grid_uid"
  meters_changes = meter_values.map do |u|
    h = {
      "account_uid" => change_uid_pair(u.second, oo_id, no_id),
      "customer_uid" => change_uid_pair(u.third, oo_id, no_id),
      "device_uid" => change_uid_pair(u.fourth, oo_id, no_id),
      "grid_uid" => change_uid_pair(u.fifth, oo_id, no_id)
    }.merge(default_changes)

    h["uid"] = [u.first, "#{h.fetch('customer_uid').last}_#{h.fetch('device_uid').last}"]

    h
  end

  puts meters_changes

  meters_changes.each do |c|
    meters_queries << <<~SQL.squish
      UPDATE data_meters
      SET
        #{build_attr_set_sql(c)}
      #{where_old_source} AND uid = #{c.fetch('uid').first}
    SQL

    meters_ts_queries << <<~SQL.squish
      UPDATE data_meters_ts
      SET
        #{build_attr_set_sql('device_uid', 'source_id', 'organization_id')}
      #{where_old_source}
        AND device_uid = #{c.fetch('device_uid').first}
    SQL
  end

  # grids => grids_ts

  grids_result = query("SELECT * FROM data_grids #{where_old_source}", run: true)

  puts "Found #{grids_result.ntuples} grids"

  grid_values = grids_result.pluck "uid"
  grids_changes = grid_values.map do |u|
    h = {
      "uid" => change_uid_pair(u.first, oo_id, no_id),
      "grid_uid" => change_uid_pair(u.first, oo_id, no_id)
    }.merge(default_changes)
    h
  end
  puts grids_changes
  grids_changes.each do |c|
    grids_queries << <<~SQL.squish
      UPDATE data_grids
      SET
        #{build_attr_set_sql(c, 'uid', 'source_id', 'organization_id')}
      #{where_old_source} AND uid = #{c.fetch('uid').first}
    SQL

    grids_ts_queries << <<~SQL.squish
      UPDATE data_grids_ts
      SET
        #{build_attr_set_sql(c, 'grid_uid', 'source_id', 'organization_id')}
      #{where_old_source}
        AND grid_uid = #{c.fetch('grid_uid').first}
    SQL
  end

  # shs => shs_ts

  shs_result = query("SELECT * FROM data_shs #{where_old_source}", run: true)

  puts "Found #{shs_result.ntuples} shs"

  shs_values = shs_result.pluck "uid", "device_uid", "customer_uid", "account_uid"
  shs_changes = shs_values.map do |u|
    h = {
      "device_uid" => change_uid_pair(u.second, oo_id, no_id),
      "customer_uid" => change_uid_pair(u.third, oo_id, no_id),
      "account_uid" => change_uid_pair(u.fourth, oo_id, no_id)
    }.merge(default_changes)
    h["uid"] = [u.first, "#{h.fetch('customer_uid').last}_#{h.fetch('device_uid').last}"]
    h
  end
  puts shs_changes
  shs_changes.each do |c|
    shs_queries << <<~SQL.squish
      UPDATE data_shs
      SET
        #{build_attr_set_sql(c)}
      #{where_old_source} AND uid = #{c.fetch('uid').first}
    SQL

    shs_ts_queries << <<~SQL.squish
      UPDATE data_shs_ts
      SET
        #{build_attr_set_sql(c, 'device_uid', 'source_id', 'organization_id')}
      #{where_old_source}
        AND device_uid = #{c.fetch('device_uid').first}
    SQL
  end

  # payments_ts

  payments_result = query("SELECT * FROM data_payments_ts #{where_old_source}", run: true)

  puts "Found #{payments_result.ntuples} payments"

  payments_values = payments_result.pluck "uid", "account_uid"
  payments_changes = payments_values.map do |u|
    h = {
      "account_uid" => change_uid_pair(u.second, oo_id, no_id)
    }.merge(default_changes)

    new_uid = u.first.gsub "_#{oo_id}_", "_#{no_id}_"
    h["uid"] = [u.first, new_uid]
    h
  end
  puts payments_changes
  payments_changes.each do |c|
    payments_ts_queries << <<~SQL.squish
      UPDATE data_payments_ts
      SET
        #{build_attr_set_sql(c)}
      #{where_old_source}
        AND uid = #{c.fetch('uid').first}
    SQL
  end

  # custom

  custom_result = query("SELECT * FROM data_custom #{where_old_source}", run: true)

  puts "Found #{custom_result.ntuples} custom"

  custom_values = custom_result.pluck "uid"
  custom_changes = custom_values.map do |u|
    h = {
      "uid" => change_uid_pair(u.first, oo_id, no_id)
    }.merge(default_changes)
    h
  end

  puts custom_changes

  custom_changes.each do |c|
    custom_queries << <<~SQL.squish
      UPDATE data_custom
      SET
        #{build_attr_set_sql(c)}
      #{where_old_source} AND uid = #{c.fetch('uid').first}
    SQL
  end

  [
    meters_queries,
    meters_ts_queries,
    grids_queries,
    grids_ts_queries,
    shs_queries,
    shs_ts_queries,
    payments_ts_queries,
    custom_queries
  ].each do |qs|
    qs.each { |q| puts q }
  end
end

###
# BE CAREFUL! So you need to explicitly set run: true to actually execute it
# It depends on uid generation logic - if that changes you need to change things here
#

def move_data_source(old_source_id, new_source_id, run: false)
  os = Source.find old_source_id
  oo_id = os.organization_id
  ns = Source.find new_source_id
  no_id = ns.organization_id

  # deactivate all sources
  [os, ns].each do |s|
    s.active = false
    s.save!
  end

  if oo_id == no_id
    Source.move_data_to_source! Source.find(new_source_id)
  else
    move_data_source_diff_org(old_source_id, oo_id, new_source_id, no_id, run:)
  end

  # activate new source
  ns.active = true
  ns.save!
end
