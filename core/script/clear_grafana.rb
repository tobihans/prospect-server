# frozen_string_literal: true

Grafana::Client.new.tap do |client|
  client.all_orgs.each do |org|
    client.all_users.map { |user| client.delete_user(user[:id]) }
    client.delete_org(org_id: org[:id])
  end
end
