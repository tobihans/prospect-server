# frozen_string_literal: true

module UserHelper
  def show_invitation_url?(invited_user)
    invited_user.invited? && invited_user.created_at < 5.minutes.ago && policy(invited_user).show_invitation_url?
  end
end
