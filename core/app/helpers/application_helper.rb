# frozen_string_literal: true

module ApplicationHelper
  include UserHelper
  include Pagy::Frontend

  TW_SUBMIT_PROMINENT_CLASSES = %w[
    w-full
    py-2
    px-4
    font-bold
    uppercase
    text-grey-1
    hover:text-white
    hover:bg-primary/70
    bg-gradient-to-r
    hover:animate-pulse
    whitespace-nowrap
    disabled:!bg-grey-3
    disabled:!bg-none
    disabled:!animate-pulse
  ].freeze

  TW_GRADIENTS = {
    ok: %w[from-shade-3 to-shade-2],
    bad: %w[from-pink to-grey-4],
    skip: %w[from-grey-4 to-grey-3]
  }.freeze

  TW_RADIO_CLASSES = %w[accent-primary text-primary focus:ring-primary].freeze

  TW_TEXT_INPUT_CLASSES = %w[rounded focus:ring-primary focus:border-primary].freeze

  TW_PASSWORD_INPUT_CLASSES = %w[pr-10].freeze

  TW_SELECT_INPUT_CLASSES = %w[rounded focus:ring-primary focus:border-primary].freeze

  TW_PROMINENT_LINK_CLASSES = %w[rounded-full shadow].freeze

  def tw_submit_prominent_args(more = nil, color: :ok, without: [])
    raise "unhandled color #{color}!" unless TW_GRADIENTS[color]

    klasses = (TW_SUBMIT_PROMINENT_CLASSES + TW_GRADIENTS[color]).without(without)
    as_class_arg join(klasses, more)
  end

  def tw_link_prominent_args(more = nil, color: :ok, without: [])
    klasses = (TW_PROMINENT_LINK_CLASSES + TW_GRADIENTS[color]).without(without)
    klasses = join klasses, more
    tw_submit_prominent_args klasses, color:, without:
  end

  def tw_radio_args(more = nil)
    as_class_arg join(TW_RADIO_CLASSES, more)
  end

  def tw_text_input_args(more = nil)
    as_class_arg join(TW_TEXT_INPUT_CLASSES, more)
  end

  def tw_password_input_args(more = nil)
    as_class_arg join(TW_TEXT_INPUT_CLASSES + TW_PASSWORD_INPUT_CLASSES, more)
  end

  def tw_select_input_args(more = nil)
    as_class_arg join(TW_SELECT_INPUT_CLASSES, more)
  end

  def user_can?(privilege, record: nil)
    Current.user.can? privilege, record:
  end

  def toastinfo(title, heading:, description:)
    render "application/toastinfo", { title:, heading:, description: }
  end

  # Calculate + print the percentage of Numeric or countable object (eg. AR). Thinly wraps of ActiveSupports number_to_percentage
  def percentage_s(numeric_or_countable, full_numeric_or_countable, **number_to_percentage_args)
    part_amount = numeric_or_countable.is_a?(Numeric) ? numeric_or_countable : numeric_or_countable.count
    full_amount = full_numeric_or_countable.is_a?(Numeric) ? full_numeric_or_countable : full_numeric_or_countable.count

    percentage = part_amount * 100 / full_amount.to_f

    percentage = if percentage.nan?
                   0.0
                 elsif percentage.infinite?
                   "-"
                 else
                   percentage
                 end

    ntp_args = { precision: 1, strip_insignificant_zeros: true }.merge(number_to_percentage_args)
    ActiveSupport::NumberHelper.number_to_percentage percentage, **ntp_args
  end

  private

  def join(some, arg)
    others = if arg.is_a? String
               arg.split
             elsif arg.is_a? Array
               arg
             elsif arg.nil?
               []
             else
               raise "Can not handle this type of thing: #{arg.class}"
             end
    (some + others).join " "
  end

  def as_class_arg(str)
    { class: str }
  end

  def booly(bool)
    bool ? "✓" : "⤫"
  end
end
