# frozen_string_literal: true

class OrganizationUserPolicy < ApplicationPolicy
  def index?
    user.can?("user.manage", record:)
  end

  def update?
    user.can?("user.manage", record:)
  end
end
