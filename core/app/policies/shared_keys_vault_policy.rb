# frozen_string_literal: true

class SharedKeysVaultPolicy < ApplicationPolicy
  include PolicyConcerns::RecordWithOrganization

  def edit?
    # TODO
    true
  end

  def create?
    # TODO
    true
  end

  def delete?
    # TODO
    true
  end
end
