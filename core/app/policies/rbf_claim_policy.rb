# frozen_string_literal: true

class RbfClaimPolicy < ApplicationPolicy
  def index?
    Current.organization&.rbf_claim_templates&.any?
  end

  def show?
    current_involved_organization?
  end

  def new?
    RbfClaimTemplate.any? { _1.organization_can_create_claims?(user.organization) } &&
      user.can?("rbf_claim.create")
  end

  def create?
    current_submitting_organization? && user.can?("rbf_claim.create")
  end

  def summary?
    show?
  end

  def do_data_collection?
    current_submitting_organization? && user.can?("rbf_claim.do_data_collection")
  end

  def do_data_collection_add_all?
    do_data_collection?
  end

  def show_do_data_collection?
    do_data_collection?
  end

  def print_submitted?
    record.submitted_to_program? &&
      current_submitting_organization? &&
      user.can?("rbf_claim.print_submitted")
  end

  def show_do_assign_verifiers?
    current_verifying_organization? && user.can?("rbf_claim.assign_verifiers")
  end

  def do_assign_verifiers?
    current_verifying_organization? && user.can?("rbf_claim.assign_verifiers")
  end

  def show_do_verification?
    do_verification?
  end

  def do_verification?
    current_verifying_organization? &&
      user.can?("rbf_claim.do_verification") &&
      user.in?(record.verification_users)
  end

  def download_verification_csv?
    record.verification? &&
      current_verifying_organization? &&
      user.can?("rbf_claim.verification_download_data")
  end

  def do_sample_secondary_verification?
    do_verification? && record.verification_stage == :default_done
  end

  def do_device_verification?
    do_verification?
  end

  def show_verification_result?
    (current_involved_organization? && user.can?("rbf_claim.see_verification_result")) ||
      (current_verifying_organization? && user.can?("rbf_claim.approve_verification"))
  end

  def show_print_voucher?
    current_verifying_organization? && user.can?("rbf_claim.do_payment")
  end

  def print_voucher?
    show_print_voucher?
  end

  def do_payment?
    current_managing_organization? && user.can?("rbf_claim.do_payment")
  end

  def show_do_payment?
    do_payment?
  end

  def show_paid_out?
    show?
  end

  def download?
    true
  end

  def verify_devices?
    true
  end

  def device_details?
    true
  end

  def device_claim?
    true
  end

  def mark_paid?
    true
  end

  def event?
    true
  end

  def iva_result?
    true
  end

  private

  def current_involved_organization?
    record.rbf_claim_template.organization_involved? Current.organization
  end

  def current_supervising_organization?
    record.rbf_claim_template.supervising_organization == Current.organization
  end

  def current_managing_organization?
    record.rbf_claim_template.managing_organization == Current.organization
  end

  def current_verifying_organization?
    record.rbf_claim_template.verifying_organization == Current.organization
  end

  def current_submitting_organization?
    record.rbf_claim_template.organization_can_create_claims? Current.organization
  end
end
