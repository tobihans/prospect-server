# frozen_string_literal: true

class RbfDashboardPolicy < ApplicationPolicy
  def index?
    true
  end

  def rbf_products?
    true
  end

  def add_agent?
    user.can?("source.manage")
  end

  def toggle_agent?
    user.can?("source.manage")
  end

  def setup_project?
    user.can?("project.manage")
  end
end
