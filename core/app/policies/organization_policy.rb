# frozen_string_literal: true

class OrganizationPolicy < ApplicationPolicy
  def mine?
    true
  end

  def show?
    true
  end

  def update?
    admin_or_can_manage_org?
  end

  def edit?
    admin_or_can_manage_org?
  end

  def show_token?
    user.admin? || (user.can?("organization.view_api_token") && record == user.organization)
  end

  private

  def admin_or_can_manage_org?
    user.admin? || (user.can?("organization.manage") && record == user.organization)
  end
end
