# frozen_string_literal: true

module PolicyConcerns
  module RecordWithOrganization
    extend ActiveSupport::Concern

    included do
      private

      def record_belongs_to_user_organization?
        record.owning_organization == user.organization
      end

      def can_view_organization_resource?
        user.admin? || record_belongs_to_user_organization?
      end
    end
  end
end
