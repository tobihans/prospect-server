# frozen_string_literal: true

class AnalyticsPolicy < ApplicationPolicy
  def index?
    user.can?("analytics.view")
  end

  def show?
    user.can?("analytics.view")
  end

  def grafana_new?
    user.can?("analytics.view")
  end

  def preview?
    true
  end
end
