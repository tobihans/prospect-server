# frozen_string_literal: true

class DynamicFormPolicy < ApplicationPolicy
  include PolicyConcerns::RecordWithOrganization
end
