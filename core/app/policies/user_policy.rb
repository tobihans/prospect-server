# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def show?
    true
  end

  def me?
    true
  end

  def missing_organization?
    true
  end

  def new?
    user.can?("user.manage")
  end

  def update?
    return false if non_admin_making_admins?

    create?
  end

  def create?
    user.can?("user.manage") # dont pass record because the user is still part of maybe another organization!
  end

  def toggle_archive?
    create? && user != record
  end

  def accept_invitation?
    true
  end

  def reject_invitation?
    true
  end

  def resend_invitation?
    user.admin? && record.invited?
  end

  def invitation?
    registering_invited_user?
  end

  def register?
    registering_invited_user?
  end

  def invitations?
    true
  end

  def accept_terms?
    true
  end

  def show_invitation_url?
    user.admin?
  end

  def switch_organization?
    true
  end

  private

  def non_admin_making_admins?
    record.admin? && !user.admin?
  end

  def registering_invited_user?
    user.nil? && record.invited?
  end
end
