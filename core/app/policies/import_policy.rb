# frozen_string_literal: true

class ImportPolicy < ApplicationPolicy
  include PolicyConcerns::RecordWithOrganization

  def show?
    can_view_organization_resource?
  end

  def index?
    can_view_organization_resource?
  end

  def download?
    user.can?("data.download")
  end

  def reprocess?
    user.admin?
  end

  def destroy?
    user.can?("source.delete", record:)
  end
end
