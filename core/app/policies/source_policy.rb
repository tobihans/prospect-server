# frozen_string_literal: true

class SourcePolicy < ApplicationPolicy
  include PolicyConcerns::RecordWithOrganization

  def show?
    can_view_organization_resource?
  end

  def show_secret?
    user.can?("source.manage", record:)
  end

  def destroy?
    user.can?("source.delete", record:)
  end

  def create?
    user.can?("source.manage")
  end

  def update?
    user.can?("source.manage", record:)
  end

  def manual_upload?
    user.can?("source.upload_data", record:) && record.kind == "manual_upload"
  end

  def trigger_now?
    user.admin? && record.adapter.pull?
  end

  def backfill?
    user.admin? && record.adapter.backfill_possible?
  end

  def backfill_completely?
    user.admin? && record.adapter.capability?(:backfill_completely)
  end

  def backfill_from_date?
    user.admin? && record.adapter.capability?(:backfill_from_date)
  end

  def delete_data_and_imports?
    user.admin?
  end

  def reprocess?
    user.admin?
  end

  def update_project?
    user.can?("source.manage", record:)
  end

  def edit_project?
    update_project?
  end

  def public_recorded?
    true
  end

  def public_recorded_upload?
    true
  end

  def view_data?
    user.can?("data.download", record:)
  end

  def csv_update?
    user.can?("source.update_data", record:)
  end

  def csv_update_submit?
    csv_update?
  end

  def dynamic_form_template?
    true
  end

  def dynamic_form?
    record.active? &&
      (record.adapter.try(:public_access?) || user&.can?("source.upload_data"))
  end

  def dynamic_form_submit?
    dynamic_form?
  end
end
