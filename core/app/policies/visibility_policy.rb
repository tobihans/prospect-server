# frozen_string_literal: true

class VisibilityPolicy < ApplicationPolicy
  def show?
    true
  end

  def new?
    may_manipulate?
  end

  def edit?
    may_manipulate?
  end

  def create?
    may_manipulate?
  end

  def update?
    may_manipulate?
  end

  def destroy?
    may_manipulate?
  end

  def may_manipulate?
    user.can?("visibility.manage", record:)
  end
end
