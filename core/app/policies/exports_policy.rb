# frozen_string_literal: true

class ExportsPolicy < ApplicationPolicy
  def index?
    user.can?("data.download")
  end

  def export?
    user.can?("data.download")
  end

  def export_form?
    user.can?("data.download")
  end
end
