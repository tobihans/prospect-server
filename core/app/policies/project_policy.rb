# frozen_string_literal: true

class ProjectPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    user.admin? || record_belongs_to_user_organization?
  end

  def new?
    user.can?("project.manage")
  end

  def edit?
    user.can?("project.manage", record:)
  end

  def create?
    user.can?("project.manage", record:)
  end

  def update?
    user.can?("project.manage", record:)
  end

  def destroy?
    user.can?("project.delete", record:)
  end

  def view_data_rbf?
    user.can?("data.download", record:) && user.can?("source.update_data", record:)
  end

  def view_data?
    user.can?("data.download", record:)
  end
end
