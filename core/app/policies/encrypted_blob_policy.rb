# frozen_string_literal: true

class EncryptedBlobPolicy < ApplicationPolicy
  def show?
    user.can?("data.download")
  end
end
