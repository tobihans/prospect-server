# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_claim_verifiers
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  rbf_claim_id :bigint           not null
#  user_id      :bigint
#  verifier_id  :bigint           not null
#
# Indexes
#
#  index_rbf_claim_verifiers_on_rbf_claim_id_and_verifier_id  (rbf_claim_id,verifier_id) UNIQUE
#
class RbfClaimVerifier < ApplicationRecord
  include OwningOrganization
  include Creator

  belongs_to :rbf_claim
  belongs_to :verifier, class_name: "User"

  validates :rbf_claim, uniqueness: { scope: :verifier }

  def owning_organization
    rbf_claim&.organization
  end

  def to_s
    "#{verifier} can verify claims of #{rbf_claim}"
  end
end
