# frozen_string_literal: true

# == Schema Information
#
# Table name: shared_keys_vaults
#
#  id                          :bigint           not null, primary key
#  encrypted_key               :string           not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  organization_id             :bigint           not null
#  shared_with_organization_id :bigint           not null
#  user_id                     :bigint
#
# Indexes
#
#  idx_on_organization_id_shared_with_organization_id_41045e9954  (organization_id,shared_with_organization_id) UNIQUE
#  index_shared_keys_vaults_on_user_id                            (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#  fk_rails_...  (shared_with_organization_id => organizations.id)
#  fk_rails_...  (user_id => users.id)
#
class SharedKeysVault < ApplicationRecord
  include Creator
  include OwningOrganization

  belongs_to :organization
  belongs_to :shared_with_organization, class_name: "Organization"

  validates :organization_id, uniqueness: { scope: %i[shared_with_organization_id] }

  def owning_organization
    organization
  end

  def self.organizations_to_share(organization)
    # TODO: only show organization that actually have a encryption key
    orgs_for_sharing = organization.organization_groups.
                       map { _1.organization_group_members.visible_for_sharing }.
                       flatten.map(&:organization).
                       uniq

    already_shared = organization.shared_keys.map(&:shared_with_organization)

    (orgs_for_sharing - already_shared - [organization])
  end
end
