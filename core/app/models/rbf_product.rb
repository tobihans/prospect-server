# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_products
#
#  id                    :bigint           not null, primary key
#  manufacturer          :string           not null
#  model                 :string           not null
#  rated_power_w         :integer
#  rbf_category          :string           not null
#  technology            :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  organization_id       :bigint           not null
#  rbf_claim_template_id :bigint           not null
#  user_id               :bigint
#
# Indexes
#
#  idx_on_model_organization_id_manufacturer_c8532bbe4b  (model,organization_id,manufacturer) UNIQUE
#  index_rbf_products_on_organization_id                 (organization_id)
#  index_rbf_products_on_rbf_claim_template_id           (rbf_claim_template_id)
#  index_rbf_products_on_user_id                         (user_id)
#
class RbfProduct < ApplicationRecord
  include Creator

  belongs_to :organization
  belongs_to :rbf_claim_template
  has_many :rbf_product_prices, dependent: :destroy

  validates :manufacturer, :model, :rbf_category, presence: true

  validates :model, uniqueness: { scope: %i[organization_id manufacturer], case_sensitive: false }
  validate :validate_easp_rbf_category

  accepts_nested_attributes_for :rbf_product_prices, allow_destroy: true

  scope :with_prices, -> { includes(:rbf_product_prices).where.not(rbf_product_prices: { id: nil }) }

  auto_strip_attributes :manufacturer, :model, :technology, :rbf_category, squish: true

  def to_s
    "#{manufacturer} #{model}"
  end

  private

  def validate_easp_rbf_category
    allowed = rbf_claim_template&.valid_product_categories

    return if allowed.blank?

    return if rbf_category.in? allowed

    errors.add :rbf_category, "must be one of: #{allowed.join(', ')}"
  end
end
