# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_group_members
#
#  id                    :bigint           not null, primary key
#  visible_for_sharing   :boolean          default(FALSE), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  organization_group_id :bigint           not null
#  organization_id       :bigint           not null
#  user_id               :bigint
#
# Indexes
#
#  idx_on_organization_group_id_organization_id_3f94b906ec    (organization_group_id,organization_id) UNIQUE
#  index_organization_group_members_on_organization_group_id  (organization_group_id)
#  index_organization_group_members_on_organization_id        (organization_id)
#
class OrganizationGroupMember < ApplicationRecord
  include Creator

  belongs_to :organization
  belongs_to :organization_group

  validates :organization_id, uniqueness: { scope: :organization_group_id }

  scope :visible_for_sharing, -> { where(visible_for_sharing: true) }

  after_commit do
    RbfViewsWorker.perform_async
  end

  after_destroy_commit do
    RbfViewsWorker.perform_async
  end
end
