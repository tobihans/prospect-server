# frozen_string_literal: true

# == Schema Information
#
# Table name: visibilities
#
#  id              :bigint           not null, primary key
#  columns         :jsonb            not null
#  conditions      :jsonb            not null
#  data_from       :datetime
#  data_until      :datetime
#  license         :string
#  resharing       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer          not null
#  project_id      :bigint           not null
#  user_id         :integer
#
# Indexes
#
#  index_visibilities_on_organization_id                 (organization_id)
#  index_visibilities_on_project_id_and_organization_id  (project_id,organization_id) UNIQUE
#  index_visibilities_on_user_id                         (user_id)
#
class Visibility < ApplicationRecord
  include Visibility::DataTableViews
  include Visibility::CustomViews
  include Creator
  include OwningOrganization

  LICENCES = ["ASCENT - Data Sharing License", "Carbon Climate Aggregation License"].freeze

  VIEW_COLUMNS = %i[source_name source_organization_name source_project_name source_project_id].freeze

  belongs_to :organization
  belongs_to :project
  has_many :sources, through: :project
  has_one :project_organization, through: :project, source: :organization

  scope :for_organization, ->(organization) { where organization_id: organization.id }
  scope :shared_to_organization, lambda { |organization|
    joins(:project).where.not(projects: { organization_id: organization.id }).for_organization(organization)
  }

  scope :includes_for_view_creation, -> { includes(:organization, :project, :sources, :project_organization) }

  validates :organization_id, uniqueness: { scope: :project_id }
  validates :data_until, comparison: { greater_than: :data_from }, if: ->(v) { v.data_from && v.data_until }

  after_commit do
    [organization_id, organization_id_previously_was].compact.uniq.each do |organization_id|
      Visibility.async_views_refresh! organization_id:
    end
  end

  after_destroy_commit do
    [organization_id, organization_id_previously_was].compact.uniq.each do |organization_id|
      Visibility.async_views_refresh! organization_id:
    end
  end

  before_validation do
    sanitize_columns
    sanitize_data_time_range
  end

  def self.async_views_refresh!(organization_id: nil)
    if Rails.env.production?
      VisibilityViewsRefreshWorker.perform_async organization_id
    else
      VisibilityViewsRefreshWorker.new.perform organization_id
    end
  end

  def self.full_views_refresh!(organization_id: nil)
    Postgres::RemoteControl.create_visibilities_schema!
    create_default_visibility_views!

    create_visibility_views!(organization_id:)
    create_organization_views!(organization_id:)
    create_custom_views!(organization_id:)
    cleanup_data_views!
    cleanup_custom_views!
  end

  # nice helper function to create a lot ov views in parallel
  def self.parallel_each(items)
    workers = ActiveRecord::Base.connection_pool.size - 2
    workers = 0 if workers.negative? || Rails.env.test? # https://github.com/grosser/parallel/issues/152
    items_c = items.count

    Parallel.each_with_index(items.to_a, in_threads: workers) do |item, index|
      ActiveRecord::Base.connection_pool.with_connection do
        Rails.logger.debug { "PARALLEL EACH: #{Parallel.worker_number} / #{workers} INDEX: #{index} / #{items_c}" }
        yield item
      end
    end
  end

  def self.create_default!(project)
    Visibility.create! project:, organization: project.organization, columns: {}
  end

  def refresh_views!(async: true)
    if async
      Visibility.async_views_refresh!(organization_id:)
    else
      Visibility.full_views_refresh!(organization_id:)
    end
  end

  def default?
    project_organization.id == organization_id
  end

  def all_data?
    default? || share_everything?
  end

  def share_everything
    self.columns = { "share_everything" => true }
  end

  def share_everything?
    columns["share_everything"]
  end

  def to_s
    "Visibility for #{project} to #{organization}"
  end

  def to_s_columns
    if all_data?
      "All data"
    elsif visible_dt_names.present? || custom_view_names.present?
      tables = (visible_dt_names + custom_view_names).sort.join(", ")
      "#{visible_columns_total} columns from #{tables}"
    else
      "No columns shared"
    end
  end

  def visible_columns_total
    data_table_columns.values.map(&:count).sum +
      custom_table_columns.values.map(&:count).sum
  end

  # used by both data table views and custom views to put meta info into resulting views
  # when source is nil it build empty fields for use in default view
  def self.default_fields_sql_arr(source)
    source_name, source_organization_name, source_project_name, source_project_id =
      if source
        [
          sql_quote(source.name),
          sql_quote(source.project.organization.name),
          sql_quote(source.project.name),
          source.project.id
        ]
      else
        %w['' '' '' NULL::numeric]
      end

    [
      "#{source_name} AS source_name",
      "#{source_organization_name} AS source_organization_name",
      "#{source_project_name} AS source_project_name",
      "#{source_project_id} AS source_project_id"
    ]
  end

  # prevent sql injection/breakage by properly quoting string values
  def self.sql_quote(str)
    "'#{ActiveRecord::Base.connection.quote_string(str)}'"
  end

  def owning_organization
    project_organization
  end

  private

  # sanitize from form like {test: {col: 1, col2: 1}} to { test: [col, col2]}
  def sanitize_columns
    columns.transform_values! do |cols|
      case cols
      when Hash
        # checkbox input stoff, reject deselected, make it column array
        cols.reject { |_k, v| v == "0" }.keys
      when Array
        cols
      when nil
        []
      when true
        true
      else
        raise "Can not sanitize column data of type #{cols.class}"
      end
    end
    columns.compact_blank!
  end

  def sanitize_data_time_range
    self.data_from = data_from&.beginning_of_day
    self.data_until = data_until&.end_of_day
  end
end
