# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_claim_templates
#
#  id                          :bigint           not null, primary key
#  currency                    :string           not null
#  name                        :string
#  trust_trace_check           :string           not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  managing_organization_id    :bigint           not null
#  organization_group_id       :bigint           not null
#  supervising_organization_id :bigint           not null
#  user_id                     :bigint           not null
#  verifying_organization_id   :bigint           not null
#
# Indexes
#
#  index_rbf_claim_templates_on_managing_organization_id     (managing_organization_id)
#  index_rbf_claim_templates_on_name                         (name) UNIQUE
#  index_rbf_claim_templates_on_organization_group_id        (organization_group_id)
#  index_rbf_claim_templates_on_supervising_organization_id  (supervising_organization_id)
#  index_rbf_claim_templates_on_user_id                      (user_id)
#  index_rbf_claim_templates_on_verifying_organization_id    (verifying_organization_id)
#
class RbfClaimTemplate < ApplicationRecord
  include Creator

  VALID_PRODUCT_CATEGORIES = {
    rbf_easp_ogs_is_eligible: %w[A B],
    rbf_easp_ccs_is_eligible: %w[Ai Aii Aiii B C D Ei Eii F],
    rbf_easp_pue_is_eligible: %w[A B C D E F]
  }.with_indifferent_access.freeze

  VERIFICATION_CATEGORY_PERCENTAGES = {
    rbf_easp_ogs_is_eligible: { default: "desk", "phone" => { min: 5, max: 10 } },
    rbf_easp_ccs_is_eligible: { default: "desk", "phone" => { min: 5, max: 10 } },
    rbf_easp_pue_is_eligible: { default: "desk", "field" => { min: 100, max: 100 } }
  }.with_indifferent_access.freeze

  # allow to adjust subsidy in the form based on entered data...
  SUBSIDY_ADJUSTMENTS = {
    rbf_easp_pue_is_eligible: { female_owned_business: { "true" => 50_000, "false" => 0 } }
  }.with_indifferent_access.freeze

  CLAIM_SUBMISSION_MINIMUMS = {
    rbf_easp_ogs_is_eligible: 500, rbf_easp_ccs_is_eligible: 200, rbf_easp_pue_is_eligible: 10
  }.with_indifferent_access.freeze

  belongs_to :organization_group
  belongs_to :supervising_organization, class_name: "Organization"
  belongs_to :managing_organization, class_name: "Organization"
  belongs_to :verifying_organization, class_name: "Organization"
  has_many :organization_group_members, through: :organization_group
  has_many :organizations, through: :organization_group_members
  has_many :rbf_organization_configs, dependent: :destroy
  has_many :dynamic_forms, dependent: :nullify
  has_many :rbf_products, dependent: :destroy
  has_many :rbf_claims, dependent: :destroy

  validates :name, :trust_trace_check, :currency, presence: true
  validate :validate_all_organizations_in_group

  scope :controlled_by_organization, lambda { |org|
    where(supervising_organization: org).or(
      where(managing_organization: org).or(
        where(verifying_organization: org)
      )
    )
  }

  scope :default_order, -> { order name: :asc }

  auto_strip_attributes :name, :trust_trace_check, :currency, squish: true

  after_commit do
    RbfViewsWorker.perform_async
  end

  after_destroy_commit do
    RbfViewsWorker.perform_async
  end

  delegate :to_s, to: :name

  def non_submitting_organizations
    [supervising_organization, managing_organization, verifying_organization]
  end

  def organization_can_create_claims?(organization)
    non_submitting_organizations.exclude?(organization) &&
      organizations.include?(organization)
  end

  def organization_involved?(organization)
    non_submitting_organizations.include?(organization) ||
      organizations.include?(organization)
  end

  def valid_product_categories
    VALID_PRODUCT_CATEGORIES[trust_trace_check]
  end

  def default_verification_category
    VERIFICATION_CATEGORY_PERCENTAGES[trust_trace_check][:default]
  end

  def verification_categories_percentages
    VERIFICATION_CATEGORY_PERCENTAGES[trust_trace_check]
  end

  def subsidy_adjustments
    SUBSIDY_ADJUSTMENTS[trust_trace_check].to_h
  end

  def claim_submission_minimum
    CLAIM_SUBMISSION_MINIMUMS[trust_trace_check] || 0
  end

  def validate_all_organizations_in_group
    return unless organization_group

    %i[managing_organization supervising_organization verifying_organization].each do |attr|
      org = public_send attr
      next unless org

      errors.add attr, "must be part of Organization Group" unless org.in?(organizations)
    end
  end

  def self.full_views_refresh!
    Organization.find_each do |org|
      summary_view_q = organization_summary_view_q org
      Postgres::RemoteControl.create_view_safe! summary_view_q

      config_view_q = organization_rbf_config_view_q org
      Postgres::RemoteControl.create_view_safe! config_view_q
    end
  end

  def self.organization_views_where_q(organization)
    rcts = organization.rbf_claim_templates

    organization_ids = rcts.to_h do |rct|
      o_ids = if organization.in?(rct.non_submitting_organizations)
                rct.organizations.pluck(:id)
              else
                [organization.id]
              end
      [rct.id, o_ids]
    end

    if organization_ids.blank?
      "FALSE"
    else
      organization_ids.map do |rct_id, o_ids|
        "(rct.id = #{rct_id} AND o.id IN (#{o_ids.join(',')}))"
      end.join(" OR ")
    end
  end

  def self.organization_summary_view_q(organization)
    <<~SQL.squish
      CREATE OR REPLACE VIEW #{Naming.db_organization_rbf_summary_view(organization)} AS
      SELECT
        rdc.id as rbf_device_claim_id,
        rdc.subject_origin AS subject_origin,
        rdc.subject_uid AS subject_uid,
        rdc.trust_trace_uid AS trust_trace_uid,
        rdc.device_category AS device_category,

        rdc.submission_category AS submission_category,
        rdc.verification_category AS verification_category,
        rdc.rejection_category AS rejection_category,
        rdc.reviewer_comment AS reviewer_comment,
        rdc.state AS state,
        rdc.claimed_subsidy AS claimed_subsidy,
        rdc.claimed_incentive AS claimed_incentive,
        rdc.verified_subsidy AS verified_subsidy,
        rdc.verified_incentive AS verified_incentive,

        rc.id AS claim_id,
        rc.code AS claim_code,
        rc.aasm_state AS claim_state,
        rc.created_at AS claim_created_at,
        rc.updated_at AS claim_updated_at,
        rc.approved_at as claim_approved_at,
        rc.paid_out_at as claim_paid_out_at,
        rc.rejected_at as claim_rejected_at,
        rc.submitted_to_program_at as claim_submitted_to_program_at,
        rc.verification_at as claim_verification_at,

        o.id AS organization_id,
        o.name AS organization_name,

        rct.id AS rbf_claim_template_id,
        rct.name AS rbf_claim_template_name
      FROM rbf_device_claims rdc
        LEFT JOIN rbf_claims rc ON rc.id = rdc.rbf_claim_id
        LEFT JOIN rbf_claim_templates rct ON rct.id = rc.rbf_claim_template_id
        LEFT JOIN organizations o ON o.id = rc.organization_id
      WHERE #{organization_views_where_q(organization)}
    SQL
  end

  def self.organization_rbf_config_view_q(organization)
    <<~SQL.squish
      CREATE OR REPLACE VIEW #{Naming.db_organization_rbf_config_view(organization)} AS
      SELECT
        roc.organization_id AS organization_id,
        roc.code AS organization_code,
        roc.total_grant_allocation AS total_grant_allocation,
        roc.rbf_claim_template_id AS rbf_claim_template_id,

        o.name AS organization_name,
        rct.name AS rbf_claim_template_name
      FROM rbf_organization_configs roc
        LEFT JOIN organizations o ON o.id = roc.organization_id
        LEFT JOIN rbf_claim_templates rct ON rct.id = roc.rbf_claim_template_id
      WHERE #{organization_views_where_q(organization)}
    SQL
  end

  def self.valid_product_categories_hint
    shorter = VALID_PRODUCT_CATEGORIES.transform_keys { _1.to_s.match(/rbf_easp_([^_]+)_/).captures.first.upcase }
    nicer = shorter.map { |k, v| "#{k}: #{v.join(', ')}" }.join(" / ")
    "Valid Categories are: #{nicer}"
  end
end
