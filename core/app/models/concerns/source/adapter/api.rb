# frozen_string_literal: true

class Source
  module Adapter
    module Api
      extend ActiveSupport::Concern

      DETAILS_KEYS_FOR_DUPLICATE_CHECK = %w[username app_id app_key].freeze

      #
      # this Concern is to be included in adapters which allow the user to import data from predefined apis
      #

      included do
        include Source::Adapter

        def duplicate_fingerprint
          details.slice(*DETAILS_KEYS_FOR_DUPLICATE_CHECK).values
        end

        def data_schema
          source.kind
        end
      end
    end
  end
end
