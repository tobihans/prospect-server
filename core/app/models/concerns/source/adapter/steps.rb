# frozen_string_literal: true

class Source
  module Adapter
    module Steps
      extend ActiveSupport::Concern
      ##
      # error handling and protocol generation for adapters
      #
      # Basic Idea. Declare a step, do some actions and log messages
      # until next step declared all log messages and failures will be regarded as part of the current step
      # A Step is successful if next step is created and fail was not called
      # To get the result you can get a log of all steps and their output as Array protocol, or as text with protocol_text
      # Also check if things failed with failed? and error (string)
      #
      # Usage:
      # step "Good Step"
      # log "Good message"
      #
      # step nil  # until next step log messages will not be nested
      # log "Un-nested Message"
      #
      # step "Some Step"
      # if do_something(data)
      #   log "Something looks good #{something}"
      # else
      #   fail! "My Error Message" and return  # <- jumping out of flow must still be done manually
      # end
      #
      # Will generate, if do_something returns false..
      #   protocol:
      #     Good Step ... OK
      #       Good Message
      #     Un-nested Message
      #     Some Step ... ERROR
      #       My Error Message
      #     >>> ERROR
      #   error:
      #     Some Step: My Error Message
      #

      included do
        attr_accessor :error

        # Add a log message to the current step
        delegate :log, to: :current_step, allow_nil: true

        # Fail the pipeline, set error (prefixed with current step name) and mark current step as failed
        # You still need to exit your control flow seperately
        # if skip is true, it will only mark the step as failed, but not set the global error
        #
        def fail!(msg, skip: false)
          msg = msg.message if msg.is_a? Exception

          current_step.fail! msg

          return if skip

          msgs = Array(msg).map(&:to_s).join(" / ").truncate(200)
          self.error = [current_step.name, msgs].compact.join(": ")
        end

        # check if the pipeline is failed (globally)
        def failed?
          error.present?
        end

        # create or return the current Step
        def current_step
          steps.last || step(nil)
        end

        # List all steps which have taken place
        def steps
          @steps ||= []
        end

        # create a current step with given name
        def step(name)
          steps << Step.new(name)
          current_step
        end

        # get the log of all steps as Array of strings
        def protocol
          steps.map(&:full).flatten + [failed? ? ">>> ERROR" : ">>> SUCCESS"]
        end

        def reset
          @steps = nil
          @error = nil
        end
      end

      # Wrapper class for a Step
      class Step
        attr_reader :name, :failed
        alias failed? failed

        def initialize(name)
          @name = name
          @log = []
        end

        # add a message to the step log
        def log(msg)
          case msg
          when Array
            msg.each { log _1 }
          when nil
            nil
          else
            Rails.logger.debug msg.to_s.truncate(200)
            @log << msg.to_s.truncate(200)
          end
        end

        # set the step as failed
        def fail!(msg)
          @failed = true
          log msg
        end

        # get full log of step as array of strings
        def full
          header = "#{name} ... #{failed? ? 'ERROR' : 'OK'}" if name
          subs = @log.map { |l| "#{name ? '  ' : ''}#{l}" }

          subs.unshift(header).compact
        end
      end
    end
  end
end
