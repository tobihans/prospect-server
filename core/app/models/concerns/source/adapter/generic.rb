# frozen_string_literal: true

class Source
  module Adapter
    module Generic
      extend ActiveSupport::Concern
      include ActionView::Helpers::TagHelper

      #
      # this Concern is to be included in adapters which allow the user to import data in one of our standard schemas
      #

      included do
        include Source::Adapter

        def data_schema
          "generic"
        end

        def data_origin
          "generic"
        end

        def data_categories
          Source::DATA_CATEGORIES
        end

        def data_table
          Postgres::DataTable.new source.data_category
        end

        def convert_to_hashes(data)
          return data if data.is_a? Array # nothing to see here :)

          case details["file_format"].to_s.to_sym
          when :csv
            CSV.parse(data, headers: true).map(&:to_h)
          when :json
            parsed = JSON.parse data
            raise "Data must be an Array" unless parsed.is_a?(Array)

            parsed
          when :xlsx
            excel = Roo::Excelx.new StringIO.new(data), extension: :xlsx
            excel.parse headers: :first_row, clean: true
          else
            raise "#{details['file_format']} is not supported"
          end
        rescue => e
          raise "Failed to parse file: #{e.message}"
        end

        def process_data(new_data)
          data = ingest(new_data)

          # for dataworker next generic importer, nest the data into json key of data_category
          data = { source.data_category => data } if data.present?

          Result.new format: :json, data:, protocol:, error:, hint: nil, more_data: nil
        end

        def retrieve_data(since = nil)
          raw, more_data, hint = fetch(since)

          data = ingest raw unless failed? || raw.nil?

          Result.new format: :json, data:, more_data:, hint:, protocol:, error:
        end

        def ingest(data, table: data_table)
          step nil
          begin
            if data.blank?
              fail! "No data"
              return
            end
            step "Checking data file format: #{details['file_format']}"

            as_hashes = convert_to_hashes(data)
          rescue => e
            fail! e.message
            return
          end
          step "Checking schema is conforming to: #{table.data_category}"
          caster = table.caster as_hashes
          fail! caster.errors and return unless caster.valid?

          casted = caster.data
          step "Applying cleaning"
          begin
            table.apply_cleaning! casted, source.organization
          rescue => e
            fail! e.message
            return
          end

          step "Checking data presence"

          fail! "No data" and return unless casted.count.positive?

          log "Counted #{casted.count} rows"

          if source.custom_data? && (source.inactive? || source.details["custom_columns"].blank?)
            step "Persisting custom structure"
            cu_co_conf = table.custom_column_conf(casted)
            cu_co_errors = table.custom_column_conf_errors(cu_co_conf)

            fail! cu_co_errors.join(" ") and return if cu_co_errors.present?

            source.details["custom_columns"] = cu_co_conf
            source.save!
            source.details["custom_columns"].each do |col_name, postgres_type|
              log "#{col_name} => #{postgres_type}"
            end
          end

          casted
        end
      end
    end
  end
end
