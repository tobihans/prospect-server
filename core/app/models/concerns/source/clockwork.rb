# frozen_string_literal: true

class Source
  module Clockwork
    extend ActiveSupport::Concern

    included do
      ## clockwork methods
      #
      # basic scheduling idea:
      # - everything longer than hourly runs at 03:some minute
      # - everything runs at minute (id mod 60) to spread the load
      # - monthly: run on first of month
      # - weekly: run on sunday
      # - skip_first_run always to not double execute thing when deploying at wrong time
      # - timing is UTC
      #

      def frequency
        freq = adapter_frequency.to_s

        if freq == "hourly"
          1.hour
        elsif freq.include? "minutes"
          # asume format like "2.minutes", avoid eval for security reasons
          minutes = freq.split(".").first.to_i # parse int part
          minutes.minutes
        else
          1.day
        end
        # weekly,... are managed by if
      end

      def at
        freq = adapter_frequency.to_s

        hour = "03"
        minute = (id % 60).to_s.rjust(2, "0")

        if freq == "hourly"
          "**:#{minute}"
        elsif freq.include? "minutes"
          nil # TODO: "**:**" not supported?
        else
          "#{hour}:#{minute}"
        end
      end

      def if?(time = Time.zone.now)
        return false unless active? && frequency

        freq = adapter_frequency.to_s

        return true if freq.include? "minutes"

        case freq
        when "monthly"
          time.day == 1
        when "weekly"
          time.monday?
        when "daily", "hourly" # at should be enough here
          true
        else
          false
        end
      end

      def adapter_frequency
        details["frequency"] || adapter_class::DEFAULT_FREQUENCY
      end

      def tz
        "UTC"
      end

      # we only skip the first run if there are already imports
      # to avoid running all imports on a clockworks restart
      def skip_first_run
        false
      end
    end
  end
end
