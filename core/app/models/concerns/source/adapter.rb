# frozen_string_literal: true

class Source
  module Adapter
    extend ActiveSupport::Concern
    # the very basics of an adapter, used for both fixed APIs as well as user provided connections

    AUTOGENERATE_SECRET = false

    DEFAULT_FREQUENCY = nil

    # The Result Data Class - principal result of "running" the adapter - returned by retrieve_data and process_data
    # Data Classes are immutable Value Objects since 3.2. https://justin.searls.co/links/2022-11-23-ruby-3-2-adds-a-new-data-class/
    Result = Data.define(
      :format,   # one of :json, :txt, :csv
      :data,     # the processed data
      :error,    # a String which - when set will mark the import as failed (optional)
      :protocol, # An array of strings which describe steps taken and their result,
      :hint,     # a hint that is used as since parameter on next call (optional)
      :more_data # boolean, true if the adapter has more data that could be retrieved on a subsequent call (optional)
    ) do
      def initialize(**args)
        if args[:protocol].is_a? Array
          protocol_s = args[:protocol].join("\n")
          super(**args.merge(protocol: protocol_s))
        else
          super
        end
      end

      def ok?
        error.blank?
      end
    end

    included do
      include Source::Adapter::Steps

      attr_accessor :raw_data, :validation_mode, :import
      attr_reader :source

      def initialize(source)
        @source = source
      end

      delegate :details, to: :source
      delegate :details_struct, to: :source

      def pull?
        self.class::PULL
      end

      def push?
        !pull?
      end

      def configure_step?
        false
      end

      def data_origin
        raise "Implement ME!"
      end

      def data_schema
        raise "Implement ME"
      end

      #
      # Signals different capabilities of the Adapter. Some capabilities can change at runtime.
      #
      # We should add new capabilities here and might migrate others to this data structure. e.g PUSH/PULL m
      #
      def capabilities
        {
          backfill_completely: false,
          # if backfill_from_date: true, adapter needs to implement
          # backfill_from_date_import_hint(from_date) which should return import_hint (String) for given from_date (Date)
          backfill_from_date: false
        }
      end

      def capability?(key)
        capabilities[key] == true
      end

      #
      # return array of at least one data category
      # if more than one category is returned, the UI renders an element to choose one category
      #
      def data_categories
        raise "Implement ME"
      end

      #
      # wrappes validation_result, rescues from exceptions and creates a failed result object
      #
      def validation_result_wrapped
        validation_result
      rescue => e
        fail! e.message
        Rails.logger.error e
        Sentry.capture_exception e
        Result.new format: :json, data: nil, protocol:, error:, hint: nil, more_data: nil
      end

      #
      # Executes PULL - actual api call/ retrieve operation.
      # probably calls ingest
      #
      # Should return a Result
      #
      def retrieve_data(_since = nil)
        raise "Implement ME"
        # looks ideally like:
        #   raw, more_data, hint = fetch(since)
        #   data = ingest raw unless failed?
        #   Result.new format: :csv, data:, ...
      end

      #
      # Executes PUSH data ingestion, called directly for PUSH sources
      # probably calls ingest
      #
      # Should return a new Result
      #
      def process_data(_raw_data)
        raise "Implement ME"
        # looks ideally like:
        #   data = ingest raw_data
        #   Result.new format: :json, data:, ...
      end

      #
      # This method handles the data: casts, validates and pseudonymizes it.
      # input: raw data
      # output: cleaned data
      #
      def ingest(_raw_data)
        raise "Implement ME"
      end

      #
      # This method fetches raw data since the last_import_hint
      # should return an array of
      #   raw - raw data fetched
      #   more_data - notify if there is more data available to fetch in a later call
      #   hint - mark how we should fetch data on the next run
      #
      def fetch(_since)
        raise "Implement ME"
        # returns something alike
        #   [raw, more_data, hint]
      end

      #
      # Default implementation for backfill completely. This is used when adapter sets capability backfill_completely = true
      #
      def backfill_completely_import_hint
        nil
      end

      #
      # Helper method for UI
      #
      def backfill_possible?
        capability?(:backfill_completely) || capability?(:backfill_from_date)
      end

      #
      # Adapter specific validation can be implement here. It is called before the corresponding source is saved to DB.
      #
      def adapter_constraints; end

      # list of values that defines a unique source, used in validate_unique_source
      def duplicate_fingerprint
        false
      end

      #
      # Avoid source duplication. We don't allow the multiple use of the same account.
      # define in duplicate_fingerprint
      #
      def validate_unique_source
        return unless duplicate_fingerprint # skip validation for adapters where fingerprint returns false / nil
        return unless source.organization # in some tests the project did not yet set the org

        dup = Source.potential_duplicates(source).to_a.find { _1.duplicate_fingerprint == duplicate_fingerprint }
        source.errors.add :base, "is a duplicate of an existing Source with the same properties: #{dup}" if dup
      end
    end
  end
end
