# frozen_string_literal: true

module Currencify
  extend ActiveSupport::Concern

  include ActiveSupport::NumberHelper

  included do
    def self.stringify_with_currency(*method_names, suffix: "_s", currency_method: :currency)
      method_names.each do |method_name|
        define_method "#{method_name}#{suffix}" do
          value = send method_name
          stringify_with_currency(value, currency_method:)
        end
      end
    end

    def stringify_with_currency(value, currency_method: :currency)
      return unless value

      currency = send currency_method
      number_to_currency value, unit: currency, strip_insignificant_zeros: true
    end
  end
end
