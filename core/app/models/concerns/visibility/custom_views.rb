# frozen_string_literal: true

class Visibility
  module CustomViews
    extend ActiveSupport::Concern
    included do
      def self.create_custom_views!(organization_id: nil)
        vis = organization_id ? Visibility.where(organization_id:) : Visibility.all
        vis = vis.includes_for_view_creation

        vis.find_each(&:create_custom_views!)
      end

      def self.cleanup_custom_views!
        active_views = Visibility.includes_for_view_creation.map(&:custom_view_names_w_schema).flatten
        existing_views = Postgres::RemoteControl.all_custom_views_everywhere

        removable = existing_views - active_views

        removable.each { |v| Postgres::RemoteControl.drop_view! v }
      end

      def map_project_custom_sources(&)
        visible_custom_sources.map(&)
      end

      def visible_custom_sources
        configured_custom_views = custom_table_columns.keys
        project.sources.custom_data.select do |src|
          all_data? || Naming.db_custom_view(src).in?(configured_custom_views)
        end
      end

      def custom_table_columns
        columns.select { _1.start_with? Naming.db_custom_view_prefix(prefix: true) }
      end

      def custom_view_names
        map_project_custom_sources(&:custom_view)
      end

      def custom_view_names_w_schema
        map_project_custom_sources { Naming.db_visibility_custom_view self, _1 }
      end

      def create_custom_views!
        build_custom_view_qs.each { Postgres::RemoteControl.create_view_safe! _1 }
      end

      def build_custom_view_qs
        map_project_custom_sources { build_custom_view_q _1 }
      end

      def build_custom_view_q(source)
        start_q = "CREATE OR REPLACE VIEW #{Naming.db_visibility_custom_view(self, source)} AS SELECT"

        fields = self.class.default_fields_sql_arr source

        allowed_fields = columns[Naming.db_custom_view(source)].to_a

        custom_fields = source.custom_structure_h.map do |col_name, postgres_type|
          show_column = all_data? || col_name.in?(allowed_fields)
          base = if show_column
                   "(custom ->> '#{col_name}') AS \"#{col_name}\""
                 else
                   "(NULL) AS \"#{col_name}\""
                 end
          function = case postgres_type
                     when "numeric"
                       "try_cast_numeric"
                     when "timestamp"
                       "try_cast_timestamp"
                     else
                       ""
                     end
          function + base # e.g. try_cast_numeric(custom ->> 'voltage') AS "voltage"
        end

        fields += custom_fields

        Postgres::DataTable.new(:custom).columns.each do |c|
          c.sql_columns.each do |sql_c|
            show_column = all_data? || c.mandatory_visible? || sql_c.in?(allowed_fields)
            fields << "t.#{sql_c} AS #{sql_c}" if show_column
          end
        end

        fields_q = fields.join(", ")
        from_q = "FROM #{Naming.db_data_table(:custom)} AS t"

        conditions = []
        conditions << "source_id = #{source.id}"
        conditions << "created_at >= '#{data_from}'" if data_from
        conditions << "created_at <= '#{data_until}'" if data_until

        conditions_q = "WHERE #{conditions.join(' AND ')}"

        [
          start_q,
          fields_q,
          from_q,
          conditions_q
        ].join(" ").squish
      end
    end
  end
end
