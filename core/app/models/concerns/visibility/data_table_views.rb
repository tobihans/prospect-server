# frozen_string_literal: true

class Visibility
  module DataTableViews
    extend ActiveSupport::Concern
    included do
      def self.create_visibility_views!(organization_id: nil)
        vis = organization_id ? Visibility.where(organization_id:) : Visibility.all
        vis = vis.includes_for_view_creation

        parallel_each(vis, &:create_views!)
      end

      def self.create_organization_views!(organization_id: nil)
        existing_views = Postgres::RemoteControl.all_visibility_views(parse: true)

        orgs = organization_id ? Organization.where(id: organization_id) : Organization.all

        parallel_each(orgs) do |org|
          qs = Postgres::DataTable.all.map do |dt|
            table = dt.table_name
            relevant_views = existing_views.select { |v| v[:organization_id] == org.id && v[:table] == table }
            org_view = "#{Naming.db_organization_schema(org)}.#{table}"
            unionize_views_q dt, org_view, relevant_views.pluck(:view)
          end

          begin
            # run small Qs combined. Bigger seperately. Combining too many big queries can trigger Postgres SharedMemory error
            big_qs, small_qs = qs.partition { _1.size > 10_000 }
            Postgres::RemoteControl.create_views_unsafe! small_qs
            big_qs.each { Postgres::RemoteControl.create_views_unsafe! _1 }
          rescue => e
            raise unless Rails.env.production?

            Rails.logger.error "Failed to create union organization views unsafely: #{e.message}"
            Sentry.capture_exception e
            qs.each { Postgres::RemoteControl.create_view_safe! _1 }
          end
        end
      end

      def self.create_default_visibility_views!
        Postgres::DataTable.each do |dt|
          dt_q = build_dt_default_view_q(dt)
          Postgres::RemoteControl.create_view_safe! dt_q
        end
      end

      def self.unionize_views_q(datatable, org_view, visibility_views)
        create_q = "CREATE OR REPLACE VIEW #{org_view} AS"

        unionize = visibility_views + [Naming.db_visibility_view_default(datatable)]

        union_q = unionize.map do |v|
          "SELECT * FROM visibilities.#{v}"
        end.join(" UNION ALL ")

        [create_q, union_q].join(" ").squish
      end

      def self.cleanup_data_views!
        active_views = Visibility.includes_for_view_creation.map(&:view_names).flatten
        existing_views = Postgres::RemoteControl.all_visibility_views

        removable = existing_views - active_views

        removable.each { |v| Postgres::RemoteControl.drop_view! "visibilities.#{v}" }
      end

      def self.build_dt_default_view_q(datatable)
        conditions_sql = "WHERE FALSE"

        [
          build_dtv_start_sql(Naming.db_visibility_view_default_with_schema(datatable)),
          build_dtv_fields_sql(datatable, nil, :all),
          build_dtv_from_sql(datatable),
          conditions_sql
        ].join(" ").squish
      end

      def self.build_dtv_start_sql(view_name)
        "CREATE OR REPLACE VIEW #{view_name} AS SELECT"
      end

      def self.build_dtv_from_sql(datatable)
        "FROM #{datatable.table_name} AS t"
      end

      # allowed_fields must be either %w[array of fields] or :all (for default vis view)
      def self.build_dtv_fields_sql(datatable, source, allowed_fields)
        fields = default_fields_sql_arr source

        datatable.columns.each do |c|
          c.sql_columns.each do |sql_c|
            show_column = allowed_fields == :all || c.mandatory_visible? || sql_c.in?(allowed_fields)

            fields << if show_column
                        "t.#{sql_c} AS #{sql_c}"
                      else
                        # https://stackoverflow.com/questions/41864212/union-all-cast-null-as-double-precision-postgres
                        null_type = Postgres::RemoteControl.data_table_sql_types[datatable.table_name][sql_c]
                        "CAST(NULL AS #{null_type}) AS #{sql_c}"
                      end
          end
        end
        fields.join(", ")
      end

      def visible_dt_names
        all_data? ? Postgres::DataTable.all.map(&:table_name) : data_table_columns.keys
      end

      def visible_data_tables
        vis_t = visible_dt_names # cache method result
        Postgres::DataTable.all.select { _1.table_name.in? vis_t }
      end

      def data_table_columns
        columns.reject { _1.start_with? Naming.db_custom_view_prefix(prefix: true) }
      end

      def create_views!
        qs = build_views_qs
        begin
          Postgres::RemoteControl.create_views_unsafe! qs
        rescue => e
          raise unless Rails.env.production?

          Rails.logger.error "Failed to create visibility views unsafely: #{e.message}"
          Sentry.capture_exception e
          qs.each { Postgres::RemoteControl.create_view_safe! _1 }
        end
      end

      def map_datatables_and_sources
        vis_dt = visible_data_tables
        project.sources.map do |source|
          vis_dt.map do |dt|
            yield dt, source
          end
        end.flatten
      end

      def build_views_qs
        map_datatables_and_sources { |dt, source| build_dt_view_q dt, source }
      end

      def view_names
        map_datatables_and_sources { |dt, source| view_name dt, source }
      end

      def view_name(datatable, source)
        Naming.db_visibility_view self, source, datatable
      end

      def build_dt_view_q(datatable, source)
        conditions = []
        conditions << "source_id = #{source.id}"
        conditions << "created_at >= '#{data_from}'" if data_from
        conditions << "created_at <= '#{data_until}'" if data_until
        conditions_sql = "WHERE #{conditions.join(' AND ')}"

        allowed_fields = all_data? ? :all : columns[datatable.table_name].to_a

        [
          self.class.build_dtv_start_sql(Naming.db_visibility_view_with_schema(self, source, datatable)),
          self.class.build_dtv_fields_sql(datatable, source, allowed_fields),
          self.class.build_dtv_from_sql(datatable),
          conditions_sql
        ].join(" ").squish
      end
    end
  end
end
