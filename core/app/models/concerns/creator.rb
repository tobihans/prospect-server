# frozen_string_literal: true

module Creator
  extend ActiveSupport::Concern

  included do
    # storing the user with the record that created the record
    belongs_to :user, optional: true

    before_create :store_user

    def creator
      user
    end

    private

    def store_user
      self.user ||= Current.user
    end
  end
end
