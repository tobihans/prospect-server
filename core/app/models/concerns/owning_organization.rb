# frozen_string_literal: true

#
# Each record needs to know to which organization it belongs. Use this concern in anything that could be used as record in the policies.
#
module OwningOrganization
  extend ActiveSupport::Concern

  included do
    def owning_organization
      raise "implement me"
    end
  end
end
