# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_device_claims
#
#  id                         :bigint           not null, primary key
#  claimed_devices_data       :jsonb            not null
#  claimed_incentive          :integer          default(0)
#  claimed_payments_data      :jsonb            not null
#  claimed_subsidy            :integer          default(0)
#  claimed_trust_trace_data   :jsonb            not null
#  device_category            :string
#  rejection_category         :string
#  reviewer_comment           :text
#  state                      :string           not null
#  subject_origin             :string           not null
#  subject_uid                :string           not null
#  submission_category        :string
#  submission_comment         :text
#  trust_trace_uid            :string
#  verification_category      :string
#  verified_data              :jsonb            not null
#  verified_incentive         :integer          default(0)
#  verified_subsidy           :integer          default(0)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  parent_rbf_device_claim_id :bigint
#  rbf_claim_id               :bigint           not null
#  reviewer_id                :bigint
#  user_id                    :bigint           not null
#
# Indexes
#
#  index_rbf_device_claims_on_parent_rbf_device_claim_id      (parent_rbf_device_claim_id)
#  index_rbf_device_claims_on_rbf_claim_id                    (rbf_claim_id)
#  index_rbf_device_claims_on_reviewer_id                     (reviewer_id)
#  index_rbf_device_claims_on_subject_origin_and_subject_uid  (subject_origin,subject_uid)
#  index_rbf_device_claims_on_trust_trace_uid                 (trust_trace_uid)
#  index_rbf_device_claims_on_user_id                         (user_id)
#
class RbfDeviceClaim < ApplicationRecord
  include Creator
  include Currencify

  STATES = %w[submitted approved rejected].freeze
  FINAL_STATES = %w[approved rejected].freeze
  SUBMISSION_CATEGORIES = %w[initial_claim resubmission repossessed].freeze
  VERIFICATION_CATEGORIES = %w[desk phone field].freeze
  DEFAULT_REJECTION_CATEGORIES = %w[broken missing_data duplicate].freeze
  SECONDARY_REJECTION_CATEGORIES = %w[repossessed not_bought no_contract wrong_customer different_price different_product broken].freeze
  UNREACHABLE_CATEGORY = "unreachable"

  belongs_to :rbf_claim
  has_one :rbf_claim_template, through: :rbf_claim
  has_one :organization, through: :rbf_claim

  belongs_to :parent_rbf_device_claim, optional: true, class_name: "RbfDeviceClaim"
  has_one :child_rbf_device_claim, class_name: "RbfDeviceClaim",
                                   foreign_key: :parent_rbf_device_claim_id,
                                   inverse_of: :parent_rbf_device_claim,
                                   dependent: :nullify

  belongs_to :reviewer, optional: true, class_name: "User"

  validates :state, :subject_origin, :subject_uid, presence: true
  validates :state, inclusion: { in: STATES }
  validates :submission_category, inclusion: { in: SUBMISSION_CATEGORIES }
  validates :verification_category, inclusion: { in: VERIFICATION_CATEGORIES }, allow_blank: true
  validates :rejection_category, inclusion: { in: DEFAULT_REJECTION_CATEGORIES + SECONDARY_REJECTION_CATEGORIES }, if: :rejected?
  validates :parent_rbf_device_claim_id, uniqueness: true, allow_nil: true # rubocop:disable Rails/UniqueValidationWithoutIndex

  auto_strip_attributes :reviewer_comment, :submission_comment, squish: true

  before_validation on: :create do
    self.state ||= STATES.first
    self.submission_category ||= SUBMISSION_CATEGORIES.first
  end

  before_validation do
    handle_repossessed_amounts
    persist_verified_amounts
  end

  scope :of_organization, ->(org) { includes(:organization).where(organizations: { id: org.id }) }

  scope :without_unreachable, -> { where.not(rejection_category: UNREACHABLE_CATEGORY).or(where(rejection_category: nil)) }
  scope :approved_unreachable, -> { approved.where(rejection_category: UNREACHABLE_CATEGORY) }
  scope :approved_not_unreachable, -> { approved.without_unreachable }

  SUBMISSION_CATEGORIES.each do |c|
    scope c.to_sym, -> { where submission_category: c }

    define_method "#{c}?" do
      c == submission_category
    end
  end

  STATES.each do |s|
    scope s.to_sym, -> { where state: s }

    define_method "#{s}?" do
      s == state
    end
  end

  delegate :currency, to: :rbf_claim_template

  def default_verification?
    verification_category == rbf_claim_template.default_verification_category
  end

  def secondary_verification?
    !default_verification?
  end

  def unreachable?
    rejection_category == UNREACHABLE_CATEGORY
  end

  def secondary_verification_approved_unreachable?
    secondary_verification? && approved? && unreachable?
  end

  def possible_state_rejection_combinations
    if default_verification?
      STATES.without("rejected") +
        DEFAULT_REJECTION_CATEGORIES.map { "rejected_#{_1}" }
    else
      STATES.without("rejected") +
        ["approved_#{UNREACHABLE_CATEGORY}"] +
        SECONDARY_REJECTION_CATEGORIES.map { "rejected_#{_1}" }
    end
  end

  def state_rejection_combined
    [state, rejection_category].compact.join("_")
  end

  def state_rejection_combined=(str)
    s, rc = str.to_s.split("_", 2)
    self.state = s
    self.rejection_category = rc
  end

  def move_to_secondary_verification(verification_category)
    self.verification_category = verification_category
    self.state = "submitted"
    self.rejection_category = nil
  end

  def move_to_secondary_verification!(verification_category)
    move_to_secondary_verification verification_category
    save!
  end

  def device_s
    claimed_devices_data.
      slice("manufacturer", "model", "serial_number").values.join(" / ")
  end

  def properly_verified?
    state.in? FINAL_STATES
  end

  def repair?
    parent_rbf_device_claim&.rejected_broken?
  end

  def rejected_broken?
    state == "rejected" && rejection_category == "broken"
  end

  def add_claimed_data(data)
    self.claimed_devices_data = data[:device]
    self.claimed_trust_trace_data = data[:trust_trace]
    self.claimed_payments_data = data[:payments]
    self.subject_uid = data[:subject_uid]
    self.subject_origin = data[:subject_origin]
    self.trust_trace_uid = data[:uid]
    self.claimed_incentive = data[:trust_trace].dig("custom", "incentive__amount")
    self.claimed_subsidy = data[:trust_trace].dig("custom", "subsidy__amount")
    self.device_category = data[:trust_trace].dig("custom", "subsidy__category")
  end

  def finally_repossessed?
    repossessed? && approved? && rbf_claim.paid_out?
  end

  def repossessed_later?
    child_rbf_device_claim&.finally_repossessed?
  end

  def claimed_immediate_payout
    claimed_subsidy.to_i + claimed_immediate_incentive.to_i
  end

  def claimed_delayed_payout
    repossessed? || repossessed_later? ? 0 : claimed_delayed_incentive
  end

  def claimed_immediate_incentive
    claimed_incentive.to_i - claimed_delayed_incentive
  end

  def claimed_delayed_incentive
    (claimed_incentive.to_i * 0.5).to_i
  end

  stringify_with_currency :claimed_subsidy,
                          :verified_subsidy,
                          :claimed_incentive,
                          :verified_incentive,
                          :claimed_immediate_incentive,
                          :claimed_delayed_incentive,
                          :claimed_immediate_payout

  # i am special :)
  def claimed_delayed_payout_s
    if repossessed_later?
      "Repossessed later"
    else
      stringify_with_currency claimed_delayed_payout
    end
  end

  def handle_repossessed_amounts
    return unless repossessed?

    self.claimed_incentive = claimed_incentive * -1 if claimed_incentive.positive?
    self.claimed_subsidy = claimed_subsidy * -1 if claimed_subsidy.positive?
  end

  def persist_verified_amounts
    if approved?
      self.verified_incentive = claimed_incentive
      self.verified_subsidy = claimed_subsidy
    elsif rejected? || submitted?
      self.verified_incentive = 0
      self.verified_subsidy = 0
    end
  end
end
