# frozen_string_literal: true

module Service
  # Class that represents a legacy data format from A2EI Hop meters and receives data from legacy RDS
  class SmMetaData
    attr_accessor :meter_id, :name, :district, :type_of_network, :type_of_smartmeter, :power_reading, :appliances,
                  :facilities, :latitude, :longitude, :installed_at

    def initialize(*init)
      init.first.each { |k, v| public_send("#{k}=", v) } unless init.empty?
    end

    # get a list of all meters in Benin
    def self.meters
      # send the query to the database and get the result
      Service::RdsConnection.connection.execute("SELECT account_id as meter_id from sm_meta_benin")
    end

    def self.technical_data_for_meter(meter_id, resolution, start_time, end_time)
      sql = sanitize_tech_data(resolution, start_time, end_time, meter_id)
      resultset = []
      # send the query to the database and get the result
      Service::RdsConnection.connection.execute(sql).each do |entry|
        resultset << { meterId: meter_id,
                       timestamp: entry["start_period"],
                       timestampInMillis: entry["start_period"].to_i.in_milliseconds,
                       metered_power: entry["metered_power"],
                       energyReadingKwh: entry["energy_consumed_total_kwh"],
                       customerAccountId: entry["customer"],
                       timeIntervalMinutes: resolution_in_mins(resolution),
                       energyConsumptionKwh: entry["energy_consumed_in_interval_kwh"] }
      end
      resultset
    end

    def self.resolution_in_mins(resolution)
      # this should be fine, resolution value is checked by the API
      1.send(resolution.downcase).in_minutes.round
    end

    def self.technical_data_profile_for_meter(meter_id, range)
      sql = sanitize_techprofile_data(range, meter_id)
      resultset = []
      # send the query to the database and get the result
      Service::RdsConnection.connection.execute(sql).each do |entry|
        resultset << { meterId: meter_id,
                       hourOfTheDay: entry["hour"],
                       averagePower: entry["avg_power"],
                       stdDevPower: entry["std_power"],
                       maxPower: entry["max_power"],
                       minPower: entry["min_power"],
                       numberDatapoints: entry["datapoints"] }
      end
      resultset
    end

    def self.meta_data_for_meter(meter_id)
      sql = sanitize_meta_data meter_id
      sm_meta_data = nil

      Service::RdsConnection.connection.execute(sql).each do |row|
        sm_meta_data = Service::SmMetaData.new(**row.to_h)
      end

      sm_meta_data
    end

    # == Schema Information
    #
    # Table name: sm_logs_benin
    #
    #  time             :timestamp without time zone      not null
    #  account_id       :character varying                not null
    #  meteredVoltageA  :double precision
    #  measurementTime  :timestamp without time zone
    #  powerFactorA     :double precision
    #  lifetimeEnergy   :double precision
    #  meteredPowerA    :double precision
    #  signalStrength   :double precision
    #  frequency        :double precision
    #  currentA         :double precision
    #  meteredVoltageB  :double precision
    #  powerFactorB     :double precision
    #  meteredPowerB    :double precision
    #  currentB         :double precision
    #  meteredVoltageC  :double precision
    #  powerFactorC     :double precision
    #  meteredPowerC    :double precision
    #  currentC         :double precision
    #  sourceCreatedAt  :timestamp without time zone
    #  createdOn        :timestamp without time zone
    #  powerFactor      :double precision
    #  meteredPower     :double precision
    #  reactPower       :double precision
    #  created_at       :timestamp without time zone
    #
    # Indexes
    #
    #  ix_sm_logs_benin             (account_id)
    #  ix_sm_logs_benin_compound    (time,account_id)
    #
    def self.sanitize_tech_data(resolution, start_time, end_time, meter_id)
      sql = <<-SQL.squish
        SELECT
          meta.account_id as meter_id,
          date_trunc( :res, logs."measurementTime" ) AS start_period,
          ROUND( AVG( logs."meteredPower" ) ) AS metered_power,
          MAX( logs."lifetimeEnergy" ) AS energy_consumed_total_kwh,
          MAX( logs."lifetimeEnergy" ) - MIN( logs."lifetimeEnergy" ) AS energy_consumed_in_interval_kwh,
          meta.name_of_hc as customer
        FROM
          sm_logs_benin logs
        LEFT JOIN sm_meta_benin meta
          ON CAST( meta.account_id AS VARCHAR ) = logs.account_id
        WHERE
          "measurementTime" >= :start AND
          "measurementTime" < :end AND
          meta.account_id = :id
        GROUP BY
          meta.id,2
        ORDER BY
          1,2
      SQL

      Service::RdsConnection.sanitize_sql([sql, { res: resolution, start: start_time, end: end_time, id: meter_id }])
    end

    # == Schema Information
    #
    # Materialized view name: sm_logs_benin_load_profiles
    #
    #  account_id                           :character varying        not null
    #  name                                 :string
    #  hour_of_the_day_wat                  :smallint
    #  datapoints_day                       :bigint
    #  datapoints_week                      :bigint
    #  datapoints_month                     :bigint
    #  datapoints_month_weekdays            :bigint
    #  datapoints_full                      :bigint
    #  average_power_w_day                  :double precision
    #  average_power_w_week                 :double precision
    #  average_power_w_month                :double precision
    #  average_power_w_month_weekdays       :double precision
    #  average_power_w_full                 :double precision
    #  standard_deviation_w_day             :double precision
    #  standard_deviation_w_week            :double precision
    #  standard_deviation_w_month           :double precision
    #  standard_deviation_w_month_weekdays  :double precision
    #  standard_deviation_w_full            :double precision
    #  max_power_w_day                      :double precision
    #  max_power_w_week                     :double precision
    #  max_power_w_month                    :double precision
    #  max_power_w_month_weekdays           :double precision
    #  max_power_w_full                     :double precision
    #  min_power_w_day                      :double precision
    #  min_power_w_week                     :double precision
    #  min_power_w_month                    :double precision
    #  min_power_w_month_weekdays           :double precision
    #  min_power_w_year                     :double precision
    #
    # Indexes
    #
    #  ix_sm_logs_benin_load_profiles_uid     (account_id)
    #
    def self.sanitize_techprofile_data(range, meter_id)
      # range not subject to SQL injection since it is guarded by API
      sql = <<-SQL.squish
        SELECT
          account_id as "meter_id",
          hour_of_the_day_wat as "hour",
          average_power_w_#{range} as "avg_power",
          standard_deviation_w_#{range} as "std_power",
          max_power_w_#{range} as "max_power",
          min_power_w_#{range} as "min_power",
          datapoints_#{range} as "datapoints"
        FROM
          sm_logs_benin_load_profiles
        WHERE
          CAST(account_id as INT) = :id
        ORDER BY
          1,2
      SQL

      Service::RdsConnection.sanitize_sql([sql, { id: meter_id }])
    end

    # == Schema Information
    #
    # Table name: sm_meta_benin
    #
    #  id                  :bigint             not null, primary key
    #  account_id          :bigint
    #  department          :text
    #  commune             :text
    #  district            :text
    #  name_of_hc          :text
    #  type_of_hc          :text
    #  latitude            :double precision
    #  longitude           :double precision
    #  type_of_network     :text
    #  type_of_smartmeter  :text
    #  meternumber         :bigint
    #  power_reading       :text
    #  sim_card_number     :text
    #  other_sources       :text
    #  dc_meter_id         :bigint
    #  appliances          :text
    #  facilities          :text
    #
    #
    # Table name: sm_meta_benin_appliances
    #
    #  index               :bigint             not null, primary key
    #  name_of_hc          :text
    #  hc_id               :bigint
    #  appliance           :text
    #  type                :text
    #  power_w             :text
    #  quantity            :double precision
    #  comment             :text
    #
    def self.sanitize_meta_data(meter_id)
      sql = <<-SQL.squish
        SELECT
          meta.account_id as meter_id,
          meta.name_of_hc as name,
          meta.district,
          meta.latitude,
          meta.longitude,
          meta.type_of_network,
          meta.type_of_smartmeter,
          meta.power_reading,
          meta.facilities,
          array_agg(row_to_json(appl)) as appliances
        from
          sm_meta_benin meta
        LEFT JOIN sm_meta_benin_appliances appl
          ON appl.hc_id = meta.id
        WHERE account_id = :id
        GROUP BY meta.id
      SQL

      Service::RdsConnection.sanitize_sql([sql, { id: meter_id }])
    end
  end
end
