# frozen_string_literal: true

module Service
  class RdsConnection < ApplicationRecord
    self.abstract_class = true
    establish_connection :aws_rds
  end
end
