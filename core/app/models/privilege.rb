# frozen_string_literal: true

# == Schema Information
#
# Table name: privileges
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  organization_user_id :bigint           not null
#  user_id              :bigint           not null
#
# Indexes
#
#  index_privileges_on_name                           (name)
#  index_privileges_on_organization_user_id           (organization_user_id)
#  index_privileges_on_organization_user_id_and_name  (organization_user_id,name) UNIQUE
#  index_privileges_on_user_id                        (user_id)
#
class Privilege < ApplicationRecord
  include Creator
  include OwningOrganization

  belongs_to :organization_user
  validates :name, presence: true, uniqueness: { scope: :organization_user_id, case_sensitive: false }

  #
  # Privilege names are following the pattern: "subject.verb".
  # Names serve as a key in I18n files to provide an explanation for each privilege.
  #
  # Let's start with not too fine grained permissions: user.delete, user.create is summarized as user.manage
  #
  PRIVILEGES = %w[
    analytics.view
    data.download
    source.manage
    source.update_data
    source.upload_data
    source.delete
    user.manage
    organization.manage
    organization.view_api_token
    project.manage
    project.delete
    visibility.manage
    grafana.edit
    rbf_claim.create
    rbf_claim.do_data_collection
    rbf_claim.print_submitted
    rbf_claim.do_verification
    rbf_claim.do_payment
    rbf_claim.submit_to_program
    rbf_claim.revoke_submission
    rbf_claim.start_verification
    rbf_claim.approve_verification
    rbf_claim.reject_completely
    rbf_claim.mark_paid_out
    rbf_claim.show_claims_full
    rbf_claim.show_claims_status
    rbf_claim.show_claims_minimal
    rbf_claim.assign_verifiers
    rbf_claim.verification_download_data
    rbf_claim.see_verification_result
  ].freeze

  #
  # List of privileges a normal user gets by default
  # subset of PRIVILEGES
  #
  USER_PRIVILEGES = %w[
    analytics.view
    data.download
    source.manage
    source.upload_data
    organization.view_api_token
    project.manage
    visibility.manage
    grafana.edit
  ].freeze

  #
  # List of privileges a 'viewer' gets by default
  # subset of USER_PRIVILEGES
  #
  VIEWER_PRIVILEGES = %w[
    analytics.view
    data.download
  ].freeze

  # manager organization scoped: manage users, create data sources, manage visibilities, ...
  # user    consume data etc..
  # viewer  can't change data or system, just see data
  #
  # we assume that each template is included in the next template. Starting with the least powerfull template.
  TEMPLATES = {
    "viewer" => VIEWER_PRIVILEGES,
    "user" => USER_PRIVILEGES,
    "manager" => PRIVILEGES
  }.freeze

  TEMPLATE_NAMES = TEMPLATES.keys.freeze

  def owning_organization
    organization_user.organization
  end

  delegate :to_s, to: :name

  def self.grouped_privileges
    grouped = {}
    PRIVILEGES.sort.each do |name|
      group, short = name.split(".", 2)
      grouped[group] ||= []
      grouped[group] << { name:, short: }
    end
    grouped
  end

  #
  # finds the best fitting template key for given list of privileges.
  # Adds a flag more_privileges to indicate if the match is exact (false) or if there are more privileges in the list (true)
  #
  # returns {template: key, more_privileges: bool}
  def self.match_templates(privileges)
    TEMPLATES.reverse_each do |key, template_privs|
      #
      # A: template privs
      # B: parameter privs
      # We look for the bigestest template that is included in B, so A - B = 0
      #
      diff_ab = template_privs.difference(privileges)

      next unless diff_ab.empty?

      # to detect an exact match: B - A = 0
      diff_ba = privileges.difference(template_privs)

      return { template: key, more_privileges: diff_ba.any? }
    end

    # no template matches
    { template: "no priv. template", more_privileges: privileges.any? }
  end

  #
  # Removes all privileges from db that are not defined in the code.
  #
  def self.cleanup_privileges
    count_before = Privilege.count

    Privilege.where.not(name: PRIVILEGES).find_each(&:destroy!)

    Rails.logger.info "Cleanup privileges finished. Count before: #{count_before}, after: #{Privilege.count}."
  end
end
