# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations_users
#
#  id              :bigint           not null, primary key
#  activated_at    :datetime
#  organization_id :bigint           not null
#  user_id         :bigint           not null
#
# Indexes
#
#  index_organizations_users_on_organization_id              (organization_id)
#  index_organizations_users_on_user_id                      (user_id)
#  index_organizations_users_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
class OrganizationUser < ApplicationRecord
  include OwningOrganization

  self.table_name = "organizations_users"

  belongs_to :user, inverse_of: :organization_users
  belongs_to :organization, inverse_of: :organization_users

  has_many :privileges, dependent: :destroy

  scope :invited_users, -> { where(activated_at: nil) }
  scope :active_users, -> { where.not(activated_at: nil) }

  def activate!
    update! activated_at: Time.zone.now
    Grafana::RemoteControl.add_user_to_org(user, organization)
  end

  def privilege?(name)
    privileges.exists?(name:)
  end

  def add_privilege(name)
    return if user.admin?

    privileges.create(name:) unless privilege? name

    update_grafana_rights if name == "grafana.edit"
  end

  # adds an array of privileges
  def add_privileges(privileges)
    privileges.each { |p| add_privilege p }
  end

  def apply_privilege_template(template)
    privs = Privilege::TEMPLATES[template.to_s] # allow symbol too
    raise "No privileges for template #{template}" if privs.blank?

    add_privileges privs
  end

  def update_grafana_rights
    Grafana::RemoteControl.update_user_permission self
  end

  def remove_privilege(name)
    priv = privileges.find_by(name:)
    priv&.delete

    update_grafana_rights if name == "grafana.edit"
  end

  def privilege_names
    privileges.map(&:name)
  end

  #
  # creates privileges in the list, and removes privileges not in the list
  #
  def privilege_names=(list)
    Privilege::PRIVILEGES.each do |name|
      if Array(list).include? name
        add_privilege name
      elsif privilege? name
        remove_privilege name
      end
    end
  end

  def owning_organization
    organization
  end

  def to_s_org_role
    organization.to_s
  end

  #
  # Shows users privilege template and admin info in one (short) string
  #
  # e.g. admin, viewer, viewer+ (where is the +sign indicates extra privileges)
  def privilege_summary
    if user.admin?
      "admin"
    else
      r = Privilege.match_templates(privileges.map(&:name))
      "#{r[:template]}#{r[:more_privileges] ? '+' : ''}"
    end
  end
end
