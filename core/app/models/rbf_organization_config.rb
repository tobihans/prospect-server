# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_organization_configs
#
#  id                     :bigint           not null, primary key
#  code                   :string           not null
#  total_grant_allocation :float            default(0.0), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  organization_id        :integer          not null
#  rbf_claim_template_id  :integer          not null
#  user_id                :integer
#
# Indexes
#
#  idx_on_code_rbf_claim_template_id_967e2caa74             (code,rbf_claim_template_id) UNIQUE
#  idx_on_organization_id_rbf_claim_template_id_2d999cb87a  (organization_id,rbf_claim_template_id) UNIQUE
#
class RbfOrganizationConfig < ApplicationRecord
  include Creator
  include OwningOrganization
  include Currencify

  belongs_to :organization
  belongs_to :rbf_claim_template
  has_one :organization_group, through: :rbf_claim_template

  validates :code, presence: true
  validates :total_grant_allocation, presence: true
  validates :code, uniqueness: { scope: :rbf_claim_template_id, case_sensitive: false }
  validates :rbf_claim_template_id, uniqueness: { scope: :organization_id }

  delegate :currency, to: :rbf_claim_template, allow_nil: true

  after_save :move_organization_into_group

  stringify_with_currency :total_grant_allocation

  def owning_organization
    organization
  end

  def move_organization_into_group
    OrganizationGroupMember.find_or_create_by!(organization:, organization_group:)
  end
end
