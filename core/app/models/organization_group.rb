# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_groups
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_organization_groups_on_name  (name) UNIQUE
#
class OrganizationGroup < ApplicationRecord
  include Creator

  has_many :organization_group_members, dependent: :destroy
  has_many :organizations, through: :organization_group_members
  has_many :rbf_claim_templates, dependent: :destroy

  auto_strip_attributes :name

  validates :name, uniqueness: { case_sensitive: false }

  accepts_nested_attributes_for :organization_group_members, allow_destroy: true

  scope :default_order, -> { order name: :asc }
end
