# frozen_string_literal: true

class DynamicForm
  class Validator
    attr_accessor :fields

    def initialize(form_data)
      @fields = {}
      @errors = {}
      form_data.each_value do |groups|
        groups["fields"].each do |k, v|
          @fields[k] = []
          # check select field that have options specified
          @fields[k] << DynamicForm::Validator::Rules::Select.new(field: k, values: v["options"]) if v["type"] == "select" && v["options"].any?

          next unless v["validation_rules"]
          next unless v["data_column"]

          @fields[k] << Rules.eval(v["validation_rules"])
          @fields[k].flatten!
        end
      end
    end

    ValidationResult = Data.define(:valid, :errors, :row) do
      def to_errors_s
        errors.map { |key, value| "\t#{key.titleize}: #{value}" }.join("\n")
      end
    end

    def validate_multiple_to_errors(submissions)
      validate_multiple(submissions, only_invalid: true).map do |result|
        "Row ##{result.row}:\n#{result.to_errors_s}\n"
      end
    end

    def validate_multiple(submissions, only_invalid: false)
      results = submissions.map.with_index { |s, i| validate s, row: i + 1 }
      only_invalid ? results.reject(&:valid) : results
    end

    def validate(submission, row: nil)
      errors = {}
      submission.each do |key, value|
        rules = @fields[key]
        next unless rules

        rules.each do |rule|
          errors[key] = rule.error(key, value) unless rule.check(value, submission)
        end
      end

      ValidationResult.new(valid: errors.empty?, errors:, row:)
    end

    class Rules
      def self.eval(ruleset)
        ruleset.map do |rules|
          rules.split(",").map do |rule|
            create_rule(rule)
          end
        end.flatten
      end

      Select = Data.define(:field, :values) do
        def check(_input, form)
          form[field].in?(values.pluck("value").map(&:to_s))
        end

        def error(key, value)
          "#{value} for #{key} not in list of allowed values: #{values.pluck('value').join(', ')}"
        end
      end

      Currency = Data.define do
        # TODO: impl
        def check(_input, _form)
          true
        end

        def error(_key, value)
          "#{value} is not a valid currency"
        end
      end

      MaxLen = Data.define(:amount) do
        def check(input, _form)
          input.nil? ? true : input.length <= amount
        end

        def error(key, _value)
          "the maximum length for #{key} is #{amount}"
        end
      end

      MinLen = Data.define(:amount) do
        def check(input, _form)
          input.nil? ? true : input.length >= amount
        end

        def error(key, _value)
          "the minimum length for #{key} is #{amount}"
        end
      end

      MaxDate = Data.define(:date) do
        def check(input, _form)
          Date.parse(input) <= date
        rescue
          false
        end

        def error(key, value)
          Date.parse(value)
          "the date #{value} must not be futher in the future than #{date}"
        rescue
          "date value in #{key} is invalid"
        end
      end

      MinDate = Data.define(:date) do
        def check(input, _form)
          Date.parse(input) >= date
        rescue
          false
        end

        def error(key, value)
          Date.parse(value)
          "the date #{value} should not be earlier than #{date}"
        rescue
          "date value in #{key} is invalid"
        end
      end

      RequiredIf = Data.define(:field, :value) do
        def check(input, form)
          values = value.split("|")
          if form[field].in? values
            input.present?
          else
            true
          end
        end

        def error(key, _value)
          "#{key} is required if #{field} is #{value}"
        end
      end

      Required = Data.define do
        def check(input, _form)
          input.present?
        end

        def error(key, _value)
          "#{key} is required"
        end
      end

      Regex = Data.define(:format) do
        def check(input, _form)
          input.nil? ? true : !!input.match(format)
        end

        def error(_key, value)
          "#{value} does not match the required format: #{format}"
        end
      end

      Format = Data.define(:format) do
        def check(input, _form)
          return true unless input

          case format
          when "uppercase"
            input.upcase == input
          else
            true
          end
        end

        def error(_key, value)
          "#{value} is not in the required format: #{format}"
        end
      end

      LesserThan = Data.define(:format) do
        def check(input, form)
          return true unless input

          (input.to_i <= form[format].to_i)
        end

        def error(_form, format)
          "Should not be bigger than #{format} field"
        end
      end

      def self.create_rule(rule)
        rule, params = rule.split(":")
        case rule
        when "required"
          Required.new
        when "required_if"
          RequiredIf.new(*params.split("="))
        when "max"
          MaxLen.new amount: params.to_i
        when "min"
          MinLen.new amount: params.to_i
        when "currency"
          Currency.new
        when "max_date"
          # TODO: enforce a specification via active admin validations
          date = if params.match(/current_date|today/)
                   Time.zone.today
                 else
                   Date.parse params
                 end
          MaxDate.new date:
        when "min_date"
          MinDate.new date: Date.parse(params)
        when "format"
          Format.new format: params
        when "regex"
          Regex.new format: params
        when "lesser_than"
          LesserThan.new format: params
        else
          "Unkown Validator rule: #{rule}"
        end
      end
    end
  end
end
