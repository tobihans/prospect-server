# frozen_string_literal: true

# == Schema Information
#
# Table name: imports
#
#  id                     :bigint           not null, primary key
#  error                  :string
#  ingestion_finished_at  :datetime
#  ingestion_started_at   :datetime
#  last_imported_hint     :string
#  processing_finished_at :datetime
#  processing_started_at  :datetime
#  protocol               :text
#  rows_duplicated        :integer
#  rows_failed            :integer
#  rows_inserted          :integer
#  rows_updated           :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  source_id              :bigint
#
# Indexes
#
#  index_imports_on_source_id  (source_id)
#

class Import < ApplicationRecord
  include OwningOrganization

  CONTENT_TYPES = { json: "application/json", csv: "text/csv", txt: "text/plain" }.freeze

  belongs_to :source
  has_one :project, through: :source
  has_one :organization, through: :project
  has_many :encrypted_blobs, dependent: :destroy

  has_one_attached :file
  has_one_attached :recording

  delegate :organization_id, to: :project, allow_nil: false

  scope :failed, -> { where.not error: nil }
  scope :no_error, -> { where error: nil }
  scope :pending, -> { no_error.where(ingestion_started_at: nil) }
  scope :ingestion_started, -> { no_error.where.not(ingestion_started_at: nil).where(ingestion_finished_at: nil) }
  scope :ingestion_finished, -> { no_error.where.not(ingestion_finished_at: nil).where(processing_started_at: nil) }
  scope :processing_started, -> { no_error.where.not(processing_started_at: nil).where(processing_finished_at: nil) }
  scope :processing_finished, -> { no_error.where.not(processing_finished_at: nil) }
  scope :unfinished, -> { no_error.where(processing_finished_at: nil) }

  before_destroy :delete_data!
  after_commit :notify_processing

  STATES = %w[
    pending
    ingestion_started
    ingestion_finished
    processing_started
    processing_finished
  ].freeze

  STATES.each do |state_name|
    # failed scopes like: ingestion_started_failed
    scope :"#{state_name}_failed", -> { public_send(state_name).unscope(where: :error).failed }

    # state checker no error like: ingestion_started?
    define_method "#{state_name}?" do
      state == state_name
    end

    # state checker failed like: ingestion_started_failed?
    define_method "#{state_name}_failed?" do
      state == "#{state_name}_failed"
    end

    # set and persist state timestamps as now: ingestion_started!
    unless state_name == "pending" # rubocop:disable Style/Next
      define_method "#{state_name}!" do
        update! "#{state_name}_at" => Time.zone.now
      end
    end
  end

  def failed!(error_msg)
    update! error: error_msg
  end

  def no_error?
    error.nil?
  end

  def failed?
    error.present? && error != "no_data"
  end

  def no_data?
    error.present? && error == "no_data"
  end

  def step_state
    if !ingestion_started_at
      "pending"
    elsif !ingestion_finished_at
      "ingestion_started"
    elsif !processing_started_at
      "ingestion_finished"
    elsif !processing_finished_at
      "processing_started"
    else
      "processing_finished"
    end
  end

  def state
    return "no_data" if no_data?

    error_state = failed? ? "_failed" : ""
    step_state + error_state
  end

  def state_int
    STATES.index(step_state)
  end

  def state_percent
    return 0 if no_data?

    (state_int.to_f / (STATES.count - 1)) * 100
  end

  def state_int_of
    "#{state_int}/#{STATES.count - 1}"
  end

  def state_symbol
    if failed?
      "⨯"
    elsif processing_finished?
      "✔"
    else
      "…"
    end
  end

  def time_s
    "#{created_at} ... #{processing_finished_at}".strip
  end

  def notify_processing(force: false)
    unless should_process? || force
      Rails.logger.debug { "Skipped processing notification for Import #{id}" }
      return
    end

    FaktoryPusher.push source.data_schema, faktory_worker_args

    notify_rbf_post_processing
  end

  def notify_rbf_post_processing
    return unless organization.rbf_submitting?

    ImportPostProcessingWorker.enqueue_for_rbf_import self
  end

  def requires_recording?
    source.adapter.try(:requires_recording?) && !recording.attached?
  end

  def should_process?
    !failed? && ingestion_finished? && file.attached? && !requires_recording?
  end

  def faktory_worker_args
    [
      id, # the id of the import job, used to track progress / error
      source_id, # source id to note on data, so we can track origin / calculate visibility
      file.key, # basically the filename in minio
      file.content_type, # file format, in mime type notation - helps to parse properly
      source.data_origin, # to help build uids, eg. [organization_id]_[data_origin]_[some_external_id]
      source.project.organization_id # to help build uids, eg. [organization_id]_[data_origin]_[some_external_id]
    ]
  end

  def push_manual_update!
    return if failed? || ingestion_finished? || !file.attached? || !persisted?

    FaktoryPusher.push "generic", faktory_worker_args

    # skip the normal pushing callback
    update_column :ingestion_finished_at, Time.zone.now # rubocop:disable Rails/SkipsModelValidations
  end

  def reprocess!
    update! processing_finished_at: nil, processing_started_at: nil, error: nil
  end

  def delete_data!
    dts = Postgres::DataTable.all.select do |dt|
      (%w[import_id] - dt.columns.map(&:name)).empty?
    end
    queries = dts.map do |dt|
      "DELETE FROM #{dt.table_name} WHERE import_id = #{id}"
    end
    queries.each { |q| Postgres::RemoteControl.query! q }
  end

  #
  # store data with nice filename and custom blob key

  # currently supported:
  #   store_data! :json, {some: "Hash"} => stored as json
  #   store_data! :json, '{"json": "string"}' => stored as json
  #   store_data! :csv, "csv,string" => stored as csv
  #   store_data! :txt, "some random text we parse later" => stored as plain text
  #
  def store_data!(format, data)
    raise "Import must be persisted to generate proper filename and blob key" unless persisted?

    extension = format

    data = data.to_json if data.is_a? Hash
    data = data.to_json if data.is_a? Array

    raise "Data Must be a string now, but is #{data.class}" unless data.is_a?(String)

    content_type = CONTENT_TYPES[extension]

    raise "Could not determine content type for #{extension}, import not saved to data lake" unless content_type

    fn = "#{id}-#{source_id}-#{source.kind}"
    # https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-keys.html#object-key-guidelines
    fn = fn.tr "^a-zA-Z0-9-_", "_"
    fn += ".#{extension}"

    # put each file into folders determined by a time format for better file management, default is by year-month-week/...
    key = "#{Time.zone.today.strftime(ENV.fetch('MINIO_PATH_STRFTIME_PREFIX', '%Y-%m-%W'))}/#{fn}"

    io = StringIO.new data

    file.attach io:, content_type:, filename: fn, key:, identify: false # identify: false disables automatic content-type detection of ActiveStorage
  end

  def self.run!(source, &block)
    i = Import.create!(source:)
    i.ingestion_started!
    block.call i
    i.ingestion_finished! unless i.failed?
    i
  end

  def owning_organization
    organization
  end
end
