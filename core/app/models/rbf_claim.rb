# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_claims
#
#  id                         :bigint           not null, primary key
#  aasm_state                 :string
#  approved_at                :datetime
#  claimed_data               :jsonb            not null
#  claimed_subunit            :integer
#  code                       :string
#  paid_out_at                :datetime
#  rejected_at                :datetime
#  submitted_to_program_at    :datetime
#  verification_at            :datetime
#  verified_data              :jsonb            not null
#  verified_subunit           :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  approved_by_id             :bigint
#  assigned_to_verifier_id    :bigint
#  organization_id            :bigint           not null
#  paid_out_by_id             :bigint
#  rbf_claim_template_id      :bigint           not null
#  rejected_by_id             :bigint
#  submitted_to_program_by_id :bigint
#  user_id                    :bigint           not null
#  verification_by_id         :bigint
#
# Indexes
#
#  index_rbf_claims_on_aasm_state             (aasm_state)
#  index_rbf_claims_on_code                   (code) UNIQUE
#  index_rbf_claims_on_organization_id        (organization_id)
#  index_rbf_claims_on_rbf_claim_template_id  (rbf_claim_template_id)
#  index_rbf_claims_on_user_id                (user_id)
#
class RbfClaim < ApplicationRecord # rubocop:disable Metrics/ClassLength
  # Thursday 2DO
  #
  # - mark systems claimed  custom: rbf_claim_id
  # - submit iva result
  # UPDATE data_meters
  # SET custom = custom || '{"bla": 1, "nom": "nomnom"}'::jsonb

  include AASM
  include Creator
  include Currencify

  belongs_to :organization
  belongs_to :rbf_claim_template

  with_options class_name: "User", optional: true do
    belongs_to :submitted_to_program_by
    belongs_to :verification_by
    belongs_to :approved_by
    belongs_to :paid_out_by
    belongs_to :rejected_by
    belongs_to :assigned_to_verifier
  end

  has_one :organization_group, through: :rbf_claim_template

  has_many :rbf_device_claims, dependent: :destroy

  has_many :rbf_claim_verifiers, dependent: :destroy
  has_many :verification_users, through: :rbf_claim_verifiers, source: :verifier, class_name: "User"

  delegate :supervising_organization, :managing_organization, :verifying_organization,
           :currency, :trust_trace_check,
           to: :rbf_claim_template

  monetize :claimed_subunit, as: "claimed_amount", with_model_currency: :currency, allow_nil: true
  monetize :verified_subunit, as: "verified_amount", with_model_currency: :currency, allow_nil: true

  validates :rbf_claim_template,
            uniqueness: { scope: %i[organization_id aasm_state],
                          message: :duplicate_data_collection },
            if: :data_collection?, on: :create

  validates :code, uniqueness: { case_sensitive: false, allow_nil: true }

  scope :default_order, -> { order id: :desc }

  scope :with_organization, lambda { |org|
    claim_templates = RbfClaimTemplate.controlled_by_organization org
    where(organization: org).or(where(rbf_claim_template: claim_templates))
  }

  scope :without_data_collection_by_others, lambda { |org|
    where.not("rbf_claims.aasm_state = ? AND rbf_claims.organization_id != ?", "data_collection", org.id)
  }

  scope :for_index, ->(org) { with_organization(org).without_data_collection_by_others(org) }

  before_validation :assign_code, on: :create

  EVENT_ORGS =
    {
      organization: %i[submit_to_program],
      managing_organization: %i[start_verification revoke_submission mark_paid_out reject_completely],
      verifying_organization: %i[approve_verification]
    }.freeze

  ORG_ROLES = EVENT_ORGS.keys.freeze

  NEGATIVE_EVENTS = %i[revoke_submission reject_completely].freeze

  aasm timestamps: true do
    state :data_collection, initial: true
    state :submitted_to_program, before_enter: proc { track_state_entry :submitted_to_program }
    state :verification, before_enter: proc { track_state_entry :verification }, after_enter: :assign_initial_verification_categories
    state :approved, before_enter: proc { track_state_entry :approved }
    state :paid_out, before_enter: proc { track_state_entry :paid_out }
    state :rejected, before_enter: proc { track_state_entry :rejected }

    event :submit_to_program, guards: [:ready_for_submit_to_program?] do
      transitions from: :data_collection, to: :submitted_to_program
    end

    event :revoke_submission, guards: [:ready_for_revoke_submission?] do
      transitions from: :submitted_to_program, to: :data_collection
    end

    event :start_verification, guards: [:ready_for_start_verification?] do
      transitions from: :submitted_to_program, to: :verification
    end

    event :approve_verification, guards: [:ready_for_approve_verification?] do
      transitions from: :verification, to: :approved
    end

    # event :reject_completely, guards: [:ready_for_reject_completely?] do
    #   transitions from: :verification, to: :rejected
    # end

    event :mark_paid_out, guards: [:ready_for_mark_paid_out?] do
      transitions from: :approved, to: :paid_out
    end
  end

  def verification_users_ids=(user_ids)
    self.rbf_claim_verifiers = Array(user_ids).map { RbfClaimVerifier.new verifier_id: _1, rbf_claim: self }
  end

  def verification_users_ids
    verification_users.pluck :id
  end

  def submitted_at
    submitted_to_program_at || created_at
  end

  def allow_submission_by_schedule?
    return true unless Rails.application.config.full_prod_mode

    last_claim = latest_sibling_claim_before

    return true unless last_claim
    return true unless last_claim.submitted_to_program_at

    last_claim.submitted_to_program_at < Time.zone.now.beginning_of_month
  end

  def ready_for_submit_to_program?
    rbf_device_claims.present? &&
      allow_submission_by_schedule? &&
      allow_submission_by_count?
  end

  def allow_submission_by_count?(override_count: nil)
    return true unless Rails.application.config.full_prod_mode

    return true unless rbf_claim_template.claim_submission_minimum

    claims_count = override_count || rbf_device_claims.count

    return true if claims_count >= rbf_claim_template.claim_submission_minimum

    # allow 90 days after last claim submission / beginning of program
    previous_claim_date =
      latest_sibling_claim_before&.submitted_to_program_at ||
      Time.zone.parse("2024-11-01")

    previous_claim_date <= 90.days.ago
  end

  def ready_for_revoke_submission?
    true
  end

  def ready_for_start_verification?
    true # TODO: user assigned: verification_user.present?
  end

  def ready_for_approve_verification?
    broad_verification_stage == :finished
  end

  def ready_for_reject_completely?
    true
  end

  def ready_for_mark_paid_out?
    verified_data.present?
  end

  def track_state_entry(future_state)
    send "#{future_state}_by=", Current.user
    true
  end

  def to_s
    "RBF Claim ##{id}: #{aasm_state.humanize} (#{rbf_claim_template})"
  end

  def assign_code
    self.code = generate_code
  end

  def generate_code
    return if rbf_organization_config&.code.blank?

    [rbf_organization_config&.code, number_code].join("/")
  end

  def rbf_organization_config
    RbfOrganizationConfig.find_by(rbf_claim_template:, organization:)
  end

  def sibling_claims
    RbfClaim.where(organization_id:, rbf_claim_template:)
  end

  def sibling_claims_before
    q = sibling_claims
    q = q.where("id < ?", id) if persisted?
    q
  end

  def latest_sibling_claim_before
    sibling_claims_before.last
  end

  def number_claims_before
    sibling_claims_before.count
  end

  def number
    number_claims_before + 1
  end

  def number_code(padded: 3)
    number.to_s.rjust(padded, "0")
  end

  def current_organization_role
    organization_role Current.organization
  end

  def organization_role(organization)
    ORG_ROLES.find do |org_role|
      public_send(org_role) == organization
    end
  end

  def events_for_organization(organization)
    EVENT_ORGS[organization_role(organization)]
  end

  def potential_event_names
    aasm.events(possible: true).map(&:name)
  end

  def potential_events
    potential_event_names.index_with do |en|
      {
        may_fire: aasm.may_fire_event?(en),
        positive: NEGATIVE_EVENTS.exclude?(en),
        precondition: "Textual representation of precondition from translations"
      }
    end
  end

  def potential_positive_event_names
    potential_event_names.without NEGATIVE_EVENTS
  end

  def potential_positive_event_name
    potential_positive_event_names.first
  end

  def possible_events
    potential_events.select { |_k, v| v[:may_fire] }
  end

  def possible_positive_events
    possible_events.select { |_k, v| v[:positive] }
  end

  def possible_positive_event_names
    possible_positive_events.keys
  end

  def possible_positive_event_name
    possible_positive_event_names.first
  end

  def possible_negative_events
    possible_events.reject { |_k, v| v[:positive] }
  end

  def possible_negative_event_names
    possible_negative_events.keys
  end

  def possible_negative_event_name
    possible_negative_event_names.first
  end

  def org_roles_for_event(event)
    EVENT_ORGS.select { |_, v| v.include? event }.keys
  end

  def next_possible_event_org_role
    org_roles_for_event(potential_positive_event_name).first
  end

  def next_possible_event_org
    org_r = next_possible_event_org_role
    org_r && public_send(org_r)
  end

  def organization_can_now?(event, organization)
    return false unless possible_events[event]

    able_org_roles = org_roles_for_event(event)

    able_org_roles.any? { public_send(_1) == organization }
  end

  def waiting_for_s
    if verification? && broad_verification_stage != :finished
      if broad_verification_stage == :default
        "#{rbf_claim_template.default_verification_category.titleize} Verification #{claim_verification_progress}"
      else
        "Secondary Verification"
      end
    elsif next_possible_event_org
      "#{aasm_state.to_s.titleize} - waiting for #{potential_positive_event_name.to_s.titleize} from #{next_possible_event_org}"
    else
      aasm_state.to_s.titleize
    end
  end

  def do_payment(params)
    update! verified_data: params
  end

  def do_device_verification(uid:, state_rejection_combined:, reviewer_comment:)
    rdc = rbf_device_claims.find_by! trust_trace_uid: uid
    rdc.state_rejection_combined = state_rejection_combined
    rdc.reviewer_comment = reviewer_comment
    rdc.reviewer = Current.user
    rdc.save!

    return unless rdc.secondary_verification_approved_unreachable?

    take_into_secondary_verification_category! rdc.verification_category
  end

  def do_verification(params)
    params.each do |id, details|
      rdc = RbfDeviceClaim.find id
      rdc.state_rejection_combined = details[:action]
      rdc.reviewer_comment = details[:reviewer_comment].presence
      rdc.reviewer = Current.user
      rdc.verification_category = "desk"
      rdc.save!
    end
  end

  def do_data_collection_add_all
    uids = query_batch.reject { _1[:previous_device_claim] }.pluck(:uid)
    minimal_params = uids.to_h { [_1, { add: true }] }

    do_data_collection minimal_params, mode: :add
  end

  def do_data_collection(params, mode:)
    mode = mode.to_sym
    case mode
    when :add
      device_claim_ids = params.map do |uid, details|
        next if details[:add].blank?

        d = device_data(uid, mode:)
        device_claim = d[:existing_device_claim] || rbf_device_claims.build
        device_claim.submission_comment = details[:submission_comment]
        device_claim.add_claimed_data d
        device_claim.save!
        device_claim.id
      end.compact
      rbf_device_claims.initial_claim.where.not(id: device_claim_ids).find_each(&:destroy)
    when :resubmission
      device_claim_ids = params.map do |uid, details|
        next if details[:resubmission].blank?

        d = device_data(uid, mode:)
        device_claim = d[:existing_device_claim] || rbf_device_claims.build
        device_claim.submission_comment = details[:submission_comment]
        device_claim.parent_rbf_device_claim_id = details[:parent_device_claim_id]
        device_claim.submission_category = "resubmission"
        device_claim.add_claimed_data d
        device_claim.save!
        device_claim.id
      end.compact
      rbf_device_claims.resubmission.where.not(id: device_claim_ids).find_each(&:destroy)
    when :repossessed
      device_claim_ids = params.map do |uid, details|
        next if details[:repossessed].blank?

        d = device_data(uid, mode:)
        device_claim = d[:existing_device_claim] || rbf_device_claims.build
        device_claim.submission_comment = details[:submission_comment]
        device_claim.submission_category = "repossessed"
        device_claim.parent_rbf_device_claim_id = details[:parent_device_claim_id]
        device_claim.add_claimed_data d
        device_claim.save!
        device_claim.id
      end.compact
      rbf_device_claims.repossessed.where.not(id: device_claim_ids).find_each(&:destroy)
    else
      raise "unhandled mode #{mode}"
    end
  end

  def stats_from_query(initial: false)
    data = query_batch
    data.reject! { _1[:previous_device_claim] } if initial

    subsidy_total = data.map { _1.dig :trust_trace, :custom, :subsidy__amount }.map(&:to_i).sum
    incentive_total = data.map { _1.dig :trust_trace, :custom, :incentive__amount }.map(&:to_i).sum
    devices = data.
              map { _1[:device].slice(:manufacturer, :model, :serial_number) }.
              map { _1.values.join(" / ") }

    {
      devices_amount: data.count,
      total_subsidy: "#{currency} #{subsidy_total}",
      total_incentive: "#{currency} #{incentive_total}",
      potentially_claimable_devices: devices
    }.transform_keys(&:to_s)
  end

  def claimed_subsidy
    rbf_device_claims.map(&:claimed_subsidy).sum
  end

  def claimed_incentive
    rbf_device_claims.map(&:claimed_incentive).sum
  end

  def approved_subsidy
    rbf_device_claims.approved.map(&:claimed_subsidy).sum
  end

  def approved_incentive
    rbf_device_claims.approved.map(&:claimed_incentive).sum
  end

  def claimed_immediate_payout
    rbf_device_claims.map(&:claimed_immediate_payout).sum
  end

  def approved_immediate_payout
    rbf_device_claims.approved.map(&:claimed_immediate_payout).sum
  end

  def approved_delayed_payout
    rbf_device_claims.approved.map(&:claimed_delayed_payout).sum
  end

  stringify_with_currency :claimed_subsidy,
                          :claimed_incentive,
                          :approved_subsidy,
                          :approved_incentive,
                          :claimed_immediate_payout,
                          :approved_immediate_payout,
                          :approved_delayed_payout

  def device_hash_from_claimed(rdc, from_parent: false)
    {
      uid: rdc.trust_trace_uid,
      subject_origin: rdc.subject_origin,
      subject_uid: rdc.subject_uid,
      trust_trace: rdc.claimed_trust_trace_data,
      device: rdc.claimed_devices_data,
      existing_device_claim: (from_parent ? nil : rdc),
      previous_device_claim: (from_parent ? rdc : rdc.parent_rbf_device_claim),
      payments: rdc.claimed_payments_data
    }.with_indifferent_access
  end

  def payments_for_device_data(device_data)
    account_uid = device_data["account_uid"]
    Data::PaymentsTs.where(account_uid:).order(paid_at: :asc).map(&:attributes)
  end

  def device_data(uid, mode: :add)
    mode = mode.to_sym

    # we already have the claim attached...
    existing = rbf_device_claims.find_by(trust_trace_uid: uid)
    return device_hash_from_claimed(existing) if existing

    case mode
    when :add
      query_batch(uid:).first
    when :existing
      r = rbf_device_claims.find_by(trust_trace_uid: uid)
      device_hash_from_claimed r
    when :resubmission
      r = RbfDeviceClaim.of_organization(organization).rejected.where(trust_trace_uid: uid).last
      device_hash_from_claimed r, from_parent: true
    when :repossessed
      r = RbfDeviceClaim.of_organization(organization).approved.where(trust_trace_uid: uid).last
      device_hash_from_claimed r, from_parent: true
    else
      raise "Unknown mode #{mode}"
    end
  end

  # query all successful or data of a single uid
  def query_batch(uid: nil)
    tts = Data::TrustTrace.where organization_id: organization.id, check: trust_trace_check

    tts = if uid
            tts.where(uid:)
          else
            tts.where(result: "True")
          end

    tts.map do |tt|
      device_data_class = "Data::#{tt.subject_origin.delete_prefix('data_').camelize}".constantize
      device_data = device_data_class.find_by organization_id: organization.id,
                                              uid: tt.subject_uid
      next if device_data.blank?

      existing_device_claim = rbf_device_claims.find_by subject_origin: tt.subject_origin,
                                                        subject_uid: tt.subject_uid

      previous_device_claim =
        RbfDeviceClaim.where.not(rbf_claim_id: id).
        find_by(subject_origin: tt.subject_origin, subject_uid: tt.subject_uid)

      {
        uid: tt.uid,
        subject_origin: tt.subject_origin,
        subject_uid: tt.subject_uid,
        trust_trace: tt.attributes,
        device: device_data.attributes,
        existing_device_claim:,
        previous_device_claim:,
        payments: (uid ? payments_for_device_data(device_data) : [])
      }.with_indifferent_access
    end
  end

  def device_claims_for_resubmission
    all_rejected_former = RbfDeviceClaim.rejected.includes(:rbf_claim).
                          where(rbf_claims: { rbf_claim_template:, organization:, aasm_state: :paid_out })

    all_rejected_former.reject do |x|
      RbfDeviceClaim.where(trust_trace_uid: x.trust_trace_uid).
        where.not(rbf_claim_id: id).
        limit(2).
        count > 1
    end
  end

  def device_claims_for_repossession
    all_approved_former = RbfDeviceClaim.includes(:rbf_claim).approved.
                          where(
                            submission_category: %w[initial_claim resubmission],
                            rbf_claims: { rbf_claim_template:, organization:, aasm_state: :paid_out }
                          )
    all_approved_former.reject do |x|
      RbfDeviceClaim.repossessed.where.not(rbf_claim_id: id).exists?(trust_trace_uid: x.trust_trace_uid)
    end
  end

  # Inside the Verification we have two stages called default and secondary.
  # The verification categories for default and secondary stage and their sample sizes are configured in rbf_claim_template
  # In default stage the default category is used (e.g. "desk") for all device claims.
  # After all device claims are verified in default stage and either approved or rejected we sample for the second stage.
  # According to the percentages from approved device claims we sample and change the device claims verification_category
  # When a device claim is UNREACHABLE we take a new one. until the maximum percentage is reached.

  # gives us info which stage of verification we are in
  def broad_verification_stage
    return nil unless verification?

    if rbf_device_claims.all? { _1.verification_category == rbf_claim_template.default_verification_category }
      :default
    elsif rbf_device_claims.all?(&:properly_verified?) && !missing_for_secondary_verification?
      :finished
    else
      :secondary
    end
  end

  # the progress within the verification stage
  def claim_verification_progress
    if rbf_device_claims.none?(&:properly_verified?)
      :started
    elsif rbf_device_claims.all?(&:properly_verified?)
      :done
    else
      :ongoing
    end
  end

  # detailed verification state, combining stage and progress within
  def verification_stage
    bvs = broad_verification_stage
    if bvs.in? %i[default secondary]
      :"#{bvs}_#{claim_verification_progress}"
    else
      bvs
    end
  end

  # put all claims into the default verification category
  def assign_initial_verification_categories
    rbf_device_claims.each { _1.update! verification_category: rbf_claim_template.default_verification_category }
  end

  # for second stage we sample and change the verification category
  def sample_for_secondary_verification!
    calculate_secondary_verification_sample.each do |cat, ids|
      ids.each { rbf_device_claims.find(_1).move_to_secondary_verification! cat }
    end
  end

  # check if we need to move another from default verification category to the secondary one
  def missing_for_secondary_verification?
    secondary_verification_amounts.any? do |_cat, amnts|
      (amnts[:missing]).positive? && potentials_for_secondary_verification.present?
    end
  end

  # check specifically for one category if we can move an
  def should_take_into_secondary_verification_category?(verification_category)
    amounts = secondary_verification_amounts[verification_category]
    return false if amounts.blank?

    (amounts[:missing]).positive? && potentials_for_secondary_verification.present?
  end

  def take_into_secondary_verification_category!(verification_category)
    return nil unless should_take_into_secondary_verification_category?(verification_category)

    potentials_for_secondary_verification.sample.move_to_secondary_verification!(verification_category)
  end

  def potentials_for_secondary_verification
    rbf_device_claims.approved.where(verification_category: rbf_claim_template.default_verification_category)
  end

  def secondary_verification_amounts
    total = rbf_device_claims.count
    config = rbf_claim_template.verification_categories_percentages

    config.without(:default).to_h do |cat, perc|
      min = (total * perc[:min].to_f / 100).ceil
      max = (total * perc[:max].to_f / 100).ceil

      rdcs_in_cat = rbf_device_claims.where(verification_category: cat)
      rdcs_replaceable = rdcs_in_cat.select(&:secondary_verification_approved_unreachable?)

      is = rdcs_in_cat.count
      replaceable = rdcs_replaceable.count

      expected = min + replaceable
      should = [expected, max].min
      missing = should - is

      max_exceeded = expected - max

      warning = if max_exceeded.zero?
                  :limit_reached
                elsif max_exceeded.positive?
                  :limit_exceeded
                end

      [cat, { min:, max:, is:, replaceable:, should:, missing:, expected:, max_exceeded:, warning: }]
    end.with_indifferent_access
  end

  def calculate_secondary_verification_sample
    amounts = secondary_verification_amounts
    result = {}

    dc_ids = potentials_for_secondary_verification.pluck(:id)

    amounts.each do |cat, am|
      sample_ids = dc_ids.sample am[:missing]
      dc_ids -= sample_ids
      result[cat] = sample_ids
    end

    result
  end
end

# quick console thing for demos to approve everything excpet the last 5 device claims
# RbfClaim.last.rbf_device_claims.submitted.to_a[0..-5].each{|c| c.update! state: :approved}
