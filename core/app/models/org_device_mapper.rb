# frozen_string_literal: true

# == Schema Information
#
# Table name: org_device_mappers
#
#  id              :bigint           not null, primary key
#  device_type     :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  device_id       :bigint           not null
#  organization_id :bigint           not null
#
# Indexes
#
#  index_org_device_mappers_on_device_type_and_device_id        (device_type,device_id) UNIQUE
#  index_org_device_mappers_on_device_type_and_organization_id  (device_type,organization_id)
#  index_org_device_mappers_on_organization_id                  (organization_id)
#
class OrgDeviceMapper < ApplicationRecord
  include OwningOrganization

  DEVICE_TYPES = %w[hop_meter aam_v3].freeze

  belongs_to :organization
  validates :device_id, :device_type, presence: true
  validates :device_id, uniqueness: { scope: :device_type }
  validates :device_type, inclusion: { in: DEVICE_TYPES }

  DEVICE_TYPES.each do |dt|
    scope dt.to_sym, -> { where device_type: dt }
  end

  delegate :name, to: :organization, prefix: true, allow_nil: true

  def organization_name=(name)
    self.organization_id = Organization.find_by(name:)&.id
  end

  def self.import_csv!(csv)
    log = []

    table = CSV.parse(csv, headers: true)
    log << "Total rows: #{table.count}"

    table.each_with_index do |row, i|
      odm = OrgDeviceMapper.find_or_create_by device_id: row["device_id"],
                                              device_type: row["device_type"]
      odm.organization_name = row["organization_name"]

      odm_s = "#{odm.device_type} - #{odm.device_id}"
      success_result = odm.persisted? ? "UPDATED" : "SAVED"

      msg = if odm.save
              "OK - #{success_result}"
            else
              "ERR - #{odm.errors.to_a.join('/')}"
            end
      log << "#{i + 1} #{odm_s}: #{msg}"
    end

    log
  end

  def owning_organization
    organization
  end

  def self.to_csv
    CSV.generate do |csv|
      csv << %w[device_id device_type organization_name]
      OrgDeviceMapper.includes(:organization).pluck("device_id", "device_type", "organizations.name").each do |arr|
        csv << arr
      end
    end
  end
end
