# frozen_string_literal: true

# == Schema Information
#
# Table name: encrypted_blobs
#
#  id         :bigint           not null, primary key
#  error      :text
#  url_e      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  import_id  :bigint           not null
#
# Indexes
#
#  index_encrypted_blobs_on_import_id  (import_id)
#
class EncryptedBlob < ApplicationRecord
  belongs_to :import
  has_one :organization, through: :import

  has_one_attached :blob

  def self.store_data!(data, organization, import)
    eb = EncryptedBlob.create!(import:)

    begin
      uri = URI.parse(data)

      encrypted_data = if uri.is_a?(URI::HTTP)
                         PullBlobWorker.perform_async(uri.to_s, eb.id)
                         nil
                       else
                         RsaOaepEncrypt.encrypt(data, organization)
                       end
    rescue
      encrypted_data = RsaOaepEncrypt.encrypt(data, organization)
    end

    eb.attach_blob! encrypted_data if encrypted_data

    eb.code
  end

  def image_to_data_uri(data, content_type)
    encoded = Base64.encode64(data).gsub("\n", "")
    "data:#{content_type};base64,#{encoded}"
  end

  def encrypt_attach_blob!(data, content_type: nil)
    data = image_to_data_uri(data, content_type) if content_type
    attach_blob! RsaOaepEncrypt.encrypt(data, organization)
  end

  def attach_blob!(data)
    io = StringIO.new data
    blob.attach io:, content_type: "application/octet-stream", filename: "encrypted_blob_#{id}", key:, identify: false
    self.error = nil
    save!
  end

  def key
    # will partition the files in minio via the key: encrypted_blobs/1/2/3/abdf5g"
    ["encrypted_blobs", import.organization.id, import.source.id, import.id, id, SecureRandom.hex(4)].join("/")
  end

  def code
    "#{Rails.application.config.public_url}#{Rails.application.routes.url_helpers.encrypted_blob_path(id)}"
  end
end
