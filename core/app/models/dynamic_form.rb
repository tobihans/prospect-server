# frozen_string_literal: true

# == Schema Information
#
# Table name: dynamic_forms
#
#  id                    :bigint           not null, primary key
#  active                :boolean          default(TRUE), not null
#  description           :string
#  form_data             :json
#  name                  :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  rbf_claim_template_id :bigint
#
# Indexes
#
#  index_dynamic_forms_on_rbf_claim_template_id  (rbf_claim_template_id)
#

class DynamicForm < ApplicationRecord
  has_one_attached :logo

  belongs_to :rbf_claim_template, optional: true

  scope :default_order, -> { order name: :asc }
  scope :active, -> { where active: true }
  scope :inactive, -> { where active: false }

  validates :name, presence: true
  validates :form_data, presence: true

  delegate :to_s, to: :name

  def primary_data_category
    form_data.each_value do |v|
      return v["data_table"] if v["data_table"].present?
    end
  end

  def available_for_organization?(organization)
    return true unless rbf_claim_template

    rbf_claim_template.organization_can_create_claims? organization
  end

  def self.available_for_organization(organization)
    DynamicForm.default_order.active.select { _1.available_for_organization? organization }
  end

  def fields
    form_data.values.pluck("fields").map(&:keys).flatten
  end

  def validator
    @validator ||= DynamicForm::Validator.new form_data
  end
end
