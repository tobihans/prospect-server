# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations
#
#  id                :bigint           not null, primary key
#  api_key           :string(1000)
#  crypto_added_at   :datetime
#  iv                :text
#  name              :string           not null
#  postgres_password :string(1000)
#  public_key        :text
#  salt              :text
#  wrapped_key       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  grafana_org_id    :integer
#  user_id           :integer
#
# Indexes
#
#  index_organizations_on_name     (name) UNIQUE
#  index_organizations_on_user_id  (user_id)
#
class Organization < ApplicationRecord
  include Creator
  include OwningOrganization

  #
  # Group of users which provide / read data
  #

  COUNT_LIMIT = 10_000

  # after passing the corresponding migration 20231027112700 everywhere we should be able to drop fixed and previous stuff
  encrypts :api_key, deterministic: { fixed: false }, previous: { deterministic: false } # used for reading / accessing data via API
  encrypts :postgres_password # the organization user in postgres

  has_many :users, dependent: :nullify # organization team
  has_many :visibilities, dependent: :destroy # data it can see
  has_many :projects, dependent: :destroy # data it can see
  has_many :sources, through: :projects # data which is provided by organization
  has_many :imports, through: :sources # imports uploaded by organization

  has_many :organization_group_members, dependent: :destroy
  accepts_nested_attributes_for :organization_group_members, allow_destroy: true

  has_many :rbf_organization_configs, dependent: :destroy
  accepts_nested_attributes_for :rbf_organization_configs, allow_destroy: true

  has_many :organization_groups, through: :organization_group_members
  has_many :rbf_claim_templates, through: :organization_groups
  has_many :organization_users, dependent: :destroy
  has_many :active_users, through: :organization_users, source: :user
  has_many :rbf_products, dependent: :destroy
  has_many :shared_keys, class_name: "SharedKeysVault", dependent: :destroy

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :postgres_password, presence: true
  validates :crypto_keys_complete_or_not_set, acceptance: true

  scope :other, -> { where.not id: Current.organization&.id }
  scope :default_order, -> { order name: :asc }

  auto_strip_attributes :name, squish: true

  with_options on: :create do
    before_validation :generate_postgres_password, :generate_api_key
  end

  before_validation :handle_crypto_keys_before_validation

  after_create :setup_postgres!, :setup_grafana!, :invite_manager!
  after_commit :create_data_views!

  after_create_commit :create_shared_view!

  after_destroy_commit do
    Visibility.full_views_refresh! # TODO: move to background worker
  end

  attr_accessor :create_with_manager_email

  delegate :to_s, to: :name

  def rbf_submitting?
    rbf_claim_templates.any? { _1.organization_can_create_claims? self }
  end

  def submittable_rbf_claim_templates
    rbf_claim_templates.select { _1.organization_can_create_claims? self }
  end

  def data_categories_counts(source_id: nil)
    Postgres::RemoteControl.data_categories.index_with do |dc|
      count_for_category dc, source_id:
    end
  end

  def count_for_category(data_category, source_id: nil)
    Postgres::RemoteControl.count_as_organization(self, data_category:, source_id:, limit: COUNT_LIMIT)
  end

  def count_for_custom_view(custom_view, source_id: nil)
    Postgres::RemoteControl.count_as_organization(self, custom_view:, source_id:, limit: COUNT_LIMIT)
  end

  def custom_views
    Postgres::RemoteControl.custom_views_of_organization self
  end

  def custom_views_counts(source_id: nil)
    custom_views.index_with do |cv|
      count_for_custom_view cv, source_id:
    end
  end

  def all_for_category(data_category, source_id: nil)
    da_ca = Postgres::RemoteControl.data_categories.find { |dc| dc == data_category } # Say NO to SQL injection!
    raise "unknown data_category #{data_category}" unless da_ca

    Postgres::RemoteControl.select_as_organization self, "*", data_category: da_ca, source_id:
  end

  def all_for_custom_view(custom_view, source_id: nil)
    cu_vi = Postgres::RemoteControl.custom_views_of_organization(self).find { |cv| cv == custom_view } # Say NO to SQL injection!
    raise "unknown custom_view #{custom_view}" unless cu_vi

    Postgres::RemoteControl.select_as_organization self, "*", custom_view: cu_vi, source_id:
  end

  def setup_postgres!
    Postgres::RemoteControl.setup_organization! self
  end

  def setup_grafana!
    result = Grafana::RemoteControl.create_organization self
    # skip all the validations and callbacks, as we are already in an after_save callback
    update_column(:grafana_org_id, result[:orgId]) # rubocop:disable Rails/SkipsModelValidations

    Grafana::RemoteControl.create_data_source self
    Grafana::RemoteControl.update_org_theme self, "light"
    Grafana::RemoteControl.default_dashboards_for_organization! self
  end

  def create_data_views!
    Visibility.async_views_refresh! organization_id: id
  end

  def invite_manager!
    #
    # WE MIGHT WANT TO REFACTOR THAT INTO ITS OWN SERVICE.-
    # IF WE DO TAKE THE ALMOST IDENTICAL CODE IN UsersController#create too
    # AND REFACTOR TOGETHER!
    #
    normalized_mail = create_with_manager_email.to_s.strip
    return if normalized_mail.blank?

    user = User.by_normalized_email normalized_mail

    if user
      if user.user_in_organization? self
        :already_in_organization
      elsif user.invitations.pluck(:organization_id).include? id
        :already_invited
      else
        user.organizations << self
        ou = user.organization_users.find_by(organization: self)
        ou.apply_privilege_template("manager")
        user.mail_organization_invite(self)
        :existing_user_invited_as_manager
      end
    else
      user = User.create! email: normalized_mail, organization: self
      ou = user.organization_users.find_by(organization: self)
      ou.apply_privilege_template("manager")
      :new_user_invited_as_manager
    end
  end

  def create_shared_view!
    Postgres::RemoteControl.create_shared_visibility! self
  end

  def generate_api_key
    self.api_key ||= SecureRandom.hex
  end

  def generate_postgres_password
    self.postgres_password ||= SecureRandom.hex
  end

  def encryption_keys?
    [public_key, wrapped_key, iv, salt].all?(&:present?)
  end

  def encryption_keys_blank?
    [public_key, wrapped_key, iv, salt].all?(&:blank?)
  end

  def crypto_keys_complete_or_not_set
    encryption_fields = [iv, salt, wrapped_key, public_key, crypto_added_at]
    encryption_fields.all?(&:present?) || encryption_fields.none?(&:present?)
  end

  def handle_crypto_keys_before_validation
    # dont save empty strings
    self.public_key = public_key.presence
    self.wrapped_key = wrapped_key.presence
    self.iv = iv.presence
    self.salt = salt.presence

    # manage crypto_added_at timestamp
    if encryption_keys?
      self.crypto_added_at ||= Time.zone.now
    elsif encryption_keys_blank?
      self.crypto_added_at = nil
    end
  end

  def remove_encryption_keys!
    self.public_key = nil
    self.wrapped_key = nil
    self.iv = nil
    self.salt = nil
    self.crypto_added_at = nil
    save!
  end

  def crypto_options
    {
      public_key:,
      wrapped_key:,
      iv:,
      salt:,
      enc_shared_keys: SharedKeysVault.where(shared_with_organization_id: id).pluck(:organization_id, :encrypted_key)
    }
  end

  # Checks if this is the main organization specified in env var 'MAIN_ORG_ID'.
  def main_organization?
    id == ENV.fetch("MAIN_ORG_ID", -1).to_i # default to -1, no main organization defined
  end

  def owning_organization
    self
  end
end
