# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id              :bigint           not null, primary key
#  description     :string
#  name            :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer          not null
#  user_id         :integer
#
# Indexes
#
#  index_projects_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_projects_on_organization_id           (organization_id)
#  index_projects_on_user_id                   (user_id)
#
class Project < ApplicationRecord
  include Creator
  include OwningOrganization

  belongs_to :organization

  has_many :sources, dependent: :destroy
  has_many :visibilities, dependent: :destroy
  has_many :visibilities_for_others, ->(p) { where.not organization_id: p.organization_id },
           class_name: "Visibility", inverse_of: false, dependent: nil
  has_many :visible_for_organizations, through: :visibilities, source: :organization

  validates :name, presence: true, uniqueness:  { scope: :organization_id, case_sensitive: false }

  scope :visible_for_organization, lambda { |organization|
    ids = Visibility.where(organization:).pluck :project_id
    where(id: ids)
  }

  scope :default_order, -> { order name: :asc }

  auto_strip_attributes :name, :description, squish: true

  after_create :create_default_visibility

  delegate :to_s, to: :name

  def refresh_views!
    visibilities.each(&:refresh_views!)
  end

  def visible_to_other_organizations
    visible_for_organizations.reject { _1.id == organization_id }
  end

  def default_visibility
    Visibility.find_by organization_id:, project_id: id
  end

  def create_default_visibility
    Visibility.create_default! self
  end

  def owning_organization
    organization
  end
end
