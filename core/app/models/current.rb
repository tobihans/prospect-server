# frozen_string_literal: true

# like old request store gem
# https://api.rubyonrails.org/classes/ActiveSupport/CurrentAttributes.html

class Current < ActiveSupport::CurrentAttributes
  attribute :user

  delegate :organization, to: :user, allow_nil: true

  def self.url_login_token
    jwk = Rails.application.config.jwk
    headers = { kid: jwk.kid }
    JWT.encode(user.jwt_json, jwk.keypair, "ES256", headers)
  end
end
