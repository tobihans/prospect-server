# frozen_string_literal: true

# == Schema Information
#
# Table name: common_products
#
#  id           :bigint           not null, primary key
#  brand        :string(255)
#  data_origin  :string(255)
#  kind         :string(255)
#  model_number :string(255)
#  name         :string(500)      not null
#  properties   :jsonb            not null
#  website      :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_common_products_on_name                             (name)
#  index_common_products_on_name_and_brand_and_model_number  (name,brand,model_number) UNIQUE
#
class CommonProduct < ApplicationRecord
  validates :name, uniqueness: { scope: %i[brand model_number] }

  auto_strip_attributes :brand, :data_origin, :kind, :model_number, :website, squish: true
end
