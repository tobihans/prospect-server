# frozen_string_literal: true

# == Schema Information
#
# Table name: rbf_product_prices
#
#  id                          :bigint           not null, primary key
#  base_subsidized_sales_price :integer          not null
#  base_subsidy_amount         :integer          not null
#  category                    :string           not null
#  original_sales_price        :float            not null
#  valid_from                  :date             not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  rbf_product_id              :bigint           not null
#  user_id                     :bigint
#
# Indexes
#
#  idx_on_rbf_product_id_category_valid_from_73af8c87d8  (rbf_product_id,category,valid_from) UNIQUE
#  index_rbf_product_prices_on_rbf_product_id            (rbf_product_id)
#
class RbfProductPrice < ApplicationRecord
  include ActiveSupport::NumberHelper
  #
  # Represents prices for rbf products which are valid_from a a certain date on.
  #
  # Given you know the rbf_product_id (e.g. 17) and category (e.g. cash )
  # you can query the price valid at a certain purchase date (e.g. 2024-05-06) like so:
  #
  #   SELECT * FROM rbf_product_prices
  #   WHERE rbf_product_id = 17
  #     AND category = 'cash'
  #     AND valid_from <= '2024-05-06'
  #   ORDER BY valid_from DESC
  #   LIMIT 1
  #
  include Creator
  include Currencify

  CATEGORIES = %w[paygo cash].freeze

  belongs_to :rbf_product
  has_one :rbf_claim_template, through: :rbf_product
  has_one :organization, through: :rbf_product

  validates :original_sales_price, :base_subsidized_sales_price, :base_subsidy_amount, :category, :valid_from, presence: true
  validates :category, inclusion: { in: CATEGORIES }
  validates :valid_from, uniqueness: { scope: %i[category rbf_product_id] }

  validates :original_sales_price, comparison: { greater_than: 0 }
  validates :base_subsidized_sales_price, comparison: { greater_than: 0, less_than: :original_sales_price }
  validates :base_subsidy_amount, comparison: { greater_than: 0, less_than: :original_sales_price }
  validate :validate_adjusted_subsidized_price_positive

  scope :default_order, -> { order valid_from: :desc }

  delegate :currency, to: :rbf_claim_template, allow_nil: true

  stringify_with_currency :base_subsidy_amount, :base_subsidized_sales_price, :original_sales_price

  def potentially_valid_at?(date)
    date >= valid_from
  end

  def self.price_for_at(rbf_product, category, purchase_date)
    default_order.find_by(rbf_product:, category:, valid_from: ..purchase_date)
  end

  def subsidy_percentage
    (base_subsidy_amount.to_f / original_sales_price) * 100
  end

  def sums_up?
    base_subsidized_sales_price + base_subsidy_amount == original_sales_price
  end

  # small fix for https://github.com/rails/rails/issues/31975 (has_one through not available on unpersisted instances)
  # will make tests pass where all things are all build but not yet persisted
  def rbf_claim_template
    persisted? ? super : rbf_product&.rbf_claim_template
  end

  private

  def validate_adjusted_subsidized_price_positive
    return unless rbf_claim_template # dont crash if we link to invalid rbf_product

    rbf_claim_template.subsidy_adjustments.each do |field, val_adj|
      val_adj.each do |value, adj|
        adj_price = base_subsidized_sales_price.to_i - adj
        if adj_price <= 0
          error = "must be positive even when adjusted by #{field} -> #{value} -> #{-adj}"
          errors.add :base_subsidized_sales_price, error
        end
      end
    end
  end
end
