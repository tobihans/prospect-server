# frozen_string_literal: true

# == Schema Information
#
# Table name: sources
#
#  id            :bigint           not null, primary key
#  activated_at  :datetime
#  data_category :string
#  details       :jsonb            not null
#  kind          :string           not null
#  name          :string           not null
#  secret        :string(1000)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  project_id    :bigint           not null
#  user_id       :integer
#
# Indexes
#
#  index_sources_on_activated_at         (activated_at)
#  index_sources_on_kind                 (kind)
#  index_sources_on_name_and_project_id  (name,project_id) UNIQUE
#  index_sources_on_project_id           (project_id)
#  index_sources_on_user_id              (user_id)
#
class Source < ApplicationRecord
  include Source::Clockwork
  include Creator
  include OwningOrganization

  # not implemented:
  # - google_sheet

  KINDS = %w[
    manual_upload
    api_push
    s3_bucket
    sunking
    google_drive
    yaka
    aam_parent
    aam_child
    api/spark_meter
    api/sparkmeter_koios
    api/victron
    api/stellar
    api/stellar_multi_org_parent
    api/stellar_multi_org_child
    api/payg_ops
    api/upya
    api/solarman
    manual_input/ueccc
    manual_input/ueccc_s3
    api/supamoto
    recorded_upload/airtel_zm
    recorded_upload/mtn_zm
    api/angaza
  ].freeze

  BUILD_STEPS = %w[
    meta
    connection
    test
    configure
    activate
  ].freeze

  # used for validation
  # available categories depend on the adapter
  DATA_CATEGORIES = [
    "shs",
    "meters",
    "grids",
    "payments_ts",
    "meters_ts",
    "meter_events_ts",
    "shs_ts",
    "grids_ts",
    "custom",
    (Rails.env.production? ? nil : "test")
  ].compact.freeze

  MAIN_ORGANIZATION_ONLY_KINDS = %w[api/stellar_multi_org_parent aam_parent].freeze

  encrypts :secret, deterministic: true # secret provided (api-key, google auth, AWS...)

  belongs_to :project
  has_one :organization, through: :project
  has_many :visibilities, through: :project
  has_many :imports, dependent: :destroy

  with_options on: %i[create update] do
    validates :data_category, :kind, :name, presence: true
    validates :kind, inclusion: { in: KINDS }, if: :data_category # one error is enough
    validates :data_category, inclusion: { in: DATA_CATEGORIES }, if: :kind # one error is enough
    validates :name, uniqueness: { scope: :project_id, case_sensitive: false }
    validate :validate_adapter_data_category, if: :data_category
    validate :adapter_constraints # leave it to the adapter to check settings before saving
    validate :validate_unique_source
  end

  # wizard validations - selectively validate the above on: :wizard_step
  with_options on: :meta do
    validates :kind, presence: true
    validates :kind, inclusion: { in: KINDS }, if: :kind # one error is enough
    validates :name, uniqueness: { scope: :project_id, case_sensitive: false }
  end

  with_options on: :connection do
    validates :name, :data_category, :kind, presence: true
    validates :data_category, inclusion: { in: DATA_CATEGORIES }, if: :data_category # one error is enough
    validates :secret, presence: true, if: -> { adapter_class::SECRET_REQUIRED }
    validate :validate_details
  end

  scope :inactive, -> { where activated_at: nil }
  scope :active, -> { where.not activated_at: nil }
  scope :by_organization, ->(organization) { by_organization_id organization.id }
  scope :by_organization_id, ->(organization_id) { joins(:project).where projects: { organization_id: } }
  # same kind, same org, but not itself
  scope :potential_duplicates, ->(source) { by_organization_id(source.organization.id).where(kind: source.kind).where.not(id: source.id) }
  scope :custom_data, -> { where data_category: :custom }
  scope :default_order, -> { order name: :asc }

  auto_strip_attributes :name, squish: true

  before_validation :autofill_data_category
  before_validation :data_category_from_adapter, on: :connection
  before_save :generate_secret
  before_destroy :delete_data!

  after_commit do
    project.refresh_views!
    run_on_activation
  end

  after_destroy_commit do
    project.refresh_views!
  end

  ## helper to do source kind specific things
  #
  def adapter_class
    "Source::#{kind.camelize}".safe_constantize if kind.present?
  end

  # Adapters can be visible/hidden depending of the organization (main or not)
  def self.visible_kinds(organization)
    KINDS.reject do |k|
      k.in?(MAIN_ORGANIZATION_ONLY_KINDS) && !organization.main_organization?
    end
  end

  def self.grouped_kinds(kinds)
    grouped = {}
    kinds.sort.each do |k|
      group, title = k.include?("/") ? k.split("/", 2) : ["other", k]
      grouped[group] ||= {}
      grouped[group][title] = k
    end
    grouped.sort.to_h
  end

  def self.visible_kinds_grouped(organization)
    grouped_kinds visible_kinds(organization)
  end

  def adapter
    @adapter ||= adapter_class&.new self
  end

  delegate :adapter_constraints, :validate_unique_source, :duplicate_fingerprint, :test_connection, :retrieve_data, :data_schema, :data_origin, :data_categories,
           to: :adapter, allow_nil: true
  delegate :organization_id, to: :project, allow_nil: false

  # form helper - for the details json attributes as form fields
  def details_struct
    OpenStruct.new details
  end

  def details=(details)
    self["details"] = self.details.merge(details).with_indifferent_access
  end

  def data_table
    adapter.try :data_table
  end

  def to_s
    [
      name,
      data_category_matters? ? "(#{data_category&.titleize})" : nil
    ].compact.join(" ")
  end

  def data_category_matters?
    adapter&.data_schema == "generic" && !kind&.start_with?("manual_input")
  end

  def to_s_adapter
    details_s = [
      kind.titleize,
      data_category_matters? ? data_category.titleize : nil
    ].compact.join(" / ")
    "#{name} (#{details_s})"
  end

  def to_s_download
    Naming.columnize name
  end

  def active?
    activated_at.present?
  end

  def inactive?
    !active?
  end

  def wizard_step_links?
    created_at.present? && (active? || created_at < 30.minutes.ago)
  end

  alias active active? # form helper

  def active=(bool)
    b = ActiveModel::Type::Boolean.new.cast(bool)
    self.activated_at = b ? Time.zone.now : nil
  end

  def state
    active? ? "active" : "inactive"
  end

  def custom_data?
    data_category == "custom"
  end

  def custom_view
    Naming.db_custom_view(self) if custom_data?
  end

  def custom_structure_s
    custom_structure_h.map { |k, v| "#{k} => #{v}" }.join(", ") if custom_data?
  end

  def custom_structure_h
    Array(details["custom_columns"]).to_h
  end

  # A string that is interpreted by the corresponding adapter
  def last_imported_hint
    last_valid_import&.last_imported_hint
  end

  def last_valid_import
    imports.
      where.not(ingestion_finished_at: nil). # consider only successful imports
      where(error: [nil, "no_data"]).
      last
  end

  def update_from_csv!(raw_data, data_category)
    i = Import.create!(source: self)
    i.ingestion_started!

    adapter = Source::CsvUpdate.new self
    result = adapter.process_data(raw_data, data_category:)

    i.protocol = result.protocol

    if result.ok? && result.data.present?
      # save the data
      i.store_data! result.format, result.data
    elsif result.ok? && result.data.nil?
      i.error = "no_data" # a bit hacky: "no_data" is a magic string, signaling no_data state
    else
      i.error = result.error
    end

    i.save!
    i.push_manual_update!
    i
  end

  #
  #
  # Parameters
  #
  # - raw_data - in case it is a push-type adapter we need the data here
  # - use_db_import_hint - (Boolean) if true we will load the last successful import from db and take the import_hint from there.
  # - import_hint - (String) If use_db_import_hint is false, import_hint is used. It can be nil as well (default)
  #
  def trigger_import!(raw_data = nil, use_db_import_hint: true, import_hint: nil)
    raise "undefined behaviour - pull adapter can not receive raw_data" if adapter.pull? && raw_data

    imp = nil

    if adapter.push? && !raw_data
      adapter.notify! if adapter.respond_to?(:notify!)
      imp = 42
    else
      loop do
        result = nil
        adapter.reset # cleans up stuff from previous imports
        my_last_imported_hint = last_imported_hint # get it here, before creating the next import...

        imp = Import.run! self do |i|
          adapter.import = i
          result = if adapter.pull?
                     current_import_hint = use_db_import_hint ? my_last_imported_hint : import_hint
                     adapter.retrieve_data current_import_hint
                   else
                     adapter.process_data raw_data
                   end

          i.protocol = result.protocol
          i.last_imported_hint = result.hint

          #
          # We handle 3 cases: ok, failed, no_data
          #
          if result.ok? && result.data.present?
            # save the data
            i.store_data! result.format, result.data
          elsif result.ok? && result.data.nil?
            i.error = "no_data" # a bit hacky: "no_data" is a magic string, signaling no_data state
          else
            i.error = result.error
          end
        rescue => e
          # make sure to get whatever might have been logged in adapter
          i.protocol = adapter.protocol
          i.protocol += "\\n#{e.message}"
          i.error = e.message # signal error
          Rails.logger.error e
          Sentry.capture_exception e
        ensure
          i.save! # in case of error save protocol and last_imported_hint
        end

        # parameter import_hint is only for first iteration, all following should take import_hint from last valid import in db
        use_db_import_hint = true

        # result.ok? avoids looping in any error condition (including 'no_data'), we try again when triggered again...
        run_again = adapter.pull? && result && result.more_data && result.ok?
        break unless run_again
      end
    end

    imp
  end

  def run_on_activation
    ImportWorker.perform_async(source_id: id) if activated_at_previously_changed?(from: nil)
  end

  # /!\ Only call this function if you really know what you do. Test in staging first!
  def move_data_to_source!(other_source, i_know_what_i_do: false)
    raise "Only call this function if you really know what you do. Test in staging first!" unless i_know_what_i_do
    raise "The other source must belong to the same organization" if organization != other_source.organization
    raise "The other source is the same as the original one " if self == other_source

    queries = Postgres::DataTable.all.map do |dt|
      q = "UPDATE #{dt.table_name} SET source_id = #{other_source.id} WHERE source_id = #{id}"
      # we need to switch off the before_update_check triggers! But timescale does not allow DISABLE TRIGGER :()
      if dt.history?
        <<~SQL.squish
          SET session_replication_role = replica;
          #{q};
          SET session_replication_role = DEFAULT;
        SQL
      else
        q
      end
    end
    ActiveRecord::Base.transaction requires_new: true do
      queries.each { |q| Postgres::RemoteControl.query! q }
    end
  end

  def delete_data!
    queries = Postgres::DataTable.all.map do |dt|
      "DELETE FROM #{dt.table_name} WHERE source_id = #{id}"
    end
    queries.each { |q| Postgres::RemoteControl.query! q }
  end

  def owning_organization
    organization
  end

  private

  def validate_details
    adapter_class::REQUIRED_DETAILS_KEYS.each do |k|
      errors.add k, "must be present" if details[k.to_s].blank?
    end
  end

  def autofill_data_category
    self.data_category ||= data_categories.first if data_categories&.one?
  end

  def data_category_from_adapter
    self.data_category ||= adapter.try :auto_data_category
  end

  def validate_adapter_data_category
    errors.add :data_category, "must be in #{data_categories.join(', ')}" unless
      data_category.in? data_categories
  end

  def generate_secret
    self.secret ||= SecureRandom.hex if adapter_class::AUTOGENERATE_SECRET
  end
end
