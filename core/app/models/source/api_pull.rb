# frozen_string_literal: true

class Source
  class ApiPull
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[url header http_method format frequency].freeze
    SECRET_REQUIRED = true
    PULL = true

    def validation_hint
      "Please make sure your api is ready at #{details['url']}" % { secret: "****" }
    end

    def validation_result
      retrieve_data
    end

    def retrieve_data(_since = nil)
      # TODO: implement it properly
      Result.new format: :txt, data: "42", error:, protocol:, hint: nil, more_data: nil
    end
  end
end
