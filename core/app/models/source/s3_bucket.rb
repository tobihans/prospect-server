# frozen_string_literal: true

class Source
  #
  # Accesses S3 buckets and downloads objects in lexicographical order
  #
  # Don't reuse instance after a change of credentials.
  #
  class S3Bucket
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[access_key region bucket_name file_format frequency order_by].freeze
    SECRET_REQUIRED = true
    PULL = true

    def validation_hint
      "Please put an example file into your S3 bucket, so you can test the settings."
    end

    def validation_result
      retrieve_data
    end

    def fetch(since)
      step "Checking AWS credentials"
      begin
        s3_client
      rescue => e
        fail! e and return [nil, false, since]
      end

      step "Checking Bucket Access"
      bucket_name = details["bucket_name"]
      acc_err = bucket_access_error bucket_name
      fail! acc_err and return [nil, false, since] if acc_err

      step "Checking for new files#{since ? " after #{since}" : ''}"
      object_list = order_by_strategy(bucket_name, since)

      log "Found #{object_list.count} objects, filtering..."

      object_list = filter_object_list object_list

      log "After filter: #{object_list.count} objects."

      return [nil, false, since] unless object_list.count.positive? # no data

      current_object_key = object_list.first[:key]
      step "Downloading #{current_object_key}"
      resp = download_object bucket_name, current_object_key

      more_data = object_list.length > 1

      [resp.body&.string, more_data, object_list.first[:next_since], object_list.first[:meta]]
    end

    #
    # overwrite this to implement other filters in inheriting class
    #
    def filter_object_list(object_list)
      object_list
    end

    def s3_client
      @s3_client ||= Aws::S3::Client.new(
        region: details["region"], credentials: Aws::Credentials.new(details["access_key"], source.secret)
      )
    end

    def bucket_access_error(bucket_name)
      s3_client.head_bucket(bucket: bucket_name)
      nil
    rescue => e
      Rails.logger.info e
      return e.message unless e.respond_to? :context

      "#{e.message}: #{e&.context&.operation_name} #{e&.context&.params} #{e&.context&.http_response&.body_contents}"
    end

    #
    # returns object keys of objects lexicographically after a given object_key
    #
    def find_latest_objects(bucket_name, start_after = nil); end

    def capabilities
      {
        backfill_completely: true,
        backfill_from_date: details["order_by"] == "last_modified"
      }
    end

    def backfill_from_date_import_hint(from_date)
      from_date.to_s # import_hint format for last_modified strategy
    end

    #
    # Selects and calls the strategy to download the correct files in the correct order
    #
    def order_by_strategy(bucket_name, start_after, order_by = nil)
      strategies = {}

      #
      # a strategy returns an array of [filename, next start after, meta data] triples. The format of next start_after depends on the stragey
      # each file has it's own next start_after because we don't know here how many files we want/are able to download.

      # lexicographical strategy uses the start_after parameter to look for *filenames* after the given filename
      strategies["lexicographical"] = lambda {
        log "using lexicographical strategy"
        params = {
          bucket: bucket_name,
          start_after: # refers to object key and lexicographical order
        }

        new_files = []
        resp = nil
        while resp.nil? || resp.is_truncated
          resp = s3_client.list_objects_v2 params

          params[:continuation_token] = resp.next_continuation_token if resp.is_truncated

          new_files.concat resp.contents
        end

        new_files.sort_by!(&:key) # oldest new file on top of list
        new_files.map { |file| { key: file.key, next_since: file.key, meta: file } }
      }

      # last_modified strategy uses the start_after parameter to look at the *last modified* timestamp of each file
      # and returns newer ones
      strategies["last_modified"] = lambda {
        log "using last_modified strategy"
        params = {
          bucket: bucket_name
        }

        new_files = []
        resp = nil
        while resp.nil? || resp.is_truncated
          resp = s3_client.list_objects_v2 params

          params[:continuation_token] = resp.next_continuation_token if resp.is_truncated

          # we download all (up to 1000 files entries) and filter by the given start_after value
          filtered_response = if start_after.present?
                                since = Time.zone.parse(start_after)
                                resp.contents.filter { |file| file.last_modified > since }
                              else
                                resp.contents
                              end
          new_files.concat filtered_response
        end

        new_files.sort_by!(&:last_modified) # oldest new file on top of list
        new_files.map { |file| { key: file.key, next_since: file.last_modified.to_s, meta: file } }
      }

      # default strategy is lexicographical (to migrate old s3 sources)
      strategy_name = order_by || details["order_by"] || "lexicographical"
      strategies[strategy_name].call
    end

    #
    # :response_target (String, IO) — Where to write response data, file path, or IO object.
    # e.g. to write to a temp file, specify the file's path as string.
    # To get a StringIO don't specify response_target and access with resp.body.read
    #
    def download_object(bucket_name, object_key, response_target = nil)
      s3_client.get_object(
        response_target:,
        bucket: bucket_name,
        key: object_key
      )
    end

    def adapter_constraints
      #
      # if order_by strategy changes, we need to reset the last_import_hint
      #
      return unless source.details_changed?
      # detect order_by change
      return unless source.details_change.first["order_by"] != source.details_change.second["order_by"]

      # find last import
      last_import = source.last_valid_import
      return if last_import.blank?

      last_import.last_imported_hint = nil
      last_import.save!
    end
  end
end
