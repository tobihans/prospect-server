# frozen_string_literal: true

class Source
  class ApiPush
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[].freeze
    SECRET_REQUIRED = false
    AUTOGENERATE_SECRET = true
    PULL = false

    def validation_hint
      "Please POST your data to #{Rails.application.config.public_url}api/in/#{source.data_category} using Curl or another API client and check the result there. Dont forget the Authorization header."
    end

    def swagger_doc_url
      self.class.swagger_doc_url source.data_category
    end

    def self.swagger_doc_url(data_category)
      # https://app.prospect.energy/api/swagger?url=https://app.prospect.energy/swagger_doc#!/in/postApiV1InMetersTs
      "#{Rails.application.config.public_url}api/swagger?url=#{Rails.application.config.public_url}swagger_doc#!/in/postApiV1In#{data_category.to_s.camelize}"
    end

    def validation_result
      Result.new protocol: "This can not be easily tested at the moment",
                 format: nil, data: nil, error: nil, hint: nil, more_data: nil
    end

    def ingest(data)
      step "Applying pseudonymization"
      begin
        data_table.apply_cleaning! data, source.organization
      rescue => e
        fail! e.message
        return
      end
      data
    end
  end
end
