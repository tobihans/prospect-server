# frozen_string_literal: true

class Source
  class GoogleSheet
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[url].freeze
    SECRET_REQUIRED = true
    PULL = true

    def validation_hint
      "Please put some example data int your google sheet"
    end

    def validation_result
      retrieve_data
    end

    def retrieve_data(_since = nil)
      fail! "Not implemented!"
      Result.new format: :json, data: nil, error:, protocol:, more_data: nil, hint: nil
    end
  end
end
