# frozen_string_literal: true

class Source
  module ManualInput
    #
    # Manual input source
    #
    class UecccS3 < S3Bucket
      include Source::ManualInput::UecccConcern

      REQUIRED_DETAILS_KEYS = %i[access_key region bucket_name file_format frequency order_by form].freeze

      def validation_hint
        return unless dynamic_form

        tag.p(I18n.t("build.kinds.ueccc.validation_hint").to_s, class: "mb-4") +
          tag.p(I18n.t("build.kinds.ueccc.validation_hint_template", form_name: dynamic_form.name).to_s, class: "mb-4")
      end

      def process_data(new_data)
        data = ingest_ueccc(new_data)
        Result.new format: :json, data:, protocol:, error:, hint: nil, more_data: nil
      end

      def retrieve_data(since = nil)
        raw, more_data, hint = fetch(since)

        raw = CSV.new raw.strip, headers: true if raw.is_a?(String) && details["file_format"] == "csv"

        raw = JSON.parse raw.strip if raw.is_a?(String) && details["file_format"] == "json"

        data = ingest_ueccc raw.to_a unless failed? || raw.nil?

        Result.new format: :json, data:, more_data:, hint:, protocol:, error:
      end
    end
  end
end
