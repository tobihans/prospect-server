# frozen_string_literal: true

class Source
  module ManualInput
    module UecccConcern
      extend ActiveSupport::Concern

      included do
        def adapter_constraints
          return unless rbf_claim_template

          other_templates = source.project.sources.map(&:adapter).map { _1.try :rbf_claim_template }.compact
          return unless other_templates.present? && !rbf_claim_template.in?(other_templates)

          source.errors.add :form, "can not be mixed with forms for other RBF programs within one Project"
        end

        def auto_data_category
          dynamic_form&.primary_data_category
        end

        def dynamic_form
          DynamicForm.find_by id: details["form"]
        end

        def rbf_claim_template
          dynamic_form&.rbf_claim_template
        end

        def data_origin
          "manual_input"
        end

        def data_categories
          %w[shs meters payments_ts]
        end

        def ingest_ueccc(new_data)
          step "Validate against Form Definition: #{dynamic_form}"
          form_errors = dynamic_form.validator.validate_multiple_to_errors new_data
          if form_errors.present?
            log form_errors
            fail! "Invalid Data" and return
          end

          step "Restructure into Prospect Data Schema"
          # find the partition based on the data_column definition
          partition = dynamic_form.form_data.each_with_object({}) do |(_, values), obj|
            values["fields"].each_key do |key|
              obj[key] = values["data_table"]
            end
          end

          # split up the data based on the partition
          new_data.map! do |hash|
            hash.group_by { |key, _| partition[key] }.to_h.transform_values(&:to_h)
          end

          new_data = new_data.each_with_object(Hash.new { |k, v| k[v] = [] }) do |hash, obj|
            hash.each do |k, v|
              (obj[k] << v).flatten!
            end
          end

          step "Checking for unknown columns"
          if new_data.key? nil
            log "Unknown column(s) detected, will be ignored: #{new_data[nil].map(&:keys).flatten.uniq.join(', ')}"
            new_data.delete nil
          end

          # put payment account_id on each data category, because we might have combined form as input
          # assume: all records belong to each other
          if new_data.count > 1 # more than one table
            new_data.values.first.count.times do |idx|
              payment_account_id = new_data.values.map { _1[idx]["payment_account_id"].presence }.compact.first
              new_data.each_value { _1[idx]["payment_account_id"] = payment_account_id }
            end
          end

          new_data.to_h do |table, objs|
            # apply custom UECCC data treatment on each object and the mapping
            step "Cleaning Submitted Data of #{table}"
            objs.map! { custom_ueccc_data_treatment(table, _1, source.data_category) }

            objs.map! do |obj|
              row = {}
              dynamic_form.form_data.each_value do |group|
                group["fields"].each do |field_key, field|
                  field_key = field_key.to_s
                  data_column = field["data_column"]

                  next if obj[field_key].blank?

                  if field["apply_prefix"]
                    raise "apply_prefix option requires a prefix" unless field["prefix"]
                    raise "apply_prefix option can not be combined with encrypt" if field["encrypt"]

                    row[data_column] = if obj[field_key].to_s.start_with?(field["prefix"])
                                         obj.delete(field_key).to_s
                                       else
                                         "#{field['prefix']}#{obj.delete(field_key)}"
                                       end
                  elsif field["encrypt"]
                    row["#{data_column}_e"] = RsaOaepEncrypt.send field["encrypt"], obj.delete(field_key), source.organization, import
                  else
                    row[data_column] = obj.delete(field_key)
                  end
                end
              end
              row.merge!(obj)
              row.compact_blank!
              row
            end

            step "Applying Generic Data treatment for #{table}"
            [
              table,
              ingest(objs, table: Postgres::DataTable.find(table))
            ]
          end
        rescue => e
          fail! "Internal Error: #{e.message}"
        end

        def custom_ueccc_data_treatment(category, data_row, origin)
          # clean currency delimiters
          %w[payment_amount amount price down_payment financed duration].each do |key|
            data_row[key] = data_row[key].gsub(/[^0-9.]/, "") if data_row[key].presence
          end

          case category
          when "payments_ts"
            data_row["paid_at"] = if data_row["date_of_transaction"].blank?
                                    Time.zone.now.strftime("%Y-%m-%d %H:%M:%S").to_s
                                  else
                                    "#{data_row['date_of_transaction']} #{data_row['time_of_transaction'].presence || '12:00:00'}"
                                  end
            data_row["payment_external_id"] = "ugx_unknown"
            data_row["currency"] = "UGX"
            data_row["account_origin"] ||= origin
          when "shs", "meters"
            data_row["customer_name"] = "#{data_row['given_name']} #{data_row['surname']}"
            data_row["payment_account_id"] = (data_row["payment_account_id"].presence || "ugx_unknown")
            data_row["customer_external_id"] = "ugx_unknown"
            data_row["device_external_id"] = data_row["product_serial_number"]
            data_row["payment_plan_currency"] = "UGX"
            data_row["customer_latitude"], data_row["customer_longitude"] = data_row["gps_location"].to_s.split(",") if
              data_row["gps_location"].present?
          end

          data_row
        end
      end
    end
  end
end
