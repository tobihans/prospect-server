# frozen_string_literal: true

class Source
  module ManualInput
    #
    # Manual input source
    #
    class Ueccc
      include Source::Adapter::Generic
      include Source::ManualInput::UecccConcern

      REQUIRED_DETAILS_KEYS = %i[form].freeze
      SECRET_REQUIRED = false
      PULL = false

      def validation_hint
        tag.p(I18n.t("build.kinds.ueccc.validation_hint").to_s, class: "mb-4") +
          tag.p(I18n.t("build.kinds.ueccc.validation_hint_template", form_name: dynamic_form.name).to_s, class: "mb-4") +
          tag.p(I18n.t("build.kinds.ueccc.validation_hint_access", form_public_access: public_access?).to_s)
      end

      def bulk_upload?
        ActiveRecord::Type::Boolean.new.deserialize details["bulk_upload"]
      end

      def public_access?
        ActiveRecord::Type::Boolean.new.deserialize details["public_access"]
      end

      def self.generate_random_slug
        # generate a random url that is unique and at least 50 characters long
        SecureRandom.hex(50)
      end

      def process_data(new_data)
        data = ingest_ueccc(new_data)
        Result.new format: :json, data:, protocol:, error:, hint: nil, more_data: nil
      end
    end
  end
end
