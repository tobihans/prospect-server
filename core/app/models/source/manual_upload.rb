# frozen_string_literal: true

class Source
  class ManualUpload
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[file_format].freeze
    SECRET_REQUIRED = false
    PULL = false

    def manual_upload?
      true
    end

    def validation_hint
      "Please upload an example file"
    end

    def validation_result
      process_data raw_data
    end
  end
end
