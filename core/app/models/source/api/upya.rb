# frozen_string_literal: true

class Source
  module Api
    class Upya
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[username].freeze
      SECRET_REQUIRED = true
      PULL = true
      URL = "https://data.upya.io/data/"
      DEFAULT_FREQUENCY = "daily"

      RETRIES = 3 # number of times we try to get data from each endpoint

      #
      # Core stuff
      #

      def data_origin
        "upya"
      end

      def data_categories
        ["meters"]
      end

      def validation_hint
        ""
      end

      def validation_result
        retrieve_data
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_s
      end

      def retrieve_data(since = nil)
        data, more_data, hint = fetch(details["username"], source.secret, since)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      #
      # API specific stuff
      #

      ENDPOINTS = {
        contracts: {
          path: "search/contracts",
          clean: lambda { |data, cleaner|
                   data.each do |obj|
                     %w[agent.profile.firstName
                        agent.profile.lastName
                        agent.contact.address
                        agent.contact.email
                        client.profile.firstName
                        client.profile.lastName].each do |key|
                       cleaner.clean_nested_key(obj, key, :text, :all)
                     end

                     cleaner.clean_nested_key(obj, "client.profile.birthday", :birthday, :all)

                     %w[agent.contact.mobile
                        agent.contact.fullNumber
                        agent.contact.secondaryMobile
                        client.contact].each do |key|
                       cleaner.clean_nested_key(obj, key, :phone_number, :all)
                     end
                   end
                 }
        },
        clients: {
          path: "search/clients",
          clean: lambda { |data, cleaner|
            data.each do |obj|
              cleaner.clean_key(obj.dig("profile", "gps"), %w[latitude longitude], :geo, :all) if obj.dig("profile", "gps")
              %w[profile.firstName
                 profile.lastName
                 profile.address
                 contact.email].each do |key|
                cleaner.clean_nested_key(obj, key, :text, :all)
              end

              cleaner.clean_nested_key(obj, "profile.birthday", :birthday, :all)

              %w[contact.mobile
                 contact.fullNumber].each do |key|
                cleaner.clean_nested_key(obj, key, :phone_number, :all)
              end
            end
          }
        },
        payments: {
          path: "search/payments",
          clean: lambda { |data, cleaner|
            data.each do |obj|
              %w[agent.profile].each do |key|
                cleaner.clean_nested_key(obj, key, :text, :all)
              end

              %w[mobile].each do |key|
                cleaner.clean_nested_key(obj, key, :phone_number, :all)
              end
            end
          },
          use_query: true
        },
        assets: {
          path: "search/assets",
          clean: lambda { |data, cleaner|
            data.each do |obj|
              %w[ownedBy.profile].each do |key|
                cleaner.clean_nested_key(obj, key, :text, :all)
              end

              %w[ownedBy.contact.mobile].each do |key|
                cleaner.clean_nested_key(obj, key, :phone_number, :all)
              end
            end
          }
        },
        "contract-events": {
          path: "search/contract-events",
          clean: lambda { |data, cleaner|
            data.each do |obj|
              %w[user.profile agent.profile].each do |key|
                cleaner.clean_nested_key(obj, key, :text, :all)
              end
            end
          },
          use_query: true
        },
        products: {
          path: "products/search"
        },
        deals: {
          path: "deals/search"
        }
      }.freeze

      # see https://developer.upya.io/
      def fetch(username, password, since)
        step "Connecting to Upya API"

        data = {}

        ENDPOINTS.each do |key, ep|
          data[key] = load_endpoint ep, username, password, since

          # if any of the endpoints fails the whole import fails
          fail! "couldn't retrieve any data for #{key}" and return [nil, false, since] if data[key].nil?
        end

        [data.to_json, false, Time.zone.today.to_s]
      end

      def cleaner
        @cleaner ||= Cleaner.new(source.organization)
      end

      #
      # all endpoints share the same parameter, only different fields need to be cleaned
      #
      # endpoint: {path:"path to attach to URL", clean:[Field, ...]}
      def load_endpoint(endpoint, username, password, since)
        log "Connecting to endpoint #{endpoint[:path]}."

        response = post_request(URL + endpoint[:path], username, password, since, endpoint[:use_query])

        log "no data for this endpoint" and return nil unless response&.body

        data = JSON.parse(response.body)

        # clean
        if endpoint[:clean]
          log "Pseudonymize and encrypt data fields."
          endpoint[:clean]&.call(data, cleaner)
        end

        data
      end

      #
      # Helper to construct valid get request
      # Needs complete url. E.g. https://data.upya.io/data/search/clients
      #
      # returns nil in case the request fails
      def post_request(url, username, password, since, use_query)
        auth = { username:, password: }

        # some calls should not be date limited (use_query = false)
        # limit data to last update date. Since upya seems to use only dates (no times), we will overlapp the current day. use $gte instead of $gt
        body = since && use_query ? { query: { updatedAt: { "$gte": since } } }.to_json : { query: {} }.to_json

        log "Using updatedAt: { $gte: #{since} }" if since && use_query

        RETRIES.times do
          response = HTTParty.post(
            url,
            headers: { "Content-Type" => "application/json" },
            basic_auth: auth,
            body:
          )
          Rails.logger.debug response

          return response if response.success?

          Rails.logger.info "Couldn't get data from Upya API #{url}. POST body: #{body}. Status code #{response.code}, message: #{response.message}."
          sleep 4 unless Rails.env.test?
        end

        message = "Couldn't get data after #{RETRIES} tries from Upya API #{url}. POST body: #{body}."
        Rails.logger.warn message
        log message
        nil
      end
    end
  end
end
