# frozen_string_literal: true

class Source
  module Api
    class Victron
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[username].freeze
      SECRET_REQUIRED = true
      PULL = true
      URL = "https://vrmapi.victronenergy.com/v2/"
      DEFAULT_FREQUENCY = Rails.application.config.full_prod_mode ? "120.minutes" : "300.minutes"

      # cache expiry for api supplied bearer token
      TOKEN_EXPIRY_SECONDS = 5183 # 5183s comes from chat with Stefan, in a test the token was still valid after days...

      #
      # Core stuff
      #

      def data_origin
        "victron"
      end

      def data_categories
        %w[grids shs]
      end

      def data_schema
        "#{source.kind}_#{source.data_category}"
      end

      def validation_hint
        ""
      end

      def validation_result
        retrieve_data
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_i.to_s
      end

      def retrieve_data(since = nil)
        data, more_data, hint = fetch(URL, since)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      #
      # API specific stuff
      #

      # floor to last 15 min interval (:00, :15, :30, :45)
      # returns a TimeWithZone
      def self.floor_15min(time)
        Time.zone.at((time.to_i / 15.minutes) * 15.minutes)
      end

      #
      # Helper method that calculates start and end timestamps and decides if there should be
      # a follow up import triggered (more_data = true)
      #
      # since: unix timestamp as int
      #
      # Returns [from (TimeWithZone), to (TimeWithZone), more_data (bool)]
      #
      def self.calc_interval(since)
        since = since.blank? ? 1.day.ago : Time.zone.at(since.to_i) # init to one day ago

        from = floor_15min(since)

        # we give them a minute to get the data available in the API, needs to be checked if this is enough
        timestamp_now = floor_15min(1.minute.ago)
        timestamp_30d = from + 30.days # already floored to 15 minutes

        to = timestamp_30d # max 30 day intervals (API supports 31), also round
        more_data = true

        # dont go into future and signal finished when querying now
        if to >= timestamp_now
          to = timestamp_now
          more_data = false
        end

        [from, to, more_data]
      end

      # see https://vrm-api-docs.victronenergy.com/
      def fetch(url, since)
        from, to, more_data = self.class.calc_interval(since)

        if from == to
          log "Skipping import because interval time has not passed since last import."
          return nil, false, since # just take since from previous import
        end

        step "Connecting to Victron API"
        url = url.strip
        url += "/" unless url.end_with? "/"

        unless bearer_token
          fail! "Can't authenticate with Victron API"
          return nil, false, since
        end

        log "Got bearer token"

        step "Retrieving sites"
        insts = installations(url)

        return nil, false, since unless insts

        log "Found #{insts['records'].size} sites."

        step "Load stats for each site"

        log "Using start=#{from} and end=#{to}."

        # query stats for each installation and add them to the list of installations
        insts["records"].each do |site|
          site["stats_response"] = stats(url, site["idSite"], from.to_i, to.to_i)
        end

        [insts.to_json, more_data, to.to_i]
      end

      def bearer_token(force_reload: false)
        cache_key = "victron_auth_token_#{URL}_#{source.id}"
        auth_hash = Rails.cache.fetch(cache_key)

        return auth_hash if auth_hash && !force_reload

        log "Requesting new bearer token..."
        body = {
          username: details["username"],
          password: source.secret,
          remember_me: true
        }.to_json
        response = HTTParty.post("#{URL}auth/login", body:)

        # if we get caught in a rate limit we get something like that:
        # => {"success"=>false, "errors"=>"Too many requests. Please try again in a few minutes", "error_code"=>nil}
        fail! "Failed to login: #{response['errors']}" and return if response["errors"].present?

        auth_hash = { user_id: response["idUser"], token: response["token"] }
        Rails.cache.write(cache_key, auth_hash, expires_in: TOKEN_EXPIRY_SECONDS)

        auth_hash
      end

      def request_headers
        { "Content-Type" => "application/json", "x-authorization" => "Bearer #{bearer_token[:token]}" }
      end

      #
      # Helper to construct valid get request
      # Needs complete url. E.g. https://vrmapi.victronenergy.com/v2/users/123/installations
      #
      #
      def get_request(url)
        response = HTTParty.get(
          url,
          headers: request_headers
        )

        # Retry once for invalid / expired tokens
        #
        # token invalid: {"success":false,"errors":"Token is invalid","error_code":"invalid_token"}
        # token expired: couldn't reproduce this case, assuming that the error code contains 'token' as well ;-)
        if response.code == 401 && (response["error_code"].include? "token")
          bearer_token(force_reload: true) # get new token
          response = HTTParty.get(
            url,
            headers: request_headers
          )
        end

        fail! "Couldn't get data from Victron API. Status code #{response.code}, message: #{response.message}." and return if response.code != 200

        response
      end

      def installations(url)
        response = get_request("#{url}users/#{bearer_token[:user_id]}/installations?extended=1")
        return unless response

        clean_inst response
        response
      end

      #
      # see https://vrm-api-docs.victronenergy.com/#/operations/installations/idSite/stats for
      # parameter meaning and additional parameters
      #
      # for a list of attributes check out the result for
      # https://vrm-api-docs.victronenergy.com/#/operations/installations/idSite/diagnostics
      #
      # (end - start) must not be more than 31 days
      def stats(url, installation_id, from, to)
        query_str = "interval=15mins&type=custom&start=#{from}&end=#{to}"

        kpi_params = ["bs", # Battery Storage % charged
                      "Bc", # Battery to consumers
                      "Pb", # PV to battery
                      "Pc"] # PV to consumers

        kpi_params.each { |kpi| query_str += "&attributeCodes[]=#{kpi}" }

        get_request("#{url}installations/#{installation_id}/stats?#{query_str}")
      end

      def clean_inst(resp)
        cleaner = Cleaner.new source.organization
        resp["records"].each do |inst|
          inst["extended"].each do |ext|
            if ext["description"].in? %w[Latitude Longitude]
              inst[ext["description"].downcase] = ext["rawValue"]
              inst["extended"].delete ext
            end
          end
          cleaner.clean_key inst, "phonenumber", :phone_number, :all
          cleaner.clean_key inst, %w[latitude longitude], :geo, :all
        end
        resp
      end
    end
  end
end
