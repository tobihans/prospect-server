# frozen_string_literal: true

class Source
  module Api
    class SparkmeterKoios
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[api_key].freeze
      SECRET_REQUIRED = true
      PULL = true
      URL = "https://www.sparkmeter.cloud/api/v1/"
      MAX_RETRIES = 3
      # import takes around ~26min (~250 pages of 5 customers)
      DEFAULT_FREQUENCY = "daily"

      #
      # Core stuff
      #

      def data_origin
        "sparkmeter" # koios is the successor of sparkmeter api, assume they share the same id space
      end

      def data_categories
        ["meters"]
      end

      def validation_hint
        ""
      end

      def validation_result
        @validation_mode = true
        retrieve_data
      end

      def retrieve_data(_since = nil)
        data, more_data, hint = fetch(details["api_key"], source.secret)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      #
      # API specific stuff
      #

      ENDPOINT = {

        path: "customers",
        clean: lambda { |data, cleaner|
                 data.each do |obj|
                   cleaner.clean_nested_key(obj, "name", :text, :all)
                   cleaner.clean_nested_key(obj, "phone_number", :phone_number, :all)

                   # meters is an array...
                   obj["meters"].each do |meter|
                     cleaner.clean_nested_key(meter, "address", :text, :all)
                     cleaner.clean_key(meter["coordinates"], %w[latitude longitude], :geo, :all) if meter["coordinates"]
                   end
                 end
               },
        query: {
          reading_details: true,
          per_page: 20 # we hit timeouts trying to get more customers when using cursor, 20 determined with trial and error
        }

      }.freeze

      def fetch(api_key, api_secret)
        step "Get customers from Koios API"

        data = {}
        data["customers"] = load_endpoint ENDPOINT, api_key, api_secret

        # if any of the endpoints returned something, we don't fail
        fail! "couldn't retrieve any data from API" and return [nil, false, nil] if data["customers"].blank?

        [data.to_json, false, nil]
      end

      def cleaner
        @cleaner ||= Cleaner.new(source.organization)
      end

      #
      # all endpoints share the same parameter, only different fields need to be cleaned
      #
      # endpoint: {path:"path to attach to URL", clean:[Field, ...]}
      def load_endpoint(endpoint, api_key, api_secret)
        log "Connecting to endpoint #{endpoint[:path]}."

        data = []
        current_response = nil
        # paging loop
        page = 0
        while current_response.nil? || current_response["cursor"].present?
          page += 1
          log "Get page #{page}..."

          query = endpoint[:query].dup
          query.merge!({ cursor: current_response["cursor"] }) if current_response && current_response["cursor"].present?

          response = get_request(URL + endpoint[:path], api_key, api_secret, query)

          break if response&.body.blank? # we don't expect more data after empty response

          current_response = JSON.parse(response.body)
          data.concat current_response["data"]

          break if @validation_mode # when validating, one request is enough
        end

        log "no data for this endpoint" and return nil if data.empty?

        # clean
        if endpoint[:clean]
          log "Pseudonymize and encrypt data fields."
          endpoint[:clean]&.call(data, cleaner)
        end

        data
      end

      #
      # Helper to construct valid get requests
      #
      def get_request(url, api_key, api_secret, query)
        headers = { "X-API-KEY" => api_key, "X-API-SECRET" => api_secret }

        retries = 0
        begin
          response = HTTParty.get(url, query:, headers:, timeout: 10) # sometimes we get a socket closed timeout which takes a while..
        rescue => e
          if retries < MAX_RETRIES
            retries += 1
            Rails.logger.info "Going for retry #{retries}, because of: #{e.message}"
            sleep 2**retries unless Rails.env.test?
            retry
          else
            fail! "Failed after #{MAX_RETRIES} retries. Error: #{e.message}"
            return
          end
        end

        if response.code != 200
          message = "Couldn't get data from Koios API #{response.request.last_uri}. Status code #{response.code}, message: #{response.message}."
          fail! message
          Rails.logger.info message
          return
        end
        response
      end
    end
  end
end
