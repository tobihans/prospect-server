# frozen_string_literal: true

class Source
  module Api
    #
    # Implements Stellar Connector for HOP smartmeters
    #
    class Stellar
      include Source::Adapter::Api

      CONCURRENT_REQUESTS = Rails.application.config.full_prod_mode ? 2 : 1

      REQUIRED_DETAILS_KEYS = %i[].freeze
      SECRET_REQUIRED = true
      PULL = true
      URL = "https://stellar.newsunroad.com/api/v0/"
      DEFAULT_FREQUENCY = Rails.application.config.full_prod_mode ? "15.minutes" : "120.minutes"

      MAX_DATE_RANGE = 30.days # max allowed interval per request

      # backfill calls that are more than 5 days in the past need only to query data that is 5 days in the past, because we will use `start`
      # parameter later on (instead of `receivedAfter`)
      # to avoid ugly border cases we take 4 days as limit
      RECEIVED_AFTER_MAX_PAST = 4.days

      #
      # Core stuff
      #

      # there is a v1 dataworker with name "api/stellar", but we want to trigger v2 dataworker here
      def data_schema
        "api/stellar_v2"
      end

      def data_origin
        "stellar"
      end

      def data_categories
        ["meters"]
      end

      def validation_hint
        ""
      end

      def validation_result
        retrieve_data nil, true
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_s
      end

      def retrieve_data(since = nil, test_connection = nil)
        data, more_data, hint = fetch(since, test_connection)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      #
      # API specific stuff
      #

      #
      # when test_connection = true, we try to shorten the time for the whole data retrieval process
      # and only get raw data for the first few installations
      #
      def fetch(since, test_connection)
        step "Retrieving sites from Stellar API"
        insts = installations

        if insts.body.blank? || insts["data"].nil?
          fail! "Can't read server response"
          return [nil, false, since]
        end

        log "Found #{insts['data'].size} sites."

        return [nil, false, since] unless insts["data"].size.positive?

        from, to, more_data = self.class.timerange since

        step "Load raw data for each site"

        # for parallel execution of requests
        hydra = Typhoeus::Hydra.new(max_concurrency: CONCURRENT_REQUESTS)

        # this makes sure we get all the data, even latecomers, where receivedAfter is newer than there start date
        start_param = if from >= (RECEIVED_AFTER_MAX_PAST.ago - 1.hour)
                        "receivedAfter" # new stuff
                      else
                        "start" # old stuff, backfill etc.
                      end

        log "Timerange: #{from} => #{to}. Using param '#{start_param}' for query."

        query_str = "#{start_param}=#{from.rfc3339}&end=#{to.rfc3339}"

        # query stats for each installation and add them to the list of installations
        insts["data"].each_with_index do |site, counter|
          request = get_request_typhoeus("#{URL}rawData/a2ei_#{site['uid']}?#{query_str}")

          request.on_complete do |response|
            log "Receiving data for site #{site['uid']}"
            if response.success?
              site["rawData_response"] = self.class.parse_jsonl(response.body)
            else
              log "Failed to get data for site #{site['uid']}: #{response.status_message}"
              Rails.logger.warn("Stellar failed request: #{response.body}")
            end
          end

          hydra.queue request

          if counter >= 2 && (test_connection || !Rails.application.config.full_prod_mode)
            log "Testing connection, only getting data for the first 3 sites."
            break
          end
        end

        hydra.run # blocking call, executes requests in parallel

        [insts.to_json, more_data, to.to_s]
      rescue => e
        fail! "While retrieving data an error occured: #{e.message}"
        Rails.logger.error "Error: Stellar API request failed #{e}"
        [nil, false, since]
      end

      #
      # Parses JSONL (each line contains valid json) and returns an array containing all lines: [parsed_json1, parsed_json2, ...]
      #
      def self.parse_jsonl(jsonl)
        jsonl.lines.map { JSON.parse _1 }
      end

      #
      # since: date time string as in Time.to_s
      # We use different queries for different timeranges. Ranges more than 4 days ago need to be separated from ranges after 4 days ago.
      #
      #
      def self.timerange(since)
        from = since.blank? ? 1.day.ago : Time.zone.parse(since)

        # we give them a minute to get the data available in the API, needs to be checked if this is enough
        timestamp_now = 1.minute.ago
        timestamp_max_to = from + MAX_DATE_RANGE

        to = timestamp_max_to

        # dont go into future and signal finished when querying now
        more_data = false
        if to >= timestamp_now
          to = timestamp_now
        else
          more_data = true
        end

        # no range can have RECEIVED_AFTER_MAX_PAST inside, either we query before or after this date...
        if from < RECEIVED_AFTER_MAX_PAST.ago && to > RECEIVED_AFTER_MAX_PAST.ago
          to = RECEIVED_AFTER_MAX_PAST.ago + 1.hour # we add 1 hour to avoid that this happens on the next import as well
          more_data = true # we need one more import which will be [4.days.ago + 1.hour, now]
        end

        [from, to, more_data]
      end

      #
      # Helper to construct valid get request
      #
      def get_request(url)
        Rails.logger.warn "Stellar Connector Get #{url}"
        response = HTTParty.get(
          url,
          headers: { "Content-Type" => "application/json",
                     "Authorization" => "Token #{source.secret}" }
        )

        fail! "Couldn't get data from Stellar API. Status code #{response.code}, message: #{response.message}, body: #{response.body}." and return if response.code != 200

        response
      end

      def get_request_typhoeus(url)
        Typhoeus::Request.new(url, headers: { "Content-Type" => "application/json",
                                              "Authorization" => "Token #{source.secret}" })
      end

      def installations
        response = get_request("#{URL}sites")
        return unless response

        pseudonymize_and_encrypt_inst response
      end

      def pseudonymize_and_encrypt_inst(resp)
        # typical array element in resp["data"]:
        #
        #  {"contactName"=>nil,
        #   "phoneNumber1"=>nil,
        #   "phoneNumber2"=>nil,
        #   "meterId"=>2020123456789,
        #   "connectionDate"=>"2021-05-04",
        #   "application"=>nil,
        #   "latitude"=>nil,
        #   "longitude"=>nil,
        #   "siteType"=>"xzy_xz_12345",
        #   "market"=>nil,
        #   "address"=>"Cameroon",
        #   "reportingInterval"=>1,
        #   "uid"=>"12345",
        #   "operatorId"=>"a2ei",
        #   "country"=>"CM",
        #   "timezone"=>"Africa/Lagos",
        #   "powerSystemView"=>{"regionId"=>"cm_uol_mecs"}}

        # set source organization
        cleaner = Cleaner.new(source.organization)
        resp["data"].each do |inst|
          %w[contactName address].each do |key|
            cleaner.clean_key inst, key, :text, :all
          end
          %w[phoneNumber1 phoneNumber2].each do |key|
            cleaner.clean_key inst, key, :phone_number, :all
          end
          cleaner.clean_key inst, %w[latitude longitude], :geo, :all
        end
        resp
      end
    end
  end
end
