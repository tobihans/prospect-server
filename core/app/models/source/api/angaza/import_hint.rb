# frozen_string_literal: true

class Source
  module Api
    class Angaza
      class ImportHint
        attr_reader :status, :endpoints, :first_run_hint

        INITIAL_BACKFILL = "initial_backfill"
        NORMAL = "normal"

        def initialize(since, endpoints)
          parsed = JSON.parse(since) if since.present?
          # need to check for status as a migration from old import_hint_format
          @hint = parsed.present? && parsed["status"].present? ? parsed : initial_hint(endpoints)
          @status = @hint["status"]
          @endpoints = @hint["endpoints"]
          @first_run_hint = @hint["first_run_hint"]
        end

        # Check if we are in initial backfill mode and need to continue
        def initial_backfill?
          @status == INITIAL_BACKFILL
        end

        # Check if this is the first import of the backfill
        def first_run?(endpoint)
          initial_backfill? && @endpoints[endpoint].zero?
        end

        # compute next hint with new status and endpoint offsets
        def generate_next_hint(responses, loop_status, endpoints_config)
          all_hit_last_page = loop_status.all? { |_, value| value[:hit_exit_condition] == false }
          next_status = initial_backfill? && !all_hit_last_page ? INITIAL_BACKFILL : NORMAL

          first_run_hint = if first_run?(endpoints_config.keys.first) # just check for first endpoint
                             build_first_run_hint(responses, endpoints_config)
                           else
                             @first_run_hint # take old hint, this keeps information form the first import until the end of INITIAL_BACKFILL
                           end

          next_import_hints = build_next_import_hints(responses, loop_status, endpoints_config, first_run_hint, next_status)

          next_hint = {
            "status" => next_status,
            "endpoints" => next_import_hints

          }
          next_hint.merge!({ "first_run_hint" => first_run_hint }) if next_status == INITIAL_BACKFILL

          next_hint
        end

        private

        # Build the initial hint
        def initial_hint(endpoints)
          {
            "status" => INITIAL_BACKFILL,
            "endpoints" => endpoints.to_h { |endpoint, _config| [endpoint, 0] }
          }
        end

        # Build the first run hint based on the responses
        def build_first_run_hint(responses, endpoints_config)
          endpoints_config.to_h do |endpoint, config|
            items = responses[endpoint].dig("_embedded", "item")
            hint = items.first.present? ? items.first[config[:last_import_hint_key]] : nil # take hint from first (newest!) item
            [endpoint, hint]
          end
        end

        # Build the next import hints for both modes (initial backfill or normal)
        def build_next_import_hints(responses, loop_status, endpoints_config, first_run_hint, next_status)
          endpoints_config.to_h do |endpoint, config|
            if next_status == INITIAL_BACKFILL
              [endpoint, loop_status[endpoint][:offset]]
            else
              items = responses[endpoint].dig("_embedded", "item")
              hint = if config[:last_import_hint_key].nil?
                       nil
                     elsif initial_backfill?
                       # here we transition from INITIAL_BACKFILL to NORMAL mode
                       # we use the very first import, which was looped through all INITIAL_BACKFILL imports
                       # and take the import_hint values from there.
                       first_run_hint[endpoint]
                     elsif items.empty?
                       @endpoints[endpoint].presence # take the last import hint
                     else
                       # response is ordered by API by last_import_hint_key in descending order
                       # First item is the newest item. We save the value per endpoint as last_import_hint
                       items.first[config[:last_import_hint_key]]
                     end
              [endpoint, hint]
            end
          end
        end
      end
    end
  end
end
