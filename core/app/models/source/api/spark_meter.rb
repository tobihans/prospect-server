# frozen_string_literal: true

class Source
  module Api
    class SparkMeter
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[url].freeze
      SECRET_REQUIRED = true
      PULL = true
      DEFAULT_FREQUENCY = Rails.application.config.full_prod_mode ? "10.minutes" : "120.minutes"

      #
      # Core stuff
      #
      def data_origin
        "spark_meter"
      end

      def data_categories
        ["meters"]
      end

      def validation_hint
        ""
      end

      def validation_result
        retrieve_data
      end

      def retrieve_data(_since = nil)
        # SparkMeter API doesn't need a since parameter, always the full list of customers with updated values is returned.
        # Therefor we always signal more_data: nil after one call
        data = fetch(details["url"], source.secret).to_json

        Result.new format: :json, data:, error:, protocol:,
                   hint: nil, more_data: nil
      end

      #
      # API specific stuff
      #

      # see https://api.sparkmeter.io/#07548a18-f018-44e2-b2b7-de26725c98a9
      def fetch(url, token)
        step "Connecting to SparkMeter API"
        fail! "URL must be present" and return if url.blank?

        url += "/" unless url.end_with? "/"
        begin
          response = HTTParty.get(
            "#{url}customers",
            headers: { "Content-Type" => "application/json", "Authentication-Token" => token }
          )
        rescue => e
          fail! e.message
          return
        end

        if response.code != 200
          fail! "Couldn't get data from SparkMeter API. Status code #{response.code}, message: #{response.message}."
          return
        end

        if response["customers"].present?
          log "Found #{response['customers'].size} customers"
          clean_response response
        else
          fail! "No customers found"
        end
      end

      def clean_response(resp)
        step "Encrypt and Pseudonymize data"
        cleaner = Cleaner.new(source.organization)

        resp["customers"].each do |customer|
          cleaner.clean_key customer, "name", :text, :all
          cleaner.clean_key customer, "phone_number", :phone_number, :all

          customer["meters"].each do |meter|
            %w[address street1 street2].each do |key|
              cleaner.clean_key meter, key, :text, :all
            end
            begin
              cleaner.clean_key meter, ["coord"], :geo, :all
            rescue => e
              # log parse error but still use the meter data
              Rails.logger.error "SparkMeter: cant parse meter coord. Continuing with empty location values. #{e}"
            end
          end
        end
        resp
      end

      # Example customers response:
      #
      # {customers: [
      #   {"code"=>"",
      #     "credit_balance"=>123.4567890123,
      #     "debt_balance"=>0.0,
      #     "ground"=>{"id"=>"123e4567-e89b-12d3-a456-426614174000", "name"=>"xyz-location001"},
      #     "id"=>"223e4567-e89b-12d3-a456-426614174001",
      #     "meters"=>
      #      [{"active"=>true,
      #        "address"=>"A-1, xxxxxxx, North Western Province, Zambia",
      #        "bootloader"=>"0100020000",
      #        "city"=>"Mwinilunga",
      #        "coords"=>"",
      #        "country"=>"Zambia",
      #        "current_daily_energy"=>nil,
      #        "current_tariff_name"=>"Medium Consumption",
      #        "firmware"=>"0100050000",
      #        "is_running_plan"=>true,
      #        "last_config_datetime"=>"2022-11-17T06:15:26.406849",
      #        "last_cycle_start"=>"2022-11-01T19:00:00",
      #        "last_energy"=>1999.9123456,
      #        "last_energy_datetime"=>"2022-11-20T23:30:00",
      #        "last_meter_state_code"=>1,
      #        "last_plan_expiration_date"=>"2022-11-21T06:15:00",
      #        "last_plan_payment_date"=>"2022-11-20T06:15:00",
      #        "model"=>"SM60R",
      #        "operating_mode"=>2,
      #        "plan_balance"=>6.74597901785486,
      #        "postalcode"=>"",
      #        "serial"=>"SM60R-07-000123456",
      #        "state"=>"North Western Province",
      #        "street1"=>"A-1",
      #        "street2"=>"",
      #        "tags"=>[],
      #        "total_cycle_energy"=>226.718437500002}],
      #     "name"=>"Max Musterman",
      #     "phone_number"=>"+260123456789",
      #     "phone_number_verified"=>false}
      # ],
      # status: success,
      # errors: nil
      # }
    end
  end
end
