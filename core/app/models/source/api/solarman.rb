# frozen_string_literal: true

# SolarMAN is a solar inverter monitoring platform that is used by many different inverter manufacturers.
# This adapter is used by the following manufacturers:
# - Ginlong
# - GoodWe
# - Growatt
# - Kostal
# - Omnik
# - Solis
# - Sungrow
# - Trannergy
# - Voltronic
# - Zeversolar
# - Deye
# This class will allow the import of real time device data that is available through SolarMan
# Data is received through an OpenAPI compatible REST API.

class Source
  module Api
    class Solarman
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[app_id app_secret email].freeze
      SECRET_REQUIRED = true
      PULL = true
      URL =  "https://globalapi.solarmanpv.com/"
      DEFAULT_FREQUENCY = "daily"

      def cache_authtoken_key
        @cache_authtoken_key ||= "auth_token_#{URL}#{details['app_id']}"
      end

      #
      # Core stuff
      #

      def data_origin
        "solarman"
      end

      def data_categories
        ["meters"]
      end

      def validation_hint
        ""
      end

      def validation_result
        if details["app_id"].blank? || details["app_secret"].blank? || details["email"].blank? || source.secret.blank?
          return Result.new format: :json, data: nil, error: "ERROR", protocol: "ERROR", more_data: nil, hint: "parameter missing"
        end

        retrieve_data
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_s
      end

      def retrieve_data(since = nil)
        log "Using since=#{since}"
        data, more_data, hint = fetch(since)
        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      def fetch(since = nil)
        from_date = since.presence || 31.days.ago # init to 30 days ago
        from_date = from_date.to_date # make sure it is a date
        email = details["email"]
        password = source.secret
        app_id = details["app_id"]
        app_secret = details["app_secret"]

        if from_date >= Time.zone.today
          log "Skipping import because interval time has not passed since last import."
          return nil, false, since # just take since from previous import
        end

        step "Connecting to Solarman API"
        auth_data = auth_token(email, password, app_secret, app_id) # get the Bearer token

        unless auth_data
          fail! "Can't authenticate with Solarman API"
          return nil, false, since
        end
        log "Got bearer token"

        # the list of plants is needed to get the installation id for the stats query
        step "Retrieving plant list"
        plants = plant_list(auth_data)

        # sometimes the Bearer token is not valid anymore (although it should be)- then we need to get a new one
        unless plants
          log "Auth token expired premature - getting new one..."
          auth_data = auth_token(email, password, app_secret, app_id) # get the Bearer token
          return nil, false, since unless auth_data

          plants = plant_list(auth_data)
        end

        return nil, false, since unless plants

        log "Found #{plants.length} plants."

        step "Load stats for each site"

        log "Using start=#{from_date}."

        # query stats for each installation and add them to the list of installations
        plants.each do |plant|
          plant["stats_frame_detail_data"] = historic_stats(plant["id"], auth_data, from_date)
          plant["stats_daily_aggregated_data"] = historic_stats(plant["id"], auth_data, from_date, 2)
        end

        plants = clean_inst(plants)

        [plants, false, Time.zone.today]
      end

      def auth_token(email, password, app_secret, app_id)
        # first check if there is still a valid token that can be used
        # Term of validity of token is two month (60 days)
        cache_authtoken_key = "auth_token_#{URL}#{app_id}"
        auth_token = Rails.cache.fetch(cache_authtoken_key)

        return { accessToken: auth_token } unless auth_token.nil?

        # if there is no key anymore check for a refresh token - seems not implemented yet on their side
        # refresh_token_cache_key = "refresh_#{url}#{email}#{app_id}"
        # refresh_token = Rails.cache.fetch(refresh_token_cache_key)

        # sample call:
        # curl --location --request POST 'https://globalapi.solarmanpv.com/account/v1.0/token?appId=YOUR_APP_ID&language=en' \
        # --header 'Content-Type: application/json' \
        # --header 'Cookie: acw_tc=sample' \
        # --data-raw '{
        #   "appSecret": "YOUR_APP_SECRET",
        #   "password": "SHA256_YOUR_SOLARMAN_WEBSITE_PASSWORD",
        #   "email": "YOUR_SOLARMAN_WEBSITE_EMAIL"
        # }'

        #
        # sample response:
        # {
        #     "code": null,
        #     "msg": null,
        #     "success": true,
        #     "requestId": "b07f1630999db2b4",
        #     "access_token": "SAMPLE_TOKEN",
        #     "token_type": "bearer",
        #     "refresh_token": "SAMPLE",
        #     "expires_in": "5183999",
        #     "scope": null,
        #     "uid": 13307020
        # }
        #

        log "Asking for new Bearer token"
        response = HTTParty.post("#{URL}account/v1.0/token?appId=#{app_id}",
                                 body: { appSecret: app_secret,
                                         email:,
                                         password: Digest::SHA256.hexdigest(password) }.to_json,
                                 headers: { "Content-Type" => "application/json" })

        fail! "Failed to login: #{response['msg']}" and return unless response["success"]

        # cache the token for 59 days
        validity_of_token = response["expires_in"].to_i

        Rails.cache.write(cache_authtoken_key, response["access_token"], expires_in: (validity_of_token - 1000).seconds)

        { access_token: response["access_token"], refresh_token: response["refresh_token"] }
      end

      def plant_list(auth_data)
        # sample call:
        # curl --location --request POST 'https://globalapi.solarmanpv.com/station/v1.0/list?language=en' \
        # --header 'Authorization: Bearer YOUR_BEARER_AUTH_TOKEN' \
        # --header 'Content-Type: application/json' \
        # --data-raw '{
        # "page": 1, "size": 100
        # }'

        # sample resonse:
        # {
        #     "code": null,
        #     "msg": null,
        #     "success": true,
        #     "requestId": "6c113d6ed6d8704d",
        #     "total": 1,
        #     "stationList": [
        #         {
        #             "id": 60993186,
        #             "name": "SAM",
        #             "locationLat": 7.1758639,
        #             "locationLng": 2.1525448,
        #             "locationAddress": "Samionta ",
        #             "regionNationId": 20,
        #             "regionLevel1": 456,
        #             "regionLevel2": null,
        #             "regionLevel3": null,
        #             "regionLevel4": null,
        #             "regionLevel5": null,
        #             "regionTimezone": "Europe/Amsterdam",
        #             "type": "GROUND",
        #             "gridInterconnectionType": "BATTERY_BACKUP",
        #             "installedCapacity": 17.48,
        #             "startOperatingTime": 1688762734.000000000,
        #             "stationImage": "https://img1.solarmanpv.com/default/3fce41ab612144a2a2f1a4555d67ea851696346430577.jpg",
        #             "createdDate": 1688762775.000000000,
        #             "batterySoc": 100.0,
        #             "networkStatus": "NORMAL",
        #             "generationPower": 1414.0,
        #             "lastUpdateTime": 1699029886.000000000,
        #             "contactPhone": null,
        #             "ownerName": null
        #         }
        #     ]
        # }
        page = 1
        max_pages = 1
        size_per_page = 50

        plants = []
        while page <= max_pages
          log "Loading page #{page}"
          response = HTTParty.post("#{URL}station/v1.0/list?language=en",
                                   headers: { "Content-Type" => "application/json",
                                              "Authorization" => "Bearer #{auth_data[:access_token]}" },
                                   body: { page:,
                                           size: size_per_page }.to_json)
          page += 1
          # sometimes the auth token expires too early then we need to get a new one

          if response && response["msg"] == "auth invalid token"
            log "Auth token expired - getting new one - need to retrigger import"
            Rails.cache.delete(cache_authtoken_key)
            return false
          end

          return nil unless response && response["success"] && response["stationList"] && response["stationList"].length.positive? # check if there are any plants

          plants += response["stationList"]

          log "Total pages: #{response['total'].to_i}" if response["total"].to_i > 1 && page == 2 # only log once
          max_pages += 1 if response["total"].to_i > size_per_page * page
        end
        log "Received #{plants.length} plants."
        plants
      end

      #
      # load historic stats for site
      # (end - start) must not be more than 31 days
      # the data will be sent for the startTime day
      #
      # sample call:
      # curl --location --request POST 'https://globalapi.solarmanpv.com/station/v1.0/history?language=en' \
      # --header 'Authorization: Bearer YOUR_AUTH_TOKEN' \
      # --header 'Content-Type: application/json' \
      # --data-raw '{
      #   "startTime": "2023-10-12",
      #   "endTime": "2023-11-01",
      #   "stationId": 60993186,
      #   "timeType": 1
      # }'
      #
      # sample response:
      # {
      #  "code": null,
      # "msg": null,
      # "success": true,
      # "requestId": "825a10932194c335",
      # "total": 283,
      # "stationDataItems": [
      #     {
      #         "generationPower": 0.0,
      #         "usePower": 1103.0,
      #         "gridPower": null,
      #         "purchasePower": null,
      #         "wirePower": 0.0,
      #         "chargePower": null,
      #         "dischargePower": 1361.0,
      #         "batteryPower": 1361.0,
      #         "batterySoc": 89.0,
      #         "irradiateIntensity": null,
      #         "generationValue": null,
      #         "generationRatio": 0.0,
      #         "gridRatio": null,
      #         "chargeRatio": null,
      #         "useValue": null,
      #         "useRatio": null,
      #         "buyRatio": null,
      #         "useDischargeRatio": null,
      #         "gridValue": null,
      #         "buyValue": null,
      #         "chargeValue": null,
      #         "dischargeValue": null,
      #         "fullPowerHours": null,
      #         "irradiate": null,
      #         "theoreticalGeneration": null,
      #         "pr": null,
      #         "cpr": null,
      #         "dateTime": 1697061600.000000000,
      #         "year": 2023,
      #         "month": 10,
      #         "day": 12
      #     },
      #   ]
      # }
      #
      # we also need to load the specific consumption data for the site and day of the month - this is not included in the frame stats
      # that is time_type = 2
      #

      def historic_stats(station_id, auth_data, start_day, time_type = 1)
        station_data = []

        endpoint = "#{URL}station/v1.0/history?language=en"

        # if start_time is longer ago than just one day - call for each day - this one is needed for the detailed request (timeType==1)
        while start_day <= Time.zone.yesterday
          log "Loading stats for #{start_day}"
          response = HTTParty.post(endpoint,
                                   headers: { "Content-Type" => "application/json",
                                              "Authorization" => "Bearer #{auth_data[:access_token]}" },
                                   body: { endTime: Time.zone.yesterday.to_s,
                                           startTime: start_day,
                                           stationId: station_id,
                                           timeType: time_type }.to_json)

          return nil unless response && response["success"]

          station_data += response["stationDataItems"]
          break if time_type == 2 # we get all data for the day in one call

          start_day += 1.day
        end
        log "Found #{station_data.length} log entries for this site."
        station_data
      end

      def clean_inst(plants)
        cleaner = Cleaner.new source.organization
        plants.each do |inst|
          cleaner.clean_key inst, "contactPhone", :phone_number, :all
          cleaner.clean_key inst, "ownerName", :text, :all
        end
        plants
      end
    end
  end
end
