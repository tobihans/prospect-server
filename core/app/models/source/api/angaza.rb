# frozen_string_literal: true

class Source
  module Api
    class Angaza
      include Source::Adapter::Api

      SECRET_REQUIRED = true
      REQUIRED_DETAILS_KEYS = %w[username].freeze
      PULL = true
      URL = "https://payg.angazadesign.com/data/"
      DEFAULT_FREQUENCY = "daily"

      PAGE_SIZE = 100
      MAX_RETRIES = 3
      MAX_ENTRIES_PER_ENDPOINT = 20_000

      # users attributes are not in the client attributes list and are treated seperatly here
      USERS_ATTRIBUTE_WHITELIST = ["AB010206"].freeze # physical address

      ENDPOINTS = {
        "payments" => {
          last_import_hint_key: "effective_date",
          clean: lambda { |data, cleaner|
            data["_embedded"]["item"].each do |payment|
              cleaner.clean_key payment, "msisdn", :phone_number, :all
            end
          }
        },
        "accounts" => {
          last_import_hint_key: "registration_date"
        },
        "clients" => { last_import_hint_key: "qid",
                       clean: lambda { |data, cleaner|
                                data["_embedded"]["item"].each do |client|
                                  cleaner.clean_key client, "name", :text, :all
                                  cleaner.clean_key client, "primary_phone", :phone_number, :all
                                  client["attribute_values"].each do |attribute|
                                    attribute["value"] = attribute["value"].to_s if attribute["type"] == "custom_anonymize"
                                    cleaner.clean_key attribute, "value", :text, :all if attribute["type"] == "custom_anonymize"
                                    cleaner.clean_key attribute, "value", :text, :all if private_attributes.include? attribute["name"]
                                  end
                                end
                              } },
        "replacements" => { last_import_hint_key: "created_when" },
        "users" => { # last_import_hint_key: "", # no apparent order of results
          clean: lambda { |data, cleaner|
                   data["_embedded"]["item"].each do |user|
                     cleaner.clean_key user, "first_name", :text, :all
                     cleaner.clean_key user, "last_name", :text, :all
                     cleaner.clean_key user, "username", :text, :all
                     cleaner.clean_key user, "primary_phone", :phone_number, :all
                     # all whitelisted attributes will be P14n
                     user["attribute_values"].each do |attribute|
                       cleaner.clean_key attribute, "value", :text, :all
                     end
                   end
                 }
        },
        "pricing_groups" => {}
      }.freeze

      OPTIONS = %w[custom custom_anonymize ignore].freeze

      def capabilities
        {
          backfill_completely: true
        }
      end

      def adapter_constraints
        mapping = source.details["attribute_mapping"]
        return if mapping.blank?

        # migrate old naming: 'anonymize' -> 'custom_anonymize'
        # this can be removed after all angaza instances got updated ...
        mapping.each { |k, v| mapping[k] = "custom_anonymize" if v == "anonymize" }

        attributes = (mapping.values - OPTIONS).tally # {"name"=>2, "phone"=>1}

        attributes.select { |_attribute, occurences| occurences > 1 }.each_key do |attribute|
          source.errors.add "attribute_mapping", "attribute #{attribute} is used more than once"
        end
      end

      def custom_attributes
        log "Requesting pricing_groups."
        response = get_request("pricing_groups") # client_attributes
        pricing_groups = JSON.parse response.body
        pricing_groups = pricing_groups["_embedded"]["item"]
        log "Found #{pricing_groups.size} pricing_groups. Getting attributes for all groups."

        attributes = []
        pricing_groups.each do |pg|
          response = get_request("client_attributes", 0, { group_qid: pg["qid"] })
          attributes_for_group = JSON.parse response.body
          attributes_for_group = attributes_for_group["_embedded"]["item"]
          attributes.concat(attributes_for_group)
        end

        log "Found #{attributes.size} attributes."

        remove_photo_links_from_attributes(attributes)

        attribute_names = attributes.pluck("name").uniq
        log "Using #{attribute_names.size} unique and filtered attributes."
        attribute_names
      end

      #
      # returns list of column names the api data can be mapped to
      #
      def self.table_attributes
        @@table_attributes ||= load_table_attributes[0] # rubocop:disable Style/ClassVars
      end

      #
      # returns a subset of column names that need to be p14n
      #
      def self.private_attributes
        @@private_attributes ||= load_table_attributes[1] # rubocop:disable Style/ClassVars
      end

      #
      # Those attributes are displayed to the user for mapping
      #
      def self.ui_attributes
        OPTIONS + table_attributes
      end

      def self.load_table_attributes
        dt = Postgres::DataTable.find("shs")
        columns = []
        privacy_columns = []
        dt.columns.each do |c|
          next if c.exclude_from_ingress?

          columns << c.name
          privacy_columns << c.name if c.pseudonymized
        end

        [columns.sort, privacy_columns.sort]
      end

      def configure_step?
        true
      end

      def data_origin
        "angaza"
      end

      def data_categories
        ["payments_ts"]
      end

      def validation_hint
        ""
      end

      def validation_result
        return Result.new format: :json, data: nil, error: "No credentials provided", protocol: nil, hint: nil, more_data: nil if details["username"].blank? || source.secret.blank?

        self.validation_mode = true
        retrieve_data nil
      end

      def retrieve_data(since = nil)
        log "Using last import hint: #{since}"

        data, more_data, hint = fetch since

        Result.new format: :json, data:, error:, protocol:,
                   hint: hint.to_json, more_data:
      end

      def fetch(since)
        step "Connecting to Angaza API"

        begin
          responses, loop_status = get_responses since
        rescue => e
          Rails.logger.info e
          fail! e.message
          return
        end

        ENDPOINTS.each_key do |endpoint|
          log "Found #{responses[endpoint]['_embedded']['item'].size} #{endpoint}"
        end

        remove_photo_links_from_response(responses)
        filter_users_attributes(responses)
        apply_attributes_mapping(responses)

        step "Encrypt and Pseudonymize data"
        Rails.logger.info "Angaza start cleaning"

        cleaner = Cleaner.new(source.organization)
        ENDPOINTS.each do |endpoint, config|
          Rails.logger.info "Angaza cleaning #{endpoint}"
          config[:clean]&.call(responses[endpoint], cleaner)
        end

        # build next last import hint
        last_import_hint = ImportHint.new(since, ENDPOINTS)
        next_hint = last_import_hint.generate_next_hint(responses, loop_status, ENDPOINTS)
        next_more_data = last_import_hint.initial_backfill? # we have more data to fetch until we switch to normal mode

        [responses, next_more_data, next_hint]
      end

      def get_responses(since)
        last_import_hint = ImportHint.new(since, ENDPOINTS)

        responses = {}
        loop_status = {}

        ENDPOINTS.each do |endpoint, config|
          log "Requesting from endpoint #{endpoint} after #{last_import_hint.endpoints[endpoint]}."

          # we only use items from response and throw away the rest
          # but keep the structure of an real API response
          responses[endpoint] = { "_embedded" => { "item" => [] } }

          # we need to record the offset and the reason for exiting the loop
          loop_status[endpoint] = {
            offset: last_import_hint.status == ImportHint::NORMAL ? 0 : last_import_hint.endpoints[endpoint],
            hit_exit_condition: false # we need to remember the break reason
          }
          loop do
            Rails.logger.info { "Angaza endpoint #{endpoint}, offset: #{loop_status[endpoint][:offset]}." }

            response = get_request(endpoint, loop_status[endpoint][:offset])

            # handle blank or empty response
            break if response.blank?

            parsed = JSON.parse(response.body)
            break if parsed.empty? || parsed["_embedded"].nil? # seems like response is {} when trying to get more items than available

            # we have items ...
            new_items = parsed["_embedded"]["item"]

            responses[endpoint]["_embedded"]["item"].concat(new_items)

            # validation stops on first page. if items are empty we stop as well
            break if validation_mode || new_items.empty?

            # Exit condition NORMAL mode
            # check and stop if we hit an item we imported already in last import
            hint_key = config[:last_import_hint_key]

            if last_import_hint.status == ImportHint::NORMAL && hint_key

              hint_value = last_import_hint.endpoints[endpoint]
              new_items_last_value = new_items.last[hint_key]

              if hint_value.present? && new_items_last_value <= hint_value
                log "Found item with #{hint_key} #{new_items_last_value} which is before or at last import hint #{hint_value}. Stopping."
                loop_status[endpoint][:hit_exit_condition] = true
                break
              end
            end

            # Exit condition INITIAL_BACKFILL mode
            # check maximum per import
            if last_import_hint.status == ImportHint::INITIAL_BACKFILL && (loop_status[endpoint][:offset] - last_import_hint.endpoints[endpoint] >= MAX_ENTRIES_PER_ENDPOINT - PAGE_SIZE)
              log "Reached max entries per endpoint. Stopping."
              loop_status[endpoint][:hit_exit_condition] = true
              break
            end
            loop_status[endpoint][:offset] += PAGE_SIZE
          end

          log "Last offset: #{loop_status[endpoint][:offset]}, page size is #{PAGE_SIZE}. Hit exit condition: #{loop_status[endpoint][:hit_exit_condition]}"
        end

        [responses, loop_status]
      end

      def get_request(endpoint, endpoint_offset = 0, params = {})
        credentials = Base64.strict_encode64("#{details['username']}:#{source.secret}")

        retries = 0
        begin
          response = HTTParty.get(
            URL + endpoint,
            headers: { "Content-Type" => "application/json", "Authorization" => "Basic #{credentials}" },
            query: { "offset" => endpoint_offset }.merge!(params)
          )
          # handle wrong credentials
          if response.code == 401
            fail! "wrong credentials provided"
            return
          end

          # handle gateway error 502, we assume that their db struggles to get historic data, so we give extra time
          if response.code == 502
            sleep 30
            raise "Gateway eRails.logger.inforror 502, slept 30s" # use retry logic
          end
        rescue => e
          Rails.logger.debug e
          if retries < MAX_RETRIES
            retries += 1
            Rails.logger.info "Going for retry #{retries}, because of: #{e.message}"
            sleep 2**retries unless Rails.env.test?
            retry
          else
            log "Failed after #{MAX_RETRIES} retries. Error: #{e.message}"
            raise e # rethrow e to fail import and throw away partial data
          end
        end

        response
      end

      def remove_photo_links_from_response(responses)
        responses.dig("clients", "_embedded", "item")&.each do |item|
          remove_photo_links_from_attributes(item["attribute_values"])
        end
        responses
      end

      def filter_users_attributes(responses)
        step "Filtering users attributes"

        responses.dig("users", "_embedded", "item")&.each do |item|
          item["attribute_values"].delete_if { |a| USERS_ATTRIBUTE_WHITELIST.exclude?(a["attribute_qid"]) }
        end

        responses
      end

      #
      # deletes foto attributes from given array of attributes
      #
      def remove_photo_links_from_attributes(attributes)
        # attributes with links to photos have type=PHOTO or type=GENERIC_PHOTO
        attributes.delete_if { |a| a["type"].include?("PHOTO") }
      end

      def apply_attributes_mapping(responses)
        mapping = source.details["attribute_mapping"]
        return responses unless mapping

        step "Applying attribute mapping"
        responses.dig("clients", "_embedded", "item")&.each do |item|
          # remove attributes marked as 'ignore'
          item["attribute_values"].delete_if { |a| mapping[a["name"]] == "ignore" }
          item["attribute_values"].each do |attribute|
            if OPTIONS.include? mapping[attribute["name"]] # don't overwrite custom or anonymize
              attribute["type"] = mapping[attribute["name"]]
            elsif mapping.keys.include? attribute["name"]
              attribute["name"] = mapping[attribute["name"]]
            end
          end
        end

        responses
      end
    end
  end
end
