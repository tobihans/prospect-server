# frozen_string_literal: true

class Source
  module Api
    class Supamoto
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[username].freeze
      SECRET_REQUIRED = true
      PULL = true
      DEFAULT_FREQUENCY = "daily"

      URL = "https://api.supamoto.app/api/v2/"
      ENDPOINTS = {
        stoves: "stoves",             # https://supamoto.readme.io/reference/get-stoves-page
        orders: "stoves/purchases",   # https://supamoto.readme.io/reference/get-purchase-orders
        payments: "stoves/payments"   # https://supamoto.readme.io/reference/get-payments
      }.freeze

      DEVICE_BATCH_SIZE = 50 # too much and we get a 500
      PAGE_SIZE = 300
      HTTP_OPTS = { open_timeout: 120, read_timeout: 120, ssl_timeout: 120, use_ssl: true }.freeze

      DEFAULT_BACKFILL = Date.parse("2023-08-01") # fresh connector will fetch from there on (takes long)
      OVERLAP = 2.days # will overlap old imports to catch maybe missed things
      DELAY = 2.days # never fetch data fresher 2 days old as payments might still shift

      def data_origin
        "supamoto"
      end

      def data_categories
        ["payments_ts"]
      end

      def validation_hint
        ""
      end

      def validation_result
        self.validation_mode = true
        retrieve_data
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_s
      end

      def retrieve_data(since = nil)
        data, more_data, hint = fetch(since)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      def fetch(since)
        from, to = from_to(since)
        fail! "Nothing to fetch in this date range: #{from} .. #{to}" and return if from >= to

        data = {}

        step "Fetching Stoves"
        data[:stoves] = fetch_stoves
        return if error # first request might kill it with unauthorized or something

        stove_ids = data[:stoves].pluck "deviceId"

        %i[orders payments].each do |endpoint|
          step "Fetching #{endpoint.to_s.titleize}"
          data[endpoint] = fetch_stove_data endpoint, stove_ids, from, to
        end

        [data, false, to.to_s]
      rescue => e
        fail! "Failed with error: #{e.message}"
        raise unless validation_mode
      end

      def fetch_stoves
        stoves = paginated do |page_query|
          uri = URI "#{URL}#{ENDPOINTS[:stoves]}?#{page_query}"

          Net::HTTP.start(uri.host, uri.port, **HTTP_OPTS) do |http|
            request = Net::HTTP::Get.new uri, headers.stringify_keys

            response = http.request request
            JSON.parse(response.read_body)["content"]
          end
        end

        clean_stoves stoves
      end

      def clean_stoves(stoves)
        cleaner = Cleaner.new source.organization
        stoves.each do |h|
          cleaner.clean_key h, %w[latitude longitude], :geo, :all
          cleaner.clean_key h["customer"], "firstName", :text, :all if h.dig("customer", "firstName") # never got that in API response but it exists in API doc.
        end
        stoves
      end

      def fetch_stove_data(endpoint, stove_ids, from, to)
        batches = stove_ids.in_groups_of DEVICE_BATCH_SIZE, false
        batches = batches.first(3) if validation_mode

        batches.map.with_index do |batch, i|
          log "Batch #{i}"
          body = { deviceIds: batch }.to_json

          paginated do |page_query|
            uri = URI "#{URL}#{ENDPOINTS[endpoint]}?#{page_query}&startDate=#{from}&endDate=#{to}"

            Net::HTTP.start(uri.host, uri.port, **HTTP_OPTS) do |http|
              request = Net::HTTP::Post.new uri, headers.stringify_keys
              request.body = body
              request.content_type = "application/json"

              response = http.request request
              JSON.parse(response.read_body)["content"]
            end
          end
        end.inject(:+)
      end

      def from_to(since)
        from = if since.nil?
                 DEFAULT_BACKFILL
               else
                 Date.parse(since) - OVERLAP
               end
        to = Time.zone.today - DELAY # give them time to reconcile
        [from, to]
      end

      def basic_auth
        Base64.encode64 "#{details['username']}:#{source.secret}"
      end

      def headers
        @headers ||= {
          Authorization: "Basic #{basic_auth}",
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      end

      def paginated
        data = []
        page = 0
        another_page = true

        while another_page
          log "Page #{page} (#{PAGE_SIZE})"
          page_query = "page=#{page}&pageSize=#{PAGE_SIZE}"

          page_data = yield page_query

          data += page_data

          page += 1
          another_page = validation_mode ? (page < 2 && page_data.present?) : page_data.present?
        end
        data
      end
    end
  end
end
