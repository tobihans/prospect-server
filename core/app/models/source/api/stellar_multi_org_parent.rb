# frozen_string_literal: true

class Source
  module Api
    #
    # Uses Stellar connector to query the API and then splits the data into seperate imports for each StellarMultiOrgChild source it finds.
    #
    class StellarMultiOrgParent < Stellar
      #
      # Core stuff
      ##

      #
      # This value should not be used for a parent connector, and if used it will not find a dataworker with the name api/stellar_multi_org_parent
      #
      def data_schema
        "api/stellar_multi_org_parent"
      end

      def data_origin
        "stellar_multi_org_parent"
      end

      #
      # recives data for all organizations. generates imports for every active a2ei meter connector.
      # The framework expects data returned from this method. We use a dummy value.
      #
      def retrieve_data(since = nil, test_connection = nil)
        data, more_data, hint = fetch(since, test_connection)

        if data
          create_imports data
        else
          fail! "No imports generated, no data received."
        end

        # we return nil data, so that no import file is generated for this connector
        Result.new format: :txt, data: nil, error:, protocol:, more_data:, hint:
      end

      #
      # creates the actuall imports, here the real data gets written to the datalake
      #
      def create_imports(data)
        step "Load child sources and map organisation to meter ids"
        sources = Source.active.where(kind: "api/stellar_multi_org_child")
        log "Found #{sources.size} active child sources."

        data = JSON.parse(data)

        sources.each do |s|
          meter_ids = OrgDeviceMapper.hop_meter.where(organization: s.organization).pluck(:device_id)
          log "Source id #{s.id}: Search for org id #{s.organization.id}, found #{meter_ids}."
          filtered_data = data["data"].select do |site|
            meter_ids.include? site["meterId"].to_i
          end
          log "Matched with #{filtered_data.size} sites."

          s.trigger_import!({ "data" => filtered_data })
        end
      end
    end
  end
end
