# frozen_string_literal: true

class Source
  module Api
    class PaygOps
      include Source::Adapter::Api

      REQUIRED_DETAILS_KEYS = %i[username url].freeze
      SECRET_REQUIRED = true
      PULL = true
      DEFAULT_FREQUENCY = "daily"

      #
      # Core stuff
      #

      def configure_step?
        true
      end

      def adapter_constraints
        # remove empty element in test_tags, which will be send by default
        @source.details["test_tags"]&.delete("")
      end

      def data_origin
        "payg_ops"
      end

      def data_categories
        ["payments_ts"]
      end

      def validation_hint
        I18n.t("payg_ops.permission")
      end

      def validation_result
        retrieve_data(nil, test: true)
      end

      def capabilities
        {
          backfill_completely: false,
          backfill_from_date: true
        }
      end

      def backfill_from_date_import_hint(from_date)
        from_date.to_i.to_s
      end

      # returns the base url and api url
      # base url is needed for a temporary workaround
      # e.g. ["https://instance.paygops.com", "https://instance.paygops.com/api/v1/"]
      def prepare_url(url)
        url = url[0..-2] if url.end_with?("/")
        [url, "#{url}/api/v1/"]
      end

      def retrieve_data(since = nil, test: false)
        data, more_data, hint = fetch(since, test)

        Result.new format: :json, data:, error:, protocol:, more_data:, hint:
      end

      #
      # API specific stuff
      #

      # endpoints we want to query and bool flag for datetime parameter
      # all listed endpoints support pagination but not all support datetime filters
      # p14n functions to pseudonymize the data for the endpoint
      # needed permission, checked on source creation
      ENDPOINTS = { "contracts" => {
                      optional_params: {
                        # "undocumented" parameter to include more information on the contracts
                        # parameter was only listed on the direct contract query endpoint
                        # but seems to work on the list endpoint as well
                        special_data: true
                      },
                      permissions: [:ViewClients]
                    },
                    "clients" => {
                      clean: lambda { |data, cleaner|
                        data.map do |obj|
                          # combine name and surname
                          obj["name"] = "#{obj['name']} #{obj['surname']}"
                          obj.delete "surname"

                          # encrypt the fields
                          %w[name].each do |key|
                            cleaner.clean_key(obj, key, :text, :all)
                          end
                          cleaner.clean_key(obj, "preferred_phone_number", :phone_number, :all)
                          obj["phone_numbers"].map! { |n| RsaOaepEncrypt.e(n, cleaner.organization) }
                          cleaner.clean_key(obj, %w[gps_latitude gps_longitude], :geo, :all)
                          obj
                        end
                      },
                      permissions: [:ViewClients]
                    },
                    "contract_repayments" => {
                      from_date: true,
                      permissions: [:ViewClients]
                    },
                    "contract_events" => {
                      from_date: true,
                      permissions: [:ViewClients]
                    },
                    "payments" => {
                      from_date: true,
                      clean: lambda { |data, cleaner|
                        data.map do |obj|
                          cleaner.clean_key(obj, "wallet_name", :text, :all)
                        end
                      },
                      permissions: [:ViewPayments]
                    },
                    "payment_reconciliations" => {
                      permissions: [:ViewPayments]
                    },
                    "offers" => {
                      permissions: [:ViewOffers]
                    },
                    "lead_generators" => {
                      clean: lambda { |data, cleaner|
                        data.map do |obj|
                          # combine name and surname
                          obj["name"] = "#{obj['name']} #{obj['surname']}"
                          obj.delete "surname"

                          %w[name].each do |key|
                            cleaner.clean_key(obj, key, :text, :all)
                          end
                          cleaner.clean_key(obj, "preferred_phone_number", :phone_number, :all)
                          obj["phone_numbers"].map! { |pn| RsaOaepEncrypt.e(pn, cleaner.organization) }
                          obj
                        end
                      },
                      permissions: %i[ViewLeadGenerators ViewOwnLeadGenerators]
                    },
                    "leads" => {
                      clean: lambda { |data, cleaner|
                        data.map do |obj|
                          # combine name and surname
                          obj["name"] = "#{obj['name']} #{obj['surname']}"
                          obj.delete "surname"

                          %w[name commission_note].each do |key|
                            cleaner.clean_key(obj, key, :text, :all)
                          end
                          cleaner.clean_key(obj, "preferred_phone_number", :phone_number, :all)
                          cleaner.clean_key(obj, %w[gps_latitude gps_longitude], :geo, :all)
                          obj["phone_numbers"].map! { |pn| RsaOaepEncrypt.e(pn, cleaner.organization) }
                          obj
                        end
                      },
                      permissions: %i[ViewLeads]
                    },
                    "tags" => {
                      permissions: %i[]
                    },
                    # single endpoints we get specific informations from
                    "settings/CurrencyISOName" => {
                      skip_pagination: true,
                      permissions: [:ConfigureGeneralSettingsAdmin]
                    },
                    "settings/CountryName" => {
                      skip_pagination: true,
                      permissions: [:ConfigureGeneralSettingsAdmin]
                    } }.freeze

      # see https://demo.paygops.com/admin/api_docs
      def fetch(since, test)
        base_url, url = prepare_url(details["url"])
        # we need an api key before we can use the API
        # as they have a default expiration of 3600 seconds
        # we just generate a new one for each import
        # that way the user is also not required to create a new api key
        # if the current one expires
        api_key = generate_api_key(details["username"], source.secret, url)

        return nil, false, since if api_key.nil?

        # stores all requests in a single object, all endpoints as keys with empty array
        obj = Source::Api::PaygOps::ENDPOINTS.keys.index_with { [] }

        # add custom config information for the transformer
        obj["test_tags"] = source.details["test_tags"]

        # TEMPORARY workaround
        obj, to = request_contract_payments_csv(obj, base_url)
        from = Time.zone.at(since.to_i).rfc3339

        log I18n.t("payg_ops.invalid_data_range", since: Time.zone.at(since.to_i), to:) and return [nil, false, since.to_i] if to < from

        # default params
        default_params = { "page" => 1, "include_objects" => "true" }

        # nil.to_i => 0 => 1970-01-01T00:00:00Z
        date_params = { "from_date" => from.to_s, "to_date" => to.to_s.split("+").first }

        # set organization for encryption
        cleaner = Cleaner.new(source.organization)

        # we loop through all endpoints to geather all the data
        ENDPOINTS.each do |(endpoint, ops)|
          step I18n.t("payg_ops.request.log", endpoint:)

          params = default_params.dup

          if ops[:from_date]
            params.merge! date_params
            log I18n.t("payg_ops.request.since_to", since: Time.zone.at(since.to_i), to:)
          end

          params.merge! ops[:optional_params] if ops[:optional_params]

          # we continue with the requests and increase the page until we hit an empty response
          loop do
            log I18n.t("payg_ops.request.page", page: params["page"])
            response = get_request("#{url}#{endpoint}?#{URI.encode_www_form(params)}", auth: api_key, permissions: ops[:permissions])

            break if response&.body.nil?

            data = JSON.parse(response.body)

            break if data.empty?

            # for endpoint without pagination or
            # testing all endpoints if user permissions are correct
            if ops[:skip_pagination] || test
              obj[endpoint] = data
              break
            else
              # append data of current page to overall set
              obj[endpoint] += data
            end

            # increase page counter for next request
            params["page"] += 1
          end

          # clean endpoint data (enc + p14n)
          ops[:clean]&.call(obj[endpoint], cleaner)
          # encrypt endpoint data
          # ops[:enc]&.call(obj[endpoint])

          # pseudonymize endpoint data
          # ops[:p14n]&.call(obj[endpoint])
        end

        unless obj["tags"].empty?
          # we could be in a connection test or normal import here
          # either way I'll just get a new database reference to our source
          # and update it directly to make sure the test tags are present
          # in the wizzard or after each run of the import
          s = Source.find((@source || source).id)
          s.update_attribute! :details, s.details.merge("tags" => obj["tags"].pluck("name"))
        end

        [obj, false, to.to_i]
      rescue => e
        fail! e
      end

      def generate_api_key(user, password, url)
        step I18n.t("payg_ops.api_key.generate")

        url = "#{url}api_key/request"
        response = post_request(url, { password:, username: user })

        fail! I18n.t("payg_ops.api_key.failed", permission: :CreateAPITokenAdmin) and return if response&.body.nil?

        log I18n.t("payg_ops.api_key.success")

        JSON.parse(response.body)["api_key"]
      end

      def auth_headers(auth)
        auth.nil? ? {} : { "Authorization" => "Bearer #{auth}" }
      end

      def post_request(url, data, json: true)
        response = HTTParty.post(
          url,
          body: json ? data.to_json : data,
          headers: json ? { "Content-Type" => "application/json" } : {}
        )

        fail! I18n.t("payg_ops.api_fail", code: response.code, message: response.message, permissions: "-") and return unless response.code in 200..299

        response
      end

      def get_request(url, auth: nil, headers: {}, permissions: [], json: true)
        headers = headers.merge({ "Content-Type" => "application/json" }) if json
        response = HTTParty.get(
          url,
          headers: auth_headers(auth).merge(headers)
        )

        fail! I18n.t("payg_ops.api_fail", code: response.code, message: response.message, permissions: permissions.join(", ")) and return unless response.code in 200..299

        response
      end

      # TEMPORARY workaround: get csv exports for repayment data for credit value of each repayment
      def request_contract_payments_csv(obj, base_url)
        response = post_request("#{base_url}/login/", { username: details["username"], password: Digest::SHA256.hexdigest(source.secret), action: "" }, json: false)
        export_page = get_request("#{base_url}/admin/data_exports", headers: { Cookie: response.headers["set-cookie"] }, json: false)

        result = export_page.body.match %r{(file/exports/repayment_data_static.*)"}
        raise "Could not find repayment data csv link" if result.captures.empty?

        payment_export_csv = get_request("#{base_url}/#{result[1]}", headers: { Cookie: response.headers["set-cookie"] }, json: false)

        obj["payment_export"] = {}

        CSV.parse(payment_export_csv.body, headers: true).map(&:to_h).each do |row|
          obj["payment_export"][row["Id"]] = row
        end

        # latest date in the csv export, we only pull data til that time
        to = DateTime.parse(obj["payment_export"][obj["payment_export"].keys.map(&:to_i).max.to_s]["Payment Date"])
        [obj, to]
      end
    end
  end
end
