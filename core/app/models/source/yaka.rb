# frozen_string_literal: true

class Source
  #
  # Import data from Google Drive in yaka format. Applies p14n and saves result as json for further processing by dataworker
  #
  class Yaka < GoogleDrive
    def data_schema
      "yaka"
    end

    def data_origin
      "yaka"
    end

    # we just write to datalake, no ingestion here
    def retrieve_data(since = nil)
      data, more_data, hint = fetch(since)

      Result.new format: :json, data:, more_data:, hint:, protocol:, error:
    end

    def fetch(since)
      data, more_data, hint = super

      return data, more_data, hint unless data

      parsed_data = []
      # we actually have data
      # let's parse csv and apply p14n
      CSV.parse(data, headers: true).map(&:to_h).each do |row|
        cleaner.clean_nested_key(row, "Customer Name", :text, :all)
        parsed_data << row
      end

      fail! "Didn't find any data rows in file. Is the file in CSV format?" and return [nil, false, since] if parsed_data.empty?

      [{ "data" => parsed_data }, more_data, hint]
    end

    def cleaner
      @cleaner ||= Cleaner.new(source.organization)
    end
  end
end
