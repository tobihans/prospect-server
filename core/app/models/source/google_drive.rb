# frozen_string_literal: true

require "google/apis/drive_v3"
require "googleauth"

class Source
  #
  # Accesses Google Drive folders and downloads files in lexicographical order
  #
  # Don't reuse instance after a change of credentials.
  #
  class GoogleDrive
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[folder_id file_format frequency].freeze
    SECRET_REQUIRED = false
    PULL = true

    def validation_hint
      "Please put an example file into your Google Drive folder, so you can test the settings."
    end

    def validation_result
      retrieve_data
    end

    def capabilities
      {
        backfill_completely: true,
        backfill_from_date: true
      }
    end

    def backfill_from_date_import_hint(from_date)
      # import_hint format is "Date;filename", '!' is kind of the first filename possible, so we import all files from the given date.
      "#{from_date};!"
    end

    def adapter_constraints
      # only verify if the folder_id is already filled in. Could be the user is editing a source or be in the process of filling in all the steps in the wizard
      #
      # For security reasons we only allow one Source per folder_id. So folder_id hijacking should be minimized. The security risk arises from the fact
      # that we use one Service Account for all Google Drive sources.
      return if details["folder_id"].blank?

      # Check all other google drive sources and see if any of them has the
      return if Source.active.where(kind: source.kind).where.not(id: source.id).none? { |s| s.details["folder_id"] == details["folder_id"] }

      # NOTE: not specifying the other source's name here, because this could leak information between different users...
      source.errors.add "folder_id", "The given folder_id is already used by another Source. It can only be used once. Deactivate the other Source or use another folder here."
    end

    #
    # since - is a combination of last modifiedTime and filename, divided by a semicolon
    #
    def fetch(since)
      step "Checking Google Drive connection"

      drive_service # init service object

      folder_id = details["folder_id"]

      fail! "No read permission for folder_id. Make sure you share the folder with our Service account" unless check_access_to_folder folder_id

      step "Checking for new files #{since ? " after #{since}" : ''} in folder with id #{folder_id}"
      files = find_files_wrapped since

      log "Found #{files.count} new files"

      if files.count.positive?
        current_file = files.first

        step "Downloading #{current_file.name}"
        data = StringIO.new
        download_file current_file.id, data
        more_data = files.length > 1

        [data.string, more_data, "#{current_file.modified_time};#{current_file.name}"]
      else
        [nil, false, since]
      end
    rescue => e
      fail! e and return [nil, false, since]
    end

    #
    # returns nil, in case google credential file can't be read
    #
    def self.service_account_email
      @service_account_email ||= begin
        file = File.read(ENV.fetch("GOOGLE_DRIVE_CREDENTIAL_FILE"))
        JSON.parse(file)["client_email"]
      end
    rescue => e
      Rails.logger.error "Can't read Google Drive service account email, needs setup on this instance? Error: #{e.message}"
      nil
    end

    def drive_service
      @drive_service ||= begin
        credentials = Google::Auth::ServiceAccountCredentials.make_creds(
          json_key_io: File.open(ENV.fetch("GOOGLE_DRIVE_CREDENTIAL_FILE", "r")),
          scope: Google::Apis::DriveV3::AUTH_DRIVE_READONLY
        )
        Google::Apis::DriveV3::DriveService.new.tap do |service|
          service.authorization = credentials
        end
      end
    end

    def check_access_to_folder(folder_id)
      permissions = drive_service.list_permissions(folder_id)
      permissions.permissions.any? do |permission|
        permission.role.in? %w[reader writer owner]
      end
    rescue => e
      # the api throws an exception in case the folder is not shared with the Service Account
      # the error message is meaningless for the user, so we improve it here
      raise "Error checking permissions for folder_id: '#{e.message}'. This could indicate that you haven't shared the folder with the Service Account."
    end

    def find_files_wrapped(since)
      if since.present?
        start_after, filename = since.split(";", 2) # just split on first occurence of ;
        start_after = Time.zone.parse(start_after)
        raise "Error in since field, filename can't be empty." if filename.blank?

        files = find_latest_files(start_after)
        log "Found #{files.count} files with modifiedTime >= #{start_after}, checking for imported files..."

        # Filter out files we already imported (same timestamp, and filename 'before' imported one)
        #
        # This is based on ordering the files by name, when they have the same modifiedTime.
        # - GDrive allows for files with the same name in the same folder 😿, we assume here that this is bad practice and nobody does this actually...
        # - We check for equality with seconds, thats why we use to_i
        files.select { |f| f.modified_time.to_time > start_after || (f.name > filename && f.modified_time.to_i == start_after.to_i) }

      else
        find_latest_files
      end
    end

    #
    # returns files ordered by modifiedTime after or at a given time (start_after)
    #
    def find_latest_files(start_after = nil)
      query = "mimeType != 'application/vnd.google-apps.folder' and trashed = false and parents = '#{details['folder_id']}'"
      query += " and modifiedTime >= '#{start_after.rfc3339}'" if start_after
      drive_service.list_files(q: query, fields: "files(name, id, modifiedTime)", order_by: "modifiedTime,name").files
    end

    #
    # :response_target (String, IO) — Where to write response data, file path, or IO object.
    # e.g. to write to a temp file, specify the file's path as string.
    # To get a StringIO don't specify response_target and access with resp.body.read
    #
    def download_file(file_id, response_target = nil)
      drive_service.get_file(file_id, download_dest: response_target)
    end
  end
end
