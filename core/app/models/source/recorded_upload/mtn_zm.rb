# frozen_string_literal: true

class Source
  module RecordedUpload
    class MtnZm < Base
      def data_origin
        "mtn_zm"
      end

      def data_schema
        "mtn_zm_payments_statement"
      end

      def download_url
        "https://www.mtnmobilemoney.co.zm/partner/"
      end

      def download_hint
        "Payments Statement"
      end

      def parse_to_hashes(raw_data)
        step "Parsing CSV"

        csv = CSV.parse raw_data, headers: true
        csv.map(&:to_h)
      end

      def clean(data)
        step "Encrypt and Pseudonymize data"
        cleaner = Cleaner.new(source.organization)
        data.each do |d|
          cleaner.clean_key d, "From", :text, :all
          cleaner.clean_key d, "From Name", :text, :all
          cleaner.clean_key d, "From Handler Name", :text, :all
        end
        data
      end

      def identify_hint(data)
        step "Identify Hint for next import"
        t = data.pluck("Date").compact.map { Time.zone.parse _1 }.max.to_s
        log "Latest transaction at #{t}"
        t
      end
    end
  end
end
