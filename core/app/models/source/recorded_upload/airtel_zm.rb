# frozen_string_literal: true

class Source
  module RecordedUpload
    class AirtelZm < Base
      def data_origin
        "airtel_zm"
      end

      def data_schema
        "airtel_zm_customer_transactions"
      end

      def download_url
        "https://airtelmoneyweb.airtel.co.zm/AirtelMoney/"
      end

      def download_hint
        "Customer Transaction Report"
      end

      def parse_to_hashes(raw_data)
        step "Parsing CSV"

        utf8_converter = ->(field) { field&.encode("utf-8", invalid: :replace, undef: :replace, replace: "") }
        raw_data.scrub!
        csv = CSV.parse raw_data, converters: utf8_converter
        data = csv.to_a

        header_idx = data.index { |r| r.first == "Record No" }
        header = data[header_idx]
        data.shift header_idx + 1 # delete beginning including header
        header_len = header.count

        step "Cleaning unusual Reference No"
        ref_idx = header.index "Reference No."

        hash_ready = []

        # handle non-standard rows
        data.each do |d|
          additional_fields = d.length - header_len

          if additional_fields.positive? # comma(s) in the payment reference
            ref_a = d.slice! ref_idx..(ref_idx + additional_fields)
            ref = ref_a.join "_"
            log "Concatenating reference #{ref} in Record No. #{d.first}"
            d.insert ref_idx, ref
          elsif additional_fields == -1 # payment reference completely omitted
            log "Insert empty reference in Record No. #{d.first}"
            d.insert ref_idx, nil
          elsif additional_fields < -1
            Rails.logger.error "Column is to short: #{d.inspect}"
            raise "Column too short!"
          end

          hash_ready << [header, d].transpose
        end

        hash_ready.map(&:to_h)
      end

      def clean(data)
        step "Encrypt and Pseudonymize data"
        cleaner = Cleaner.new(source.organization)
        data.each do |d|
          cleaner.clean_key d, "Payer Bank Account No/Mobile No", :text, :all
          cleaner.clean_key d, "Payer User Name", :text, :all
          cleaner.clean_key d, "Payer Nick Name", :text, :all
          cleaner.clean_key d, "Payer Mobile Number", :phone_number, :all
        end
        data
      end

      def identify_hint(data)
        step "Identify Hint for next import"
        t = data.pluck("Transaction Date & Time").map { Time.zone.parse _1 }.max.to_s
        log "Latest transaction at #{t}"
        t
      end
    end
  end
end
