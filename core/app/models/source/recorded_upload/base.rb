# frozen_string_literal: true

class Source
  module RecordedUpload
    class Base < ManualUpload
      ## Base Class for Recorded Uploads of payments Statements

      REQUIRED_DETAILS_KEYS = %i[email frequency].freeze
      DEFAULT_FREQUENCY = "weekly"

      UPLOAD_CODE_EXPIRY = 5.days
      DELAY = 2.days
      INITIAL_IMPORT_DELAY = 3.months
      OVERLAP = 2.days

      def data_categories
        ["payments_ts"]
      end

      def requires_recording?
        true
      end

      def data_origin
        raise "implement me!"
      end

      def data_schema
        raise "implement me!"
      end

      def download_url
        raise "implement me!"
      end

      def download_hint
        raise "implement me!"
      end

      def from_hint
        if source.last_imported_hint.present?
          t = Time.zone.parse source.last_imported_hint
          t.to_date - OVERLAP
        else
          Time.zone.today - INITIAL_IMPORT_DELAY
        end
      end

      def until_hint
        Time.zone.today - DELAY
      end

      def validation_result
        process_data raw_data
      end

      def notify!
        send_notification next_import_code
      end

      def next_import_code
        info = [source.id, source.imports.last&.id.to_i].join " "
        VerifiedCode.generate info, expires_in: UPLOAD_CODE_EXPIRY
      end

      def send_notification(code)
        SourceMailer.
          with(source:, code:, code_expiry: UPLOAD_CODE_EXPIRY).
          notify_recorded_upload.
          deliver_later
      end

      def process_data(raw_data)
        parsed = parse_to_hashes raw_data
        data = clean parsed
        hint = identify_hint data

        Result.new format: :json, data:, protocol:, error:, hint:, more_data: nil
      end

      def parse_to_hashes(_raw_data)
        raise "implement me!"
      end

      def clean(_data)
        raise "implement me!"
      end

      def identify_hint(_data)
        raise "implement me!"
      end
    end
  end
end
