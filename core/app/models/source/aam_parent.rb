# frozen_string_literal: true

class Source
  #
  # Import data from SKGS V3 distributors in AAM format.
  # The data is stored in S3, so we inherit from S3Bucket connector
  #
  # Don't reuse instance after a change of credentials.
  #
  class AamParent < S3Bucket
    #
    # Core stuff
    ##
    DEFAULT_FREQUENCY = "daily"
    AAM_DEFAULT_STRATEGY = "last_modified"

    def data_origin
      "aam"
    end

    def data_schema
      "aam_v3"
    end

    def data_categories
      ["shs_ts"]
    end

    def validation_hint
      "" # overwrite S3Bucket validation hint
    end

    REQUIRED_DETAILS_KEYS = %i[access_key region bucket_name].freeze # remove some S3 bucket details, and ...

    #
    # recives data for all organizations. generate import if active and matching aam child connector is found.
    # The framework expects data returned from this method. We use a dummy value.
    #
    # won't call S3Bucket.fetch, because we need to get all files and not only the first one
    def retrieve_data(since = nil)
      step "Checking AWS credentials"
      begin
        s3_client
      rescue => e
        fail! e and return Result.new format: :txt, data: nil, error:, protocol:, more_data: false, hint: since
      end

      step "Checking Bucket Access"
      bucket_name = details["bucket_name"]
      acc_err = bucket_access_error bucket_name
      fail! acc_err and return Result.new format: :txt, data: nil, error:, protocol:, more_data: false, hint: since if acc_err

      step "Checking for new files#{since ? " after #{since}" : ''}"

      # explicitly set strategy because AAM UI doesn't (and shouldn't) allow to set it
      object_list = order_by_strategy(bucket_name, since, AAM_DEFAULT_STRATEGY)

      more_data = false # stategy paginates itself, so one run is enough. TODO - make lexicographical also paginate itself.
      hint = object_list.empty? ? since : object_list.last[:next_since]

      log "Found #{object_list.count} objects, filtering..."

      object_list = filter_object_list object_list

      log "After filter: #{object_list.count} objects. If you expect more objects to pass the filter,
      check that the devices are uploaded to the org device mapper and that there is an active AAM Child source configured."

      return Result.new format: :txt, data: nil, error:, protocol:, more_data:, hint: hint unless object_list.count.positive? # no data

      object_list.each do |o|
        current_object_key = o[:key]
        step "Downloading #{current_object_key}"
        resp = download_object bucket_name, current_object_key
        data = resp.body&.string

        create_import data, o[:meta]
      end

      # we return nil data, so that no import file is generated for this connector
      Result.new format: :txt, data: nil, error:, protocol:, more_data:, hint:
    end

    def filter_object_list(object_list)
      size_before = object_list.size

      # match keys of the form: <device_id>/<type>_<file_creation_timestamp>.log
      # device_id and file_creation_timestamp are integers
      # type: a2ei or a2eiday
      object_list.select! do |o|
        o[:key].match? "^[0-9]*/(a2ei|a2eiday)_[0-9]*.log$"
      end

      log "Objects not passing the expected filename pattern: #{size_before - object_list.size}."

      # check if we can find a matching source
      # this means we can parse the folder_name and it corresponds to an active source
      # this is an optimization and avoids downloading files that will not be processed in later steps...

      stats = Hash.new(0) # sets default value to 0
      object_list.select! do |o|
        folder_name = o[:key].split("/", 2).first
        source = source_for_folder_name folder_name
        stats[folder_name] += 1 unless source
        source
      end

      # print stats
      unless stats.empty?
        log "No source found for the following device ids: "
        long_device_log = stats.map do |device_id, counter|
          "#{device_id}(#{counter})"
        end.join(", ")
        log long_device_log # logs get truncated
        Rails.logger.info long_device_log # log full message
      end

      object_list
    end

    #
    # creates the actual import, here the real data gets written to the datalake
    #
    # returns [device_id, source] source is nil, in case no source could be found
    def create_import(data, meta)
      return [nil, nil] if data.blank?

      folder_name = meta.key.split("/", 2).first

      source = source_for_folder_name folder_name
      return [folder_name, nil] if source.nil?

      # prepare meta data for first line
      metadata = {
        last_modified: meta.last_modified.to_s,
        key: meta.key,
        etag: meta.etag
      }.to_json

      complete_data = [metadata, "---", data].join("\n") # add metadata with a separator line

      # generate import for child source
      source.trigger_import!(complete_data)

      [folder_name, source]
    end

    #
    # find source for folder name
    #
    def source_for_folder_name(folder_name)
      begin
        folder_name = Integer(folder_name)
      rescue
        log "Can't parse folder name (#{folder_name}) as meter id"
        return nil
      end

      device = OrgDeviceMapper.aam_v3.find_by(device_id: folder_name)

      unless device
        Rails.logger.debug { "Device #{folder_name} not found in OrgDeviceMapper" }
        return nil
      end

      Source.active.by_organization(device.organization).find_by(kind: "aam_child")
    end
  end
end
