# frozen_string_literal: true

class Source
  #
  # Import data from SKGS V3 distributors in AAM format.
  # The data is stored in S3, so we inherit from S3Bucket connector
  #
  # Don't reuse instance after a change of credentials.
  #
  class Sunking < S3Bucket
    SUNKING_STRATEGY = "lexicographical"

    REQUIRED_DETAILS_KEYS = %i[access_key region bucket_name frequency].freeze

    def data_origin
      "sunking"
    end

    def data_schema
      "sunking"
    end

    def data_categories
      ["shs_ts"]
    end

    # force SUNKING_STRATEGY, typical filename has timestamp in it:
    #   Energy_Explorer_Energy_Explorer_Gsm_Api_Data_2024-06-10T0900_ZDkVrg.csv
    def order_by_strategy(bucket_name, start_after)
      super(bucket_name, start_after, SUNKING_STRATEGY)
    end

    #
    # convert csv to json
    #
    def retrieve_data(since = nil)
      csv, more_data, hint = fetch(since)
      data = csv.blank? ? nil : CSV.parse(csv, headers: true).map(&:to_h)

      Result.new format: :json, data:, more_data:, hint:, protocol:, error:
    end
  end
end
