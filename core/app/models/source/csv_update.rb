# frozen_string_literal: true

## This should not be standard adapter of a source, rather instantiate manually to process data...
#

class Source
  class CsvUpdate
    include Source::Adapter::Generic

    REQUIRED_DETAILS_KEYS = %i[].freeze
    SECRET_REQUIRED = false
    PULL = false

    def manual_upload?
      true
    end

    def validation_hint
      "If you see this, there is an error!"
    end

    def validation_result
      raise "Not implemented"
    end

    def process_data(update_data, data_category:)
      data = ingest update_data, data_category

      data = { data_category => data } if data.present?

      Result.new format: :json, data:, protocol:, error:, hint: nil, more_data: nil
    end

    def ingest(data, data_category)
      log "Manual Update from CSV for data category #{data_category}"

      begin
        if data.blank?
          fail! "No data"
          return
        end

        step "Checking data file format: CSV"
        as_hashes = CSV.parse(data, headers: true, liberal_parsing: true).map(&:to_h)
      rescue => e
        fail! e.message
        return
      end

      step "Checking schema is conforming to: #{source.data_category}"

      data_table = Postgres::DataTable.new data_category
      fail! "This table is not updatable" and return unless data_table.updatable?

      caster = data_table.caster as_hashes, update_mode: true
      fail! caster.errors and return unless caster.valid?

      casted = caster.data

      step "Applying cleaning"
      begin
        data_table.apply_cleaning! casted, source.organization
      rescue => e
        fail! e.message
        return
      end

      step "Checking data presence"

      fail! "No data" and return unless casted.count.positive?

      log "Counted #{casted.count} rows"

      casted
    end
  end
end
