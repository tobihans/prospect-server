# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                                  :bigint           not null, primary key
#  access_count_to_reset_password_page :integer          default(0)
#  active_at                           :datetime
#  admin                               :boolean          default(FALSE), not null
#  archived_at                         :datetime
#  code_of_conduct                     :json
#  crypted_password                    :string
#  data_protection_policy              :json
#  email                               :string           not null
#  invite_code                         :string
#  reset_password_email_sent_at        :datetime
#  reset_password_token                :string
#  reset_password_token_expires_at     :datetime
#  salt                                :string
#  terms_of_use                        :json
#  username                            :string
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  organization_id                     :integer
#
# Indexes
#
#  index_users_on_active_at             (active_at)
#  index_users_on_archived_at           (archived_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_invite_code           (invite_code) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token)
#  index_users_on_username              (username)
#
class User < ApplicationRecord
  include OwningOrganization

  authenticates_with_sorcery!

  MIN_PW_LENGTH = ENV.fetch("MIN_PW_LENGTH", 12).to_i
  STATES = %i[invited active archived].freeze

  encrypts :invite_code, deterministic: true
  normalizes :email, with: ->(email) { User.normalize_email(email) }

  has_many :authentications, dependent: :destroy

  has_many :organization_users, autosave: true, dependent: :destroy, inverse_of: :user
  has_many :invitations, -> { where(activated_at: nil) }, dependent: nil, class_name: "OrganizationUser", inverse_of: false
  has_many :active_organization_users, -> { where.not(activated_at: nil) }, dependent: nil, class_name: "OrganizationUser", inverse_of: :user
  has_many :organizations, through: :active_organization_users
  has_one :current_organization_user, ->(u) { where(organization_id: u.organization_id) }, class_name: "OrganizationUser", dependent: nil, inverse_of: :user, autosave: true

  # following as Creator
  has_many :created_organizations, class_name: "Organization", dependent: :nullify
  has_many :projects, dependent: :nullify
  has_many :sources, dependent: :nullify
  has_many :visibilities, dependent: :nullify

  has_many :rbf_claim_verifiers, foreign_key: :verifier_id, dependent: :destroy, inverse_of: :verifier
  has_many :verification_rbf_claims, through: :rbf_claim_verifiers, source: :rbf_claim, class_name: "RbfClaim"

  belongs_to :organization, autosave: true

  validates_associated :organization
  validates_associated :organization_users
  validates_associated :current_organization_user

  validates :username, presence: true, unless: :invited?
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: URI::MailTo::EMAIL_REGEXP
  validates :invite_code, uniqueness: { case_sensitive: false }, allow_blank: true
  validates :password, confirmation: true, if: :password
  validates :password, unpwned: { min: MIN_PW_LENGTH, max: 64 }, if: :password unless Rails.env.development?

  accepts_nested_attributes_for :organization_users

  auto_strip_attributes :email, :username, squish: true

  before_validation on: :create do
    generate_invite_code
  end

  after_save do
    active_organization_users.each(&:update_grafana_rights) if active? && admin_previously_changed?
  end

  after_create_commit do
    mail_invitation if invited? && invite_code.present?
  end

  after_commit :create_grafana_user, on: :create

  scope :invited, -> { where active_at: nil }
  scope :active, -> { where.not(active_at: nil).where(archived_at: nil) }
  scope :archived, -> { where.not archived_at: nil }
  scope :inactive, -> { invited.or archived }

  # privileges template on creation of a user
  attr_accessor :priv_template

  # allow this in invitation form, trick the password manager to use the email as login
  def emanresu_in_reverse
    username
  end

  def emanresu_in_reverse=(username)
    self.username = username
  end

  def organization=(organization)
    if new_record?
      self.current_organization_user ||= OrganizationUser.new(activated_at: Time.zone.now, organization:)
      self.current_organization_user.organization = organization
    end
    super
  end

  def organization_id=(organization_id)
    if new_record?
      self.current_organization_user ||= OrganizationUser.new(activated_at: Time.zone.now, organization_id:)
      self.current_organization_user.organization_id = organization_id
    end
    super
  end

  def self.priv_templates_available_for_creation
    raise "user lags 'user.manage' privilege and can't create new users" unless Current.user.can?("user.manage")

    Privilege::PRIV_TEMPLATES.keys
  end

  def state
    return :invited unless active_at
    return :active unless archived_at

    :archived
  end

  STATES.each do |st|
    define_method "#{st}?" do
      state == st
    end
  end

  def inactive?
    state != :active
  end

  def active?
    !inactive?
  end

  def activate
    self.active_at = Time.zone.now
  end

  def archive
    self.archived_at = Time.zone.now
  end

  def to_s_email
    [username, email].compact.join(" - ")
  end

  def to_s
    (username.presence || email).to_s
  end

  def jwt_json
    { sub: id.to_s, username: Naming.grafana_user(self) }
  end

  # check if user accepted all terms at the current version
  def all_terms_accepted?
    { terms_of_use:,
      data_protection_policy:,
      code_of_conduct: }.map do |k, v|
      v["accepted"] && v["version"] == Rails.configuration.current_terms_version[k]
    end.all?
  end

  def self.normalize_email(email)
    email.to_s.strip.downcase
  end

  def self.by_normalized_email(email)
    find_by email: normalize_email(email)
  end

  def mail_invitation
    UserMailer.with(user: self, inviting_user: Current.user).invite.deliver_later
  end

  def mail_welcome
    UserMailer.with(user: self).welcome.deliver_later
  end

  def mail_organization_invite(organization)
    UserMailer.with(user: self, organization:).organization_invite.deliver_later
  end

  def generate_invite_code
    self.invite_code = SecureRandom.hex
  end

  def user_in_organization?(organization_or_id)
    organization_id = organization_or_id.try(:id) || organization_or_id
    organizations.pluck(:id).intersect?([organization_id.to_i])
  end

  #
  # checks if user has privilege and the the record belongs to the same organization as the privilege
  #
  def can?(privilege, record: nil, in_organization: nil)
    raise "Unknown privilege #{privilege}" unless Privilege::PRIVILEGES.include? privilege

    return true if admin?

    if in_organization
      has_priv = active_organization_users.find_by(organization_id: in_organization.id)&.privilege? privilege
      has_priv && (record.nil? || record.owning_organization == in_organization)
    else
      has_priv = current_organization_user.privilege? privilege
      has_priv && (record.nil? || record.owning_organization == organization)
    end
  end

  def switch_organization!(organization_or_id)
    organization_id = organization_or_id.try(:id) || organization_or_id

    return unless user_in_organization?(organization_id)

    Grafana::RemoteControl.switch_current_organization self, Organization.find(organization_id)
    update! organization_id:
  end

  def can_any?(privileges_array, record: nil, in_organization: nil)
    privileges_array.any? { can? _1, record:, in_organization: }
  end

  def owning_organization
    organization
  end

  def create_grafana_user
    Grafana::RemoteControl.create_user self, organization
  end

  def grafana_role
    if can?("grafana.edit")
      "Editor"
    else
      "Viewer"
    end
  end
end
