class Decrypt {
    privateKey;
    enc_shared_keys;
    shared_keys;
    saltArrayBuffer;
    ivArrayBuffer;
    wrappedKeyBuffer;

    constructor(params) {
        this.buildBytesFromParams(params.wrapped_key, params.salt, params.iv);
        this.enc_shared_keys = params.enc_shared_keys;
        this.shared_keys = {};
    }

    buildBytesFromParams(wKeyString, saltString, ivString) {
        this.wrappedKeyBuffer = this.base64ToArrayBuffer(wKeyString);
        this.saltArrayBuffer = this.base64ToArrayBuffer(saltString);
        this.ivArrayBuffer = this.base64ToArrayBuffer(ivString);
    }

    async setPasswordUnwrapKey(password) {
        this.privateKey = await this.unwrapPrivateKey(password);
        await this.decryptSharedKeys();
    }

    async setPrivateKey(key_b64) {
        const key = this.base64ToArrayBuffer(key_b64);
        this.privateKey = await window.crypto.subtle.importKey(
            "pkcs8",
            key,
            { name: "RSA-OAEP", hash: "SHA-256" },
            true,
            ["decrypt"],
        );
        await this.decryptSharedKeys();
    }

    async decryptSharedKeys() {
        for (const [id, enc_key] of this.enc_shared_keys) {
            const array_buffer = await this.decryptMessage(enc_key, false);

            const key = await window.crypto.subtle.importKey(
                "pkcs8",
                array_buffer,
                {
                    name: "RSA-OAEP",
                    hash: "SHA-256",
                },
                false,
                ["decrypt"],
            );
            this.shared_keys[id] = key;
        }
    }

    base64ToArrayBuffer(base64) {
        return Uint8Array.from(atob(base64), (char) => char.charCodeAt(0));
    }

    getKeyMaterial(password) {
        const enc = new TextEncoder();
        return window.crypto.subtle.importKey(
            "raw",
            enc.encode(password),
            { name: "PBKDF2" },
            false,
            ["deriveBits", "deriveKey"],
        );
    }

    async getUnwrappingKey(password) {
        // get the key material (user-supplied password)
        const keyMaterial = await this.getKeyMaterial(password);

        // derive the key from key material and salt
        return window.crypto.subtle.deriveKey(
            {
                name: "PBKDF2",
                salt: this.saltArrayBuffer,
                iterations: 100000,
                hash: "SHA-256",
            },
            keyMaterial,
            { name: "AES-CBC", length: 256 },
            true,
            ["wrapKey", "unwrapKey"],
        );
    }

    async unwrapPrivateKey(password) {
        // 1. get the unwrapping key
        const unwrappingKey = await this.getUnwrappingKey(password);

        try {
            return await window.crypto.subtle.unwrapKey(
                "pkcs8", // import format
                this.wrappedKeyBuffer, // ArrayBuffer representing key to unwrap
                unwrappingKey, // CryptoKey representing key encryption key
                {
                    // algorithm params for key encryption key
                    name: "AES-CBC",
                    iv: this.ivArrayBuffer,
                },
                {
                    // algorithm params for key to unwrap
                    name: "RSA-OAEP",
                    hash: "SHA-256",
                },
                true, // extractability of key to unwrap
                ["decrypt"], // key usages for key to unwrap
            );
        } catch (e) {
            console.error(`Could not unwrap the private key: ${e}`);
            alert("It seems the password was not correct - please try again");
            this.password = null;
            throw "Incorrect password";
        }
    }

    // checking base64 via regex can fail if the string to test is way to long, so we try this:
    // https://stackoverflow.com/questions/77464085/validate-base64-regex-causes-rangeerror-maximum-call-stack-size-exceeded
    isValidBase64(msg) {
        try {
            return msg.length > 3 && !(msg.length % 4) && atob(msg) && true;
        } catch (err) {
            return false;
        }
    }

    checkBase64AndConvertToArrayBuffer(enc_msg) {
        if (!this.isValidBase64(enc_msg)) {
            console.error(`The encoded data is not base64 encoded:${enc_msg}`);
            return null;
        }
        return this.base64ToArrayBuffer(enc_msg);
    }

    async decryptRSA(enc_msg, rsa_key) {
        const decrypted = await crypto.subtle.decrypt(
            {
                name: "RSA-OAEP",
            },
            rsa_key,
            enc_msg,
        );
        return decrypted;
    }

    async decryptAES(aes_data, enc_msg) {
        const [key, iv] = aes_data.split(":");

        const aes_key = await window.crypto.subtle.importKey(
            "raw",
            this.base64ToArrayBuffer(key),
            {
                name: "AES-CTR",
            },
            false,
            ["decrypt"],
        );

        const msg = await crypto.subtle.decrypt(
            {
                name: "AES-CTR",
                counter: this.base64ToArrayBuffer(iv), // intuitive naming...
                length: 128,
            },
            aes_key,
            enc_msg,
        );
        return msg;
    }

    // the encrypted messages have two parts, the first is the encrypted AES key that needs to be decrypted via the
    // private RSA key and the second part is the actual AES encrypted message
    async decryptMessage(message, encode = true, rsa_key = this.privateKey) {
        if (message === null || message === undefined || message.length === 0) {
            return null;
        }

        const dec = new TextDecoder();

        // message could be a pure RSA encrypted one or AES and RSA combined
        try {
            if (message.includes(":")) {
                const [id, aes, enc_msg] = message.split(":");
                let use_rsa_key;
                if (this.shared_keys[id]) {
                    use_rsa_key = this.shared_keys[id];
                } else {
                    use_rsa_key = rsa_key;
                }
                const aes_data = await this.decryptMessage(
                    aes,
                    true,
                    use_rsa_key,
                );
                const enc_msg_ab =
                    this.checkBase64AndConvertToArrayBuffer(enc_msg);

                const decrypted = await this.decryptAES(aes_data, enc_msg_ab);
                if (encode) {
                    return dec.decode(decrypted);
                }
                return decrypted;
            }

            const enc_msg_ab = this.checkBase64AndConvertToArrayBuffer(message);
            const msg = await this.decryptRSA(enc_msg_ab, rsa_key);
            if (encode) {
                return dec.decode(msg);
            }
            return msg;
        } catch (e) {
            console.error(
                `Could not decrypt the message: ${message}; Error: ${e}`,
            );
            return null;
        }
    }
}

window.Decrypt = Decrypt;
