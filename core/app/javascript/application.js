// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"
import "keygen"
import "decrypt"
import Papa from 'papaparse'
import TomSelect from 'tom-select'

window.Papa = Papa
window.TomSelect = TomSelect