(() => {

  /*
  Store the calculated ciphertext here, so we can decrypt the message later.
  */
  let ciphertext;
  let keyPair;
  let salt;
  let iv;

  let decryptArea;

  /*
  Fetch the contents of the "message" textbox, and encode it
  in a form we can use for the encrypt operation.
  */
  function getMessageEncoding() {
    const messageBox = document.querySelector("#rsa-oaep-message");
    const message = messageBox.value;
    const enc = new TextEncoder();
    return enc.encode(message);
  }

  /*
  Get the encoded message, encrypt it and display a representation
  of the ciphertext in the "Ciphertext" element.
  */
  async function encryptMessage(key) {
    const encoded = getMessageEncoding();
    ciphertext = await window.crypto.subtle.encrypt(
      {
        name: "RSA-OAEP"
      },
      key,
      encoded
    );

    const buffer = new Uint8Array(ciphertext, 0, 5);
    const ciphertextValue = document.querySelector(".rsa-oaep .ciphertext-value");
    ciphertextValue.classList.add('fade-in');
    ciphertextValue.addEventListener('animationend', () => {
      ciphertextValue.classList.remove('fade-in');
    });
    //ciphertextValue.textContent = `${buffer}...[${ciphertext.byteLength} bytes total]`;
    ciphertextValue.textContent = _arrayBufferToBase64(ciphertext);
  }


  function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
  }

  function _arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  function base64ToArrayBuffer(base64) {
    return Uint8Array.from(atob(base64), char => char.charCodeAt(0))
  }

  async function loadDataFromListAndDecrypt() {
    // retrieve private Key from wrapped Key
    if (passwordField.value === "") {
      alert("Please enter your decryption password");
      return;
    }

    const unwrappingKey = document.querySelector("#wrapped-key").value;
    const wrappedKey = base64ToArrayBuffer(unwrappingKey);
    const privateKey = await unwrapPrivateKey(wrappedKey);

    // load messages that need to be decrypted
    const messageList = document.querySelector("#message-list");
    const items = messageList.getElementsByTagName("li");

    for (let i = 0; i < items.length; ++i) {
      //decrypt the message
      const decrypted_message = await decryptMessage(privateKey, items[i].innerText);
      if (decrypted_message != null) {
        items[i].innerText = decrypted_message;
      }
    }
  }

  /*
  Fetch the ciphertext and decrypt it.
  Write the decrypted message into the "Decrypted" box.
  */
  async function decryptMessage(key, message) {
    // load data that needs to be decrypted
    let to_be_decrypted;
    try {
      // check if it is base64 encoded
      const base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
      if (!base64regex.test(message)) {
        return null;
      }
      to_be_decrypted = base64ToArrayBuffer(message);
    } catch (e) {
      return null;
    }
    const decrypted = await window.crypto.subtle.decrypt(
      {
        name: "RSA-OAEP"
      },
      key,
      to_be_decrypted,
    );

    const dec = new TextDecoder();
    return dec.decode(decrypted);
  }

  async function exportPublicKey(key) {
    const exported = await window.crypto.subtle.exportKey("spki", key);
    const exportedAsString = ab2str(exported);
    const exportedAsBase64 = window.btoa(exportedAsString);
    const pemExported = `${exportedAsBase64}`;
    exportKeyOutput.value = pemExported;
  }

  function exportWrappedKey(key) {
    const exportedAsString = ab2str(key);
    const exportedAsBase64 = window.btoa(exportedAsString);
    const pemExported = `${exportedAsBase64}`;

    return pemExported;
  }


  /*
  Get some key material to use as input to the deriveKey method.
  The key material is a password supplied by the user.
  */
  function getKeyMaterial(unwrap) {
    const password = passwordField.value;
    const enc = new TextEncoder();
    return window.crypto.subtle.importKey(
      "raw",
      enc.encode(password),
      { name: "PBKDF2" },
      false,
      ["deriveBits", "deriveKey"]
    );
  }

  /*
  Given some key material and some random salt
  derive an AES-CBC key using PBKDF2.
  */
  function getKey(keyMaterial, salt) {
    return window.crypto.subtle.deriveKey(
      {
        name: "PBKDF2",
        salt: salt,
        iterations: 100000,
        hash: "SHA-256"
      },
      keyMaterial,
      { name: "AES-CBC", length: 256 },
      true,
      ["wrapKey", "unwrapKey"]
    );
  }

  /*
   Derive an AES-GCM key using PBKDF2.
 */
  async function getUnwrappingKey() {
    // 1. get the key material (user-supplied password)
    const keyMaterial = await getKeyMaterial();
    // 2 initialize the salt parameter.
    // The salt must match the salt originally used to derive the key.
    // In this example it's supplied as a constant "saltBytes".
    const saltBuffer = base64ToArrayBuffer(document.querySelector("#salt").value);

    // 3 derive the key from key material and salt
    return window.crypto.subtle.deriveKey(
      {
        name: "PBKDF2",
        salt: saltBuffer,
        iterations: 100000,
        hash: "SHA-256",
      },
      keyMaterial,
      { name: "AES-CBC", length: 256 },
      true,
      ["wrapKey", "unwrapKey"]
    );
  }

  /*
  Unwrap an RSA-PSS private signing key from an ArrayBuffer containing
  the raw bytes.
  Takes an array containing the bytes, and returns a Promise
  that will resolve to a CryptoKey representing the private key.
  */
  async function unwrapPrivateKey(wrappedKey) {
    // 1. get the unwrapping key
    const unwrappingKey = await getUnwrappingKey();
    // const unwrappingKey = wrappingKeyGlobal;
    // 2. initialize the wrapped key
    const wrappedKeyBuffer = wrappedKey;

    // 3. initialize the iv
    const ivBuffer = base64ToArrayBuffer(document.querySelector("#iv").value);
    // 4. unwrap the key
    const privKey = await window.crypto.subtle.unwrapKey(
      "pkcs8", // import format
      wrappedKeyBuffer, // ArrayBuffer representing key to unwrap
      unwrappingKey, // CryptoKey representing key encryption key
      {
        // algorithm params for key encryption key
        name: "AES-CBC",
        iv: ivBuffer,
      },
      {
        // algorithm params for key to unwrap
        name: "RSA-OAEP",
        hash: "SHA-256",
      },
      true, // extractability of key to unwrap
      ["decrypt"] // key usages for key to unwrap
    );
    return privKey;
  }

  let wrappedKeyBufferGlobal;
  let wrappingKeyGlobal;

  /*
  Wrap the given key and write it into the "wrapped-key" space.
  */
  async function wrapCryptoKey(keyToWrap) {
    // get the key encryption key
    const keyMaterial = await getKeyMaterial();
    salt = window.crypto.getRandomValues(new Uint8Array(16));
    const wrappingKey = await getKey(keyMaterial, salt);
    iv = window.crypto.getRandomValues(new Uint8Array(16));

    const wrapped = await window.crypto.subtle.wrapKey(
      "pkcs8",
      keyToWrap,
      wrappingKey,
      {
        name: "AES-CBC",
        iv: iv
      }
    );

    wrappingKeyGlobal = wrappingKey;

    const wrappedKeyBuffer = new Uint8Array(wrapped);
    wrappedKeyBufferGlobal = wrappedKeyBuffer;

    const wrappedKeyOutput = document.querySelector("#wrapped-key");
    wrappedKeyOutput.value = exportWrappedKey(wrappedKeyBuffer);

    const saltOutput = document.querySelector("#salt");
    saltOutput.value = _arrayBufferToBase64(salt);
    const ivOutput = document.querySelector("#iv");
    ivOutput.value = _arrayBufferToBase64(iv);
  }


  /*
  Generate an encryption key pair, then set up event listeners
  on the "Encrypt" and "Decrypt" buttons.
  */

  async function generateKeyPair() {

    //check if Password was entered

    if (passwordField.value === "") {
      alert("Please enter a password to encrypt the data.");
      return;
    }

    window.crypto.subtle.generateKey(
      {
        name: "RSA-OAEP",
        // Consider using a 4096-bit key for systems that require long-term security
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: "SHA-256",
      },
      true,
      ["encrypt", "decrypt"]
    ).then((gkeyPair) => {
      keyPair = gkeyPair;

      exportPublicKey(gkeyPair.publicKey);
      wrapCryptoKey(gkeyPair.privateKey);

      decryptButton.addEventListener("click", () => {
        loadDataFromListAndDecrypt();
      });
      sendMessageButton.style = "display: block;";

    });
  }

  const sendMessageButton = document.querySelector("#encrypt_section");
  const passwordField = document.querySelector("#password");
  const publicKeyfield = document.querySelector("#password");
  const createKeyButton = document.querySelector("#create_key");
  const exportKeyOutput = document.querySelector("#public-key");
  const decryptButton = document.querySelector("#decrypt_messages");


  createKeyButton.addEventListener("click", () => {
    generateKeyPair();
  });

  if (exportKeyOutput.value !== "") {
    sendMessageButton.style = "display: block;";
    decryptButton.addEventListener("click", () => {

      loadDataFromListAndDecrypt();
    });
  }
})();