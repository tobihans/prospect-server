import { Controller } from "@hotwired/stimulus"
import { TabulatorFull as Tabulator } from "tabulator-tables";

export default class extends Controller {
  static values = {
    options: Object
  }
  static targets = [ "table" ]

  connect() {
    let tableId = "#" + this.tableTarget.id;
    let defaultOptions = {
      layout: "fitDataStretch",
      pagination: true,
      paginationSize:100,
      // paginationSizeSelector:true
    }
    let mergedOpts = { ...defaultOptions, ...this.optionsValue};

    this.table = new Tabulator(tableId, mergedOpts);


    // Optionally, expose the table instance for global access, e.g., for debugging or direct manipulation
    // window.myTabulatorInstance = this.table;
  }
}