import { Controller } from "@hotwired/stimulus"
import { TabulatorFull as Tabulator } from "tabulator-tables";

export default class extends Controller {
  static values = {
    options: Object
  }

  connect() {
    const options = JSON.parse(this.element.dataset.tabulatorOptions);
    // we can't create the callback functions in json, so here we go:
    // if the data comes from our API, we split out the custom column
    // and merge it with the rest of the row, to expant the table with the
    // addional values we have in the custom column
    if (options.ajaxURL) {
      options.ajaxResponse = (...[, , response]) => {
        response.data = response.data.map((row) => {
          const { custom, ...data } = row;
          return { ...data, ...custom }
        });
        return response;
      };
    }
    this.table = new Tabulator(this.element, options);

    // Optionally, expose the table instance for global access, e.g., for debugging or direct manipulation
    window.myTabulatorInstance = this.table;
  }
}