import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static values = {"url": String, "filename": String};
  static targets = ["button"];

  initialize() {
    this.started = false;
  }

  downloadCSV() {
    if (this.started) { return }

    if (!window.vault.is_open()) {
      window.vault.openModal(() => {
        this.downloadCSV()
      })
      return
    }

    this.started = true
    this.buttonTarget.innerText = "Downloading CSV..."
    window.dispatchEvent(new CustomEvent("open-modal"))
    fetch(this.urlValue)
      .then(response => {
        // Ensure the request was successful
        if (!response.ok) {
          throw new Error(`Network response was not ok ${response.statusText}`);
        }
        return response.blob();
      })
      .then(blob => {
        this.parseCSVFromBlobAndDecrypt(blob);
      })
      .catch(error => {
        this.buttonTarget.innerText = "Failed to Download"
        console.error('There was an error downloading the CSV file:', error);
      });
  }

  downloadBlobAsCSV(obj) {
    const csvData = new Blob([Papa.unparse(obj.data)], {type: 'text/csv;charset=utf-8;'});
    const url = window.URL.createObjectURL(csvData);
    const a = document.createElement('a');

    a.style.display = 'none';
    a.href = url;
    // Specify filename to download as
    a.download = this.filenameValue;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    this.buttonTarget.innerText = "Download Done"
    this.started = false;
  }


  async decryptCSVcolumns(csv) {
    // Assuming the first row contains column headers
    const count = csv.data[0].filter((h) => {
      return h.endsWith('_e')
    }).length * csv.data.length;
    let i = 1;
    if (csv.data.length > 0) {
      for (const [Yindex, header] of csv.data[0].entries()) {
        if (!header.endsWith("_e")) {
          continue;
        }

        csv.data[0][Yindex] = header.slice(0, -2)

        for (const row of csv.data.slice(1)) {
          this.buttonTarget.innerText = `Decrypting ${i}/${count}`
          i++;
          row[Yindex] = await window.vault.decryptMessage(row[Yindex])
        }
      }
      this.downloadBlobAsCSV(csv);
    }
  }

  async parseCSVFromBlobAndDecrypt(blob) {
    const reader = new FileReader();
    reader.onload = (event) => {
      const csvText = event.target.result;
      Papa.parse(csvText, {
        complete: this.decryptCSVcolumns.bind(this),
        error: (error) => {
          console.error('Error parsing CSV:', error);
          this.buttonTarget.innerText = 'Error parsing CSV!';
        }
      });
    };
    reader.onerror = (error) => {
      console.error('Error reading CSV blob:', error);
      this.buttonTarget.innerText = 'Error reading CSV blob!';
    };
    reader.readAsText(blob);
  }
}