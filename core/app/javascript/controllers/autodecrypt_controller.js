import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {}

  async decryptElement(element) {
    let enc_data = element.dataset.encData;

    if (enc_data.match("http.*encrypted_blob")) {
      console.log("ENCRYPTED BLOB");
      let src = await window.vault.decryptMessage(await(await fetch(enc_data)).text());

      if (src.startsWith("data:application/pdf;")) {
        console.log("PDF detected")
        const el = document.createElement("a");
        el.setAttribute("href", src);
        el.setAttribute("target", "_blank");
        el.classList.add("text-primary", "hover:underline");
        el.innerText = "Open PDF"
        element.textContent = null;
        element.appendChild(el);
      } else {
        let img = document.createElement("img");
        img.setAttribute("src", src);
        img.style.maxWidth = "300px";
        img.style.maxHeight = "300px";
        element.textContent = null;
        element.appendChild(img);

        img.onclick = (e) => {
          const preview = document.getElementById("image_view");
          if (preview === null) return;
          preview.parentElement.rotation = 0
          preview.innerHTML = "";
          let cloned = e.target.cloneNode();
          cloned.removeAttribute("style");
          preview.append(cloned);
          window.dispatchEvent(new Event("open-modal"))
        }
      }

      return;
    }

    const plain = await window.vault.decryptMessage(enc_data.trim());

    if (plain !== null && plain.startsWith("data:image/")) {
      let img = document.createElement("img");
      img.setAttribute("src", plain);
      img.style.maxWidth = "300px";
      img.style.maxHeight = "300px";
      element.textContent = null;
      element.appendChild(img);
    } else {
      element.textContent = plain;
    }
  }

  // decrypt will be triggered automatically if the vault is opened
  // via the `vault:vault-opened` event on window
  async decrypt() {
    console.log("Decrypt triggered");
    if (!window.vault.is_open()) {
      console.log("vault closed but decrypt was triggered");
      return;
    }

    for (const item of this.element.querySelectorAll(
      "[data-field-key$='_e']",
    )) {
      this.decryptElement(item);
    }
  }
}
