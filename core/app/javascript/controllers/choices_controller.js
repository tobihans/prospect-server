import { Controller } from "@hotwired/stimulus";

import Choices from "choices.js";

export default class extends Controller {
  static targets = ["field"];

  static element = this.targets.element;

  connect() {
    var defaultOptions = {
      allowHTML: true,
      removeItems: true,
      removeItemButton: true,
    };

    if (this.element.dataset.choicesContainerOuterClass) {
      var selectBoxOptions = {
        classNames: {
          containerOuter: this.element.dataset.choicesContainerOuterClass,
        },
      };
      defaultOptions = { ...defaultOptions, ...selectBoxOptions };
    }

    new Choices(this.element, defaultOptions);
  }
}
