import {Controller} from "@hotwired/stimulus";

export default class extends Controller {
  // static element = this.targets.element;

  connect() {
    new TomSelect(
      this.element,
      {maxOptions: 100, create: false, plugins: ["remove_button"]}
    );
  }
}
