// Inspired from https://github.com/stimulus-components/stimulus-checkbox-select-all/
// to allow grouped checkboxes: show the select all button in tabbar, and select a certain checkbox_group

import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['checkboxAll', 'checkboxGroup', 'checkbox']

  connect() {
    this.refresh()
  }

  refresh() {
    this.checkboxGroupTargets.forEach((cgt) => {
      let group = cgt.dataset.checkboxGroup
      let checkedCount = this.checked(group).length
      let uncheckedCount = this.unchecked(group).length
      cgt.checked = checkedCount > 0
      cgt.indeterminate = checkedCount > 0 && uncheckedCount > 0
    })

    let allCheckedCount = this.allChecked().length
    let allUncheckedCount = this.allUnchecked().length

    this.checkboxAllTarget.checked = allCheckedCount > 0
    this.checkboxAllTarget.indeterminate = allCheckedCount > 0 && allUncheckedCount > 0
  }

  toggleAll(e) {
    e.preventDefault()
    this.checkboxTargets.forEach(checkbox => {
      checkbox.checked = e.target.checked
      checkbox.indeterminate = false
    })
    this.checkboxGroupTargets.forEach(checkbox => {
      checkbox.checked = e.target.checked
      checkbox.indeterminate = false
    })
  }

  toggleGroup(e) {
    let group = e.target.dataset.checkboxGroup
    this.checkboxTargetsForGroup(group).forEach(checkbox => {
      checkbox.checked = e.target.checked
    })
    this.refresh()
  }

  checkboxTargetsForGroup(group) {
    return this.checkboxTargets.filter((cbt) => cbt.dataset.checkboxGroup == group);
  }

  checked(group) {
    return this.checkboxTargetsForGroup(group).filter(cbt => cbt.checked)
  }

  unchecked(group) {
    return this.checkboxTargetsForGroup(group).filter(cbt => !cbt.checked)
  }

  allChecked() {
    return this.checkboxTargets.filter(checkbox => checkbox.checked)
  }

  allUnchecked() {
    return this.checkboxTargets.filter(checkbox => !checkbox.checked)
  }
}
