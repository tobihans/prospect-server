import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = [
    "decryptPassword",
    "openVault",
    "lockVault",
    "lock",
    "unlock",
    "unlockDialog",
    "lockDialog",
    "cancel",
  ];

  vault_is_open = false;
  callback = null;
  broadcast_channel = null;

  toggleLock() {
    const svg1 = window.vault.lockTarget.querySelector("svg:not(.hidden)");
    const svg2 = window.vault.lockTarget.querySelector("svg.hidden");
    svg1.classList.add("hidden");
    svg2.classList.remove("hidden");
    this.vault_is_open = !this.vault_is_open;
  }

  toggleDialog() {
    this.unlockDialogTarget.classList.toggle("hidden");
    this.lockDialogTarget.classList.toggle("hidden");
  }

  async exportKeyToStorage(decryptor) {
    const private_key = await window.crypto.subtle.exportKey(
      "pkcs8",
      decryptor.privateKey,
    );
    const codes = new Uint8Array(private_key);
    const bin = String.fromCharCode.apply(null, codes);
    const b64 = btoa(bin);
    localStorage.setItem("vault_key", b64);
  }

  async openVault(broadcast = true) {
    const decryptor = new Decrypt(
      JSON.parse(this.element.dataset.cryptoOptions),
    );
    this.decryptMessage = decryptor.decryptMessage.bind(decryptor);
    try {
      // if the local storage is there, we assume the vault unlocked on load
      // and we try to unlock it automatically
      const key = localStorage.getItem("vault_key");
      if (key) {
        await decryptor.setPrivateKey(key);
      } else {
        await decryptor.setPasswordUnwrapKey(this.decryptPasswordTarget.value);
      }
      this.toggleLock();
      this.toggleDialog();

      if (this.callback) {
        this.callback.call();
        this.clearCallback();
      }

      if (broadcast) {
        await this.exportKeyToStorage(decryptor);
        this.broadcast_channel.postMessage("vault_opened");
      }
      this.dispatch("vault-opened");
    } catch (e) {
      console.error(`Could not open Vault: ${e}`);
    }
  }

  decryptMessage() {
    throw "Vault not open";
  }

  is_open() {
    return this.vault_is_open;
  }

  lockVault(broadcast = true) {
    if (!this.is_open()) {
      return;
    }
    this.decryptMessage = () => {
      throw "Vault not open";
    };
    localStorage.removeItem("vault_key");
    this.toggleLock();
    this.toggleDialog();

    if (broadcast) {
      this.broadcast_channel.postMessage("vault_locked");
    }
    window.location.reload();
  }

  // optional function to call after unlocking the vault
  openModal(callback) {
    this.lockTarget.click();
    this.callback = callback;
  }

  clearCallback() {
    this.callback = null;
  }

  setupBroadcast() {
    this.broadcast_channel = new BroadcastChannel("vault_channel");

    this.broadcast_channel.onmessage = (event) => {
      switch (event.data) {
        case "vault_locked":
          // only lock if the current tab is not the one that locked the vault
          if (this.is_open()) {
            this.lockVault(false);
          }
          break;
        case "vault_opened":
          if (!this.is_open()) {
            this.openVault(false);
          }
          break;
      }
    };
  }

  connect() {
    this.openVaultTarget.onclick = this.openVault.bind(this);
    this.lockVaultTarget.onclick = this.lockVault.bind(this);
    this.cancelTarget.onclick = this.clearCallback.bind(this);
    this.setupBroadcast();

    this.lockTarget.firstChild.appendChild(this.unlockTarget);

    window.vault = this;
    if (localStorage.getItem("vault_key")) {
      this.openVault();
    }
  }
}
