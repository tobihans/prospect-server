// Import and register all your controllers from the importmap under controllers/*

import { application } from "controllers/application"

// Eager load all controllers defined in the import map under controllers/**/*_controller
import { eagerLoadControllersFrom } from "@hotwired/stimulus-loading"
eagerLoadControllersFrom("controllers", application)

// Lazy load controllers as they appear in the DOM (remember not to preload controllers in import map!)
// import { lazyLoadControllersFrom } from "@hotwired/stimulus-loading"
// lazyLoadControllersFrom("controllers", application)
import Dropdown from 'stimulus-dropdown'
application.register('dropdown', Dropdown)

import { Tabs, Modal } from "tailwindcss-stimulus-components"
application.register('tabs', Tabs)
application.register('modal', Modal)

import PasswordVisibility from '@stimulus-components/password-visibility'
application.register('password-visibility', PasswordVisibility)

import Clipboard from '@stimulus-components/clipboard'
application.register('clipboard', Clipboard)