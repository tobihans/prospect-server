import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static values = { "msg": String } // upload POST url

  connect() {
    this.element.addEventListener("click", this.disableLink.bind(this))
  }

  disableLink(e) {
    console.log("Disabling the link")
    this.element.setAttribute("disabled", true);
    this.element.textContent = this.msgValue;
    this.element.classList.remove("from-shade-3", "to-shade-2", "from-pink", "to-grey-4");
    this.element.classList.add("from-grey-4", "to-grey-3", "cursor-wait", "pointer-events-none")
  }
}
