// Screen Recording a file upload
// Starts recording with start().
// With stop() it uploads the file + the recording to url
// uploads as formdata with blobs recording and file

import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
  static values = { "url": String } // upload POST url
  static targets = ["start", "file", "stop"] // button, file input, button
  static classes = [ "uploading" ] // enhance stop element during upload

  connect() {
    console.log(this.uploadingClasses)
  }

  async start() {
    let stream = await this.recordScreen();
    let mimeType = 'video/webm';
    this.mediaRecorder = this.createRecorder(stream, mimeType);

    this.startTarget.textContent = "Recording in progress"
    this.dispatch("start");
    console.log("Started recording");
  }

  stop() {
    this.mediaRecorder.stop();
  }

  async recordScreen() {
    return await navigator.mediaDevices.getDisplayMedia({
      audio: true,
      video: { mediaSource: "screen"}
    });
  }

  createRecorder (stream, mimeType) {
    // the stream data is stored in this array
    let recordedChunks = [];

    const mediaRecorder = new MediaRecorder(stream);

    mediaRecorder.ondataavailable =  (e) => {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);
      }
    };

    mediaRecorder.onstop =  () => {
      mediaRecorder.stream.getTracks().forEach( track => track.stop() );
      this.startTarget.textContent = "Recording stopped"
      this.saveFile(recordedChunks);
      recordedChunks = [];
    };

    mediaRecorder.start(200); // For every 200ms the stream data will be stored in a separate chunk.
    return mediaRecorder;
  }

  saveFile(recordedChunks) {
    const blob = new Blob(recordedChunks, {type: 'video/webm'});
    const file = this.fileTarget.files[0];
    const formData = new FormData();
    formData.append("recording", blob);
    formData.append("file", file);

    let csrf = document.getElementsByName("csrf-token")[0].content

    this.stopTarget.textContent = "Uploading...";
    this.stopTarget.classList.add(...this.uploadingClasses);

    fetch(this.urlValue, {
      method: 'POST',
      headers: {"X-CSRF-Token": csrf},
      body: formData
    })
      .then(resp => resp.json())
      .then(json => {
        this.stopTarget.textContent = "Success";
        this.stopTarget.classList.remove(...this.uploadingClasses);
        console.log(json);
      })
      .catch(error => {
        this.stopTarget.textContent = "Error";
        this.stopTarget.classList.remove(...this.uploadingClasses);
        console.log(error);
      });
  }
}
