import { Controller } from "@hotwired/stimulus";
import { TabulatorFull as Tabulator, Module } from "tabulator-tables";
import PapaParse from "papaparse";

function getCsrfToken() {
  return document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");
}

class Mapper {
  map
  reverse_map

  constructor(mapping, category) {
    if (mapping?.[category]) {
      this.map = mapping[category];
      this.reverse_map = this.flip(mapping[category]);
    }
  }

  flip(mapping) {
    return Object.fromEntries(
      Object.entries(mapping).map(([key, value]) => {
        const v = structuredClone(value);
        const k = value.field;
        v.field = key
        return [k, v]
      }),
    );
  }

  key(value) {
    return this.getFieldInfo(value, this.map)
  }

  reverseKey(value) {
    return this.getFieldInfo(value, this.reverse_map)
  }

  getFieldInfo(value, map) {
    if (map) {
      if (map[value]) {
        return map[value]
      }
      if (map[value.slice(0, -2)]) {
        const tmp = structuredClone(map[value.slice(0, -2)]);
        tmp.field = `${tmp.field}_e`
        return tmp
      }
    } else {
      return { field: value }
    }
  }
}

Tabulator.extendModule("filter", "filters", {
  eq: (filterVal, rowVal, rowData, filterParams) => { return filterVal === rowVal },
  lt: (filterVal, rowVal, rowData, filterParams) => { return filterVal < rowVal },
  gt: (filterVal, rowVal, rowData, filterParams) => { return filterVal > rowVal },
  lteq: (filterVal, rowVal, rowData, filterParams) => { return filterVal <= rowVal },
  gteq: (filterVal, rowVal, rowData, filterParams) => { return filterVal >= rowVal },
  not_eq: (filterVal, rowVal, rowData, filterParams) => { return filterVal !== rowVal },
  matches: (filterVal, rowVal, rowData, filterParams) => { return filterVal.matches(rowVal) }
});

class Filter {
  column_select;
  filter_select;
  filter_input;
  filter_count;
  filter_list;
  table;
  remote_filter;
  local_filter;

  constructor(
    table,
    column_select,
    filter_select,
    filter_input,
    filter_count,
    filter_list,
    add_filter_button,
    clear_filter_button,
    columns,
  ) {
    if (table.filter){
      this.remote_filter = table.filter.remote_filter;
      this.local_filter = table.filter.local_filter;
    }else{
      this.remote_filter = [];
      this.local_filter = [];
    }

    this.table = table;

    column_select.innerHTML = ""

    for (const c of columns) {
      const option = new Option();

      const mapping = this.table.options.mapper.reverseKey(c);

      if (mapping) {
        option.value = mapping.field;
        option.encrypted = mapping.encrypted;
      }

      option.text = c;
      if (c.endsWith("_e")) {
        option.disabled = true;
      } else {
        if (option.encrypted) {
          option.text = `${option.text} (local)`
        } else {
          option.text = `${option.text} (remote)`
        }
      }

      column_select.add(option);
    }

    const filter = [
      { text: "=", value: "eq" },
      { text: "<", value: "lt" },
      { text: ">", value: "gt" },
      { text: "<=", value: "lteq" },
      { text: ">=", value: "gteq" },
      { text: "!=", value: "not_eq" },
      { text: "~", value: "matches" },
    ];

    filter_select.innerHTML = ""
    for (const f of filter) {
      filter_select.add(new Option(f.text, f.value));
    }

    this.column_select = column_select;
    this.filter_select = filter_select;
    this.filter_input = filter_input;
    this.filter_count = filter_count;
    this.filter_list = filter_list;
    this.filter_count.parentElement.onclick = () => {
      this.filter_list.classList.toggle("hidden");
    };
    add_filter_button.onclick = this.addFilter.bind(this);
    clear_filter_button.onclick = this.clearFilters.bind(this);
  }

  clearFilters() {
    this.local_filter = [];
    this.remote_filter = [];
    this.table.options.filterMode = "remote";
    this.table.clearFilter();
    this.updateFilterCount();
    this.updateFilterList();
  }

  addFilter() {
    const filter = `${this.column_select.value} ${this.filter_select.value} ${this.filter_input.value}`;
    if (this.column_select.selectedOptions[0].encrypted) {
      this.local_filter.push({
        field: this.table.options.mapper.key(this.column_select.value).field,
        type: this.filter_select.value,
        value: this.filter_input.value,
      });
    } else {
      this.remote_filter.push({
        field: this.column_select.value,
        type: this.filter_select.value,
        value: this.filter_input.value,
      });
    }
    this.triggerFilter();
  }

  triggerFilter() {
    // partition filters in remote and local filters based on the column type (encrypted/not encrypted)
    // disable columns for local filters if vault is locked, listen to unlock and unlock the filter

    // apply remote filters
    this.table.options.filterMode = "remote";
    this.table.setFilter(this.remote_filter);

    // apply local filters
    this.table.options.filterMode = "local"
    this.table.setFilter(this.local_filter);

    this.table.module("filter").filterList = this.remote_filter;

    this.filter_input.value = "";
    this.updateFilterCount();
    this.updateFilterList();
  }

  updateFilterList() {
    // clean list
    this.filter_list.innerHTML = '';

    const addElementToList = (index, filter, list) => {
      const div = document.createElement("div");
      div.classList.add("m-1", "cursor-pointer");
      div.innerText = `${filter.field} ${filter.type} ${filter.value}`;
      this.filter_list.append(div);

      const remove = document.createElement("span");
      remove.innerText = "✖ ";
      remove.onclick = () => {
        list.splice(index, 1)
        this.triggerFilter();
      };
      div.prepend(remove);
    };

    for (const [index, filter] of this.remote_filter.entries()) {
      addElementToList(index, filter, this.remote_filter);
    }

    for (const [index, filter] of this.local_filter.entries()) {
      addElementToList(index, filter, this.local_filter);
    }
  }

  updateFilterCount() {
    const filter_count = this.remote_filter.length + this.local_filter.length;
    if (filter_count > 0) {
      this.filter_count.innerText = filter_count;
      this.filter_count.classList.remove("hidden")
    } else {
      this.filter_count.classList.add("hidden")
    }
  }
}

class SelectableColumnsModule extends Module {
  static moduleName = "selectableColumns";

  initialize() {
    this.table.options.ajaxResponse = async (...[, , response]) => {
      const new_data = [];
      for (const row of response.data) {
        const { custom, ...data } = row;

        // if custom has nested json we would otherwise just show: Object object
        for (const key of Object.keys(custom)) {
          if (typeof custom[key] === "object" && custom[key] !== null) {
            custom[key] = JSON.stringify(custom[key]);
          }
        }

        const comb_row = { ...data, ...custom };
        const new_row = {};

        if (window.vault?.is_open()) {
          for (const key in comb_row) {
            if (key.endsWith("_e")) {
              if (comb_row[key]?.match("http.*encrypted_blob")) {
                new_row[key.slice(0, -2)] = comb_row[key]
              } else {
                new_row[key.slice(0, -2)] = await window.vault.decryptMessage(
                  comb_row[key],
                );
              }
            } else {
              new_row[key] = comb_row[key];
            }
          }
          new_data.push(new_row);
        } else {
          new_data.push(comb_row);
        }
      }

      return {
        data: new_data,
        errors: response.errors,
        last_page: response.last_page,
      };
    };

    this.subscribe("table-initialized", this.addButtonAction.bind(this));
    this.subscribe("columns-loaded", this.initializeColumn.bind(this));
    this.subscribe("cell-value-changed", this.cellUpdated.bind(this));
    this.subscribe("data-loaded", this.mapData.bind(this));
    this.subscribe("column-format", this.formatColumn.bind(this));
    this.subscribe("data-loading", this.dataLoading.bind(this));
  }

  dataLoading(data, params, config, silent) {
    if (this.table.filter){
      this.table.options.filterMode = "remote"
      this.table.module("filter").filterList = this.table.filter.remote_filter;
    }
    return true;
  }

  formatColumn(column, title, el) {
    const mapping = this.table.options.mapper.reverseKey(title)
    if (window.vault && !window.vault.is_open() && mapping?.encrypted) {
      return title;
    }
    column.definition.formatter = mapping?.format;
    return title;
  }

  mapData(data) {
    if (this.table.filter) {
      this.table.options.filterMode = "local"
      this.table.module("filter").filterList = this.table.filter.local_filter;
    }

    if (!this.table.options.mapper) {
      return data.data;
    }

    const category = this.table.options.data_category;

    const mapped_data = [];

    for (const row of data.data) {
      const mapped_row = {};
      for (const [key, value] of Object.entries(row)) {
        // TODO: we need some kind of mechanism to map _e or _p or _b(?) specifically
        if (key.endsWith("_p")) {
          continue;
        }

        const mapped = this.table.options.mapper.key(key)
        if (mapped) {
          mapped_row[mapped.field] = value;
        }
      }
      mapped_data.push(mapped_row);
    }

    return mapped_data;
  }

  cellUpdated(cell) {
    cell.getElement().style["background-color"] = "rgb(93, 239, 236)";
    cell.edited = true;
  }

  addButtonAction() {
    if (this.table.options.update_path === null) {
      this.table.editButton.classList.toggle("hidden");
    }
    this.table.editButton.onclick = () => {
      this.editMode();
    };
    this.table.cancelButton.onclick = () => {
      this.readMode();
    };
    this.table.commitButton.onclick = () => {
      this.saveChanges();
    };
  }

  saveChanges() {
    const updated_rows = this.table
      .getRows()
      .filter(
        (row) => row.getCells().filter((cell) => cell._cell.edited).length > 0,
      );
    const data = updated_rows.map((row) => row.getData());

    if (data.length === 0) {
      alert("No changes found");
      return;
    }

    const csv = PapaParse.unparse(data);

    const fd = new FormData();
    fd.append("source[data_category]", this.table.options.data_category);
    fd.append("source[file]", new Blob([csv]), "update.csv");

    fetch(this.table.options.update_path, {
      method: "POST",
      headers: {
        "X-CSRF-Token": getCsrfToken(),
      },
      body: fd,
    })
      .then((response) => {
        if (response.ok) {
          window.location = response.url;
        } else {
          alert("Error occured while submitting changes.");
          console.log("Request: ", response);
        }
      })
      .catch((error) => {
        alert("Error occured while submitting changes.");
        console.log("Submission error:", error);
      });
  }

  editMode() {
    if (window.vault && !window.vault.is_open()) {
      if (
        !confirm(
          "Vault is locked, encrypted data not viewable and can only be completely overwritten.",
        )
      ) {
        return;
      }
    }

    const columns = this.table.getColumns();
    const data_table = this.table.options.data_table;

    for (const column of columns) {
      if (
        data_table[column.getDefinition().title]?.exclude_from_ingress ||
        data_table[this.table.options.mapper.reverseKey(column.getDefinition().title)?.field]
          ?.exclude_from_ingress
      ) {
        column.getDefinition().title = `${column.getDefinition().title} 🔒`;
        column.getDefinition().formatter = (...[cell, ,]) => {
          cell.getElement().classList.add("text-grey-4");
          return cell.getValue();
        };
      } else {
        column.getDefinition().editor = "input";
      }
    }

    this.table.setColumns(columns.map((c) => c.getDefinition()));
    this.table.commitButton.classList.toggle("hidden");
    this.table.cancelButton.classList.toggle("hidden");
    this.table.editButton.classList.toggle("hidden");
  }

  readMode() {
    const columns = this.table.getColumns();
    const data_table = this.table.options.data_table;

    for (const column of columns) {
      // restore old values
      for (const cell of column.getCells()) {
        if (cell._cell.edited) {
          cell.setValue(cell.getOldValue());
        }
      }
      if (data_table[column.getDefinition().field]?.exclude_from_ingress) {
        // remove the lock symbol from the title
        column.getDefinition().title = [...column.getDefinition().title]
          .slice(0, -2)
          .join("");
        column.getDefinition().formatter = (...[cell, ,]) => {
          cell.getElement().classList.remove("text-grey-4");
          return cell.getValue();
        };
      } else {
        column.getDefinition().editor = undefined;
      }
    }

    this.table.setColumns(columns.map((c) => c.getDefinition()));
    this.table.cancelButton.classList.toggle("hidden");
    this.table.commitButton.classList.toggle("hidden");
    this.table.editButton.classList.toggle("hidden");
  }

  initializeColumn() {
    if (this.table.getColumns().length === 0) {
      return;
    }

    const active_selection = {};

    if (this.table.column_filter) {
      for (const option of this.table.column_filter.options) {
        active_selection[option.innerText] = option.selected;
      }

      if (this.table.column_filter.parentElement) {
        this.table.column_filter.parentElement.removeChild(
          this.table.column_filter,
        );
      }
    }

    const column_select = document.createElement("select");
    column_select.multiple = true;

    for (const column of this.table.getColumns()) {
      // we try to be faster than the onclick event, that triggers the sort
      // to change the sort mode based on the data in the column
      // I found no better way to do this...
      const mapped = this.table.options.mapper.reverseKey(column.getDefinition().field);
      column.getElement().onmousedown = (e) => {
        if (mapped.encrypted || column.getDefinition().field.endsWith("_e")) {
          this.table.options.sortMode = "local"
          this.table.module("filter").filterList = this.table.filter.local_filter;
        } else {
          this.table.options.sortMode = "remote"
        }
      }
      // encrypted columns are too big with the 'fitData' layout
      // there seems to be no option to set this globally, so
      // we do it here on each column, makes the table look way better
      if (
        column._column.field.endsWith("_p") ||
        column._column.field.endsWith("_e") ||
        column._column.field.includes("photo")
      ) {
        column._column.setMaxWidth(200);
      }

      const option = new Option();
      option.innerText = column.getDefinition().field;

      if (option.innerText in active_selection) {
        option.selected = active_selection[option.innerText];
      } else {
        option.selected = true;
      }

      option.column = column;
      column_select.add(option);
    }

    column_select.onchange = () => {
      for (const option of column_select.options) {
        if (option.selected ^ option.column.isVisible()) {
          option.column.toggle();
          this.table.redraw();
        }
      }
    };

    this.table.control_element.after(column_select);
    this.table.column_filter = column_select;

    if (this.table.tom) {
      this.table.tom.destroy();
    }

    this.table.tom = new TomSelect(column_select, {
      plugins: {
        checkbox_options: {
          checkedClassNames: ["ts-checked"],
          uncheckedClassNames: ["ts-unchecked"],
        },
        remove_button: {
          title: "Don't show this column",
        },
      },
    });

    column_select.onchange();
    this.table.tom.control.classList.add("!hidden");

    this.table.headerIcon.onclick = () => {
      this.table.tom.control.click();
    };

    this.table.filter = new Filter(
      this.table,
      this.table.columnSelect,
      this.table.filterSelect,
      this.table.filterInput,
      this.table.filterCount,
      this.table.filterList,
      this.table.addFilterButton,
      this.table.clearFilterButton,
      this.table.getColumns().map((c) => {
        return c.getField();
      }),
    );
  }
}

// ########### URL Generator ###########
// we overwrite the default url generator to build correct array parameters for rails
function generateParamsList(data, prefix) {
  let output = [];

  prefix = prefix || "";

  if (Array.isArray(data)) {
    data.forEach((item, i) => {
      output = output.concat(
        generateParamsList(item, prefix ? `${prefix}[]` : i),
      );
    });
  } else if (typeof data === "object") {
    for (const key in data) {
      output = output.concat(
        generateParamsList(data[key], prefix ? `${prefix}[${key}]` : key),
      );
    }
  } else {
    output.push({ key: prefix, value: data });
  }

  return output;
}

function serializeParams(params) {
  const output = generateParamsList(params);
  const encoded = [];

  for (const item of output) {
    encoded.push(
      `${encodeURIComponent(item.key)}=${encodeURIComponent(item.value)}`,
    );
  }

  return encoded.join("&");
}

function transformSort(params) {
  const sort = structuredClone(params.sort);
  delete params.sort
  params.s = []
  for (const obj of sort) {
    const mapping = this.options.mapper.reverseKey(obj.field);
    params.s.push(`${mapping.field} ${obj.dir}`)
  }

  return params
}

function generateUrl(url, config, params) {
  if (url) {
    if (params && Object.keys(params).length) {
      if (params.sort) {
        params = transformSort.bind(this)(params)
      }
      if (!config.method || config.method.toLowerCase() === "get") {
        config.method = "get";

        url += (url.includes("?") ? "&" : "?") + serializeParams(params);
      }
    }
  }

  return url;
}

// ########### URL Generator ###########

// improve image formatter to ignore undefined values
Tabulator.extendModule("format", "formatters", {
  image: (cell, formatterParams, onRendered) => {
    const el = document.createElement("button");
    el.style = "border: 1px solid;padding: 2px;border-radius: 5px;"
    el.innerText = "Load Image";

    el.onclick = async (event) => {
      // create button to load image
      let src = cell.getValue();

      if (src === undefined) {
        console.log("Undefined Source!")
        event.target.replaceWith(document.createElement("div"));
        return;
      }

      src = await window.vault.decryptMessage(await(await fetch(src)).text());

      if (formatterParams.urlPrefix) {
        src = formatterParams.urlPrefix + src;
      }

      if (formatterParams.urlSuffix) {
        src = src + formatterParams.urlSuffix;
      }

      let el = null;
      if (src.startsWith("data:application/pdf;")) {
        console.log("PDF detected")
        el = document.createElement("a");
        el.setAttribute("href", src);
        el.setAttribute("target", "_blank");
        el.classList.add("text-primary", "hover:underline");
        el.innerText = "Open PDF"
      } else {
        console.log("Probably IMAGE detected")
        el = document.createElement("img");
        el.setAttribute("src", src);

        el.onclick = (e) => {
          const preview = document.getElementById("image_view");
          preview.parentElement.rotation = 0
          preview.innerHTML = "";
          preview.append(e.target.cloneNode());
          window.dispatchEvent(new Event("open-modal"))
        }
      }

      switch (typeof formatterParams.height) {
        case "number":
          el.style.height = `${formatterParams.height}px`;
          break;

        case "string":
          el.style.height = formatterParams.height;
          break;
      }

      switch (typeof formatterParams.width) {
        case "number":
          el.style.width = `${formatterParams.width}px`;
          break;

        case "string":
          el.style.width = formatterParams.width;
          break;
      }

      el.addEventListener("load", () => {
        cell.getRow().normalizeHeight();
      });

      event.target.replaceWith(el);
    };

    return el;
  },
});

export default class extends Controller {
  static targets = [
    "filterSelect",
    "table",
    "control",
    "editButton",
    "cancelButton",
    "commitButton",
    "headerIcon",
    "columnSelect",
    "filterSelect",
    "filterInput",
    "filterCount",
    "filterList",
    "clearFilter",
    "addFilter",
    "imageToggle",
  ];

  static values = {
    options: Object,
  };

  connect() {
    Tabulator.registerModule(SelectableColumnsModule);

    const options = JSON.parse(this.element.dataset.tabulatorOptions);
    options.mapper = new Mapper(options.mapper, options.data_category)

    this.table = new Tabulator(this.tableTarget, options);
    this.table.options.ajaxURLGenerator = generateUrl.bind(this.table);

    // TODO: maybe bind the controller to the table and access the targets directly
    this.table.control_element = this.controlTarget;
    this.table.editButton = this.editButtonTarget;
    this.table.cancelButton = this.cancelButtonTarget;
    this.table.commitButton = this.commitButtonTarget;
    this.table.headerIcon = this.headerIconTarget;
    this.table.columnSelect = this.columnSelectTarget;
    this.table.filterSelect = this.filterSelectTarget;
    this.table.filterInput = this.filterInputTarget;
    this.table.filterCount = this.filterCountTarget;
    this.table.filterList = this.filterListTarget;
    this.table.clearFilterButton = this.clearFilterTarget;
    this.table.addFilterButton = this.addFilterTarget;

    console.log(this.table);
    // Optionally, expose the table instance for global access, e.g., for debugging or direct manipulation
    window.myTabulatorInstance = this.table;
  }
}
