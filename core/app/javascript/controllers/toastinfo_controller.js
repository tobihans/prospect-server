import { Controller } from "@hotwired/stimulus";


export default class extends Controller {
  static values = { heading: String, description: String };
  static tooltipWrapper;
  static tooltipHeading;
  static tooltipContent;



  connect() {
    const cssCode = `#tooltipBackground {
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
}

span#closeTooltip {
  position: absolute;
  z-index: 999;
  top: 10px;
  right: 10px;
  cursor: pointer;
  font-weight: bold;
  display: block
}

#tooltipWrapper {
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100vh;
  height: 100svh;
  display: block;
  background-color: rgba(0, 0, 0, 0);
  z-index: -1;
  pointer-events: none;
  transition: all 0.3s ease-in-out;
}

#tooltipWrapper.active {
  background-color: rgba(0, 0, 0, 0.6);
  z-index: 9999;
  pointer-events: all;
}

#tooltipModal {
  margin-bottom: -100vh;
  z-index: 9999;
  background-color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.6);
  pointer-events: none;
  opacity: 0;
  transform: translateY(0);
  transition: transform 0.3s ease-in-out;
  pointer-events: all;
}

#tooltipWrapper.active #tooltipBackground {
  cursor: pointer;
}

#tooltipWrapper.active #tooltipModal {
  opacity: 1;
  display: block;
  position: absolute;
  top: 100%;
  margin: 20px;
  transform: translateY(calc(-100% + -40px));
  right: 0;
  left: 0;
  z-index: 9999;
  background-color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.6);
}

.prospectTooltip {
  cursor: help;
}

#tooltipModal h3 {
  font-weight: bold;
  line-height: 24px;
  font-size: 20px;
  margin-bottom: 10px;
}`;

if (!document.getElementById("tooltipWrapper")) {
  
    const tooltipMarkup = `
      <div id="tooltipWrapper" class="hidden">
        <div id="tooltipBackground"></div>
        <div id="tooltipModal">
          <span id="closeTooltip">&times;</span>
          <h3 id="tooltipHeading"></h3>
          <p id="tooltipContent"></p>
        </div>
      </div>
    `;

    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = tooltipMarkup;
    document.body.prepend(tempDiv.firstElementChild);
    const styles = document.createElement('style');
    styles.id = "toolTipStyles";
    styles.prepend(cssCode);
    document.body.prepend(styles);
    }

    // Bind methods to ensure proper context
    this.showTooltip = this.showTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
    this.handleEscapeKey = this.handleEscapeKey.bind(this);

    // Add event listener for the tooltip click
    this.element.addEventListener("click", this.showTooltip);

    // Close tooltip when pressing the Escape key
    document.addEventListener("keydown", this.handleEscapeKey);

    this.tooltipWrapper = document.getElementById("tooltipWrapper");
    this.tooltipHeading = document.getElementById("tooltipHeading");
    this.tooltipContent = document.getElementById("tooltipContent");
    // Add click event listener for closing the tooltip
    this.tooltipWrapper.addEventListener("click", this.closeTooltip);
  }

  disconnect() {
    console.log("Tooltip controller disconnected");
    const styleElement = document.getElementById("toolTipStyles");
    if (styleElement) {
      styleElement.remove();
    }
    // Remove event listeners
    this.element.removeEventListener("click", this.showTooltip);
    document.removeEventListener("keydown", this.handleEscapeKey);

    // const tooltipWrapper = document.getElementById("tooltipWrapper");
    this.tooltipWrapper.removeEventListener("click", this.closeTooltip);
  }

  showTooltip(event) {
    this.tooltipHeading.innerHTML = this.headingValue;
    this.tooltipContent.innerHTML = this.descriptionValue;
    this.tooltipWrapper.classList.add("active");
  }

  closeTooltip(event) {
    if (
      event.target.id === "tooltipBackground" ||
      event.target.id === "closeTooltip"
    ) {
      this.tooltipWrapper.classList.remove("active");
    }
  }

  handleEscapeKey(event) {
    if (event.key === "Escape") {
      this.closeTooltip(event);
    }
  }
}
