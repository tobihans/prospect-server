// Inspired from https://github.com/stimulus-components/stimulus-checkbox-select-all/
// to allow grouped checkboxes: show the select all button in tabbar, and select a certain checkbox_group

import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['stepOverlay']

  initialize() {
    this.index = 0;
    this.showCurrent()
  }

  next() {
    this.index++;
    this.showCurrent()
  }

  showCurrent() {
    this.stepOverlayTargets.forEach((element, i) => {
      element.hidden = (i === this.index)
    })
  }

}
