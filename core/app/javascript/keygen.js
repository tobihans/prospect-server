class KeyGen {
	publicKey
	wrappedKey
	salt
	iv

	constructor() {
		this.createKeyButton = document.querySelector("#crypt-create-key");
		this.passwordField = document.querySelector("#crypt-password");
		this.passwordConfirmationField = document.querySelector("#crypt-password-confirmation");
		this.exportKeyOutput = document.querySelector("#crypt-public-key");
		this.wrappedKeyOutput = document.querySelector("#crypt-wrapped-key");
		this.ivOutput = document.querySelector("#crypt-iv");
		this.saltOutput = document.querySelector("#crypt-salt");
		this.cryptoForm = document.querySelector("#crypt-form");

		const check = [this.createKeyButton, this.passwordField, this.passwordConfirmationField, this.exportKeyOutput, this.wrappedKeyOutput, this.ivOutput, this.saltOutput, this.cryptoForm].some((ele) => {
			ele === null
		});

		if (check){
			alert("Could not find all required fields for KeyGen script!")
		}

		document.addEventListener('hibpCheck', async (e) => {
			e.stopImmediatePropagation();
			if (e.detail) {
				alert('The pasword is not secure enough and was found in a database of leaked passwords. Please choose a different password.');
				this.passwordField.value = "";
			} else {
				await this.generateKeyPair();
				this.fillForm();
			}
		});

		if (this.createKeyButton && this.cryptoForm) {
			const result = this.createKeyButton.addEventListener("click", () => {
				this.checkPassword();
			});

			this.cryptoForm.addEventListener("submit", (event) => {
				this.passwordField.value = "";
			});
		}
	}

	fillForm(){
		this.exportKeyOutput.value = this.publicKey;
		this.wrappedKeyOutput.value = this.wrappedKey;
		this.ivOutput.value = window.btoa(this.ab2str(this.iv));
		this.saltOutput.value = window.btoa(this.ab2str(this.salt));
	}

	async checkPassword() {
		if (this.passwordField.value !== this.passwordConfirmationField.value) {
			alert(
				"Password does not match password confirmation.",
			);
			return;
		}

		if (this.passwordField.value.length < 8) {
			alert(
				"Please enter a password with at least 8 characters to encrypt the data.",
			);
			return;
		}
		//check if password is secure
		await this.hibpCheck(this.passwordField.value);
	}

	async sha1(source) {
		const sourceBytes = new TextEncoder().encode(source);
		const digest = await crypto.subtle.digest("SHA-1", sourceBytes);
		const resultBytes = [...new Uint8Array(digest)];
		return resultBytes.map(x => x.toString(16).padStart(2, '0')).join("");
	}

	async hibpCheck(pwd) {
		// We hash the pwd first
		const hash = await this.sha1(pwd);
		// We send the first 5 chars of the hash to hibp's API
		const req = new XMLHttpRequest();
		req.addEventListener("load", function () {
			// When we get back a response from the server
			// We create an array of lines and loop through them
			const resp = this.responseText.split("\n");
			const hashSub = hash.slice(5).toUpperCase();
			let result = false;
			for (const index in resp) {
				// Check if the line matches the rest of the hash
				if (resp[index].substring(0, 35) === hashSub) {
					result = true;
					break; // If found no need to continue the loop
				}
			}
			// Trigger an event with the result
			const event = new CustomEvent("hibpCheck", { detail: result });
			document.dispatchEvent(event);
		});
		req.open(
			"GET",
			`https://api.pwnedpasswords.com/range/${hash.substring(0, 5)}`,
		);
		req.send();
	}

	ab2str(buf) {
		return String.fromCharCode.apply(null, new Uint8Array(buf));
	}

	async exportPublicKey(key) {
		const exported = await window.crypto.subtle.exportKey("spki", key);
		const exportedAsString = this.ab2str(exported);
		this.publicKey = window.btoa(exportedAsString);
	}

	exportWrappedKey(key) {
		const exportedAsString = this.ab2str(key);
		this.wrappedKey = window.btoa(exportedAsString)
	}

	async generateKeyPair() {
		const gkeyPair = await window.crypto.subtle
			.generateKey(
				{
					name: "RSA-OAEP",
					// Consider using a 4096-bit key for systems that require long-term security
					modulusLength: 2048,
					publicExponent: new Uint8Array([1, 0, 1]),
					hash: "SHA-256",
				},
				true,
				["encrypt", "decrypt"],
			)

		await this.exportPublicKey(gkeyPair.publicKey);
		await this.wrapCryptoKey(gkeyPair.privateKey);
	}

	async getKeyMaterial() {
		// TODO: create password field
		const password = this.passwordField.value;
		const enc = new TextEncoder();
		return window.crypto.subtle.importKey(
			"raw",
			enc.encode(password),
			{ name: "PBKDF2" },
			false,
			["deriveBits", "deriveKey"],
		);
	}

	async getKey(keyMaterial, salt) {
		return window.crypto.subtle.deriveKey(
			{
				name: "PBKDF2",
				salt: salt,
				iterations: 100000,
				hash: "SHA-256",
			},
			keyMaterial,
			{ name: "AES-CBC", length: 256 },
			true,
			["wrapKey", "unwrapKey"],
		);
	}

	async wrapCryptoKey(keyToWrap) {
		// get the key encryption key
		const keyMaterial = await this.getKeyMaterial();
		this.salt = window.crypto.getRandomValues(new Uint8Array(16));
		const wrappingKey = await this.getKey(keyMaterial, this.salt);
		this.iv = window.crypto.getRandomValues(new Uint8Array(16));
		const wrapped = await window.crypto.subtle.wrapKey(
			"pkcs8",
			keyToWrap,
			wrappingKey,
			{
				name: "AES-CBC",
				iv: this.iv,
			},
		);

		const wrappedKeyBuffer = new Uint8Array(wrapped);

		this.exportWrappedKey(wrappedKeyBuffer);

		alert(
			"The key was generated - please note down your chosen password. You will need it to decrypt the data later. You can now press SAVE",
		);
		this.createKeyButton.className =
			"w-full py-2 px-4 font-bold uppercase text-grey-1 hover:text-white hover:bg-primary/70 bg-gradient-to-r hover:animate-pulse from-shade-3 to-shade-1 rounded-b-lg";
		this.createKeyButton.value = "Recreate Keypair";
	}
}

window.KeyGen = KeyGen
