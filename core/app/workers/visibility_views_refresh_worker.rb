# frozen_string_literal: true

class VisibilityViewsRefreshWorker < BaseFaktoryWorker
  faktory_options retry: 2, queue: "no_concurrency"

  def perform(organization_id)
    Visibility.full_views_refresh!(organization_id:)
  end
end
