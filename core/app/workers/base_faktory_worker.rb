# frozen_string_literal: true

class BaseFaktoryWorker
  include Faktory::Job
  include SemanticLogger::Loggable

  # see https://github.com/contribsys/faktory/wiki/The-Job-Payload
  faktory_options reserve_for: 5.hours.to_i

  # We can use the following queues
  #   name            service     process   description
  #   -----------------------------------------------------------------------------------------------------------------
  #   etl             dataworker  separate  ETL and processing jobs for dataworker
  #   default         core        shared    for normal jobs send directly to faktory
  #   active_jobs     core        shared    default queue for jobs send via Rails standard ActiveJob (mailers, etc)
  #   grafana-preview core        shared    updating previews of dashboards, low prio
  #   imports         core        shared    running imports and fetching data
  #   urgent_quick    core        separate  exclusively for urgent jobs, which should not precess for long!
  #   no_concurrency  core        separate  exclusively for processes which should run concurrently, basically a simple queue
  #
end
