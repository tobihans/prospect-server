# frozen_string_literal: true

class ImportWorker < BaseFaktoryWorker
  faktory_options retry: 2, queue: "imports"

  def perform(args)
    raise "No source_id specified, can't start job." unless args["source_id"]

    source = Source.find args["source_id"]

    # Determine whether to use the database import hint (default: true)
    use_db_import_hint = args.fetch("use_db_import_hint", true)

    # Set the import hint only if `use_db_import_hint` is false
    import_hint = use_db_import_hint ? nil : args["import_hint"]

    source.trigger_import!(nil, use_db_import_hint:, import_hint:)
  end
end
