# frozen_string_literal: true

class GrafanaPreviewWorker < BaseFaktoryWorker
  faktory_options retry: -1, queue: "grafana-preview"

  def perform(org_id, url, uid)
    org = Organization.find org_id
    image = Grafana::DashboardPreview.create_grafana_dashboard_preview(url, org.grafana_org_id)
    return if image.nil?

    Grafana::DashboardPreview.save_preview(image, uid, org.grafana_org_id)
  end
end
