# frozen_string_literal: true

class FailUnfinishedImportsWorker < BaseFaktoryWorker
  faktory_options retry: -1

  ERROR_MSG = "Stale import which never managed to finish"

  def perform(limited = true) # rubocop:disable Style/OptionalBooleanParameter (Faktory Jobs only know positional args)
    q = Import.unfinished.where("created_at < ?", 2.days.ago)
    q = q.order(id: :desc).limit(1_000_000) if limited
    q.update_all error: ERROR_MSG # rubocop:disable Rails/SkipsModelValidations
  end
end
