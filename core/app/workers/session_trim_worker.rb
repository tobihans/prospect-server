# frozen_string_literal: true

class SessionTrimWorker < BaseFaktoryWorker
  def perform
    Rails.application.load_tasks
    Rake::Task["db:sessions:trim"].invoke
  end
end
