# frozen_string_literal: true

class DefaultDashboardUpdateWorker < BaseFaktoryWorker
  faktory_options retry: -1

  def perform
    Organization.find_each do |org|
      Grafana::RemoteControl.default_dashboards_for_organization!(org)
    rescue
      Rails.logger.error "Error while updating default dashboards for: #{org.name} (#{org.id})"
    end
  end
end
