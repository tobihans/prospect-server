# frozen_string_literal: true

class PullBlobWorker < BaseFaktoryWorker
  def perform(url, blob_id)
    blob = EncryptedBlob.find(blob_id)

    response = HTTParty.get(url)

    if response.ok?
      blob.encrypt_attach_blob! response.body, content_type: response.content_type
    else
      blob.update! url_e: RsaOaepEncrypt.encrypt(url, blob.organization), error: "Received Error Code: #{response.code} while fetching data"
    end
  rescue => e
    blob.update! url_e: RsaOaepEncrypt.encrypt(url, blob.organization), error: e.to_s
  end
end
