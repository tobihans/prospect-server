# frozen_string_literal: true

class IxoCertifyWorker < BaseFaktoryWorker
  faktory_options retry: -1

  def perform(certify = false, reject = false, collection_id:) # rubocop:disable Style/OptionalBooleanParameter
    Ixo::Certifier.new(dry_run: !certify, reject:, collection_id:)
  end
end
