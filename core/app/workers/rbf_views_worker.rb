# frozen_string_literal: true

class RbfViewsWorker < BaseFaktoryWorker
  faktory_options retry: 2

  def perform
    RbfClaimTemplate.full_views_refresh!
  end
end
