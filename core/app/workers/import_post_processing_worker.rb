# frozen_string_literal: true

class ImportPostProcessingWorker < BaseFaktoryWorker
  #
  # This worker polls if an import has finished processing,
  # then triggers an import scoped dataworker processing job
  #
  POLLING_DELAY = 5.seconds
  MAX_POLLING_COUNT = 50
  RBF_POST_PROCESSING_JOB = "rbf_easp_is_eligible_per_import"

  faktory_options retry: 2, queue: "urgent_quick"

  def perform(import_id, processing_job, polling_count)
    import = Import.find import_id

    if import.processing_finished?
      FaktoryPusher.push processing_job, [import_id]
    elsif polling_count >= MAX_POLLING_COUNT
      raise "MAX_POLLING_COUNT reached for Import #{import_id} with Job #{processing_job}"
    else
      next_polling_count = polling_count + 1
      delay = next_polling_count * POLLING_DELAY
      ImportPostProcessingWorker.perform_in delay, import_id, processing_job, next_polling_count
    end
  end

  def self.enqueue_for_rbf_import(import)
    enqueue_for_import import, RBF_POST_PROCESSING_JOB
  end

  def self.enqueue_for_import(import, processing_job)
    perform_in POLLING_DELAY, import.id, processing_job, 0
  end
end
