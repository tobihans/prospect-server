# frozen_string_literal: true

ActiveAdmin.register Source do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 1, parent: "Import"

  # For security, limit the actions that should be available
  actions :all, except: []

  action_item(:view_in_app, only: :show) { link_to "View in App", resource, class: "action-item-button" }

  # Add or remove filters to toggle their visibility
  filter :id
  filter :name
  filter :kind, as: :select
  filter :data_category, as: :select
  filter :organization, collection: proc { Organization.default_order }
  filter :user
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visibility in the index action
  index do
    selectable_column
    id_column
    column :name
    column :kind
    column :data_category
    column :project
    column :organization
    column("Last Import") do |s|
      last_imp = s.imports.last
      if last_imp
        link_to last_imp.state, last_imp
      else
        "-"
      end
    end
    column :created_at
    actions
  end

  # Add or remove rows to toggle their visibility in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :kind
      row :data_category
      row :project
      row :organization
      row :user
      row :created_at
      row :updated_at
    end

    panel "Last 200 Imports" do
      table_for(resource.imports.order(id: :desc).limit(200)) do
        column :state do |i|
          link_to i.state, i
        end
        column :no_error?
        column :created_at
        column :updated_at
        column :last_imported_hint
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :name
    end
    f.actions
  end
end
