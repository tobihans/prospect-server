# frozen_string_literal: true

ActiveAdmin.register Organization do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 1, parent: "Members"

  # For security, limit the actions that should be available
  actions :all, except: []

  action_item(:view_in_app, only: :show) { link_to "View in App", resource, class: "action-item-button" }

  # Add or remove filters to toggle their visibility
  filter :id
  filter :name
  filter :description
  filter :organization_groups, as: :select, collection: proc { OrganizationGroup.default_order }
  filter :rbf_claim_templates
  filter :grafana_org_id
  filter :user
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visibility in the index action
  index do
    selectable_column
    id_column
    column :name
    column :grafana_org_id
    column :created_at
    column :updated_at
    actions
  end

  # Add or remove rows to toggle their visibility in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :grafana_org_id
      row(:rbf_claim_templates) { resource.rbf_claim_templates.map(&:to_s).join(", ") }
      row :user
      row :crypto_added_at
      row :created_at
      row :updated_at
    end

    panel "RBF Configs" do
      table_for(resource.rbf_organization_configs) do
        column(:id) { link_to _1.id, prospect_admin_rbf_organization_config_path(_1.id) }
        column :rbf_claim_template
        column :code
        column :total_grant_allocation
      end
    end

    panel "Organization Groups" do
      table_for(resource.organization_group_members) do
        column :organization_group
        column :visible_for_sharing
      end
    end

    panel "Users" do
      table_for(resource.organization_users) do
        column(:user) { _1.user.to_s_email }
        column(:summary) { "#{_1.user.state} / #{_1.privilege_summary}" }
        column :activated_at
        column(:privileges) { _1.privileges.map(&:to_s).join(", ") }
      end
    end

    panel "RBF Products" do
      table_for(resource.rbf_products) do
        column(:id) { link_to _1.id, prospect_admin_rbf_product_path(_1.id) }
        column :manufacturer
        column :model
        column :rbf_category
        column :rbf_claim_template
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :name, hint: "Name of the Organization"
      f.input :create_with_manager_email, as: :email, label: "Manager Email", hint: "If given, will invite the user as manager of the new organization" if f.object.new_record?
    end
    f.inputs "RBf Configs" do
      f.has_many :rbf_organization_configs, heading: false, allow_destroy: true, new_record: "New Rbf Config" do |ogm|
        ogm.input :rbf_claim_template, as: :select, collection: OrganizationGroup.order(:name)
        ogm.input :code, hint: "Internal Identifier of the contract. Used to create codes for claims, should not be changed later"
        ogm.input :total_grant_allocation
      end
    end
    f.inputs "Groups" do
      f.has_many :organization_group_members, heading: false, allow_destroy: true, new_record: "New Group" do |ogm|
        ogm.input :organization_group, label: "Organization Group", as: :select, collection: OrganizationGroup.order(:name)
        ogm.input :visible_for_sharing
      end
    end
    f.actions
  end
end
