# frozen_string_literal: true

ActiveAdmin.register DynamicForm do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 1, parent: "RBF"

  # For security, limit the actions that should be available
  actions :all, except: []

  action_item :rebuild_from_yml, only: :index do
    link_to "Rebuild from YML", rebuild_from_yml_prospect_admin_dynamic_forms_path, method: :post, class: "action-item-button"
  end

  # Add or remove filters to toggle their visibility
  filter :id
  filter :name
  filter :active
  filter :description
  filter :rbf_claim_template
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visiblity in the index action
  index do
    selectable_column
    id_column
    column :name
    column :active
    column :description
    column :rbf_claim_template
    column :updated_at
    actions
  end

  # Add or remove rows to toggle their visiblity in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :active
      row :description
      row :rbf_claim_template
      row :created_at
      row :updated_at
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs do
      f.input :name
      f.input :active
      f.input :description
      f.input :rbf_claim_template
    end
    f.actions
  end

  collection_action :rebuild_from_yml, method: :post do
    Ueccc::FormGenerator.run!
    redirect_to collection_path, notice: "UECCC Forms rebuild from YMLs" # rubocop:disable Rails/I18nLocaleTexts
  end
end
