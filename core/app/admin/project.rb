# frozen_string_literal: true

ActiveAdmin.register Project do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 1, parent: "Import"

  # For security, limit the actions that should be available
  actions :all, except: []

  action_item(:view_in_app, only: :show) { link_to "View in App", resource, class: "action-item-button" }

  filter :id
  filter :name
  filter :description
  filter :organization, as: :select, collection: proc { Organization.default_order }
  filter :user
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :name
    column :organization
    column :description
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :description
      row :organization
      row :user
      row :created_at
      row :updated_at
    end

    panel "Sources" do
      table_for(resource.sources.order(:name)) do
        column(:name) { link_to _1, prospect_admin_source_path(_1) }
        column :kind
        column :activated_at
        column("Last Import") do |s|
          last_imp = s.imports.last
          if last_imp
            link_to last_imp.state, last_imp
          else
            "-"
          end
        end
        column("Last 10 Imports") do |s|
          code do
            s.imports.order(id: :desc).last(10).map do |i|
              link_to i.state_symbol, import_path(i)
            end.join(" ").html_safe # rubocop:disable Rails/OutputSafety:
          end
        end
      end
    end

    panel "Visibilities" do
      resource.visibilities.each do |v|
        attributes_table_for(v) do
          row :organization
          row :default?
          row(:summary, &:to_s_columns)
          row :data_from
          row :data_until
          row :conditions
          row :license
          row :resharing
          row :created_at
          row :user
          row :columns
        end
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :name
      f.input :description
    end
    f.actions
  end
end
