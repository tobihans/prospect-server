# frozen_string_literal: true

ActiveAdmin.register RbfOrganizationConfig do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 3, parent: "RBF"

  # For security, limit the actions that should be available
  actions :all, except: [:new]

  filter :id
  filter :organization, collection: proc { Organization.default_order }
  filter :rbf_claim_template, as: :select, collection: proc { RbfClaimTemplate.default_order }
  filter :code
  filter :total_grant_allocation
  filter :user
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :organization
    column :rbf_claim_template
    column :code
    column :total_grant_allocation
    actions
  end

  show do
    attributes_table_for(resource) do
      row :id
      row :organization
      row :rbf_claim_template
      row :code
      row :total_grant_allocation
      row :user
      row :created_at
      row :updated_at
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :organization, as: :select, collection: Organization.default_order, input_html: { disabled: true }
      f.input :rbf_claim_template, as: :select, collection: RbfClaimTemplate.default_order, input_html: { disabled: true }
      f.input :code
      f.input :total_grant_allocation
    end
    f.actions
  end
end
