# frozen_string_literal: true

ActiveAdmin.register_page "Misc" do
  menu priority: 1000

  content do
    panel "Org Device Mapper" do
      div do
        span "Upload a csv file with the columns:"
        pre "device_id, device_type, organization_name"
        br
        span "Allowed values for device_type are:"
        pre OrgDeviceMapper::DEVICE_TYPES.join(", ")
        br
        span "Make sure to have the headers in the first row."
        br
        br
        span do
          link_to "Download csv of all existing org_device_mappers", prospect_admin_misc_org_device_mapper_csv_path
        end
        br
        br

        active_admin_form_for :file, url: prospect_admin_misc_org_device_mapper_upload_path, method: :post do |f|
          f.inputs do
            f.input :file, as: :file, required: true
          end
          f.submit
        end
      end
    end

    panel "Verasol Data" do
      div do
        span "Please upload the archive 'data.csv'"
        br
        span "You can get it from"
        span do
          link_to "verasol", "https://data.verasol.org/download/all/sek/file?verified=true&sortBy=date-product-listing-was-last-updated-mm-dd-yyyy_desc&"
        end

        active_admin_form_for :file, url: prospect_admin_misc_verasol_upload_path, method: :post do |f|
          f.inputs do
            f.input :file, as: :file, required: true
          end
          f.submit
        end
      end
    end
  end

  page_action :org_device_mapper_csv, method: :get do
    filename = "prospect_org_device_mapper_#{Time.zone.now.iso8601.gsub(/\W/, '-')}.csv"
    send_data(OrgDeviceMapper.to_csv, filename:)
  end

  page_action :org_device_mapper_upload, method: :post do
    uploaded_file = params.dig(:file, :file)
    if uploaded_file.present?
      @import_log = OrgDeviceMapper.import_csv! uploaded_file.read
      render "import_log", layout: "active_admin", notice: "File imported"
    else
      flash[:alert] = "Please select a file" # rubocop:disable Rails/I18nLocaleTexts
      redirect_to action: :index
    end
  end

  page_action :verasol_upload, method: :post do
    uploaded_file = params.dig(:file, :file)
    if uploaded_file.present?
      @import_log = VerasolProductImporter.import uploaded_file.read
      render "import_log", layout: "active_admin", notice: "File imported"
    else
      flash[:alert] = "Please select a file" # rubocop:disable Rails/I18nLocaleTexts
      redirect_to action: :index
    end
  end
end
