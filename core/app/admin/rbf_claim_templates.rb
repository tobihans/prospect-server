# frozen_string_literal: true

ActiveAdmin.register RbfClaimTemplate do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 1, parent: "RBF"

  # For security, limit the actions that should be available
  actions :all, except: []

  action_item :reevaluate_ineligible, only: :index do
    link_to "Re-evaluate all ineligible", reevaluate_ineligible_prospect_admin_rbf_claim_templates_path, method: :post, class: "action-item-button"
  end

  # Add or remove filters to toggle their visibility
  filter :id
  filter :organization_group, as: :select, collection: proc { OrganizationGroup.default_order }
  filter :supervising_organization_id
  filter :managing_organization_id
  filter :verifying_organization_id
  filter :name
  filter :user
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visibility in the index action
  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end

  # Add or remove rows to toggle their visibility in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :trust_trace_check
      row :organization_group
      row :supervising_organization
      row :managing_organization
      row :verifying_organization
      row :user
      row :created_at
      row :updated_at
    end

    panel "Dynamic Forms" do
      resource.dynamic_forms.each do |df|
        attributes_table_for(df) do
          row :name
          row :description
          row :active
        end
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :name
      f.input :trust_trace_check
      f.input :currency
      f.input :organization_group
      f.input :supervising_organization
      f.input :managing_organization
      f.input :verifying_organization
    end
    f.actions
  end

  collection_action :reevaluate_ineligible, method: :post do
    FaktoryPusher.push "rbf_easp_ogs_is_eligible"
    FaktoryPusher.push "rbf_easp_ccs_is_eligible"
    FaktoryPusher.push "rbf_easp_pue_is_eligible"
    redirect_to collection_path, notice: "Ineligible Sales will be reevaluated in the background" # rubocop:disable Rails/I18nLocaleTexts
  end
end
