# frozen_string_literal: true

ActiveAdmin.register OrganizationGroup do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 2, parent: "Members"

  # For security, limit the actions that should be available
  actions :all, except: []

  # Add or remove filters to toggle their visibility
  filter :id
  filter :name
  filter :user
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visibility in the index action
  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end

  # Add or remove rows to toggle their visibility in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :name
      row :user
      row :created_at
      row :updated_at
    end

    resource.organization_group_members.each do |ogm|
      attributes_table_for(ogm) do
        row :organization
        row :visible_for_sharing
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)
    f.inputs "Details" do
      f.input :name
    end

    f.inputs "Members" do
      f.has_many :organization_group_members, heading: false, allow_destroy: true, new_record: true do |ogm|
        ogm.input :organization_id, label: "Organization", as: :select, collection: Organization.order(:name).pluck(:name, :id)
        # check if organization has key specified or sharing would not work
        ogm.input :visible_for_sharing
      end
    end
    f.actions
  end
end
