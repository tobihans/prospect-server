# frozen_string_literal: true

ActiveAdmin.register RbfProduct do
  controller do
    def permitted_params
      params.permit! # we are all only responsible admins here
    end
  end

  menu priority: 3, parent: "RBF"

  # For security, limit the actions that should be available
  actions :all, except: []

  # Add or remove filters to toggle their visibility
  filter :id
  filter :organization, collection: proc { Organization.default_order }
  filter :rbf_claim_template
  filter :manufacturer
  filter :model
  filter :rbf_category
  filter :technology
  filter :rated_power_w
  filter :user
  filter :created_at
  filter :updated_at

  # Add or remove columns to toggle their visibility in the index action
  index do
    selectable_column
    id_column
    column :organization
    column :manufacturer
    column :model
    column :rbf_category
    column :rated_power_w
    column :rbf_claim_template
    column :updated_at
    actions
  end

  # Add or remove rows to toggle their visibility in the show action
  show do
    attributes_table_for(resource) do
      row :id
      row :organization
      row :manufacturer
      row :model
      row :rbf_category
      row :technology
      row :rated_power_w
      row :rbf_claim_template
      row :user
      row :created_at
      row :updated_at
    end

    panel "Prices" do
      table_for(resource.rbf_product_prices.default_order) do
        column(:id)
        column :category
        column :valid_from
        column :original_sales_price
        column :base_subsidized_sales_price
        column :base_subsidy_amount
        column("Subsidy %") { ActiveSupport::NumberHelper.number_to_percentage _1.subsidy_percentage, precision: 1, strip_insignificant_zeros: true }
        column :sums_up?
      end
    end
  end

  # Add or remove fields to toggle their visibility in the form
  form do |f|
    f.semantic_errors(*f.object.errors.attribute_names)

    f.inputs "Details" do
      f.input :organization, as: :select, collection: Organization.default_order
      f.input :manufacturer
      f.input :model
      f.input :rbf_category, hint: RbfClaimTemplate.valid_product_categories_hint
      f.input :technology
      f.input :rated_power_w
      f.input :rbf_claim_template
    end

    f.inputs "Prices" do
      f.has_many :rbf_product_prices, heading: false, allow_destroy: true, new_record: true do |rpp|
        rpp.input :category, as: :select, collection: RbfProductPrice::CATEGORIES
        rpp.input :original_sales_price, hint: "Price without the RBF programme."
        rpp.input :base_subsidized_sales_price, hint: "Price customer actually normally has to pay. Base for RBF Specific special calculations."
        rpp.input :base_subsidy_amount, hint: "The subsidy of a sale with this price. Base for RBF Specific special calculations."
        rpp.input :valid_from
      end
    end

    f.actions
  end
end
