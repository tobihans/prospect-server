# frozen_string_literal: true

ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    res = Source.active.includes(:organization, :project).active.map do |s|
      imps = s.imports.order(id: :desc).first(10)
      last_i = imps.first
      imps_wo_last = imps.without(last_i)

      looks_good =
        imps.empty? ||
        (imps.none?(&:failed?) && imps_wo_last.all?(&:processing_finished_at))

      t = Time.zone.now
      last_x_ages_seconds = imps.map { t - _1.created_at }.map(&:round)

      last_x_counts = imps.map do |i|
        {
          i: i.rows_inserted,
          u: i.rows_updated,
          d: i.rows_duplicated,
          f: i.rows_failed
        }
      end

      old = last_i && (last_i.created_at < 35.days.ago)

      last_x_all_failed = imps.present? && imps.all?(&:failed?)
      last_x_any_failed = imps.present? && imps.any?(&:failed?)
      unused = imps.empty?

      cat, urgency = if old
                       [:old, 0]
                     elsif unused
                       [:unused, 1]
                     elsif looks_good
                       [:good, 10]
                     elsif last_x_all_failed
                       [:all_failed, 100]
                     elsif last_x_any_failed
                       [:some_failed, 80]
                     else
                       [:other, 20]
                     end

      {
        name: s.name,
        source: s,
        organization: s.organization,
        kind: s.kind,
        project: s.project,
        last_import: last_i.present?,
        last_import_at: last_i&.created_at,
        last_import_state: last_i&.state,
        last_import_failed: last_i&.failed?,
        last_x_all_failed:,
        last_x_any_failed:,
        last_x_failed: imps.map(&:failed?),
        last_x_states: imps.map { _1.state.split("_").map(&:first).join },
        last_x_ages_seconds:,
        last_x_ages_hours: last_x_ages_seconds.map { _1 / 1.hour },
        last_x_ages_days: last_x_ages_seconds.map { _1 / 1.day },
        last_x_counts:,
        previous_x_all_finished: imps_wo_last.all?(&:processing_finished_at),
        previous_x_finished: imps_wo_last.map { _1.processing_finished_at.present? },
        looks_good:,
        old:,
        urgency:,
        cat:,
        imps:,
        unused:
      }
    end

    grouped = res.sort_by { _1[:urgency] }.reverse.group_by { _1[:cat] }

    kinds = res.pluck(:kind)

    very_bad_kinds = kinds.select { |k| res.select { _1[:kind] == k }.all? { _1[:last_x_all_failed] } }.map do |k|
      last_succ = Import.includes(:source).where(sources: { kind: k }).order(id: :desc).find_by(error: nil)
      { kind: k, imp: last_succ }
    end

    panel "Source Kinds failing everywhere" do
      table_for very_bad_kinds do
        column(:kind) { link_to _1[:kind], prospect_admin_sources_path(q: { kind_eq: _1[:kind] }) }
        column("Last Success At") { _1[:imp] ? link_to(_1[:imp].created_at, _1[:imp]) : "-" }
        column("Last Success Source") { _1[:imp] ? link_to(_1[:imp].source, prospect_admin_source_path(_1[:imp].source)) : "-" }
      end
    end

    grouped.each do |label, data|
      panel label.to_s.titleize do
        table_for data do
          column(:kind) { link_to _1[:kind], prospect_admin_sources_path(q: { kind_eq: _1[:kind] }) }
          column(:org) { link_to _1[:organization].to_s.truncate(20), prospect_admin_organization_path(_1[:organization].id) }
          column(:project) { link_to _1[:project].to_s.truncate(20), prospect_admin_project_path(_1[:project].id) }
          column(:name) { link_to _1[:name].truncate(20), prospect_admin_source_path(_1[:source].id) }
          column(:last_imps) do |s|
            code do
              table(style: "font-size: 9px; line-height: 10px") do
                td_style = { style: "width: 80px; padding: 0;" }
                tr do
                  th(style: "width: 50px;") { "STA" }
                  s[:last_x_states].each_with_index do |x, i|
                    td(**td_style) do
                      link_to x, s[:imps][i]
                    end
                  end
                end
                tr do
                  th { "ERR" }
                  s[:last_x_failed].each do |x|
                    td(**td_style) { x ? "X" : "-" }
                  end
                end
                tr do
                  th { "AGE" }
                  s[:last_x_ages_days].each do |x|
                    td(**td_style) { x }
                  end
                end
                %i[i u d f].each do |x|
                  tr do
                    th { x }
                    s[:last_x_counts].each do |y|
                      td(**td_style) { text_node y[x] || "-" }
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
