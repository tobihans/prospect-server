# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    class Sources < Grape::API
      use Rack::JSONP

      include Api::Defaults

      resource :out do
        resources :sources do
          route_param :id do
            # GET source/:id
            desc "Data source" do
              detail "This endpoint returns info about an individual data source"
              success [{ code: 200,
                        message: # rubocop:disable Layout/HashAlignment
                          'Sample JSON Response:
                            {
                              "id": 1,
                              "created_at": "2024-10-02T15:20:40.334Z",
                              "details": {
                                "file_format": "csv"
                              },
                              "kind": "manual_upload",
                              "name": "My Name",
                              "project_id": 1
                      }' }]
              headers Authorization: {
                description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                required: true
              }
              failure [[401, "Unauthorized"]]
              hidden false
              deprecated false
              is_array false
              produces ["application/json"]
            end

            get do
              authenticate_org
              source = Source.find(params[:id])
              authorize_org source
              present source, with: Entities::Source
            end
          end
        end

        # GET sources
        resources :sources do
          desc "Data sources for organization" do
            detail "This endpoint returns all data sources for an organization as an array"
            success [{ code: 200,
                       message:
                         'Sample JSON Response:
                         {
                           [
                              "id": 1,
                              "created_at": "2024-10-02T15:20:40.334Z",
                              "details": {
                                "file_format": "csv"
                              },
                              "kind": "manual_upload",
                              "name": "My Name",
                              "project_id": 1
                            ]
                         }' }]
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            failure [[401, "Unauthorized"]]
            hidden false
            deprecated false
            is_array true
            produces ["application/json"]
          end

          params do
            use :pagination
          end

          get do
            authenticate_org

            present @api_org.sources.page(params[:page]).per(params[:size]), with: Entities::Source
          end
        end
      end
    end
  end
end
