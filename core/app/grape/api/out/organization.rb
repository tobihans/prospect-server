# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    class Organization < Grape::API
      use Rack::JSONP

      include Api::Defaults

      resource :out do
        resource :organization do
          desc "This will export the wrapped Crypto Information that can be unwrapped with the password which was used during setup of organisational crypto key." do
            detail "This method returns the Base64 encoded wrapped key. If no key was set yet, the information will be empty."
            success [{ code: 200,
                       message: 'Sample JSON Response:
                         {
                           id: 1
                           public_key: "base64encodedPublicKey",
                           wrapped_key: "base64encodedwrappedPrivateKey",
                           iv: "base64encodedIv",
                           salt: "base64encodedSalt",
                           enc_shared_keys: [ [ 2, "base64encodedEncryptedPrivateKey" ] ]
                         }' }]
            failure [[401, "Unauthorized"]]
            named "Wrapped Crypto Key"
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            hidden false
            deprecated false
            is_array true
            nickname "cryptokey_endpoint"
            produces ["application/json"]
            consumes ["application/json"]
          end

          get "wrapped_key" do
            authenticate_org
            return { id: @api_org.id }.merge(@api_org.crypto_options)
          end
        end
      end
    end
  end
end
