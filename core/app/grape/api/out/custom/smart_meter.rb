# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    module Custom
      # virtual class that represents a smart meter as by legacy A2EI definition
      class SmartMeter < Grape::API
        use Rack::JSONP

        include Api::Defaults

        resource :out do
          resource :custom do
            resource :smart_meter do
              desc "List of meter ids." do
                detail "The method returns an array ids with the meter ids"
                success [{ code: 200, message: "Sample JSON Response: [{\"meter_id\":1375},{\"meter_id\":1377}]" }]
                failure [[401, "Unauthorized"]]
                named "Meter list route"
                headers Authorization: {
                  description: 'Validates your identity\n Use the Bearer format with "Bearer TOKENVALUE"',
                  required: true
                }
                hidden false
                deprecated false
                is_array true
                nickname "list_ids_endpoint"
                produces ["application/json"]
                consumes ["application/json"]
              end

              params do
                optional :callback, type: String, description: "Callback method if you prefer JSONP return"
              end

              get "meters" do
                authenticate
                Service::SmMetaData.meters
              end

              desc "Meta data for a specific meter." do
                detail "The method returns all meta data per meter and the list of appliances installed"
                success Api::Entities::SmMetaData
                failure [[401, "Unauthorized"]]
                named "Meter meta data route"
                headers Authorization: {
                  description: 'Validates your identity\n Use the Bearer format with "Bearer TOKENVALUE"',
                  required: true
                }
                hidden false
                deprecated false
                is_array true
                nickname "metadata_endpoint"
                produces ["application/json"]
                consumes ["application/json"]
              end

              params do
                requires :id, type: Integer, allow_blank: false
                optional :callback, type: String, description: "Callback method if you prefer JSONP return"
              end

              get "meta_data" do
                authenticate
                Service::SmMetaData.meta_data_for_meter(params[:id])
              end

              desc "Technical data for a specific meter." do
                detail "The method returns an array of Hashes that present the avarage power consumpion of the requested meter in a given resolution"
                success [{ code: 200,
                           message: 'Sample JSON Response: [{
                            "meterId": 1380,
                            "timestamp": "2021-10-01T00:00:00.000Z",
                            "timestampInMillis": 1633046400,
                            "metered_power": 26.0,
                            "energyReadingKwh": 13.19,
                            "customerAccountId": "Houènonko",
                            "timeIntervalMinutes": 43920,
                            "energyConsumptionKwh": 13.19
                        },]' }]
                failure [[401, "Unauthorized"]]
                named "Meter technical route"
                headers Authorization: {
                  description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                  required: true
                }
                hidden false
                deprecated false
                is_array true
                nickname "techdata_endpoint"
                produces ["application/json"]
                consumes ["application/json"]
              end

              params do
                requires :id, type: Integer, allow_blank: false
                # TODO: the resolution value "custom" is stuffed into `date_trunc` from Postgres and it is not a valid input
                #       resulting in an PG::InvalidParameterValue ERROR, do we need that value?
                optional :resolution, type: String, default: "month", values: %w[hour day week month year custom],
                                      description: "Defines the period, if custom is chosen there will be only one period between start and end"
                optional :from, type: DateTime, default: 1.year.ago, description: "Begining of period"
                optional :end, type: DateTime, default: DateTime.now, description: "End of period"
                optional :callback, type: String, description: "Callback method if you prefer JSONP return"
              end

              get "technical_data" do
                authenticate
                Service::SmMetaData.technical_data_for_meter(params[:id], params[:resolution], params[:from],
                                                             params[:end])
              end

              desc "Day profile of technical data for a specific meter." do
                detail "The method returns an array that represents the aggregated day profile of the requested meter for a given time range."
                success [{ code: 200,
                           message: 'Sample JSON Response: [{
                            "meterId": 1380,
                            "hourOfTheDay": 16,
                            "averagePower": 201.1,
                            "stdDevPower": 13.6,
                            "maxPower": 317.8,
                            "minPower": 23.2,
                            "numberDatapoints": 134
                        },]' }]
                failure [[401, "Unauthorized"]]
                named "Meter technical profile route"
                headers Authorization: {
                  description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                  required: true
                }
                hidden false
                deprecated false
                is_array true
                nickname "techprofile_endpoint"
                produces ["application/json"]
                consumes ["application/json"]
              end

              params do
                requires :id, type: Integer, allow_blank: false
                optional :range, type: String, default: "month", values: %w[day week month month_weekdays year],
                                 description: "Defines the time range of aggregation starting from current date. Weekdays excludes Sat/Sun for calculation."
                optional :callback, type: String, description: "Callback method if you prefer JSONP return"
              end

              get "load_profile" do
                authenticate
                Service::SmMetaData.technical_data_profile_for_meter(params[:id], params[:range])
              end
            end
          end
        end
      end
    end
  end
end
