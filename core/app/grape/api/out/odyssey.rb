# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    # Handles Odyssey pull API calls, tailored to Odyssey's specification.
    class Odyssey < Grape::API
      use Rack::JSONP

      include Api::Defaults

      resource :out do
        resource :odyssey do
          desc "Meter readings for all meters in a time interval. Intended for integration with Odyssey pull API." do
            detail "This method returns a hash with meta information and a readings array containing information for each meter."
            success [{ code: 200,
                       message: 'Sample JSON Response: {
                          ...
                          "readings": [{
                          "timestamp": "2021-05-17T09:01:13.123Z",
                          "meterId": "002B-002-5678",
                          "energyConsumptionKwh": 0.08,
                          "energyReadingKwh": 2.6
                          "timeIntervalMinutes": 15,
                          "customerAccountId": "B0002",
                          "customerName": "Example Customer 2",
                          "customerPhone": "+234 0814 881 9281",
                          "latitude": "6.465399",
                          "longitude": "3.405100",
                          // optional fields
                          "rate": "1.5",
                          "rateCurrency": "Naira"
                          }, ...]
                          }' }]
            failure [[401, "Unauthorized"]]
            named "Meter data Odyssey format"
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            hidden false
            deprecated false
            is_array true
            nickname "odyssey_endpoint"
            produces ["application/json"]
            consumes ["application/json"]
          end

          params do
            requires :FROM, type: DateTime, default: 1.day.ago, description: "Beginning of period"
            requires :TO, type: DateTime, default: DateTime.now, description: "End of period"
            optional :id, type: String, allow_blank: true, description: "Just returns data for given meter id"
          end

          get "readings" do
            authenticate_org

            result = []
            select_odyssey_as_organization(@api_org, params[:FROM], params[:TO], meter_id:
                                                                  params[:id]).each do |entry|
              result << {
                timestamp: entry["metered_at"],
                meterId: entry["device_external_id"],
                energyConsumptionKwh: entry["output_energy_interval_wh"].to_f / 1000.0,
                energyReadingKwh: entry["output_energy_lifetime_wh"].to_f / 1000.0,
                timeIntervalMinutes: 5, # fix when shs_ts table has a interval_seconds field, AAMs are programmed to give readings every 5 minutes
                customerAccountId: entry["customer_external_id"],
                customerName: entry["customer_name_p"],
                customerPhone: entry["customer_phone_p"],
                readingId: entry["import_id"],
                # financingId
                # rate
                # rateCurrency
                latitude: entry["customer_latitude_b"],
                longitude: entry["customer_longitude_b"]
              }
            end

            {
              errors: [],
              readings: result
            }
          rescue => e
            {
              errors: [e.message],
              readings: nil
            }
          end

          desc "Payments in a given time interval. Intended for integration with Odyssey pull API." do
            detail "This method returns a hash with meta information and a payments array containing payments."
            success [{ code: 200,
                       message: 'Sample JSON Response: {
                          ...
                          "payments": [{
                            "timestamp": "2021-05-16T14:16:01.350Z",
                            "amount": 1000,
                            "currency": "Naira",
                            "transactionType": "FULL_PAYMENT",
                            "meterId": "M-001-123",
                            "customerId": "customer-123",
                            "customerName": "Example mini-grid Customer 1",
                            "customerPhone": "+234 0814 777 5555",
                            "transactionId": "minigrid-monthly-payment-001",
                            "financingId": "REA_NEP_PBG",
                            "agentId": "kiosk-001",
                            "latitude": "6.555555",
                            "longitude": "3.444444"
                            }, ...]
                          }' }]
            failure [[401, "Unauthorized"]]
            named "Payments Odyssey format"
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            hidden false
            deprecated false
            is_array true
            nickname "odyssey_payments_endpoint"
            produces ["application/json"]
            consumes ["application/json"]
          end

          params do
            requires :FROM, type: DateTime, default: 1.day.ago, description: "Beginning of period"
            requires :TO, type: DateTime, default: DateTime.now, description: "End of period"
          end

          get "payments" do
            authenticate_org

            transaction_type_mapping = {
              "Contract Payment" => "INSTALLMENT_PAYMENT",
              "Downpayment" => "INSTALLMENT_PAYMENT",
              "Manual Delay" => "DISCOUNT"
            }

            customer_category_mapping = {
              "household" => "Residential",
              "commercial" => "Commercial"
            }

            result = []

            select_payments_odyssey_as_organization(@api_org, params[:FROM], params[:TO]).each do |entry|
              result << {
                timestamp: entry["paid_at"],
                amount: entry["amount"],
                currency: entry["currency"],
                transactionType: transaction_type_mapping[JSON.parse(entry["custom"])["type"]],
                tarnsactionId: entry["uid"],
                serialNumber: entry["serial_number"],
                customerId: entry["customer_external_id"],
                customerPhone: entry["customer_phone_p"],
                meterId: entry["device_external_id"],
                customerCategory: customer_category_mapping[entry["customer_category"]],
                latitude: entry["customer_latitude_b"],
                longitude: entry["customer_longitude_b"]
              }
            end

            {
              errors: "", # payment API requires a string here, oposed to readings API (which needs an array)
              payments: result
            }
          rescue => e
            {
              errors: e.message,
              payments: nil
            }
          end
        end
      end

      private

      helpers do
        def select_odyssey_as_organization(organization, from, to, meter_id: nil)
          q = query_odyssey_as_organization(organization, meter_id:)

          params = [{ value: from }, { value: to }]
          params << { value: meter_id } if meter_id

          Postgres::RemoteControl.raw_query_with_params!(q, params)
        end

        #
        # Pulling data from shs tables
        #
        # If you need it from meter tables, check version history ;-)
        def query_odyssey_as_organization(organization, meter_id: nil)
          # select clause
          select_shs_ts = %w[metered_at device_uid output_energy_lifetime_wh output_energy_interval_wh import_id].map { |f| "sts.#{f} AS #{f}" }.join(",")
          select_shs = %w[device_external_id customer_external_id customer_name_p customer_phone_p customer_latitude_b customer_longitude_b].map { |f| "s.#{f} AS #{f}" }.join(",")

          select = "#{select_shs_ts}, #{select_shs}"

          # from clause
          shs_org_t = Naming.db_organization_data_view organization, "shs"
          shs_ts_org_t = Naming.db_organization_data_view organization, "shs_ts"
          from = "#{shs_ts_org_t} AS sts LEFT JOIN #{shs_org_t} AS s ON sts.device_uid = s.device_uid"

          # where clause
          time_filter = "sts.metered_at BETWEEN $1 AND $2"
          meter_filter = meter_id ? " AND s.device_external_id = $3" : ""
          where = time_filter + meter_filter

          "SELECT #{select} FROM #{from} WHERE #{where}"
        end

        def select_payments_odyssey_as_organization(organization, from, to)
          q = query_payments_odyssey_as_organization(organization)

          params = [{ value: from }, { value: to }]
          # params << { value: meter_id } if meter_id

          Postgres::RemoteControl.raw_query_with_params!(q, params)
        end

        #
        # Pulling payment data from shs tables
        #
        def query_payments_odyssey_as_organization(organization)
          # select clause
          select_payments_ts = %w[paid_at amount currency custom uid].map { |f| "pts.#{f} AS #{f}" }.join(",")
          select_shs = %w[device_external_id serial_number customer_external_id device_uid customer_category customer_latitude_b customer_longitude_b].map { |f| "s.#{f} AS #{f}" }.join(",")

          select = "#{select_payments_ts}, #{select_shs}"

          # from clause
          shs_org_t = Naming.db_organization_data_view organization, "shs"
          payments_ts_org_t = Naming.db_organization_data_view organization, "payments_ts"
          from = "#{payments_ts_org_t} AS pts LEFT JOIN #{shs_org_t} AS s ON pts.account_external_id = s.account_external_id"

          # where clause
          time_filter = "pts.paid_at BETWEEN $1 AND $2"
          # meter_filter = meter_id ? " AND s.device_uid = $3" : ""
          where = time_filter

          "SELECT #{select} FROM #{from} WHERE #{where}"
        end
      end
    end
  end
end
