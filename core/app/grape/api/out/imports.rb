# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    class Imports < Grape::API
      use Rack::JSONP

      include Api::Defaults

      resource :out do
        resources :imports do
          route_param :id do
            # GET imports/:id
            desc "Import" do
              detail "This endpoint returns info about an individual import"
              success [{ code: 200,
                        message: # rubocop:disable Layout/HashAlignment
                          'Sample JSON Response:
                          {
                            "id": 1,
                            "error": null,
                            "ingestion_finished_at": "2024-10-07T08:53:32.002Z",
                            "ingestion_started_at": "2024-10-07T08:53:31.904Z",
                            "last_imported_hint": null,
                            "processing_finished_at": "2024-10-07T08:53:32.173Z",
                            "processing_started_at": "2024-10-07T08:53:32.019Z",
                            "protocol": "Checking data file format: csv ... ",
                            "rows_duplicated": 0,
                            "rows_failed": 0,
                            "rows_inserted": 1,
                            "rows_updated": 0,
                            "created_at": "2024-10-07T08:53:31.901Z",
                            "source_id": 1
                          }' }]
              headers Authorization: {
                description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                required: true
              }
              failure [[401, "Unauthorized"]]
              hidden false
              deprecated false
              is_array false
              produces ["application/json"]
            end

            get do
              authenticate_org
              import = Import.find(params[:id])

              authorize_org import

              present import, with: Entities::Import
            end

            # DELETE imports/:id
            desc "Delete Import" do
              detail "This endpoint deletes an individual import"
              success [{ code: 204 }]
              headers Authorization: {
                description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                required: true
              }
              failure [[401, "Unauthorized"]]
              hidden false
              deprecated false
              is_array false
              produces ["application/json"]
            end

            delete do
              authenticate_org
              import = Import.find(params[:id])
              authorize_org import

              import.destroy
            end
          end

          # GET imports
          desc "Data imports for organization" do
            detail "This endpoint returns all imports done by an organization as an array"
            success [{ code: 200,
                        message: # rubocop:disable Layout/HashAlignment
                          'Sample JSON Response:
                          {
                            ["id": 1,
                            "error": null,
                            "ingestion_finished_at": "2024-10-07T08:53:32.002Z",
                            "ingestion_started_at": "2024-10-07T08:53:31.904Z",
                            "last_imported_hint": null,
                            "processing_finished_at": "2024-10-07T08:53:32.173Z",
                            "processing_started_at": "2024-10-07T08:53:32.019Z",
                            "protocol": "Checking data file format: csv ... ",
                            "rows_duplicated": 0,
                            "rows_failed": 0,
                            "rows_inserted": 1,
                            "rows_updated": 0,
                            "created_at": "2024-10-07T08:53:31.901Z",
                            "source_id": 1]
                          }' }]
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            failure [[401, "Unauthorized"]]
            hidden false
            deprecated false
            is_array true
            produces ["application/json"]
          end

          params do
            use :pagination
            use :filter
          end

          get do
            authenticate_org
            filtered_imports = @api_org.imports.ransack(params[:filter]).result.order(params[:order])
            paginated_imports = filtered_imports.page(params[:page]).per(params[:size])
            present paginated_imports, with: Entities::Import
          end

          # DELETE imports?:import_ids
          desc "Batch-deletes data imports by organization" do
            detail "This endpoint deletes all imports passed in the params by an organization."
            success [{ code: 204 }]
            headers Authorization: {
              description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
              required: true
            }
            failure [[401, "Unauthorized"]]
            hidden false
            deprecated false
            is_array true
            produces ["application/json"]
          end

          params do
            requires :import_ids,
                     type: [Integer],
                     desc: "Ids of the imports to be deleted. Must be integers."
          end

          delete do
            authenticate_org
            imports = Import.find(params[:import_ids])
            authorize_org imports
            imports.map(&:destroy)
          end
        end

        # GET sources/:id/imports
        resources :sources do
          route_param :id do
            resource :imports do
              desc "Imports for source" do
                detail "This endpoint returns info about all imports for a source"
                success [{ code: 200,
                        message: # rubocop:disable Layout/HashAlignment
                          'Sample JSON Response:
                          {
                            "id": 1,
                            "error": null,
                            "ingestion_finished_at": "2024-10-07T08:53:32.002Z",
                            "ingestion_started_at": "2024-10-07T08:53:31.904Z",
                            "last_imported_hint": null,
                            "processing_finished_at": "2024-10-07T08:53:32.173Z",
                            "processing_started_at": "2024-10-07T08:53:32.019Z",
                            "protocol": "Checking data file format: csv ... ",
                            "rows_duplicated": 0,
                            "rows_failed": 0,
                            "rows_inserted": 1,
                            "rows_updated": 0,
                            "created_at": "2024-10-07T08:53:31.901Z",
                            "source_id": 1
                          }' }]
                headers Authorization: {
                  description: "Validates your identity\n Use the Bearer format with \"Bearer TOKENVALUE\"",
                  required: true
                }
                failure [[401, "Unauthorized"]]
                hidden false
                deprecated false
                is_array false
                produces ["application/json"]
              end

              params do
                use :pagination
                use :filter
              end

              get do
                authenticate_org
                filtered_imports = Source.find(params[:id]).imports.ransack(params[:filter]).result.order(params[:order])
                paginated_imports = filtered_imports.page(params[:page]).per(params[:size])

                present paginated_imports, with: Entities::Import
              end
            end
          end
        end
      end
    end
  end
end
