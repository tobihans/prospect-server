# frozen_string_literal: true

require "rack/contrib"

module Api
  module Out
    def example(data_table); end

    class GenericDataTable < Grape::API
      GRAPE_TYPES = Api::In::GenericDataTable::GRAPE_TYPES

      use Rack::JSONP

      include Api::Defaults

      resource :out do
        Postgres::DataTable.all.select(&:exposed_to_out_api?).sort_by(&:data_category).each do |dt|
          columns = []
          dt.columns.each do |c|
            c.sql_columns_hint.each do |sql_c, type|
              is_cleartext = %i[pseudonymized encrypted].exclude? type
              is_number = c.type.in? %w[float integer]
              wrapped_example = is_number ? c.example.to_s : "\"#{c.example}\""
              example = is_cleartext ? wrapped_example : "***"
              comment = (is_cleartext ? "# #{c.type}" : " # #{type}") + ", #{c.desc}"

              columns << "\"#{sql_c}\": #{example}, #{comment}"
            end
          end

          examples = <<~EXAMPLES
            {
              "errors": [],
              "data": [
                {
                  #{columns.join("\n")}
                },
                ...
              ]
            }
          EXAMPLES

          desc dt.data_category do
            detail dt.desc
            success [{ code: 200, message: "Example data:\n#{examples}" }]
            failure [[401, "Unauthorized"]]
            named "#{dt.data_category} export API "
            headers Authorization: {
              description: 'Validates your identity. Use the Bearer format with "Bearer APISECRET"',
              required: true
            }
            hidden false
            deprecated false
            is_array false
            produces ["application/json"]
          end

          params do
            # Maps migration style types from YAML table description
            # to Grape API types, that are mostly ruby standard types
            type_mapping = {
              integer: Integer,
              string: String,
              boolean: Grape::API::Boolean,
              float: Float,
              datetime: DateTime
            }.freeze

            # Hash.fetch raises exception in case of missing key, tests should capture this
            dt.filter_columns.each do |c|
              optional :"#{c.name}_from",
                       type: type_mapping.fetch(c.type.to_sym),
                       description: "start of intervall. The results contain #{c.name} values equal or greater to the from parameter and lesser or equal than the to parameter. [from,to]"
              optional :"#{c.name}_to",
                       type: type_mapping.fetch(c.type.to_sym),
                       description: "end of intervall. The results contain #{c.name} values equal or greater to the from parameter and lesser or equal than to parameter. [from,to]"
              all_or_none_of :"#{c.name}_from", :"#{c.name}_to"
            end

            use :pagination
            # require at least one filter to avoid accidental filterless queries
            # TODO: at_least_one_of dt.filter_columns.map{|c| :"#{c.name}_from"}

            optional :q,
                     desc: "Filter or search using ransack ([docs](https://activerecord-hackery.github.io/ransack/)).
                         Use the format q[field_eq]=value, where field is the column to search and eq means 'equal'.
                         Example: q[name_eq]=apple. Use with caution, because filtering might not be supported by indices and can take much longer.
                         Currently the query builder you can reach via the 'Try it out' button, doesnt support this parameter."
          end

          get dt.data_category.to_sym do
            authenticate_org

            result, last_page = query_db(@api_org, dt, params)

            result = {
              errors: [],
              data: result
            }

            # last_page is a hidden parameter for tabulator, because the pagination module
            # wants to know the last page, but it is kinda expensive to count all the entries
            # on every request, so it's only for tabulator, there is an open issue to address this:
            # https://github.com/olifolkerd/tabulator/issues/3909
            params["last_page"] ? result.merge(last_page:) : result
          rescue => e
            Rails.logger.warn e
            error!({
                     errors: [e.message],
                     data: nil
                   }, 500)
          end
        end
      end

      private

      helpers do
        def query_db(organization, data_table, params)
          ar = Postgres::DataTable.ar(data_table.data_category, organization)
          columns = ar.columns.map(&:name)

          custom_queries = []

          params[:filter]&.each do |filter|
            if filter["field"].in?(columns)
              ransack_matcher = "#{filter['field']}_#{filter['type']}"
              params[:q][ransack_matcher] = filter["value"]
            else
              # if we can't find the column, we try to filter for it in the custom json field
              custom_queries << Arel::Nodes::InfixOperation.new("->>", ar.arel_table[:custom], Arel::Nodes.build_quoted(filter["field"])).public_send(filter["type"], filter["value"])
            end
          end

          params[:q][:s] = params[:s].join(" ") if params[:s]
          query = ar.ransack(params[:q].to_h).result

          # "backwards compatibility"
          data_table.filter_columns.each do |c|
            from_p = params[:"#{c.name}_from"]
            to_p = params[:"#{c.name}_to"]

            next unless from_p && to_p

            query = query.where(c.name => from_p..to_p)
          end

          custom_queries.each do |q|
            query = query.where(q)
          end

          # last page calculation for tabulator
          last_page = if params["last_page"]
                        count = query.size
                        (count.to_f / params["size"]).ceil
                      end

          query = query.select(data_table.sql_columns).
                  limit(params["size"]).
                  offset(params["size"] * (params["page"] - 1))

          [query, last_page]
        end
      end
    end
  end
end
