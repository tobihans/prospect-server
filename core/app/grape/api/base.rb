# frozen_string_literal: true

module Api
  # entry point for Grape API
  class Base < Grape::API
    mount Api::Out::Custom::SmartMeter
    mount Api::In::GenericDataTable
    mount Api::Out::GenericDataTable
    mount Api::Out::Odyssey
    mount Api::Out::Organization
    mount Api::Out::Sources
    mount Api::Out::Imports
    add_swagger_documentation consumes: ["application/json"],
                              produces: ["application/json"]
  end
end
