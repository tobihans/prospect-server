# frozen_string_literal: true

module Api
  # Class that facades the underlying model for API access
  module Entities
    class SmMetaData < Grape::Entity
      expose :meter_id, documentation: { type: "Integer", desc: "Unique Meter Identifier" }
      expose :name, documentation: { type: "String", desc: "Name of entity where meter is installed" }
      expose :district, documentation: { type: "String", desc: "Geographical District" }
      expose :type_of_network, documentation: { type: "String", desc: "Either Grid or Off-Grid" }
      expose :type_of_smartmeter, documentation: { type: "String", desc: "Single or Multiphase Meter" }
      expose :power_reading, documentation: { type: "String" }
      expose :appliances, documentation: { type: :Hash, desc: "List of appliances that are installed and metered" }
      expose :facilities, documentation: { type: "String" }
      expose :longitude, documentation: { type: "String", desc: "Geographical Longitude" }
      expose :latitude, documentation: { type: "String", desc: "Geographical Latitude" }
      expose :installed_at, documentation: { type: "String", desc: "Date when the meter was installed" }
    end
  end
end
