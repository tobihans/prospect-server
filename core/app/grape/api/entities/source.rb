# frozen_string_literal: true

module Api
  module Entities
    class Source < Grape::Entity
      expose :id, documentation: { type: :integer }
      expose :created_at, documentation: { type: :date }
      expose :kind, documentation: { type: :string }
      expose :name, documentation: { type: :string }
      expose :project_id, documentation: { type: :integer }
    end
  end
end
