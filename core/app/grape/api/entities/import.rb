# frozen_string_literal: true

module Api
  module Entities
    class Import < Grape::Entity
      expose :id, documentation: { type: :integer }
      expose :error, documentation: { type: :string }
      expose :ingestion_finished_at, documentation: { type: :date }
      expose :ingestion_started_at, documentation: { type: :date }
      expose :last_imported_hint, documentation: { type: :string }
      expose :processing_finished_at, documentation: { type: :date }
      expose :processing_started_at, documentation: { type: :date }
      expose :protocol, documentation: { type: :string }
      expose :rows_duplicated, documentation: { type: :integer }
      expose :rows_failed, documentation: { type: :integer }
      expose :rows_inserted, documentation: { type: :integer }
      expose :rows_updated, documentation: { type: :integer }
      expose :created_at, documentation: { type: :date }
      expose :source_id, documentation: { type: :integer }
    end
  end
end
