# frozen_string_literal: true

module Api
  # Provide standard helper functions for Grape API
  module Defaults
    extend ActiveSupport::Concern

    included do
      version "v1", using: :path
      default_format :json
      format :json

      helpers do
        include ActionController::HttpAuthentication::Token

        def authenticate
          @api_user = api_user_by_token token
          error!("401 Unauthorized", 401) unless @api_user
        end

        def authenticate_org
          @api_org = api_org_by_token token

          error!("401 Unauthorized", 401) unless @api_org
        end

        def authorize_org(record_or_records)
          records = Array(record_or_records)
          unauthorized = records.any? { |r| r&.organization_id != @api_org.id }
          error!("401 Unauthorized", 401) if unauthorized
        end

        def token
          headers["Authorization"].blank? ? nil : token_params_from(headers["Authorization"]).shift[1]
        end

        def api_user_by_token(token)
          return nil if token.blank?

          case token
          when ENV.fetch("JRC_TOKEN", nil)
            :jrc
          when ENV.fetch("ODYSSEY_TOKEN", nil)
            :odyssey
          end
        end

        def api_org_by_token(token)
          return nil if token.blank?

          Organization.find_by(api_key: token)
        end

        params :pagination do
          requires :size, type: Integer, values: 1..100, default: 25, description: "Amount of records per page"
          # endless range does not work for some reason
          # even when it's in the grape documentation... is this a bug?
          requires :page, type: Integer, values: ->(v) { v.is_a?(Integer) && v.positive? }, default: 1, description: "Request n-th page, must be a positive number"
        end

        params :filter do
          optional :filter, type: Hash do
            requires :created_at_gteq, type: DateTime, desc: "Start date for the created_at filter", default: 24.hours.ago
            requires :created_at_lteq, type: DateTime, desc: "End date for the created_at filter", default: Time.zone.now
          end
          requires :order, type: String, desc: "Order of the results", default: "created_at DESC"
        end
      end
    end

    #
    # Explicitly disallow some parameters
    # usage:
    #   optional attr_name, forbidden: true
    #
    class Forbidden < Grape::Validations::Validators::Base
      def validate_param!(attr_name, params)
        return if params[attr_name].blank?

        raise Grape::Exceptions::Validation.new params: [@scope.full_name(attr_name)],
                                                message: "is forbidden and must not be present"
      end
    end
  end
end
