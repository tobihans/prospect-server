# frozen_string_literal: true

class AnalyticsController < ApplicationController
  after_action :verify_authorized

  @header_section = "Analytics"

  def index
    @page_title = t(".your_dashboards")
    @dashboards = dashboards

    @dashboards["General"].select! { _1[:title] =~ /rbf/i } if Current.organization.rbf_submitting?

    authorize :analytics
  end

  def preview
    @uid = params[:board_uid]
    key = "#{Current.organization.id}-#{@uid}-categories"
    categories = Rails.cache.fetch key

    unless categories
      categories = Grafana::RemoteControl.dashboard_categories(@uid, Current.organization)
      Rails.cache.write key, categories
    end

    @no_data = if categories.empty?
                 false
               else
                 categories.map do |cat|
                   key = "#{Current.organization.id}-#{cat}-amount"
                   amount = Rails.cache.fetch key

                   unless amount
                     amount = Postgres::RemoteControl.count_as_organization(Current.organization, data_category: cat, limit: 1)
                     Rails.cache.write key, amount, expires_in: 1.minute
                   end

                   amount
                 end.sum.zero?
               end
  rescue
    @no_date = true
  ensure
    authorize :analytics
    render :preview_tile
  end

  def show
    authorize :analytics

    uid = params[:id]
    dashboard = dashboards.values.flatten.select { |db| db[:uid] == uid }.first

    @slim_header = true
    @page_title = dashboard[:title].to_s
    @grafana_url = build_grafana_url dashboard[:url]
    @dashboards = dashboard_keys
  end

  def grafana_new
    authorize :analytics

    path, title_prefix =
      case params[:type]
      when "folder"
        %w[dashboards/folder/new/ New]
      when "dashboard"
        %w[dashboard/new New]
      when "import_dashboard"
        %w[dashboard/import]
      else
        raise "Unknown type #{params[:type]}"
      end

    @slim_header = true
    @grafana_url = build_grafana_url(path)
    @dashboards = dashboard_keys
    @page_title = "#{title_prefix} #{params[:type].titleize}"
    render :show
  end

  private

  def dashboards
    Grafana::RemoteControl.organization_dashboards(Current.organization).
      reject { |k, _v| k.starts_with?("_") }
  end

  def dashboard_keys
    dashboards.values.flatten.map do |db|
      [db[:uid], db[:title]]
    end
  end

  def build_grafana_url(url)
    url = url[1..] if url.start_with?("/")

    base = ENV.fetch("GRAFANA_EMBED_URL", nil).to_s
    instance_url = ENV.fetch("PUBLIC_URL", nil).to_s
    theme = "prospect"
    grafana_default_params =
      "kiosk=tv&OrgId=#{Current.organization.grafana_org_id}&auth_token=#{Current.url_login_token}"

    "#{base}/#{url}?var-themeid=#{theme}&var-instanceURL=#{instance_url}&#{grafana_default_params}"
  end
end
