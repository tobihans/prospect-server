# frozen_string_literal: true

class ExportsController < ApplicationController
  @header_section = "Export"

  def index
    @source_id = clean_source_id_param
    @source = Source.find_by id: @source_id

    @page_title = if @source
                    t(".data_export_source", source: @source.to_s)
                  else
                    t(".data_export")
                  end

    @data_categories =
      Current.organization.data_categories_counts(source_id: @source_id)
    @custom_views =
      Current.organization.custom_views_counts(source_id: @source_id)

    authorize :exports
  end

  def export_form
    authorize :exports
    source_id = clean_source_id_param

    source = Source.find(source_id)
    @export_name = source.to_s_download

    # HACK: hack hack hack hack
    # TODO: fix this
    # UECCC form insert shs/meters and payments_ts data
    # and we magically know, that those tables are connected
    # via the account_external_id column

    shs_or_meters = Data::Meters.where(source_id: source.id).map(&:serializable_hash).presence ||
                    Data::SHS.where(source_id: source.id).map(&:serializable_hash).presence
    payments = Data::PaymentsTs.where(source_id: source.id).map(&:serializable_hash)

    result = [shs_or_meters, payments].flatten.group_by { |obj| obj["account_external_id"] }.to_h.map do |_account, data|
      data.reduce({}, :deep_merge)
    end

    form = DynamicForm.find(source.details["form"].to_i)
    result = Ueccc::FormBuilder.custom_ueccc_mapping(form, %w[meters payments_ts], result)

    @data = {}
    @header = result.map(&:keys).flatten.uniq - ["custom"]
    len = result.count
    result.each_with_index do |row, i|
      custom = JSON.parse(row.delete("custom") || "{}")
      row.merge(custom).each do |key, val|
        (@header << key).uniq!
        @data[key] ||= Array.new(len)
        @data[key][i] = val
      end
    end

    respond_to do |format|
      format.csv do
        full_csv = CSV.generate do |csv|
          csv << @header
          @data.values.transpose.each do |row|
            csv << row
          end
        end
        send_data full_csv, filename: "prospect_#{@export_name}.csv"
      end
      format.xlsx do
        # the rest happens in export.xlsx.axlsx
        response.headers["Content-Disposition"] = "attachment; filename=\"prospect_#{@export_name}.xlsx\""
      end
    end
  end

  def export
    authorize :exports
    @data_category = params[:data_category].presence
    @custom_view = params[:custom_view].presence
    source_id = clean_source_id_param

    raise "can only live with either data_category or custom_view!" if @data_category && @custom_view

    @export_name = [@data_category, @custom_view].compact.first

    result = if @data_category
               Current.organization.all_for_category(@data_category, source_id:)
             elsif @custom_view
               Current.organization.all_for_custom_view(@custom_view, source_id:)
             else
               raise "either data_category or custom_view - one should be given!"
             end

    @data = {}
    @header = result.fields - ["custom"]
    len = result.count
    result.each_with_index do |row, i|
      custom = JSON.parse(row.delete("custom") || "{}")
      row.merge(custom).each do |key, val|
        (@header << key).uniq!
        @data[key] ||= Array.new(len)
        @data[key][i] = val
      end
    end

    respond_to do |format|
      format.csv do
        full_csv = CSV.generate do |csv|
          csv << @header
          @data.values.transpose.each do |row|
            csv << row
          end
        end
        send_data full_csv, filename: "prospect_#{@export_name}.csv"
      end
      format.xlsx do
        # the rest happens in export.xlsx.axlsx
        response.headers["Content-Disposition"] = "attachment; filename=\"prospect_#{@export_name}.xlsx\""
      end
    end
  end
end
