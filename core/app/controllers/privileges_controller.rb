# frozen_string_literal: true

class PrivilegesController < ApplicationController
  after_action :verify_authorized

  def index
    @ou = authorize OrganizationUser.find(params[:organization_user_id])
    @privileges = @ou.privileges
  end

  def update
    @ou = authorize OrganizationUser.find(params[:organization_user_id])
    @ou.privilege_names = params[:privileges]

    redirect_to organizations_mine_path, notice: t(".updated", user: @ou.user.email, organization: @ou.organization.name)
  end
end
