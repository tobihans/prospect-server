# frozen_string_literal: true

class SharedKeysVaultController < ApplicationController
  def edit
    authorize :shared_keys_vault
  end

  def create
    authorize :shared_keys_vault

    shared_keys = params.require(:shared_keys_vault).require(:shared_keys).map { JSON.parse(_1).with_indifferent_access }

    shared_keys.each do |sk|
      SharedKeysVault.create!(
        organization: Current.organization,
        shared_with_organization_id: sk[:organization_id],
        encrypted_key: sk[:shared_key],
        user: Current.user
      )
    end

    if Current.organization.rbf_submitting?
      redirect_to root_path
    else
      redirect_to shared_keys_vault_edit_path
    end
  end

  def delete
    authorize :shared_keys_vault

    shared_with_organization_id = params.require(:shared_keys_vault).require(:shared_with_organization_id)
    SharedKeysVault.find_by(organization: Current.organization, shared_with_organization_id:).delete

    redirect_to shared_keys_vault_edit_path
  end
end
