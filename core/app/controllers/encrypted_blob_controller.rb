# frozen_string_literal: true

class EncryptedBlobController < ApplicationController
  def show
    eb = authorize EncryptedBlob.find(params[:id])
    render body: eb.blob.download
  end
end
