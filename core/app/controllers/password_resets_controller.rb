# frozen_string_literal: true

class PasswordResetsController < ApplicationController
  skip_before_action :require_login

  @header_section = "User"

  # form to enter email for forgotten password
  def new; end

  # This is the reset password form.
  def edit
    @token = params[:id]
    @user = User.load_from_reset_password_token @token

    not_authenticated if @user.blank?
  end

  # request password reset.
  # you get here when the user entered their email in the reset password form and submitted it.
  def create
    @user = User.by_normalized_email params[:email]

    if @user
      if @user.authentications.any?
        # tell people they should use the other authentication method not just change pw
        UserMailer.reset_password_other_auth(@user).deliver_later
      else
        @user.deliver_reset_password_instructions!
      end
    end

    # prevent timing attack to figure out if email exists
    sleep rand(1000).to_f / 1000 unless Rails.env.test?

    # Tell the user instructions have been sent whether or not email was found.
    # This is to not leak information to attackers about which emails exist in the system.
    redirect_to root_path, notice: "Instructions have been sent to #{params[:email]} if email exists in our system. "
  end

  # This action fires when the user has sent the reset password form.
  def update
    @token = params[:id]
    @user = User.load_from_reset_password_token @token

    not_authenticated and return if @user.blank?

    # the next line makes the password confirmation validation work
    @user.password_confirmation = params[:user][:password_confirmation]
    # the next line clears the temporary token and updates the password
    if @user.change_password(params[:user][:password])
      redirect_to root_path, notice: t(".success")
    else
      render action: "edit", status: :unprocessable_content
    end
  end
end
