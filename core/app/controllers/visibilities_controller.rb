# frozen_string_literal: true

class VisibilitiesController < ApplicationController
  after_action :verify_authorized

  @header_section = "Import"

  def show
    @visibility = authorize Visibility.find params[:id]
  end

  def new
    project = Project.find params[:project_id]
    @visibility = authorize Visibility.new(project:)
    @organizations = Organization.where.not(id: project.visible_for_organization_ids).default_order
  end

  def edit
    @visibility = authorize Visibility.find params[:id]
    @organizations = Organization.where.not(id: @visibility.project.organization_id).default_order
  end

  def create
    project = Project.find params[:project_id]
    @visibility = Visibility.new visibility_params
    @visibility.project = project

    authorize @visibility

    if @visibility.save
      redirect_to visibility_path(@visibility), notice: t(".created")
    else
      @organizations = Organization.where.not(id: project.visible_for_organization_ids)
      render :new, alert: t(".invalid"), status: :unprocessable_content
    end
  end

  def update
    @visibility = Visibility.find params[:id]
    @visibility.assign_attributes visibility_params
    authorize @visibility

    if @visibility.save
      redirect_to @visibility, notice: t(".updated")
    else
      @organizations = Organization.all
      render :edit, alert: t(".invalid"), status: :unprocessable_content
    end
  end

  def destroy
    @visibility = authorize Visibility.find params[:id]
    project = @visibility.project

    if @visibility.destroy
      redirect_to project_path(project), notice: t(".destroyed")
    else
      redirect_to @visibility, alert: t(".destroy_blocked")
    end
  end

  private

  def visibility_params
    return {} unless params[:visibility]

    params.require(:visibility).permit :organization_id,
                                       :data_from,
                                       :data_until,
                                       :license,
                                       :resharing,
                                       { columns: {} }
  end
end
