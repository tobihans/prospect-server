# frozen_string_literal: true

class RbfClaimsController < ApplicationController
  # after_action :verify_authorized

  @header_section = "RBF"

  def index
    authorize :rbf_claim
    base_query = RbfClaim.for_index(Current.organization).default_order

    see_all_privs = %w[rbf_claim.show_claims_full rbf_claim.show_claims_status rbf_claim.show_claims_minimal]

    @claims = if current_user.can_any?(see_all_privs)
                base_query
              elsif current_user.can? "rbf_claim.do_verification"
                base_query.verification.select { current_user.in? _1.rbf_claim_verifiers }
              else
                base_query.none
              end
  end

  def show
    @claim = authorize RbfClaim.find params[:id]
  end

  def new
    @claim = RbfClaim.new rbf_claim_template_id: params[:rbf_claim_template_id],
                          organization: Current.organization
  end

  def create
    @claim = RbfClaim.new organization: Current.organization,
                          rbf_claim_template_id: params.dig(:rbf_claim, :rbf_claim_template_id)
    authorize @claim

    if @claim.save
      redirect_to @claim, notice: "Claim created" # rubocop:disable Rails/I18nLocaleTexts
    else
      render :new, status: :unprocessable_content, alert: @claim.errors.full_messages.to_sentence
    end
  end

  def summary
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_do_data_collection
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_do_assign_verifiers
    @claim = authorize RbfClaim.find params[:id]
    @candidates = @claim.
                  verifying_organization.active_users.
                  select { _1.can? "rbf_claim.do_verification", in_organization: @claim.verifying_organization }
  end

  def show_do_verification
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_do_payment
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_paid_out
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_verification_result
    @claim = authorize RbfClaim.find params[:id]
  end

  def show_print_voucher
    @claim = authorize RbfClaim.find params[:id]
  end

  def print_voucher
    @claim = authorize RbfClaim.find params[:id]
    render :print_voucher, layout: false
  end

  def do_data_collection
    @claim = authorize RbfClaim.find params[:id]
    @claim.do_data_collection data_collection_claim_params.to_h, mode: params[:mode]
    redirect_to @claim, notice: "Devices for submission updated" # rubocop:disable Rails/I18nLocaleTexts
  end

  def do_data_collection_add_all
    @claim = authorize RbfClaim.find params[:id]
    @claim.do_data_collection_add_all
    redirect_to @claim, notice: "All eligible devices added to claim" # rubocop:disable Rails/I18nLocaleTexts
  end

  def data_collection_claim_params
    params.permit(claim: {})[:claim]
  end

  def do_assign_verifiers
    @claim = authorize RbfClaim.find params[:id]
    @claim.verification_users_ids = params.dig(:rbf_claim, :verification_users_ids)
    redirect_to @claim, notice: "Users for verification assigned" # rubocop:disable Rails/I18nLocaleTexts
  end

  def do_device_verification
    @claim = authorize RbfClaim.find params[:id]
    @claim.do_device_verification uid: params[:uid],
                                  state_rejection_combined: params[:rbf_device_claim][:state_rejection_combined],
                                  reviewer_comment: params[:rbf_device_claim][:reviewer_comment]
    if params[:commit].to_s.include?("next") && params[:next_device].present?
      redirect_to device_details_rbf_claim_path(@claim, uid: params[:next_device], mode: :existing)
    elsif params[:commit].to_s.include?("back")
      redirect_to rbf_claim_path(@claim)
    else
      redirect_to device_details_rbf_claim_path(@claim, uid: params[:uid], mode: :existing)
    end
  end

  def print_submitted
    @claim = authorize RbfClaim.find params[:id]
    render :print_submitted, layout: false
  end

  def download_verification_csv
    @claim = authorize RbfClaim.find params[:id]

    fields = [
      :submission_category,
      :submission_comment,
      :verification_category,
      :state,
      :rejection_category,
      :reviewer_comment,
      :device_category,
      :claimed_subsidy,
      :claimed_incentive,
      "claimed_devices_data->'purchase_date'",
      "claimed_devices_data->'model'",
      "claimed_devices_data->'manufacturer'",
      "claimed_devices_data->'serial_number'",
      "claimed_devices_data->'custom'->'serial_number_2'",
      "claimed_devices_data->'custom'->'customer_firstname_e'",
      "claimed_devices_data->'custom'->'customer_surname_e'",
      "claimed_devices_data->'customer_id_type'",
      "claimed_devices_data->'customer_id_number_e'",
      "claimed_devices_data->'customer_gender'",
      "claimed_devices_data->'custom'->'is_household_head'",
      "claimed_devices_data->'customer_phone_e'",
      "claimed_devices_data->'customer_phone_2_e'",
      "claimed_devices_data->'customer_location_area_1'",
      "claimed_devices_data->'customer_location_area_2'",
      "claimed_devices_data->'customer_location_area_3'",
      "claimed_devices_data->'customer_location_area_4'",
      "claimed_devices_data->'customer_location_area_5'",
      "claimed_devices_data->'payment_plan_type'",
      "claimed_devices_data->'payment_plan_days'",
      "claimed_devices_data->'payment_plan_down_payment'",
      "claimed_devices_data->'account_external_id'"
    ].map(&:to_s)

    pluck_args = fields.map { _1.include?("->") ? Arel.sql(_1) : _1 }
    headers = fields.map do |f|
      f.gsub("claimed_devices_data", "device").gsub("'custom'->", "").gsub("'", "").gsub("->", "_")
    end

    plucked = @claim.rbf_device_claims.pluck(*pluck_args)

    data = CSV.generate do |csv|
      csv << headers
      plucked.each { csv << _1 }
    end

    send_data data
  end

  def do_verification
    @claim = authorize RbfClaim.find params[:id]
    @claim.do_verification params[:claim]
    render :show_do_verification, notice: "Verified Devices updated"
    # redirect_to @claim, notice: "Verified Devices updated"
  end

  def do_sample_secondary_verification
    @claim = authorize RbfClaim.find params[:id]
    @claim.sample_for_secondary_verification!
    redirect_to @claim, notice: "Devices sampled for secondary validation" # rubocop:disable Rails/I18nLocaleTexts
  end

  def do_payment
    @claim = authorize RbfClaim.find params[:id]
    @claim.do_payment params[:claim]
    redirect_to @claim, notice: "Payment Information updated" # rubocop:disable Rails/I18nLocaleTexts
  end

  def device_details
    @claim = authorize RbfClaim.find params[:id]
    @verification_mode = @claim.verification? && authorize(@claim, :do_verification?)
    @device_data = @claim.device_data params[:uid], mode: params[:mode].to_sym
  end

  def device_claim
    @claim = authorize RbfClaim.find params[:id]
    @device_claim = @claim.rbf_device_claims.find(params[:rbf_device_claim_id])
  end

  def download
    typ = params[:type]
    @claim = authorize RbfClaim.find params[:id]
    header = @claim.claimed_data["data"].first.keys
    full_csv = CSV.generate do |csv|
      csv << header
      sample_len = @claim.claimed_data["data"].count / 10
      sample_half = sample_len / 2

      stuff = case typ
              when "all", "desk"
                @claim.claimed_data["data"]
              when "phone"
                @claim.claimed_data["data"].sample(sample_len, random: Random.new(@claim.id))[0..sample_half]
              when "field"
                @claim.claimed_data["data"].sample(sample_len, random: Random.new(@claim.id))[(sample_half + 1)..]
              end

      stuff.each do |r|
        csv << header.map { r[_1] }
      end
    end
    send_data full_csv, filename: "claim_#{@claim.id}_#{typ}.csv"
  end

  def event
    @claim = authorize RbfClaim.find params[:id]
    ev = @claim.aasm.events(possible: true).find { |e| e.to_s == params[:event] }

    if @claim.public_send "may_#{ev}?"
      @claim.public_send "#{ev}!"
      redirect_to @claim, notice: "Claim moved to #{@claim.reload.aasm_state.titleize}"
    else
      redirect_to @claim, alert: "You can not perform this action at the moment" # rubocop:disable Rails/I18nLocaleTexts:
    end
  end
end
