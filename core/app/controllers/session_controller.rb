# frozen_string_literal: true

class SessionController < ApplicationController
  skip_before_action :require_login, only: %i[create oauth callback]
  skip_before_action :check_accepted_terms

  @header_section = "User"

  def logout_user
    logout
    redirect_to :root, notice: t("session.logged_out")
  end

  def create
    @user = login User.normalize_email(params[:email]),
                  params[:password], params[:remember]
    if @user
      reset_session
      auto_login(@user)
      redirect_back_or_to(:root, notice: t(".login_success"))
    else
      flash.now[:alert] = t(".login_fail")
      render "users/welcome", status: :unauthorized # turbo needs non-200 code
    end
  end

  # sends the user to the provider, and after authorizing there back to the callback url.
  def oauth
    redirect_to sorcery_login_url(params[:provider]), allow_other_host: true
  end

  def callback
    @provider_name = params[:provider]

    existing_full_user = User.load_from_provider @provider_name, sorcery_user_info[:uid]

    if existing_full_user
      if existing_full_user.active?
        do_sorcery_login! @provider_name
      else
        login_failed! @provider_name, "User inactive. #{existing_full_user}"
      end
    else
      invited_user = User.invited.by_normalized_email sorcery_user_info[:email]
      if invited_user
        # activate the user and allow future logins via external provider
        invited_user.username ||= sorcery_user_info[:username]
        invited_user.activate
        if invited_user.valid?
          invited_user.save!
          invited_user.add_provider_to_user @provider_name, sorcery_user_info[:uid]
          invited_user.mail_welcome
          do_sorcery_login! @provider_name
        else
          login_failed! @provider_name,
                        "Could not activate and save invited user: #{invited_user} #{invited_user.errors.to_a.join(', ')}"
        end
      else
        login_failed! @provider_name, "User not found"
      end
    end
  end

  private

  # put together some sorcery user info which is otherwise spread out
  # also nice for mocking in specs to test the complicated callback function
  def sorcery_user_info
    @sorcery_user_info ||= begin
      sorcery_fetch_user_hash @provider_name
      clean_attrs = user_attrs @provider.user_info_mapping, @user_hash
      clean_attrs.merge uid: @user_hash[:uid].to_s
    end
    @sorcery_user_info ||= {}
  end

  def do_sorcery_login!(provider_name)
    @user = login_from provider_name

    raise "Against all odds we failed to login via #{provider_name}" unless @user

    Rails.logger.info "Successful login: #{@user.email} via #{provider_name}"

    redirect_to root_path, notice: t("session.logged_in_from", provider: provider_name.titleize), allow_other_host: true
  end

  def login_failed!(provider_name, log_str = "")
    Rails.logger.warn "Failed to login: #{log_str}"
    reset_session
    redirect_to root_path, alert: t("session.log_in_from_failed", provider: provider_name.titleize), allow_other_host: true
  end
end
