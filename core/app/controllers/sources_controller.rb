# frozen_string_literal: true

class SourcesController < ApplicationController
  with_options only: %i[public_recorded public_recorded_upload] do
    skip_before_action :require_login
    before_action :validate_code_load_source
  end

  after_action :verify_authorized

  with_options only: %i[dynamic_form dynamic_form_submit] do
    skip_before_action :require_login
  end

  @header_section = "Import"

  def show
    @source = authorize Source.find params[:id]
    @page_title = t(".data_source", name: @source.name)
  end

  def show_secret
    @source = authorize Source.find params[:source_id]
    @visible = ActiveRecord::Type::Boolean.new.deserialize params[:visible]
  end

  def destroy
    @source = authorize Source.find params[:id]
    project = @source.project

    if @source.destroy
      redirect_to project_path(project), notice: t(".destroyed")
    else
      redirect_to @source, alert: t(".destroy_blocked")
    end
  end

  def reprocess
    @source = authorize Source.find params[:source_id]
    @source.delete_data!
    @source.imports.each(&:reprocess!)

    redirect_to @source, notice: t(".reprocess")
  end

  def delete_data_and_imports
    @source = authorize Source.find params[:source_id]
    @source.imports.each(&:destroy)
    @source.delete_data!
    redirect_to @source, notice: t(".imports_and_data_deleted")
  end

  def trigger_now
    @source = authorize Source.find params[:source_id]
    ImportWorker.perform_async(source_id: @source.id)
    redirect_to @source, notice: t(".background")
  end

  def backfill
    @source = authorize Source.find params[:source_id]
  end

  def backfill_completely
    @source = authorize Source.find params[:source_id]
    import_hint = @source.adapter.backfill_completely_import_hint

    ImportWorker.perform_async(source_id: @source.id, use_db_import_hint: false, import_hint:)
    redirect_to @source, notice: t(".background")
  end

  def backfill_from_date
    @source = authorize Source.find params[:source_id]

    # validate :from_date parameter
    from_date = begin
      Date.parse(params[:from_date])
    rescue
      nil # will render error
    end

    if from_date
      import_hint = @source.adapter.backfill_from_date_import_hint from_date
      ImportWorker.perform_async(source_id: @source.id, use_db_import_hint: false, import_hint:)

      redirect_to @source, notice: t(".background", date: from_date)
    else
      render :backfill, status: :unprocessable_content, alert: t(".date_wrong")
    end
  end

  def csv_update
    @source = authorize Source.find params[:source_id]
  end

  def csv_update_submit
    @source = authorize Source.find params[:source_id]

    data = params.dig(:source, :file)&.read

    import = @source.update_from_csv! data, params[:source][:data_category]

    redirect_to import
  end

  def edit_project
    @source = authorize Source.find params[:source_id]
    @projects = Project.where(organization: @source.organization).default_order
  end

  def update_project
    @source = Source.find params[:source_id]
    @source.assign_attributes project_id: params[:source][:project_id]
    authorize @source

    if @source.save
      redirect_to @source, notice: t(".updated")
    else
      @projects = Project.where(organization: @source.organization)
      render :edit_project, status: :unprocessable_content,
                            alert: t(".invalid", errors: @source.errors.to_a.join(", "))
    end
  end

  def manual_upload
    @source = authorize Source.find params[:source_id]

    data = params.dig(:source, :file)&.read

    if data.blank?
      redirect_back fallback_location: source_path(@source), alert: t(".no_data")
    else
      import = @source.trigger_import! data
      if import.failed?
        redirect_to import_path(import), alert: t(".failed")
      else
        redirect_to import_path(import), notice: t(".uploaded")
      end
    end
  end

  def view_data
    @source = authorize Source.find params[:source_id]

    tables = Postgres::DataTable.select { _1.active_record.where(source_id: @source.id).limit(1).count.positive? }

    endpoints = Api::Base.routes.select { |r| tables.any? { r.origin =~ %r{out/#{_1.data_category.underscore}$} } }.
                map { _1.origin.gsub(":version", _1.version) }

    update_path = source_csv_update_submit_path(@source.id)

    @options = tables.zip(endpoints).to_h do |tb, ep|
      [
        tb.data_category.underscore,
        {
          ajaxURL: "#{root_url}/api/#{ep[1..]}",
          ajaxConfig: {
            headers: {
              Authorization: "Bearer #{Current.user.organization.api_key}"
            }
          },
          ajaxParams: {
            "q[source_id_eq]": @source.id,
            last_page: true
          },
          # will fit the default 25 lines, if we use auto as height,
          # the browser will scroll to the top of the page each time
          # we flip to another page, that's a little annoying
          height: "800px",
          layout: "fitData",
          # if autoColumns is only set to true, the table is build based on the first element
          # but every entry could have some other attributes in the custom column, so we can
          # scan all the entries for column keys via 'full'
          autoColumns: "full",
          pagination: true,
          paginationMode: "remote",
          paginationSize: 25,
          filterMode: "remote",
          sortMode: "remote",
          paginationSizeSelector: [10, 25, 50, 100],
          data_table: tb.columns.each_with_object({}) { |c, o| o[c.name] = c.config },
          update_path: (tb.updatable? ? update_path : nil),
          mapper: nil,
          formatter: nil,
          data_category: tb.data_category.underscore
        }
      ]
    end

    render :view_data
  end

  def dynamic_form_template
    source = authorize Source.find params[:source_id]
    form = DynamicForm.find(source.details["form"].to_i)

    csv_file = CSV.generate do |csv|
      csv << form.fields
    end

    send_data csv_file, filename: "#{source.name.downcase.gsub(' ', '_')}_form_template.csv"
  end

  def dynamic_form
    @slug = params.require :slug

    @source = Source.find_by("details->>'slug' = ?", @slug)

    unless @source
      skip_authorization
      render :not_found, status: :not_found, layout: false and return
    end

    authorize @source

    @form = @source.adapter&.dynamic_form
    render :not_found, status: :not_found, layout: false and return unless @form

    @page_title = @form.name
    @logo_image = @form.logo.presence || "uecc-new-logo.png"
    @form_data = @form.form_data || {}
    @dependent_selects = Ueccc::FormBuilder.generate_dependent_selects(@form_data)

    Ueccc::FormBuilder.inject_products!(@form_data, @dependent_selects,
                                        @form.rbf_claim_template, @source.organization)

    @auto_display = Ueccc::FormBuilder.generate_price_auto_display(@form.rbf_claim_template, @source.organization)
  end

  # endpoint for dynamic form data submissions - has to be interpreted to match the data tables
  def dynamic_form_submit
    source = Source.find_by("details->>'slug' = ?", params[:slug])

    unless source
      skip_authorization
      render json: { status: "error", message: "Source not found" }, status: :unprocessable_content and return
    end

    authorize source

    dynamic_form = source.adapter&.dynamic_form
    render json: { status: "error", message: "Dynamic Form not found" }, status: :unprocessable_content and return unless
      dynamic_form

    data = if params[:file] || params.dig(:source, :file)
             CSV.parse((params[:file] || params[:source][:file]).read, headers: true).map(&:to_h)
           else
             [params.permit(*dynamic_form.fields).to_h]
           end

    import = source.trigger_import! data

    if params[:file] || params[:source][:file]
      redirect_to import_path(import)
    elsif import.failed?
      render status: :unprocessable_content, json: { status: "error", message: import.error, errorLog: import.protocol }
    else
      render json: { status: "success" }
    end
  rescue => e
    Rails.logger.error "Error processing submission: #{e.message}"
    render json: { status: "error", message: e.message }, status: :internal_server_error
  end

  def public_recorded
    authorize @source

    @code = params[:code]

    @page_title = "Recorded Upload for #{@source.name}"
  end

  def public_recorded_upload
    authorize @source

    data = params[:file].try(:read)

    render json: { error: "Data Missing" } and return if data.blank?
    render json: { error: "Recording Missing" } and return if params[:recording].blank?

    import = @source.trigger_import! data

    if import.failed?
      render json: { error: import.error }
    else
      import.recording = params[:recording]
      import.save!
      render json: { error: nil }
    end
  end

  private

  def validate_code_load_source
    cleartext = VerifiedCode.parse(params[:code])
    render plain: "Invalid or expired Code", status: :bad_request and return if cleartext.blank?

    source_id, last_import_id = cleartext.split

    @source = Source.find_by id: source_id
    render plain: "Invalid or expired Code", status: :bad_request and return unless @source

    later_import_exists = @source.imports.no_error.exists?(["id > ?", last_import_id])
    render plain: "Link is outdated. Imports already existing", status: :bad_request if later_import_exists
  end
end
