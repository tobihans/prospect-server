# frozen_string_literal: true

class StatusController < ApplicationController
  skip_before_action :require_login

  def ping
    services = {
      postgres: 20,
      faktory: 10,
      grafana: 2,
      minio: 1
    }

    result = {}

    threads =
      services.map do |s, i|
        Thread.new do
          good, msg, code =
            begin
              if send "test_#{s}"
                [true, "OK", 0]
              else
                [false, "DOWN", i]
              end
            rescue => e
              Sentry.capture_exception e
              [false, "ERROR", i]
            end
          msg = "#{s.upcase} #{msg}"
          result[s] = { good:, msg:, code: }
        end
      end
    threads.each(&:join)
    result = result.sort.to_h

    msg_prefix, http_code =
      if result.values.all? { |v| v[:good] }
        ["PONG", 200]
      else
        sum = result.values.pluck(:code).sum
        ["DEGRADED", 500 + sum]
      end

    result_msg = result.values.pluck(:msg).join(" | ")
    message = [msg_prefix, result_msg, ENV.fetch("GIT_COMMIT", "unknown commit")].join(" : ")

    render plain: message, status: http_code
  end

  private

  def test_postgres
    Organization.count.present?
  end

  def test_faktory
    Faktory::Client.new.close.nil?
  end

  def test_grafana
    Grafana::RemoteControl.client.current_org
  end

  def test_minio
    ActiveStorage::Blob.service.exist? "README.md"
    true
  end
end
