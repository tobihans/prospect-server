# frozen_string_literal: true

class UsersController < ApplicationController
  helper UserHelper
  skip_before_action :require_login, only: %i[welcome invitation register]
  skip_before_action :check_accepted_terms, only: %i[accept_terms]
  skip_before_action :capture_missing_organization,
                     only: %i[missing_organization switch_organization accept_invitation reject_invitation]
  after_action :verify_authorized, except: %i[welcome]

  @header_section = "Organization"

  def show
    @user = User.find params[:id]
    authorize @user
    @page_title = @user
  end

  def me
    @header_section = "User"
    @user = current_user
    authorize @user
    @page_title = @user
    render :show
  end

  def welcome
    if Current.user && Current.organization&.rbf_submitting?
      redirect_to rbf_dashboard_index_path
    else
      @header_section = nil
      @page_title = t(".welcome")
      @hide_page_title = true
    end
  end

  def new
    @user = User.new organization: Current.organization
    authorize @user
  end

  def edit
    @user = authorize User.find params[:id]
    @header_section = "User" if @user == Current.user
    @page_title = "Edit #{@user}"
  end

  def missing_organization
    @user = authorize Current.user
    redirect_to root_path if Current.user.organization
  end

  def switch_organization
    authorize Current.user
    Current.user.switch_organization! params[:organization_id]
    redirect_to root_path
  end

  # A new user can be invited to an organization via the invitation form.
  # If the user is already on the Prospect platform, we invite him to join this organization
  # else he will be created like any new user.
  # Once an already existing user is invited, he can accept or reject the invite himself.

  def create
    #
    # WE MIGHT WANT TO REFACTOR THAT INTO ITS OWN SERVICE.-
    # IF WE DO TAKE THE ALMOST IDENTICAL CODE IN Organization#invite_manager! too
    # AND REFACTOR TOGETHER!
    #
    @user = User.by_normalized_email create_user_params[:email]

    organization = Organization.find(create_user_params[:organization_id])
    raise "Only admins can add users to a different organization" if
      organization != Current.organization && !Current.user.admin?

    if @user
      # user exists
      authorize @user
      if @user.user_in_organization?(create_user_params[:organization_id])
        redirect_to organizations_mine_path, notice: t(".already_part_of_organization")
      elsif create_user_params[:organization_id].to_i.in?(@user.invitations.pluck(:organization_id))
        redirect_to organizations_mine_path, notice: t(".already_invited")
      else
        @user.organizations << organization
        ou = @user.invitations.find_by(organization:)

        ou.apply_privilege_template(create_user_params[:priv_template])

        @user.mail_organization_invite(organization)
        redirect_to organizations_mine_path, notice: t(".invited")
      end
    else
      # user doesn't exist yet, create it
      @user = User.new create_user_params
      raise "Only admins can create other admins" if !Current.user.admin? && @user.admin?

      authorize @user
      if @user.save
        ou = @user.current_organization_user

        ou.apply_privilege_template(create_user_params[:priv_template])

        redirect_to organizations_mine_path, notice: t(".invited")
      else
        render :new, status: :unprocessable_content
      end
    end
  end

  def update
    @user = User.find params[:id]
    @user.assign_attributes update_user_params.compact_blank

    @user.admin = params[:user][:admin] if Current.user.admin? && params[:user][:admin].present? # only admins can create/disable other admins

    authorize @user

    if @user.save
      redirect_to @user, notice: t(".updated")
    else
      @page_title = "Edit #{@user}"
      render :edit, status: :unprocessable_content
    end
  end

  def toggle_archive
    @user = authorize User.find params[:id]

    if @user.invited?
      @user.destroy!
      redirect_to organizations_mine_path, notice: t(".destroyed")
    elsif @user.active?
      @user.archive
      @user.save!
      redirect_to organizations_mine_path, notice: t(".archived")
    elsif @user.archived?
      @user.update! archived_at: nil
      redirect_to organizations_mine_path, notice: t(".reactivated")
    else
      raise "Unhandled user state, can not toggle nothing!"
    end
  end

  def resend_invitation
    organization_user = OrganizationUser.where(user_id: params[:id], organization: Current.organization).first

    authorize organization_user.user

    UserMailer.with(user: organization_user.user, inviting_user: Current.user).invite.deliver_later

    redirect_back_or_to :root, notice: "The invitation has been resent."
  end

  def invitations
    authorize Current.user
  end

  def invitation
    @user = User.find_by! invite_code: params[:invite_code]
    authorize @user
  rescue Pundit::NotAuthorizedError
    redirect_to root_path, alert: t(".already_registered")
  end

  def accept_invitation
    authorize Current.user

    invitation = Current.user.invitations.find_by(organization_id: invitation_params[:organization_id])
    if invitation
      invitation.activate!
      flash[:notice] = t(".accepted")
      Current.user.switch_organization! invitation.organization unless Current.user.organization
    end

    redirect_to Current.user.invitations.any? ? invitations_users_path : root_path
  end

  def reject_invitation
    authorize Current.user
    invitation = Current.user.invitations.find_by(organization_id: invitation_params[:organization_id])
    if invitation
      invitation.destroy
      flash[:notice] = t(".rejected")
    end
    redirect_to invitations_users_path
  end

  def register
    @user = User.find_by! invite_code: params[:invite_code]
    authorize @user

    @user.assign_attributes register_user_params
    @user.activate

    if @user.save
      @user.mail_welcome
      redirect_to root_path, notice: t(".complete_please_login")
    else
      render :invitation, status: :unprocessable_content
    end
  end

  def accept_terms
    @user = authorize current_user
    redirect_to root_path and return if @user.all_terms_accepted?
    return unless request.post?

    values = accept_terms_user_params.to_h.map do |k, v|
      case v
      when "1"
        version = Rails.configuration.current_terms_version[k]
        [k, { accepted: true,
              ip: request.ip,
              version:,
              date: Time.zone.now }]
      else
        [k, { accepted: false }]
      end
    end
    @user.assign_attributes values.to_h
    if @user.all_terms_accepted? && @user.save
      flash[:notice] = I18n.t("users.terms_and_conditions.accepted")
      redirect_to root_path
    else
      flash[:alert] = I18n.t("users.terms_and_conditions.accept_all")
      render :accept_terms, status: :unprocessable_content
    end
  end

  private

  def invitation_params
    params.permit :organization_id
  end

  def create_user_params
    params.require(:user).permit :email, :priv_template, :organization_id
  end

  def update_user_params
    params.require(:user).permit :username, :email, :organization_id, :password, :password_confirmation
  end

  def register_user_params
    params.require(:user).permit :emanresu_in_reverse, :password, :password_confirmation
  end

  def accept_terms_user_params
    params.require(:user).permit :terms_of_use, :data_protection_policy, :code_of_conduct
  end
end
