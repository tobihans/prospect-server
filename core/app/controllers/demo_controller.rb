# frozen_string_literal: true

# this controller is just a temporary proof of concept and will be deleted when
# decoding is implemented

class DemoController < ApplicationController
  def index
    # check if organisation already has the public key, wrapped private key and iv/salt
    org = current_user.organization
    render :encrypt, locals: { public_key: org.public_key, wrapped_key: org.wrapped_key, iv: org.iv, salt: org.salt }
  end

  def post_key
    org = current_user.organization

    # check if organisation already has the public key, wrapped private key and iv/salt
    if !org.encryption_keys? &&
       (!params[:publickey].nil? && !params[:wrapped_key].nil? && !params[:iv].nil? && !params[:salt].nil?)

      org.public_key = params[:publickey]
      org.wrapped_key = params[:wrapped_key]
      org.iv = params[:iv]
      org.salt = params[:salt]
      org.save!
    end

    key = OpenSSL::PKey::RSA.new(Base64.strict_decode64(org.public_key))

    enc = key.public_encrypt_oaep(params["message"], "", OpenSSL::Digest::SHA256)
    encoded_text = Base64.strict_encode64(enc)
    redirect_to show_path(message: encoded_text, salt: org.salt, iv: org.iv, wrapped_key: org.wrapped_key)
  end

  def show
    respond_to do |format|
      format.turbo_stream { render :show, locals: { message: params[:message], salt: params[:salt], iv: params[:iv], wrapped_key: params[:wrapped_key] } }
    end
  end
end
