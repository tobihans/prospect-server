# frozen_string_literal: true

class RbfDashboardController < ApplicationController
  before_action :load_rbf_managers

  def index
    @page_title = "RBF Dashboard"
  end

  def rbf_products
    @rbf_products = Current.organization.rbf_products.group_by(&:rbf_claim_template)
  end

  def setup_project
    @rbf_manager.setup_project!
    redirect_to :root
  end

  def add_agent
    @rbf_manager.add_agent params.dig(:rbf_manager, :agent_name)
    redirect_to :root, notice: "Agent created" # rubocop:disable Rails/I18nLocaleTexts
  rescue => e
    redirect_to :root, alert: "Failed to create Agent: #{e.message}"
  end

  def toggle_agent
    s = Source.find params[:source_id]
    s.active = !s.active?
    s.save!
    redirect_to :root, notice: "#{s} is now #{s.state}"
  end

  private

  def load_rbf_managers
    @rbf_managers = Current.organization.submittable_rbf_claim_templates.map do |rct|
      Ueccc::Manager.new rct, Current.organization
    end

    return unless params[:rbf_claim_template_id] || params[:rbf_manager]

    claim_template_id = params[:rbf_claim_template_id] || params.dig(:rbf_manager, :rbf_claim_template_id)
    @rbf_claim_template = RbfClaimTemplate.find claim_template_id
    @rbf_manager = @rbf_managers.find { _1.rbf_claim_template == @rbf_claim_template }
  end
end
