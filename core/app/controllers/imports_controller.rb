# frozen_string_literal: true

class ImportsController < ApplicationController
  after_action :verify_authorized

  @header_section = "Import"

  def index
    @source_id = clean_source_id_param
    @source = Source.find_by id: @source_id

    @pagy, @imports = pagy(@source.imports.order(id: :desc))

    # for collection, check for privs on source as that's the important bit
    authorize @source, :view_data? # there is no source#index policy
  end

  def show
    @import = authorize Import.find params[:id]
    @page_title = t(".title", id: @import.id, source_name: @import.source.name)
  end

  def download
    @import = authorize Import.find params[:import_id]

    f, filename = if params[:recording]
                    [@import.recording, "source_#{@import.source_id}_#{@import.id}.webm"]
                  else
                    [@import.file, @import.file.filename.to_s]
                  end

    send_data f.download, filename:, content_type: f.content_type
  end

  def reprocess
    @import = authorize Import.find params[:import_id]
    @import.delete_data!
    @import.reprocess!

    redirect_to @import, notice: t(".reprocess")
  end

  def destroy
    @import = authorize Import.find params[:id]
    @import.destroy
    redirect_to source_path(@import.source), notice: t(".destroyed")
  end
end
