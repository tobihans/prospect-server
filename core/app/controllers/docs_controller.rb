# frozen_string_literal: true

class DocsController < ApplicationController
  skip_before_action :require_login

  @header_section = "Docs"

  def index
    @page_title = "Data Documentation"
    @datatables = Postgres::DataTable.all.sort_by(&:data_category)
  end

  def show
    @datatable = Postgres::DataTable.find params[:id]
    @page_title = "Documentation: #{@datatable.data_category}"

    respond_to do |format|
      format.html
      format.csv do
        send_data @datatable.examples_csv, filename: "prospect_example_#{@datatable.data_category}.csv"
      end
    end
  end
end
