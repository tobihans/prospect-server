# frozen_string_literal: true

module Sources
  class BuildController < ApplicationController
    @header_section = "Import"

    ##
    # A wizard with wicked gem
    # MUST Read https://jonsully.net/blog/rails-wizards-part-one/
    #
    # Between requests we store the Source attributes in in session[:source]
    # this way we don't need to create new models on the fly and have half finished stuff cleaned up
    # But 2 things:
    #  - Cookie overflow: 4k max. Better look for different session store
    #  - Conflict when opening the wizard in multiple tabs
    #
    # So: another idea for later - just put the already collected things in some hidden input and
    # bounce it back and forth (maybe json encode it) - it's stateless at least.
    #

    before_action :load_and_authorize_source
    after_action :verify_authorized

    include Wicked::Wizard

    steps(*Source::BUILD_STEPS)

    def show
      skip_step if step == "configure" && !@source.adapter&.configure_step?

      @page_title = @source.persisted? ? "Edit Data source #{@source}" : "New Data Source"

      # clean out the session store if visited from the outside / create source link on index page...
      session[:source] = nil if step == Source::BUILD_STEPS.first && starting_fresh?

      # step test automatically tests the adapter connection
      @result = @source.adapter.validation_result_wrapped if @source.adapter&.pull? && %w[test configure].include?(step)

      render_wizard
    end

    def update
      proceed =
        case step
        when "meta"
          @source.assign_attributes source_params
          # bit more complicated, to have before_validation modifiers run
          if @source.persisted?
            @source.valid?(step.to_sym) && @source.save
          else
            @source.valid?(step.to_sym)
          end
        when "connection"
          @source.assign_attributes source_params
          @source.valid?(step.to_sym) && @source.save
        when "test"
          # either render file upload, or like here we take the test file, and then render, not redirect
          # because no idea no time how to carry a file over redirects :)
          if @source.adapter.try(:manual_upload?) && source_params[:example].present?
            @source.adapter.raw_data = source_params[:example]&.read
            @result = @source.adapter.validation_result_wrapped
            false
          else
            # normally we just wanna redirect...
            true
          end
        when "configure"
          @source.assign_attributes source_params
          @result = @source.adapter.validation_result_wrapped
          @source.save
        when "activate"
          @source.assign_attributes source_params
          @source.save
          redirect_to source_path(@source), notice: t("sources.updated") and return
        end

      session[:source] = @source.attributes

      if proceed
        step_to_go = params[:commit].to_s.downcase == "back" ? previous_step : next_step
        redirect_to_next step_to_go
      else
        render_wizard nil, status: :unprocessable_content
      end
    end

    private

    def source_id_param
      params.permit(:source_id)[:source_id]
    end

    def project_id_param
      params.permit(:project_id)[:project_id]
    end

    def source_params
      return {} unless params[:source]

      params.require(:source).permit :name,
                                     :kind,
                                     :secret,
                                     :source_id,
                                     :data_category,
                                     :active,
                                     :example,
                                     { details: {} }
    end

    def starting_fresh?
      request.referer.present? && !URI(request.referer).path.start_with?("/build_source")
    end

    def load_and_authorize_source
      @source = if params[:source_id].present?
                  Source.find source_id_param
                elsif session[:source].present? && !starting_fresh?
                  Source.new session[:source]
                else
                  Source.new project_id: project_id_param
                end

      @source.new_record? ? authorize(@source, :create?) : authorize(@source, :update?)
    end
  end
end
