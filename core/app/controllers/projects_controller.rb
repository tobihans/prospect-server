# frozen_string_literal: true

class ProjectsController < ApplicationController
  after_action :verify_authorized

  @header_section = "Import"

  def index
    authorize :project
    @projects = Project.includes(:sources, :visibilities).
                where(organization: Current.organization).default_order
    @visibilities = Visibility.includes(project: :sources).shared_to_organization(Current.organization) # shared_to_organization
  end

  def show
    @project = authorize Project.find params[:id]
    @page_title = @project.to_s
  end

  def new
    @project = authorize Project.new
    @page_title = "New Project"
  end

  def edit
    @project = authorize Project.find params[:id]
    @page_title = "Edit Project #{@project}"
  end

  def create
    @project = Project.new(project_params)
    @project.organization = Current.organization
    authorize @project

    if @project.save
      redirect_to project_path(@project), notice: t(".created")
    else
      render :new, status: :unprocessable_content, alert: t(".invalid")
    end
  end

  def update
    @project = Project.find params[:id]
    @project.assign_attributes(project_params)
    authorize @project

    if @project.save
      redirect_to project_path(@project), notice: t(".updated")
    else
      render :edit, status: :unprocessable_content, alert: t(".invalid")
    end
  end

  def destroy
    @project = authorize Project.find params[:id]

    if @project.sources.empty? && @project.destroy
      redirect_to projects_path, notice: t(".destroyed")
    else
      redirect_to @project, alert: t(".destroy_blocked")
    end
  end

  # Show / Edit data from Projects setup which is managed by UECCC::Manager
  def view_data_rbf
    rbf_claim_template = RbfClaimTemplate.find params[:rbf_claim_template_id]
    manager = Ueccc::Manager.new rbf_claim_template

    @project = authorize manager.project

    sources = @project.sources
    source_ids = sources.pluck :id

    tables = Postgres::DataTable.select { _1.active_record.where(source_id: source_ids).limit(1).count.positive? }

    endpoints = Api::Base.routes.
                select { |r| tables.any? { r.origin =~ %r{out/#{_1.data_category.underscore}$} } }.
                map { _1.origin.gsub(":version", _1.version) }

    backoffice_sources = {
      shs: manager.backoffice_sales_source,
      meters: manager.backoffice_sales_source,
      payments_ts: manager.backoffice_payments_source
    }.with_indifferent_access

    @options = tables.zip(endpoints).to_h do |tb, ep|
      backoffice_source = backoffice_sources[tb.data_category]

      if backoffice_source
        form = backoffice_source.adapter.dynamic_form
        update_path = dynamic_form_submit_sources_path(slug: backoffice_source.details["slug"])
        mapper = form.form_data.values.each_with_object({}) do |part, obj|
          obj[part["data_table"]] ||= {}
          part["fields"].each do |field, options|
            next unless options["data_column"]

            obj[part["data_table"]][options["data_column"]] =
              { field:,
                format: options["formatter"] || "plaintext",
                encrypted: !options["encrypt"].nil? || tb.column(options["data_column"])&.encrypted }
          end
        end
      else
        update_path = nil
        mapper = nil
      end

      [
        tb.data_category.underscore,
        {
          ajaxURL: "#{root_url}/api/#{ep[1..]}",
          ajaxConfig: {
            headers: {
              Authorization: "Bearer #{Current.user.organization.api_key}"
            }
          },
          ajaxParams: {
            "q[source_id_in]": source_ids,
            last_page: true
          },
          # will fit the default 25 lines, if we use auto as height,
          # the browser will scroll to the top of the page each time
          # we flip to another page, that's a little annoying
          height: "800px",
          layout: "fitData",
          # if autoColumns is only set to true, the table is build based on the first element
          # but every entry could have some other attributes in the custom column, so we can
          # scan all the entries for column keys via 'full'
          autoColumns: "full",
          pagination: true,
          paginationMode: "remote",
          paginationSize: 25,
          filterMode: "remote",
          sortMode: "remote",
          paginationSizeSelector: [10, 25, 50, 100],
          data_table: tb.columns.each_with_object({}) { |c, o| o[c.name] = c.config },
          update_path: (tb.updatable? ? update_path : nil),
          mapper:,
          data_category: tb.data_category
        }
      ]
    end
    @from_rbf_dashboard = true
    @title_mapping = { "payments_ts" => "Payments", "shs" => "Sales SHS", "meters" => "Sales" }
    @allow_wirdescreen = true
    render :view_data
  end

  # display data in generic format - no way to update
  def view_data
    @project = authorize Project.find params[:project_id]
    source_ids = @project.sources.pluck :id

    tables = Postgres::DataTable.select { _1.active_record.where(source_id: source_ids).limit(1).count.positive? }

    endpoints = Api::Base.routes.select { |r| tables.any? { r.origin =~ %r{out/#{_1.data_category.underscore}$} } }.
                map { _1.origin.gsub(":version", _1.version) }

    @title_mapping = {}
    @options = tables.zip(endpoints).to_h do |tb, ep|
      [
        tb.data_category.underscore,
        {
          ajaxURL: "#{root_url}/api/#{ep[1..]}",
          ajaxConfig: {
            headers: {
              Authorization: "Bearer #{Current.user.organization.api_key}"
            }
          },
          ajaxParams: {
            "q[source_id_in]": source_ids,
            last_page: true
          },
          # will fit the default 25 lines, if we use auto as height,
          # the browser will scroll to the top of the page each time
          # we flip to another page, that's a little annoying
          height: "800px",
          layout: "fitData",
          # if autoColumns is only set to true, the table is build based on the first element
          # but every entry could have some other attributes in the custom column, so we can
          # scan all the entries for column keys via 'full'
          autoColumns: "full",
          filterMode: "remote",
          pagination: true,
          paginationMode: "remote",
          paginationSize: 25,
          paginationSizeSelector: [10, 25, 50, 100],
          data_table: tb.columns.each_with_object({}) { |c, o| o[c.name] = c.config },
          update_path: nil,
          mapper: nil,
          data_category: tb.data_category.underscore
        }
      ]
    end
  end

  private

  def project_params
    return {} unless params[:project]

    params.require(:project).permit :name, :description
  end
end
