# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit::Authorization
  include Pagy::Backend

  before_action :require_login
  before_action :set_current_user
  before_action :capture_missing_organization
  before_action :check_accepted_terms
  before_action :expose_header_section
  around_action :switch_locale

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def self.header_section
    @header_section ||= "Home"
  end

  def append_info_to_payload(payload)
    super
    payload[:user_id] = Current.user&.id
    payload[:organization_id] = Current.organization&.id
  end

  private

  def set_current_user
    Current.user = current_user
    Sentry.set_user(id: current_user&.id, email: current_user&.email)
  end

  def user_not_authorized(exception)
    flash[:alert] = t("users.not_authorized")
    Sentry.capture_exception exception
    redirect_back_or_to(root_path)
  end

  def not_authenticated
    redirect_to root_path
  end

  def check_accepted_terms
    redirect_to accept_terms_users_path if Current.user && !Current.user&.all_terms_accepted?
  end

  def switch_locale(&)
    locale = params[:locale] || I18n.default_locale # here you would set the local, e.g. from users preferences
    I18n.with_locale(locale, &) # avoids locale leakage to other requests in same thread
  end

  def expose_header_section
    # make sure we activate the right button in the page header.
    # use header section either from controller method and fall back to Controller Class Sections.
    # This allows to set default one for whole controller and change if needed
    @header_section = defined?(@header_section) ? @header_section : self.class.header_section
  end

  def authenticate_admin_user!
    redirect_to root_path unless Current.user.admin?
  end

  def capture_missing_organization
    redirect_to me_missing_organization_users_path if Current.user&.active? && !Current.user.organization
  end

  # make it nil or integer to prevent any weird attacks...
  def clean_source_id_param
    params[:source_id].present? ? params[:source_id].to_i : nil
  end
end
