# frozen_string_literal: true

class OrganizationsController < ApplicationController
  after_action :verify_authorized

  @header_section = "Organization"

  def show
    @organization = authorize Organization.find params[:id]
  end

  def show_token
    @organization = authorize Organization.find params[:organization_id]
    @visible = ActiveRecord::Type::Boolean.new.deserialize params[:visible]
  end

  def edit
    @organization = authorize Organization.find params[:id]
  end

  def update
    organization = authorize Organization.find params[:id]

    # do not allow to change the encryption keys as this would make all encrypted data unreadable
    # TODO: explicitly permit and name the encryption params
    organization.assign_attributes organization_params.except(:name) unless organization.encryption_keys?

    # only admins can change the name of an org to prevent pretending to be another org
    organization.name = organization_params[:name] if Current.user.admin?
    organization.save!

    if organization.rbf_submitting?
      redirect_to root_path, notice: t(".updated")
    else
      redirect_to organization_path(organization), notice: t(".updated")
    end
  end

  def mine
    # show my organization
    @organization = authorize Current.organization
    render :show
  end

  private

  def create_organization_params
    params.require(:organization).permit :name
  end

  def organization_params
    params.require(:organization).permit(:name, :iv, :salt, :wrapped_key, :public_key)
  end
end
