# frozen_string_literal: true

class FaktoryPusher
  class << self
    # see https://github.com/contribsys/faktory/wiki/The-Job-Payload
    def push(jobtype, args = [], queue: "etl", retry: 3, reserve_for: 5 * 60 * 60, jid: SecureRandom.hex)
      c = Faktory::Client.new debug: !Rails.env.production?
      c.push({ jobtype:, args:, queue:, retry:, reserve_for:, jid: })
      c.close
    end
  end
end
