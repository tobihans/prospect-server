# frozen_string_literal: true

module Postgres
  class DataTable
    DEFAULT_OPTIONS = {
      required: false,
      exclude_from_ingress: false,
      pseudonymized: nil,
      encrypted: nil,
      blurred: false
    }.with_indifferent_access.freeze

    DEFAULT_COL_DEFAULT_OPTS = { required: true, exclude_from_ingress: true }.freeze

    DEFAULT_COLUMNS =
      {
        created_at: { type: :datetime, desc: "Timestamp when row was inserted", example: "2021-05-03 6:23:55" },
        updated_at: { type: :datetime, desc: "Timestamp when row was updated", example: "2021-05-03 6:23:55", out_api_filter: true },
        source_id: { type: :integer, desc: "References Source of data", example: 1 },
        import_id: { type: :integer, desc: "References Import of data", example: 1 },
        organization_id: { type: :integer, desc: "References Organization of data", example: 1 },
        custom: { type: :jsonb, desc: "Saves additional imported data out of standard structure", example: '{ "some": "key" }' },
        data_origin: { type: :string, desc: "Origin of the data, generic or name of the api", example: "generic" }
      }.
      transform_values { |v| DEFAULT_COL_DEFAULT_OPTS.merge(v) }.
      with_indifferent_access.freeze

    DEFAULT_AGGREGATE_TS_COLUMNS = DEFAULT_COLUMNS.without :import_id

    HISTORY_COLUMNS =
      {
        last_import_id: { type: :integer, required: false, desc: "Id of the last import which updated the record", example: 337 },
        last_source_id: { type: :integer, required: false, desc: "Id of the last Source which updated the record", example: 33 }
      }.
      transform_values { |v| DEFAULT_COL_DEFAULT_OPTS.merge(v) }.
      with_indifferent_access.freeze

    STRUCTURE_PATH = Rails.root.join("db/data_tables").freeze

    attr_reader :data_category

    def initialize(data_category)
      @data_category = data_category
    end

    class << self
      include Enumerable

      def all
        @all ||= Dir.glob("*.yml", base: STRUCTURE_PATH).
                 map { |f| File.basename f, ".yml" }.
                 map { |cat| DataTable.new cat }
      end

      def non_custom
        @non_custom ||= all.reject { |dt| dt.data_category == "custom" }
      end

      # we include Enumerable and implement each
      # so we get all the bells and whistles
      # to enumerate on the data tables
      def each(&)
        all.each(&)
      end

      def find(name)
        all.find { _1.data_category == name.to_s }
      end

      def public_ar(data_category)
        find(data_category).active_record
      end

      def ar(data_category, organization)
        find(data_category).active_record.dup.tap do |active_record|
          active_record.table_name = Naming.db_organization_data_view(organization, data_category)
        end
      end
    end

    # add here if you need a standard active record method
    # like .last etc. on the collection of all data tables
    %i[first count].each do |method_name|
      define_singleton_method method_name do
        all.send method_name
      end
    end

    def default_columns
      if aggregate_ts?
        DEFAULT_AGGREGATE_TS_COLUMNS
      else
        DEFAULT_COLUMNS
      end
    end

    def history_columns
      history? ? HISTORY_COLUMNS : []
    end

    def structure
      @structure ||= YAML.load_file(structure_file_path).with_indifferent_access
    end

    #
    # returns array of Column. Those are the configured columns, database column names can be found with sql_columns().
    #
    def columns
      @columns ||=
        structure[:columns].map { |name, config| Column.new self, name, DEFAULT_OPTIONS.merge(config) } +
        default_columns.map { |name, config| Column.new self, name, DEFAULT_OPTIONS.merge(config), default: true } +
        history_columns.map { |name, config| Column.new self, name, DEFAULT_OPTIONS.merge(config), default: true }
    end

    def column(name)
      columns.find { |c| c.name == name.to_s }
    end

    def filter_columns
      columns.select(&:out_api_filter)
    end

    def sql_columns
      columns.map(&:sql_columns).flatten
    end

    def primary_key_column
      columns.select(&:primary_key?).first
    end

    def examples(only_in: true)
      columns.
        select { |c| only_in ? c.exposed_for_ingress? : true }.
        to_h { |c| [c.name, c.example] }
    end

    def examples_csv(only_in: true, lines: 1)
      ex = examples(only_in:)
      CSV.generate do |csv|
        csv << ex.keys
        lines.times { csv << ex.values }
      end
    end

    def example(column_name)
      column(column_name).example
    end

    def structure_file_path
      STRUCTURE_PATH.join "#{data_category}.yml"
    end

    def table_name
      Naming.db_data_table data_category
    end

    def ts?
      data_category.end_with? "_ts"
    end

    def aggregate_ts?
      structure[:aggregate_ts]
    end

    def desc
      structure[:desc]
    end

    def history?
      structure[:history]
    end

    def exposed_to_out_api?
      Rails.env.production? ? structure.fetch(:exposed_to_out_api, true) : true
    end

    def exposed_to_in_api?
      Rails.env.production? ? structure.fetch(:exposed_to_in_api, true) : true
    end

    def updatable?
      structure.fetch :updatable, false
    end

    def caster(array_of_hashes, update_mode: false)
      Caster.new self, array_of_hashes, update_mode:
    end

    def castable_as_timestamp?(val)
      return true if val.blank?

      if val.length > 10
        Time.zone.strptime val, "%Y-%m-%d %H:%M:%S"
      else
        Time.zone.strptime val, "%Y-%m-%d"
      end
      true
    rescue
      false
    end

    def castable_as_numeric?(val)
      return true if val.blank?

      Float val
      true
    rescue
      false
    end

    def custom_column_conf(array_of_hashes)
      conf = {}

      all_keys = array_of_hashes.map(&:keys).flatten.uniq
      all_keys -= columns.map(&:name)

      all_keys.each do |k|
        # only blank vals - lets make it text, otherwise it would all become a timestamp
        if array_of_hashes.all? { |h| h[k].blank? }
          conf[k] = "text"
          next
        end

        # first try if its all parseable as timestamp
        if array_of_hashes.all? { |h| castable_as_timestamp?(h[k].to_s) }
          conf[k] = "timestamp"
          next
        end

        # then check if we can cast it all as numeric
        if array_of_hashes.all? { |h| castable_as_numeric?(h[k].to_s) }
          conf[k] = "numeric"
          next
        end

        # or just default to text
        conf[k] = "text"
      end

      conf.to_a
    end

    def custom_column_conf_errors(conf)
      errors = []
      Array(conf).map(&:first).each do |key|
        if key.blank?
          errors << "Column without title detected."
        elsif key =~ /^\s/ || key =~ /\s$/
          errors << "Column title can not start or end with whitespace: '#{key}'."
        elsif key !~ /^[a-zA-Z0-9 _-]+$/
          errors << "Column title can only consist of letter, number, underscore, dash, space: '#{key}'."
        end
      end
      errors
    end

    def p14ner
      P14ner.new self
    end

    def rsaer
      RsaEr.new self
    end

    def apply_rsa!(array_of_hashes, organization)
      rsaer.apply! array_of_hashes, organization
    end

    alias_method "apply_encryption!", :apply_rsa!

    def apply_p14n!(array_of_hashes)
      p14ner.apply! array_of_hashes
    end

    def apply_cleaning!(array_of_hashes, organization)
      cleaner = Cleaner.new(organization)

      # walk through all columns and apply encryption
      array_of_hashes.each do |h|
        rsaer.enc_text_columns.each do |col|
          cleaner.clean_key h, col.name, :text, :encrypt, delete_key: false
        end
        rsaer.enc_geo_prefixes.each do |prefix, fields|
          cleaner.clean_key h, [fields[:lat], fields[:lon]], :geo, :encrypt, delete_key: false, key_prefix: prefix, key_full: true, ignore_missing: true
        end
        p14ner.p14n_text_columns.each do |col|
          cleaner.clean_key h, col.name, :text, :pseudonymize
        end
        p14ner.p14n_phone_number_columns.each do |col|
          cleaner.clean_key h, col.name, :phone_number, :pseudonymize
        end
        p14ner.p14n_geo_prefixes.each do |prefix, fields|
          cleaner.clean_key h, [fields[:lat], fields[:lon]], :geo, :pseudonymize, ignore_missing: true, key_full: true, key_prefix: prefix
        end
        rsaer.enc_text_columns.each do |col|
          h.delete col.name unless h[col.name].nil?
        end
        rsaer.enc_geo_prefixes.each_value do |fields|
          h.delete fields[:lon] unless h[fields[:lon]].nil?
          h.delete fields[:lat] unless h[fields[:lat]].nil?
        end
      end

      # walk through all columns and apply pseudoniymization

      # walk through all columns again and delete original fields if still exist
      # there might be a few left that are only encrypted but not pseudonymized
    end

    def to_s
      "#{self.class} #{data_category}"
    end

    def active_record
      Data.const_get(data_category.camelcase)
    end
  end

  # create wrapper models for all data tables
  Postgres::DataTable.each do |dt|
    next if Data.const_defined? dt.data_category.camelcase

    klass = Class.new(ApplicationRecord) do
      self.table_name = dt.table_name
      self.primary_key = dt.primary_key_column.name
    end

    # this will cast the enum column in a ransacker search to text
    # ransacker checks if a column exists but will happily search with
    # an invalid enum value which will fail on the database side catastrophically
    # this basically convert:
    # ... WHERE enum_column = 'Value' ...
    # to
    # ... WHERE text(enum_column) = 'Value' ...
    # if we don't do this, an invalid enum value will throw an "invalid input value for enum" error
    # because Postgres tries to convert the value into an enum not the column to text
    dt.columns.select { |c| c.type.to_sym == :enum }.each do |c|
      klass.ransacker c.name do
        column_cast = "text(#{c.name})"
        Arel.sql(column_cast)
      end
    end

    Data.const_set dt.data_category.camelcase, klass
  end
end
