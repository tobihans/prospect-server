# frozen_string_literal: true

module Postgres
  module RemoteControl
    class << self
      ###
      # The big class to control Postgres and manage data tables and organization views.
      #
      # With a bit of convention:
      # methods starting with query_... are used to build the query string only.
      # all queries are build this way and then executed in another method. For easy testing and better debugging.
      #

      def query!(sql)
        ActiveRecord::Base.connection.execute sql
      end

      # params has the form [{ value: value_param_1 }, { value: value_param_2 }]
      # see https://www.rubydoc.info/gems/pg/0.17.1/PG%2FConnection:exec_params
      def raw_query_with_params!(sql, params)
        ActiveRecord::Base.connection.raw_connection.exec_params(sql, params)
      end

      def current_schema
        ActiveRecord::Base.connection.current_schema
      end

      def current_version
        # we assume no one will run postgres older 10...
        query!("SHOW server_version").first["server_version"].match(/(?<major>\d\d)\..*/)[:major].to_i * 100
      rescue
        Rails.logger.error "Could not determine PostgreSQL version or it's < 10, which is no longer supported"
        false
      end

      def timescale_available?
        query!("SELECT * FROM pg_available_extensions WHERE name = 'timescaledb'").any?
      end

      def timescale_enabled?
        query!("SELECT * FROM pg_available_extensions WHERE installed_version IS NOT NULL AND name = 'timescaledb'").any?
      end

      def all_tables
        r = query! "SELECT table_name FROM information_schema.tables WHERE table_schema = '#{current_schema}'"
        r.values.flatten
      end

      def data_table_sql_types
        @data_table_sql_types ||= query_data_table_sql_types!
      end

      def query_data_table_sql_types!
        q = <<~SQL.squish
          SELECT table_name, column_name, udt_name, datetime_precision
          FROM information_schema.columns
          WHERE table_name LIKE 'data_%'
        SQL
        r = query! q

        types = {}
        r.to_a.each do |col|
          type_name = col["udt_name"]
          # otherwise we might get mismatches later udt_name has no precision, but not for date!
          type_name += "(#{col['datetime_precision']})" if col["datetime_precision"] && type_name != "date"

          types[col["table_name"]] ||= {}
          types[col["table_name"]][col["column_name"]] = type_name
        end
        types
      end

      def all_visibility_views(parse: false)
        q = <<~SQL.squish
          SELECT table_name
          FROM information_schema.tables
          WHERE table_schema = 'visibilities'
          AND table_name LIKE '#{Naming.db_visibility_prefix(prefix: true)}%'
        SQL
        r = query! q
        names = r.values.flatten
        parse ? names.map { parse_visibility_view _1 } : names
      end

      def default_visibility_views
        q = <<~SQL.squish
          SELECT table_name
          FROM information_schema.tables
          WHERE table_schema = 'visibilities'
          AND table_name LIKE '#{Naming.visibility_default_prefix(prefix: true)}%'
        SQL
        r = query! q
        r.values.flatten
      end

      def parse_visibility_view(view_name)
        parts = view_name.split "_", 6
        {
          view: view_name,
          organization_id: parts[1].to_i,
          visibility_id: parts[2].to_i,
          project_id: parts[3].to_i,
          source_id: parts[4].to_i,
          table: parts[5]
        }
      end

      def all_organization_views(organization: nil, data_category: nil)
        schema_q = if organization
                     "table_schema = '#{Naming.db_organization_schema(organization)}'"
                   else
                     "table_schema LIKE '#{Naming.db_organization_prefix(prefix: true)}%'"
                   end

        q = if data_category
              <<~SQL.squish
                SELECT table_schema, table_name
                FROM information_schema.tables
                WHERE table_name = '#{Naming.db_data_table(data_category)}'
                AND #{schema_q}
              SQL
            else
              <<~SQL.squish
                SELECT table_schema, table_name
                FROM information_schema.tables
                WHERE #{schema_q}
              SQL
            end

        r = query! q
        r.values.map { |ts, tn| [ts, tn].join(".") }
      end

      # param not used but still in some old migrations
      # we just drop everything.
      # Make sure to have
      def drop_all_organization_views!(_data_category = nil)
        all_organization_views.each do |v|
          drop_view! v
        end
      end

      def all_custom_views_everywhere
        q = <<~SQL.squish
          SELECT table_schema, table_name
          FROM information_schema.tables
          WHERE table_name LIKE '#{Naming.db_custom_view_prefix(prefix: true)}%'
        SQL
        r = query! q
        r.values.map { |ts, tn| [ts, tn].join(".") }
      end

      def custom_views_of_organization(organization)
        q = <<~SQL.squish
          SELECT table_name
          FROM information_schema.tables
          WHERE table_name LIKE '#{Naming.db_custom_view_prefix(prefix: true)}%'
          AND table_schema = '#{Naming.db_organization_schema(organization)}'
        SQL
        r = query! q
        r.values.flatten
      end

      def data_tables
        @data_tables ||= all_tables.select { |t| t.start_with? Naming.db_data_table_prefix(prefix: true) }
      end

      def data_categories
        @data_categories ||= data_tables.map { |dt| dt.partition(Naming.db_data_table_prefix(prefix: true)).last }.sort
      end

      def count_as_organization(organization, data_category: nil, custom_view: nil, source_id: nil, limit: 10_000)
        return 0 if data_category && all_organization_views(organization:, data_category:).none?

        q = query_count_as_organization(organization, data_category:, custom_view:, source_id:, limit:)
        r = query! q
        r.values.first.first
      end

      # this is a quick fix to get the exports overview load fast,
      # as somehow our views dont use the index on source_id
      # but as visibility views will change anyway, we just do that quickly here...
      def query_count_as_organization(organization, data_category: nil, custom_view: nil, limit: nil, source_id: nil)
        from = if data_category
                 Naming.db_organization_data_view(organization, data_category)
               elsif custom_view
                 Naming.db_organization_custom_view(organization, custom_view)
               else
                 raise "Either data_category or custom_view must be given"
               end

        where = "source_id = #{source_id.to_i}" if source_id

        # subquery = limit applies!
        [
          "SELECT COUNT(*) FROM (SELECT 1 FROM #{from}",
          ("WHERE #{where}" if where),
          ("LIMIT #{limit}" if limit),
          ") AS x"
        ].compact.join(" ")
      end

      def select_as_organization(organization, select, data_category: nil, custom_view: nil, limit: nil, source_id: nil)
        q = query_select_as_organization(organization, select, data_category:, custom_view:, limit:, source_id:)
        query! q
      end

      def query_select_as_organization(organization, select, data_category: nil, custom_view: nil, limit: nil, source_id: nil)
        from = if data_category
                 Naming.db_organization_data_view organization, data_category
               elsif custom_view
                 Naming.db_organization_custom_view organization, custom_view
               else
                 raise "Either data_category or custom_view must be given"
               end
        [
          "SELECT #{select} FROM #{from}",
          ("LIMIT #{limit}" if limit),
          ("WHERE source_id = #{source_id}" if source_id)
        ].compact.join(" ")
      end

      ## Wrap Migrations into this if you rename or drop data_ columns
      ## Because it would normally complain if you manipulate the columns
      ## just drop everything! _data_categories is just there for backward compatibility
      def migration_without_views(*_deprecated, &block)
        Rails.logger.warn <<~MSG
          Postgres::RemoteControl.migration_without_views is deprecated as its not rollback compatible.
          This is replaced by db:prepare_migration which is automatically run before db:migrate
        MSG

        drop_all_custom_views!
        drop_all_organization_views!
        drop_all_visibility_views!

        block.call

        after_migration!
      end

      def after_migration!
        ActiveRecord::Migration.say "Postgres::RemoteControl.after_migration!" unless Rails.env.test?

        ActiveRecord::Migration.say "setup_all_organizations!", true unless Rails.env.test?
        setup_all_organizations!

        ActiveRecord::Migration.say "create_all_views!", true unless Rails.env.test?
        create_all_views!

        ActiveRecord::Migration.say "create_shared_visibilities!", true unless Rails.env.test?
        create_all_shared_visibilities!
      rescue => e
        puts <<~TXT # rubocop:disable Rails/Output
          Failed to run Postgres::RemoteControl.after_migration!
          Error: #{e.message}
          Please run manually:
            Postgres::RemoteControl.setup_all_organizations!
            Postgres::RemoteControl.create_all_views!
          Hint: If create_all_views! fails it could be the problem that data *.ymls and db diverged,
                especially if multiple migrations are used to bring the structure into sync.
                Make just sure that Postgres::RemoteControl.after_migration! passes after all migrations are done!
        TXT
      end

      # Check if we should auto-install the postgres function
      def install_fn_org_role?
        ActiveRecord::Type::Boolean.new.deserialize(
          ENV.fetch("INSTALL_POSTGRES_FN_ORG_ROLE", Rails.env.local?)
        )
      end

      # Install PG Function to Create Organization roles
      def install_fn_org_role!
        q = Rails.root.join("db/fn_create_org_role.sql").read.squish
        ActiveRecord::Base.connection.execute q
      end

      # call this in initial migration, e.g. we already have existing organizations, and need the postgres stuff
      def setup_all_organizations!
        Organization.find_each do |organization|
          setup_organization! organization
        end
      end

      # run me after migrations touching the data tables!
      def create_all_views!
        Visibility.full_views_refresh!
        RbfClaimTemplate.full_views_refresh!
      end

      def create_shared_visibility!(organization)
        sql = <<~SQL.squish
          CREATE OR REPLACE VIEW #{Naming.db_organization_shared_visibilities_view(organization)} AS SELECT
            s.name AS source_name, s.id AS source_id, p.name AS project_name, p.id AS project_id, o.name AS org_name, o.id AS org_id
            FROM visibilities v
            JOIN projects p ON v.project_id = p.id
            JOIN sources s ON s.project_id = p.id
            JOIN organizations o ON o.id = p.organization_id
            WHERE v.organization_id = #{organization.id}
        SQL
        Postgres::RemoteControl.create_view_safe! sql
      end

      def create_all_shared_visibilities!
        Organization.find_each do |organization|
          create_shared_visibility! organization
        end
      end

      def create_visibilities_schema!
        query! "CREATE SCHEMA IF NOT EXISTS #{Naming.db_visibility_schema}"
      end

      # more than a wooden hammer, be careful, this is just utility...
      def drop_all_custom_views!
        all_custom_views_everywhere.each { drop_view! _1 }
      end

      def drop_all_visibility_views!
        all_visibility_views.each { drop_view! "#{Naming.db_visibility_schema}.#{_1}" }
        default_visibility_views.each { drop_view! "#{Naming.db_visibility_schema}.#{_1}" }
      end

      def query_drop_view(view_name)
        "DROP VIEW IF EXISTS #{view_name} CASCADE"
      end

      def drop_view!(view_name)
        query! query_drop_view(view_name)
      end

      def create_views_unsafe!(view_queries)
        combined = Array(view_queries).map { _1.end_with?(";") ? _1 : "#{_1};" }.join
        query! combined
      end

      # CREATE OR REPLACE VIEW can only add new columns at the end, so we can try that first.
      # If it fails, because columns are changed in middle, we drop the view and do it fresh
      # We do that in transactions, as a failure should not rollback rollback everything we did so far
      def create_view_safe!(query)
        ActiveRecord::Base.transaction requires_new: true do
          query! query
        end
      rescue ActiveRecord::StatementInvalid
        ActiveRecord::Base.transaction requires_new: true do # open new transaction as the old failed
          view_name = query.match(/CREATE OR REPLACE VIEW (\S+) AS SELECT/).captures.first
          drop_view! view_name
          query! query
        end
      end

      def fn_create_org_role_q(name, password)
        "SELECT fn_create_org_role('#{name}', '#{password}')"
      end

      def setup_organization!(organization)
        q = fn_create_org_role_q Naming.db_organization_user(organization), organization.postgres_password
        query! q
      end
    end
  end
end
