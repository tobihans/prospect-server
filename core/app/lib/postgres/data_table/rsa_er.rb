# frozen_string_literal: true

module Postgres
  class DataTable
    class RsaEr
      attr_reader :data_table, :data

      def initialize(data_table)
        @data_table = data_table
      end

      def enc_text_columns
        @enc_text_columns ||= data_table.columns.select { |c| c.encrypted == "text" }
      end

      def enc_geo_columns
        @enc_geo_columns ||= data_table.columns.select { |c| c.encrypted == "geo" }
      end

      def enc_geo_prefixes
        @enc_geo_prefixes ||=
          enc_geo_columns.map(&:encrypt_geo_attribs).pluck(:prefix).map(&:presence).index_with do |pre|
            {
              lat: [pre, "latitude"].compact.join("_"),
              lon: [pre, "longitude"].compact.join("_")
            }
          end
      end

      def apply!(array_of_hashes, organization)
        array_of_hashes.each do |h|
          enc_text_columns.each do |col|
            RsaOaepEncrypt.text_key h, col.name, organization, ignore_missing: true
          end
          enc_geo_prefixes.each do |pre, fields|
            RsaOaepEncrypt.geo_key h, fields[:lat], fields[:lon], organization, key_prefix: pre, key_full: true, ignore_missing: true
          end
        end
      end
    end
  end
end
