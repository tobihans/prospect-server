# frozen_string_literal: true

module Postgres
  class DataTable
    class P14ner
      attr_reader :data_table, :data

      def initialize(data_table)
        @data_table = data_table
      end

      def p14n_text_columns
        @p14n_text_columns ||= data_table.columns.select { |c| c.pseudonymized == "text" }
      end

      def p14n_phone_number_columns
        @p14n_phone_number_columns ||= data_table.columns.select { |c| c.pseudonymized == "phone_number" }
      end

      def p14n_geo_columns
        @p14n_geo_columns ||= data_table.columns.select { |c| c.pseudonymized == "geo" }
      end

      def p14n_geo_prefixes
        @p14n_geo_prefixes ||=
          p14n_geo_columns.map(&:p14n_geo_attribs).pluck(:prefix).map(&:presence).index_with do |pre|
            {
              lat: [pre, "latitude"].compact.join("_"),
              lon: [pre, "longitude"].compact.join("_")
            }
          end
      end

      def apply!(array_of_hashes)
        array_of_hashes.each do |h|
          p14n_text_columns.each do |col|
            P14n.text_key h, col.name, ignore_missing: true
          end
          p14n_phone_number_columns.each do |col|
            P14n.phone_number_key h, col.name, ignore_missing: true
          end
          p14n_geo_prefixes.each do |pre, fields|
            P14n.geo_key h, fields[:lat], fields[:lon], key_prefix: pre, key_full: true, ignore_missing: true
          end
        end
      end
    end
  end
end
