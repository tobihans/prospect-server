# frozen_string_literal: true

##
# A IXO Certify service
#
# make sure you have the IXO API container configured via ENV and running
#   docker compose ixo up
#
# needs a CSV file with verified fuel purchase transactions for initialization, required columns:
#   - supamoto_transaction_id : Mobile money provider transaction id - used to identify a payment (string)
#   - supamoto_wallet_operator : downcased name of mobile money provider, used to match a payment (string)
#   - supamoto_amount : amount of payment (float)
#   - supamoto_verified : 1 for verified, other int for reject (int)
#
# currently run manually like so:
#   bundle exec rails r
#     'Ixo::Certify.new(transactions_path: "/Users/karl/Downloads/supamoto_verified_20230629.csv",
#                       dry_run: false, reject: true).run!'
#
# check thing if claim certification is committed at claim/%{claim_id}, e.g.
#   https://devnet-cellnode.ixo.earth/claims/claim/bafkreiemkbvptyx7g6hylv65rqnn2hq5s7cgv6rwignilya36y2dgnchai
#
module Ixo
  class CertifyScript
    # This verification model will commit into the chain - change it when we update the verification method
    VERIFICATION_MODEL = "ProspectFuelPurchaseCrmMoneyProviderXCheck"
    VERIFICATION_MODEL_VER = "0.0.1"

    CLAIMS_BASE_URL = ENV.fetch "IXO_API_URL", nil
    CLAIMS_AUTH_HEADER = ENV.fetch "IXO_API_AUTHORIZATION", nil
    ORACLE_DID = ENV.fetch "IXO_ISSUER_DID", nil
    COLLECTION_ID = ENV.fetch "IXO_COLLECTION_ID", nil

    FETCH_BATCH_SIZE = 800
    OPEN_CLAIMS_URL = "#{CLAIMS_BASE_URL}collection/#{COLLECTION_ID}/claims?type=FuelPurchase&status=0&take=#{FETCH_BATCH_SIZE}".freeze

    DATA_PARALLEL = 3
    DATA_BATCH_SIZE = 6
    DATA_BATCH_SLEEP = 5.seconds
    CLAIM_DATA_URL = "#{CLAIMS_BASE_URL}claim/%{claim_id}/data".freeze

    CERTIFY_BATCH_SIZE = 6
    CERTIFY_BATCH_SLEEP = 0 # 1.second
    CERTIFY_URL = "#{CLAIMS_BASE_URL}certify".freeze

    HEADERS = { "Content-Type" => "application/json",
                "Accept" => "application/json",
                "Authorization" => CLAIMS_AUTH_HEADER }.freeze

    CSV_DELIMITER = ","

    REJECT_ERRORS = %i[not_verified prospect_not_found].freeze

    attr_reader :transactions_path, :dry_run, :reject, :claim_time, :log_name_time
    attr_accessor :result

    # dry_run: if true will not make http requests to certify the claims
    # reject: if true will also reject claims with error in REJECT_ERRORS
    #
    def initialize(transactions_path:, dry_run: true, reject: false)
      @transactions_path = transactions_path
      @dry_run = dry_run
      @reject = reject
      @result = []

      t = Time.now.utc
      @claim_time = t.iso8601(3)
      @log_name_time = t.to_s.gsub(/\W/, "-")
    end

    def run!
      Rails.logger.debug "FETCHING ALL OPEN CLAIMS LIST..."
      open_claims = fetch_open_claims

      open_claims.in_groups_of(DATA_BATCH_SIZE, false).each_with_index do |group, gidx|
        Rails.logger.debug { "FETCHING CLAIMS GROUP #{gidx}" }
        responses = fetch_batched(group, gidx)

        Rails.logger.debug "CHECKING RESPONSES..."
        self.result = check_responses(responses)

        Rails.logger.debug "SUMMARY..."
        handle_result

        Rails.logger.debug "CERTIFY..."
        commit_claims! certified: true

        Rails.logger.debug "REJECT..."
        commit_claims! certified: false if reject
      end

      Rails.logger.debug "Done!"
      nil
    end

    private

    def fetch_open_claims
      cursor = 0
      claim_ids = []

      while cursor >= 0
        fetch_url = cursor.zero? ? OPEN_CLAIMS_URL : OPEN_CLAIMS_URL + "&cursor=#{cursor}"
        Rails.logger.debug { "Fetching claims cursor: #{cursor}: #{fetch_url}" }

        open_claims_resp = HTTParty.get(fetch_url, headers: HEADERS)

        open_claims_resp.code == 200 ||
          raise("Couldn't get data from #{fetch_url}. Status code #{open_claims_resp.code}, message: #{open_claims_resp.message}.")

        batch_claim_ids = open_claims_resp.parsed_response["data"].pluck "claimId"
        Rails.logger.debug { "Fetched #{batch_claim_ids.count} claims: #{batch_claim_ids.first} ... #{batch_claim_ids.last}" }

        claim_ids += batch_claim_ids

        cursor = if open_claims_resp.parsed_response["metaData"]["hasNextPage"]
                   open_claims_resp.parsed_response["metaData"]["cursor"]
                 else
                   -1
                 end
      end

      Rails.logger.debug { "Total #{claim_ids.count} claims: #{claim_ids.first} ... #{claim_ids.last}" }

      claim_ids
    end

    def fetch_batched(group, gidx)
      responses = {}
      hydra = Typhoeus::Hydra.new(max_concurrency: DATA_PARALLEL)

      # fetch_open_claims.first(100)
      group.each_with_index do |claim_id, idx|
        url = CLAIM_DATA_URL % { claim_id: }
        request = Typhoeus::Request.new(url, headers: HEADERS)

        request.on_complete do |response|
          responses[claim_id] =
            if response.success?
              Rails.logger.debug { "Success #{gidx} #{idx} - #{claim_id}" }
              JSON.parse(response.body)
            else
              Rails.logger.debug { "Fail #{gidx} #{idx} #{claim_id}: #{response.status_message}" }
              {}
            end
        end
        hydra.queue request
      end

      hydra.run

      responses
    end

    def parse_response(response)
      p = response.dig "credentialSubject", "evidence", "payment"

      return {} unless p

      {
        transaction_id: p["identifier"],
        provider: p["provider"],
        amount: p["amount"]["value"],
        currency: p["amount"]["currency"],
        time: p["amount"]["dateTime"]
      }
    end

    def check_responses(responses)
      responses.map do |claim_id, resp|
        claim_payment = parse_response(resp)
        list_payment = transactions_list[claim_payment[:transaction_id]]

        error = if claim_payment.blank?
                  :claim_fetch_failed
                elsif list_payment.blank?
                  :prospect_not_found
                elsif list_payment[:supamoto_amount].to_f != claim_payment[:amount].to_f # rubocop:disable Lint/FloatComparison
                  :amount_mismatch
                elsif list_payment[:supamoto_wallet_operator].downcase != claim_payment[:provider].downcase
                  :provider_mismatch
                elsif list_payment[:supamoto_verified].to_i != 1
                  :not_verified
                end

        Rails.logger.debug { "#{claim_id} - #{claim_payment} : #{error}" } if error

        { claim_id:, error:, claim: claim_payment, in_list: list_payment }
      end
    end

    def parse_transactions_list
      csv = CSV.table(transactions_path, headers: true, col_sep: CSV_DELIMITER, converters: nil).map(&:to_h)
      Rails.logger.debug { "File parsed: #{csv.first.inspect}" }

      csv.to_h do |item|
        key = item[:supamoto_transaction_id]
        [key, item]
      end
    end

    def transactions_list
      @transactions_list ||= parse_transactions_list
    end

    def handle_result
      errors = result.select { |r| r[:error] }

      Rails.logger.debug { "ERRORS: #{errors.count}, OK: #{certifiable_claims.count}" }

      logfile = Rails.root.join("log/ixo-#{log_name_time}-failed.txt")
      File.open(logfile, "a") do |f|
        errors.each do |e|
          str = [
            e[:claim_id],
            e.dig(:claim, :time),
            e.dig(:claim, :amount),
            e.dig(:claim, :currency),
            e.dig(:claim, :provider),
            e.dig(:claim, :transaction_id),
            e[:error]
          ].join(",")

          f.write "#{str}\n"
          Rails.logger.debug str
        end
      end
    end

    def certifiable_claims
      result.reject { |r| r[:error] }.pluck :claim_id
    end

    def rejectable_claims
      result.select { |r| r[:error].in? REJECT_ERRORS }.pluck :claim_id
    end

    def certify_claims_body(claim_ids, certified:)
      {
        type: "",
        collectionId: COLLECTION_ID.to_s,
        storage: "cellnode",
        evaluationCreds: claim_ids.map do |claim_id|
          {
            claimId: claim_id,
            reason: (certified ? 1 : 2),
            status: (certified ? 1 : 2),
            oracle: ORACLE_DID,
            generate: {
              type: "FuelPurchaseEvaluationProspect",
              data: [
                {
                  fuelPurchaseClaimId: claim_id,
                  validFrom: claim_time,
                  status: (certified ? "verified" : "rejected"),
                  evaluation: {
                    model: VERIFICATION_MODEL,
                    version: VERIFICATION_MODEL_VER,
                    date: claim_time
                  }
                }
              ]
            }
          }
        end
      }
    end

    def commit_claims!(certified:)
      claim_ids, mode = if certified
                          [certifiable_claims, "verified"]
                        else
                          [rejectable_claims, "rejected"]
                        end

      Rails.logger.debug "... No claim_ids" if claim_ids.empty?

      claim_ids.each_slice(CERTIFY_BATCH_SIZE).with_index do |batch, idx|
        sleep CERTIFY_BATCH_SLEEP unless idx.zero?

        Rails.logger.debug { "COMMITTING #{mode} batch #{idx}, claims: #{batch}" }

        certify_body = certify_claims_body(batch, certified:).to_json
        # Rails.logger.debug certify_body

        if dry_run
          Rails.logger.debug "no request - dry run!"
        else
          certify_resp = HTTParty.post CERTIFY_URL, headers: HEADERS, body: certify_body
          # Rails.logger.debug certify_resp.parsed_response

          certify_resp.code == 201 ||
            raise("Couldn't post to #{CERTIFY_URL}. Status code #{certify_resp.code}, message: #{certify_resp.message}.")
        end

        logfile = Rails.root.join("log/ixo-#{log_name_time}-committed.txt")
        File.open(logfile, "a") do |f|
          batch.each do |claim_id|
            f.puts "b#{idx} #{claim_id} #{mode}\n"
          end
        end
      end
    end
  end
end
