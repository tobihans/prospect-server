# frozen_string_literal: true

##
# A IXO Certify service
#
# make sure you have the IXO API container configured via ENV and running
#   docker compose ixo up
#
# needs a CSV file with verified fuel purchase transactions for initialization, required columns:
#   - supamoto_transaction_id : Mobile money provider transaction id - used to identify a payment (string)
#   - supamoto_wallet_operator : downcased name of mobile money provider, used to match a payment (string)
#   - supamoto_amount : amount of payment (float)
#   - supamoto_verified : 1 for verified, other int for reject (int)
#
# currently run manually like so:
#   bundle exec rails r
#     'Ixo::Certify.new(transactions_path: "/Users/karl/Downloads/supamoto_verified_20230629.csv",
#                       dry_run: false, reject: true).run!'
#
# check thing if claim certification is committed at claim/%{claim_id}, e.g.
#   https://devnet-cellnode.ixo.earth/claims/claim/bafkreiemkbvptyx7g6hylv65rqnn2hq5s7cgv6rwignilya36y2dgnchai
#
module Ixo
  class Certifier
    # This verification model will commit into the chain - change it when we update the verification method
    VERIFICATION_MODEL = "ProspectFuelPurchaseCrmMoneyProviderXCheck"
    VERIFICATION_MODEL_VER = "0.0.2"

    CLAIMS_BASE_URL = ENV.fetch "IXO_API_URL", nil
    CLAIMS_AUTH_HEADER = ENV.fetch "IXO_API_AUTHORIZATION", nil
    ORACLE_DID = ENV.fetch "IXO_ISSUER_DID", nil

    FETCH_BATCH_SIZE = 800

    DATA_PARALLEL = 6
    DATA_BATCH_SIZE = 6
    DATA_BATCH_SLEEP = 6.seconds
    CLAIM_DATA_URL = "#{CLAIMS_BASE_URL}claim/%{claim_id}/data".freeze

    CERTIFY_URL = "#{CLAIMS_BASE_URL}certify".freeze

    HEADERS = { "Content-Type" => "application/json",
                "Accept" => "application/json",
                "Authorization" => CLAIMS_AUTH_HEADER }.freeze

    REJECT_ERRORS = %i[not_verified prospect_not_found].freeze

    attr_reader :transactions_path, :dry_run, :reject, :claim_time, :collection_id
    attr_accessor :result

    # dry_run: if true will not make http requests to certify the claims
    # reject: if true will also reject claims with error in REJECT_ERRORS
    #
    def initialize(collection_id:, dry_run: true, reject: false)
      @dry_run = dry_run
      @reject = reject
      @collection_id = collection_id
      @result = []

      t = Time.now.utc
      @claim_time = t.iso8601(3)
    end

    def open_claims_url
      @open_claims_url ||= "#{CLAIMS_BASE_URL}collection/#{collection_id}/claims?type=FuelPurchase&status=0&take=#{FETCH_BATCH_SIZE}".freeze
    end

    def run!
      Rails.logger.debug { "Oracle DID: #{ORACLE_DID}" }

      Rails.logger.debug "FETCHING ALL OPEN CLAIMS LIST..."
      open_claims = fetch_open_claims

      # Prevent just sleeping/preventing rate limit on only fetching especially on repeated runs.
      # As endpoints are rate-limited separately, lets rather spend our time validating things, too.
      open_claims.shuffle!

      handle_claims open_claims
    ensure
      csv = result_csv
      Rails.logger.info csv
      Rails.root.join("log/ixo_collection-#{collection_id}_#{Time.zone.now.iso8601}_log.csv").write(csv)

      Rails.logger.info "Done!"

      nil
    end

    private

    def result_csv
      ([result_csv_header] + result_csv_data).join("\n")
    end

    def result_csv_header
      %w[
        claim_id
        error
        claim_tx
        claim_amount
        claim_currency
        claim_time
        prospect_uid
        prospect_amount
        prospect_result
      ].join(",")
    end

    def result_csv_data
      result.
        sort_by { _1[:claim_id] }.
        map do |r|
        [
          r[:claim_id],
          r[:error],
          r[:claim][:transaction_id],
          r[:claim][:amount],
          r[:claim][:currency],
          r[:claim][:time],
          r[:trutra][:uid],
          r[:trutra][:amount],
          r[:trutra][:result]
        ].join(",")
      end
    end

    def handle_claims(open_claim_ids)
      open_claim_ids.in_groups_of(DATA_BATCH_SIZE, false).each_with_index do |claim_ids, gidx|
        Rails.logger.info "Starting Batch #{gidx} * #{DATA_BATCH_SIZE} = #{gidx * DATA_BATCH_SIZE}"

        rate_limited(DATA_BATCH_SLEEP) do
          responses = fetch_claims_info claim_ids, gidx
          batch = check_responses responses
          commit_claims!(batch)
          self.result += batch
        end
      end
    end

    def fetch_open_claims
      cursor = :first
      claim_ids = []

      while cursor.present?
        fetch_url = cursor == :first ? open_claims_url : open_claims_url + "&cursor=#{cursor}"
        Rails.logger.debug { "Fetching claims cursor: #{cursor}: #{fetch_url}" }

        open_claims_resp = HTTParty.get(fetch_url, headers: HEADERS)

        open_claims_resp.code == 200 ||
          raise("Couldn't get data from #{fetch_url}. Status code #{open_claims_resp.code}, message: #{open_claims_resp.message}.")

        batch_claim_ids = open_claims_resp.parsed_response["data"].pluck "claimId"
        Rails.logger.debug { "Fetched #{batch_claim_ids.count} claims: #{batch_claim_ids.first} ... #{batch_claim_ids.last}" }

        claim_ids += batch_claim_ids

        cursor = (open_claims_resp.parsed_response["metaData"]["cursor"] if open_claims_resp.parsed_response["metaData"]["hasNextPage"])
      end

      Rails.logger.debug { "Total #{claim_ids.count} claims: #{claim_ids.first} ... #{claim_ids.last}" }

      claim_ids
    end

    def fetch_claims_info(claim_ids, gidx)
      responses = {}
      hydra = Typhoeus::Hydra.new(max_concurrency: DATA_PARALLEL)

      # fetch_open_claims.first(100)
      claim_ids.each_with_index do |claim_id, idx|
        url = CLAIM_DATA_URL % { claim_id: }
        request = Typhoeus::Request.new(url, headers: HEADERS)

        request.on_complete do |response|
          responses[claim_id] =
            if response.success?
              Rails.logger.debug { "Success #{gidx} #{idx} - #{claim_id}" }
              JSON.parse(response.body)
            else
              Rails.logger.debug { "Fail #{gidx} #{idx} #{claim_id}: #{response.status_message}" }
              {}
            end
        end
        hydra.queue request
      end

      hydra.run

      responses
    end

    def parse_response(response)
      p = response.dig "credentialSubject", "evidence", "payment"

      return {} unless p

      {
        transaction_id: p["identifier"],
        amount: p["amount"]["value"],
        currency: p["amount"]["currency"],
        time: p["amount"]["dateTime"]
      }
    end

    def get_valid_transaction_q(transaction_id)
      <<~SQL.squish
        SELECT
            data_payments_ts.uid AS uid,
            data_payments_ts.amount AS amount,
            data_trust_trace.result AS result
        FROM data_payments_ts
        LEFT JOIN data_trust_trace
          ON data_payments_ts.uid = data_trust_trace.subject_uid
          AND data_trust_trace.subject_origin = 'payments_ts'
          AND data_trust_trace.check = 'fuel_sale_wallet_matches_validated_payment'
        WHERE data_payments_ts.payment_external_id = '#{transaction_id}'
      SQL
    end

    # returns like that: { transaction_id:, uid:, amount:, result: }
    # empty {} for non found payment
    def get_verified_order(transaction_id)
      return {} if transaction_id.blank?

      r = Postgres::RemoteControl.query! get_valid_transaction_q(transaction_id)

      return {} if r.ntuples.zero?

      info = r.to_a.first.merge(transaction_id:).with_indifferent_access

      info[:result] = :duplicated_transaction if r.ntuples > 1

      info
    end

    def check_responses(responses)
      responses.map do |claim_id, resp|
        claim_order = parse_response resp
        trutra_order = get_verified_order claim_order[:transaction_id]

        error = if claim_order.blank?
                  :claim_fetch_failed
                elsif trutra_order.blank?
                  :prospect_not_found
                elsif trutra_order[:amount].to_f != claim_order[:amount].to_f # rubocop:disable Lint/FloatComparison
                  :amount_mismatch
                elsif trutra_order[:result].to_s.downcase != "ok"
                  trutra_order[:result].presence || :not_verified
                end

        Rails.logger.debug { "#{claim_id} - #{claim_order} - #{trutra_order} : #{error}" } if error

        { claim_id:, error:, claim: claim_order, trutra: trutra_order }
      end
    end

    def commit_claims!(batch)
      bad, good = batch.partition { _1[:error].present? }.map { _1.pluck(:claim_id) }

      if bad.present? && reject
        Rails.logger.info "Rejecting: #{bad}"
        bad_body = certify_claims_body(bad, certified: false).to_json
        post_claims_body!(bad_body)
      elsif !reject
        Rails.logger.debug("Skipping rejection")
      else
        Rails.logger.debug("No rejectable Claims in batch")
      end

      if good.present?
        Rails.logger.info "Certifying: #{good}"
        good_body = certify_claims_body(good, certified: true).to_json
        post_claims_body!(good_body)
        Rails.logger.debug "Certified!"
      else
        Rails.logger.debug("No certifiable Claims in batch")
      end
    end

    def post_claims_body!(body)
      Rails.logger.info "no request - dry run! Would post: #{body}" and return if dry_run

      certify_resp = HTTParty.post(CERTIFY_URL, headers: HEADERS, body:, timeout: 200)
      # Rails.logger.debug certify_resp.parsed_response

      certify_resp.code == 201 ||
        Rails.logger.error("Couldn't post to #{CERTIFY_URL}. Status code #{certify_resp.code}, message: #{certify_resp.message}.")
    end

    def certify_claims_body(claim_ids, certified:)
      {
        type: "",
        collectionId: collection_id.to_s,
        storage: "cellnode",
        evaluationCreds: claim_ids.map do |claim_id|
          {
            claimId: claim_id,
            reason: (certified ? 1 : 2),
            status: (certified ? 1 : 2),
            oracle: ORACLE_DID,
            generate: {
              type: "FuelPurchaseEvaluationProspect",
              data: [
                {
                  fuelPurchaseClaimId: claim_id,
                  validFrom: claim_time,
                  status: (certified ? "verified" : "rejected"),
                  evaluation: {
                    model: VERIFICATION_MODEL,
                    version: VERIFICATION_MODEL_VER,
                    date: claim_time
                  }
                }
              ]
            }
          }
        end
      }
    end

    def rate_limited(limit)
      t = Time.zone.now

      yield

      elapsed = Time.zone.now - t
      missing = limit - elapsed

      return unless missing.positive?

      Rails.logger.debug { "Rate limited - sleep #{missing}" }
      sleep missing
    end
  end
end
