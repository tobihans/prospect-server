# frozen_string_literal: true

require "geo/coord"

# Pseudonymization - P14n
#
# Contains methods for normalization and p14n of imported data.
#
# As a convention the data field containing the original data is removed and the p14n value
# is added under a key with suffix '_p'. e.g. 'name' -> 'name_p'.
#
# In an attempt to make the p14n data easier to query/use, it is
# normalized before applying the p14n step.
#
# e.g. A name string 'HAns Musterman' will be normalized to 'hans musterman'.
# p14n(normalize('HAns Musterman')) will be stored.
# A query like `SELECT ... WHERE name_p = p14n(normalize('Hans Musterman')` will
# be successful because of the applied normalization.
#
# Some background on the choosen p14n method:
#
# What is HMAC?
#   HMAC-H(K,m) = H(K1 || H( K2 || m)) where:
#
#   - H: hash function (e.g. SHA256, MD5)
#   - K1, K2: derived from secret key K
#   - m: message (our data)
#
# Why HMAC?
# - adds a secret key, this avoids vulnerability to (pre-)calculated look-up tables
# - no length extension attacks, etc
# - it's a standard supported in postgresql etc.
#
class P14n
  class << self
    #
    # Applies a hash function on the given string and returns the hash.
    # Returns nil if input is `blank?`
    #
    # HMAC in postgres
    # postgres: SELECT HMAC('test message','secret key','SHA256');, needs CREATE EXTENSION IF NOT EXISTS pgcrypto;
    def pseudonymize(input)
      # hashed empty strings look just like hashes of any other string
      # it's preferable to not inflate the data and make clear where
      # there was missing data
      return nil if input.blank?

      @digest ||= OpenSSL::Digest.new("sha256")
      OpenSSL::HMAC.hexdigest(@digest, Rails.application.secret_key_base, input)
    end
    alias p pseudonymize

    # Normalization and p14n for generic text fields
    #
    # - lower case (full Unicode case mapping)
    # - remove leading and trailing whitespace (any of the following characters:
    #   null, horizontal tab, line feed, vertical tab, form feed, carriage return, space.)
    def text(text)
      return nil if text.blank?

      normal = text.strip
      normal.downcase!
      P14n.p normal
    end

    # Normalization and p14n for phone numbers.
    #
    # Returns hash of best case e164 international number format
    #  - if its a valid phone number will be converted to e164
    #  - if its an invalid phone number it will be treated as text
    #
    def phone_number(phone)
      # check implementation of to_s: valid numbers properly parsed, invalid ones stay original
      norm_phone = Phonelib.parse(phone).to_s
      P14n.text norm_phone
    end

    GEO_PRECISION = 6 # 'full' precision, to be passed through p14n
    GEO_PRECISION_BLURRED = 3 # blurred precision, doesnt reveal exact location
    #
    # Normalization and p14n of coordinates.
    #
    # - lat_or_combined_key: if maybe_lon_key is not given, expects as value of
    #   lat_or_combined_key a lat,lon string: e.g. '50.1, 20.213'
    # - if lat_or_combined_key and maybe_lon_key are specified the corresponding values
    #   in the hash should contain the lat and lon strings
    #
    # Removes the given key(s) from the hash and adds the following new keys
    #
    #   - lat_p hashed latitude normalized by rounding to max 6 digits
    #   - lon_p hashed longitude normalized by rounding to max 6 digits
    #   - lat_b latitude blurred by rounding to max 3 digits
    #   - lon_b longitude blurred by rounding to max 3 digits
    #
    # If key_prefix is given resulting keys will be prefixed with it, seperated by _
    # e.g. key_prefix: "customer" => customer_lat_p, ...
    #
    # If key_full is true lat will become latitude and lon longitude in resulting keys name
    # e.g. key_full: true => latitude_p, ....
    #
    # Empty value or failure to parse the value of key will remove key as well
    # and add the new fields with empty strings.
    #
    # Set ignore missing: true if it should just return and not create empty keys when location key is not found
    #
    # In case of parse error, the error is raised so that the caller can decide
    # to skip this record or continue with empty lat/lon fields.
    def geo_key(hash, lat_or_combined_key, maybe_lon_key = nil, key_prefix: nil, key_full: false, ignore_missing: true)
      return if ignore_missing && !hash.key?(lat_or_combined_key)

      result_lat_key = [key_prefix, (key_full ? "latitude" : "lat")].compact.join "_"
      result_lon_key = [key_prefix, (key_full ? "longitude" : "lon")].compact.join "_"
      add_empty_lat_lon hash, result_lat_key, result_lon_key

      gc = if maybe_lon_key
             lat = hash[lat_or_combined_key]
             lon = hash[maybe_lon_key]

             # make sure we delete the raw values before parsing, bc. of parse exceptions
             # hash.delete lat_or_combined_key
             # hash.delete maybe_lon_key

             return if lat.blank? || lon.blank?

             Geo::Coord.new(
               BigDecimal(lat, 10),
               BigDecimal(lon, 10)
             )

           else
             coord = hash[lat_or_combined_key]

             # hash.delete lat_or_combined_key

             return if coord.blank?

             Geo::Coord.parse_ll(coord)
           end

      # use gc to add p14n and blurred version of location
      hash["#{result_lat_key}_p"] = P14n.p(gc.lat.round(GEO_PRECISION).to_s)
      hash["#{result_lon_key}_p"] = P14n.p(gc.lon.round(GEO_PRECISION).to_s)

      hash["#{result_lat_key}_b"] = gc.lat.round(GEO_PRECISION_BLURRED).to_f
      hash["#{result_lon_key}_b"] = gc.lon.round(GEO_PRECISION_BLURRED).to_f
    end

    # Adds methods that will remove `key` in hash and add a new key `key_p`
    # with the pseudonymized value of `key` for each type
    # ignore_missing: true - will ignore missing key, otherwise create nil key
    #
    %w[pseudonymize text phone_number].each do |method|
      P14n.define_singleton_method "#{method}_key" do |hash, key, ignore_missing: false|
        return if ignore_missing && !hash.key?(key)

        value = hash[key]
        hash["#{key}_p"] = self.method(method).call(value)
      end
    end

    private

    def add_empty_lat_lon(hash, lat_key, lon_key)
      hash["#{lat_key}_p"] = nil
      hash["#{lon_key}_p"] = nil
      hash["#{lat_key}_b"] = nil
      hash["#{lon_key}_b"] = nil
    end
  end
end
