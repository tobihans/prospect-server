# frozen_string_literal: true

#
# Generate and parse signed and possibly expiring codes
# Those can be given to the public and when coming back in be parsed and verified that they were provided by us
# Nice for public URLs
class VerifiedCode
  class << self
    def verifier
      @verifier ||= ActiveSupport::MessageVerifier.new Rails.application.secret_key_base
    end

    # Call me like:
    # VerifiedCode.generate("something_50", expires_in: 3.hours)
    #
    def generate(clear, expires_in: nil)
      verifier.generate clear.to_s, expires_in:
    end

    # Call me like:
    # VerifiedCode.parse("sOm3cr4ZYc0d3")
    # => "something_50" (valid)
    # => nil (invalid or expired code)
    #
    def parse(code)
      verifier.verify code
    rescue ActiveSupport::MessageVerifier::InvalidSignature
      nil
    end
  end
end
