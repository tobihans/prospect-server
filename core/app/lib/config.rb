# frozen_string_literal: true

class Config
  class << self
    # Try to fetch config for a chain of ENV Vars
    # first present value will be returned
    # Will raise error if nothing found and no fallback value provided
    def fetch_envs(*envs, fallback: nil, raise_blank: true, bool: false)
      result = nil

      envs.each do |e|
        result = ENV.fetch(e, nil).presence
        break if result
      end

      result ||= fallback

      raise "Could not fetch value from envs: #{envs.join(', ')}. And no fallback provided." if
        !result && raise_blank

      bool ? ActiveRecord::Type::Boolean.new.deserialize(result) : result
    end
  end
end
