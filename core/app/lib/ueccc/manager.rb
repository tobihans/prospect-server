# frozen_string_literal: true

module Ueccc
  class Manager
    BACKOFFFICE_SALES_SOURCE_NAME = "Sales Data from Backoffice"
    BACKOFFFICE_PAYMENTS_SOURCE_NAME = "Payments Data from Backoffice"
    AGENT_SALES_SOURCE_NAME_PREFIX = "Sales Data from Agent "
    PROJECT_NAME_PREFIX = "Data for "

    SALES_FORM_SUFFIX = " Sales Form"
    PAYMENT_FORM_NAME = "Payment Form"

    attr_reader :organization, :rbf_claim_template

    def initialize(rbf_claim_template, organization = Current.organization)
      @organization = organization
      @rbf_claim_template = rbf_claim_template
    end

    def setups
      [EncryptionSetup, ShareKeySetup, ProjectSetup].map { _1.new self }
    end

    def all_setup?
      setups.all?(&:done?)
    end

    def required_setups
      setups.reject(&:done?)
    end

    def required_setups_sym
      required_setups.map(&:to_sym)
    end

    def next_setup_sym
      required_setups_sym.first
    end

    def instructions
      required_setups.map(&:instruction)
    end

    def next_instruction
      instructions.first
    end

    def setup_project!
      s = ProjectSetup.new self
      s.setup! if s.ready?
    end

    def project_name
      "#{PROJECT_NAME_PREFIX}#{rbf_claim_template.name}"
    end

    def agent_sales_source_name(agent_name)
      "#{AGENT_SALES_SOURCE_NAME_PREFIX}#{agent_name}"
    end

    def agent_name_from_sales_source(source)
      source.name.delete_prefix AGENT_SALES_SOURCE_NAME_PREFIX
    end

    def project
      organization.projects.find_by(name: project_name)
    end

    def agent_sources
      project.sources.select { _1.name.starts_with? AGENT_SALES_SOURCE_NAME_PREFIX }
    end

    def source_for_agent_name(agent_name)
      project.sources.find_by name: agent_sales_source_name(agent_name)
    end

    def sales_form
      rbf_claim_template.dynamic_forms.to_a.find { _1.name.ends_with? SALES_FORM_SUFFIX }
    end

    def payments_form
      DynamicForm.find_by name: PAYMENT_FORM_NAME
    end

    def backoffice_sales_source
      project.sources.find_by name: BACKOFFFICE_SALES_SOURCE_NAME
    end

    def backoffice_payments_source
      project.sources.find_by name: BACKOFFFICE_PAYMENTS_SOURCE_NAME
    end

    def sales_source_data_category
      rbf_claim_template.trust_trace_check.include?("_ogs_") ? "shs" : "meters"
    end

    def add_agent(name)
      raise "Name must be present" if name.blank?
      raise "Duplicate" if source_for_agent_name(name)

      Source.create! project:,
                     name: agent_sales_source_name(name),
                     data_category: sales_source_data_category,
                     kind: "manual_input/ueccc",
                     active: true,
                     details: { form: sales_form.id, bulk_upload: "false", public_access: "true",
                                slug: Source::ManualInput::Ueccc.generate_random_slug }
    end

    def share_to_orgs
      [rbf_claim_template.managing_organization,
       rbf_claim_template.verifying_organization].uniq
    end

    class SetupSuper
      attr_reader :manager

      def initialize(manager)
        @manager = manager
      end

      %i[to_sym instruction done? ready?].each do |m|
        define_method m do
          raise "implement me!"
        end
      end
    end

    class EncryptionSetup < SetupSuper
      def to_sym
        :encryption_setup
      end

      def done?
        manager.organization.encryption_keys?
      end

      def instruction
        "Create Encryption Key for your Organizations Personal and Private Datapoints"
      end

      def ready?
        !done?
      end

      def setup!
        :manual_action_required
      end
    end

    class ShareKeySetup < SetupSuper
      def to_sym
        :share_key_setup
      end

      def done?
        manager.share_to_orgs.all? do |share_to_org|
          SharedKeysVault.exists? organization: manager.organization,
                                  shared_with_organization: share_to_org
        end
      end

      def instruction
        "Share Your Encryption Key with: #{manager.share_to_orgs.join(', ')}"
      end

      def ready?
        !done?
      end

      def setup!
        :manual_action_required
      end
    end

    class ProjectSetup < SetupSuper
      def to_sym
        :project_setup
      end

      def done?
        manager.project &&
          manager.backoffice_sales_source &&
          manager.backoffice_payments_source
      end

      def ready?
        !done? &&
          manager.sales_form &&
          manager.payments_form
      end

      def instruction
        "Click the button to create Project with standard datasources"
      end

      def setup!
        create_project!
        create_project_visibilities!
        create_backoffice_sales_source!
        create_backoffice_payments_source!
      end

      def create_project!
        Project.find_or_create_by! organization: manager.organization, name: manager.project_name
      end

      def create_project_visibilities!
        manager.share_to_orgs.each do |share_to_org|
          v = Visibility.find_or_initialize_by project: manager.project, organization: share_to_org
          v.share_everything
          v.save!
        end
      end

      def create_backoffice_sales_source!
        s = Source.find_or_initialize_by project: manager.project, name: BACKOFFFICE_SALES_SOURCE_NAME
        s.kind = "manual_input/ueccc"
        s.details = { form: manager.sales_form.id, bulk_upload: "true", public_access: "false", slug: Source::ManualInput::Ueccc.generate_random_slug }
        s.data_category = manager.sales_source_data_category
        s.active = true
        s.save!
      end

      def create_backoffice_payments_source!
        s = Source.find_or_initialize_by project: manager.project, name: BACKOFFFICE_PAYMENTS_SOURCE_NAME
        s.kind = "manual_input/ueccc"
        s.data_category = "payments_ts"
        s.active = true
        s.details = { form: manager.payments_form.id, bulk_upload: "true", public_access: "false", slug: Source::ManualInput::Ueccc.generate_random_slug }
        s.save!
      end
    end
  end
end
