# frozen_string_literal: true

module Ueccc
  module FormGenerator
    ## Generate default forms or UECCC RBF
    #
    #   Ueccc::FormGenerator.run!
    #
    def self.run!
      %w[ogs ccs pue].each do |program|
        form_data = sales_form_data program

        rbf_claim_template = RbfClaimTemplate.find_by(trust_trace_check: "rbf_easp_#{program}_is_eligible")

        sales_form = DynamicForm.find_or_initialize_by(name: "#{program.upcase} Sales Form")
        sales_form.assign_attributes(description: "Form for the UECCC #{program.upcase} Sales",
                                     form_data:, rbf_claim_template:)
        sales_form.save!
      end

      payment_form = DynamicForm.find_or_initialize_by(name: "Payment Form")
      payment_form.assign_attributes(description: "Form for the UECCC Customer Payments", form_data: payment_form_data)
      payment_form.rbf_claim_template = nil
      payment_form.save!
    end

    def self.sales_form_data(program)
      YAML.load_file(Rails.root.join("lib/data/ueccc_#{program}_sales_form.yml"))
    end

    def self.payment_form_data
      YAML.load_file(Rails.root.join("lib/data/ueccc_payment_form.yml"))
    end
  end
end
