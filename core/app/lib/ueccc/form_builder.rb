# frozen_string_literal: true

module Ueccc
  module FormBuilder
    def self.custom_ueccc_mapping(form, data_category, data)
      mapping = form.form_data.values.pluck("fields").map do |field|
        field.map do |k, v|
          if v["encrypt"]
            ["#{k}_e", "#{v['data_column']}_e"]
          else
            [k, v["data_column"]]
          end
        end
      end

      mapping = mapping.flatten(1).each_with_object({}) do |(k, v), o|
        o[v] = k
      end

      hints = {}
      [data_category].flatten.each do |category|
        Postgres::DataTable.find(category).columns.each do |c|
          hints[c.name] = c.sql_columns_hint
        end
      end

      hints.transform_values! { _1.values.compact.empty? ? {} : _1 }

      data.map do |row|
        row["custom"] = row["custom"].is_a?(Hash) ? row["custom"] : JSON.parse(row["custom"])
        mapping.each_with_object({}) do |(from, to), obj|
          if !from.in?(hints.keys)
            obj[to] = row["custom"][from]
          elsif hints[from].empty?
            obj[to] = row[from]
          else
            # only map the encrypted part
            hints[from].select { _2 == :encrypted }.each_key do |key|
              obj["#{to}#{key[-2..]}"] = row[key]
            end
          end
        end
      end
    end

    def self.generate_dependent_selects(form_data)
      dependent_selects = {}

      form_data.each_value do |group|
        next unless group["fields"]

        group["fields"].each do |field_key, field|
          next unless field["type"] == "select" && field["dependent_on"]

          dependent_selects[field_key] = field["options"] do |opts|
            opts.map { |opt| [opt["display_text"], opt["value"]] }
          end
        end
      end

      dependent_selects
    end

    def self.generate_price_auto_display(rbf_claim_template, organization)
      return {} unless rbf_claim_template

      watch_fields_base = %w[date product_brand product_model paygo_or_cash_customer]
      watch_fields_base_extra = []

      combinations = []
      rbf_claim_template.rbf_products.with_prices.where(organization:).map do |product|
        %w[cash paygo].each do |category|
          prices = product.rbf_product_prices.default_order.where(category:, valid_from: ..(Time.zone.today + 10.days)).to_a
          110.times.each do |n|
            # last 100 days and a few days of future for offline users
            date = (Time.zone.today + 10.days) - n.days
            price = prices.find { _1.potentially_valid_at? date }
            break unless price

            required = {
              date: date.to_s,
              product_brand: product.manufacturer,
              product_model: product.model,
              paygo_or_cash_customer: category
            }

            if rbf_claim_template.subsidy_adjustments.present?
              rbf_claim_template.subsidy_adjustments.each do |field, value_adjustments|
                value_adjustments.each do |value, adjustment|
                  field_val = { field.to_s => value.to_s }
                  watch_fields_base_extra << field.to_s

                  required_adj = required.merge field_val
                  result = price.base_subsidized_sales_price - adjustment
                  combinations << { required: required_adj, result: }
                end
              end
            else
              combinations << { required:, result: price.base_subsidized_sales_price }
            end
          end
        end
      end

      {
        price: {
          watch: (watch_fields_base + watch_fields_base_extra).uniq,
          combinations:
        }
      }
    end

    # inject the RBF Products into the select fields
    # kinda hacky and we need a better solution in the future
    def self.inject_products!(form_data, dependent_selects, rbf_claim_template, organization)
      form_data.deep_symbolize_keys!

      return unless rbf_claim_template
      return unless form_data.dig(:product, :fields, :product_brand) && form_data.dig(:product, :fields, :product_model)

      dependent_selects["product_model"] = {}
      form_data[:product][:fields][:product_brand][:options] ||= []
      form_data[:product][:fields][:product_brand][:options] << {}
      form_data[:product][:fields][:product_model][:options] ||= []
      form_data[:product][:fields][:product_model][:options] << {}
      rbf_claim_template.rbf_products.with_prices.where(organization:).find_each do |product|
        dependent_selects["product_model"][product.manufacturer] ||= []
        form_data[:product][:fields][:product_brand][:options] << { value: product.manufacturer, display_text: product.manufacturer }
        form_data[:product][:fields][:product_brand][:options].uniq!
        dependent_selects["product_model"][product.manufacturer] << { value: product.model, display_text: product.model }
      end
    end

    def self.load_districts_towns_wards
      file_path = Rails.root.join("lib/data/UECC_locations.json")
      file_content = File.read(file_path)
      JSON.parse(file_content)
    rescue => e
      Rails.logger.error "Failed to load districts, towns, and wards data: #{e.message}"
      {}
    end
  end
end
