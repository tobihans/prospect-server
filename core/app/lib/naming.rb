# frozen_string_literal: true

module Naming
  #
  # Coming up for naming principles in out external services (postgres / grafana)
  #
  # Grafana:
  #     Organization : prospect_organization_17
  #             User : prospect_user_35
  #      Data Source : prospect_organization_17@postgres
  #
  # Postgres:
  #             User : prospect_organization_17@postgres
  #   Organization Schema : prospect_organization_17
  #       Data Table : public.data_meters (public = default schema)
  #     Organization View : prospect_organization_17.data_meters
  #

  SEPARATOR = "_"
  DB_DATA_TABLE_PREFIX = "data"
  DB_CUSTOM_VIEW_PREFIX = "custom"
  DB_SHARED_VIEW_NAME = "shared_data"

  ORGANIZATION_PREFIX = "prospect_organization"
  USER_PREFIX = "prospect_user"
  DATA_SOURCE_PREFIX = "postgres_sql"

  VISIBILITIES_SCHEMA = "visibilities"
  VISIBILITY_PREFIX = "vis"
  VISIBILITY_DEFAULT_PREFIX = "default"

  class << self
    #
    # Repeat the constants for the sake of having a common interface (not some as constants and some functions).
    # and support prefixed function everywhere, meaning the thing will be generated with trailing _ for lookups
    #

    # data
    def db_data_table_prefix(prefix: false)
      join DB_DATA_TABLE_PREFIX, prefix:
    end

    # prospect_organization
    def organization_prefix(prefix: false)
      join ORGANIZATION_PREFIX, prefix:
    end

    # prospect_user
    def user_prefix(prefix: false)
      join USER_PREFIX, prefix:
    end

    # prospect_organization_17.shared_data_summary
    def db_organization_shared_visibilities_view(organization, prefix: false)
      join db_organization_schema(organization), DB_SHARED_VIEW_NAME, separator: ".", prefix:
    end

    # custom
    def db_custom_view_prefix(prefix: false)
      join DB_CUSTOM_VIEW_PREFIX, prefix:
    end

    # data_meters
    def db_data_table(data_category, prefix: false)
      join DB_DATA_TABLE_PREFIX, data_category, prefix:
    end

    # prospect_test_prospect_organization
    def db_organization_prefix(prefix: false)
      join ActiveRecord::Base.connection.current_database, organization_prefix, prefix:
    end

    # prospect_test_prospect_organization_17
    def db_organization_user(organization, prefix: false)
      join db_organization_prefix, organization.id, prefix:
    end

    # prospect_test_prospect_organization_17
    def db_organization_schema(organization, prefix: false)
      join db_organization_user(organization), prefix:
    end

    # prospect_organization_17.data_meters
    def db_organization_data_view(organization, data_category, prefix: false)
      join db_organization_schema(organization), db_data_table(data_category), separator: ".", prefix:
    end

    # custom_31_source-name
    def db_custom_view(source, prefix: false)
      source_sane_name = columnize source.name
      join db_custom_view_prefix, source.id, source_sane_name, prefix:
    end

    # prospect_organization_17.custom_31_source-name
    def db_visibility_custom_view(visibility, source, prefix: false)
      join db_organization_schema(visibility.organization), db_custom_view(source), separator: ".", prefix:
    end

    # prospect_organization_17.custom_31_source-name
    def db_organization_custom_view(organization, custom_view, prefix: false)
      join db_organization_schema(organization), custom_view, separator: ".", prefix:
    end

    # prospect_organization_17.rbf_summary
    def db_organization_rbf_summary_view(organization, prefix: false)
      join db_organization_schema(organization), "rbf_summary", separator: ".", prefix:
    end

    # prospect_organization_17.rbf_config
    def db_organization_rbf_config_view(organization, prefix: false)
      join db_organization_schema(organization), "rbf_config", separator: ".", prefix:
    end

    # visibilities
    def db_visibility_schema(prefix: false)
      join VISIBILITIES_SCHEMA, prefix:
    end

    # vis
    def db_visibility_prefix(prefix: false)
      join VISIBILITY_PREFIX, prefix:
    end

    # vis_17_77_60_31_data_meters
    def db_visibility_view(visibility, source, datatable, prefix: false)
      join db_visibility_prefix, visibility.organization_id, visibility.id,
           visibility.project_id, source.id, datatable.table_name, prefix:
    end

    # visibilities.vis_17_77_60_31_data_meters
    def db_visibility_view_with_schema(visibility, source, datatable, prefix: false)
      join db_visibility_schema, db_visibility_view(visibility, source, datatable), separator: ".", prefix:
    end

    # default
    def visibility_default_prefix(prefix: false)
      join VISIBILITY_DEFAULT_PREFIX, prefix:
    end

    # default_data_meters
    def db_visibility_view_default(datatable, prefix: false)
      join visibility_default_prefix, datatable.table_name, prefix:
    end

    # visibilities.default_data_meters
    def db_visibility_view_default_with_schema(datatable, prefix: false)
      join db_visibility_schema, db_visibility_view_default(datatable), separator: ".", prefix:
    end

    # prospect_organization_17
    def grafana_organization_org(organization, prefix: false)
      join organization_prefix, organization.id, prefix:
    end

    # prospect_user_35
    def grafana_user(user, prefix: false)
      join USER_PREFIX, user.id, prefix:
    end

    # postgres_sql_prospect_organization_17
    def grafana_data_source(organization, prefix: false)
      join DATA_SOURCE_PREFIX, organization_prefix, organization.id, prefix:
    end

    def join(*args, separator: SEPARATOR, prefix: false)
      joined = args.map(&:to_s).join separator
      prefix ? joined + SEPARATOR : joined
    end

    def columnize(str)
      str.to_s.downcase.gsub(/[^0-9a-z]/, "_")
    end
  end
end
