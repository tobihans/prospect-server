# frozen_string_literal: true

# this class is initialized with an organization and then used to either
# pseudonymize / and / or encrypt data fields

class Cleaner
  attr_accessor :organization

  def initialize(organization)
    @organization = organization
  end

  #
  # wrapper for clean_key to allow for nested fields in dot notation. E.g. "contract.agent.lastName"
  # if the specified field is itself a hash we try clean each field in this hash.
  # so for contract.phones = {phone1: "123", phone2: "123", ...} you can use "contract.phones" as field_path
  #
  def clean_nested_key(data, field_path, key_type, cleaning_method, delete_key: true, key_prefix: nil, ignore_missing: false, key_full: false) # rubocop:disable Metrics/ParameterLists
    hash, names = nested_hash_field data, field_path

    return unless hash

    names.each do |name|
      clean_key(hash, name, key_type, cleaning_method, delete_key:, key_prefix:, ignore_missing:, key_full:)
    end
  end

  def clean_key(data, field_name, key_type, cleaning_method, delete_key: true, key_prefix: nil, ignore_missing: false, key_full: false, ignore_geo_error: true) # rubocop:disable Metrics/ParameterLists
    should_encrypt = encrypt?(cleaning_method)
    should_p14n = pseudonymize?(cleaning_method)

    case key_type
    when :text
      RsaOaepEncrypt.text_key(data, field_name, @organization) if should_encrypt
      P14n.text_key(data, field_name, ignore_missing:) if should_p14n
      data.delete field_name if delete_key
    when :geo
      begin
        RsaOaepEncrypt.geo_key(data, field_name.first, field_name.second, @organization, key_prefix:, key_full:, ignore_missing:) if should_encrypt
        P14n.geo_key(data, field_name.first, field_name.second, key_prefix:, key_full:, ignore_missing:) if should_p14n
      rescue => e
        raise unless ignore_geo_error

        Rails.logger.warn "Failed to clean key #{field_name}: #{e.inspect}"
      end

      data.delete field_name.first if delete_key
      data.delete field_name.second if delete_key
    when :phone_number
      RsaOaepEncrypt.text_key(data, field_name, @organization) if should_encrypt
      P14n.phone_number_key(data, field_name, ignore_missing:) if should_p14n
      data.delete field_name if delete_key
    when :birthday
      # keep year
      parsed = Time.zone.parse data[field_name] if data && data[field_name]
      data["#{field_name}_b"] = parsed.year if parsed # _b as in blurred

      # and treat it as :text
      RsaOaepEncrypt.text_key(data, field_name, @organization) if should_encrypt
      P14n.text_key(data, field_name, ignore_missing:) if should_p14n
      data.delete field_name if delete_key
    end
    data
  end

  def encrypt?(cleaning_method)
    %w[all encrypt].include? cleaning_method.to_s
  end

  def pseudonymize?(cleaning_method)
    %w[all pseudonymize].include? cleaning_method.to_s
  end

  private

  #
  # path contains the nested fieldname.
  # We go down the nested path in hash until we find the hash containing the field/ nested hash.
  # returns [hash containing the field, [fieldnames]
  #
  def nested_hash_field(hash, field_path)
    # maybe consider including https://github.com/joshbuddy/jsonpath in cleaner? Check especially the manipulation part...
    path = field_path.split(".")
    hash = hash[path.shift] while path.size > 1 && hash

    return [nil, []] if hash.nil? # path does not exist

    field = path.shift

    if hash[field].is_a?(Hash)
      # supports specifying the parent hash as field name, matches all keys
      [hash[field], hash[field].keys]
    else
      [hash, [field]]
    end
  end
end
