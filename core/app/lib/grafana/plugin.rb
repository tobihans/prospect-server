# frozen_string_literal: true

module Grafana
  module Plugin
    def installed_plugins
      endpoint = "/api/plugins"
      get_request endpoint
    end

    # not needed now, maybe later
    # def available_plugins
    #   endpoint = "/api/gnet/plugins"
    #   get_request endpoint
    # end

    def install_plugin(name:)
      return "Already installed" if name.to_s.in?(installed_plugins.pluck(:id))

      endpoint = "/api/plugins/#{name}/install"
      post_request endpoint, "" # force json payload header
    end

    def create_library_element(data:, org_id:)
      endpoint = "/api/library-elements"
      data = data.with_indifferent_access

      install_plugin name: data[:type]

      post_request endpoint, data.to_json, org_id
    end

    def update_library_element(uid:, data:, org_id:)
      endpoint = "/api/library-elements/#{uid}"

      patch_request endpoint, data.to_json, org_id
    end

    def available_library_elements(org_id:)
      get_request "/api/library-elements", org_id
    end
  end
end
