# frozen_string_literal: true

# from https://github.com/hartfordfive/ruby-grafana-api
# but the repository seems stale
module Grafana
  class Client
    attr_reader :debug, :session_cookies, :headers, :logger, :api_instance, :login_at

    include Grafana::HttpRequest
    include Grafana::User
    include Grafana::Datasource
    include Grafana::Org
    include Grafana::Dashboard
    include Grafana::Admin
    include Grafana::Folder
    include Grafana::Plugin
    include Grafana::Team

    URL = URI.parse(ENV.fetch("GRAFANA_API_URL", nil))
    ADMIN = ENV.fetch("GRAFANA_ADMIN_USER", "admin")
    PASSWORD = ENV.fetch("GRAFANA_ADMIN_PASSWORD", "admin")

    # Threshold to re-login automatically
    # must be less than what is configured in grafana as
    #   login_maximum_inactive_lifetime_duration  (default 7d)
    LOGIN_DURATION = 6.days

    def initialize(url: URL, user: ADMIN, pass: PASSWORD, settings: {})
      settings = settings.with_indifferent_access

      settings[:timeout] = 10 if settings[:timeout].to_i <= 0

      settings[:open_timeout] = 10 if settings[:open_timeout].to_i <= 0

      Rails.logger.debug { "Initializing API client #{url}" }
      Rails.logger.debug { "Options: #{settings}" }

      @api_instance = RestClient::Resource.new(
        url.to_s,
        timeout: settings[:timeout],
        open_timeout: settings[:open_timeout]
      )

      @headers = {}

      login user, pass
    end

    def login_expired?
      login_at&.before?(LOGIN_DURATION.ago)
    end

    def login(user, pass)
      Rails.logger.debug "Attempting to establish user session"
      request_data = { User: user, Password: pass }
      begin
        resp = @api_instance["/login"].post(
          request_data.to_json,
          { content_type: "application/json; charset=UTF-8" }
        )
        @session_cookies = resp.cookies

        raise "Failed to login into Grafana. Status: #{resp.code}" unless resp.code.to_i == 200

        @headers = {
          content_type: "application/json; charset=UTF-8",
          cookies: @session_cookies
        }

        @login_at = Time.zone.now

        Rails.logger.debug "User session initiated"
        true
      rescue => e
        Rails.logger.error "Error running POST request on /login: #{e}"
        Rails.logger.error "Request data: #{request_data.to_json}"
        raise
      end
    end
  end
end
