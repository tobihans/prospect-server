# frozen_string_literal: true

module Grafana
  module HttpRequest
    def get_request(endpoint, org_id = nil)
      issue_request :get, endpoint, org_id
    end

    def post_request(endpoint, data = {}, org_id = nil)
      issue_request :post, endpoint, org_id, data
    end

    def put_request(endpoint, data = {}, org_id = nil)
      issue_request :put, endpoint, org_id, data
    end

    def delete_request(endpoint, org_id = nil)
      issue_request :delete, endpoint, org_id
    end

    def patch_request(endpoint, data = {}, org_id = nil)
      issue_request :patch, endpoint, org_id, data
    end

    private

    def issue_request(method_type, endpoint = "/", org_id = nil, data = {}, try_again: true)
      Rails.logger.debug { "Running: Grafana::HttpRequest::#{caller[0][/`([^']*)'/, 1]} on #{endpoint} with data: #{data}" }
      resp = case method_type
             when :get
               @api_instance[endpoint].get grafana_headers(org_id)
             when :post
               @api_instance[endpoint].post data, grafana_headers(org_id)
             when :patch
               @api_instance[endpoint].patch data, grafana_headers(org_id)
             when :put
               @api_instance[endpoint].put data, grafana_headers(org_id)
             when :delete
               @api_instance[endpoint].delete grafana_headers(org_id)
             end

      code_i = resp.code.to_i
      if code_i.in?(200..299)
        JSON.parse(resp.body.empty? ? "{}" : resp.body, symbolize_names: true)
      else
        # don't even ask
        raise "Bad Grafana Response: #{code_i} - #{resp.body}" unless try_again

        client = Grafana::Client.new
        @api_instance = client.instance_variable_get("@api_instance")
        @headers = client.instance_variable_get("@headers")
        @login_at = client.instance_variable_get("@login_at")
        @session_cookies = client.instance_variable_get("@session_cookies")
        issue_request(method_type, endpoint, org_id, data, try_again: false)

      end
    rescue => e
      Rails.logger.error "Error: Request on #{endpoint} failed: #{e}"
      raise
    end

    def grafana_headers(org_id)
      @headers.merge("x-grafana-org-id": org_id).compact
    end
  end
end
