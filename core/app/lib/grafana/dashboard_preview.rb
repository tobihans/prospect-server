# frozen_string_literal: true

module Grafana
  module DashboardPreview
    PATH = "#{Rails.public_path}/dashboard_preview/".freeze
    TYPE = "png" # preview service creates png files
    class << self
      def create_grafana_dashboard_preview(url, grafana_org_id)
        url = "#{ENV.fetch('GRAFANA_PREVIEW_SERVICE_URL', 'http://grafana_preview:8080')}#{url}?orgId=#{grafana_org_id}"
        response = HTTParty.get(url, timeout: 180)

        return unless response.ok? && response.headers["content-type"] == MIME::Types.of(TYPE).first

        response.body
      end

      def file_path(uid, grafana_org_id)
        PATH + "#{grafana_org_id}_#{uid}.#{TYPE}"
      end

      def save_preview(image, uid, grafana_org_id)
        FileUtils.mkdir_p(PATH)
        File.binwrite(file_path(uid, grafana_org_id), image)
      end

      def get_preview(uid, grafana_org_id)
        file = file_path(uid, grafana_org_id)
        # return /dashboard_preview/<file>.png for asset_url to find the file
        File.exist?(file) ? file.remove(Rails.public_path.to_s) : nil
      end
    end
  end
end
