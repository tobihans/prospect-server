# frozen_string_literal: true

module Grafana
  module RemoteControl
    DASHBOARDS_PATH = Rails.root.join "config/grafana_templates", ENV.fetch("THEME", "prospect")

    # is it really necessary to have different database env vars for production and development?
    # use GRAFANA_POSTGRES_URL for local development, if Grafana reaches Postgres differently than the Core App
    #
    DB_URI = URI.parse Config.fetch_envs("GRAFANA_POSTGRES_URL", "POSTGRES_URL", "POSTGRES_DEV_URL",
                                         fallback: "postgres://postgres:postgres@postgres:5432/prospect_development")

    DATA_SOURCE_DEFAULT = {
      type: :postgres,
      typeName: :PostgreSQL,
      access: :proxy,
      basicAuth: false,
      isDefault: true, # one data source for one organization
      jsonData: {
        postgresVersion: Postgres::RemoteControl.current_version,
        sslmode: :disable, # database is local by default
        timescaledb: Postgres::RemoteControl.timescale_enabled?,
        maxOpenConns: 10,
        maxIdleConns: 2,
        maxIdleConnsAuto: false,
        connMaxLifetime: 20.minutes.to_i,
        database: DB_URI.path[1..] # should not start with /
      },
      readOnly: true
    }.freeze

    # rubocob:disable Metrics/ClassLength
    class << self
      def create_organization(organization)
        client.create_org name: Naming.grafana_organization_org(organization)
      end

      def update_org_theme(organization, theme)
        client.update_org_preferences({ theme: }, org_id: organization.grafana_org_id)
      end

      def create_data_source(organization)
        ds_custom = {
          name: Naming.grafana_data_source(organization),
          url: "#{DB_URI.host}#{DB_URI.port ? ":#{DB_URI.port}" : ''}",
          user: Naming.db_organization_user(organization),
          secureJsonData: { password: organization.postgres_password }
        }

        data_source = DATA_SOURCE_DEFAULT.merge ds_custom

        client.create_data_source(data_source:, org_id: organization.grafana_org_id)
      end

      def switch_all_datasources_to_current_db
        Organization.find_each do |org|
          current_ds_conf = {
            url: "#{DB_URI.host}#{DB_URI.port ? ":#{DB_URI.port}" : ''}",
            user: Naming.db_organization_user(org),
            database: ActiveRecord::Base.connection.current_database,
            secureJsonData: { password: org.postgres_password },
            jsonData: { database: ActiveRecord::Base.connection.current_database }
          }

          ds = client.data_sources(org_id: org.grafana_org_id)
          relevant = ds.select { |d| d[:name] == Naming.grafana_data_source(org) }
          relevant.each do |d|
            client.update_data_source uid: d[:uid], org_id: org.grafana_org_id,
                                      data_source: current_ds_conf
          end
        end
      end

      def create_user(user, organization)
        params = {
          login: Naming.grafana_user(user),
          password: SecureRandom.hex,
          OrgId: organization.grafana_org_id
        }

        client.create_user(params)
      end

      def update_user_permission(organization_user)
        result = client.search_for_users_by({ login: Naming.grafana_user(organization_user.user) })
        return if result.empty?

        id = client.search_for_users_by({ login: Naming.grafana_user(organization_user.user) }).first[:id]

        client.update_user_permissions id, organization_user.user.grafana_role, organization_user.organization.grafana_org_id
      end

      def available_default_dashboards
        @available_default_dashboards ||= Dir.glob("*.json", base: DASHBOARDS_PATH).
                                          map { |f| File.basename f, ".json" }
      end

      def read_dashboard_file(name)
        File.read DASHBOARDS_PATH.join "#{name}.json"
      end

      def add_datasource_to_dashboard(template, organization)
        postgres_uid = organization_postgres_data_source(organization)[:uid]

        template["inputs"] = []
        # no folder UID -> General folder
        template["folderUid"] = ""
        # update existing dashboard
        template["overwrite"] = true

        datasource = template["dashboard"]["__inputs"].select do |obj|
          obj["type"] == "datasource" && obj["pluginName"] == "PostgreSQL"
        end.first

        raise "No postgres datasource found in template" if datasource.nil?

        template["inputs"] << {
          name: datasource["name"],
          type: "datasource",
          pluginId: datasource["pluginId"],
          value: postgres_uid
        }

        constants = template["dashboard"]["__inputs"].select do |obj|
          obj["type"] == "constant"
        end

        constants.each do |c|
          template["inputs"] << {
            name: c["name"],
            type: "constant",
            value: c["value"]
          }
        end

        template
      end

      def dashboard_categories(uid, organization)
        template = client.dashboard(uid, org_id: organization.grafana_org_id)
        if template[:dashboard][:__tables]
          template[:dashboard][:__tables].map { |name| name.split("data_").last }
        else
          []
        end
      end

      def dashboard_for_organization!(name, organization)
        template = JSON.parse(read_dashboard_file(name))
        template = { "dashboard" => template }
        dashboard = add_datasource_to_dashboard(template, organization).with_indifferent_access
        install_dashboard_dependencies(dashboard, organization)
        update_library_panels(dashboard, organization)
        client.create_dashboard(dashboard, org_id: organization.grafana_org_id)[:uid]
      end

      def update_library_panels(dashboard, organization)
        path = %w[model options __prospect_version]
        # check library panels for `__prospect_version`, in case we have to update this library panel
        return unless (elements = dashboard.dig("dashboard", "__elements").reject { |_uid, ele| ele.dig(*path).nil? }).any?

        available_library_panel = client.available_library_elements(org_id: organization.grafana_org_id)[:result][:elements]
        data_source_uid = organization_postgres_data_source(organization)[:uid]

        elements.each do |uid, updated_panel|
          current_panel = available_library_panel.select { _1[:uid] == uid }.first.with_indifferent_access
          # skip if we dont know this panel
          # skip if this panel is not yet imported
          next if current_panel.nil?

          current_version = current_panel.dig(*path)
          new_version = updated_panel.dig(*path)

          next unless current_version.nil? || new_version > current_version

          updated_panel["version"] = current_panel[:version]
          # we have to explicitly set the datasource, we look for ${DS_POSTGRES_SQL_PROSPECT_ORGANIZATION_...}
          # and replace it with the datasource id for this organization, if one is needed for this panel
          updated_panel = JSON.parse(updated_panel.to_json.gsub(/\$\{DS_POSTGRES.*?\}/, data_source_uid))
          client.update_library_element(uid:, data: updated_panel, org_id: organization.grafana_org_id)
        end
      end

      def default_dashboards_for_organization!(organization)
        available_default_dashboards.each do |dd|
          uid = dashboard_for_organization! dd, organization

          # make sure permissions are there
          client.update_dashboard_role_permission(role: :viewer, permission: :view, uid:, org_id: organization.grafana_org_id)
          client.update_dashboard_role_permission(role: :editor, permission: :view, uid:, org_id: organization.grafana_org_id)
        rescue
          Rails.logger.error "Error while updating default dashboard '#{dd}' for: #{organization.name} (#{organization.id})"
        end
      end

      def setup_all_organizations!
        Organization.find_each do |organization|
          setup_organization! organization
        end
      end

      def organization_installed?(organization)
        client.all_orgs.any? { |org| org[:name] == Naming.grafana_organization_org(organization) }
      end

      def data_source_installed?(organization)
        client.data_sources(org_id: organization.grafana_org_id).any? do |ds|
          ds[:name] == Naming.grafana_data_source(organization)
        end
      end

      def user_installed?(user)
        client.search_for_users_by({ login: Naming.grafana_user(user) }).present?
      end

      def setup_organization!(organization)
        create_organization(organization) unless organization_installed?(organization)
        create_data_source(organization) unless data_source_installed?(organization)
        organization.users.each do |user|
          create_user(user, organization) unless user_installed?(user)
        end
        default_dashboards_for_organization!(organization)
      end

      def organization_postgres_data_source(organization)
        # there could actually be more datasources, but hopefully only one for postgres
        client.data_sources(org_id: organization.grafana_org_id).find do |ds|
          ds[:typeName] == "PostgreSQL" && ds[:name] == Naming.grafana_data_source(organization)
        end
      end

      def install_dashboard_dependencies(dashboard_json, _organization)
        needed_plugins = dashboard_json["dashboard"]["__requires"].select { _1["type"] == "panel" }.pluck("id")
        timeout_bak = client.api_instance.options[:timeout]

        client.api_instance.options[:timeout] = 60

        needed_plugins.map { |plugin| client.install_plugin name: plugin }
        client.api_instance.options[:timeout] = timeout_bak
      end

      def switch_current_organization(user, org)
        users = client.search_for_users_by(login: Naming.grafana_user(user))
        return if users.empty?

        client.switch_user_context(users.first[:id], org.grafana_org_id)
      rescue => e
        Rails.logger.error "Failed to switch organization for user #{user} to #{org}: #{e.message}"
      end

      def add_user_to_org(user, org)
        client.add_user_to_org(login_or_email: Naming.grafana_user(user),
                               role: user.grafana_role,
                               org_id: org.grafana_org_id)
      end

      def organization_dashboards(organization)
        # list all organization dashboard and group them by folder
        # General folder is not specified and therefor nil
        client.dashboard_list(org_id: organization.grafana_org_id).
          group_by { |db| db[:folderTitle] }.
          transform_keys! { |key| key.nil? ? "General" : key }
      end

      def delete_default_dashboards!(organization)
        default_dashboards = client.dashboard_list(org_id: organization.grafana_org_id).select do |db|
          client.dashboard(db[:uid], org_id: organization.grafana_org_id).dig(:dashboard, :__default)
        end
        default_dashboards.each do |db|
          client.delete_dashboard(db[:uid], org_id: organization.grafana_org_id)
        end
      end

      def client
        @client = Grafana::Client.new if !@client || @client.login_expired?
        @client
      end
    end
  end
end
