# frozen_string_literal: true

module Grafana
  module Team
    # create team
    def create_team(name:, org_id:)
      post_request "/api/teams", { name: }.to_json, org_id
    end

    # delete team
    def delete_team(id:, org_id:)
      delete_request "/api/teams/#{id}", org_id
    end

    # add user to team
    def add_team_member(user_id:, team_id:, org_id:)
      post_request "/api/teams/#{team_id}/members", { userId: user_id }.to_json, org_id
    end

    # remove user from team
    def remove_team_member(user_id:, team_id:, org_id:)
      delete_request "/api/teams/#{team_id}/members/#{user_id}", org_id
    end

    def get_team(name:, org_id:)
      result = get_request "/api/teams/search?name=#{name}", org_id
      # team names are unique and the name search returns only full matches
      result[:teams].first
    end

    def get_team_members(team_id:, org_id:)
      get_request "/api/teams/#{team_id}/members", org_id
    end
  end
end
