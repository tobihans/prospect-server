# frozen_string_literal: true

module Grafana
  module Dashboard
    def dashboard_list(org_id:)
      endpoint = "/api/search?type=dash-db"
      get_request endpoint, org_id
    end

    def dashboard(uid, org_id:)
      endpoint = "/api/dashboards/uid/#{uid}"
      get_request endpoint, org_id
    end

    def create_dashboard(dashboard = {}, org_id:)
      endpoint = "/api/dashboards/import"
      dashboard = { dashboard: } unless "dashboard".in?(dashboard.keys)
      dashboard[:overwrite] = true
      post_request endpoint, dashboard.to_json, org_id
    end

    def delete_dashboard(uid, org_id:)
      endpoint = "/api/dashboards/uid/#{uid}"
      delete_request endpoint, org_id
    end

    def home_dashboard
      endpoint = "/api/dashboards/home"
      get_request endpoint
    end

    def dashboard_tags
      endpoint = "/api/dashboards/tags"
      get_request endpoint
    end

    def search_dashboards(params = { query: "" })
      endpoint = "/api/search/?query=#{CGI.escape(params[:query])}"
      endpoint += "&starred=#{params[:starred]}" if params.key?(:starred)
      endpoint += "&tag=#{params[:tags]}" if params.key?(:tags)

      get_request endpoint
    end

    def dashboard_permissions(uid:, org_id:)
      endpoint = "/api/dashboards/uid/#{uid}/permissions"
      get_request endpoint, org_id
    end

    def update_dashboard_role_permission(role:, permission:, uid:, org_id:)
      role = role.to_s.capitalize
      permission = permission.to_s.capitalize
      permission_hash = { "View" => 1, "Edit" => 2, "Admin" => 4 }

      return false unless role.in?(%w[Viewer Editor Admin])
      return false unless permission.in?(permission_hash.keys)

      # Grafana created by default all three roles with
      # the corresponding permission for each dashboard
      permissions = dashboard_permissions(uid:, org_id:)

      # return unless permissions # uncomment me if db:reset fails (might keep grafana dirty!)

      permissions.map! do |permission_entry|
        permission_entry[:permission] = permission_hash[permission] if permission_entry[:role] == role
        permission_entry
      end

      endpoint = "/api/dashboards/uid/#{uid}/permissions"

      post_request endpoint, { items: permissions }.to_json, org_id
    end
  end
end
