# frozen_string_literal: true

module Grafana
  module Datasource
    def data_sources(org_id:)
      endpoint = "/api/datasources"
      get_request endpoint, org_id
    end

    def data_source(uid:, org_id:)
      endpoint = "/api/datasources/uid/#{uid}"
      get_request endpoint, org_id
    end

    def update_data_source(uid:, data_source:, org_id:)
      existing_ds = data_source(uid:, org_id:)
      data_source = existing_ds.deep_merge data_source

      endpoint = "/api/datasources/uid/#{uid}"
      put_request endpoint, data_source.to_json, org_id
    end

    def create_data_source(data_source:, org_id:)
      unless data_source.present? &&
             data_source.is_a?(Hash) &&
             data_source[:name] &&
             data_source.dig(:jsonData, :database)
        Rails.logger.warn "Can not create datasource from missing or bad parameters"
        return false
      end

      endpoint = "/api/datasources"
      post_request endpoint, data_source.to_json, org_id
    end

    def delete_data_source(uid:, org_id:)
      endpoint = "/api/datasources/uid/#{uid}"
      delete_request endpoint, org_id
    end
  end
end
