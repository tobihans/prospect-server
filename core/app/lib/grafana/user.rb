# frozen_string_literal: true

module Grafana
  module User
    # User

    def current_user
      endpoint = "/api/user"
      get_request endpoint
    end

    def update_current_user_pass(old_password:, new_password:, confirm_new:)
      endpoint = "/api/user/password"
      put_request endpoint, { oldPassword: old_password, newPassword: new_password, confirmNew: confirm_new }.to_json
    end

    def current_user_orgs
      endpoint = "/api/user/orgs"
      get_request endpoint
    end

    # Users

    def all_users
      endpoint = "/api/users"
      get_request endpoint
    end

    def user_by_id(id)
      endpoint = "/api/users/#{id}"
      get_request endpoint
    end

    def search_for_users_by(search = {})
      all_users.select do |u|
        user = u.with_indifferent_access
        search.map do |key, value|
          key = key.to_s
          user[key] && user[key] == value
        end.all?
      end
    end

    def user_orgs(user_id)
      endpoint = "/api/users/#{user_id}/orgs"
      get_request endpoint
    end
  end
end
