# frozen_string_literal: true

module Grafana
  module Admin
    def admin_settings
      endpoint = "/api/admin/settings"
      get_request endpoint
    end

    def update_user_permissions(id, perm, org_id)
      valid_perms = %w[Viewer Editor Admin]

      return false if perm.is_a?(String) && !perm.in?(valid_perms)

      return false if perm.is_a?(Hash) && (!perm.key?(:isGrafanaAdmin) || !perm[:isGrafanaAdmin].in?([true, false]))

      if perm.is_a? Hash
        endpoint = "/api/admin/users/#{id}/permissions"
        put_request endpoint, perm.to_json, org_id
      else
        user = user_by_id(id)
        endpoint = "/api/orgs/#{user[:orgId]}/users/#{id}"
        grafana_user = {
          orgId: user[:orgId],
          role: perm.downcase.capitalize
        }
        patch_request endpoint, grafana_user.to_json, org_id
      end
    end

    def delete_user(user_id)
      return false if user_id == 1

      endpoint = "/api/admin/users/#{user_id}"
      delete_request endpoint
    end

    def create_user(properties = {})
      endpoint = "/api/admin/users"
      post_request endpoint, properties.to_json
    end

    def update_user_pass(user_id, password)
      endpoint = "/api/admin/users/#{user_id}/password"
      put_request endpoint, { password: }.to_json
    end

    def switch_user_context(user_id, org_id)
      endpoint = "/api/users/#{user_id}/using/#{org_id}"
      post_request endpoint, {}.to_json
    end
  end
end
