# frozen_string_literal: true

module Grafana
  module Folder
    def folder(uid:, org_id:)
      endpoint = "/api/folders/#{uid}"
      get_request endpoint, org_id
    end

    def folders(org_id:)
      endpoint = "/api/folders"
      get_request endpoint, org_id
    end

    def create_folder(name, org_id:)
      endpoint = "/api/folders"
      post_request endpoint, { title: name }.to_json, org_id
    end

    def delete_folder(uid:, org_id:)
      endpoint = "/api/folders/#{uid}"
      delete_request endpoint, org_id
    end
  end
end
