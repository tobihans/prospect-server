# frozen_string_literal: true

module Grafana
  module Org
    # Organization

    def switch_org_context(org_id:)
      endpoint = "/api/users/1/using/#{org_id}"
      post_request endpoint, {}.to_json
    end

    def current_org
      endpoint = "/api/org"
      get_request endpoint
    end

    def update_current_org(properties = {})
      endpoint = "/api/org"
      put_request endpoint, properties
    end

    def current_org_users
      endpoint = "/api/org/users"
      get_request endpoint
    end

    def add_user_to_current_org(login_or_email:, role:)
      endpoint = "/api/org/users"
      post_request endpoint, { loginOrEmail: login_or_email, role: }.to_json
    end

    # Organizations

    def all_orgs
      endpoint = "/api/orgs"
      get_request endpoint
    end

    def org(id)
      endpoint = "/api/orgs/#{id}"
      get_request endpoint
    end

    def create_org(name:)
      endpoint = "/api/orgs/"
      post_request endpoint, { name: }.to_json
    end

    def update_org(properties = {}, org_id:)
      endpoint = "/api/orgs/#{org_id}"
      put_request endpoint, properties.to_json
    end

    def delete_org(org_id:)
      endpoint = "/api/orgs/#{org_id}"
      delete_request endpoint
    end

    def org_users(org_id:)
      endpoint = "/api/orgs/#{org_id}/users"
      get_request endpoint
    end

    def add_user_to_org(login_or_email:, role:, org_id:)
      endpoint = "/api/orgs/#{org_id}/users"
      post_request endpoint, { loginOrEmail: login_or_email, role: }.to_json
    end

    def update_org_user(user_id, properties = {}, org_id:)
      endpoint = "/api/orgs/#{org_id}/users/#{user_id}"
      patch_request endpoint, properties.to_json
    end

    def delete_user_from_org(user_id, org_id:)
      endpoint = "/api/orgs/#{org_id}/users/#{user_id}"
      delete_request endpoint
    end

    def update_org_preferences(preferences, org_id:)
      put_request "/api/org/preferences", preferences.to_json, org_id
    end
  end
end
