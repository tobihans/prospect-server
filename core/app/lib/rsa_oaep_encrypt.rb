# frozen_string_literal: true

require "geo/coord"

# Encryption
#
# This module provides methods to encrypt data.
#
# As a convention the data field containing the original data is removed and the encrypted value
# is added under a key with suffix '_e'. e.g. 'name' -> 'name_e'.
#
# The original format is being maintained - no normalisation is applied.
#
# Some background on the choosen encryption method:
#
# What is RSA?
#   RSA is a public-key cryptosystem that is widely used for secure data transmission.
# What is OAEP?
#   Optimal Asymmetric Encryption Padding (OAEP) is a padding scheme often used together with RSA encryption.
#
# Why RSA-OAEP?
# - it's a standard supported in postgresql etc.
# - it's a public-key cryptosystem, so we can encrypt data without having to share a secret key
# - it's a hybrid cryptosystem, so we can encrypt data of arbitrary length
# - it's a probabilistic encryption scheme, so the same plaintext will encrypt to different ciphertexts
# - we only need to know the public key of an organisation to encrypt the data but only the holder of the private key
#   is able to decrypt again - Prospect has no option to decode due to lack of private key
#
# The private key is wrapped in a password-based encryption scheme (PBES2) using AES-256-CBC and SHA-256 and saved
# at the server together with the salt and initialisation vector (IV) used for encryption.
# It can only be unpacked using the password that was used to wrap it during initialisation by the user on the client.
#
class RsaOaepEncrypt
  class << self
    GEO_PRECISION = 10 # 'full' precision, to be passed through encryption

    #
    # Encrypts the given string with the organisations public key and returns the encrypted string.
    #
    def encrypt(input, organization, _import = nil)
      return nil if input.blank? || organization&.public_key.blank?

      # initiate a public key out of the base64 encoded string
      pkey = OpenSSL::PKey::RSA.new(Base64.decode64(organization.public_key))

      # create symmetric key to encrypt message
      cipher = OpenSSL::Cipher.new("aes-128-ctr").encrypt

      # generate and concat aes key/iv
      aes_data = [cipher.random_key, cipher.random_iv].map { Base64.strict_encode64 _1 }.join(":")

      # encrypt symmetric key/iv with asymmetric public key
      aes_msg = pkey.public_encrypt_oaep(aes_data, "", OpenSSL::Digest::SHA256)

      # encrypt the input with the generated AES key
      enc_msg = cipher.update(input.to_s) + cipher.final

      # concat the organization id and both encrypted parts with ':', which is a uniq symbol not included in base64
      [[organization.id.to_s] + [aes_msg, enc_msg].map { Base64.strict_encode64 _1 }].join(":")
    end

    # provide aliases to maintain same signatures as p14n
    alias e encrypt
    alias phone_number encrypt
    alias text encrypt

    #
    # Encryption of coordinates.
    #
    # - lat_or_combined_key: if maybe_lon_key is not given, expects as value of
    #   lat_or_combined_key a lat,lon string: e.g. '50.1, 20.213'
    # - if lat_or_combined_key and maybe_lon_key are specified the corresponding values
    #   in the hash should contain the lat and lon strings
    #
    #
    #   - lat_e encrypted latitude normalized by rounding to max 6 digits
    #   - lon_e encrypted longitude normalized by rounding to max 6 digits
    #
    # If key_prefix is given resulting keys will be prefixed with it, seperated by _
    # e.g. key_prefix: "customer" => customer_lat_e, ...
    #
    # If key_full is true lat will become latitude and lon longitude in resulting keys name
    # e.g. key_full: true => latitude_e, ....
    #
    # Empty value or failure to parse the value of key will remove key as well
    # and add the new fields with empty strings.
    #
    # Set ignore missing: true if it should just return and not create empty keys when location key is not found
    #
    # In case of parse error, the error is raised so that the caller can decide
    # to skip this record or continue with empty lat/lon fields.
    def geo_key(hash, lat_or_combined_key, maybe_lon_key = nil, organization, key_prefix: nil, key_full: false, ignore_missing: true) # rubocop:disable Metrics/ParameterLists, Style/OptionalArguments
      return if ignore_missing && !hash.key?(lat_or_combined_key)
      return if organization&.public_key.blank?

      result_lat_key = [key_prefix, (key_full ? "latitude" : "lat")].compact.join "_"
      result_lon_key = [key_prefix, (key_full ? "longitude" : "lon")].compact.join "_"
      add_empty_lat_lon hash, result_lat_key, result_lon_key

      gc = if maybe_lon_key
             lat = hash[lat_or_combined_key]
             lon = hash[maybe_lon_key]

             return if lat.blank? || lon.blank?

             Geo::Coord.new(
               BigDecimal(lat, 10),
               BigDecimal(lon, 10)
             )

           else
             coord = hash[lat_or_combined_key]

             return if coord.blank?

             Geo::Coord.parse_ll(coord)
           end

      # use gc to add enc and blurred version of location
      hash["#{result_lat_key}_e"] = RsaOaepEncrypt.e(gc.lat.round(GEO_PRECISION).to_s, organization)
      hash["#{result_lon_key}_e"] = RsaOaepEncrypt.e(gc.lon.round(GEO_PRECISION).to_s, organization)
    end

    # Adds methods that will add a new key `key_e`
    # with the encrypted value of `key` for each type
    # ignore_missing: true - will ignore missing key, otherwise create nil key
    #
    %w[encrypt text].each do |method|
      RsaOaepEncrypt.define_singleton_method "#{method}_key" do |hash, key, organization, ignore_missing: false|
        return if ignore_missing && !hash.key?(key)

        value = hash[key]
        hash["#{key}_e"] = self.method(method).call(value, organization)
      end
    end

    def blob(input, organization, import)
      EncryptedBlob.store_data!(input, organization, import)
    end

    private

    def add_empty_lat_lon(hash, lat_key, lon_key)
      hash["#{lat_key}_e"] = ""
      hash["#{lon_key}_e"] = ""
    end
  end
end
