# frozen_string_literal: true

require "csv"

#
# Imports products from Verasol CSV database export into CommonProducts table
#
class VerasolProductImporter
  class << self
    #
    # Returns [String] of log messages
    #
    def import(csv)
      log = []
      log << "Importing products"

      table = CSV.parse(csv, headers: true)

      # clean up field values
      # we remove 'N/A' and 'Modular'
      table.each do |row|
        row.headers.each do |field|
          row[field] = row[field].to_s.gsub("N/A", "")
          row[field] = row[field].to_s.gsub("Modular", "")
        end
      end

      #
      # Converting rows to Products and save to DB
      #
      size = table.size
      stats = { "Total rows" => size }
      failed = []
      table.each_with_index do |row, i|
        log << "processing row #{i + 1}/#{size}"
        begin
          p = product_from_row(row)
          p.save!
          stats["Saved successfully"] = stats["Saved successfully"].to_i + 1
        rescue => e
          log << "Can't add #{p&.name}, record: #{i + 1}: #{e.message}"
          failed << p
          stats["Failed"] = stats["Failed"].to_i + 1
        end
      end

      #
      # print final stats and failed info
      #

      log << "Summary: #{stats}"
      log << "Listing all Failed:"
      failed.each do |p|
        log << "#{p&.name}|#{p&.brand}|#{p&.model_number}"
      end
      Rails.logger.debug log.join("\n")
      log
    end

    def product_from_row(row)
      p = CommonProduct.find_or_create_by name: row.delete("Product Name")[1],
                                          brand: row.delete("Brand")[1],
                                          model_number: row.delete("Model Number")[1]

      p.assign_attributes kind: row.delete("Product Type")[1],
                          website: row.delete("Website")[1],
                          properties: row.to_h.compact_blank!, # avoid filling the db with unused hash keys
                          data_origin: "verasol"

      p
    end
  end
end
