# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: ENV.fetch("MAILFROM", "noreply@prospect.energy")
  layout "mailer"
end
