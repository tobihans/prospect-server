# frozen_string_literal: true

class SourceMailer < ApplicationMailer
  def notify_recorded_upload
    @source = params[:source]
    @code = params[:code]
    @code_expiry = params[:code_expiry]

    subject = "#{Rails.application.config.theme[:prefix]}: Please upload data for #{@source.name}"
    mail(to: params[:email], subject:)
  end
end
