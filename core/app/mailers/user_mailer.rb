# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def welcome
    @user = params[:user]

    mail to: @user.email, subject: "Welcome to #{Rails.application.config.theme[:prefix]}, #{@user}"
  end

  def invite
    @inviting_user = params[:inviting_user]
    @user = params[:user]

    mail to: @user.email, subject: "Invitation to #{Rails.application.config.theme[:prefix]} / #{@user.organization}"
  end

  def organization_invite
    @inviting_user = params[:inviting_user]
    @user = params[:user]
    @organization = params[:organization]

    mail to: @user.email, subject: "Invitation to #{Rails.application.config.theme[:prefix]} / #{@organization}"
  end

  def reset_password_email(user)
    @user = user
    @url = edit_password_reset_url @user.reset_password_token
    mail to: @user.email, subject: "#{Rails.application.config.theme[:prefix]}: Your password has been reset"
  end

  def reset_password_other_auth(user)
    @user = user
    @providers = @user.authentications.map { |auth| auth.provider.humanize }.join(", ")
    mail to: @user.email, subject: "#{Rails.application.config.theme[:prefix]}: Password Reset - Login via #{@providers}"
  end
end
