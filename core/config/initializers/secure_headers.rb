# frozen_string_literal: true

SecureHeaders::Configuration.default do |config|
  config.cookies = {
    secure: true, # mark all cookies as "Secure"
    httponly: true, # mark all cookies as "HttpOnly"
    samesite: {
      lax: true # mark all cookies as SameSite=lax
    }
  }

  config.x_frame_options =
    "ALLOW-FROM:*.#{Rails.application.config.host_basename},#{Rails.application.config.host_with_port},accounts.google.com,gitlab.com"

  config.referrer_policy = %w[origin-when-cross-origin strict-origin-when-cross-origin]

  common = [
    Rails.application.config.host_with_port,
    Rails.application.config.host_basename,
    ENV.fetch("GRAFANA_EMBED_URL", nil)
  ]

  config.csp = {
    # self data unsafe-inline https://*.prospect.energy
    default_src: (%w['self' validator.swagger.io data: 'unsafe-inline'] + common), # rubocop:disable Lint/PercentStringArray
    frame_src: (%w['self' blob:] + common), # rubocop:disable Lint/PercentStringArray
    script_src: (%w['self' data: 'unsafe-inline' 'unsafe-eval' blob: umami.a2.lu/script.js] + common), # rubocop:disable Lint/PercentStringArray
    connect_src: (%w['self' data: api.pwnedpasswords.com login.microsoftonline.com accounts.google.com umami.a2.lu] + common), # rubocop:disable Lint/PercentStringArray
    font_src: (%w['self' fonts.gstatic.com data: 'unsafe-inline'] + common), # rubocop:disable Lint/PercentStringArray
    style_src_elem: (%w['self' fonts.googleapis.com 'unsafe-inline'] + common), # rubocop:disable Lint/PercentStringArray
    report_uri: [ENV.fetch("SENTRY_CSP_URL", nil)]
  }
end
