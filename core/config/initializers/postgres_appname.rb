# frozen_string_literal: true

#
# active record connections also set the postgres application_name for better monitoring
# modernized version of this: https://github.com/vagmi/pg-app-name/blob/master/lib/pg-app-name.rb
#

module PgApplicationName
  def self.application_name
    @application_name ||=
      [
        "Core",
        (Faktory.worker? ? "Faktory" : "App"),
        `hostname`.to_s.squish
      ].compact.join(" ")
  end

  module Unpooled
    def establish_connection(*args)
      result = super(*args)
      appname = "#{PgApplicationName.application_name} Unpooled"

      ::ActiveRecord::Base.connection.execute("SET application_name = '#{appname}';")

      result
    end
  end

  module Pooled
    def new_connection(*args)
      result = super(*args)

      @pool_number ||= 1
      appname = "#{PgApplicationName.application_name} Pool-#{@pool_number}"
      result.execute("SET application_name = '#{appname}';")
      @pool_number += 1

      result
    end
  end
end

ActiveSupport.on_load(:active_record) do
  ActiveRecord::Base.prepend PgApplicationName::Unpooled # rubocop:disable Rails/ActiveSupportOnLoad
  ActiveRecord::ConnectionAdapters::ConnectionPool.prepend PgApplicationName::Pooled
end
