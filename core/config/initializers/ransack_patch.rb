# frozen_string_literal: true

#
# Make Active Admin Work without always specifying ransackable attributes, just allow all by default.
# Found here:
#   https://gorails.com/blog/patching-models-for-ransack-4-0-0-extending-activerecord-for-gem-compatibility
#   https://github.com/activeadmin/activeadmin/discussions/8033
#
# If you need to exclude attruibutes do it like that:
#   class User < ApplicationRecord
#     def self.ransackable_attributes(_auth_object = nil)
#       super - ["password"]
#     end
#   end
#

module RansackPatch
  def ransackable_attributes(_auth_object = nil)
    authorizable_ransackable_attributes
  end

  def ransackable_associations(_auth_object = nil)
    authorizable_ransackable_associations
  end
end

ActiveSupport.on_load(:active_record) do
  extend RansackPatch
end
