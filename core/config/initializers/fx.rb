# frozen_string_literal: true

#
# prevent dumping the timescale triggers
# done alike https://github.com/teoljungberg/fx/issues/97
#
module Fx
  module SchemaDumper
    module Trigger
      private

      def dumpable_triggers_in_database
        @_dumpable_triggers_in_database ||= # rubocop:disable Naming/MemoizedInstanceVariableName
          Fx.database.triggers.reject { |trigger| trigger.name.in? %w[ts_insert_blocker metadata_insert_trigger] }.sort
      end
    end
  end
end
