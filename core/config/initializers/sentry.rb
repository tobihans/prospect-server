# frozen_string_literal: true

Sentry.init do |config|
  config.dsn = ENV.fetch("SENTRY_DSN", nil)
  config.breadcrumbs_logger = %i[active_support_logger http_logger]
  # we want to know about missing objects or bad links!
  config.excluded_exceptions -= ["ActiveRecord::RecordNotFound"]
end
