# frozen_string_literal: true

# apply class field_with_errors to input instead of wrapping it and messing with our beautiful layout!
#
# Thanks @ https://www.jorgemanrubia.com/2019/02/16/form-validations-with-html5-and-modern-rails/
#

ActionView::Base.field_error_proc = proc do |html_tag, _instance_tag|
  fragment = Nokogiri::HTML.fragment(html_tag)
  field = fragment.at("input,select,textarea")

  # model = instance_tag.object
  # error_message = model.errors.full_messages.join(", ")

  html = if field
           field["class"] = "#{field['class']} field_with_errors border-pink"
           html = <<-HTML
              #{fragment}
           HTML
           html
         else
           html_tag
         end

  html.html_safe # rubocop:disable Rails/OutputSafety
end
