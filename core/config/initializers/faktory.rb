# frozen_string_literal: true

#
# wrapping all the helper classes and methods we are gonna use below...
#

# How to test if its working
# * Make output look like production:
#   in application.rb uncomment `config.rails_semantic_logger.add_file_appender = false`
# * Configure Sentry, e.g. set SENTRY_DSN in .env
# * startup faktory-worker-core
#   -> look at log output during startup
# * Test normal / Failing normal Faktory Job, e.g.
#   ImportWorker.perform_async( source_id: 876543)
#   -> Check faktory output
#   -> Check Sentry
# * Test ActiveJob Job in Faktory Backend
#   - use faktory as backend for dev, in development.rb uncomment:
#     config.active_job.queue_adapter = :faktory
#   - Enqueue email worker
#     UserMailer.with(user: User.last).welcome.deliver_later
#   -> Check log output
#   - insert error into Usermailer Template
#   - Enqueue again
#   -> Check log output
#   -> Check Sentry
#

module FaktoryHelpers
  FAKTORY_SEMANTIC_LOGGER_JOB_TAGS = %w[jid queue jobtype args].freeze

  class SentryErrorHandler
    def self.call(exception, ctx_h)
      Sentry.with_scope do |temp_scope|
        job_h = ctx_h.with_indifferent_access[:job] # mix of str and sym keys :)
        logger_h = FaktoryHelpers.extract_tags job_h

        context_h = {
          jid: logger_h["jid"],
          args: logger_h["args"],
          active_job_id: logger_h["active_job_id"],

          retry_count: job_h.dig("failure", "retry_count"),
          retry_remaining: job_h.dig("failure", "remaining")
        }

        tag_h = {
          "faktory.queue" => logger_h["queue"],
          "faktory.jobtype" => logger_h["jobtype"],
          "active_job.class" => logger_h["active_job_class"],
          "active_job.call" => logger_h["active_job_call"]
        }

        temp_scope.set_context :faktory, context_h
        temp_scope.set_tags tag_h

        Sentry.capture_exception exception
      end
    end
  end

  class TaggedSemanticLogger
    def call(_worker_instance, job, &)
      SemanticLogger.named_tagged FaktoryHelpers.extract_tags(job), &
    end
  end

  def self.extract_tags(faktory_h)
    faktory_tags = faktory_h.slice(*FaktoryHelpers::FAKTORY_SEMANTIC_LOGGER_JOB_TAGS)
    active_job_tags = FaktoryHelpers.extract_active_job_tags faktory_h

    faktory_tags.merge active_job_tags
  end

  def self.extract_active_job_tags(faktory_h)
    return {} unless faktory_h["jobtype"] == "ActiveJob::QueueAdapters::FaktoryAdapter::JobWrapper"

    args = faktory_h["args"].to_a.first.to_h
    {
      "active_job_id" => args.fetch("job_id"),
      "active_job_class" => args.fetch("job_class"),
      # looks like this: "arguments": ["UserMailer","welcome","deliver_now", {"_aj_ruby2_keywords": ["params","args"],
      "active_job_call" => args.fetch("arguments").to_a.take_while { |arg| arg.is_a?(String) }.join("/")
    }
  end
end

#
# Setting up logging and Error Handling
#

# setting base logger for faktory worker, used for startup and shutdown of faktory worker
Faktory.logger = SemanticLogger["CoreFaktoryWorker"]

# clear default handler, no need to log exceptions twice, we handle them with our handler only
Faktory.options[:error_handlers] = []

# Go bit down from default concurrency of 10
Faktory.options[:concurrency] = ENV.fetch("CORE_FAKTORY_CONCURRENCY", 6).to_i

# Configure Faktory Worker with logger and Sentry
Faktory.configure_worker do |config|
  config.worker_middleware do |chain|
    chain.add FaktoryHelpers::TaggedSemanticLogger
  end
  config.error_handlers << FaktoryHelpers::SentryErrorHandler
end

#
# tag also job started and finished messages, as they are not implemented as middleware
# so we monkeypatch the Faktory::JobLogger class with those improvements:
#   * tagged logging with job info
#   * job_runtime not in string but as payload
#   * explicitly pass exception to error log level
#   * runtime in milliseconds instead of seconds
#

require "faktory/job_logger" # require early, otherwise the later require by faktory overwrites our monkey patch

module Faktory
  class JobLogger
    def call(item)
      SemanticLogger.reopen # just to be safe...

      start = Time.zone.now
      SemanticLogger.named_tagged FaktoryHelpers.extract_tags(item) do
        logger.info "start"
        yield
        logger.info "done", job_runtime: elapsed_ms(start)
      rescue Exception => e # rubocop:disable Lint/RescueException
        logger.error "fail", { job_runtime: elapsed_ms(start) }, e
        raise
      end
    end

    # an alternative in milliseconds to the original
    def elapsed_ms(start)
      ((Time.zone.now - start) * 1000).round(2)
    end
  end
end
