# frozen_string_literal: true

# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "@hotwired/turbo-rails", to: "turbo.min.js", preload: true
pin "@hotwired/stimulus", to: "@hotwired--stimulus.js" # @3.2.2
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin_all_from "app/javascript/controllers", under: "controllers"
pin "stimulus-dropdown" # @2.0.0
pin "hotkeys-js" # @3.10.0
pin "stimulus-use" # @0.50.0
pin "keygen"
pin "decrypt"
pin "papaparse" # @5.4.1
pin "tailwindcss-stimulus-components" # @5.1.1
pin "@hotwired/stimulus", to: "@hotwired--stimulus.js" # @3.2.2
pin "tabulator-tables" # @6.2.1
pin "choices.js" # @10.2.0
pin "tom-select" # @2.3.1
pin "papaparse" # @5.4.1
pin "@stimulus-components/password-visibility", to: "@stimulus-components--password-visibility.js" # @3.0.0
pin "@stimulus-components/clipboard", to: "@stimulus-components--clipboard.js" # @5.0.0
