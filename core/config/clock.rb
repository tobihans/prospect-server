# frozen_string_literal: true

require "clockwork"
require "clockwork/database_events"
require_relative "../config/boot"
require_relative "../config/environment"

module Clockwork
  configure do |config|
    config[:sleep_timeout] = 5
    config[:logger] = Rails.logger
    config[:tz] = "UTC"
  end

  error_handler do |error|
    Sentry.capture_exception error
  end

  # required to enable database syncing support
  Clockwork.manager = DatabaseEvents::Manager.new

  sync_database_events model: Source, every: 1.minute do |source|
    Rails.logger.info "Hallo Clockwork"
    ImportWorker.perform_async(source_id: source.id)
  end

  on(:after_run) do
    Clockwork.manager.log "Clocked Jobs: #{Clockwork.manager.instance_variable_get('@events').map(&:to_s).inspect}"
    true
  end

  def self.run_core_worker(freq, worker_class, *opts)
    every freq, "core:workers:#{worker_class.to_s.underscore}" do
      worker_class.perform_async(*opts)
    end
  end

  def self.run_dataworker_processor(freq, name)
    every freq, "dataworker.processors.#{name}" do
      FaktoryPusher.push name
    end
  end

  every(2.hours, "grafana.dashboard.previews") do
    # let's only add more jobs this round if we are done with the old ones
    # default to zero if the queue does not exist
    if (Faktory::Client.new.info.dig("faktory", "queues", "grafana-preview") || 0).zero?
      Organization.find_each do |org|
        Grafana::RemoteControl.organization_dashboards(org).values.flatten.each do |db|
          GrafanaPreviewWorker.perform_async(org.id, db[:url], db[:uid])
        end
      end
    end
  end

  run_core_worker 1.day,  SessionTrimWorker if Rails.env.production?
  run_core_worker 1.day,  FailUnfinishedImportsWorker
  run_core_worker 1.week, FailUnfinishedImportsWorker, false

  run_dataworker_processor 1.hour,     "meter_events"
  run_dataworker_processor 1.day,      "paygo_accounts_snapshots"
  # run_dataworker_processor 1.day,      "update_demo_data_timestamps" if ENV["PROSPECT_ENVIRONMENT"] == "demo" # TODO: fix processor
  run_dataworker_processor 1.day,     "rbf_easp_ogs_is_eligible"
  run_dataworker_processor 1.day,     "rbf_easp_ccs_is_eligible"
  run_dataworker_processor 1.day,     "rbf_easp_pue_is_eligible"
end
