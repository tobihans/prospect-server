# frozen_string_literal: true

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Core
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1
    # config.add_autoload_paths_to_load_path = true
    # https://guides.rubyonrails.org/upgrading_ruby_on_rails.html#autoloaded-paths-are-no-longer-in-$load-path

    # updating the version will trigger the accept terms form anew for all users
    config.current_terms_version = {
      terms_of_use: "V2.0 - 29.03.2023",
      code_of_conduct: "V2.0 - 29.03.2023",
      data_protection_policy: "V2.0 - 29.03.2023"
    }.with_indifferent_access

    # theme config
    theme = ENV.fetch("THEME", "prospect")
    config_folder = Rails.root.join("app/assets/images/themes/#{theme}").to_s
    theme_config = YAML.load_file("#{config_folder}/theme.yml").with_indifferent_access

    config.theme = {
      title: theme_config[:title],
      prefix: theme_config[:prefix],
      logo: "themes/#{theme}/#{theme_config[:logo]}".to_s,
      favicon: "themes/#{theme}/favicon"
    }

    config.assets.paths << "#{config_folder}/favicon"

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.action_mailer.preview_paths << Rails.root.join("spec/mailer_previews").to_s

    # move all rails active jobs into own queue, primarily e-mails
    config.active_job.default_queue_name = "active_jobs"

    # make sure we build proper urls...
    public_url_s = ENV.fetch("PUBLIC_URL", "http://localhost:3000/")
    public_url_s += "/" unless public_url_s.end_with? "/" # make sure we end with slash some things depend on it downstream!
    public_url = URI public_url_s

    config.public_url = public_url
    config.host_with_port = public_url.port.in?([80, 443]) ? public_url.host : "#{public_url.host}:#{public_url.port}"
    config.host_basename = public_url.host

    config.action_mailer.default_url_options = {
      host: public_url.host,
      port: public_url.port,
      protocol: public_url.scheme
    }

    config.env_hint = if Rails.env.test?
                        "TEST"
                      elsif Rails.env.development?
                        "DEV"
                      elsif ENV["PROSPECT_ENVIRONMENT"] != "production"
                        ENV["PROSPECT_ENVIRONMENT"]
                      end

    config.full_prod_mode = Rails.env.production? && ENV["PROSPECT_ENVIRONMENT"] == "production"

    config.ms_auth = ActiveModel::Type::Boolean.new.cast(ENV.fetch("MS_AUTH", false))
    config.google_auth = ActiveModel::Type::Boolean.new.cast(ENV.fetch("GOOGLE_AUTH", false))

    config.active_record.encryption.hash_digest_class = OpenSSL::Digest::SHA256
    config.active_record.encryption.support_sha1_for_non_deterministic_encryption = true

    # dumping a nice structure.sql, we need some options. See: https://github.com/lfittl/activerecord-clean-db-structure#caveats
    config.activerecord_clean_db_structure.ignore_ids = true
    config.activerecord_clean_db_structure.indexes_after_tables = true
    config.activerecord_clean_db_structure.order_column_definitions = true
    config.activerecord_clean_db_structure.order_schema_migrations_values = true

    # when schema search path is set pg_dump is called with --schema=.. flags and thus omits extensions.
    # So we need to dump the extensions explicitly... Dont for get to add yours here when you add a new one.
    # see: https://github.com/rails/rails/issues/17157 and https://postgrespro.com/list/thread-id/2493088
    ActiveRecord::Tasks::DatabaseTasks.structure_dump_flags = %w[--extension=plpgsql --extension=timescaledb]

    # nicer logging with semantic logger
    config.semantic_logger.application = "ProspectCore"

    config.log_tags = {
      request_id: :request_id,
      ip: :remote_ip
    }

    # config.rails_semantic_logger.add_file_appender = false # prod behaviour, uncomment to test it

    $stdout.sync = true
  end
end
