# frozen_string_literal: true

Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  mount Rswag::Ui::Engine => "/api-docs"
  mount Api::Base, at: "/api"

  # Defines the root path route ("/")
  root "users#welcome"

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  resources :users, except: %i[destroy] do
    get :me, on: :collection
    get "me/missing_organization", on: :collection, to: "users#missing_organization"
    get :logout, on: :collection, to: "session#logout_user"
    post :logout, on: :collection, to: "session#logout_user"
    get :invitations, on: :collection
    get :accept_invitation, on: :collection
    get :reject_invitation, on: :collection
    get "invitation/:invite_code", on: :collection, to: "users#invitation", as: "invitation"
    post "register/:invite_code", on: :collection, to: "users#register", as: "register"
    post :toggle_archive, on: :member
    get :accept_terms, on: :collection
    post :accept_terms, on: :collection
    get :switch_organization, on: :collection
    get :resend_invitation, on: :member
  end

  resources :organization_users do
    resources :privileges, only: [:index] do
      put :update, on: :collection
    end
  end

  resources :password_resets, only: %i[new create edit update]

  post "login_local" => "session#create", as: :login_local

  # oauth
  post "session/callback"
  get  "session/callback"
  get  "session/:provider" => "session#oauth", as: :auth_at_provider
  get  "oauth/callback/microsoft" => "session#callback", defaults: { provider: "microsoft" }

  resources :sources, only: %i[show destroy] do
    get :show_secret
    get :dynamic_form_template
    get :dynamic_form, on: :collection
    post :dynamic_form_submit, on: :collection
    get :view_data
    post :manual_upload
    post :trigger_now
    get :edit_project
    post :update_project
    get "public_recorded/:code", on: :collection,
                                 to: "sources#public_recorded", as: :public_recorded
    post "public_recorded_upload/:code", on: :collection,
                                         to: "sources#public_recorded_upload", as: :public_recorded_upload
    post :reprocess
    post :delete_data_and_imports
    get :csv_update
    post :csv_update_submit
    get :backfill
    get :backfill_completely
    post :backfill_from_date

    resources :imports, only: :index
  end

  resources :projects do
    resources :visibilities, except: :index, shallow: true
    get :view_data
    get :view_data_rbf

    # /sources/dynamic_form
    get :dynamic_form, on: :collection
    post :dynamic_form_submit, on: :collection
  end

  resources :build_source, only: %i[show update], controller: "sources/build"

  resources :imports, only: %i[show destroy] do
    get :download
    post :reprocess
  end

  resources :encrypted_blob

  get "demo" => "demo#index"
  get "demo/show" => "demo#show"
  get "demo/wrap_key" => "demo#wrap_key"
  post "demo/post_key" => "demo#post_key"
  post "demo/ajax_test" => "demo#ajax_test"
  get "demo/easy_web_crypto" => "demo#easy_web_crypto"

  resources :analytics, only: %i[index show] do
    get "grafana_new/:type", on: :collection, to: "analytics#grafana_new", as: :grafana_new
    get "preview/:board_uid", on: :collection, to: "analytics#preview", as: :preview
  end

  resources :docs, only: %i[index show]

  get "exports" => "exports#index"
  get "exports/export"
  get "exports/export_form"

  match "ping", to: "status#ping", via: %i[get post]

  get "organizations/mine"
  resources :organizations, except: %i[new create] do
    get :show_token
  end

  get "shared_keys_vault/edit" => "shared_keys_vault#edit"
  post "shared_keys_vault/create" => "shared_keys_vault#create"
  post "shared_keys_vault/delete" => "shared_keys_vault#delete"

  resources :rbf_claims do
    get :summary, on: :member
    get :show_do_data_collection, on: :member
    get :print_submitted, on: :member
    get :show_do_assign_verifiers, on: :member
    get :show_do_verification, on: :member
    get :download_verification_csv, on: :member
    get :show_do_payment, on: :member
    get :show_paid_out, on: :member
    get :show_verification_result, on: :member
    get :show_print_voucher, on: :member
    get :print_voucher, on: :member

    post :do_data_collection, on: :member
    post :do_data_collection_add_all, on: :member
    post :do_assign_verifiers, on: :member
    post :do_verification, on: :member
    post :do_device_verification, on: :member
    post :do_payment, on: :member
    post :do_sample_secondary_verification, on: :member

    get :device_details, on: :member
    get :device_claim, on: :member
    post "event/:event", on: :member, to: "rbf_claims#event", as: :event
    get "download/:type", on: :member, to: "rbf_claims#download", as: :download
  end

  resources :rbf_dashboard, only: :index do
    get :rbf_products, on: :collection
    post :setup_project, on: :collection
    post :add_agent, on: :collection
    post :toggle_agent, on: :collection
  end
end
