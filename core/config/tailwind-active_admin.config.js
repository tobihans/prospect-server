/*
  run:
  bundle exec tailwindcss -c config/tailwind-active_admin.config.js -o vendor/stylesheets/active_admin.css
  to manually create the active admin css file
  tailwindcss-rails can not handle multiple tailwind configs yet:
  https://github.com/rails/tailwindcss-rails/pull/242
*/
const execSync = require('child_process').execSync;
const activeAdminPath = execSync('bundle show activeadmin', { encoding: 'utf-8' }).trim();

module.exports = {
  content: [
    `${activeAdminPath}/vendor/javascript/flowbite.js`,
    `${activeAdminPath}/plugin.js`,
    `${activeAdminPath}/app/views/**/*.{arb,erb,html,rb}`,
    './app/admin/**/*.{arb,erb,html,rb}',
    './app/views/active_admin/**/*.{arb,erb,html,rb}',
    './app/views/admin/**/*.{arb,erb,html,rb}',
    './app/javascript/**/*.js'
  ],
  darkMode: "class",
  plugins: [
    require(`${activeAdminPath}/plugin.js`)
  ]
}
