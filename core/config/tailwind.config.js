const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './public/*.html',
    './app/helpers/**/*.rb',
    './app/javascript/**/*.js',
    './app/views/**/*.{erb,haml,html,slim}',
    './config/initializers/heroicon.rb',
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#FFFFFF',
      primary: '#03B9B5',
      secondary: '#FFF601',
      pink: '#DB2777',
      yellow: '#FDE047',
      dark: '#1C322F',
      black: '#000000',
      graph: {
        1: '#03B9B5', // also primary
        2: '#6ECD66',
        3: '#FFDE00', // close to secondary, but better to see
        4: '#F28147',
        5: '#DB2777'  // also pink
      },
      grey: {
        1: '#F4F4F4',
        2: '#E6E6E6',
        3: '#a0a0a2',
        4: '#777579',
        6: '#2c2c2c',
        7: '#222222'
      },
      shade: {
        1: '#B2EC29',
        2: '#53D96A',
        3: '#218465',
        4: '#136660'
      }
    },
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
        mono: ['Andale', ...defaultTheme.fontFamily.mono],
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
  ]
}
