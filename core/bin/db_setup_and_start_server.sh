#! /bin/bash

bundle install

if ! bundle exec rake db:exists ; then
  echo "running db setup"
  bundle exec rake db:setup
fi

if $PROSPECT_AUTOMATIC_DB_MIGRATE ; then
  echo "running migrations"
  bundle exec rake db:migrate
fi

bundle exec rake assets:precompile

rm -f tmp/pids/server.pid

bundle exec rails s -e development -p 3000 -b '0.0.0.0' 
