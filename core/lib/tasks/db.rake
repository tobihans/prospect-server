# frozen_string_literal: true

namespace :db do
  desc "Check if database connection exists"
  task exists: :environment do
    ActiveRecord::Base.connection
  rescue
    exit 1
  end

  desc "Drop all views and such so we can migrate the DB smoothly"
  task prepare_migration: :environment do
    Postgres::RemoteControl.drop_all_custom_views!
    Postgres::RemoteControl.drop_all_organization_views!
    Postgres::RemoteControl.drop_all_visibility_views!
  end
end
