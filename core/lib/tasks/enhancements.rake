# frozen_string_literal: true

# Enhance db:migrate to
# before:
# - run db:prepare_migration (drops views and such)
# and then after:
# - also dump the structure.sql not just schema.rb in dev env
# - run the views recreation
#
Rake::Task["db:migrate"].enhance(["db:prepare_migration"]) do
  if Rails.env.development?
    puts "Dump Database Structure"
    ENV["SCHEMA_FORMAT"] = "sql"
    Rake::Task["db:schema:dump"].invoke
  end

  if Postgres::RemoteControl.install_fn_org_role?
    puts "Installing Postgres Function to Create Organization Roles"
    Postgres::RemoteControl.install_fn_org_role!
  end

  puts "Recreating Organization Views"
  Postgres::RemoteControl.after_migration!

  puts "Updating Default Dashboards"
  DefaultDashboardUpdateWorker.perform_async

  puts "Remove undefined privileges from db"
  Privilege.cleanup_privileges
end

Rake::Task["db:create"].enhance do
  if Postgres::RemoteControl.install_fn_org_role?
    puts "Installing Postgres Function to Create Organization Roles"
    Postgres::RemoteControl.install_fn_org_role!
  end
end

# Enhance db:schema:dump to dump Timescale hypertable functions
# currently it only dumps !simple! create_hypertable statements
# if we use more timescaledb features like compression we need to enhance it even more!
# also make sure we leave no place of fn_create_org_role as we install it ourself in test/dev only
# and in prod leave it to deployer.
#
Rake::Task["db:schema:dump"].enhance do
  if ENV["SCHEMA_FORMAT"] == "sql"
    structure_sql_schema_if_not_exists!
    structure_sql_timescale_cleanup!
    structure_sql_fn_org_role_cleanup!
    structure_sql_sort!
  else
    schema_rb_cleanup_bad_schemas!
    schema_rb_fn_org_role_cleanup!
    # prevented updating schema, but also does not seem to be needed anymore, says Germain, leave it like so
    # schema_rb_cleanup_trigger_on_timescaledb_internal!
    schema_rb_trigger_remove_public_prefix!
  end
end

def structure_sql_schema_if_not_exists!
  puts "Create schema creation with IF NOT EXISTS in structure.sql"
  rewrite_db_dump_file "structure.sql" do |file|
    file.gsub "CREATE SCHEMA", "CREATE SCHEMA IF NOT EXISTS"
  end
end

def structure_sql_timescale_cleanup!
  require "timescaledb/database"

  puts "Dumping Timescale related commands into structure.sql"

  rewrite_db_dump_file "structure.sql" do |file|
    # remove timescale triggers
    file.gsub!(/^.*ts_insert_blocker.*/, "")

    # create_hypertable commands
    hypertables = Timescaledb::Hypertable.order(:hypertable_name).to_a

    # inspired by Timescaledb::SchemaDumper#timescale_hypertable
    create_sqls = hypertables.map do |hypertable|
      dim = hypertable.main_dimension
      Timescaledb::Database.create_hypertable_sql hypertable.hypertable_name,
                                                  dim.column_name,
                                                  chunk_time_interval: dim.time_interval.inspect
    end

    puts "Hypertable Creation: #{create_sqls.inspect}"

    create_sqls.unshift "-- Creating Hypertables (after we set the correct search_path)"

    # insert before schema migrations, because it needs to be after setting the search path!
    index = file.index 'INSERT INTO "schema_migrations"'

    raise "Could not find place to insert hypertable statements" unless index

    delimited = create_sqls.map { |s| "#{s}\n\n" }.join

    file.insert index, delimited

    # remove empty lines
    file.gsub!(/\n{3,}/m, "\n\n")

    file
  end
end

def structure_sql_fn_org_role_cleanup!
  puts "Removing fn_create_org_role from structure.sql"

  rewrite_db_dump_file "structure.sql" do |file|
    regex = /^(-- Name: fn_create_org_role.*\n\n(.+\n)+\n)/
    file.gsub regex, ""
  end
end

# delete schema create statements from schema.rb because of https://github.com/rails/rails/issues/49926
# schema search path is ignored with dump format :ruby
def schema_rb_cleanup_bad_schemas!
  puts "Removing unnecessary db schemas from schema.rb"

  rewrite_db_dump_file "schema.rb" do |file|
    file.gsub(/^ {2}create_schema.*\n+/, "")
  end
end

def schema_rb_fn_org_role_cleanup!
  puts "Removing fn_create_org_role from schema.rb"

  rewrite_db_dump_file "schema.rb" do |file|
    regex = /^( {2}create_function :fn_create_org_role.*\n( {6}.*\n)+^ {2}SQL.*)/
    file.gsub regex, ""
  end
end

def structure_sql_sort!
  rewrite_db_dump_file "structure.sql" do |file|
    sections_to_sort = ["TABLE", "SEQUENCE", "SEQUENCE OWNED BY"]
    tables, rest = file.split(/(?<=-- Name: )/). # split up sections, keep the element we split on
                   map { |s| { meta: s.match(/(?<name>.*); Type: (?<type>.*)\n\n/)&.named_captures, sql: s } }. # identify section name and type
                   partition { |obj| obj.dig(:meta, "type")&.in?(sections_to_sort) } # partition the section we wanna sort and the rest
    tables.sort_by! { |obj| obj.dig(:meta, "name") } # sort the tables with its sequences
    index = (rest.count - rest.reverse.map { |obj| obj.dig(:meta, "type") }.find_index("FUNCTION")) # find the index of the last FUNCTION block
    rest.pluck(:sql).insert(index, tables.pluck(:sql)).join # merge the sorted part and the rest back together
  end
end

def schema_rb_cleanup_trigger_on_timescaledb_internal!
  rewrite_db_dump_file "schema.rb" do |file|
    file.gsub!(/create_trigger :.*\n.*CREATE TRIGGER.*_timescaledb_internal.*\n.*SQL\n/, "")
  end
end

def schema_rb_trigger_remove_public_prefix!
  rewrite_db_dump_file "schema.rb" do |file|
    file.gsub(/(CREATE TRIGGER.*)public\.(.*)/, '\1\2')
  end
end

def rewrite_db_dump_file(file)
  filepath = File.join Rails.application.root, "db/#{file}"
  old_contents = File.read filepath

  new_contents = yield old_contents

  File.write filepath, new_contents
end
