# frozen_string_literal: true

namespace :grafana do
  desc "Generate JWK data files for Grafana"
  task generate_jwk: :environment do
    key = OpenSSL::PKey::EC.generate("prime256v1")
    jwk = JWT::JWK.new(key)

    puts "Please create those files with displayed content and place at the right place\n\n"
    puts "Put as #{JWK_PK_FN} rails app config folder or /keys or set JWK_PATH"
    puts({ keys: [jwk.export(include_private: true)] }.to_json)
    puts "\n"
    puts "Give jwks.json to Grafana and set its ENV like so: GF_AUTH_JWT_JWK_SET_FILE=/keys/jwks.json"
    puts({ keys: [jwk.export] }.to_json)
  end

  # Use that after e.g. you replay a backup and the database name has changed
  desc "Switch all Organization data sources to the current database"
  task switch_datasources_to_current_db: :environment do
    Grafana::RemoteControl.switch_all_datasources_to_current_db
  end
end
