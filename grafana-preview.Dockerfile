FROM rust:1.78.0-alpine3.19

RUN apk add musl-dev
COPY ./grafana-preview/src /build/src
COPY ./grafana-preview/Cargo.toml /build/Cargo.toml
COPY ./grafana-preview/Cargo.lock /build/Cargo.lock

WORKDIR /build
RUN cargo build --release --bin grafana-preview

FROM instrumentisto/geckodriver:125.0.3

COPY --from=0 /build/target/release/grafana-preview /grafana-preview

EXPOSE 8080

ENTRYPOINT [ "/grafana-preview" ]
