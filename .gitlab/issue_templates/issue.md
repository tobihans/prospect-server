# Problem Description

(Why was this ticket created, background information)

# Expected Behavior

(What is wanted)

# Actual Behavior

(Current status, seen on demo / staging / production, organization_id, source_id, date, ...)

# Steps to Reproduce (optional)

(Code Snippet to recreate / Screenshots for visualization)

# Acceptance Criteria

(Which steps have to be taken to move this ticket to Closed, i.e. run code snippet with expected result)

# Checklist

- [ ] descriptive title
- [ ] ticket has an Assignee (optional)
- [ ] related tickets are linked
- [ ] labels are added
- [ ] the status::xxx is correct
- [ ] updates of the ticket will be posted in the comment section
