"""Supamoto data transformer.

------------------------- API data schema -------------------------
{
    "stoves": [
        {
            "deviceId": 310035199,
            "model": "MIMIMOTO",
            "status": "ACTIVE",
            "country": "MW",
            "latitude_e": <STR>,
            "latitude_b": <FLOAT>,
            "longitude_e": <STR>,
            "longitude_b": <FLOAT>,
            "certificateCid": <STR>,
            "registrationDateTime": "2022-09-07T05:55:40",
            "customer": {
                "agreementPolicy": "NONE"
            },
        },
        ...
    ],
    "orders": [
        {
          "transactionId": "SM231218.140221.LKg3l8lfQ2Y",
          "amount": 130,
          "currency": "ZMW",
          "pelletsAmount": 30,
          "pelletsAmountUnits": "kg",
          "orderType": "PELLETS",
          "deviceId": 310032290,
          "dateTime": "2023-12-18T14:02:21.197117"
        },
        ...
    ],
    "payments": [
        {
          "transactionId": "MP231218.1550.K07681",
          "amount": 250,
          "currency": "ZMW",
          "serviceProvider": "airtel",
          "deviceId": 310032290,
          "dateTime": "2023-12-18T15:50:00"
        },
        ...
    ],
}
"""

import pandas as pd

from dataworker import logger
from dataworker.processing import df_nan_to_none

TASK_NAME = "api/supamoto"
ENTRYPOINT = "transform"


def extract_meters_data(in_dict: dict) -> list[dict]:
    """Extract meter (i.e. stove) data from dict originating from datalake."""
    stoves_df = pd.DataFrame(in_dict["stoves"])
    cols_map = {
        "deviceId": "device_external_id",
        "model": "model",
        "country": "customer_country",
        "lat_p": "customer_latitude_p",
        "lat_b": "customer_latitude_b",
        "lat_e": "customer_latitude_e",
        "lon_p": "customer_longitude_p",
        "lon_b": "customer_longitude_b",
        "lon_e": "customer_longitude_e",
        "registrationDateTime": "installation_date",
        "certificateCid": "certificateCid",
    }
    stoves_df = stoves_df[stoves_df.columns.intersection(set(cols_map.keys()))].copy()
    stoves_df = stoves_df.rename(columns=cols_map)

    stoves_df["customer_external_id"] = stoves_df["device_external_id"]
    stoves_df["serial_number"] = stoves_df["device_external_id"]
    stoves_df["account_external_id"] = stoves_df["device_external_id"]
    stoves_df["manufacturer"] = "supamoto"
    stoves_df["primary_use"] = "cooking"

    stoves_df = df_nan_to_none(stoves_df)

    return stoves_df.to_dict("records")


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract payments/orders timeseries data from dicts originating from datalake."""
    payments = pd.DataFrame(in_dict["payments"])
    cols_map_payments = {
        "transactionId": "payment_external_id",
        "amount": "amount",
        "currency": "currency",
        "serviceProvider": "provider_name",
        "deviceId": "account_external_id",
        "dateTime": "paid_at",
    }
    payments = payments[cols_map_payments.keys()].copy()
    payments = payments.rename(columns=cols_map_payments)

    payments["provider_name"] = payments["provider_name"] + "_zm"
    payments["account_origin"] = "meters"
    payments["purpose"] = "payment"

    payments = df_nan_to_none(payments)

    orders = pd.DataFrame(in_dict["orders"])
    cols_map_orders = {
        "transactionId": "payment_external_id",
        "amount": "amount",
        "currency": "currency",
        "deviceId": "account_external_id",
        "dateTime": "paid_at",
        "orderType": "purchase_item",
        "pelletsAmountUnits": "purchase_unit",
        "pelletsAmount": "purchase_quantity",
    }
    orders = orders[cols_map_orders.keys()].copy()
    orders = orders.rename(columns=cols_map_orders)

    orders["account_origin"] = "meters"
    orders["purpose"] = "order"

    orders = df_nan_to_none(orders)

    return payments.to_dict("records") + orders.to_dict("records")


def transform(file_data: dict) -> list[dict]:
    """Transform a dict containing Supamoto data read from csv file in datalake.

    The output is a dictionary of the following schema:
        {
            'data_meters': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            "data_payments_ts": [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            "data_meters_ts": [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ]
        }
    """
    logger.info("transforming supamoto API data")
    return {
        "data_meters": extract_meters_data(file_data),
        "data_payments_ts": extract_payments_ts_data(file_data),
    }
