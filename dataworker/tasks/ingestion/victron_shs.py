"""Victron data transformer.

------------------------- API data schema -------------------------
    {
        "success": true,
        "records": [
            {
                "idSite: 125796,
                ...
                "name": "Chaba Minigrid",
                ...
                "pvMax": "Africa/Johannesburg",
                ...
                "realtimeUpdates": true,
                ...
                "last_timestamp": 1672332804,
                "tags": [
                    {
                        "idTag": 41,
                        "name": "no-alarm",
                        "automatic": true
                    }
                ],
                "current_time": "17:22",
                "timezone_offset": 7200,,
                ...
                "extended": [
                    {
                        "idDataAttribute": 4,
                        "code": "lt",
                        "description": "Latitude",
                        "formatWithUnit": "%.5F LAT",
                        "dataType": "float",
                        "idDeviceType": 0,
                        "textValue": null,
                        "instance": "0",
                        "timestamp": null,
                        "dbusServiceType": "gps",
                        "dbusPath": "/Position/Latitude",
                        "rawValue": "-10.978",
                        "formattedValue": "-10.97800 LAT"
                    },
                    {
                        "idDataAttribute": 5,
                        "code": "lg",
                        "description": "Longitude",
                        "formatWithUnit": "%.5F LNG",
                        "dataType": "float",
                        "idDeviceType": 0,
                        "textValue": null,
                        "instance": "0",
                        "timestamp": null,
                        ...
                    },
                    ...
                ],
                "demo_mode": false,,
                ...
                "phonenumber_p": null,
                "stats_response": {
                    "success": true,
                    "records": {
                        "Bc": [
                            [
                                1674055344000,
                                0.446044921875
                            ],
                            [
                                1674058944000,
                                0.546142578125
                            ],
                            ...
                        ],
                        "Pc": [
                            [
                                1674055344000,
                                0.10009765625
                            ],
                            [
                                1674098544000,
                                0.050048828125
                            ],
                            ...
                        ],
                        "Pb: [
                            [
                                1674102144000,
                                0.16143798828125
                            ],
                            [
                                1674105744000,
                                0.83062744140625
                            ],
                            ...
                        "bs": [
                            [
                                1674055344000,
                                99.25,
                                98.5,
                                100
                            ],
                            [
                                1674058944000,
                                97.5,
                                96.5,
                                98
                            ],
                            ...
                        ]
                    },
                    "totals": {
                        "Bc": 6.4908447265625,
                        "Pc": 2.46575927734375,
                        "Pb": 9.41436767578125,
                        "bs": 2135.482142857143
                    }
                }
            },
            ...
        ]
    }
"""

import pandas as pd
from glom import SKIP, T, Val, glom

from dataworker import logger
from dataworker.location import reverse_geo_lookup
from dataworker.processing import (
    df_nan_to_none,
    optional,
    optional_float,
)

TASK_NAME = "api/victron_shs"
ENTRYPOINT = "transform"


def calc_with_missing(df: pd.DataFrame) -> pd.DataFrame:
    """Calculate system input/output energy and power values.

    From distribution values returned by API:

    - API: Pc = PV to Customer, Bc = Battery to Customer, Pb = PV to Battery
    - SHS_TS: P = PV Input, B = Battery Input/Output, C = System Output to Customer

    Calculations:
      P = Pb + Pc, B = Pb - Bc, C = Pc + Bc

    Additional assumptions:
    - Missing values are treated as 0
    - Interval is fixed to 1 hour = 3600 seconds, thus Power[W] = Energy[Wh]
    """
    # Fill missing data with zeroes
    cols = ["pv_to_battery_kwh", "pv_to_customer_kwh", "battery_to_customer_kwh"]
    for col in cols:
        df[col] = 0 if col not in df.columns else df[col].fillna(0)

    # calculate energy columns
    df["battery_io_w"] = df.pv_to_battery_kwh * 1000 - df.battery_to_customer_kwh * 1000
    df["pv_energy_interval_wh"] = (
        df.pv_to_battery_kwh * 1000 + df.pv_to_customer_kwh * 1000
    )
    df["output_energy_interval_wh"] = (
        df.pv_to_customer_kwh * 1000 + df.battery_to_customer_kwh * 1000
    )

    # set interval to 1 hour fixed as per definition of the API
    df["interval_seconds"] = 3600

    # calculate power values (E = P/t -> E = P for t = 1)
    df["pv_input_w"] = df["pv_energy_interval_wh"]
    df["system_output_w"] = df["output_energy_interval_wh"]

    return df


def extract_stats(in_df: pd.DataFrame) -> pd.DataFrame:
    """Apply Pandas transformations to extract metric values."""
    metrics = {
        "Pc": "pv_to_customer_kwh",
        "Pb": "pv_to_battery_kwh",
        "Bc": "battery_to_customer_kwh",
        "bs": "battery_charge_percent",
    }

    stats = pd.DataFrame({
        "shs_name": pd.Series(dtype="str"),
        "metered_at": pd.Series(dtype="datetime64[ns]"),
    })

    for m, n in metrics.items():
        # Break out values from nested lists into rows. Remove empties.
        shs_df = (
            in_df[["shs_name", m]].explode(m).loc[in_df[m] != False, :]  # noqa: E712
        )
        # Extract ids.
        ids = shs_df["shs_name"].reset_index(drop=True)

        # Extract values and name metric column.
        values = pd.DataFrame(
            [[x[0], x[-1]] for x in shs_df[m].to_list()],  # First and last values only.
            columns=["metered_at", n],
        )

        # Combine.
        stat = pd.concat([ids, values], axis=1)

        # Convert epoch (ms) integers to datetime.
        stat["metered_at"] = pd.to_datetime(
            stat["metered_at"],
            unit="ms",
        )

        # Concatenate to output to combine with other metrics and construct full df.
        stats = stats.merge(stat, how="outer", on=["shs_name", "metered_at"])

    stats = stats.merge(
        in_df[["shs_name", "serial_number", "manufacturer"]],
        how="left",
        on="shs_name",
    )

    # shs but not grid fields (remember there will be missing columns sometimes)
    # battery_charge_percent = bs
    # pv_energy_wh = pc
    # output_energy_wh = pc + bc
    stats = calc_with_missing(stats)

    # Convert nan (type float) to None.
    stats = df_nan_to_none(stats)

    return stats


def extract_shs_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake.

    Method:
        Extract ids and individual metric dictionary items with glom.
        Convert to Pandas df and apply custom function extract_stats().
        Convert back to dictionary and return.
    """
    spec = (
        "records",
        [
            {
                "Pc": optional("stats_response.records.Pc"),  # Dropped later.
                "Pb": optional("stats_response.records.Pb"),  # Dropped later.
                "Bc": optional("stats_response.records.Bc"),  # Dropped later.
                "bs": optional("stats_response.records.bs"),  # Dropped later.
                "shs_name": "name",
                "serial_number": "idSite",
                "manufacturer": Val("victron"),
            }
        ],
    )

    glommed = glom(in_dict, spec)

    # Data transformations.
    shs_ts_df = pd.DataFrame(glommed)
    shs_ts_df = extract_stats(shs_ts_df)

    out_dict = shs_ts_df.to_dict(orient="records")

    return out_dict


def extract_shs_data(in_dict: dict) -> list[dict]:
    """Extract/transform shs data from dictionary originating from datalake.

    Method:
        Extract information using GLOM.
        Return resulting dictionary.
    """
    spec = (
        "records",
        [
            {
                "account_external_id": "idSite",
                "customer_external_id": "idSite",
                "device_external_id": "idSite",
                "serial_number": "idSite",
                "manufacturer": Val("victron"),
                "name": "name",
                "customer_latitude_b": optional_float("lat_b"),
                "customer_latitude_p": optional("lat_p"),
                "customer_latitude_e": optional("lat_e"),
                "customer_longitude_b": optional_float("lon_b"),
                "customer_longitude_p": optional("lon_p"),
                "customer_longitude_e": optional("lon_e"),
                "customer_phone_p": optional("phonenumber_p"),
                "customer_phone_e": optional("phonenumber_e"),
                "fw_version": optional((
                    "extended",
                    [
                        lambda x: (
                            x["rawValue"] if x["description"] == "Fw Version" else SKIP
                        )
                    ],
                    T[0],
                )),
            }
        ],
    )

    # TODO:
    # - account_external_id: idSite
    # - customer_external_id: idSite
    # - voltage_v, rated_power_w, pv_power_w, battery_energy_wh: (
    #   some may be in `extended` attribute list)
    # - customer lat/lon (with `_b`?): same as grids

    records = glom(in_dict, spec)

    return [
        i
        | reverse_geo_lookup(
            i["customer_latitude_b"],
            i["customer_longitude_b"],
            geo_col_prefix="customer_",
        )
        for i in records
    ]


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Victron data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_shs': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_shs_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Victron API data.")
    return {
        "data_shs": extract_shs_data(file_data),
        "data_shs_ts": extract_shs_ts_data(file_data),
    }
