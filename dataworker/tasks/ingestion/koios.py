"""Koios ak Sparkmeter v2 data transformer.

Sparkmeter is a company that produces connected electricity meters or "smart
meters". An organization that operates an electrical utility such as a grid or
minigrid can purchase Sparkmeter devices, install them at their customers' homes
or places of business, and they start sending data to the Sparkmeter servers.
The devices are linked to the organization's user account with the Sparkmeter
service where they can monitor consumption as well as conduct customer managment
and billing.

TODO: insert link to API doc

"""

import pandas as pd
from glom import Check, Coalesce, Flatten, T, Val, glom

from dataworker import logger
from dataworker.location import reverse_geo_lookup
from dataworker.processing import df_nan_to_none, optional

TASK_NAME = "api/sparkmeter_koios"
ENTRYPOINT = "transform"


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract data from the Sparkmeter API data for the payment time series.

    -----| WIP
    - need to talk to users about how to map meter account to fields,
      similar to paygo payments
    """
    pass


def extract_meters_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake."""
    meters_ts_spec = (
        "customers",
        [
            (
                "meters",
                [
                    {
                        "serial_number": "serial",
                        "metered_at_str": optional("latest_reading.heartbeat_start"),
                        "phase": optional("meter_phase"),
                        "voltage_v": optional("latest_reading.voltage_avg"),
                        "power_factor": optional("latest_reading.power_factor_avg"),
                        "power_w": optional("latest_reading.apparent_power_avg"),
                        "energy_interval_wh": optional("latest_reading.energy"),
                        "frequency_hz": optional("latest_reading.frequency"),
                        "current_a": optional("latest_reading.current_avg"),
                        "interval_start": optional("latest_reading.heartbeat_start"),
                        "interval_end": optional("latest_reading.heartbeat_end"),
                        "billing_cycle_start_at": optional(
                            "tariff.last_block_rate_cycle_reset_at"
                        ),
                        "manufacturer": Val("Sparkmeter"),
                    }
                ],
            )
        ],
        Flatten(),
    )

    meters_ts_glom = glom(in_dict, meters_ts_spec)

    meters_ts_df = pd.DataFrame(meters_ts_glom)

    meters_ts_df["metered_at"] = pd.to_datetime(meters_ts_df.metered_at_str)
    meters_ts_df["start"] = pd.to_datetime(meters_ts_df.interval_start)
    meters_ts_df["end"] = pd.to_datetime(meters_ts_df.interval_end)
    # handling the rubbish made by converting the difference of NaT and NaT to integer
    interval = (meters_ts_df.end - meters_ts_df.start).fillna(pd.to_timedelta(0))
    interval = interval.astype("int64") / 1000000000
    meters_ts_df["interval"] = interval
    return meters_ts_df.to_dict(orient="records")


def extract_meters_data(in_dict: dict) -> list[dict]:
    """Extract/transform meter data from dictionary originating from datalake.

    get all the customer level stuff and do explode once on serial number,
    then do all the meter level stuff in a separate spec and then merge in pandas
    """
    cust_spec = (
        "customers",
        [
            {
                "customer_external_id": "id",
                "customer_name_p": optional("name_p"),
                "customer_name_e": optional("name_e"),
                "customer_phone_e": optional("phone_number_e"),
                "customer_phone_p": optional("phone_number_p"),
                "serial_number": ("meters", ["serial"]),
                "grid_name": optional("site_id"),
                "manufacturer": Val("Sparkmeter"),
                "is_test": Val(False),
            }
        ],
    )

    cust_glom = glom(in_dict, cust_spec)

    cust_df = pd.DataFrame(cust_glom).explode("serial_number")

    meters_spec = (
        "customers",
        [
            (
                "meters",
                [
                    {
                        "customer_address_e": optional("address_e"),
                        "customer_address_p": optional("address_p"),
                        "customer_latitude_b": optional("coordinates.lat_b"),
                        "customer_longitude_b": optional("coordinates.lon_b"),
                        "device_external_id": "id",
                        "serial_number": "serial",
                        "customer_category": optional("tariff.name"),
                        "account_external_id": optional("tariff_id"),
                        "payment_plan": optional("tariff.name"),
                        "max_power_w": optional((
                            "tariff.load_limit.value",
                            Coalesce((Check(type=list), [T["load_limit"]], max), T),
                        )),
                    }
                ],
            )
        ],
        Flatten(),
    )

    meters_glom = glom(in_dict, meters_spec)

    meters_df = pd.DataFrame(meters_glom)

    meters_combo_df = cust_df.merge(meters_df, how="inner", on="serial_number")

    meters_combo_df = df_nan_to_none(meters_combo_df)

    meters_combo = meters_combo_df.to_dict(orient="records")  # convert to json

    # complete records with location names
    meters_combo = [
        i
        | reverse_geo_lookup(
            i["customer_latitude_b"],
            i["customer_longitude_b"],
            geo_col_prefix="customer_",
        )
        for i in meters_combo
    ]

    return meters_combo


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Sparkmeter data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_meters': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_meters_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("transforming spark meter KOIOS API data")
    return {
        "data_meters": extract_meters_data(file_data),
        "data_meters_ts": extract_meters_ts_data(file_data),
        # for the moment we skip the payment info
        # "data_payments_ts": extract_payments_ts_data(file_data),
    }
