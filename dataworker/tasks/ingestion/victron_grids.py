"""Victron data transformer.

------------------------- API data schema -------------------------
    {
        "success": true,
        "records": [
            {
                "idSite: 125796,
                ...
                "name": "Chaba Minigrid",
                ...
                "pvMax": "Africa/Johannesburg",
                ...
                "realtimeUpdates": true,
                ...
                "last_timestamp": 1672332804,
                "tags": [
                    {
                        "idTag": 41,
                        "name": "no-alarm",
                        "automatic": true
                    }
                ],
                "current_time": "17:22",
                "timezone_offset": 7200,,
                ...
                "extended": [
                    {
                        "idDataAttribute": 4,
                        "code": "lt",
                        "description": "Latitude",
                        "formatWithUnit": "%.5F LAT",
                        "dataType": "float",
                        "idDeviceType": 0,
                        "textValue": null,
                        "instance": "0",
                        "timestamp": null,
                        "dbusServiceType": "gps",
                        "dbusPath": "/Position/Latitude",
                        "rawValue": "-10.978",
                        "formattedValue": "-10.97800 LAT"
                    },
                    {
                        "idDataAttribute": 5,
                        "code": "lg",
                        "description": "Longitude",
                        "formatWithUnit": "%.5F LNG",
                        "dataType": "float",
                        "idDeviceType": 0,
                        "textValue": null,
                        "instance": "0",
                        "timestamp": null,
                        ...
                    },
                    ...
                ],
                "demo_mode": false,,
                ...
                "phonenumber_p": null,
                "stats_response": {
                    "success": true,
                    "records": {
                        "Bc": [
                            [
                                1674055344000,
                                0.446044921875
                            ],
                            [
                                1674058944000,
                                0.546142578125
                            ],
                            ...
                        ],
                        "Pc": [
                            [
                                1674055344000,
                                0.10009765625
                            ],
                            [
                                1674098544000,
                                0.050048828125
                            ],
                            ...
                        ],
                        "Pb: [
                            [
                                1674102144000,
                                0.16143798828125
                            ],
                            [
                                1674105744000,
                                0.83062744140625
                            ],
                            ...
                        "bs": [
                            [
                                1674055344000,
                                99.25,
                                98.5,
                                100
                            ],
                            [
                                1674058944000,
                                97.5,
                                96.5,
                                98
                            ],
                            ...
                        ]
                    },
                    "totals": {
                        "Bc": 6.4908447265625,
                        "Pc": 2.46575927734375,
                        "Pb": 9.41436767578125,
                        "bs": 2135.482142857143
                    }
                }
            },
            ...
        ]
    }
"""

import pandas as pd
from glom import SKIP, T, glom

from dataworker import logger
from dataworker.location import reverse_geo_lookup
from dataworker.processing import (
    df_nan_to_none,
    optional,
    optional_float,
)

TASK_NAME = "api/victron_grids"
ENTRYPOINT = "transform"


def extract_stats(in_df: pd.DataFrame) -> pd.DataFrame:
    """Apply Pandas transformations to extract metric values."""
    metrics = {
        "Pc": "primary_source_to_customer_kwh",
        "Pb": "primary_source_to_battery_kwh",
        "Bc": "battery_to_customer_kwh",
        "bs": "battery_charge_state_percent",
    }

    stats = pd.DataFrame({
        "grid_name": pd.Series(dtype="str"),
        "metered_at": pd.Series(dtype="datetime64[ns]"),
    })

    for m, n in metrics.items():
        # Break out values from nested lists into rows. Remove empties.
        shs_df = (
            in_df[["grid_name", m]].explode(m).loc[in_df[m] != False, :]  # noqa: E712
        )
        # Extract ids.
        ids = shs_df["grid_name"].reset_index(drop=True)

        # Extract values and name metric column.
        values = pd.DataFrame(
            [[x[0], x[-1]] for x in shs_df[m].to_list()],  # First and last values only.
            columns=["metered_at", n],
        )

        # Combine.
        stat = pd.concat([ids, values], axis=1)

        # Convert epoch (ms) integers to datetime.
        stat["metered_at"] = pd.to_datetime(
            stat["metered_at"],
            unit="ms",
        )

        # Concatenate to output to combine with other metrics and construct full df.
        stats = stats.merge(stat, how="outer", on=["grid_name", "metered_at"])

    stats = stats.merge(
        in_df[["grid_name"]],
        how="left",
        on="grid_name",
    )

    # Convert nan (type float) to None.
    stats = df_nan_to_none(stats)

    return stats


def extract_grids_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake.

    Method:
        Extract ids and individual metric dictionary items with glom.
        Convert to Pandas df and apply custom function extract_stats().
        Convert back to dictionary and return.
    """
    spec = (
        "records",
        [
            {
                "Pc": optional("stats_response.records.Pc"),  # Dropped later.
                "Pb": optional("stats_response.records.Pb"),  # Dropped later.
                "Bc": optional("stats_response.records.Bc"),  # Dropped later.
                "bs": optional("stats_response.records.bs"),  # Dropped later.
                "grid_name": "name",
            }
        ],
    )

    glommed = glom(in_dict, spec)

    # Data transformations.
    grids_ts_df = pd.DataFrame(glommed)
    grids_ts_df = extract_stats(grids_ts_df)

    out_dict = grids_ts_df.to_dict(orient="records")

    return out_dict


def extract_grids_data(in_dict: dict) -> list[dict]:
    """Extract/transform grid data from dictionary originating from datalake.

    Method:
        Extract information using GLOM.
        Return resulting dictionary.
    """
    spec = (
        "records",
        [
            {
                "external_id": "idSite",
                "name": "name",
                "latitude": optional_float("lat_b"),
                "longitude": optional_float("lon_b"),
                "fw_version": optional((
                    "extended",
                    [
                        lambda x: (
                            x["rawValue"] if x["description"] == "Fw Version" else SKIP
                        )
                    ],
                    T[0],
                )),
            }
        ],
    )

    records = glom(in_dict, spec)

    # complete records with location
    return [i | reverse_geo_lookup(i["latitude"], i["longitude"]) for i in records]


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Victron data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_grids': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_grids_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Victron API data.")
    return {
        "data_grids": extract_grids_data(file_data),
        "data_grids_ts": extract_grids_ts_data(file_data),
    }
