"""Generic data transformer."""

import funcy

ENTRYPOINT = "transform"


def transform_key(key: str) -> str:
    """Prefix 'data_' to keys that don't start with '_'."""
    return f"data_{key}" if not key.startswith("_") else key


def transform_value(key: str, value: str | list) -> str | list:
    """Prefix 'data_' to value(s) when key is '_update'."""
    if key == "_update":
        return (
            [f"data_{v}" for v in value] if isinstance(value, list) else f"data_{value}"
        )
    return value


def transform(file_data: dict) -> dict:
    """Pass data through nearly unchanged, altering only (some) key and value strings.

    Identify keys and values which represent table names based on criteria and append
    the expected prefix 'data_'.
    """
    return funcy.walk(
        lambda kv: (transform_key(kv[0]), transform_value(kv[0], kv[1])),
        file_data,
    )
