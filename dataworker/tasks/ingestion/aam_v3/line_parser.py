"""AAMv3 line_parser methods module."""

import arrow
from funcy import walk_keys
from glom import Coalesce, Invoke, M, T, glom

from dataworker.binary import (
    BitField,
    binary_string_short_int,
    make_int_from_shorts,
    make_uint_from_shorts,
)
from dataworker.logging import logger
from dataworker.time import parse_utc_us_timestamp
from dataworker.utils import map_access, nofail

# parsed timestamps out of those year boundaries is most likely due to an
# unsynced device clock. Such samples are unusable and will be discarded.
REALISTIC_TIMESTAMP_LOWER_BOUND = arrow.get("2000")
REALISTIC_TIMESTAMP_UPPER_BOUND = arrow.get("2050")


# secure binary functions
binary_string_short_int = nofail(binary_string_short_int)
make_int_from_shorts = nofail(make_int_from_shorts)
make_uint_from_shorts = nofail(make_uint_from_shorts)

# ------------------------- shared -------------------------


def split_line(line: str) -> list[str]:
    """Split line by comma, removing leading and trailing whitspaces."""
    elements = line.split(",")
    return [e.strip() for e in elements]


def parse_shared_elements(elements: list) -> dict:
    """Return a dict containing time, log_id and line_tag out of elements."""
    return {
        "time": parse_utc_us_timestamp(elements[0]),
        "log_id": int(elements[1]),
        "line_tag": elements[2],
    }


# ------------------------- binary status fields -------------------------


def _parse_epsolar_grid_status(register_value: str) -> dict[str, bool | int]:
    bits = BitField(register_value)
    expanded = {
        "status_register": binary_string_short_int(int(register_value)),
        "input_voltage_status": map_access(
            {0: "normal", 1: "low", 2: "high", 3: "no"}, bits[15:14]
        ),
        "output_power_load_status": map_access(
            {0: "light", 1: "medium", 2: "nominal", 3: "over"}, bits[13:12]
        ),
        "busbar_over_voltage": bits[5],
        "busbar_under_voltage": bits[6],
        "input_over_current": bits[7],
        "abnormal_output_voltage": bits[8],
        "heat_sink_overheating": bits[9],
        "hardware_overvoltage": bits[10],
        "short_circuit": bits[11],
        "low_temperature": bits[4],
        "status": map_access(
            {0: "no", 1: "float", 2: "boost", 3: "equalizing"}, bits[3:2]
        ),
        "run": bits[0],
    }
    return walk_keys(lambda key: "grid_" + key, expanded)


def _parse_epsolar_load_output_status(register_value: str) -> dict[str, bool | int]:
    bits = BitField(register_value)
    expanded = {
        "status_register": binary_string_short_int(int(register_value)),
        "input_voltage_status": map_access(
            {0: "normal", 1: "low", 2: "high", 3: "no"}, bits[15:14]
        ),
        "output_power_load_status": map_access(
            {0: "light", 1: "medium", 2: "nominal", 3: "over"}, bits[13:12]
        ),
        "output_fail": bits[5],
        "high_voltage_side_short_circuit": bits[6],
        "input_over_current": bits[7],
        "abnormal_output_voltage": bits[8],
        "unable_to_stop_discharge": bits[9],
        "unable_to_discharge": bits[10],
        "short_circuit": bits[11],
        "abnormal_frequency": bits[4],
        "high_temperature": bits[3],
        "low_temperature": bits[2],
        "run": bits[1],
        "faults": bits[0],
    }
    return walk_keys(lambda key: "load_output_" + key, expanded)


def _parse_epsolar_pv_status(register_value: str) -> dict[str, bool | int]:
    bits = BitField(register_value)
    expanded = {
        "status_register": binary_string_short_int(int(register_value)),
        "input_voltage_status": map_access(
            {0: "normal", 1: "without", 2: "high", 3: "error"}, bits[15:14]
        ),
        "charging_mos_tube_short_circuit": bits[13],
        "charging_antireverse_mos_tube_open_circuit": bits[12],
        "antireverse_mos_tube_short_circuit": bits[11],
        "input_over_current": bits[10],
        "load_over_current": bits[9],
        "load_short_circuit": bits[8],
        "load_mos_tube_short_circuit": bits[7],
        "run": bits[0],
        "faults": bits[1],
        "charging_status": map_access(
            {0: "no", 1: "float", 2: "boost", 3: "equalizing"}, bits[3:2]
        ),
        "pv_input_short_circuit": bits[4],
        "led_load_open_circuit": bits[5],
        "three_way_circuit_imbalance": bits[6],
    }
    return walk_keys(lambda key: "pv_" + key, expanded)


def _parse_epsolar_battery_status(register_value: str) -> dict[str, bool | int]:
    """Epsolar battery status parser.

    For the registers of status and temperature_status, 4 bits are planned,
    but only 3 and 2 respectively are set, thus we ignore the other bits.
    """
    bits = BitField(register_value)
    match bits[2:0]:
        case 0:
            status = "normal"
        case 1:
            status = "over_voltage"
        case 2:
            status = "under_voltage"
        case 3:
            status = "over_discharge"
        case _:  # this is mainly 4, but also all values up to 7 the third bit is 1
            status = "faults"
    expanded = {
        "status_register": binary_string_short_int(int(register_value)),
        "status": status,
        "temperature_status": map_access(
            {
                0: "normal",
                1: "over_temperature",
                2: "under_temperature",
                3: "faults",
            },
            bits[5:4],
        ),
        "abnormal_internal_resistance": bits[8],
        "lithium_charging_protection": bits[9],
        "lithium_discharging_protection": bits[10],
        "nominal_voltage_identification_error": bits[15],
    }
    return walk_keys(lambda key: "battery_" + key, expanded)


# ------------------------- V1 -------------------------


def parse_inverter_elements_v1(elements: list[str]) -> dict:
    """Return dict parsing elements for inverter v1."""
    sample = {
        "input_voltage": float(elements[3]) / 1000,
        "output_voltage": float(elements[4]) / 1000,
        "input_fault_voltage": float(elements[5]) / 1000,
        "relative_output_power": float(elements[6]) * 24 / 600,
        "output_frequency": float(elements[7]) / 1000,
        "battery_voltage": 2 * float(elements[8]) / 1000,
        "temperature": float(elements[9]) / 1000,
        "status": int(elements[10]),
    }
    return walk_keys(lambda key: "inverter_" + key, sample)


def parse_MPPT_elements_v1(elements: list[str]) -> dict:
    """Return dict parsing elements for MPTT v1."""
    sample = {
        "panel_voltage": float(elements[3]) / 100,
        "panel_current": float(elements[4]) / 100,
        "battery_voltage": float(elements[5]) / 100,
        "battery_current": float(elements[6]) / 100,
        "output_voltage": float(elements[7]) / 100,
        "output_current": float(elements[8]) / 100,
        "external_temperature": float(elements[9]) / 100,
        "internal_temperature": float(elements[10]) / 100,
        "battery_temperature": float(elements[11]) / 100,
        "state_of_charge": int(elements[12]),
        "battery_status": int(elements[13]),
        "mppt_status": int(elements[14]),
    }
    return walk_keys(lambda key: "mppt_" + key, sample)


def parse_coulomb_elements_v1(elements: list[str]) -> dict:
    """Return dict parsing elements for coulomb v1."""
    sample = {
        "battery_status": int(elements[3]),
        "gauge_status": int(elements[4]),
        "operational_status": int(elements[5]),
        "time_to_empty": int(elements[6]),
        "time_to_full": int(elements[7]),
        "cycle_count": int(elements[8]),
        "state_of_charge": int(elements[9]),
        "state_of_health": int(elements[10]),
        "external_temperature": int(elements[11]),
        "internal_temperature": int(elements[12]),
        "battery_voltage": float(elements[13]),
        "battery_current": float(elements[14]),
        "remaining_capacity": float(elements[15]),
        "full_charge_capacity": float(elements[16]),
        "calibration_status": int(elements[17]),
    }
    return walk_keys(lambda key: "coulomb_" + key, sample)


def parse_paygo_elements_v1(elements: list[str]) -> dict:
    """Return dict parsing elements for paygo v1."""
    sample = {
        "time_left": int(elements[3]),
        "token_count": int(elements[4]),
    }
    return walk_keys(lambda key: "paygo_" + key, sample)


def parse_upower_elements_v1(elements: list[str]) -> dict:
    """Return dict parsing elements for upower v1."""
    sample = (
        {
            "grid_input_voltage": int(elements[3]) / 100,
            "grid_input_current": int(elements[4]) / 100,
            "grid_input_power": make_uint_from_shorts(
                int(elements[6]), int(elements[5])
            )
            / 100,
            "grid_input_frequency": int(elements[7]) / 100,
            "total_cumulative_grid_energy": make_uint_from_shorts(
                int(elements[9]), int(elements[8])
            )
            / 100,
            "grid_charging_battery_temperature": int(elements[11]) / 100,
            "grid_charging_device_temperature": int(elements[12]) / 100,
            "grid_charging_power_device_temperature": int(elements[13]) / 100,
            "load_input_voltage": int(elements[14]) / 100,
            "load_output_voltage": int(elements[15]) / 100,
            "load_output_current": int(elements[16]) / 100,
            "load_output_frequency": int(elements[18]) / 100,
            "load_total_cumulative_energy_consumption": make_uint_from_shorts(
                int(elements[20]), int(elements[19])
            )
            / 100,
            "load_heat_sink_1_temperature": int(elements[21]) / 100,
            "load_heat_sink_2_temperature": int(elements[22]) / 100,
            "pv_input_voltage": int(elements[23]) / 100,
            "pv_input_current": int(elements[24]) / 100,
            "pv_input_power": make_uint_from_shorts(
                int(elements[26]), int(elements[25])
            )
            / 100,
            "pv_total_cumulative_charge_energy": make_uint_from_shorts(
                int(elements[28]), int(elements[27])
            )
            / 100,
            "battery_voltage": int(elements[30]) / 100,
            "battery_current": make_int_from_shorts(
                int(elements[32]), int(elements[31])
            )
            / 100,
            "battery_state_of_charge": elements[33],
        }
        | _parse_epsolar_grid_status(elements[10])
        | _parse_epsolar_load_output_status(elements[17])
        | _parse_epsolar_pv_status(elements[29])
        | _parse_epsolar_battery_status(elements[34])
    )
    # precalculate power for convenience
    sample["battery_power"] = sample["battery_voltage"] * sample["battery_current"]
    sample["load_output_power"] = (
        sample["load_output_voltage"] * sample["load_output_current"]
    )
    return walk_keys(lambda key: "upower_" + key, sample)


# ------------------------- V2 -------------------------

# glom.M : use as if it was the target. raise glom.MatchError when comparison
# fails, which allow to skip to default instead of failing the call. `M` alone
# test for a value's truthiness.
# Here `M` is used to skip to default when encountering empty fields.


def parse_inverter_elements_v2(elements: list[str]) -> dict:
    """Return dict parsing elements for inverter v2."""
    spec = {
        "input_voltage": Coalesce(("3", M, float, T / 1000), default=None),
        "output_voltage": Coalesce(("4", M, float, T / 1000), default=None),
        "relative_output_power": Coalesce(("5", M, float, T * 24 / 600), default=None),
        "output_frequency": Coalesce(("6", M, float, T / 1000), default=None),
        "temperature": Coalesce(("7", M, float, T / 1000), default=None),
    }
    sample = glom(elements, spec)
    return walk_keys(lambda key: "inverter_" + key, sample)


def parse_MPPT_elements_v2(elements: list[str]) -> dict:
    """Return dict parsing elements for MPTT v2."""
    spec = {
        "panel_voltage": Coalesce(("3", M, float, T / 100), default=None),
        "panel_current": Coalesce(("4", M, float, T / 100), default=None),
        "battery_voltage": Coalesce(("5", M, float, T / 100), default=None),
        "battery_current": Coalesce(("6", M, float, T / 100), default=None),
        "internal_temperature": Coalesce(("7", M, float, T / 100), default=None),
        "battery_temperature": Coalesce(("8", M, float, T / 100), default=None),
    }
    sample = glom(elements, spec)
    return walk_keys(lambda key: "mppt_" + key, sample)


def _calibration_complete(calibration_status: bytes) -> bool:
    """Bit magic formula."""
    return calibration_status & 0xFFEF == 0x002F


def parse_coulomb_elements_v2(elements: list[str]) -> dict:
    """Return dict parsing elements for coulomb v2."""
    spec = {
        "gauge_status": Coalesce(("3", M, int), default=None),
        "time_to_empty": Coalesce(("4", M, int), default=None),
        "time_to_full": Coalesce(("5", M, int), default=None),
        "cycle_count": Coalesce(("6", M, int), default=None),
        "state_of_charge": Coalesce(("7", M, int), default=None),
        "state_of_health": Coalesce(("8", M, int), default=None),
        "battery_voltage": Coalesce(("9", M, float, T / 1000), default=None),
        "battery_current": Coalesce(("10", M, float, T / 1000), default=None),
        "calibration_status": Coalesce(("11", M, int), default=None),
        "calibration_complete": Coalesce(
            ("11", M, int, _calibration_complete), default=None
        ),
    }
    sample = glom(elements, spec)
    return walk_keys(lambda key: "coulomb_" + key, sample)


def parse_metering_elements_v2(elements: list[str]) -> dict:
    """Return dict parsing elements for metering v2."""
    spec = {
        "pv_e_in": Coalesce(("3", M, float), default=None),
        "load_e_battery": Coalesce(("4", M, float), default=None),
        "load_e_grid": Coalesce(("5", M, float), default=None),
        "grid_e_charged": Coalesce(("6", M, float), default=None),
        "bat_e_charged": Coalesce(("7", M, float), default=None),
        "bat_e_discharged": Coalesce(("8", M, float), default=None),
        "bat_v_min": Coalesce(("9", M, float, T / 100), default=None),
        "bat_v_max": Coalesce(("10", M, float, T / 100), default=None),
        "bat_i_min": Coalesce(("11", M, float, T / 100), default=None),
        "bat_i_max": Coalesce(("12", M, float, T / 100), default=None),
    }
    sample = glom(elements, spec)
    return walk_keys(lambda key: "metering_" + key, sample)


def parse_upower_elements_v2(elements: list[str]) -> dict:
    """Return dict parsing elements for upower v2."""
    main_spec = {
        "input_voltage": Coalesce(("3", M, int, T / 100), default=None),
        "input_current": Coalesce(("4", M, int, T / 100), default=None),
        "input_frequency": Coalesce(("5", M, int, T / 100), default=None),
        "battery_temperature": Coalesce(("6", M, int, T / 100), default=None),
        "device_temperature": Coalesce(("7", M, int, T / 100), default=None),
        "output_voltage": Coalesce(("8", M, int, T / 100), default=None),
        "output_current": Coalesce(("9", M, int, T / 100), default=None),
        "pv_input_voltage": Coalesce(("10", M, int, T / 100), default=None),
        "pv_input_current": Coalesce(("11", M, int, T / 100), default=None),
        "battery_voltage": Coalesce(("12", M, int, T / 100), default=None),
        "battery_current": Coalesce(
            (
                Invoke(make_int_from_shorts).specs(("14", M, int), ("13", M, int)),
                T / 100,
            ),
            default=None,
        ),
        "SOC": Coalesce(("15", M, int), default=None),
        "protocol_type": Coalesce(("17", M, int), default=None),
    }
    battery_status_spec = Coalesce(("16", M, _parse_epsolar_battery_status), default={})
    sample = glom(elements, main_spec) | glom(elements, battery_status_spec)
    return walk_keys(lambda key: "upower_" + key, sample)


# --------------------------------------------------


def parse_line(line: str) -> dict | None:
    """Extract values from a datafile line.

    If the extraction fails, no exception is raised, the error is logged and
    None returned.
    """
    log_rotate_str = "ROTATED"
    try:
        elements = split_line(line)
        shared_data = parse_shared_elements(elements)
        # the various rotation entry miss a line break,
        # avoid having invalid line breaks
        line_tag = shared_data["line_tag"]
        rotate_pos = line_tag.find(log_rotate_str)
        if rotate_pos > 0:
            line_tag = line_tag[: rotate_pos + len(log_rotate_str)]
        logger.debug("line tag: {line_tag}", line_tag=line_tag)
        match line_tag:
            # inverter
            case "INV":
                specific_data = parse_inverter_elements_v1(elements)
            case "IN" | "IN0":
                specific_data = parse_inverter_elements_v2(elements)
            # MPPT
            case "MPPT":
                specific_data = parse_MPPT_elements_v1(elements)
            case "MP" | "MP0":
                specific_data = parse_MPPT_elements_v2(elements)
            # coulomb counter
            case "COULOMB":
                specific_data = parse_coulomb_elements_v1(elements)
            case "CO" | "CO0":
                specific_data = parse_coulomb_elements_v2(elements)
            # upower module
            case "UPOWER":
                specific_data = parse_upower_elements_v1(elements)
            case "UP" | "UP0":
                specific_data = parse_upower_elements_v2(elements)
            # TODO: new measurements, clear how they will map with SKGS when stable
            # METERING_COSUPER | METERING_UPOWER
            case "MC0" | "MU0":
                logger.info("ignoring still unsupported {}", line_tag)
                return None
            # aggregated measurements
            case "ME" | "ME0":
                specific_data = parse_metering_elements_v2(elements)
            # paygo
            case "PAYGO" | "PA" | "PA0":
                specific_data = parse_paygo_elements_v1(elements)
            # log rotation, FCB ROTATED or FINE LOG ROTATED or DAY LOG ROTATED
            # NB: We have to use the guard feature of match-case
            # Here rotate has the value of line_tag but only matches on condition
            # see https://peps.python.org/pep-0636/#adding-conditions-to-patterns
            case rotate if rotate.endswith(log_rotate_str):
                logger.info("encountered log rotation")
                if len(elements) == 3:
                    return None
                # Log rotation is printed without linebreak!!!
                return parse_line(
                    line[line.find(log_rotate_str) + len(log_rotate_str) :]
                )
            # aam internals, only present when DEBUG: true
            case "EV0":
                return None
            case _:
                logger.warning("unknown line type: {}", line_tag)
                return None

        return shared_data | specific_data
    except Exception as exception:
        logger.opt(exception=True).warning(
            "error occured while parsing line: {}",
            repr(exception),
            line=line,
        )
