"""Submodule providing the transform entry point."""

from dataworker import logger
from dataworker.dataset import RecordSet

from . import reshape, samples_parser, split

MANUFACTURER = "A2EI"


def transform(file_data: str) -> RecordSet:
    """Transform text from file to RecordSet for AAMv3."""
    file_metadata, _device_info, sample_data = split.split_file(file_data)
    logger.info(
        "importing data coming from device {device_id}",
        device_id=file_metadata.device_id,
    )
    samples = samples_parser.parse_samples(sample_data)
    dataset = reshape.reshape_samples(
        samples,
        file_context={
            "manufacturer": MANUFACTURER,
            "serial_number": file_metadata.device_id,
        },
    )
    return dataset
