"""Module with methods for parsing and aggregating AAMv3 samples."""

import functools
import operator
from collections.abc import Generator

import arrow
import funcy
import pandas
from funcy import lkeep, lmap, lsplit, select_keys

from dataworker.logging import logger

from . import line_parser

# parsed timestamps out of those year boundaries is most likely due to an
# unsynced device clock. Such samples are unusable and will be discarded.
REALISTIC_TIMESTAMP_LOWER_BOUND = arrow.get("2000")
REALISTIC_TIMESTAMP_UPPER_BOUND = arrow.get("2050")


# ----------- time-based aggregation of samples -----------


def _specific_data(sample: dict) -> filter:
    """Filter out generic fields from a sample."""
    return select_keys(lambda k: k not in ["time", "log_id", "line_tag"], sample)


def _merge_samples(samples: list[dict]) -> dict:
    """Merge data from a group of samples that was emmited at the same time."""
    specific_logs = map(_specific_data, samples)
    group_time = samples[0]["time"]
    log_types = ",".join(sample["line_tag"] for sample in samples)
    return functools.reduce(
        operator.or_, specific_logs, {"time": group_time, "log_types": log_types}
    )


def _group_samples(samples: list[dict]) -> Generator[list[dict], None, None]:
    """Group together samples that were created within a 10s time window."""
    packet = []
    pd_timedelta = pandas.Timedelta(10, "s")
    for sample in samples:
        if not packet or sample["time"] - packet[-1]["time"] < pd_timedelta:
            packet.append(sample)
        else:
            yield packet
            packet = [sample]
    yield packet


def _aggregate_logs_timewise(logs: list[dict]) -> list[dict]:
    """Aggregate together samples that were created almost at the same time."""
    logs = sorted(logs, key=operator.itemgetter("time"))
    aggregated_logs = lmap(_merge_samples, _group_samples(logs))
    logger.debug(
        "time aggregation has yield {nb_logs} samples", nb_logs=len(aggregated_logs)
    )
    return aggregated_logs


# ------------------------------------------------


def _parse_file_content(content: str) -> list[dict]:
    """Extract samples from file."""
    lines = lkeep(content.split("\n"))  # empty lines filtered right away
    logger.debug("input data is {nb_lines} lines long", nb_lines=len(lines))
    samples = [
        sample for line in lines if (sample := line_parser.parse_line(line)) is not None
    ]
    if difference := (len(lines) - len(samples)):
        logger.error(
            "{nb_skipped_lines} lines could not be parsed", nb_skipped_lines=difference
        )
    else:
        logger.debug("all lines parsed successfully")
    if samples:
        return _aggregate_logs_timewise(samples)
    else:
        return []


def _validate_sample(sample: dict) -> bool:
    """Sample validation function."""
    if not (
        REALISTIC_TIMESTAMP_LOWER_BOUND
        < sample["time"]
        < REALISTIC_TIMESTAMP_UPPER_BOUND
    ):
        logger.debug(
            "unrealistic timestamp {timestamp}",
            timestamp=sample["time"],
            event="sample_timestamp_error",
        )
        return False
    return True


def _filter_samples(samples: list[dict]) -> list[dict]:
    """Remove unvalid samples."""
    valid_samples, invalid_samples = lsplit(_validate_sample, samples)
    if invalid_samples:
        logger.info(
            "{nb_samples} samples filtered out",
            nb_samples=len(invalid_samples),
            event="invalid_samples",
        )
    return valid_samples


def _forward_fill_missing_values(samples: list[dict]) -> list[dict]:
    logger.debug("forward fill missing values")
    samples_df = pandas.DataFrame(samples)
    samples_df_filled = samples_df.ffill()
    samples = samples_df_filled.to_dict("records")
    # the operation can transform None into nan, that can't be inserted in the
    # database. Those need to be reverted to None.
    samples = funcy.lmap(
        funcy.partial(funcy.walk_values, lambda v: v if pandas.notna(v) else None),
        samples,
    )
    return samples


def parse_samples(file_content: str) -> list[dict]:
    """Extract complete and valid samples from data file.

    Into an an intermediary format corresponding to the original full format.
    """
    samples = _parse_file_content(file_content)
    samples = _filter_samples(samples)
    samples = _forward_fill_missing_values(samples)
    logger.info(
        "{nb_samples} samples parsed from the file after aggregation and validation",
        nb_samples=len(samples),
        event="file_parsed",
    )
    if not samples:  # empty list
        logger.error("no valid samples could be extracted from the file content")
    return samples
