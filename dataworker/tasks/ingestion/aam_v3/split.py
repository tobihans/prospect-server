"""Module handling information extraction and file metadata for AAMv3."""

import datetime
import json
import re
from dataclasses import dataclass
from pathlib import Path

import arrow

from dataworker.logging import logger

# --------------------------------------------------


def _extract_json_header(file_data: str) -> tuple[dict, str]:
    """Extract data from the json header added by the connector."""
    separator = re.search(r"\n---\n", file_data)
    header = file_data[: separator.start()]
    body = file_data[separator.end() :]
    file_metadata = json.loads(header)
    return file_metadata, body


@dataclass
class FileMetadata:
    """Data structure for aam_v3 file metadata."""

    device_id: int
    file_creation_time: datetime.datetime
    file_upload_time: datetime.datetime


def _convert_file_metadata(header_data: dict) -> dict:
    """Convert file header into structured data."""
    file_metadata = {}

    key = Path(header_data["key"])
    file_metadata["device_id"] = int(str(key.parent))

    numbers = re.findall(r"\d+", key.stem)
    longest_number = max(numbers, key=len)
    file_metadata["file_creation_time"] = (
        arrow.get(int(longest_number)).replace(microsecond=0).datetime
    )

    usable_datestring = (
        re.search(r"[\d -:]*", header_data["last_modified"]).group(0).strip()
    )
    file_metadata["file_upload_time"] = (
        arrow.get(usable_datestring).replace(microsecond=0).datetime
    )
    return FileMetadata(**file_metadata)


def _extract_file_metadata(file_data: str) -> tuple[dict, str]:
    """Extract data from the json header added by the connector."""
    header_data, body = _extract_json_header(file_data)
    logger.debug("file header extracted")
    return _convert_file_metadata(header_data), body


def _extract_device_info(file_data_rest: str) -> tuple[dict, str]:
    """Extract data from the json footer added by the connector."""
    # matches the first smallest multiline section that starts with a solo `{` and
    # ends with a solo `}`
    first_non_nested_json_object = r"^{\s*$[\S\s]*?^}\s*$"
    first_object = re.search(
        first_non_nested_json_object, file_data_rest, flags=re.MULTILINE
    )
    if first_object:
        device_info = json.loads(first_object.group(0))
        body = file_data_rest[: first_object.start()]
        logger.debug("device info footer extracted")
        return device_info, body
    else:
        return {}, file_data_rest


def split_file(file_data: str) -> tuple[dict, dict, str]:
    """Split file data into file info, device info and samples."""
    file_metadata, file_data_rest = _extract_file_metadata(file_data)
    device_info, sample_data = _extract_device_info(file_data_rest)
    return file_metadata, device_info, sample_data
