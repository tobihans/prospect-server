"""Module to reshape, parse and extract data from AAM v3."""

from glom import Coalesce, M, T, glom

from dataworker.dataset import RecordSet
from dataworker.processing import clip_int4

SECOND_PER_HOUR = 60 * 60
SECOND_PER_DAY = 60 * 60 * 24
DAY_PER_YEAR = 365

# ------------------------- secondary parsing -------------------------

# secondary transformation operations change the parsed sample data into the
# prospect database format

DEFAULT_INVERTER_SIZE = 3000

paygo_days_left_spec = (
    "paygo_time_left",
    M,
    int,
    lambda seconds: seconds / SECOND_PER_DAY,
)

paygo_is_paid_off_spec = (
    paygo_days_left_spec,
    lambda days: days / DAY_PER_YEAR > 100,
)


def is_coulomb_counter_calibrated(data: dict) -> bool:
    """Return True if upower_protocol_type = 0 and calibration completed, else False."""
    return data.get("upower_protocol_type") == 0 and data.get(
        "coulomb_calibration_complete", False
    )


def battery_io_w(data: dict) -> float | None:
    """Return Battery watts from current and voltage."""
    if is_coulomb_counter_calibrated(data):
        current = data.get("coulomb_battery_current")
        voltage = data.get("coulomb_battery_voltage")
        if current and voltage:
            return current * voltage
    current = data.get("upower_battery_current")
    voltage = data.get("upower_battery_voltage")
    if current and voltage:
        return current * voltage
    return None


def battery_charge_percent(data: dict) -> int | float:
    """Return battery charge in percent."""
    is_calibrated = is_coulomb_counter_calibrated(data)
    if is_calibrated and (charge := data.get("coulomb_state_of_charge")):
        return charge
    return data.get("upower_SOC")


shs_ts_sample_spec = {
    "metered_at": "time",
    "pv_input_w": Coalesce(
        T["upower_pv_input_voltage"] * T["upower_pv_input_current"],
        T["mppt_panel_voltage"] * T["mppt_panel_current"],
        default=None,
    ),
    "grid_input_w": Coalesce(
        T["upower_input_voltage"] * T["upower_input_current"], default=None
    ),
    "grid_input_v": Coalesce(
        "upower_input_voltage", "inverter_input_voltage", default=None
    ),
    "battery_v": Coalesce(
        "upower_battery_voltage", "mppt_battery_voltage", default=None
    ),
    "battery_io_w": battery_io_w,
    "battery_charge_percent": battery_charge_percent,
    "system_output_w": Coalesce(
        T["upower_output_voltage"] * T["upower_output_current"],
        T["inverter_relative_output_power" * DEFAULT_INVERTER_SIZE],
        default=None,
    ),
    "grid_energy_lifetime_wh": Coalesce(
        (T["metering_grid_e_charged"] + T["metering_load_e_grid"]) / SECOND_PER_HOUR,
        default=None,
    ),
    "pv_energy_lifetime_wh": Coalesce(
        T["metering_pv_e_in"] / SECOND_PER_HOUR, default=None
    ),
    "output_energy_lifetime_wh": Coalesce(
        "upower_load_total_cumulative_energy_consumption",
        (T["metering_load_e_battery"] + T["metering_load_e_grid"]) / SECOND_PER_HOUR,
        default=None,
    ),
    "paygo_is_paid_off": Coalesce(paygo_is_paid_off_spec, default=None),
    "paygo_days_left": Coalesce((paygo_days_left_spec, clip_int4), default=None),
}


def reshape_samples(
    primary_samples: list[dict], file_context: dict | None = None
) -> RecordSet:
    """Take the samples and return a RecordSet that can be stored."""
    if file_context is None:
        file_context = {}
    shs_ts_samples = glom(primary_samples, [shs_ts_sample_spec])
    aggregated_samples = [
        original_sample | shs_ts_sample | file_context
        for (original_sample, shs_ts_sample) in zip(
            primary_samples, shs_ts_samples, strict=False
        )
    ]
    return {"data_shs_ts": aggregated_samples}
