"""mtn data transformer.

------------------------- statement data schema -------------------------
[
    {
      "Id": "4602230849",
      "External id": "8d4c7e17-d356-4fee-990d-1f759f7eddc2",
      "Date": "2023-09-30 16:42:41",
      "Status": "Successful",
      "Type": "Debit",
      "Provider Category": "2.00",
      "Information": "",
      "To message": "Merchant: Supamoto",
      "To": "FRI:36172410/MM",
      "To Name": "Supamoto Solar Payment",
      "To Handler Name": "Supamoto Solar Payment",
      "Initiated By": "",
      "On Behalf Of": "",
      "Amount": "130.00",
      "Currency": "ZMW",
      "External Amount": "",
      "External FX Rate": "",
      "External Service Provider": "",
      "Fee": "2.60",
      "Discount": "",
      "Promotion": "",
      "Coupon": "",
      "Balance": "",
      "From_e": null,
      "From_p": "c0a179aa4d58c2a5fa38368a293cfbe492c18de15796f9107e7aaff033a8067f",
      "From Name_e": null,
      "From Name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7",
      "From Handler Name_e": null,
      "From Handler Name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7"
    },
    ...
]

"""  # noqa: E501

from datetime import UTC, datetime

from glom import SKIP, Val, glom

from dataworker import logger
from dataworker.processing import optional

TASK_NAME = "mtn_zm_payments_statement"
ENTRYPOINT = "transform"


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract statements timeseries data from dict originating from datalake."""
    spec = (
        [
            lambda t: t if t["Status"] == "Successful" else SKIP,
        ],
        [
            lambda t: t if t["Type"] == "Debit" else SKIP,
        ],
        [
            {
                "payment_external_id": "Id",
                "account_external_id": Val("unknown"),
                "paid_at": (
                    "Date",
                    lambda t: datetime.strptime(t, "%Y-%m-%d %H:%M:%S").replace(
                        tzinfo=UTC
                    ),
                ),
                "amount": "Amount",
                "provider_name": Val("mtn_zm"),
                "purpose": Val("mobile money statement"),
                "currency": Val("ZMW"),
                "account_origin": Val("meters"),
                "payer_e": optional("From_e"),
                "payer_p": optional("From_p"),
                "payer_name_e": optional("From Name_e"),
                "payer_name_p": optional("From Name_p"),
                "payer_handler_name_e": optional("From Handler Name_e"),
                "payer_handler_name_p": optional("From Handler Name_p"),
                "external_transaction_id": "External id",
                "payee": optional("To"),
                "payee_name": optional("To Name"),
                "payee_handler_name": optional("To Handler Name"),
                "note": "To message",
            }
        ],
    )

    glommed = glom(in_dict, spec)

    return glommed


def transform(file_data: dict) -> list[dict]:
    """Transform a dict containing MTN data read from dict in datalake.

    The output is a dictionary of the following schema:
        {
            "data_payments_ts": [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
        }
    """
    logger.info("transforming MTN statement data")
    return {
        "data_payments_ts": extract_payments_ts_data(file_data),
    }
