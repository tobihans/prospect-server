"""Solarman data transformer."""

import datetime
import os
from io import BytesIO
from typing import BinaryIO

import requests
from glom import glom

from dataworker import logger, storage
from dataworker.config import config
from dataworker.location import reverse_geo_lookup
from dataworker.processing import optional
from dataworker.types import RecordSetType

TASK_NAME = "api/solarman"
ENTRYPOINT = "transform"


def extract_daily_stats(in_dict: dict, grid_name: str) -> dict:
    """Calculate the power that went from the primary source to the customer.

    It is the total generation minus the power that went to the battery.
    """
    for key in in_dict:
        key["grid_name"] = grid_name
        if (
            key["total_generation_power_kwh"] is not None
            and key["primary_source_to_battery_kwh"] is not None
        ):
            key["primary_source_to_customer_kwh"] = (
                key["total_generation_power_kwh"] - key["primary_source_to_battery_kwh"]
            )
        else:
            key["primary_source_to_customer_kwh"] = None

        if (
            key["total_consumption_power_kwh"] is not None
            and key["battery_to_customer_kwh"] is not None
        ):
            key["primary_source_to_customer_kwh"] = (
                key["total_consumption_power_kwh"] - key["battery_to_customer_kwh"]
            )
        else:
            key["primary_source_to_customer_kwh"] = None

    """ calculate the metered_at from the year, month and day """
    for key in in_dict:
        if key["metered_at_year"] and key["metered_at_month"] and key["metered_at_day"]:
            key["metered_at"] = datetime.datetime(
                key["metered_at_year"],
                key["metered_at_month"],
                key["metered_at_day"],
                23,
                59,
                59,
            )
        else:
            key["metered_at"] = None

    """ remove metered and total_power_kwh """
    for key in in_dict:
        del key["metered_at_year"]
        del key["metered_at_month"]
        del key["metered_at_day"]
        del key["total_generation_power_kwh"]
        del key["total_consumption_power_kwh"]

    return in_dict


def extract_grids_daily_ts_data(in_dict: dict, grid_name: str) -> dict | None:
    """Extract/transform timeseries data from dictionary originating from datalake.

    [{'generationPower': None,
    'usePower': None,
    'gridPower': None,
    'purchasePower': None,
    'wirePower': None,
    'chargePower': None,
    'dischargePower': None,
    'batteryPower': None,
    'batterySoc': None,
    'irradiateIntensity': None,
    'generationValue': 48.9,
    'generationRatio': 19.0184,
    'gridRatio': 0.0,
    'chargeRatio': 80.9816,
    'useValue': 47.3,
    'useRatio': 19.66173,
    'buyRatio': 0.0,
    'useDischargeRatio': 80.33827,
    'gridValue': 0.0,
    'buyValue': 0.0,
    'chargeValue': 39.6,
    'dischargeValue': 38.0,
    'fullPowerHours': 2.797483,
    'irradiate': None,
    'theoreticalGeneration': None,
    'pr': None,
    'cpr': None,
    'dateTime': None,
    'year': 2023,
    'month': 11,
    'day': 13}].
    """
    spec = (
        [
            {
                "metered_at_day": optional("day"),
                "metered_at_month": optional("month"),
                "metered_at_year": optional("year"),
                "total_generation_power_kwh": optional("generationValue"),
                "total_consumption_power_kwh": optional("useValue"),
                "primary_source_to_battery_kwh": optional(
                    "chargeValue"
                ),  # re-calculated later
                "battery_to_customer_kwh": optional(
                    "dischargeValue"
                ),  # re-calculated later
            }
        ],
    )
    glommed = glom(in_dict["stats_daily_aggregated_data"], spec)
    recalculated = extract_daily_stats(glommed, grid_name)

    if recalculated is None or len(recalculated) == 0:
        return None
    else:
        return recalculated


""" in  case we will need the detailed data from the api in the future

def extract_grids_ts_data(in_dict: dict):

    Method:
        Extract ids and individual metric dictionary items with glom.
        Convert to Pandas df and apply custom function extract_stats().
        Convert back to dictionary and return.

        {'generationPower': 2216.0,
        'usePower': 1611.0,
        'gridPower': None,
        'purchasePower': None,
        'wirePower': 0.0,
        'chargePower': -1883.0,
        'dischargePower': 1795.0,
        'batteryPower': -88.0,
        'batterySoc': 100.0,
        'irradiateIntensity': None,
        'generationValue': None,
        'generationRatio': 17.25888,
        'gridRatio': None,
        'chargeRatio': None,
        'useValue': None,
        'useRatio': None,
        'buyRatio': None,
        'useDischargeRatio': None,
        'gridValue': None,
        'buyValue': None,
        'chargeValue': None,
        'dischargeValue': None,
        'fullPowerHours': None,
        'irradiate': None,
        'theoreticalGeneration': None,
        'pr': None,
        'cpr': None,
        'dateTime': 1699879800.0,
        'year': 2023,
        'month': 11,
        'day': 13},



    spec = (
        [
            {
                "metered_at_timestamp": optional("dateTime"),
                "total_generation_power_w": optional("generationPower"),
                "total_consumption_power_w": optional("usePower"),
                "primary_source_to_battery_w": optional("chargePower"),  # re-calculated later
                "battery_to_customer_w": optional("dischargePower"),  # re-calculated later
                "battery_charge_state_percent": optional("batterySoc"),
            }
        ],
    )

    glommed = glom(in_dict, spec)

    # Data transformations.
    df = pd.DataFrame(glommed)
    df = extract_stats(df)

    out_dict = df.to_dict(orient="records")

    return out_dict
"""  # noqa: E501


def extract_data(in_dict: dict) -> tuple[list[dict], list[dict]]:
    """Extract/transform grid data from dictionary originating from datalake.

    Method:
        Extract information using GLOM.
        Return resulting dictionary.
    """
    spec = (
        [
            {
                "external_id": "id",
                "name": "name",
                "location_area_1": optional("regionLevel1"),
                "location_area_2": optional("regionLevel2"),
                "location_area_3": optional("regionLevel3"),
                "location_area_4": optional("regionLevel4"),
                "latitude": optional("locationLat"),
                "longitude": optional("locationLng"),
                "power_rating_kw": optional("generationPower"),
                "primary_input_installed_kw": optional("installedCapacity"),
                "operator_phone_e": optional("contactPhone_e"),
                "operator_phone_p": optional("contactPhone_p"),
                "stats_daily_aggregated_data": optional("stats_daily_aggregated_data"),
                "custom": {"station_image": optional("stationImage")},
            }
        ],
    )

    grids_data = glom(in_dict, spec)

    # complete records with location names
    grids_data = [
        i | reverse_geo_lookup(i["latitude"], i["longitude"]) for i in grids_data
    ]

    grids_ts_data = []
    for key in grids_data:
        data = extract_grids_daily_ts_data(key, key["name"])
        if data is not None:
            grids_ts_data += data

    """ remove the stats_daily_aggregated_data """
    for key in grids_data:
        del key["stats_daily_aggregated_data"]

    """ if there is a station image, try to download and save it """
    for key in grids_data:
        station_image = key["custom"]["station_image"]
        if station_image is None:
            continue
        not_downloaded_yet = image_is_not_downloaded_yet(station_image)
        if not_downloaded_yet and download_image_and_save_to_minio(station_image):
            key["custom"]["station_image_local"] = os.path.basename(station_image)

    return grids_data, grids_ts_data


def image_is_not_downloaded_yet(image_url: str) -> bool:
    """Checks if an image has already been downloaded and saved to MinIO.

    Args:
        image_url (str): The URL of the image to check.

    Returns:
        bool: True if the image has not been downloaded yet, False otherwise.
    """
    filename = os.path.basename(image_url)
    bucket_name = config.get("storage.bucket")
    minio_client = storage.get_minio_client()
    try:
        minio_client.stat_object(bucket_name, filename)
    except Exception:
        return True
    else:
        return False


def download_image(url: str) -> BytesIO:
    """Download data from url and return the BytesIO ReadableBuffer."""
    response = requests.get(url, timeout=5)
    if response.status_code != 200:
        raise Exception(  # noqa: TRY002
            f"Failed to download image from {url}. Status code: {response.status_code}"
        )
    return BytesIO(response.content)


def save_to_minio(bucket_name: str, filename: str, data: BinaryIO) -> None:
    """Save data to minio given bucket and file name."""
    minio_client = storage.get_minio_client(write=True)
    minio_client.put_object(bucket_name, filename, data, len(data.getvalue()))
    logger.info(
        "Image uploaded successfully to Min.io bucket: {}/{}", bucket_name, filename
    )


def download_image_and_save_to_minio(image_url: str) -> bool | None:
    """Download an image from the provided URL and save it to the MinIO bucket.

    Args:
        image_url (str): The URL of the image to download.

    Returns:
        bool: True if the image was downloaded successfully, False otherwise.
    """
    # Download the image data from the URL
    try:
        img_io = download_image(image_url)
    except requests.exceptions.RequestException:
        return False

    # Get the image filename from the URL
    filename = os.path.basename(image_url)

    bucket_name = config.get("storage.bucket")

    # Save the image data to the MinIO bucket
    try:
        save_to_minio(bucket_name, filename, img_io)
    except Exception:
        logger.exception("Failed to save image to MinIO")
        return False
    else:
        return True


def transform(file_data: dict) -> RecordSetType:
    """Transform a dictionary containing Victron data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_grids': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_grids_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Solarman API data.")
    grids_data, grids_ts_data = extract_data(file_data)

    return {
        "data_grids": grids_data,
        "data_grids_ts": grids_ts_data,
    }
