"""airtel data transformer.

------------------------- statement data schema -------------------------
[
    {
      "Record No": "1",
      "Transaction ID": "MP240108.0621.G07354",
      "Reference No.": "C297050015",
      "Transaction Date & Time": "08-JAN-2024  06:21:01",
      "Payer MFS Provider": "Airtel",
      "Payer Payment Instrument": "WALLET",
      "Payer Wallet Type/Linked Bank": "Normal",
      "Sender Grade": "Gold Subscriber",
      "Payer Category": "Subscriber",
      "Payee MFS Provider": "Airtel",
      "Payee Payment Instrument": "WALLET",
      "Payee Wallet Type/Linked Bank": "Normal",
      "Receiver Mobile Number": "889073368",
      "Payee Bank Account No/Mobile No": "889073368",
      "Receiver Category": "Head Merchant",
      "Receiver Grade": "2PercentMerchant",
      "Payee User Name": "EMERGING COOKING SOLUTIONS",
      "Payee Nick Name": "XXXXMOTO",
      "Service Type": "Merchant Payment",
      "Transaction Status": "Transaction Success",
      "Transaction Amount": "130",
      "Payer Previous Balance": "NA",
      "Payer Post Balance": "NA",
      "Payee Pre Balance": ".25",
      "Payee Post Balance": "130.25",
      "Total Service Charge": "1",
      "Payer Bank Account No/Mobile No_e": null,
      "Payer Bank Account No/Mobile No_p": "083a7d48cbd489602e72f1514f902451c7e7ff34b9bc1fa49ca5c453b45f2525",
      "Payer User Name_e": null,
      "Payer User Name_p": "e268d8120f5ec2996ef2f66c5f4cd3fa6fe9be4956ca0747c75494456d58946a",
      "Payer Nick Name_e": null,
      "Payer Nick Name_p": null,
      "Payer Mobile Number_e": null,
      "Payer Mobile Number_p": "083a7d48cbd489602e72f1514f902451c7e7ff34b9bc1fa49ca5c453b45f2525"
    },
    ...
]

"""  # noqa: E501

from datetime import UTC, datetime

from glom import SKIP, Val, glom

from dataworker import logger
from dataworker.processing import optional

TASK_NAME = "airtel_zm_customer_transactions"
ENTRYPOINT = "transform"


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract statements timeseries data from dict originating from datalake."""
    spec = (
        [
            lambda t: t if t["Service Type"] == "Merchant Payment" else SKIP,
        ],
        [
            lambda t: t if t["Transaction Status"] == "Transaction Success" else SKIP,
        ],
        [
            {
                "payment_external_id": "Transaction ID",
                "account_external_id": Val("unknown"),
                "paid_at": (
                    "Transaction Date & Time",
                    lambda t: datetime.strptime(t, "%d-%b-%Y  %H:%M:%S").replace(
                        tzinfo=UTC
                    ),
                ),
                "amount": "Transaction Amount",
                "provider_name": Val("airtel_zm"),
                "purpose": Val("mobile money statement"),
                "currency": Val("ZMW"),
                "account_origin": Val("meters"),
                "payer_account_e": optional("Payer Bank Account No/Mobile No_e"),
                "payer_account_p": optional("Payer Bank Account No/Mobile No_p"),
                "payer_user_name_e": optional("Payer User Name_e"),
                "payer_user_name_p": optional("Payer User Name_p"),
                "payer_nickname_e": optional("Payer Nick Name_e"),
                "payer_nickname_p": optional("Payer Nick Name_p"),
                "payer_phone_e": optional("Payer Mobile Number_e"),
                "payer_phone_p": optional("Payer Mobile Number_p"),
                "payer_grade": optional("Sender Grade"),
                "payer_category": optional("Payer Category"),
                "payee_account": optional("Payee Bank Account No/Mobile No"),
                "payee_user_name": optional("Payee User Name"),
                "payee_nickname": optional("Payee Nick Name"),
                "payee_phone": optional("Receiver Mobile Number"),
                "payee_grade": optional("Receiver Grade"),
                "payee_category": optional("Receiver Category"),
            }
        ],
    )

    glommed = glom(in_dict, spec)

    return glommed


def transform(file_data: dict) -> list[dict]:
    """Transform a dict containing airtel data read from dict in datalake.

    The output is a dictionary of the following schema:
        {
            "data_payments_ts": [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
        }
    """
    logger.info("transforming airtel statement data")
    return {
        "data_payments_ts": extract_payments_ts_data(file_data),
    }
