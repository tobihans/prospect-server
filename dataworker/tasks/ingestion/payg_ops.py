"""Paygops data transformer.

------------------------- API data schema -------------------------

{
    "contracts":
    [
        {
            'id': 101,
            'reference': 'C1430016',
            'status': 'Active',
            'start_time': '2023-04-17T13:55:39.874974Z',
            'next_payment_due_time': '2023-08-16T13:55:39.874974Z',
            'client_id': 92,
            'portfolio_id': 4,
            'offer_id': 35,
            'offer_code': '2017',
            'linked_device_serial_number': 'OPT-AAM00004844',
            'total_value': 1050000.0,
            'total_yearly_value': 304166.67,
            'add_ons': [],
            'cumulative_days_in_arrear': '2.582258333333333333333333334',
            'cumulative_amount_in_arrears': '0.00',
            'expected_amount_repaid': '325000.00',
            'cumulative_amount_repaid': '325000.00',
            'outstanding_balance': '725000.00',
            'last_payment_datetime': '2023-07-17T09:27:26.858792Z',
            'delays_since_last_payment': '1',
            'reference_pricing_amount': '25000.00',
            'reference_pricing_duration': '30.0000',
            'expected_maturity_date': '2026-01-01T13:55:39.874974Z',
            'expected_loan_duration': '990.00',
            'actual_maturity_date': '2026-01-02T13:55:39.874974Z',
            'total_lump_sum_add_ons_value': '0',
            'total_delays_given': '1.0000',
            'total_number_of_repayments_expected': 33
            'total_discounted': 0,
            'total_value_without_discounts': 1050000.0
        }
    ],
    "clients":
    [
        {
            'id': 21,
            'birthdate': '1970-01-01T00:00:00Z',
            'gender': 'Unknown',
            'custom_id': None,
            'note': '',
            'registration_date': '2022-12-14T12:50:02.763959Z',
            'picture_id': '729bb952-7bad-11ed-b52d-0242ac13000a',
            'village_id': 10024,
            'l0_entity_id': 24,
            'client_group_id': None,
            'portfolio_id': 2,
            'verbal_language': '',
            'sms_language': 'EN',
            'next_payment_due_date':
            '2023-06-13T15:42:24.579978Z',
            'phone_numbers': ['30bb707fe6d9803aa0180f1d8b27686d981fb1e800b3f80951c70e9bb683f430'],
            'contracts': ['C540021', 'C540013'],
            'leads': [29, 28],
            'form_answers': {'Data for A2EI': 9},
            'tags': [],
            'home': True,
            'business': False,
            'name_p': '81b5465796fe658fa580746023284b35a03c3f952b57c9b0f28ee3c625da8c76',
            'preferred_phone_number_p': None,
            'lat_p': '2e3fb3caef04a50d788070101d307dd407be8cab53638aa3276cc2180c8f957d',
            'lon_p': '0cb487ab08aec2fe9a16c71362a949695ac6ee475af14782eedb3063ac0b4bc9',
            'lat_b': '7.466',
            'lon_b': '9.069'
        }
    ],
    "contract_repayments":
    [
        {
            'id': 1,
            'date': '2022-08-11T16:11:48.915774Z',
            'type': 'Downpayment',
            'contract_reference': 'C60012',
            'client_id': 1,
            'total_amount': '400.00',
            'amount_paid': '400.00',
            'amount_discounted': '0.0000',
            'reconciled_payments': [1]
        }
    ],
    "contract_events":
    [
        {
            'id': 1,
            'date': '2022-08-11T16:11:48.915774Z',
            'type': 'Creation',
            'contract_reference': 'C60012',
            'client_id': 1,
            'repayment_id': 1,
            'approver_user_id': 5,
            'note': '',
            'discounted_amount': '0.00',
            'old_device_serial': None,
            'new_device_serial': None,
            'old_offer_id': None,
            'new_offer_id': None
        }
     ],
    "payments":
    [
        {
            'transaction_id': 'ef8f-8b95',
            'amount': '400.00',
            'wallet_msisdn': '',
            'sent_datetime': '2022-08-11T16:10:42.156647Z',
            'reception_datetime': '2022-08-11T16:10:42.156647Z',
            'memo': '',
            'wallet_operator': '',
            'country': '',
            'currency': '',
            'destinations': [
                {
                    'repayment_id': 44,
                    'lead_id': 1,
                    'user_id': None,
                    'client_id': None,
                    'add_on_reference': None,
                    'contract_reference': 'C60012',
                    'reversed': True,
                    'destination_type': 'contract_repayment',
                    'destination': 'C60012'
                },
                {
                    'repayment_id': 1,
                    'lead_id': 1,
                    'user_id': None,
                    'client_id': None,
                    'add_on_reference': None,
                    'contract_reference': 'C60012',
                    'reversed': True,
                    'destination_type': 'contract_repayment',
                    'destination': 'C60012'
                }
            ],
            'destination_type': 'contract_repayment',
            'destination': 'C60012',
            'wallet_name_p': 'cee3b3264fc5f0fa54cd48e0415ea798ef3bffb08dc89454b66313bce9897567'
        }
    ],
    "payment_reconciliations": [
        {
            "id": 5,
            "time": "2022-08-19T14:55:09.118789Z",
            "amount": "400.00",
            "type": "Downpayment",
            "note": "",
            "wallet": 3,
            "payment": 7,
            "payment_transaction_id": "edba-78c8",
            "contract_reference": "C90019",
            "lead": 2,
            "user": null,
            "client_id": null,
            "add_on": null,
            "add_on_id": null,
            "reversed_payment": null,
            "reversed": false,
            "reversed_by": null
        }
    ],
    "offers":
    [
        {
            'id': 1,
            'based_on': None,
            'name': 'system123',
            'code': '001',
            'type': 'Loan',
            'can_be_approved_and_registered': True,
            'in_use_for_new_leads': True,
            'approval_required': True,
            'device_type': 'OPT',
            'lighting_global_compliant': False,
            'panel_size_in_w': 800,
            'raw_unit_cost': 1000.0,
            'battery_size_in_ah': 50,
            'family': 'Home',
            'notes': '',
            'allowed_addon_offer_categories_ids': [],
            'entities_allowed_for_leads_ids': [],
            'entities_allowed_for_contracts_ids': [3],
            'time_to_ownership_in_days': 28,
            'automatic_unlock_code_sending': True,
            'allow_loan_addons': False,
            'maximum_value_extension': None,
            'downpayment': 400.0,
            'base_price_amount': 200.0,
            'discount_price_1_amount': None,
            'discount_price_2_amount': None,
            'minimum_payment': 100.0,
            'base_price_time_in_days': 7,
            'discount_price_1_time_in_days': None,
            'discount_price_2_time_in_days': None,
            'time_given_at_start_in_days': 1,
            'allow_pro_rata': True,
            'forgive_lateness': False
        }
    ],
    "lead_generators": [
        {
            "id": 1,
            "birthdate": null,
            "gender": "Unknown",
            "phone_numbers": [],
            "type": "Sales Leader",
            "working": true,
            "start_date": "2022-08-09T05:43:32.576774Z",
            "linked_user_id": 4,
            "name_e": null,
            "name_p": "7e6cd592f8c560192ec3f2b5ad7b9161d8da39be5317f5890605fd4bbfbc1cef",
            "preferred_phone_number_e": null,
            "preferred_phone_number_p": null
        }
    ],
    "settings/CurrencyISOName": {
        "name": "CurrencyISOName",
        "value": "NGN"
    },
    "settings/CountryName": {
        "name": "CountryName",
        "value": "Nigeria"
    },
    "payment_export": {
        "70": {
            "Id": "70",
            "Contract Id": "9",
            "Contract Reference": "C220012",
            "Client Id": "8",
            "Payment Date": "2022-11-09 14:28",
            "Expected Payment Date": "2022-12-02 14:14",
            "Days Late": "-22.99",
            "Total Amount": "233.33",
            "Amount Paid": "0.00",
            "Amount Of Discount": "233.33",
            "Credit Value": "0.54",
            "Type": "Manual Discount",
            "Last Updated": "2023-09-04 13:43"
        }
    }
}
"""  # noqa: E501

import pandas as pd
from glom import Val, glom

from dataworker import logger
from dataworker.dataset import RecordSet
from dataworker.processing import df_nan_to_none, optional

TASK_NAME = "api/payg_ops"
ENTRYPOINT = "transform"


def clean_a2ei_serial_number(serial_number: str) -> str:
    """For A2EI devices, remove the SN prefix and heading zeros."""
    return serial_number.removeprefix("OPT-AAM").lstrip("0")


def payg_ops_pre_processor(in_dict: dict) -> list[dict]:
    """Extract individual dataframes from data lake dump and preprocess."""
    contracts = pd.DataFrame(in_dict["contracts"])
    clients = pd.DataFrame(in_dict["clients"])
    contract_repayments = pd.DataFrame(in_dict["contract_repayments"])
    contract_events = pd.DataFrame(in_dict["contract_events"])
    # payments = pd.DataFrame(in_dict["payments"])
    offers = pd.DataFrame(in_dict["offers"])
    leads = pd.DataFrame(in_dict["leads"])
    lead_generators = pd.DataFrame(in_dict["lead_generators"])
    # payment_reconciliations = pd.DataFrame(in_dict["payment_reconciliations"])
    currency = in_dict["settings/CurrencyISOName"]["value"]
    # extract and transform country code.
    country = in_dict["settings/CountryName"]["value"]
    payment_export = pd.DataFrame(list(in_dict["payment_export"].values()))
    test_tags = in_dict["test_tags"] or []

    contracts = contracts.merge(
        offers, left_on="offer_id", right_on="id", suffixes=("_contracts", "_offers")
    )
    contract_details = contracts.merge(
        clients,
        left_on="client_id",
        right_on="id",
        suffixes=(None, "_clients"),
    )
    # DATA CLEANING
    # set is_test True if tag is in test_tags array
    contract_details["is_test"] = False
    if test_tags is not None and isinstance(test_tags, list):
        contract_details.loc[
            contract_details["tags"].apply(
                lambda tags: any(x in tags for x in test_tags)
            ),
            "is_test",
        ] = True

    # customer_category
    category = "customer_category"
    contract_details.loc[
        contract_details.home == True, category  # noqa: E712
    ] = "household"
    contract_details.loc[
        contract_details.business == True, category  # noqa: E712
    ] = "commercial"

    # calculate payment_plan_amount_financed
    contract_details["payment_plan_amount_financed"] = (
        contract_details["total_value"] - contract_details["downpayment"]
    )
    # merge contract_events to get max paid_off_date per contract
    if len(contract_events) > 0:
        contract_details = contract_details.merge(
            contract_events.loc[contract_events.type == "Completion"]
            .groupby(["contract_reference"])["date"]
            .max()
            .to_frame()
            .reset_index()
            .rename(columns={"date": "paid_off_date"}),
            left_on="reference",
            right_on="contract_reference",
            how="left",
        )
        # merge contract_events to get max repossession date per contract
        contract_details = contract_details.merge(
            contract_events.loc[contract_events.type == "Repossession"]
            .groupby(["contract_reference"])["date"]
            .max()
            .to_frame()
            .reset_index()
            .rename(columns={"date": "repossession_date"}),
            left_on="reference",
            right_on="contract_reference",
            how="left",
        )

    # extract year from birthdate
    contract_details["customer_birth_year"] = contract_details.birthdate.astype(
        str
    ).str[:4]
    # change manufacturer if A2EI
    contract_details.loc[
        contract_details.linked_device_serial_number.str.contains(
            ".*-AAM.*", regex=True, na=False
        ),
        "manufacturer",
    ] = "A2EI"
    contract_details.loc[contract_details.manufacturer.isna(), "manufacturer"] = (
        "Undefined"
    )
    # serial_number is last 4 digits of linked_device_serial_number
    contract_details.loc[contract_details.manufacturer == "A2EI", "serial_number"] = (
        contract_details.loc[
            contract_details.manufacturer == "A2EI", "linked_device_serial_number"
        ].apply(clean_a2ei_serial_number)
    )
    contract_details.loc[contract_details.manufacturer != "A2EI", "serial_number"] = (
        contract_details.linked_device_serial_number
    )
    # convert expected_loan_duration to int
    contract_details.expected_loan_duration = pd.to_numeric(
        contract_details.expected_loan_duration
    )
    # .astype("Int64")
    # convert customer_birth_year to int
    # replace string 'None' with None
    contract_details.customer_birth_year = contract_details.customer_birth_year.replace(
        "None", None
    )
    contract_details.customer_birth_year = pd.to_numeric(
        contract_details.customer_birth_year
    ).astype("Int64")
    # clean Status in contract details
    # TODO implement mapping of "Paused" status from PaygOps
    replacement_status = {
        "Cancelled": "Detached",
        "Defaulted": "Written-Off",
        "Completed": "Unlocked",
        "Active": "Enabled",
    }
    contract_details["status"] = contract_details["status"].replace(
        replacement_status, regex=True
    )
    contract_details = contract_details.rename(columns={"status": "contract_status"})
    # merge repayments and events
    if (len(contract_events) > 0) & (len(contract_repayments) > 0):
        contract_repayments.id = contract_repayments.id.astype("Int64")
        contract_events.repayment_id = contract_events.repayment_id.astype("Int64")
        contract_positions = contract_repayments.merge(
            contract_events,
            left_on="id",
            right_on="repayment_id",
            suffixes=("_repayments", "_events"),
            how="left",
        )
    elif len(contract_repayments) > 0:
        contract_positions = contract_repayments
        # add suffix to all column names and pretend the merge happened
        contract_positions = contract_positions.add_suffix("_repayments")

    elif len(contract_events) > 0:
        contract_positions = contract_events
        # Hotfix
        contract_positions = contract_positions.rename(
            columns={
                "id": "id_repayments",
                "date": "date_repayments",
                "contract_reference": "contract_reference_repayments",
            },
        )
        contract_positions.loc[
            contract_positions.type.str.contains("Pause|Resume"), "id_repayments"
        ] = (
            contract_positions.loc[
                contract_positions.type.str.contains("Pause|Resume"), "id_repayments"
            ].astype(str)
            + "_events"
        )
        contract_positions["amount_paid"] = 0
    else:
        contract_positions = pd.DataFrame()

    # TODO events that were not merged
    # contract_events[contract_events.repayment_id.isnull()]

    # # TODO merge contract_positions and payments
    # # extract repayment_id from list dict column destinations for merge with
    # contract_positions
    # payments["repayment_id"] = payments["destinations"].apply(
    #     # lambda x: [d.get("repayment_id") for d in x]
    #     lambda x: [d.get("repayment_id") for d in x]
    # )
    # # example
    # payments.loc[payments.destination == "C550020"]
    # payments.loc[payments.destination == "C550020"]["repayment_id"][180]
    # payments.loc[payments.destination == "C550020"]["repayment_id"][180][0]
    # # merge
    # dest_test = pd.merge(
    #     contract_positions,
    #     payments,
    #     # left_on=["contract_reference", "date"],
    #     # right_on=["destination", "sent_datetime"],
    #     on="transaction_id",
    #     suffixes=("", "_payments"),
    #     how="left",
    # )
    # # check if all merged & create rest dataframe
    # # missing transactions that are not mapped based on contract and date
    # paym_rest = payments.loc[~payments.transaction_id.isin(dest_test.transaction_id)]
    # # TODO empty entries in payment --> how to map? --> delete?
    # # payments.transaction_id map to contract_repayments?
    # payments.loc[payments.destination.isnull()]
    # test = payments.loc[payments.destination.isnull()]["transaction_id"]
    # dest_test.loc[dest_test.transaction_id.isin(test)]

    # keep only entries in contract_positions that have a contract assigned that appears
    # in contract_details
    if (len(contract_positions) > 0) & (
        "contract_reference_repayments" in contract_positions.columns
    ):
        contract_positions = contract_positions.loc[
            contract_positions.contract_reference_repayments.isin(
                contract_details.reference
            )
        ]
    contract_details["currency"] = currency
    contract_details["country"] = country

    # merge credit value
    payment_export.Id = pd.to_numeric(payment_export.Id)

    # Hotfix
    if (len(contract_positions) > 0) & (len(contract_repayments) > 0):
        contract_positions = contract_positions.merge(
            payment_export[["Id", "Credit Value"]],
            left_on="id_repayments",
            right_on="Id",
            how="inner",
        )
        if "amount_paid" not in contract_positions.columns:
            contract_positions["amount_paid"] = 0

        contract_positions["currency"] = currency
        contract_positions["country"] = country

    positions = contract_positions.to_dict(orient="records")  # input for payments_ts
    positions = {"data": positions}

    contract_details = contract_details.merge(
        leads,
        left_on="reference",
        right_on="contract_reference",
        suffixes=(None, "_leads"),
    )
    contract_details = contract_details.merge(
        lead_generators,
        left_on="generator",
        right_on="id",
        suffixes=(None, "_lead_generators"),
    )
    contract = contract_details.to_dict(orient="records")  # input for shs
    contract = {"data": contract}

    return contract, positions


def extract_shs_data(in_dict: dict) -> list[dict]:
    """Extract SHS data from dictionary originating from datalake."""
    spec = (
        "data",
        [
            {
                "device_external_id": "linked_device_serial_number",
                "account_external_id": "reference",
                "serial_number": "serial_number",
                "purchase_date": optional("start_time"),
                "manufacturer": "manufacturer",
                "pv_power_w": optional("panel_size_in_w"),
                # TODO how convert ah in wh?
                # "battery_energy_wh": ("offers", optional(["battery_size_in_ah"])),
                "customer_category": optional("customer_category"),
                "payment_plan_days": optional("expected_loan_duration"),
                "payment_plan_down_payment": optional("downpayment"),
                "payment_plan_amount_financed": optional(
                    "payment_plan_amount_financed"
                ),
                "payment_plan_type": optional("type"),
                "payment_plan_currency": optional("currency"),
                "paid_off_date": optional("paid_off_date"),
                "repossession_date": optional("repossession_date"),
                "is_test": optional("is_test"),
                "customer_external_id": "client_id",
                "customer_name_p": optional("name_p"),
                "customer_name_e": optional("name_e"),
                "customer_gender": optional("gender"),
                "customer_birth_year": optional("customer_birth_year"),
                "customer_country": optional("country"),
                "customer_latitude_b": optional("lat_b"),
                "customer_longitude_b": optional("lon_b"),
                "customer_latitude_p": optional("lat_p"),
                "customer_longitude_p": optional("lon_p"),
                "customer_latitude_e": optional("lat_e"),
                "customer_longitude_e": optional("lon_e"),
                "customer_phone_p": optional("preferred_phone_number_p"),
                "customer_phone_e": optional("preferred_phone_number_e"),
                "offer_id": optional("offer_id"),
                "offer_name": optional("name"),
                "contract_status": optional("contract_status"),
                "registration_date": optional("registration_date"),
                "reference_pricing_duration": optional("reference_pricing_duration"),
                "reference_pricing_amount": optional("reference_pricing_amount"),
                "time_given_at_start_in_days": optional("time_given_at_start_in_days"),
                "time_to_ownership_in_days": optional("time_to_ownership_in_days"),
                "base_price_time_in_days": optional("base_price_time_in_days"),
                "tags": optional("tags"),
                # custom_id_leads becomes customer_id_number
                "customer_id_number": optional("custom_id_leads"),
                # id_lead_generators becomes lead_generator_id
                "lead_generator_id": optional("id_lead_generators"),
                # type_lead_generators becomes lead_generator_type
                "lead_generator_type": optional("type_lead_generators"),
                # device_type from OFFERS becomes model
                "model": optional("device_type"),
                # name_[p|e]_lead_generators becomes seller_name_[p|e]
                "seller_name_p": optional("name_p_lead_generators"),
                "seller_name_e": optional("name_e_lead_generators"),
                # preferred_phone_number_[p|e]_lead_generators becomes seller_phone_[p|e]  # noqa: E501
                "seller_phone_p": optional("preferred_phone_number_p_lead_generators"),
                "seller_phone_e": optional("preferred_phone_number_e_lead_generators"),
                # gender_lead_generators becomes seller_gender
                "seller_gender": optional("gender_lead_generators"),
            }
        ],
    )
    # data cleaning
    shs_df = df_nan_to_none(pd.DataFrame(glom(in_dict, spec)))
    out_dict = shs_df.to_dict("records")
    return out_dict


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake."""
    spec = (
        "data",
        [
            {
                "payment_external_id": "id_repayments",
                "purpose": (Val("paygo payment")),
                "paid_at": "date_repayments",
                "amount": "amount_paid",
                "currency": optional("currency"),
                "provider_name": (Val("PaygOps")),
                "account_external_id": "contract_reference_repayments",
                "account_origin": (Val("shs")),
                "discounted_amount": optional("discounted_amount"),
                "type": optional("type_repayments"),
                "credit_value": optional("Credit Value"),
            }
        ],
    )
    # data cleaning
    payments_ts_df = df_nan_to_none(pd.DataFrame(glom(in_dict, spec)))
    out_dict = payments_ts_df.to_dict("records")
    return out_dict


def transform(file_data: dict) -> RecordSet:
    """Main entry point returning a RecordSet from the data from the file."""
    logger.info("Transforming PaygOps API data.")
    processed_shs, processed_payments = payg_ops_pre_processor(file_data)
    return {
        "data_shs": extract_shs_data(processed_shs),
        "data_payments_ts": extract_payments_ts_data(processed_payments),
    }
