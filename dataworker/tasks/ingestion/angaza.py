"""Angaza data transformer.

------------------------- API data schema -------------------------

{
    "payments:":
    '{
        "_embedded": {
          "item": [
            {
              "_links": {
                "self": {
                  "href": "https://payg.angazadesign.com/data/payments/PA1234"
                },
                "za:account": {
                  "href": "https://payg.angazadesign.com/data/accounts/AC1234"
                },
                "za:activation": {
                  "href": "https://payg.angazadesign.com/data/activations/AV1234"
                }
              },
              "account_number": 1234,
              "account_qid": "AC1234",
              "amount": "720.00",
              "angaza_transaction_qid": "RC1234",
              "currency": "ZMW",
              "effective_date": "2024-02-12T15:34:30Z",
              "external_xref": "MP1234.1234.M12342",
              "is_reversed": true,
              "msisdn": "+123484901",
              "qid": "PA1234"
            }
          ]
        },
        "_links": {
          "item": [
            {
              "href": "https://payg.angazadesign.com/data/payments/PA1234"
            }
          ],
          "self": {
            "href": "https://payg.angazadesign.com/data/payments?account_qid=AC1234&only_reversals=true"
          }
        },
        "limit": 100,
        "max_total_count": 10000,
        "offset": 0,
        "total_count": 1
      }',

    "accounts:":
    '{
        "max_total_count": 10000,
        "total_count": 1,
        "limit": 100,
        "offset": 0,
        "_embedded": {
          "item": [
            {
              "qid": "AC000001",
              "billing_model": "PAYG",
              "currency": "USD",
              "is_postpaid": false,
              "is_testing": false,
              "minimum_payment": "0",
              "num_installments": 7,
              "number": 12345678,
              "payment_amount_per_period": "600.000",
              "payment_due_date": "2019-02-26T13:53:34Z",
              "payment_period_in_days": 7,
              "registration_date": "2018-12-03T23:52:04Z",
              "status": "DISABLED",
              "total_paid": "1000",
              "full_price": "5000",
              "down_payment_days_included": 7,
              "down_payment": "500",
              "repayment_risk": "15",
              "client_qids": [
                "CL000001"
              ],
              "cumulative_days_disabled": 2,
              "latest_payment_when": "2019-01-26T23:03:34Z",
              "_links": {
                "self": {
                  "href": "https://payg.angazadesign.com/data/accounts/AC000001"
                },
                "za:clients": {
                  "href": "https://payg.angazadesign.com/data/clients?account_qid=AC000001&with_testing=true"
                },
                "za:payments": {
                  "href": "https://payg.angazadesign.com/data/payments?account_qid=AC000001"
                },
                "za:pricing_group": {
                  "href": "https://payg.angazadesign.com/data/pricing_groups/GR000001"
                },
                "za:responsible_user": {
                  "href": "https://payg.angazadesign.com/data/users/US000001"
                },
                "za:responsible_users": [
                  {
                    "href": "https://payg.angazadesign.com/data/users/US000001"
                  },
                  {
                    "href": "https://payg.angazadesign.com/data/users/US000002"
                  }
                ],
                "za:activations": {
                  "href": "https://payg.angazadesign.com/data/activations?account_qid=AC000001"
                },
                "za:prospective_account": {
                  "href": "https://payg.angazadesign.com/data/prospective_accounts/PP000001"
                }
              }
            }
          ]
        },
        "_links": {
          "self": {
            "href": "https://payg.angazadesign.com/data/accounts"
          },
          "next": {
            "href": "https://payg.angazadesign.com/data/accounts?offset=100"
          },
          "prev": {
            "href": "https://payg.angazadesign.com/data/accounts"
          },
          "item": [
            {
              "href": "https://payg.angazadesign.com/data/accounts/AC000001"
            }
          ]
        }
      }',
    "clients:":
    '{
        "max_total_count": 10000,
        "total_count": 1,
        "limit": 100,
        "offset": 0,
        "_embedded": {
          "item": [
            {
              "qid": "CL000001",
              "name": "Client Name",
              "external_xref": null,
              "primary_phone": "+2545555555",
              "attribute_values": [
                {
                  "attribute_qid": "AB000009",
                  "name": "City",
                  "value": "Nairobi",
                  "type": "SELECT_FROM_LIST"
                },
                {
                  "attribute_qid": "AB000010",
                  "name": "Full Name",
                  "value": "Client Name",
                  "type": "CLIENT_NAME"
                },
                {
                  "attribute_qid": "AB000008",
                  "name": "Primary Phone",
                  "value": "+2545555555",
                  "type": "CLIENT_PHONE"
                },
                {
                  "attribute_qid": "AB008946",
                  "name": "NRC Number",
                  "type": "TEXT_LINE",
                  "value": "123456/77/8"
                },
                {
                  "attribute_qid": "AB017231",
                  "name": "Does The Client Have An Alternative Name (Nickname)",
                  "type": "TEXT_LINE",
                  "value": "Poopsie"
                },
                {
                  "attribute_qid": "AB008458",
                  "name": "Gender",
                  "type": "CLIENT_GENDER",
                  "value": "Female"
                },
                {
                  "attribute_qid": "AB008457",
                  "name": "Age (Years)",
                  "type": "CLIENT_AGE",
                  "value": 30
                },
                {
                  "attribute_qid": "AB000011",
                  "name": "Client Photo",
                  "value": "https://angaza-misc.s3.amazonaws.com/photos/image-01.png",
                  "type": "PHOTO"
                },
                {
                  "attribute_qid": "AB008459",
                  "name": "Clients Province",
                  "type": "SELECT_FROM_LIST",
                  "value": "Luapula Province"
                },
                {
                  "attribute_qid": "AB017239",
                  "name": "Name Of Clients District Where The System Will Be Installed",
                  "type": "TEXT_LINE",
                  "value": "Mwansabombwe district"
                },
                {
                  "attribute_qid": "AB017230",
                  "name": "Photo Of The Outside Of The House Where The Client Stays",
                  "type": "GENERIC_PHOTO",
                  "value": "https://angaza-misc.s3.amazonaws.com/photos/image.jpg"
                },
                {
                  "attribute_qid": "AB008947",
                  "name": "Clients Physical Road Address or Town/Village",
                  "type": "TEXT_LINE",
                  "value": "Sholo village"
                },
                {
                  "attribute_qid": "AB017232",
                  "name": "What Is The Nearest Landmark To The Clients House?",
                  "type": "TEXT_LINE",
                  "value": "Chipita primary school"
                },
                {
                  "attribute_qid": "AB015802",
                  "name": "Local Chief Full Name",
                  "type": "TEXT_LINE",
                  "value": "Chief Anne Frank"
                },
                {
                  "attribute_qid": "AB014352",
                  "name": "Clients Language Preference",
                  "type": "SELECT_FROM_LIST",
                  "value": "Bemba"
                },
                {
                  "attribute_qid": "AB009963",
                  "name": "Name of 1st Family Relative",
                  "type": "TEXT_LINE",
                  "value": "Miriam Möwe"
                },
                {
                  "attribute_qid": "AB013202",
                  "name": "Clients Relation with 1st Family Relative",
                  "type": "SELECT_FROM_LIST",
                  "value": "Other"
                },
                {
                  "attribute_qid": "AB010583",
                  "name": "Physical Address of 1st Family Relative",
                  "type": "TEXT_LINE",
                  "value": "Sholo village"
                },
                {
                  "attribute_qid": "AB009964",
                  "name": "Phone Number of 1st Family Relative",
                  "type": "PHONE_NUMBER",
                  "value": "+123456789"
                },
                {
                  "attribute_qid": "AB017233",
                  "name": "Name of 2nd Family Relative (NOT SPOUSE OR CHILD)",
                  "type": "TEXT_LINE",
                  "value": "Prinz Pi"
                },
                {
                  "attribute_qid": "AB017234",
                  "name": "Clients Relation with 2nd Family Relative",
                  "type": "SELECT_FROM_LIST",
                  "value": "Other"
                },
                {
                  "attribute_qid": "AB017235",
                  "name": "Physical Address of 2nd Family Relative",
                  "type": "TEXT_LINE",
                  "value": "Sholo village"
                },
                {
                  "attribute_qid": "AB017236",
                  "name": "Phone Number of 2nd Family Relative",
                  "type": "PHONE_NUMBER",
                  "value": "+123456789"
                },
                {
                  "attribute_qid": "AB017240",
                  "name": "How Reliable Is The Clients Network Coverage At Home",
                  "type": "SELECT_FROM_LIST",
                  "value": "2) Available At A Specific Area Nearby The House"
                },
                {
                  "attribute_qid": "AB008460",
                  "name": "Clients Occupation",
                  "type": "SELECT_FROM_LIST",
                  "value": "Farmer (Crops)"
                },
                {
                  "attribute_qid": "AB009958",
                  "name": "Clients Monthly Salary from Contracted Employment (EXCLUDING PERSONAL BUSINESS INCOME)",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO CONTRACTED EMPLOYMENT SALARY - INCOME FROM PERSONAL BUSINESS"
                },
                {
                  "attribute_qid": "AB009962",
                  "name": "What is the name of the Clients workplace?",
                  "type": "TEXT_LINE",
                  "value": "Farming"
                },
                {
                  "attribute_qid": "AB010041",
                  "name": "When does the Client usually receive their Contracted Employment Salary?",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO CONTRACTED EMPLOYMENT SALARY"
                },
                {
                  "attribute_qid": "AB010579",
                  "name": "Clients Monthly Income from Personal/Family business (EXCLUDING SALARY from Contracted Employment)",
                  "type": "SELECT_FROM_LIST",
                  "value": "K2501 -> K3000"
                },
                {
                  "attribute_qid": "AB010578",
                  "name": "Clients Monthly Household Rent",
                  "type": "SELECT_FROM_LIST",
                  "value": "8) CLIENT OWNS THEIR HOUSE"
                },
                {
                  "attribute_qid": "AB017248",
                  "name": "What Is The Roofing Material On The Clients Home?",
                  "type": "SELECT_FROM_LIST",
                  "value": "2)\tMetal"
                },
                {
                  "attribute_qid": "AB010035",
                  "name": "How many dependents does the Client have?",
                  "type": "SELECT_FROM_LIST",
                  "value": "5+"
                },
                {
                  "attribute_qid": "AB024378",
                  "name": "How many people live in the clients house and will use the product?",
                  "type": "SELECT_FROM_LIST",
                  "value": "5" # "1-9" + "10+"
                },
                {
                  "attribute_qid": "AB010595",
                  "name": "How much in TOTAL does the Client pay for School Fees per Year? (For all Children)",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO SCHOOL FEES"
                },
                {
                  "attribute_qid": "AB010581",
                  "name": "Has the Client used any other Solar Home System?",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO"
                },
                {
                  "attribute_qid": "AB010582",
                  "name": "Does the Client currently have electricity supply at their home?",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO"
                },
                {
                  "attribute_qid": "AB025055",
                  "name": "For what purpose will the client be using the Product?",
                  "type": "SELECT_FROM_LIST",
                  "value": "Household use only"
                },
                {
                  "attribute_qid": "AB009959",
                  "name": "Does the client own any Farming land used for crops?",
                  "type": "SELECT_FROM_LIST",
                  "value": "0-5 acres"
                },
                {
                  "attribute_qid": "AB010593",
                  "name": "What is the Clients main Harvested Crop?",
                  "type": "SELECT_FROM_LIST",
                  "value": "Maize"
                },
                {
                  "attribute_qid": "AB017245",
                  "name": "How Much Did The Client Spend On Seed And Fertilizer Last Season",
                  "type": "SELECT_FROM_LIST",
                  "value": "2) K1001 – K2000"
                },
                {
                  "attribute_qid": "AB009960",
                  "name": "Does the client own any livestock?",
                  "type": "SELECT_FROM_LIST",
                  "value": "31+ Chickens"
                },
                {
                  "attribute_qid": "AB010594",
                  "name": "What was the income from the Clients last harvest",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO HARVEST"
                },
                {
                  "attribute_qid": "AB024387",
                  "name": "What was the Income generated from the Clients livestock last year",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO LIVESTOCK"
                },
                {
                  "attribute_qid": "AB008826",
                  "name": "Front of Zambian NRC/Passport",
                  "type": "GENERIC_PHOTO",
                  "value": "https://angaza-misc.s3.amazonaws.com/photos/photo-01.jpg"
                },
                {
                  "attribute_qid": "AB008842",
                  "name": "Back of Zambian NRC/Passport",
                  "type": "GENERIC_PHOTO",
                  "value": "https://angaza-misc.s3.amazonaws.com/photos/photo-02.jpg"
                },
                {
                  "attribute_qid": "AB010589",
                  "name": "Does the Client own a Smartphone?",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO"
                },
                {
                  "attribute_qid": "AB010584",
                  "name": "Does the Client own any Motorized Vehicles?",
                  "type": "SELECT_FROM_LIST",
                  "value": "NO"
                },
                {
                  "attribute_qid": "AB017247",
                  "name": "How Far Is The Nearest Medical Facility To The Clients House",
                  "type": "SELECT_FROM_LIST",
                  "value": "2)\t31-60min"
                },
                {
                  "attribute_qid": "AB010592",
                  "name": "What product does the Client want RDG to offer next?",
                  "type": "SELECT_FROM_LIST",
                  "value": "Farming Inputs (Fertilizer, Seeds etc)"
                }
              ],
              "_links": {
                "self": {
                  "href": "https://payg.angazadesign.com/data/clients/CL000001"
                },
                "za:accounts": {
                  "href": "https://payg.angazadesign.com/data/accounts?client_qid=CL000001&with_testing=true"
                }
              }
            }
          ]
        },
        "_links": {
          "self": {
            "href": "https://payg.angazadesign.com/data/clients"
          },
          "next": {
            "href": "https://payg.angazadesign.com/data/clients?offset=100"
          },
          "prev": {
            "href": "https://payg.angazadesign.com/data/clients"
          },
          "item": [
            {
              "href": "https://payg.angazadesign.com/data/clients/CL000001"
            }
          ]
        }
      }'
}
"""  # noqa: D301, E501,RUF002

import contextlib

import pandas as pd
from glom import Val, glom

from dataworker import logger
from dataworker.processing import optional

TASK_NAME = "api/angaza"
ENTRYPOINT = "transform"


def _add_suffixes(in_df: pd.DataFrame) -> pd.DataFrame:
    """Helper function for renaming duplicate column names."""
    counts = {}
    new_columns = []
    for col in in_df.columns:
        if col not in counts:
            counts[col] = 0
            new_col = col
        else:
            counts[col] += 1
            new_col = f"{col}{counts[col]}"
        new_columns.append(new_col)
    in_df.columns = new_columns
    return in_df


def _delete_key(collection: dict | list, key: str) -> None:
    """Helper function for recursively removing occurences of key from dict/list."""
    if isinstance(collection, dict):
        with contextlib.suppress(KeyError):
            del collection[key]
        for value in collection.values():
            _delete_key(value, key)
    elif isinstance(collection, list):
        for item in collection:
            _delete_key(item, key)


def _enrich_client_record(client: dict, accounts_df: pd.DataFrame) -> pd.DataFrame:
    """Enrich each client object and join with respective data from accounts df."""
    # construct base df for the single client.
    client_df = pd.DataFrame(client["attribute_values"])
    # include only relevant and existant columns.
    client_df = client_df.filter(["name", "value_e", "value_p", "value"])
    # add customer_external_id row and set index to 'name'.
    new_row = pd.DataFrame({"name": ["customer_external_id"], "value": [client["qid"]]})
    client_df = pd.concat([client_df, new_row], ignore_index=True).set_index("name")
    # reshape, modify, clean up.
    client_df = (
        client_df.T.reset_index(drop=False)
        .melt(id_vars="index", var_name="name", value_name=0)
        .dropna()
        .assign(name=lambda x: x["name"].str.cat(x["index"].astype(str), sep="_"))
        .set_index("name")
        .drop(columns="index")
    )

    # combine client and accounts together.
    client_enriched_df = accounts_df.reset_index().merge(
        client_df.T,
        left_on="client_qids",
        right_on="customer_external_id_value",
        how="inner",
    )
    return _add_suffixes(client_enriched_df)


def extract_shs_data(clients: list[dict], accounts: list[dict]) -> list[dict]:
    """Extract shs data from datalake data.

    NOTE: Manufacturer set to RDG until implementation of additional API enpoint.
    """
    spec = (
        [
            {
                "device_external_id": ("number"),
                "account_external_id": ("qid"),  # qualifying_account_qid_value
                "serial_number": ("number"),
                "purchase_date": optional("registration_date"),
                "manufacturer": (Val("RDG")),
                "pv_power_w": optional("pv_power_w_value"),
                "battery_energy_wh": optional("battery_energy_wh_value"),
                "voltage_v": optional("voltage_v_value"),
                "rated_power_w": optional("rated_power_w_value"),
                "customer_category": optional("customer_category_value"),
                "customer_sub_category": optional("customer_sub_category_value"),
                "primary_use": optional("primary_use_value"),
                "payment_plan_days": optional("payment_period_in_days"),
                "payment_plan_down_payment": optional("down_payment"),
                "payment_plan_amount_financed": optional("full_price"),
                "payment_plan_type": optional("billing_model"),
                "payment_plan_currency": optional("currency"),
                "paid_off_date": optional("paid_off_date_value"),
                "repossession_date": optional("repossession_date_value"),
                "interval_seconds": optional("interval_seconds_value"),
                "is_test": optional("is_testing"),
                "customer_external_id": ("customer_external_id_value"),
                "customer_name_p": optional("customer_name_value_p"),
                "customer_name_e": optional("customer_name_value_e"),
                "customer_gender": optional("customer_gender_value"),
                "customer_birth_year": optional("customer_birth_year_value"),
                "customer_former_electricity_source": optional(
                    "customer_former_electricity_source_value"
                ),
                "customer_household_size": optional((
                    "customer_household_size_value",
                    lambda x: int(x)
                    if isinstance(x, int) or (isinstance(x, str) and x.isdigit())
                    else None,
                )),
                "customer_household_size_original": optional(
                    "customer_household_size_value"
                ),
                "customer_profession": optional("customer_profession_value"),
                "customer_address_p": optional("customer_address_value_p"),
                "customer_address_e": optional("customer_address_value_e"),
                "customer_country": optional("customer_country_value"),
                "customer_latitude_b": optional("customer_latitude_value_b"),
                "customer_latitude_p": optional("customer_latitude_value_p"),
                "customer_latitude_e": optional("customer_latitude_value_e"),
                "customer_longitude_b": optional("customer_longitude_value_b"),
                "customer_longitude_p": optional("customer_longitude_value_p"),
                "customer_longitude_e": optional("customer_longitude_value_e"),
                "customer_email_p": optional("customer_email_value_p"),
                "customer_email_e": optional("customer_email_value_e"),
                "customer_phone_p": optional("customer_phone_value_p"),
                "customer_phone_e": optional("customer_phone_value_e"),
                "customer_location_area_1": optional("customer_location_area_1_value"),
                "customer_location_area_2": optional("customer_location_area_2_value"),
                "customer_location_area_3": optional("customer_location_area_3_value"),
                "customer_location_area_4": optional("customer_location_area_4_value"),
                "seller_external_id": optional("seller_external_id_value"),
                "seller_name_p": optional("seller_name_value_p"),
                "seller_name_e": optional("seller_name_value_e"),
                "seller_gender": optional("seller_gender_value"),
                "seller_address_p": optional("seller_address_value_p"),
                "seller_address_e": optional("seller_address_value_e"),
                "seller_country": optional("seller_country_value"),
                "seller_latitude_b": optional("seller_latitude_value_b"),
                "seller_latitude_p": optional("seller_latitude_value_p"),
                "seller_latitude_e": optional("seller_latitude_value_e"),
                "seller_longitude_b": optional("seller_longitude_value_b"),
                "seller_longitude_p": optional("seller_longitude_value_p"),
                "seller_longitude_e": optional("seller_longitude_value_e"),
                "seller_email_p": optional("seller_email_value_p"),
                "seller_email_e": optional("seller_email_value_e"),
                "seller_phone_p": optional("seller_phone_value_p"),
                "seller_phone_e": optional("seller_phone_value_e"),
                "seller_location_area_1": optional("seller_location_area_1_value"),
                "seller_location_area_2": optional("seller_location_area_2_value"),
                "seller_location_area_3": optional("seller_location_area_3_value"),
                "seller_location_area_4": optional("seller_location_area_4_value"),
                "time_given_at_start_in_days": optional("down_payment_days_included"),
                "is_pay_as_you_go": optional("is_postpaid"),
                "contract_status": optional("status"),
                "reference_pricing_amount": optional("payment_amount_per_period"),
                "minimum_payment": optional("minimum_payment"),
                "num_installments": optional((
                    "accounts._embedded.item",
                    ["num_installments"],
                )),
                "repayment_risk": optional("repayment_risk"),
                "total_paid": optional("total_paid"),
                "cumulative_days_disabled": optional("cumulative_days_disabled"),
                "latest_payment_when": optional("latest_payment_when"),
                "payment_due_date": optional("payment_due_date"),
            }
        ],
    )

    # construct accounts df.
    # convert client_qids to single string.
    accounts_df = pd.DataFrame(accounts).assign(
        client_qids=lambda x: x["client_qids"].apply(", ".join)
    )

    # for each client in clients, enrich and combine with relevant account data.
    shs_df = pd.concat([_enrich_client_record(c, accounts_df) for c in clients])

    shs = shs_df.to_dict(orient="records")
    return glom(shs, spec)


def extract_payments_ts_data(payments: list[dict]) -> list[dict]:
    """Extract payments_ts data from datalake data."""
    spec = (
        [
            {
                "payment_external_id": ("angaza_transaction_qid"),
                "paid_at": ("effective_date"),
                "amount": ("amount"),
                "currency": ("currency"),
                "account_external_id": ("account_qid"),
                "account_origin": (Val("shs")),
                "purpose": (Val("paygo payment")),
                "provider_name": (Val("Angaza")),
            }
        ],
    )
    return glom(payments, spec)


def transform(file_data: dict) -> dict:
    """Process datalake data dict and transform to DWH format."""
    logger.info("Transforming Angaza API data.")

    _delete_key(file_data, "_links")

    payments = file_data["payments"]["_embedded"]["item"]
    accounts = file_data["accounts"]["_embedded"]["item"]
    clients = file_data["clients"]["_embedded"]["item"]

    # Process payments and SHS data
    payments_ts = extract_payments_ts_data(payments) if payments else []
    shs = extract_shs_data(clients, accounts) if clients and accounts else []

    # Log missing datasets
    if not payments:
        logger.info(
            "Incoming dict item 'payments' contains no data. "
            "Could not extract for data_payments_ts."
        )
    if not accounts:
        logger.info(
            "Incoming dict item 'accounts' contains no data. "
            "Could not extract for data_shs."
        )
    if not clients:
        logger.info(
            "Incoming dict item 'clients' contains no data. "
            "Could not extract for data_shs."
        )

    return {"data_shs": shs, "data_payments_ts": payments_ts}
