"""Created on Wed Oct 11 10:55:11 2023.

API info: https://developer.upya.io/.
See also https://gitlab.com/prospect-energy/prospect-server/-/wikis/Development/UPYA.
"""

import pandas as pd
from glom import glom
from glom.core import UnregisteredTarget

from dataworker import logger
from dataworker.location import reverse_geo_lookup
from dataworker.processing import optional

TASK_NAME = "api/upya"
ENTRYPOINT = "transform"


def upya_shs_dataframe(raw_data: dict) -> pd.DataFrame:
    """Return a DataFrame containing shs data.

    May raise an UnregisteredTarget from glom if spec is outdated.
    """
    # =============================================================================
    # Contract (main shs)
    # =============================================================================
    spec = (
        "contracts",
        {
            "time_given_at_start_in_days": [optional("pricingSchedule.upfrontDays")],
            "purchase_date": [optional("signingDate")],
            "account_external_id": ["contractNumber"],
            "seller_external_id": [optional("respAgent")],
            "payment_plan_type": [optional("type")],
            "payment_plan_currency": [optional("ccy")],
            "customer_external_id": ["client.clientNumber"],
            "customer_birth_year": [optional("client.profile.birthday_b")],
            "customer_country": [optional("country")],
            "customer_gender": [optional("client.profile.gender")],
            "customer_phone_p": [optional("client.contact.fullNumber_p")],
            # TODO: these two columns need e, p, b treatment....
            "customer_latitude_e": [optional("client.profile.gps.latitude")],
            "customer_longitude_e": [optional("client.profile.gps.longitude")],
            # "customer_latitude_e": [optional("customer_latitude_e")],
            # "customer_longitude_e": [optional("customer_longitude_e")],
            # "customer_latitude_p": [optional("customer_latitude_p")],
            # "customer_longitude_p": [optional("customer_longitude_p")],
            # "customer_latitude_b": [optional("customer_latitude_b")],
            # "customer_longitude_b": [optional("customer_longitude_b")],
            "payment_plan_down_payment": [optional("pricingSchedule.upfrontPayment")],
            # join to asset table using contract number, then serial number
            "device_external_id": ["contractNumber"],
            # in DF: customer_name_p = first_name_p + last_name_p
            "customer_first_name": [optional("client.profile.firstName_p")],
            "customer_last_name": [optional("client.profile.lastName_p")],
            "total_cost": [optional("totalCost")],
            # custom fields
            "contract_status": [optional("status")],
            "contract_last_update": [optional("lastStatusUpdate")],
            "total_days_active": [optional("totalDaysActivated")],
            "deal_number": [optional("deal.dealNumber")],
            "remaining_debt": [optional("remainingDebt")],
            "total_paid": [optional("totalPaid")],
            "product_name": [optional("product.name")],
            # remaining payment schedule vars
            "first_days": [optional("pricingSchedule.upfrontDays")],
            "recurr_payment": [optional("pricingSchedule.recurrentPayment")],
            "recurr_freq_days": [optional("pricingSchedule.freq")],
            "min_payment": [optional("pricingSchedule.minPayment")],
            "paid_off_date": [optional("paidOffDate")],
        },
    )

    # Assets and Products
    # - this may not work based on the timing of ASSET vs CONTRACT updates
    #   but I want to at least sketch it out then backfill with contract id
    # - and then serial_number_actual should still make it into custom field
    #   (and be blank whenever serial number is missing)

    spec_assets = (
        "assets",
        {
            "serial_number_actual": [optional("serialNumber")],
            "product_name": [optional("product.name")],
            "asset_status": [optional("status")],
            "account_external_id": [optional("contract.contractNumber")],
        },
    )

    spec_products = (
        "products",
        {
            "product_name": [optional("name")],
            "manufacturer": [optional("manufacturer")],
        },
    )

    try:
        out_data = glom(raw_data, spec)
        out_assets = glom(raw_data, spec_assets)
        out_products = glom(raw_data, spec_products)

    except UnregisteredTarget:
        logger.exception("import of shs records failed; or incomplete records in data")
        raise

    else:
        output_df = pd.DataFrame(out_data)
        output_df["contract_last_update"] = pd.to_datetime(
            output_df.contract_last_update
        )

        output_df["paid_off_date"] = pd.to_datetime(output_df.paid_off_date)
        output_df["paid_off_date"] = output_df.paid_off_date.replace({pd.NaT: None})

        # calculated fields
        output_df["customer_name_p"] = (
            output_df.customer_first_name + output_df.customer_last_name
        )

        # create fields for paygo data
        output_df["payment_plan_amount_financed"] = (
            output_df.total_cost - output_df.payment_plan_down_payment
        )

        assets_df = pd.DataFrame(out_assets)
        products_df = pd.DataFrame(out_products)

        assets_df = assets_df.merge(products_df, how="left", on="product_name")

        output_df = output_df.merge(
            assets_df[["account_external_id", "serial_number_actual", "manufacturer"]],
            how="left",
            on="account_external_id",
        )

        output_df["serial_number"] = output_df.serial_number_actual.combine_first(
            output_df.account_external_id
        )
    return output_df


def upya_payment_dataframe(raw_data: dict) -> pd.DataFrame:
    """Return a DataFrame containing payment data.

    May raise an UnregisteredTarget from glom if spec is outdated.
    """
    spec_pmt = (
        "payments",
        {
            "payment_external_id": [optional("paymentReference")],
            "paid_at": [optional("date")],
            "amount": [optional("amount")],
            "currency": [optional("ccy")],
            "provider_name": [optional("operator")],
            "provider_transaction_id": [optional("transactionId")],
            "account_external_id": [optional("contractNumber")],
            "days_active": [optional("days")],
        },
    )

    try:
        out_pmt_data = glom(raw_data, spec_pmt)

    except UnregisteredTarget:
        logger.exception(
            "import of payment records failed; or missing payment records in data"
        )
        raise

    else:
        out_pmt_df = pd.DataFrame(out_pmt_data)
        out_pmt_df["paid_at"] = pd.to_datetime(out_pmt_df.paid_at)

        # upya is shs
        out_pmt_df["account_origin"] = "shs"

        out_pmt_df = out_pmt_df[pd.notna(out_pmt_df.account_external_id)].copy()

    return out_pmt_df


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Upya data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_shs': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_payments_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Upya API data.")

    shs_dict = upya_shs_dataframe(file_data).to_dict(orient="records")
    pmt_dict = upya_payment_dataframe(file_data).to_dict(orient="records")

    # NOTE: this is the way to do this now...change to _b once implemented.
    # complete records with location names
    shs_dict = [
        i
        | reverse_geo_lookup(
            i["customer_latitude_e"],
            i["customer_longitude_e"],
            geo_col_prefix="customer_",
        )
        for i in shs_dict
    ]

    return {
        "data_shs": shs_dict,
        "data_payments_ts": pmt_dict,
    }
