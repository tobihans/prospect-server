"""Sunking data transformer.

------------------------- S3 data schema -------------------------
[
    {
        "Battery ID":"142806611",
        "Data Interval":30,
        "Version":"B01F0504",
        "Pv Voltage":65.1,
        "Pv Current":18.4,
        "Ac In Voltage":0,
        "Ac Load Watt":840,
        "Fuel Ratio":49.0,
        "Energy Charged":134,
        "Energy Discharged":20,
        "Timestamp Time":"2024-06-09 11:54:51"
        },
    ...
]
"""

import pandas as pd
from glom import Val, glom

from dataworker import logger

ENTRYPOINT = "transform"


def apply_transformations(in_df: pd.DataFrame) -> pd.DataFrame:
    """Apply Pandas transformations to extract metric values."""
    out_df = in_df.copy()

    # Apply some calculations ( Energy = Power / Time )
    out_df["interval_seconds"] = out_df.interval_minutes * 60
    out_df["pv_input_w"] = out_df.pv_voltage_v * out_df.pv_current_a
    out_df["pv_energy_interval_wh"] = out_df.pv_input_w * out_df.interval_minutes / 60
    out_df["output_energy_interval_wh"] = (
        out_df.system_output_w * out_df.interval_minutes / 60
    )
    out_df["battery_io_w"] = (
        out_df.energy_charged_interval_wh - out_df.energy_discharged_interval_wh
    ) / (out_df.interval_minutes / 60)

    # Drop interval_minutes columns since we use interval_seconds in the model
    out_df = out_df.drop(columns=["interval_minutes"], axis=1)

    return out_df


def extract_shs_ts_data(in_data: list[dict]) -> list[dict]:
    """Extract/transform shs data from list of dictionaries originating from datalake.

    Method:
        Extract ids and individual metric dictionary items with glom.
        Convert to Pandas df and apply custom function extract_stats().
        Convert back to dictionary and return.
    """
    spec = (
        [
            {
                "serial_number": "Battery ID",
                "manufacturer": Val("sunking"),
                "interval_minutes": "Data Interval",
                "firmware_version": "Version",
                "pv_voltage_v": "Pv Voltage",
                "pv_current_a": "Pv Current",
                "grid_input_v": "Ac In Voltage",
                "system_output_w": "Ac Load Watt",
                "battery_charge_percent": "Fuel Ratio",
                "energy_charged_interval_wh": "Energy Charged",
                "energy_discharged_interval_wh": "Energy Discharged",
                "metered_at": "Timestamp Time",
            }
        ],
    )

    glommed = glom(in_data, spec)

    # Convert to data frame
    shs_ts_df = pd.DataFrame(glommed)

    # Convert numeric columns to float
    cols_to_convert = [
        "interval_minutes",
        "pv_voltage_v",
        "pv_current_a",
        "grid_input_v",
        "system_output_w",
        "battery_charge_percent",
        "energy_charged_interval_wh",
        "energy_discharged_interval_wh",
    ]
    shs_ts_df[cols_to_convert] = shs_ts_df[cols_to_convert].apply(
        pd.to_numeric, errors="coerce"
    )

    # Apply calculations
    shs_ts_df = apply_transformations(shs_ts_df)

    out_dict = shs_ts_df.to_dict(orient="records")

    return out_dict


def extract_shs_data(in_data: list[dict]) -> list[dict]:
    """Extract/transform shs data from list of dicts originating from datalake."""
    battery_ids = {val["Battery ID"] for val in in_data}
    shs_dicts = [
        {
            "account_external_id": battery_id,
            "customer_external_id": battery_id,
            "device_external_id": battery_id,
            "serial_number": battery_id,
            "manufacturer": "sunking",
        }
        for battery_id in battery_ids
    ]
    return shs_dicts


def transform(file_data: list[dict]) -> dict:
    """Transform list of dictionaries containing Sunking data read from datalake.

    The output is a dictionary of the following schema:
        {
            'data_shs': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_shs_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Sunking data.")
    return {
        "data_shs": extract_shs_data(file_data),
        "data_shs_ts": extract_shs_ts_data(file_data),
    }
