"""Stellar V2 data transformer.

Active as of 2024.11.13.

------------------------- API data schema -------------------------
    {
        "status",

        "meta": {
            "time",
            "configUpdatedAt",
            "count",
            "instance"
        },

        "data": [
            {
                "uid",
                "contactName_p",
                "phoneNumber1_p",
                "phoneNumber2_p",
                "meterId",
                "connectionDate",
                "application",
                "lat_p",
                "lon_p",
                "lat_b",
                "lon_b",
                "siteType",
                "market",
                "address_p",
                "operatorId",
                "country",
                "timezone",
                "powerSystemView": {
                    "regionId"
                },
                "rawData_response": [
                    {
                        "metric": {
                            "__name__",
                            "deviceId"
                        },
                        "values": [
                            ...
                        ],
                        "timestamps": [
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]
    }
"""

from datetime import UTC, datetime, timedelta

import pandas as pd
from glom import Val, glom
from sqlalchemy import desc, select

from dataworker import logger
from dataworker.database import get_connection, get_table
from dataworker.processing import df_nan_to_none, optional

TASK_NAME = "api/stellar_v2"
ENTRYPOINT = "transform"


def estimate_reporting_interval(time_series: pd.DataFrame, n: int) -> int:
    """Get minimum difference (seconds) between data points.

    time_series: a Pandas Series of timestamps.
    n: calculation window (look over n most recent datapoints in Series).
    """
    ts = pd.to_datetime(time_series)

    min_interval_s = ts.sort_values().tail(n).diff().min().seconds

    return min_interval_s


def look_back_in_db(
    serial_number: str, metric: str, first_timestamp: datetime
) -> float:
    """Accesses DB and finds last non-null value of timeseries."""
    logger.info("looking back in DB for missing {} value.", metric)
    # Access DB and meters ts table.
    connection = get_connection()
    meters_ts = get_table("data_meters_ts")

    value = connection.scalar(
        select(meters_ts.c[metric])
        .where(
            meters_ts.c.metered_at.between(
                first_timestamp - timedelta(days=30), first_timestamp
            )
        )
        .where(meters_ts.c.data_origin == "stellar")
        .where(meters_ts.c.serial_number == serial_number)
        .order_by(desc(meters_ts.c.metered_at))
        .limit(1)
    )
    logger.info("found {}", value)
    return value


def verify_starting_values(meter_df: pd.DataFrame, metric: str) -> pd.DataFrame:
    """Verify start value from older timestamps to prevent later failures.

    Given a particular metric (column) and timeseries subsetted by one particular
    meter, checks the value with oldest timestamp. If nan, retrieves the previous
    value from the meters_ts table in the database and rewrites the nan with this
    value. This prevents a later forward fill from failing.
    """
    first_value = meter_df[metric].to_numpy()[0]
    # If we have a problem (first value of incoming timeseries is nan)...
    if pd.isna(first_value):
        serial_number = str(meter_df.serial_number.to_numpy()[0])
        first_timestamp = pd.to_datetime(meter_df["metered_at"].to_numpy()[0]).replace(
            tzinfo=None
        )
        # Replace nan with value found in DB (triggers a DB connection).
        replacement_value = look_back_in_db(serial_number, metric, first_timestamp)
        if replacement_value:
            meter_df[metric].to_numpy()[0] = replacement_value
            logger.info("successfully replaced NaN")
            return meter_df

        logger.info("returning original NaN without replacement")
    return meter_df


def ffill_nans(in_df: pd.DataFrame) -> pd.DataFrame:
    """Required fix for data from NSR, filling nan.

    ('nan' assigned until value changes).
    Forward-fill on all 'nan' values using their preceding non-'nan' values.

    Values retrieved from DB when necessary.
    May trigger a DB connection.
    """
    metrics = ["energy_lifetime_wh", "power_factor", "power_w"]
    for metric in metrics:
        # check for empty first values (may trigger a DB connection).
        idx = in_df["idx"].copy()
        in_df = in_df.groupby(idx, group_keys=False).apply(
            lambda x, metric=metric: verify_starting_values(x, metric)
        )

        # fill nans.
        in_df[metric] = in_df.groupby("idx")[metric].transform(lambda x: x.ffill())
    return in_df


def transpose_data(in_list: list[dict]) -> list[dict]:
    """Transpose data from new Stellar API format (2024.11.13) to older format."""
    aggregated_data = {}  # store results here.

    for meter in in_list:
        meter_id = meter["meterId"]

        ts_data = meter.get("rawData_response", [])
        for metric in ts_data:
            device_id = metric["metric"]["deviceId"]
            metric_name = metric["metric"]["__name__"]
            timestamps = metric["timestamps"]
            values = metric["values"]

            # For every timeseries (time-value) pair for this metric...
            for timestamp_ms, value in zip(timestamps, values, strict=True):
                # Construt datetime object from epoch integer.
                time = format_timestamp(timestamp_ms)

                # Initialize unique entry if it does not exist.
                key = (device_id, time)
                if key not in aggregated_data:
                    aggregated_data[key] = {
                        "deviceId": device_id,
                        "time": time,
                        "meterId": meter_id,
                    }

                # Add metric value to the entry.
                aggregated_data[key][metric_name] = value

    # Convert aggregated data to target format.
    return list(aggregated_data.values())


def format_timestamp(timestamp_ms: int) -> datetime:
    """Translate epoch (ms) to datetime format (UTC, with 'T' and 'Z' characters)."""
    return datetime.fromtimestamp(timestamp_ms / 1000, tz=UTC).strftime(
        "%Y-%m-%dT%H:%M:00.000Z"
    )


def extract_meters_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake.

    Method:
      Accesses item "data" from api_data (JSON).
      Each item in "data" represents one meter entry.

      For each item in "data",
      accesses item "rawData_response.data" (list of dictionaries).
      Each item in "rawData_response.data" represents one unique time series datapoint.

      From each item in "rawData_response.data", transforms to specified GLOM schema.
    """
    spec = [
        {
            "idx": "deviceId",  # Used for sorting. Will be dropped.
            "metered_at": "time",
            "manufacturer": Val("HOP"),
            "serial_number": "meterId",
            "v1": optional("meteredVoltageA"),  # Will be dropped.
            "v2": optional("meteredVoltageB"),  # Will be dropped.
            "v3": optional("meteredVoltageC"),  # Will be dropped.
            "power_factor": optional("powerFactor"),
            "power_w": optional("meteredPower"),
            "energy_lifetime_wh": optional((
                "lifetimeEnergy",
                lambda t: t * 1000 if t else None,
            )),
            "frequency_hz": optional("frequency"),
            "c1": optional("currentA"),  # Will be dropped.
            "c2": optional("currentB"),  # Will be dropped.
            "c3": optional("currentC"),  # Will be dropped.
            "signal_strength": optional("signalStrength"),
            "voltage_v_phase_1": optional("meteredVoltageA"),
            "voltage_v_phase_2": optional("meteredVoltageB"),
            "voltage_v_phase_3": optional("meteredVoltageC"),
            "power_factor_phase_1": optional("powerFactorA"),
            "power_factor_phase_2": optional("powerFactorB"),
            "power_factor_phase_3": optional("powerFactorC"),
            "power_w_phase_1": optional("meteredPowerA"),
            "power_w_phase_2": optional("meteredPowerB"),
            "power_w_phase_3": optional("meteredPowerC"),
            "current_a_phase_1": optional("currentA"),
            "current_a_phase_2": optional("currentB"),
            "current_a_phase_3": optional("currentC"),
        },
    ]
    # Needed to accomodate new Stellar API.
    in_data = transpose_data(in_dict["data"])

    glommed = glom(in_data, spec, default=None)
    if not glommed:
        return []

    # Data transformations.
    meters_ts_df = pd.DataFrame(glommed).sort_values(["idx", "metered_at"])

    # Construct ["interval_seconds"].
    idx = meters_ts_df["idx"].copy()
    meters_ts_df = meters_ts_df.groupby(idx, group_keys=False).apply(
        lambda x: x.assign(
            interval_seconds=lambda x: estimate_reporting_interval(x["metered_at"], 100)
        )
    )

    # Fill in nans (may involve DB connection...)
    meters_ts_df = ffill_nans(meters_ts_df)
    meters_ts_df = meters_ts_df.drop("idx", axis=1)

    meters_ts_df = df_nan_to_none(meters_ts_df)

    # Construct voltage and current.
    # 21.02.24 check for value 0 in voltages and replace with nan to exclude from means.
    meters_ts_df["voltage_v"] = (
        meters_ts_df.loc[:, ["v1", "v2", "v3"]].replace(0, None).mean(axis=1)
    )
    meters_ts_df["current_a"] = meters_ts_df.loc[:, ["c1", "c2", "c3"]].sum(axis=1)
    meters_ts_df = meters_ts_df.drop(["v1", "v2", "v3", "c1", "c2", "c3"], axis=1)

    meters_ts_df = df_nan_to_none(meters_ts_df)
    return meters_ts_df.to_dict("records")


def extract_meters_data(in_dict: dict) -> list[dict]:
    """Extract/transform meter data from dictionary originating from datalake.

    Method:
      Accesses item "data" (list of dictionaries) from api_data (JSON).
      Each item in "data" represents one meter entry.

      From each item in "data", transforms to specified GLOM schema.
    """
    spec = (
        "data",
        [
            {
                # "account_external_id": "uid",
                "customer_external_id": "uid",
                "customer_name_p": optional("contactName_p"),
                "customer_name_e": optional("contactName_e"),
                "customer_address_p": optional("address_p"),
                "customer_address_e": optional("address_e"),
                "customer_country": optional("country"),
                "customer_latitude_b": optional("lat_b"),
                "customer_longitude_b": optional("lon_b"),
                "customer_latitude_p": optional("lat_p"),
                "customer_longitude_p": optional("lon_p"),
                "customer_latitude_e": optional("lat_e"),
                "customer_longitude_e": optional("lon_e"),
                "customer_phone_p": optional("phoneNumber1_p"),
                "customer_phone_e": optional("phoneNumber1_e"),
                "device_external_id": optional("meterId"),
                "serial_number": optional("meterId"),
                "installation_date": optional("connectionDate"),
                "manufacturer": Val("HOP"),
                # "application": optional("application"),
                # "customer_phone_2_p": optional("phoneNumber2_p"),
                # "customer_phone_2_e": optional("phoneNumber2_e"),
                # "market": optional("market"),
                # "siteType": optional("siteType"),
                # "stellar_region": optional("powerSystemView.regionId"),
            }
        ],
    )

    glommed = glom(in_dict, spec, default=None)
    if not glommed:
        return []

    # convert to dataframe.
    meters_df = pd.DataFrame(glommed)

    return meters_df.to_dict("records")


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Stellar data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_meters': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_meters_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("Transforming Stellar API V2 data.")
    return {
        "data_meters": extract_meters_data(file_data),
        "data_meters_ts": extract_meters_ts_data(file_data),
    }
