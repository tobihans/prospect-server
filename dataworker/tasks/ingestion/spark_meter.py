"""Sparkmeter data transformer.

Sparkmeter is a company that produces connected electricity meters or "smart
meters". An organization that operates an electrical utility such as a grid or
minigrid can purchase Sparkmeter devices, install them at their customers' homes
or places of business, and they start sending data to the Sparkmeter servers.
The devices are linked to the organization's user account with the Sparkmeter
service where they can monitor consumption as well as conduct customer managment
and billing.

The user here is most likely an operator of a grid or minigrid (e.g. a REA),
public or commercial. This institution has customers, who could be a mix of
residential and commercial, that have one or more meters installed.

The data in one file in our datalake is the result of the Prospect data
connector calling the Sparkmeter API with a particular set of credentials
provided by a user. The file contains a snapshot view of all the meters as a
three-level nested JSON object.
 - First level is a single API response object,
 - second level are customers,
 - and third level are meters of each customer.

The word "account" can represent in this context two different things. The
organizational Prospect user has an account with the Sparkmeter service, and
information from this API call returns data from all the meters associated with
that account. On the other hand, in the context of the Prospect database schema,
an account represents a customer of the organization, i.e. - the individual that
has the meters installed in it's premises. Thus, the term "account_uid"
represents the table column that identifies this customer's account with the
organization, which tracks electrical use from one or more meter providing
electricity to that customer.

------------------------- API data schema -------------------------

SparkAPIData :=
{
    "customers" = Array[Customer]
    "error"
    "status"
}

Customer :=
{
    "code",
    "credit_balance",
    "debt_balance",
    "ground",
    "id",
    "meters", = Array[Meter]
    "name_p",
    "phone_number_p",
    "phone_number_verified"
}

Meter :=
{
    "active",
    "address_p",
    "bootloader",
    "city",
    "coords",
    "country",
    "current_daily_energy",
    "current_tariff_name",
    "firmware",
    "is_running_plan",
    "last_config_datetime",
    "last_cycle_start",
    "last_energy",
    "last_energy_datetime",
    "last_meter_state_code",
    "last_plan_expiration_date",
    "last_plan_payment_date",
    "lat_blurred",
    "lat_p",
    "lon_blurred",
    "lon_p",
    "model",
    "operating_mode",
    "plan_balance",
    "postalcode",
    "serial",
    "state",
    "street1_p",
    "street2_p",
    "tags",
    "total_cycle_energy"
}
"""

import pandas as pd
from glom import SKIP, S, Spec, Val, flatten, glom

from dataworker import logger
from dataworker.processing import optional, optional_float

TASK_NAME = "api/spark_meter"
ENTRYPOINT = "transform"


def extract_payments_ts_data(in_dict: dict) -> list[dict]:
    """Extract data from the Sparkmeter API data for the payment time series.

    - `paid_at` is mandatory but sometimes missing -> object is skipped
    - `currency` not returned by the API. NULL for now
    """
    # the following is the definition of how to extract the data
    payment_ts_spec = (
        # access the "customer" section of the data. An array of customer objects
        "customers",
        [
            (
                lambda t: t if t.get("id") else SKIP,
                S(
                    account_external_id=optional("id"),
                ),
                "meters",
                # I've tried to put the lambda test and the transformation inside a
                # pipeline, but it does not work, thus a double loop
                [
                    # skip all entries missing data
                    lambda t: t if t.get("last_plan_payment_date") else SKIP,
                ],
                [
                    {
                        "amount": "plan_balance",
                        "paid_at": "last_plan_payment_date",
                        "payment_external_id": S.account_external_id,  # no other id
                        "account_external_id": S.account_external_id,
                        "currency": Val("NULL"),  # not returned by the API
                        "last_plan_expiration_date": "last_plan_expiration_date",
                        # here more probably grids, but useless with device_installation
                        "account_origin": Val("meters"),
                    },  # from each "meter" object, create an object with this schema
                ],
            ),
        ],  # "meter" section from each "customer" object. An array of "meter" objects
        flatten,  # change the array of arrays of meter "objects" into a single array
    )
    return glom(in_dict, payment_ts_spec)


def extract_meters_ts_data(in_dict: dict) -> list[dict]:
    """Extract/transform timeseries data from dictionary originating from datalake."""
    spec = (
        # access the "customer" section of the data. An array of customer objects
        "customers",
        [
            "meters"
        ],  # "meter" section from each "customer" object. An array of "meter" objects
        flatten,  # change the array of arrays of meter "objects" into a single array
        [
            {
                "serial_number": "serial",
                "manufacturer": Val("sparkmeter"),
                "billing_cycle_start_at": optional("last_cycle_start"),
                "metered_at": "last_energy_datetime",
                "energy_lifetime_wh": optional("last_energy"),
                "last_meter_state_code": optional("last_meter_state_code"),
                "operating_mode": optional("operating_mode"),
                "last_config_datetime": optional("last_config_datetime"),
                "current_daily_energy": optional("current_daily_energy"),
                "total_cycle_energy": optional("total_cycle_energy"),
            }  # from each "meter" objects, create an object with the above schema
        ],
    )
    return glom(in_dict, spec)


def extract_meters_data(in_dict: dict) -> list[dict]:
    """Extract/transform meter data from dictionary originating from datalake."""
    spec = (
        # access the "customer" section of the data. An array of customer objects
        "customers",
        [lambda t: t if t["id"] else SKIP],  # skip all entries missing data
        [  # for each customer object, apply the following nested pipeline
            (
                # This first step stores values from the customer in the Scope
                S(
                    customer_name_p=optional("name_p"),
                    customer_name_e=optional("name_e"),
                    customer_phone_p=optional("phone_number_p"),
                    customer_phone_e=optional("phone_number_e"),
                    account_external_id=optional("id"),
                    # Spec indicates the string is a key of the "customer" object,
                    # not a litteral string
                    # TODO: as this is an identifier, phone_number_p should be unique
                    # This is not the case and should be improved.
                    # This leads to overwrites, especially when the value is null...
                    # This should be simply "id"
                    customer_external_id=Spec("phone_number_p"),
                ),
                "meters",  # of each "customer" object access the meters array
                # for each "meter" objects, extract some fields
                # and add some from the customer object via the Scope.
                [
                    {
                        "model": optional("model"),
                        "manufacturer": Val("sparkmeter"),  # temporary hardcoded
                        "account_external_id": S.account_external_id,
                        "customer_address_p": optional("address_p"),
                        "customer_address_e": optional("address_e"),
                        "customer_country": optional("country"),
                        "payment_plan": optional("current_tariff_name"),
                        "firmware_version": optional("firmware"),
                        "is_locked": optional("is_running_plan"),
                        "serial_number": "serial",
                        "device_external_id": "serial",
                        "customer_location_area_1": optional("state"),
                        "customer_latitude_b": optional_float("lat_b"),
                        "customer_longitude_b": optional_float("lon_b"),
                        "customer_latitude_p": optional("lat_p"),
                        "customer_longitude_p": optional("lon_p"),
                        "customer_latitude_e": optional("lat_e"),
                        "customer_longitude_e": optional("lon_e"),
                        "customer_phone_p": S.customer_phone_p,
                        "customer_phone_e": S.customer_phone_e,
                        "customer_external_id": S.customer_external_id,
                        "tags": optional("tags"),
                        "street1_p": optional("street1_p"),
                        "street1_e": optional("street1_e"),
                        "postalcode": optional("postalcode"),
                        "active": optional("active"),
                        "bootloader": optional("bootloader"),
                        "city": optional("city"),
                        "street2_p": optional("street2_p"),
                        "street2_e": optional("street2_e"),
                    },
                ],
            )
        ],
        flatten,  # change an array of arrays of object into a single flat array
    )

    glommed = glom(in_dict, spec)

    meters_df = pd.DataFrame(glommed)

    return meters_df.to_dict("records")


def transform(file_data: dict) -> list[dict]:
    """Transform a dictionary containing Sparkmeter data read from file in the datalake.

    The output is a dictionary of the following schema:
        {
            'data_meters': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            'data_meters_ts': [
                {
                    '<COLUMN>':<VALUE>,
                    ...
                },
                ...
            ],
            ...
        }
    """
    logger.info("transforming spark meter API data")
    data_meters = extract_meters_data(file_data)
    data_meters_ts = extract_meters_ts_data(file_data)
    data_payments_ts = extract_payments_ts_data(file_data)
    skip_count = len(data_meters_ts) - len(data_meters)
    if skip_count:
        logger.info("Skipped {} meters that have no ID on sparkmeter", skip_count)
    return {
        "data_meters": data_meters,
        "data_meters_ts": data_meters_ts,
        "data_payments_ts": data_payments_ts,
    }
