"""Impute Wh for SHS systems.

that do not have the hardware to capture that directly
(but generic enough to work for other similar situations)

set:
output_energy_interval_wh = system_output_w * interval_seconds / 3600
where:
manufacturer == victron
AND
output_energy_interval_wh is NULL
AND
interval < [hour as default]

We set hour as interval default since longer may result
in a misleading imputed value

"""

from sqlalchemy import Table, update
from sqlalchemy.sql import and_
from sqlalchemy.sql.expression import Update

from dataworker import logger
from dataworker.database import get_or_create_connection, get_table

TABLE_UPDATES = {"data_shs_ts": {"manufacturer": "victron", "max_interval": 3600}}


def create_query(table: Table, manufacturer: str, max_interval: int) -> Update:
    """Create update query for the cases defined in parameters."""
    # Build the update statement
    stmt = (
        update(table)
        .where(
            and_(
                table.c.manufacturer == manufacturer,
                table.c.output_energy_interval_wh == None,  # noqa: E711
                table.c.interval_seconds <= max_interval,
            )
        )
        .values(
            output_energy_interval_wh=(
                table.c.system_output_w * table.c.interval_seconds / 3600
            )
        )
    )
    return stmt


def run() -> None:
    """Task entry point to impute Wh for cases defined in TABLES_UPDATES."""
    conn = get_or_create_connection()
    logger.info("Imputing missing Wh values")
    for table, value_mappings in TABLE_UPDATES.items():
        table = get_table(table)
        statement = create_query(
            table, value_mappings["manufacturer"], value_mappings["max_interval"]
        )
        conn.execute(statement)
