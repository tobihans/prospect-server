"""Calculate eligibility for the Uganda RBF EASP Program for CCS.

This task calculates eligibility, subsidy amount and incentive amount for clean cooking
solutions (CCS) device sales according to the framework of the Energy Access Scale-up
Project (EASP). This result-based financing (RBF) program is conducted by the Uganda
Energy Credit Capitalisation Company (UECCC) and affects sales of 9 different
pre-approved categories of CCS devices.

Incentive calculation logic:
    base incentive
      -according to region
    additional incentive
      -for certain districts
      -for hard to reach districts (karamoja + islands)
      -for refugee hosting district (RHD) = 5000
    no incentive
      -for Kampala (achieved by subtracting Central region incentive)

Sources:
    data_meters
    data_payments_ts
    rbf_products
    rbf_product_prices
    rbf_claim_templates
    organization_group_members
    data_trust_trace

Writes to:
    data_trust_trace
"""

from typing import Generic, TypedDict, TypeVar

from sqlalchemy import RowMapping, Table, func, select

from dataworker import logger
from dataworker.database import get_connection, get_table

from . import subchecks as subs
from .common import (
    CLAIM_TEMPLATES_TABLE,
    DISTRICT_FALLBACK_DICT,
    METERS_TABLE,
    PRODUCTS_TABLE,
    IncentiveDict,
    build_verification_for_entities,
    get_technology,
)

# task identity
CHECK_NAME = "rbf_easp_ccs_is_eligible"
task_name = CHECK_NAME

# incentive mapping for ccs
incentives_a: IncentiveDict = {
    "region": {
        "central": 5000,
        "northern": 8000,
        "eastern": 8000,
        "western": 8000,
    },
    "district": {
        "kampala": {"amount": -5000, "type": "kampala"},
        "kalangala": {"amount": 7000, "type": "islands"},
        "buvuma": {"amount": 7000, "type": "islands"},
        "kaabong": {"amount": 4000, "type": "karamoja"},
        "karenga": {"amount": 4000, "type": "karamoja"},
        "kotido": {"amount": 4000, "type": "karamoja"},
        "abim": {"amount": 4000, "type": "karamoja"},
        "amudat": {"amount": 4000, "type": "karamoja"},
        "moroto": {"amount": 4000, "type": "karamoja"},
        "nabilatuk": {"amount": 4000, "type": "karamoja"},
        "nakapiripirit": {"amount": 4000, "type": "karamoja"},
        "napak": {"amount": 4000, "type": "karamoja"},
        "adjumani": {"amount": 5000, "type": "rhd"},
        "yumbe": {"amount": 5000, "type": "rhd"},
        "madi-okollo": {"amount": 5000, "type": "rhd"},
        "terego": {"amount": 5000, "type": "rhd"},
        "isingiro": {"amount": 5000, "type": "rhd"},
        "kikuube": {"amount": 5000, "type": "rhd"},
        "obongi": {"amount": 5000, "type": "rhd"},
        "kyegegwa": {"amount": 5000, "type": "rhd"},
        "kamwenge": {"amount": 5000, "type": "rhd"},
        "lamwo": {"amount": 5000, "type": "rhd"},
        "kiryandongo": {"amount": 5000, "type": "rhd"},
        "koboko": {"amount": 5000, "type": "rhd"},
    },
}

incentives_bf: IncentiveDict = {
    "region": {
        "central": 14000,
        "northern": 24000,
        "eastern": 24000,
        "western": 24000,
    },
    "district": {
        "kampala": {"amount": -14000, "type": "kampala"},
        "kalangala": {"amount": 15000, "type": "islands"},
        "buvuma": {"amount": 15000, "type": "islands"},
        "kaabong": {"amount": 5000, "type": "karamoja"},
        "karenga": {"amount": 5000, "type": "karamoja"},
        "kotido": {"amount": 5000, "type": "karamoja"},
        "abim": {"amount": 5000, "type": "karamoja"},
        "amudat": {"amount": 5000, "type": "karamoja"},
        "moroto": {"amount": 5000, "type": "karamoja"},
        "nabilatuk": {"amount": 5000, "type": "karamoja"},
        "nakapiripirit": {"amount": 5000, "type": "karamoja"},
        "napak": {"amount": 5000, "type": "karamoja"},
        "adjumani": {"amount": 5000, "type": "rhd"},
        "yumbe": {"amount": 5000, "type": "rhd"},
        "madi-okollo": {"amount": 5000, "type": "rhd"},
        "terego": {"amount": 5000, "type": "rhd"},
        "isingiro": {"amount": 5000, "type": "rhd"},
        "kikuube": {"amount": 5000, "type": "rhd"},
        "obongi": {"amount": 5000, "type": "rhd"},
        "kyegegwa": {"amount": 5000, "type": "rhd"},
        "kamwenge": {"amount": 5000, "type": "rhd"},
        "lamwo": {"amount": 5000, "type": "rhd"},
        "kiryandongo": {"amount": 5000, "type": "rhd"},
        "koboko": {"amount": 5000, "type": "rhd"},
    },
}
T = TypeVar("T")


class CcsCategoryDict(Generic[T], TypedDict):
    """Generic TypedDict to ensure all CCS categories are defined."""

    ai: T
    aii: T
    aiii: T
    b: T
    c: T
    d: T
    ei: T
    eii: T
    f: T


INCENTIVES: CcsCategoryDict[IncentiveDict] = {
    "ai": incentives_a,
    "aii": incentives_a,
    "aiii": incentives_a,
    "b": incentives_bf,
    "c": incentives_bf,
    "d": incentives_bf,
    "ei": incentives_bf,
    "eii": incentives_bf,
    "f": incentives_bf,
}


# subchecks applicable to ccs
SUBCHECKS = [
    subs.subcheck__customer_data_is_complete,
    subs.subcheck__customer_data_is_unique,
    subs.subcheck__payment_plan_is_complete,
    subs.subcheck__product_is_on_white_list,
    subs.subcheck__sale_is_not_older_than_three_months,
    subs.subcheck__entity_is_repossessed_at_most_once,
]


# alternative incentive calculation function specific to ccs
def calculate_incentive(entity: RowMapping, incentives: dict, table: Table) -> dict:
    """Calculate incentive for an approved device.

    Will also return `"result": False` in case an error is catched
    in the mappings, especially for location fields.

    `table` must be the Table object for devices or meters.
    """
    products = get_table(PRODUCTS_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    connection = get_connection()
    device = (
        connection.execute(
            select(
                products.c.rbf_category,
            ).where(
                table.c.uid == entity["uid"],
                table.c.organization_id == products.c.organization_id,
                func.lower(table.c.manufacturer) == func.lower(products.c.manufacturer),
                func.lower(table.c.model) == func.lower(products.c.model),
                products.c.rbf_claim_template_id == claim_templates.c.id,
                claim_templates.c.trust_trace_check == CHECK_NAME,
            )
        )
        .mappings()
        .one()
    )

    incentive_category = device["rbf_category"]
    region = entity["customer_location_area_1"]
    district = entity["customer_location_area_2"]

    # handle possible mapping failures
    incentive_type = ""
    if incentive_category.lower() not in incentives:
        logger.warning(
            "unexpected incentive_category {} for entity {}",
            incentive_category,
            entity["uid"],
        )
        incentive_type = f"unknown incentive category {incentive_category}"
    elif region.lower() not in incentives[incentive_category.lower()]["region"]:
        incentive_type = f"unknown region {region}"
        logger.warning(
            "unexpected customer_location_area_1 {} for entity {}",
            region,
            entity["uid"],
        )
    if incentive_type:
        return {
            "incentive__amount": 0,
            "incentive__type": incentive_type,
            "result": False,
        }

    region_award = incentives[incentive_category.lower()]["region"][region.lower()]
    district_data = incentives[incentive_category.lower()]["district"].get(
        district.lower(), DISTRICT_FALLBACK_DICT
    )
    district_award = district_data["amount"]
    incentive_type = f"{district_data['type'].title()}/{incentive_category.title()}"

    return {
        "incentive__amount": region_award + district_award,
        "incentive__type": f"{region.title()}/{district.title()}/{incentive_type}",
    }


RBF_CONFIG = {
    "check_name": CHECK_NAME,
    "entities_table": METERS_TABLE,
    "incentives": INCENTIVES,
    "subchecks": SUBCHECKS,
    "data_append_funcs": get_technology,
    "calc_incentive_func": calculate_incentive,
}


def run() -> dict:
    """Entrypoint."""
    logger.info("verifying ccs eligibilities for easp rbf...")
    return {"data_trust_trace": build_verification_for_entities(**RBF_CONFIG)}
