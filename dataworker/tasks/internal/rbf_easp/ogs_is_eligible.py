"""Calculate eligibility for the Uganda RBF EASP Program for OGS.

This task calculates eligibility, subsidy amount and incentive amount for off-grid solar
(OGS) system sales according to the framework of the Energy Access Scale-up Project
(EASP). This result-based financing (RBF) program is conducted by the Uganda Energy
Credit Capitalisation Company (UECCC) and affects sales of solar systems falling under
categories 1-3 of the Multi-Tier Framework for Energy Access (MTF) established by the
Energy Sector Management Assistance Program (ESMAP).

Incentive calculation logic:
    base incentive
      -according to region
    additional incentive
      -for certain districts
      -for hard to reach districts (karamoja + islands)
      -for refugee hosting district (RHD) = 5000
    no incentive
      -for Kampala (achieved by subtracting Central region incentive)

Sources:
    data_shs
    data_payments_ts
    rbf_products
    rbf_product_prices
    rbf_claim_templates
    organization_group_members
    data_trust_trace

Writes to:
    data_trust_trace
"""

from dataworker import logger

from . import subchecks as subs
from .common import (
    SHS_TABLE,
    IncentiveDict,
    build_verification_for_entities,
    get_rated_power,
    get_technology,
)

# task identity
CHECK_NAME = "rbf_easp_ogs_is_eligible"
task_name = CHECK_NAME

# incentive mapping for ogs
INCENTIVES: IncentiveDict = {
    "region": {
        "central": 5000,
        "northern": 11000,
        "eastern": 11000,
        "western": 11000,
    },
    "district": {
        "kampala": {"amount": -5000, "type": "kampala"},
        "kalangala": {"amount": 10000, "type": "islands"},
        "buvuma": {"amount": 10000, "type": "islands"},
        "kaabong": {"amount": 4000, "type": "karamoja"},
        "karenga": {"amount": 4000, "type": "karamoja"},
        "kotido": {"amount": 4000, "type": "karamoja"},
        "abim": {"amount": 4000, "type": "karamoja"},
        "amudat": {"amount": 4000, "type": "karamoja"},
        "moroto": {"amount": 4000, "type": "karamoja"},
        "nabilatuk": {"amount": 4000, "type": "karamoja"},
        "nakapiripirit": {"amount": 4000, "type": "karamoja"},
        "napak": {"amount": 4000, "type": "karamoja"},
        "adjumani": {"amount": 5000, "type": "rhd"},
        "yumbe": {"amount": 5000, "type": "rhd"},
        "madi-okollo": {"amount": 5000, "type": "rhd"},
        "terego": {"amount": 5000, "type": "rhd"},
        "isingiro": {"amount": 5000, "type": "rhd"},
        "kikuube": {"amount": 5000, "type": "rhd"},
        "obongi": {"amount": 5000, "type": "rhd"},
        "kyegegwa": {"amount": 5000, "type": "rhd"},
        "kamwenge": {"amount": 5000, "type": "rhd"},
        "lamwo": {"amount": 5000, "type": "rhd"},
        "kiryandongo": {"amount": 5000, "type": "rhd"},
        "koboko": {"amount": 5000, "type": "rhd"},
    },
}


# subchecks applicable to ogs
SUBCHECKS = [
    subs.subcheck__customer_data_is_complete,
    subs.subcheck__customer_data_is_unique,
    subs.subcheck__payment_plan_is_complete,
    subs.subcheck__product_is_on_white_list,
    subs.subcheck__sale_is_not_older_than_three_months,
    subs.subcheck__entity_is_repossessed_at_most_once,
]


RBF_CONFIG = {
    "check_name": CHECK_NAME,
    "entities_table": SHS_TABLE,
    "incentives": INCENTIVES,
    "subchecks": SUBCHECKS,
    "data_append_funcs": [get_rated_power, get_technology],
}


def run() -> dict:
    """Entrypoint."""
    logger.info("verifying ogs eligibilities for easp rbf...")
    return {"data_trust_trace": build_verification_for_entities(**RBF_CONFIG)}
