"""Submodule containing all the eligibility tasks from RBF EASP."""

SUBTASKS = [
    {"TASK_NAME": "rbf_easp_ogs_is_eligible", "ENTRYPOINT": "ogs_is_eligible.run"},
    {"TASK_NAME": "rbf_easp_pue_is_eligible", "ENTRYPOINT": "pue_is_eligible.run"},
    {"TASK_NAME": "rbf_easp_ccs_is_eligible", "ENTRYPOINT": "ccs_is_eligible.run"},
    {
        "TASK_NAME": "rbf_easp_is_eligible_per_import",
        "ENTRYPOINT": "is_eligible_per_import.run",
    },
]
