"""Run OGS, PUE and CCS rbf_easp eligibility checks on data from one import."""

from . import ccs_is_eligible as ccs
from . import ogs_is_eligible as ogs
from . import pue_is_eligible as pue
from .common import build_verification_for_entities


def run(import_id: int) -> dict:
    """Entrypoint."""
    rbf_configs = [ogs.RBF_CONFIG, pue.RBF_CONFIG, ccs.RBF_CONFIG]

    trust_traces = []
    for params in rbf_configs:
        trust_traces.extend(
            build_verification_for_entities(**params, import_id=import_id)
        )
    return {"data_trust_trace": trust_traces}


task_name = "rbf_easp_is_eligible_per_import"
