"""Methods and constants shared amongst rbf_easp eligibility tasks."""

from collections import Counter
from collections.abc import Callable, Mapping
from datetime import UTC, datetime
from functools import cache
from typing import NamedTuple, NotRequired, TypedDict

import funcy
from sqlalchemy import RowMapping, Table, and_, desc, func, or_, select

from dataworker import logger
from dataworker.database import get_connection, get_table
from dataworker.processing import cast_to_bool
from dataworker.queries import fetch_scalar

# -------------------- shared tables --------------------
PRODUCTS_TABLE = "rbf_products"
PRODUCT_PRICES_TABLE = "rbf_product_prices"
CLAIM_TEMPLATES_TABLE = "rbf_claim_templates"
GROUP_MEMBERS_TABLE = "organization_group_members"
VERIFICATIONS_TABLE = "data_trust_trace"
PAYMENTS_TS_TABLE = "data_payments_ts"

# -------------------- available entities tables --------------------
METERS_TABLE = "data_meters"
SHS_TABLE = "data_shs"

# -------------------- SPECIAL DB COLUMNS --------------------
FEMALE_OWNERSHIP_CUSTOM_COL = "is_female_owned"


# -------------------- shared typing --------------------
class IncentiveDistrictValueDict(TypedDict):
    """Typed dict to enforce the correct values on district incentive definition."""

    amount: int
    type: str


incentive_district_dict_type = {
    "kampala": IncentiveDistrictValueDict,
    "kalangala": IncentiveDistrictValueDict,
    "buvuma": IncentiveDistrictValueDict,
    "kaabong": IncentiveDistrictValueDict,
    "karenga": IncentiveDistrictValueDict,
    "kotido": IncentiveDistrictValueDict,
    "abim": IncentiveDistrictValueDict,
    "amudat": IncentiveDistrictValueDict,
    "moroto": IncentiveDistrictValueDict,
    "nabilatuk": IncentiveDistrictValueDict,
    "nakapiripirit": IncentiveDistrictValueDict,
    "napak": IncentiveDistrictValueDict,
    "adjumani": IncentiveDistrictValueDict,
    "yumbe": IncentiveDistrictValueDict,
    "madi-okollo": IncentiveDistrictValueDict,
    "terego": IncentiveDistrictValueDict,
    "isingiro": IncentiveDistrictValueDict,
    "kikuube": IncentiveDistrictValueDict,
    "obongi": IncentiveDistrictValueDict,
    "kyegegwa": IncentiveDistrictValueDict,
    "kamwenge": IncentiveDistrictValueDict,
    "lamwo": IncentiveDistrictValueDict,
    "kiryandongo": IncentiveDistrictValueDict,
    "koboko": IncentiveDistrictValueDict,
}
# since one key has an hyphen, we have to use the functional syntax for this typed dict
# NB: We only list districts having special conditions. The list is non-exhaustive, the
# other districts just have no special incentive. We also don't check for case
# insensitivity, this should be done in the ingestor on the ruby side
IncentiveDistrictDict = TypedDict("IncentiveDistrictDict", incentive_district_dict_type)

DISTRICT_FALLBACK_DICT: IncentiveDistrictDict = {"amount": 0, "type": "Only Regional"}


class IncentiveRegionDict(TypedDict):
    """Typed dict defining the possible regions."""

    central: int
    northern: int
    eastern: int
    western: int


class IncentiveDict(TypedDict):
    """Incentive definition typed dict."""

    region: IncentiveRegionDict
    district: IncentiveDistrictDict


class SubsidyAdjustmentsDict(TypedDict):
    """Subsidy adjustments typed dict."""

    female_ownership: int


# -------------------- shared subcheck typing --------------------


class SubcheckResultDict(TypedDict):
    """Typed dict defining the possible regions."""

    result: bool
    details: NotRequired[list[str] | str]


# ---------------- available data append functions ---------------


def get_technology(entity: Mapping, table: Table, check_name: str) -> dict[str | None]:
    """Fetch type of tech of device if whitelisted and add result to payload."""
    products = get_table(PRODUCTS_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    technology = fetch_scalar(
        select(products.c.technology).where(
            table.c.uid == entity["uid"],
            table.c.organization_id == products.c.organization_id,
            func.lower(table.c.manufacturer) == func.lower(products.c.manufacturer),
            func.lower(table.c.model) == func.lower(products.c.model),
            products.c.rbf_claim_template_id == claim_templates.c.id,
            claim_templates.c.trust_trace_check == check_name,
        )
    )
    return {"technology": technology}


def get_rated_power(entity: Mapping, table: Table, check_name: str) -> dict[str | None]:
    """Fetch rated power of a system if whitelisted and add result to payload."""
    products = get_table(PRODUCTS_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    rated_power = fetch_scalar(
        select(products.c.rated_power_w).where(
            table.c.uid == entity["uid"],
            table.c.organization_id == products.c.organization_id,
            func.lower(table.c.manufacturer) == func.lower(products.c.manufacturer),
            func.lower(table.c.model) == func.lower(products.c.model),
            products.c.rbf_claim_template_id == claim_templates.c.id,
            claim_templates.c.trust_trace_check == check_name,
        )
    )
    return {"rated_power_w": rated_power}


# -------------------- shared functions --------------------


def calculate_incentive(entity: Mapping, incentives: dict, table: Table) -> dict:
    """Calculate incentive for an approved entity.

    Will also return `"result": False` in case an error is catched
    in the mappings, especially for location fields.

    `table` must be the Table object for the entity (device or meter).
    """
    region = entity["customer_location_area_1"]
    district = entity["customer_location_area_2"]

    # handle possible mapping failures
    if region.lower() not in incentives["region"]:
        incentive_type = f"unknown region {region}"
        logger.warning("unexpected customer_location_area_1 {} in {}", region, entity)
        return {
            "incentive__amount": 0,
            "incentive__type": incentive_type,
            "result": False,
        }

    region_award = incentives["region"][region.lower()]
    district_data = incentives["district"].get(district.lower(), DISTRICT_FALLBACK_DICT)
    district_award = district_data["amount"]
    incentive_type = district_data["type"].title()

    return {
        "incentive__amount": region_award + district_award,
        # NOTE: RHD will be Rhd.
        "incentive__type": f"{region.title()}/{district.title()}/{incentive_type}",
    }


def calculate_subsidy(
    entity: Mapping,
    table: Table,
    check_name: str,
    subsidy_adjustments: dict | None = None,
) -> dict:
    """Calculate subsidy for an approved entity.

    Will add to the return value `"result": False` in case the category
    or the gender can not be mapped.

    `table` must be the Table object for the entity (device or meter).
    """
    products = get_table(PRODUCTS_TABLE)
    product_prices = get_table(PRODUCT_PRICES_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)

    # TODO: won't be needed once we move over to devices table.
    payment_plan_column = (
        table.c.payment_plan_type
        if hasattr(table.c, "payment_plan_type")
        else table.c.payment_plan
    )

    connection = get_connection()
    entity = (
        connection.execute(
            select(
                products.c.rbf_category,
                product_prices.c.id,
                product_prices.c.base_subsidized_sales_price,
                product_prices.c.base_subsidy_amount,
                product_prices.c.original_sales_price,
                table.c.custom[FEMALE_OWNERSHIP_CUSTOM_COL].label("female_ownership"),
            )
            .where(
                table.c.uid == entity["uid"],
                table.c.organization_id == products.c.organization_id,
                func.lower(table.c.manufacturer) == func.lower(products.c.manufacturer),
                func.lower(table.c.model) == func.lower(products.c.model),
                products.c.rbf_claim_template_id == claim_templates.c.id,
                claim_templates.c.trust_trace_check == check_name,
                product_prices.c.rbf_product_id == products.c.id,
                func.lower(product_prices.c.category)
                == func.lower(payment_plan_column),
                product_prices.c.valid_from <= table.c.purchase_date,
            )
            .order_by(desc(product_prices.c.valid_from))
        )
        .mappings()
        .first()
    )
    if not entity:
        return {
            "subsidy__amount": 0,
            "subsidy__error": "No valid price found for the combination of purchase "
            "date, sales mode and product.",
            "result": False,
        }

    base_subsidized_sales_price = entity["base_subsidized_sales_price"]
    base_subsidy_amount = entity["base_subsidy_amount"]
    original_sales_price = entity["original_sales_price"]
    price_id = entity["id"]
    category = entity["rbf_category"]
    female_ownership = cast_to_bool(entity["female_ownership"])
    # handle possible mapping mismatch
    subsidy_category = ""
    if not base_subsidized_sales_price:
        subsidy_category = f"no subsidized price listed for category {category}"
        logger.warning(
            "no subsidized price listed for rbf_category {} in {}", category, entity
        )
    if subsidy_category:
        return {
            "subsidy__amount": 0,
            "subsidy__category": subsidy_category,
            "result": False,
        }

    # handle potential subsidy adjustments
    # 1. extra money for females (currently only adjustment available, for pue only)
    female_award = (
        subsidy_adjustments.get("female_ownership", 0)
        if female_ownership and subsidy_adjustments
        else 0
    )

    subsidy_category = f"{category}/female_ownership" if female_award > 0 else category

    return {
        "subsidy__base_subsidized_sales_price": base_subsidized_sales_price,
        "subsidy__aggregated_subsidy_adjustment": female_award,
        "subsidy__subsidized_sales_price": base_subsidized_sales_price - female_award,
        "subsidy__amount": base_subsidy_amount + female_award,
        "subsidy__original_sales_price": original_sales_price,
        "subsidy__price_calculated_at": datetime.now(UTC).isoformat(sep="T"),
        "subsidy__price_id": price_id,
        "subsidy__category": subsidy_category,
    }


def verification_is_successful(verification: dict) -> dict:
    """Check if all checks were passed and therefore entity successfully verified."""
    check_is_passed = all(
        val["result"] is True
        for key, val in verification.items()
        if key.startswith("subcheck")
    )
    return {"result": bool(check_is_passed)}


def build_verification(
    entity: Mapping,
    check_name: str,
    table: Table,
    subject_origin: str,
    subchecks: list[Callable],
    incentives: dict,
    subsidy_adjustments: dict | None = None,
    calc_incentive_func: Callable = calculate_incentive,
    data_append_funcs: Callable | list[Callable] | None = None,
) -> dict:
    """Build verification Record for entity given a uid."""
    # build base signature of verification
    verification = {
        "subject_origin": subject_origin,
        "check": check_name,
        "subject_uid": entity["uid"],
        "organization_id": entity["organization_id"],
        "source_id": entity["source_id"],
        "data_origin": entity["data_origin"],
        "import_id": entity["import_id"],
    }

    # run subchecks and add results to verification
    subcheck_result = funcy.join(
        funcy.map(
            lambda subcheck: {
                subcheck.__name__: subcheck(
                    entity, table, check_name, subsidy_adjustments
                )
            },
            subchecks,
        )
    )
    verification |= subcheck_result

    # determine overall success of eligibility check and add to verification
    verification |= verification_is_successful(verification)

    # add information from data append function(s), if present
    if data_append_funcs:
        if callable(data_append_funcs):
            verification |= data_append_funcs(entity, table, check_name)
        else:
            for func in data_append_funcs:
                verification |= func(entity, table, check_name)

    # if verification is successful, calculate subsidy and incentive
    if verification["result"] is True:
        verification |= calculate_subsidy(
            entity,
            table,
            check_name,
            subsidy_adjustments,
        )
        verification |= calc_incentive_func(entity, incentives, table)

    return verification


def get_unverified_entities(
    table: Table, check_name: str, import_id: int | None = None
) -> list[RowMapping]:
    """Get unique RowMappings with base data from entities, by import if given.

    If import_id is given, we need to get the entities directly affected by the import,
    but also the unverified ones belonging to account_uids of payments affected by it.

    - organization_id from `table` is in organization group related to CHECK_NAME
    - no verification result of True exists for CHECK_NAME in VERIFICATION_TABLE

    We also make sure to get all possible checks, but to only run on the check aimed
    for the product in case a product is mapping the entity.
    """
    connection = get_connection()
    group_members = get_table(GROUP_MEMBERS_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    verifications = get_table(VERIFICATIONS_TABLE)
    payments_ts = get_table(PAYMENTS_TS_TABLE)
    products = get_table(PRODUCTS_TABLE)
    columns = [
        "uid",
        "organization_id",
        "source_id",
        "data_origin",
        "import_id",
        "customer_name_p",
        "customer_id_number_p",
        "customer_gender",
        "customer_phone_p",
        "customer_location_area_1",
        "customer_location_area_2",
        "customer_location_area_3",
        "customer_location_area_4",
        "customer_location_area_5",
        "purchase_date",
        "manufacturer",
        "model",
        "serial_number",
        "customer_category",
    ]

    statement = select(
        table.c[*columns],
        table.c.custom["is_below_50_employees_and_100mio_ugx_assets"].astext.label(
            "is_small_enterprise"
        ),
    ).where(
        table.c.organization_id.in_(
            select(group_members.c.organization_id).where(
                claim_templates.c.trust_trace_check == check_name,
                group_members.c.organization_group_id
                == claim_templates.c.organization_group_id,
            )
        ),
        ~table.c.uid.in_(
            select(verifications.c.subject_uid).where(
                verifications.c.check == check_name,
                verifications.c.result == "True",
            )
        ),
    )
    if import_id:
        statement = statement.where(
            or_(
                table.c.import_id == import_id,
                table.c.last_import_id == import_id,
                # to take into account devices unmodified but with new payments
                table.c.account_uid.in_(
                    select(payments_ts.c.account_uid).where(
                        or_(
                            payments_ts.c.import_id == import_id,
                            payments_ts.c.last_import_id == import_id,
                        )
                    )
                ),
            ),
        ).distinct()
    subquery = statement.subquery()

    # here we make sure to only submit entities that have a product mapped for that
    # check OR not mapped to any product at all
    new_statement = (
        select(
            subquery.c[*columns],
            subquery.c.is_small_enterprise,
            products.c.rbf_category,
        )
        .select_from(subquery)
        .join(
            products,
            onclause=(
                and_(
                    subquery.c.organization_id == products.c.organization_id,
                    func.lower(subquery.c.manufacturer)
                    == func.lower(products.c.manufacturer),
                    func.lower(subquery.c.model) == func.lower(products.c.model),
                )
            ),
            isouter=True,
        )
        .join(
            claim_templates,
            onclause=products.c.rbf_claim_template_id == claim_templates.c.id,
            isouter=True,
        )
        .where(
            or_(
                claim_templates.c.trust_trace_check == check_name,
                claim_templates.c.trust_trace_check == None,  # noqa: E711
            )
        )
        .order_by(subquery.c.purchase_date.asc())
    )

    return list(connection.execute(new_statement).mappings())


def build_verification_for_entities(
    check_name: str,
    entities_table: str,
    subchecks: list[Callable],
    incentives: dict,
    subsidy_adjustments: dict | None = None,
    calc_incentive_func: Callable = calculate_incentive,
    data_append_funcs: Callable | list[Callable] | None = None,
    import_id: int | None = None,
) -> list[dict]:
    """Build verification records for unverified uids, by import if requested.

    Directly called by entrypoint function within the individual modules. The
    verification process starts here after taking the individual module's inputs
    (e.g. ogs, pue, ccs).

    If import_id is given, we need to get the entities directly affected by the import,
    but also the unverified ones belonging to account_uids of payments affected by it.
    """
    table = get_table(entities_table)
    subject_origin = entities_table.replace("data_", "")
    # Make sure import_id is an integer. Fail early if not the case or convertible.
    if import_id:
        import_id = int(import_id)
    entities = get_unverified_entities(table, check_name, import_id)

    verifications = []
    for entity in entities:
        try:
            verifications.append(
                build_verification(
                    entity=entity,
                    check_name=check_name,
                    table=table,
                    subject_origin=subject_origin,
                    subchecks=subchecks,
                    incentives=incentives,
                    subsidy_adjustments=subsidy_adjustments,
                    calc_incentive_func=calc_incentive_func,
                    data_append_funcs=data_append_funcs,
                )
            )
        except Exception:
            logger.exception(
                "error while building verification for {} in {}",
                entity["uid"],
                check_name,
            )

    verifications = post_check_uniqueness_inside_verification_batch(
        entities=entities, verifications=verifications, check_name=check_name
    )

    verification_result_counter = Counter(v["result"] for v in verifications)
    logger.info(
        "Finished generating verifications for {} with result: {}",
        check_name,
        ", ".join(f"{k}: {v}" for k, v in verification_result_counter.items()),
    )
    return verifications


def invalidate_verification_not_unique(verification: dict, other_uid: str) -> None:
    """Update verification/trust trace entry to invalidate it based on uniqueness."""
    verification["subcheck__customer_data_is_unique"] = {
        "result": False,
        "details": f"customer&category is not unique (conflicting uid: {other_uid}).",
    }
    verification["result"] = False
    # Using .keys() here as we would else be modifying the dict while looping on it
    for key in tuple(verification.keys()):
        if any((key.startswith("incentive_"), key.startswith("subsidy_"))):
            del verification[key]


class UniqueKey(NamedTuple):
    """Tuple identifying uniqueness of devices."""

    id_number: str
    categories: str | tuple[str]


class EntityInfo(NamedTuple):
    """Tuple containing infos about each non-unique entity and its verification."""

    index: int
    uid: str
    verification_result: bool | str


def post_check_uniqueness_inside_verification_batch(
    entities: list[Mapping], verifications: list[dict], check_name: str
) -> list[dict]:
    """Invalidate afterwards verifications that aren't unique to the verification batch.

    The uniqueness subcheck is run against data inside the DB.
    Here we make sure that we don't generate trust traces for two devices in parallel
    that would defeat the uniqueness constraint.
    Here we identify uniqueness by the customer_id_number and the category uniqueness,
    which is 1 per category for PUE and OGS, but 1 for A categories and 1 for B-F
    categories for CCS.
    We skip the potential time constraint (2 years), as this would be covered by the
    check that the sales happened within the last 3 months.
    """
    # get the counts per unique_key
    unique_key_counter = Counter(
        UniqueKey(
            entity["customer_id_number_p"], get_unique_categories(entity, check_name)
        )
        for entity in entities
        if entity["customer_id_number_p"]
    )
    non_unique_infos: dict[UniqueKey, list[EntityInfo]] = {
        unique_key: [] for unique_key, count in unique_key_counter.items() if count > 1
    }
    # if all entities have different customer id numbers, return them
    if not non_unique_infos:
        return verifications

    # Gather informations about the non-unique entities and the belonging verifications
    for index, (entity, verification) in enumerate(
        zip(entities, verifications, strict=True)
    ):
        # perf hint: done again, but we should only rarely not go to the early return
        unique_key = UniqueKey(
            entity["customer_id_number_p"],
            get_unique_categories(entity, check_name),
        )
        if unique_key in non_unique_infos:
            if entity["uid"] != verification["subject_uid"]:
                logger.error(
                    "entity uid {} does not match verification uid {}",
                    entity["uid"],
                    verification["subject_uid"],
                )
            else:
                non_unique_infos[unique_key].append(
                    EntityInfo(
                        index,
                        entity["uid"],
                        verification["result"],
                    )
                )

    # now loop again through the grouped non-unique devices, to discard only the ones
    # that have more than one verification with result True
    for unique_key, entity_info_list in non_unique_infos.items():
        positive_result_uid = None
        for entity_info in entity_info_list:
            # TODO: we should also soon have "warn" -> use "in" then
            # first time it encounters a positive result, register uid and keep
            if entity_info.verification_result is True and not positive_result_uid:
                positive_result_uid = entity_info.uid
            # any further positive verifications are invalidated
            elif entity_info.verification_result is True:
                logger.info(
                    "Invalidate verification for uid {} as for id_number and category "
                    "{} the entry with uid {} is already valid inside the check batch.",
                    entity_info.uid,
                    unique_key,
                    positive_result_uid,
                )
                invalidate_verification_not_unique(
                    verifications[entity_info.index], positive_result_uid
                )
            # non-positive results are ignored

    return verifications


@cache
def get_unique_category_mapper() -> dict[str, dict[str, tuple[str]]]:
    """Return a cached dict of category grouped per check name."""
    from .ccs_is_eligible import CHECK_NAME

    categories_a = (
        "ai",
        "aii",
        "aiii",
    )
    categories_bf = (
        "b",
        "c",
        "d",
        "ei",
        "eii",
        "f",
    )

    return {
        CHECK_NAME: {
            "ai": categories_a,
            "aii": categories_a,
            "aiii": categories_a,
            "b": categories_bf,
            "c": categories_bf,
            "d": categories_bf,
            "ei": categories_bf,
            "eii": categories_bf,
            "f": categories_bf,
        }
    }


def get_unique_categories(entity: dict, check_name: str) -> tuple[str] | str | None:
    """Get category or tuple of categories to group by for entity and check."""
    if not entity["rbf_category"]:
        return None
    unique_category_mapper = get_unique_category_mapper()
    if check_name in unique_category_mapper:
        categories = unique_category_mapper[check_name].get(
            entity["rbf_category"].lower()
        )
        if not categories:
            logger.warning(
                "unexpected category {} for check {}",
                entity["rbf_category"],
                check_name,
            )
            categories = entity["rbf_category"].lower()
    else:
        categories = entity["rbf_category"].lower()
    return categories
