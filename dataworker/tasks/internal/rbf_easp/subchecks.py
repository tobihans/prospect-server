"""Subcheck definitions available to rbf_easp eligibility tasks."""

import json
from collections.abc import Iterable, Mapping
from datetime import date
from functools import cache
from pathlib import Path

from dateutil.relativedelta import relativedelta
from sqlalchemy import Table, and_, between, desc, func, select
from sqlalchemy.sql import functions

from dataworker.database import get_table
from dataworker.processing import cast_to_bool
from dataworker.queries import fetch_dict_data, fetch_scalar, fetch_scalars

from .common import (
    CLAIM_TEMPLATES_TABLE,
    FEMALE_OWNERSHIP_CUSTOM_COL,
    PAYMENTS_TS_TABLE,
    PRODUCT_PRICES_TABLE,
    PRODUCTS_TABLE,
    VERIFICATIONS_TABLE,
    SubcheckResultDict,
    get_unique_categories,
)


@cache
def _get_regions_and_level_for_check(check_name: str) -> tuple[dict, int]:
    if check_name.startswith("rbf_easp"):
        # In the case of UECCC EASP, we only check for matching over 2 levels
        # The JSON file was generated from another in core running:
        # jq '. | map_values(keys)' ../core/app/javascript/UECC_locations.json |
        # jq 'walk(if type == "string" then ascii_downcase else . end |
        # if type == "object" then with_entries(.key |= ascii_downcase) else . end)' >
        # tasks/internal/rbf_easp/uganda_region_districts.json
        admin_regions = json.loads(
            Path("./tasks/internal/rbf_easp/uganda_region_districts.json").read_text()
        )
        return admin_regions, 2


def _subcheck__location_areas_match(
    areas: Iterable[str], check_name: str
) -> str | None:
    """Check if the given location areas do match the expectations.

    Is every area listed?
    Is every area listed and contained in the parent area?
    """
    admin_regions, level_check = _get_regions_and_level_for_check(check_name)
    areas_to_check = areas[:level_check]
    parent_area_msg = ""
    for level, area in enumerate(areas_to_check, start=1):
        area_key = area.lower() if area is not None else None
        if area_key not in admin_regions:
            return f"{area} administrative unit not found{parent_area_msg}."
        if level < level_check:
            admin_regions = admin_regions[area_key]
        parent_area_msg = f" in {area}"


def subcheck__customer_data_is_complete(
    entity: Mapping,
    table: Table,
    check_name: str,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Are all of the following values derived from the entity non-null?:

    -customer name
    -customer national id
    -customer phone_number
    -customer admin unit level [1..5]

    Do the customer admin unit match how they should?
    Is every unit listed and contained in the parent unit?
    """
    required_fields = [
        "customer_name_p",
        "customer_id_number_p",
        "customer_phone_p",
        "customer_location_area_1",
        "customer_location_area_2",
        "customer_location_area_3",
        "customer_location_area_4",
        "customer_location_area_5",
    ]
    missing_fields = [field for field in required_fields if not entity[field]]

    location_areas_match_details = _subcheck__location_areas_match(
        [value for key, value in entity.items() if key.startswith("customer_location")],
        check_name=check_name,
    )

    if not missing_fields and not location_areas_match_details:
        return {"result": True}

    details = []
    if missing_fields:
        details.extend(f"{i} is missing" for i in missing_fields)
    if location_areas_match_details:
        details.append(location_areas_match_details)

    return {"result": False, "details": ". ".join(details)}


def subcheck__customer_data_is_unique(
    entity: Mapping,
    table: Table,
    check_name: str | None = None,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Is the entity's customer_national_id_number_p unique for the category?
    It will map categories if some grouping for uniqueness is defined.
    It only accounts for devices with a positive trust_trace.
    It will take only a two years range if applicable for the check.
    """
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    products = get_table(PRODUCTS_TABLE)
    verifications = get_table(VERIFICATIONS_TABLE)
    uniqueness_over_two_years = check_name != "rbf_easp_pue_is_eligible"

    # if not mapped to a product, does not have an rbf_category -> check impossible
    if not entity["rbf_category"]:
        return {
            "result": False,
            "details": "device not in product whitelist, can't assess uniqueness",
        }
    statement = (
        select(table.c.uid)
        .join(
            products,
            onclause=(
                and_(
                    table.c.organization_id == products.c.organization_id,
                    func.lower(table.c.manufacturer)
                    == func.lower(products.c.manufacturer),
                    func.lower(table.c.model) == func.lower(products.c.model),
                )
            ),
        )
        .join(
            claim_templates,
            onclause=(
                and_(
                    products.c.rbf_claim_template_id == claim_templates.c.id,
                    claim_templates.c.trust_trace_check == check_name,
                )
            ),
        )
        .join(
            verifications,
            onclause=(
                and_(
                    verifications.c.subject_uid == table.c.uid,
                    verifications.c.check == check_name,
                )
            ),
        )
        .where(
            table.c.uid != entity["uid"],
            table.c.customer_id_number_p == entity["customer_id_number_p"],
            verifications.c.result == "True",
        )
    )

    # map for categories constituting the uniqueness criteria
    categories = get_unique_categories(entity=entity, check_name=check_name)
    if isinstance(categories, tuple):
        statement = statement.where(func.lower(products.c.rbf_category).in_(categories))
    else:
        statement = statement.where(func.lower(products.c.rbf_category) == categories)
    # check only over past two years if applicable, the between operator is inclusive.
    if uniqueness_over_two_years:
        statement = statement.where(
            between(
                table.c.purchase_date,
                entity["purchase_date"].replace(year=entity["purchase_date"].year - 2),
                entity["purchase_date"].replace(year=entity["purchase_date"].year + 2),
            )
        )

    other_devices_uid = fetch_scalars(statement).all()

    if not other_devices_uid:
        return {"result": True}
    else:
        return {
            "result": False,
            "details": "customer&category is not unique "
            f"(conflicting uid: {', '.join(sorted(other_devices_uid))}).",
        }


def subcheck__customer_is_eligible(
    entity: Mapping,
    table: Table,
    check_name: str | None = None,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Is the entity's customer_category mse or thf?
    Is the entity's is_below_50_employees_and_100mio_ugx_assets True?
    """
    is_eligible_category = str(entity["customer_category"]).lower() in ["mse", "tfh"]
    is_small_enterprise = str(entity["is_small_enterprise"]).lower() == "true"

    if is_eligible_category and is_small_enterprise:
        return {"result": True}

    # prepare details for wrong category or enterprise size
    details = []
    if not is_eligible_category:
        details.append(
            f"customer_category ({entity['customer_category']}) is not one of "
            "['mse', 'tfh']."
        )
    if not is_small_enterprise:
        details.append(
            "Enterprise has more than 50 employees and/or 100mio UGX in assets."
        )

    return {"result": False, "details": " ".join(details)}


def subcheck__entity_is_repossessed_at_most_once(
    entity: Mapping,
    table: Table,
    check_name: str | None = None,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Has the entity been repossessed at most one time?'

    To identify the entity, we will use the combination of manufacturer and
    serial_number (device_uid could have been used, but it wouldn't cover entities
    transferred between organizations).
    """
    # TODO: take into account all repossessions or only the RBF ones?
    repossessions = fetch_scalar(
        select(func.count(1)).where(
            table.c.uid != entity["uid"],
            table.c.purchase_date < entity["purchase_date"],
            func.lower(table.c.manufacturer) == func.lower(entity["manufacturer"]),
            func.lower(table.c.serial_number) == func.lower(entity["serial_number"]),
        )
    )

    if repossessions and repossessions > 1:
        return {
            "result": False,
            "details": "More than one previous sale exists for the manufacturer/serial "
            "combination.",
        }

    return {"result": True}


def subcheck__payment_plan_is_complete(
    entity: Mapping,
    table: Table,
    check_name: str | None = None,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statements for a given uid...

    Are all of the following values derived from the entity non-null?:

    -payment receipt
    -account id
    -payment plan [type, currency]
    -product price
    -payment plan (paygo only) [days, down payment]

    Are the sum of all payments for the account sufficient for the purchase?
    """
    # TODO: check receipt pdf upload -> needs to be implemented in forms first.
    # TODO: add time constraint to payments -> needs to be defined in POM first.

    # Query to sum payments for each account
    payments_ts = get_table(PAYMENTS_TS_TABLE)
    claim_templates = get_table(CLAIM_TEMPLATES_TABLE)
    products = get_table(PRODUCTS_TABLE)
    product_prices = get_table(PRODUCT_PRICES_TABLE)

    # TODO: won't be needed once we move over to devices table.
    payment_plan_column = (
        table.c.payment_plan_type
        if hasattr(table.c, "payment_plan_type")
        else table.c.payment_plan
    )

    # required fields for payment plan check
    required_fields = [
        payment_plan_column,
        table.c.payment_plan_currency,
        product_prices.c.base_subsidized_sales_price,
    ]
    additional_required_fields_paygo = [
        table.c.payment_plan_days,
        table.c.payment_plan_down_payment,
    ]

    # sum up payments for account per uid.
    payment_sums = (
        select(
            table.c.account_uid,
            functions.sum(payments_ts.c.amount).label("total_amount"),
        )
        .where(
            table.c.uid == entity["uid"],
            table.c.account_uid == payments_ts.c.account_uid,
        )
        .group_by(table.c.account_uid)
    ).alias("payment_sums")

    # fetch payment data for given uid.
    payments_dict = fetch_dict_data(select(payment_sums)).one_or_none()
    if not payments_dict:
        return {"result": False, "details": "No payments found."}

    payment_data_dict = fetch_dict_data(
        select(
            table.c.uid,
            payment_sums.c.total_amount,
            table.c.custom[FEMALE_OWNERSHIP_CUSTOM_COL].label("female_ownership"),
            *required_fields,
            *additional_required_fields_paygo,
        )
        .where(
            table.c.uid == entity["uid"],
            table.c.account_uid == payment_sums.c.account_uid,
            func.lower(table.c.manufacturer) == func.lower(products.c.manufacturer),
            func.lower(table.c.model) == func.lower(products.c.model),
            table.c.organization_id == products.c.organization_id,
            products.c.rbf_claim_template_id == claim_templates.c.id,
            claim_templates.c.trust_trace_check == check_name,
            product_prices.c.rbf_product_id == products.c.id,
            func.lower(product_prices.c.category) == func.lower(payment_plan_column),
            product_prices.c.valid_from <= table.c.purchase_date,
        )
        .order_by(desc(product_prices.c.valid_from))
    ).first()

    if not payment_data_dict:
        return {
            "result": False,
            "details": "No valid price found for the combination of purchase date, "
            "sales mode and product.",
        }

    # check if paygo and adjust required fields and financial baseline
    if payment_data_dict[payment_plan_column].lower() == "paygo":
        required_fields += additional_required_fields_paygo
        financial_baseline = "payment_plan_down_payment"
    else:
        financial_baseline = "base_subsidized_sales_price"

    # Check for female ownership.
    female_ownership = cast_to_bool(payment_data_dict["female_ownership"])
    female_award = (
        subsidy_adjustments.get("female_ownership", 0)
        if female_ownership and subsidy_adjustments
        else 0
    )

    # Check for missing fields or insufficient payments
    missing_fields = [f for f in required_fields if payment_data_dict[f] is None]
    if table.c.payment_plan_down_payment in missing_fields:
        payments_are_sufficient = None
    else:
        payments_are_sufficient = payment_data_dict["total_amount"] >= (
            payment_data_dict[financial_baseline] - female_award
        )

    if not missing_fields and payments_are_sufficient:
        return {"result": True}

    # Prepare details for False cases.
    details = []
    if missing_fields:
        details.extend(f"{field} is missing." for field in missing_fields)

    if payments_are_sufficient is None:
        details.append(f"{financial_baseline} is required for payments check.")
    elif not payments_are_sufficient:
        details.append(
            f"Payment sums for account not sufficient to cover {financial_baseline} of "
            f"{payment_data_dict[financial_baseline]}."
        )

    return {"result": False, "details": " ".join(details)}


def subcheck__product_is_on_white_list(
    entity: Mapping,
    table: Table,
    check_name: str,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Is the entity on the organization's product whitelist?
    """
    if not entity["rbf_category"]:
        return {
            "result": False,
            "details": f"Manufacturer ({entity['manufacturer']}) and model "
            f"({entity['model']}) combination not whitelisted in rbf_claim_templates.",
        }

    return {"result": True}


def subcheck__sale_is_not_older_than_three_months(
    entity: Mapping,
    table: Table,
    check_name: str | None = None,
    subsidy_adjustments: dict | None = None,
) -> SubcheckResultDict:
    """Check the truth of the following statement for a given uid...

    Did the sale of the entity take place within the last three months?

    Only sales as of 2024.11.01 (project launch date) are counted.
    """
    today = date.today()
    purchase_date = entity["purchase_date"]
    three_months_ago = today - relativedelta(months=3)
    launch_date = date(2024, 11, 1)

    if max(three_months_ago, launch_date) <= purchase_date <= today:
        return {"result": True}
    if today < purchase_date:
        return {
            "result": False,
            "details": f"The purchase date ({purchase_date}) "
            f"can't exceed the current date {today}.",
        }
    if purchase_date < launch_date:
        return {
            "result": False,
            "details": f"The purchase date ({purchase_date}) "
            f"can't preceed {launch_date}.",
        }
    return {
        "result": False,
        "details": f"Elapsed time since purchase date ({purchase_date}) "
        "exceeds three months.",
    }
