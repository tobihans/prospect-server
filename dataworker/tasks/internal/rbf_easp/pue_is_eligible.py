"""Calculate eligibility for the Uganda RBF EASP Program for PUE.

This task calculates eligibility, subsidy amount and incentive amount for Productive Use
of Energy (PUE) device sales according to the framework of the Energy Access Scale-up
Project (EASP). This result-based financing (RBF) program is conducted by the Uganda
Energy Credit Capitalisation Company (UECCC) and affects sales of 6 different
pre-approved categories of PUE devices.

Incentive calculation logic:
    base incentive
      -according to region
    additional incentive
      -for certain districts
      -for hard to reach districts (karamoja + islands)
      -for refugee hosting district (RHD) = 20000
    no incentive
      -for Kampala (achieved by subtracting Central region incentive)

Sources:
    data_meters
    data_payments_ts
    rbf_products
    rbf_product_prices
    rbf_claim_templates
    organization_group_members
    data_trust_trace

Writes to:
    data_trust_trace
"""

from dataworker import logger

from . import subchecks as subs
from .common import (
    METERS_TABLE,
    IncentiveDict,
    SubsidyAdjustmentsDict,
    build_verification_for_entities,
    get_technology,
)

# task identity
CHECK_NAME = "rbf_easp_pue_is_eligible"
task_name = CHECK_NAME

# incentive mapping for pue
INCENTIVES: IncentiveDict = {
    "region": {
        "central": 82000,
        "northern": 183000,
        "eastern": 183000,
        "western": 183000,
    },
    "district": {
        "kampala": {"amount": -82000, "type": "kampala"},
        "kalangala": {"amount": 173000, "type": "islands"},
        "buvuma": {"amount": 173000, "type": "islands"},
        "kaabong": {"amount": 72000, "type": "karamoja"},
        "karenga": {"amount": 72000, "type": "karamoja"},
        "kotido": {"amount": 72000, "type": "karamoja"},
        "abim": {"amount": 72000, "type": "karamoja"},
        "amudat": {"amount": 72000, "type": "karamoja"},
        "moroto": {"amount": 72000, "type": "karamoja"},
        "nabilatuk": {"amount": 72000, "type": "karamoja"},
        "nakapiripirit": {"amount": 72000, "type": "karamoja"},
        "napak": {"amount": 72000, "type": "karamoja"},
        "adjumani": {"amount": 20000, "type": "rhd"},
        "yumbe": {"amount": 20000, "type": "rhd"},
        "madi-okollo": {"amount": 20000, "type": "rhd"},
        "terego": {"amount": 20000, "type": "rhd"},
        "isingiro": {"amount": 20000, "type": "rhd"},
        "kikuube": {"amount": 20000, "type": "rhd"},
        "obongi": {"amount": 20000, "type": "rhd"},
        "kyegegwa": {"amount": 20000, "type": "rhd"},
        "kamwenge": {"amount": 20000, "type": "rhd"},
        "lamwo": {"amount": 20000, "type": "rhd"},
        "kiryandongo": {"amount": 20000, "type": "rhd"},
        "koboko": {"amount": 20000, "type": "rhd"},
    },
}

# subsidy adustments mapping for pue
SUBSIDY_ADJUSTMENTS: SubsidyAdjustmentsDict = {"female_ownership": 50000}

# subchecks applicable to pue
SUBCHECKS = [
    subs.subcheck__customer_data_is_complete,
    subs.subcheck__customer_data_is_unique,
    subs.subcheck__customer_is_eligible,
    subs.subcheck__payment_plan_is_complete,
    subs.subcheck__product_is_on_white_list,
    subs.subcheck__sale_is_not_older_than_three_months,
    subs.subcheck__entity_is_repossessed_at_most_once,
]

RBF_CONFIG = {
    "check_name": CHECK_NAME,
    "entities_table": METERS_TABLE,
    "incentives": INCENTIVES,
    "subchecks": SUBCHECKS,
    "subsidy_adjustments": SUBSIDY_ADJUSTMENTS,
    "data_append_funcs": get_technology,
}


def run() -> dict:
    """Entrypoint."""
    logger.info("verifying pue eligibilities for easp rbf...")
    return {"data_trust_trace": build_verification_for_entities(**RBF_CONFIG)}
