"""fuel_sale_wallet_matches_validated_payment Procssing Task.

Relies on the crm_payment_backed_by_payment_statement Processing Task.
"""

import datetime

from sqlalchemy import RowMapping, select

from dataworker import logger
from dataworker.database import get_table
from dataworker.queries import fetch_dict_data, fetch_scalars

PAYMENTS_TABLE = "data_payments_ts"
VERIFICATIONS_TABLE = "data_trust_trace"
PURCHASE_ITEM = "PELLETS"


def prune_payload(payment: RowMapping) -> dict:
    """Remove unneeded payload items."""
    p = dict(payment)
    for i in ["payment_external_id", "uid", "amount", "purchase_item", "paid_at"]:
        p.pop(i, None)
    return p


def build_trust_trace_context(uid: str) -> dict:
    """Build extra paylod items for API push to data_trust_trace."""
    return {
        "subject_uid": uid,
        "subject_origin": "payments_ts",
        "check": "fuel_sale_wallet_matches_validated_payment",
        "result": "ok",
    }


def is_verified_against_wallet(ordered_at: datetime.datetime, wallet: dict) -> bool:
    """Verify order by checking order and payment totals in wallet at time of order."""
    all_orders = wallet["orders"]
    all_payments = wallet["payments"]
    orders_sum = sum(i["amount"] for i in all_orders if i["paid_at"] <= ordered_at)
    payments_sum = sum(i["amount"] for i in all_payments if i["paid_at"] <= ordered_at)
    return payments_sum >= orders_sum


def construct_wallets(uids: list) -> list:
    """Construct list containing (verified) payments and orders from PAYMENTS_TABLE.

    List items are dictionaries grouped by account id, each representing a wallet for
    that specific account.
    """
    logger.info("constructing wallets from payment and order items.")
    payments = get_table(PAYMENTS_TABLE)

    corresponding_account_ids = fetch_scalars(
        select(payments.c.account_external_id).where(payments.c.uid.in_(uids))
    ).all()

    wallets = [
        {
            "payments": fetch_dict_data(
                select(
                    payments.c.payment_external_id,
                    payments.c.amount,
                    payments.c.paid_at,
                ).where(
                    payments.c.purpose == "payment", payments.c.account_external_id == i
                )
            ).all(),
            "orders": fetch_dict_data(
                select(
                    payments.c.uid,
                    payments.c.payment_external_id,
                    payments.c.amount,
                    payments.c.purchase_item,
                    payments.c.paid_at,
                    payments.c.organization_id,
                    payments.c.source_id,
                    payments.c.data_origin,
                    payments.c.import_id,
                ).where(
                    payments.c.purpose == "order", payments.c.account_external_id == i
                )
            ).all(),
        }
        for i in corresponding_account_ids
    ]
    return wallets


def fetch_statement_verified_uids() -> list:
    """Check VERIFICATIONS_TABLE for payments already with verification...

    where verification name is "crm_payment_backed_by_payment_statement"

    Return uids for later LOOKUP in PAYMENTS_TABLE.
    """
    logger.info("including only payments verified against mobile money statements.")
    verifications = get_table(VERIFICATIONS_TABLE)
    verified_uids = fetch_scalars(
        select(verifications.c.subject_uid).where(
            verifications.c.subject_origin == "payments_ts",
            verifications.c.check == "crm_payment_backed_by_payment_statement",
            verifications.c.result == "ok",
        )
    ).all()
    return verified_uids


def run() -> dict:
    """Entrypoint."""
    statemenet_verified_uids = fetch_statement_verified_uids()
    wallets = construct_wallets(statemenet_verified_uids)
    verified_orders = []
    for wallet in wallets:
        for order in wallet["orders"]:
            if order["purchase_item"] == PURCHASE_ITEM:
                if is_verified_against_wallet(order["paid_at"], wallet):
                    logger.info("verified: {}", order["payment_external_id"])
                    verified_orders.append(
                        prune_payload(order) | build_trust_trace_context(order["uid"])
                    )
                else:
                    logger.info("NOT verified: {}", order["payment_external_id"])
    return {"data_trust_trace": verified_orders}
