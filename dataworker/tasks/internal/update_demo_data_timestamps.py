"""Task module to update the demo data timestamps."""

import sqlalchemy as sqla

from dataworker import logger
from dataworker.database import get_or_create_connection

TABLES = [
    {"name": "data_meters_ts", "time_col": "metered_at", "id_col": "device_uid"},
    {"name": "data_shs_ts", "time_col": "metered_at", "id_col": "device_uid"},
    {"name": "data_grids_ts", "time_col": "metered_at", "id_col": "grid_uid"},
    {"name": "data_payments_ts", "time_col": "paid_at", "id_col": "account_uid"},
]


def create_query(table: str, time_col: str, id_col: str) -> str:
    """Return a query string from table and column name."""
    return f"""
    UPDATE {table}
    SET {time_col} = {time_col} + interval '1 day',
        {id_col} = {id_col} || '_tmp'
    WHERE organization_id IN (
      SELECT id
      FROM organizations
      WHERE name = 'Prospect Demo'
    );

    UPDATE {table}
    SET {id_col} = SUBSTR({id_col}, -4)
    WHERE organization_id IN (
      SELECT id
      FROM organizations
      WHERE name = 'Prospect Demo'
    );
  """  # noqa: S608


def run() -> None:
    """Task entry point to update the timestamps of the tables defined in TABLES."""
    conn = get_or_create_connection()
    logger.info("Updating hypertable timestamps in demo org")
    for tab in TABLES:
        query = create_query(tab["name"], tab["time_col"], tab["id_col"])
        conn.execute(sqla.text(query))
