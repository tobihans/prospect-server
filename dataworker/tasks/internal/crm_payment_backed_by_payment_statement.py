"""crm_payment_backed_by_payment_statement task.

Relies on the supamoto, airtel_zm and mtn_zm ingestion Tasks.

Informs the fuel_sale_wallet_matches_validated_payment task.
"""

from sqlalchemy import RowMapping, select

from dataworker import logger
from dataworker.database import get_table
from dataworker.queries import fetch_dict_data, fetch_scalar, fetch_scalars

PAYMENTS_TABLE = "data_payments_ts"
VERIFICATIONS_TABLE = "data_trust_trace"


def prune_payload(payment: RowMapping) -> dict:
    """Remove unneeded payload items."""
    p = dict(payment)
    for i in ["payment_external_id", "uid", "provider_name", "amount", "currency"]:
        p.pop(i, None)
    return p


def build_trust_trace_context(uid: str) -> dict:
    """Build extra paylod items for API push to data_trust_trace."""
    return {
        "subject_uid": uid,
        "subject_origin": "payments_ts",
        "check": "crm_payment_backed_by_payment_statement",
        "result": "ok",
    }


def is_verified_against_mm_statement(payment: RowMapping) -> bool:
    """Verify payment by matching against mobile money statement record."""
    payments = get_table(PAYMENTS_TABLE)
    matching_mobile_money_payment_id = fetch_scalar(
        select(1).where(
            payments.c.purpose == "mobile money statement",
            payments.c.provider_name == payment["provider_name"],
            payments.c.payment_external_id == payment["payment_external_id"],
            payments.c.amount == payment["amount"],
            payments.c.currency == payment["currency"],
        )
    )
    return bool(matching_mobile_money_payment_id)


def fetch_payments(excluded_uids: list) -> list:
    """Construct list containing payments from PAYMENTS_TABLE needing verification.

    Excludes those payments with uid matching any in excluded_uids.

    Returns list of payments for later INCLUSION in verification process.
    """
    logger.info("fetching payments needing verification.")
    payments = get_table(PAYMENTS_TABLE)
    unverified_payments = fetch_dict_data(
        select(
            payments.c.uid,
            payments.c.payment_external_id,
            payments.c.provider_name,
            payments.c.organization_id,
            payments.c.source_id,
            payments.c.data_origin,
            payments.c.import_id,
            payments.c.amount,
            payments.c.currency,
        ).where(payments.c.purpose == "payment", ~payments.c.uid.in_(excluded_uids))
    ).all()
    return unverified_payments


def fetch_statement_verified_uids() -> list:
    """Check VERIFICATIONS_TABLE for payments already with verification...

    where verification name is "crm_payment_backed_by_payment_statement"

    Return uids for later EXCLUSION from PAYMENTS_TABLE.
    """
    logger.info("excluding payments already verified against mobile money statements.")
    verifications = get_table(VERIFICATIONS_TABLE)
    verified_uids = fetch_scalars(
        select(verifications.c.subject_uid).where(
            verifications.c.subject_origin == "payments_ts",
            verifications.c.check == "crm_payment_backed_by_payment_statement",
            verifications.c.result == "ok",
        )
    ).all()
    return verified_uids


def run() -> dict:
    """Entrypoint."""
    uids_already_verified = fetch_statement_verified_uids()
    payments = fetch_payments(uids_already_verified)

    verified_payments = []
    for payment in payments:
        if is_verified_against_mm_statement(payment):
            logger.info("verified: {}", payment["payment_external_id"])
            verified_payments.append(
                prune_payload(payment) | build_trust_trace_context(payment["uid"])
            )
        else:
            logger.info("NOT verified: {}", payment["payment_external_id"])

    return {"data_trust_trace": verified_payments}
