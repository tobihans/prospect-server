"""Module for the paygo_accounts_snapshots task."""

from sqlalchemy import MappingResult, select
from sqlalchemy.sql.functions import max

from dataworker import logger
from dataworker.database import get_connection, get_table
from dataworker.types import DictDataset

from . import processing

PAYGO_TABLE = "data_paygo_accounts_snapshots_ts"
SHS_TABLE = "data_shs"
PAYMENTS_TS_TABLE = "data_payments_ts"
# we need some custom data for paygo metics
# and the data origin needs to set those
# therefore a hard list of data sources
# that we can create paygo metrics from
COMPATIBLE_DATA_ORIGINS = ["payg_ops"]


def get_shs_and_payment_data(org_id: str | None = None) -> list[MappingResult]:
    """Get shs, payment and latest snapshot data.

    Filter for compatible data origin.
    """
    connection = get_connection()
    shs = get_table(SHS_TABLE)
    payments = get_table(PAYMENTS_TS_TABLE)
    snapshots = get_table(PAYGO_TABLE)

    query = shs.select().where(shs.c.data_origin.in_(COMPATIBLE_DATA_ORIGINS))

    if org_id is not None:
        query = query.where(shs.c.organization_id == org_id)

    data_shs = connection.execute(query).mappings()

    query = payments.select().where(payments.c.data_origin.in_(COMPATIBLE_DATA_ORIGINS))

    if org_id is not None:
        query = query.where(payments.c.organization_id == org_id)

    data_payments_ts = connection.execute(query).mappings()

    query = select(
        max(snapshots.c.snapshot_date).label("latest_snapshot"),
        snapshots.c.account_external_id,
        snapshots.c.source_id,
        snapshots.c.organization_id,
    ).group_by(
        snapshots.c.organization_id,
        snapshots.c.source_id,
        snapshots.c.account_external_id,
    )

    if org_id is not None:
        query = query.where(snapshots.c.organization_id == org_id)

    data_snapshot_latest = connection.execute(query).mappings()

    return [data_shs, data_payments_ts, data_snapshot_latest]


def _run(org_id: str | None = None) -> DictDataset:
    """Entry point for the paygo_accounts_snapshots task module."""
    data_shs, data_payments_ts, data_snapshot_latest = get_shs_and_payment_data(org_id)

    # Check if any of the datasets are empty. If so, exit processing and return nothing.
    empty_mappings = [
        name
        for name, data in [
            ("data_shs", data_shs),
            ("data_payments_ts", data_payments_ts),
        ]
        if not any(data)
    ]
    if empty_mappings:
        logger.info("The following dataset(s) is empty: {}.", ", ".join(empty_mappings))
        return {}

    snapshots = processing.create_paygo_accounts_snapshots(
        data_shs, data_payments_ts, data_snapshot_latest
    )
    return {"data_paygo_accounts_snapshots_ts": snapshots}


def run_for_org(org_id: str) -> DictDataset:
    """Get the paygo account data for a particular org.

    This code is not currently used in production.
    """
    return run(org_id)


def run() -> DictDataset:
    """No arg version to respect constraints of task creation."""
    return _run()
