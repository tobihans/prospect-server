"""Paygo Accounts Snapshots Processor.

For data fetched directly from the PaygOps API the data is filtered to contain only
Loans since that is the only contract type that allows for the creation of
paygo accounts snapshots.

shs and payments_ts raw data is filtered by checking for specific columns required
for the snapshot calculation.
"""

import operator
from datetime import date, timedelta

import pandas as pd
from sqlalchemy import MappingResult

from dataworker import logger


def create_paygo_accounts_snapshots(
    in_data_shs: list[dict] | MappingResult,
    in_data_payments_ts: list[dict] | MappingResult,
    in_data_snapshot_latest: list[dict] | MappingResult,
) -> list[dict]:
    """Create entry for each day a contract exists and calculate snapshots."""
    # Dict to DataFrame.
    shs_raw = pd.DataFrame(in_data_shs)
    payments_ts_raw = pd.DataFrame(in_data_payments_ts)
    latest_snapshot_df = pd.DataFrame(in_data_snapshot_latest)

    # filter data for PaygOPs by shs.type = "Loan"
    if (shs_raw["data_origin"] == "payg_ops").all():
        shs_raw = shs_raw.loc[shs_raw.payment_plan_type == "Loan"]

    # check in every row if custom fields has
    # information of time_given_at_start_in_days
    shs_val_found = []
    for row in shs_raw.itertuples():
        if "time_given_at_start_in_days" in row.custom:
            shs_val_found.append(True)
        else:
            shs_val_found.append(False)
    # drop rows where information was not found
    shs = shs_raw[shs_val_found].copy()  # Suppresses SettingWithCopyWarning

    # check in every row if custom fields has
    # information of credit_value and type
    paym_val_found = []
    for row in payments_ts_raw.itertuples():
        if ("credit_value" in row.custom) and ("type" in row.custom):
            paym_val_found.append(True)
        else:
            paym_val_found.append(False)
    # drop rows where information was not found
    # Suppresses SettingWithCopyWarning...
    payments_ts = payments_ts_raw[paym_val_found].copy()
    if (len(shs) == 0) or (len(payments_ts) == 0):
        return []

    # extract all necessary custom fields
    shs["time_given_at_start_in_days"] = shs["custom"].apply(
        operator.itemgetter("time_given_at_start_in_days")
    )
    payments_ts["credit_value"] = payments_ts["custom"].apply(
        operator.itemgetter("credit_value")
    )
    payments_ts["type"] = payments_ts["custom"].apply(operator.itemgetter("type"))
    payments_ts = payments_ts.drop(columns=["data_origin"])
    # merge two data frames
    position_raw = payments_ts.merge(
        shs, on=["organization_id", "source_id", "account_external_id"], how="inner"
    )
    # convert date
    position_raw["snapshot_date"] = pd.to_datetime(position_raw["paid_at"]).dt.date
    # create continous list of days for each contract from respective min-max date
    # find min snapshot_date per account_external_id
    min_per_id = (
        position_raw.groupby(["source_id", "account_external_id"])
        .agg(min=("snapshot_date", "min"))
        .to_dict()["min"]
    )
    # empty dataframe
    cart_prod = pd.DataFrame(
        columns=["snapshot_date", "source_id", "account_external_id"]
    )
    # loop through account_external_id and start dates
    # create all snapshot dates in between start and today
    for (s_id, id), start in min_per_id.items():
        tmp_df = pd.DataFrame(
            columns=["snapshot_date", "source_id", "account_external_id"]
        )
        tmp_df["snapshot_date"] = pd.date_range(start, date.today() - timedelta(1))
        tmp_df["account_external_id"] = id
        tmp_df["source_id"] = s_id
        cart_prod = pd.concat([cart_prod.astype(tmp_df.dtypes), tmp_df])
    position_raw.snapshot_date = pd.to_datetime(position_raw.snapshot_date)
    # merge
    position_daily = position_raw.merge(
        cart_prod, on=["source_id", "account_external_id", "snapshot_date"], how="outer"
    ).sort_values(by=["source_id", "account_external_id", "snapshot_date", "paid_at"])
    # DAYS_TO_CUTOFF
    position_daily["credit_value"] = pd.to_numeric(position_daily["credit_value"])
    position_daily.loc[position_daily.type == "Manual Delay", "credit_value"] = (
        position_daily["credit_value"].astype(float) / (24)
    )
    # CUMULATIVE PAID
    # sum per group
    position_daily["amount"] = pd.to_numeric(position_daily["amount"])
    position_daily["amount_agg"] = position_daily.groupby([
        "source_id",
        "account_external_id",
        "snapshot_date",
    ])["amount"].transform(pd.Series.sum)
    # sum per group
    position_daily["days_to_cutoff_agg"] = position_daily.groupby([
        "source_id",
        "account_external_id",
        "snapshot_date",
    ])["credit_value"].transform(pd.Series.sum)
    # remove duplicate snapshot_date -- only entry per day
    position_daily = position_daily.drop_duplicates(
        subset=["source_id", "account_external_id", "snapshot_date"]
    )
    position_daily["cumulative_paid_amount"] = position_daily.groupby([
        "source_id",
        "account_external_id",
    ])["amount_agg"].transform(pd.Series.cumsum)
    # replace 0 by -1
    position_daily["days_to_cutoff_agg_tmp"] = (
        position_daily.days_to_cutoff_agg.replace(0, -1)
    )
    # fake data
    position_daily.loc[
        position_daily.days_to_cutoff_agg_tmp != -1, "days_to_cutoff_agg_tmp"
    ] = position_daily["days_to_cutoff_agg_tmp"] - 1
    # cummsum per group
    position_daily["days_to_cutoff_agg_cums"] = position_daily.groupby([
        "source_id",
        "account_external_id",
    ])["days_to_cutoff_agg_tmp"].cumsum()
    # fake data
    position_daily["days_to_cutoff_agg_cums"] = (
        position_daily["days_to_cutoff_agg_cums"] + 1
    )
    # position_daily.drop(["days_to_cutoff"], axis=1, inplace=True)
    position_daily = position_daily.rename(
        {"days_to_cutoff_agg_cums": "days_to_cutoff"}, axis=1
    )
    # ACCOUNT_STATUS
    position_daily.loc[position_daily["days_to_cutoff"] >= 0, "account_status"] = (
        "active"
    )
    position_daily.loc[position_daily["days_to_cutoff"] < 0, "account_status"] = (
        "locked"
    )
    # forward fill Cumulative Paid and account_status
    cols = [
        "time_given_at_start_in_days",
        "data_origin",
        "source_id",
        "organization_id",
        "paid_off_date",
        "repossession_date",
    ]
    for col in cols:
        position_daily[col] = position_daily.groupby([
            "source_id",
            "account_external_id",
        ])[col].transform(lambda x: x.ffill())
    # paid_off_date data preparation
    # add account_status Completed if paid_off date exists
    position_daily.loc[
        position_daily["snapshot_date"] >= position_daily["paid_off_date"],
        "account_status",
    ] = "completed"
    # remove rows for contract if account_status = "Completed"
    position_daily = position_daily.drop(
        position_daily[
            position_daily.snapshot_date > position_daily.paid_off_date
        ].index
    )
    # convert float to int
    cols = [
        "time_given_at_start_in_days",
        "source_id",
        "organization_id",
    ]
    for col in cols:
        # select and group column.
        col_grouped = position_daily.groupby([
            "source_id",
            "account_external_id",
        ])[col]
        # perform conversion on selected and grouped column.
        # initial float conversion covers case of "string of a float" (e.g. '365.0').
        position_daily[col] = col_grouped.transform(
            lambda x: x.astype(float).astype(int)
        )

    # DOB_POST_UPFRONT
    position_daily["days_in_repayment_tmp"] = position_daily[
        "time_given_at_start_in_days"
    ] * (-1)
    # create temp column with row count per group (account_external_id)
    position_daily["row_count"] = position_daily.groupby([
        "source_id",
        "account_external_id",
    ]).cumcount()
    position_daily["days_in_repayment"] = (
        position_daily["row_count"] + position_daily["days_in_repayment_tmp"]
    )
    # cumulative_days_locked
    # separate dataframe into locked and everything else
    position_daily_dis = position_daily.copy()
    position_daily_dis = position_daily_dis[
        position_daily_dis.account_status == "locked"
    ]
    # count number of occurrence of locked and cumsum, shift one row to start with 0
    position_daily_dis["cumulative_days_locked"] = (
        position_daily_dis.groupby([
            "source_id",
            "account_external_id",
        ]).account_status.transform("cumcount")
        + 1
    )
    position_daily_else = position_daily.copy()
    position_daily_else = position_daily_else[
        position_daily_else.account_status != "locked"
    ]
    # concatenate two dataframes back together
    position_daily = pd.concat([position_daily_dis, position_daily_else]).sort_values(
        by=["source_id", "account_external_id", "snapshot_date"]
    )
    # forward fill cumulative_days_locked and fill nan with 0
    cols = ["cumulative_days_locked"]
    position_daily["cumulative_days_locked"] = (
        position_daily.groupby(["source_id", "account_external_id"])[cols]
        .ffill()
        .fillna(0)
        .astype(int)
    )
    # forward fill Cumulative Paid and account_status
    cols = [
        "payment_plan_amount_financed",
        "payment_plan_down_payment",
    ]
    for col in cols:
        position_daily[col] = position_daily.groupby([
            "source_id",
            "account_external_id",
        ])[col].transform(lambda x: x.ffill())

    # total - downpayment
    position_daily["total_remaining_amount"] = (
        position_daily["payment_plan_amount_financed"]
        + position_daily["payment_plan_down_payment"]
        - position_daily["cumulative_paid_amount"]
    )

    # inform for which ids "total_remaining_amount" is coming up as nan, and why.
    faulty_accounts = position_daily[position_daily["total_remaining_amount"].isna()]
    for _, account in faulty_accounts.iterrows():
        amount_financed_is_null = pd.isna(account["payment_plan_amount_financed"])
        down_payment_is_null = pd.isna(account["payment_plan_down_payment"])
        if amount_financed_is_null and down_payment_is_null:
            message = "amount_financed and down_payment are missing"
        elif amount_financed_is_null:
            message = "amount_financed is missing"
        elif down_payment_is_null:
            message = "down_payment is missing"
        else:
            message = "could not calculate total_remaining_amount"

        logger.info(
            "{} for account_accout_external_id: {}.",
            message,
            account["account_external_id"],
        )

    # remove old snapshots
    if not latest_snapshot_df.empty:
        position_daily = position_daily.merge(
            latest_snapshot_df,
            on=["account_external_id", "source_id", "organization_id"],
            how="outer",
        )
        position_daily = position_daily.drop(
            position_daily[
                position_daily.snapshot_date <= position_daily.latest_snapshot
            ].index
        )

    position_daily = position_daily[
        [
            "account_external_id",
            "snapshot_date",
            "account_status",
            "days_in_repayment",
            "cumulative_paid_amount",
            "days_to_cutoff",
            "cumulative_days_locked",
            "organization_id",
            "data_origin",
            "source_id",
            "total_remaining_amount",
        ]
    ]
    # Convert back to dict
    out_dict = position_daily.to_dict("records")
    # print(out_dict)
    return out_dict


def process(
    in_data_shs: list[dict],
    in_data_payments_ts: list[dict],
    in_data_snapshot_latest: list[dict],
) -> list[dict]:
    """Run paygo accounts snapshots processor on payments timeseries data."""
    logger.info(
        "Running paygo accounts snapshots processor on payments timeseries data"
    )
    return {
        "data_paygo_accounts_snapshots": create_paygo_accounts_snapshots(
            in_data_shs=in_data_shs,
            in_data_payments_ts=in_data_payments_ts,
            in_data_snapshot_latest=in_data_snapshot_latest,
        )
    }
