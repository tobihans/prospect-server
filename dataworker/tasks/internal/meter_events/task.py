"""Module for the meter_events task."""

from collections.abc import Generator, Mapping, Sequence
from datetime import datetime, timedelta

from sqlalchemy import RowMapping, func, select

from dataworker import logger
from dataworker.database import get_connection, get_table
from dataworker.dataset import RecordSet
from dataworker.queries import fetch_dict_data

from . import processing

EVENTS_TABLE = "data_meter_events_ts"
METERS_TABLE = "data_meters"
METERS_TS_TABLE = "data_meters_ts"


def get_meter_ts_data_since(uid: str, range_start: datetime | None) -> list[dict]:
    """Get meter data since the desired datetime range start.

    From METERS_TS_TABLE, filtered by a given timestamp representing
    the left limit (exclusive '>') for the desired datetime range.

    If no valid range_start is given, then do not filter by datetime range (fetch all).
    """
    logger.info(
        "fetching timeseries data for meter {} {}",
        uid,
        "" if range_start is None else f"metered after {range_start}",
    )
    meters_ts = get_table(METERS_TS_TABLE)
    columns = [
        "metered_at",
        "device_uid",
        "serial_number",
        "energy_lifetime_wh",
    ]
    statement = select(meters_ts.c[*columns]).where(meters_ts.c.device_uid == uid)
    if range_start:
        statement = statement.where(meters_ts.c.metered_at > range_start)
    return fetch_dict_data(statement).all()


def get_last_event_end(uid: str) -> datetime | None:
    """Get last event to know the start timestamp for the next run of the processor.

    For a given meter, get the timestamp of the end of the last occuring event
    currently recorded in EVENTS_TABLE for that meter.
    """
    logger.info("fetching end time of last recorded event for meter {}", uid)
    connection = get_connection()
    events = get_table(EVENTS_TABLE)
    statement = select(func.max(events.c.end_at)).where(events.c.device_uid == uid)
    return connection.scalar(statement)


def generate_meter_events(meters: list[Mapping]) -> Generator[dict, None, None]:
    """For each meter, perform steps to extract meter events for EVENTS_TABLE.

    Also add meter context values for each meter to complete entry for DB.
    """
    for meter in meters:
        uid = meter["device_uid"]
        primary_use = meter["primary_use"]
        context = {
            key: meter[key] for key in ("organization_id", "source_id", "data_origin")
        }

        range_start = get_last_event_end(uid)
        samples = get_meter_ts_data_since(uid, range_start)
        events = processing.process(samples, primary_use)

        for event in events:
            yield event | context


def meter_is_active(uid: str) -> bool:
    """Check METERS_TS_TABLE for meter activity within the last month and return True.

    If no such activity exists, return False.
    """
    meters_ts = get_table(METERS_TS_TABLE)
    connection = get_connection()
    last_month = datetime.now() - timedelta(days=30)
    result = connection.execute(
        select(1)
        .where(meters_ts.c.device_uid == uid, meters_ts.c.metered_at >= last_month)
        .limit(1)
    ).scalar()
    return bool(result)


def get_applianced_meters() -> Sequence[RowMapping]:
    """Get set of id/appliance values used to trigger events processing.

    We check for the existance of a non-null value in the column "primary_use" in
    METERS_TABLE and match it against the keys in PARAMETERS defined in the processing
    module. If the value is null or does not match no such appliance is attached and no
    need to process events.

    For positive instances, also return the primary_use value itself in order to select
    the appropriate processor parameters.

    For example, 'cooking' and 'milling' -- two possible values for primary_use --
    require different parameters when running the events extraction processor. The
    value returned from this function is passed to the processor's main 'process()'
    function, where parameter selection occurs there based on the value.
    """
    logger.info("identifying meters with connected appliances")
    meters = get_table(METERS_TABLE)
    valid_appliances = list(processing.PARAMETERS.keys())
    context_fields = ["organization_id", "source_id", "data_origin"]
    return fetch_dict_data(
        select(
            meters.c.device_uid, meters.c.primary_use, meters.c[*context_fields]
        ).where(meters.c.primary_use.in_(valid_appliances))
    ).all()


def run() -> RecordSet:
    """Entrypoint for the meter_events task module."""
    meters = get_applianced_meters()
    active_meters = [m for m in meters if meter_is_active(m["device_uid"])]
    meter_events = list(generate_meter_events(active_meters))
    return {"data_meter_events_ts": meter_events}
