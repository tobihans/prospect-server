"""Meter Events Processor."""

from datetime import timedelta

import pandas as pd

from dataworker import logger

# --- PARAMETERS ---
#
# max_energy_step_kwh
#   The difference between subsequent lifetimeEnergy values (unaccounted energy) that
#   will still be grouped into events. Any unaccounted energy differences larger than
#   the limit are not included into events.
#
# cutoff_time_sec
#   The time to determine which events get grouped and which don't. Needs to bE adjusted
#   most often.
#
# category
#   Type of event, used for filtering similar appliances.
#
PARAMETERS = {
    "milling": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 10,
        "min_event_duration_sec": 60,
        "min_power_w": 100,
        "max_power_w": 5000,
        "cutoff_time_sec": 360,
        "category": "productive_use",
    },
    "epc_1kw": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 20,
        "min_event_duration_sec": 120,
        "min_power_w": 100,
        "max_power_w": 3000,
        "cutoff_time_sec": 1080,
        "category": "cooking",
    },
    "epc_3kw": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 20,
        "min_event_duration_sec": 120,
        "min_power_w": 100,
        "max_power_w": 5000,
        "cutoff_time_sec": 1500,
        "category": "cooking",
    },
    "epc_4kw": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 50,
        "min_event_duration_sec": 120,
        "min_power_w": 400,
        "max_power_w": 6000,
        "cutoff_time_sec": 1080,
        "category": "cooking",
    },
    "freezer": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 50,
        "min_event_duration_sec": 120,
        "min_power_w": 20,
        "max_power_w": 2000,
        "cutoff_time_sec": 10800,
        "category": "cold_chain",
    },
    "popcorn_machine": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 20,
        "min_event_duration_sec": 60,
        "min_power_w": 20,
        "max_power_w": 5000,
        "cutoff_time_sec": 360,
        "category": "productive_use",
    },
    "water_boiler_7kw": {
        "max_energy_step_wh": 800,
        "min_event_energy_wh": 50,
        "min_event_duration_sec": 60,
        "min_power_w": 2500,
        "max_power_w": 10000,
        "cutoff_time_sec": 360,
        "category": "cooking",
    },
    "water_boiler_12kw": {
        "max_energy_step_wh": 800,
        "min_event_energy_wh": 50,
        "min_event_duration_sec": 60,
        "min_power_w": 6000,
        "max_power_w": 14000,
        "cutoff_time_sec": 360,
        "category": "cooking",
    },
    "induction_stove_1kw": {
        "max_energy_step_wh": 300,
        "min_event_energy_wh": 20,
        "min_event_duration_sec": 120,
        "min_power_w": 100,
        "max_power_w": 3000,
        "cutoff_time_sec": 300,
        "category": "cooking",
    },
}


def filter_events(
    in_df: pd.DataFrame,
    min_event_energy_wh: int,
    min_event_duration_sec: int,
    min_power_w: int,
    max_power_w: int,
) -> pd.DataFrame:
    """Only include events who meet certain criteria."""
    filter_1 = in_df["energy_consumed_wh"] > min_event_energy_wh
    filter_2 = in_df["duration_sec"] > min_event_duration_sec
    filter_3 = in_df["power"] >= min_power_w
    filter_4 = in_df["power"] <= max_power_w
    out_df = in_df.loc[(filter_1 | filter_2) & (filter_3 & filter_4)]
    return out_df


def correct(df_in: pd.DataFrame, limit: int) -> pd.DataFrame:
    """Make final meter_events corrective adjustments.

    Due to the way the meters  function and are used in the field, there is often energy
    that occurs between the meter's save points. This is because the meters are
    continuously counting energy but only periodically save datapoints.

    When a meter is powered off (very common in cooking projects), the meter will have
    counted the energy used until it was powered off but may not have saved the count as
    a datapoint. When the meter is powered back on, it resumes counting from where it
    left off but still may take several minutes to save its count as a new datapoint.
    Thus the energy count is accurate, but the timing of the usage is unclear.

    Example:
    An appliance uses 600W of continuous power and is connected to a meter that saves
    data every 10 mintues.

    9:00 meter count is 200.00 kWh. The meter saves this datapoint.
    9:06 meter count is 200.06 kWh. The meter is powered off.
    16:00:01 meter is reconnected to power. The meter count resumes at 200.06 kWh.
    16:10 meter count is 200.16 kWh. The meter saves this datapoint.

    The datapoints will read:

    9:00: 200.00 kWh
    16:10: 200.16 kWh

    Thus the data shows an energy increase of 0.16 happening sometime during a 7 hour
    period. When processing this example as an event, one event would end at 9:00 and
    200.00 kWh (the last recorded datapoint before a long gap) and the next event would
    start at 16:10 and 200.16kWh (the first recorded datapoint after a long gap). This
    script corrects this by adding the 0.16 kWh of unaccounted energy back into the
    events. It also adjusts the times accordingly.

    The unaccounted energy is split evenly between the preceding and following events

    limit: energy gaps larger than this will be ignored and not corrected
    """
    events = df_in.copy()

    # Find the difference between the energy recorded at the end of the event and the
    # energy recorded at the start of the next
    events["unaccounted_energy"] = (
        events["energy_start_wh"].shift(-1) - events["energy_end_wh"]
    )
    events.loc[events["unaccounted_energy"] > limit, "unaccounted_energy"] = 0
    events["unaccounted_energy"] = events["unaccounted_energy"].fillna(0)

    # Add a column for calculating the power in each event
    events["power"] = events["energy_consumed_wh"] / (events["duration_sec"] / 60 / 60)

    # DO.
    events["energy_end_wh"] = events["energy_end_wh"] + (
        events["unaccounted_energy"] / 2
    )
    events["energy_start_wh"] = events["energy_start_wh"] - (
        events["unaccounted_energy"].shift(1, fill_value=0) / 2
    )

    events["end_at"] = events["end_at"] + (
        ((events["unaccounted_energy"] / 2) / events["power"]) * timedelta(hours=1)
    )

    events["start_at"] = events["start_at"] - (
        ((events["unaccounted_energy"].shift(1, fill_value=0) / 2) / events["power"])
        * timedelta(hours=1)
    )

    events["energy_consumed_wh"] = events["energy_end_wh"] - events["energy_start_wh"]
    events["start_at"] = events["start_at"].dt.round("min")
    events["end_at"] = events["end_at"].dt.round("min")
    events["duration_sec"] = (events["end_at"] - events["start_at"]) / timedelta(
        seconds=1
    )

    return events


def add_metrics(df_events: pd.DataFrame) -> pd.DataFrame:
    """Calculate additional metrics."""
    df_events["energy_consumed_wh"] = (
        df_events["energy_end_wh"] - df_events["energy_start_wh"]
    )
    df_events["duration_sec"] = (
        df_events["end_at"] - df_events["start_at"]
    ).dt.total_seconds()
    return df_events


def remove_end_of_day_events(df_events: pd.DataFrame) -> pd.DataFrame:
    """Remove events within threshhold of max recorded time.

    In case end_at is within 15 minutes of max recorded datetime of dataset.
    """
    idx = df_events["device_uid"].copy()
    reduced_df = df_events.groupby(idx, as_index=False).apply(
        lambda t: t[(t["max_time"] - t["end_at"]).dt.total_seconds() > 900],  # noqa: FURB118,RUF100
    )
    reduced_df = reduced_df.drop(["max_time"], axis=1)
    return reduced_df


def construct_events(df: pd.DataFrame, gap: int) -> pd.DataFrame:
    """Extract events from a dataframe of processed and clustered timeseries data.

    gap: int, defines timestamp difference (in sec) used to locate event boundaries.
    """
    # BACKWARD time difference.
    # time_diff_b = time[n] - time[n-1] (seconds).
    # Start of event signaled if time_diff_b > gap.
    df["time_diff_b"] = (
        df.t_min - df.t_max.shift(1, fill_value=df.t_min.min() - timedelta(minutes=999))
    ) / timedelta(seconds=1)

    # FORWARD time difference.
    # time_diff_f = time[n+1] - time[n] (seconds).
    # End of event signaled if time_diff_f > gap.
    df["time_diff_f"] = (
        df.t_min.shift(-1, fill_value=df.t_max.max() + timedelta(minutes=999))
        - df.t_max
    ) / timedelta(seconds=1)

    df["duration_sec"] = ((df.t_max - df.t_min) / timedelta(seconds=1)).astype(int)

    # Get aggregates.
    df_agg = df.groupby(
        ["device_uid", "max_time", "energy_lifetime_wh"], as_index=False
    ).aggregate(
        t_min=("t_min", "min"),
        t_max=("t_max", "max"),
        time_diff_b=("time_diff_b", "sum"),
        time_diff_f=("time_diff_f", "sum"),
        duration_sec=("duration_sec", "sum"),
    )

    # Event starts.
    df_event_start = df_agg.loc[
        ((df_agg["time_diff_b"] >= gap) & (df_agg["time_diff_f"] < gap))
        | (
            (df_agg["time_diff_b"] >= gap)
            & (df_agg["time_diff_f"] >= gap)
            & (df_agg["time_diff_b"].shift(-1) < gap)
        )
        | ((df_agg["duration_sec"] >= gap) & (df_agg["time_diff_b"].shift(-1) < gap))
        | (
            ((df_agg["time_diff_b"] + df_agg["duration_sec"]) >= gap)
            & ((df_agg["time_diff_f"] + df_agg["duration_sec"]) >= gap)
            & (df_agg["time_diff_b"].shift(-1) < gap)
        )
    ].loc[:, ["device_uid", "max_time", "t_max", "energy_lifetime_wh"]]

    df_event_start = df_event_start.rename(
        columns={
            "t_max": "start_at",
            "energy_lifetime_wh": "energy_start_wh",
        }
    )

    # Event ends.
    df_event_end = df_agg.loc[
        ((df_agg["time_diff_b"] < gap) & (df_agg["time_diff_f"] >= gap))
        | (
            (df_agg["time_diff_b"] >= gap)
            & (df_agg["time_diff_f"] >= gap)
            & (df_agg["time_diff_f"].shift(1) < gap)
        )
        | ((df_agg["duration_sec"] >= gap) & (df_agg["time_diff_f"].shift(1) < gap))
        | (
            ((df_agg["time_diff_b"] + df_agg["duration_sec"]) >= gap)
            & ((df_agg["time_diff_f"] + df_agg["duration_sec"]) >= gap)
            & (df_agg["time_diff_f"].shift(1) < gap)
        )
    ].loc[:, ["t_min", "energy_lifetime_wh"]]

    df_event_end = df_event_end.rename(
        columns={
            "t_min": "end_at",
            "energy_lifetime_wh": "energy_end_wh",
        }
    )

    # Construct events df.
    df_events = pd.concat(
        [df_event_start.reset_index(drop=True), df_event_end.reset_index(drop=True)],
        axis=1,
    ).dropna()

    df_events = remove_end_of_day_events(df_events)

    return df_events


def cluster_energies(df_in: pd.DataFrame) -> pd.DataFrame:
    """Get times of first and last occurence of consecutive identical energy values."""
    df_in["grouping"] = df_in["energy_lifetime_wh"].diff().ne(0).cumsum()
    df_in = df_in.groupby(
        ["device_uid", "max_time", "grouping", "energy_lifetime_wh"], as_index=False
    ).aggregate(
        t_min=("metered_at" if "metered_at" in df_in else "t_min", "min"),
        t_max=("metered_at" if "metered_at" in df_in else "t_max", "max"),
    )
    # Drop the first row (to avoid having any data forward filled with datapoint from
    # preceding meter).
    df_in = df_in.tail(-1)

    # Find the problem pairs.
    first_of_pairs = df_in.loc[
        df_in["energy_lifetime_wh"] > df_in["energy_lifetime_wh"].shift(-1)
    ]
    second_of_pairs = df_in.loc[
        df_in["energy_lifetime_wh"].shift(1) > df_in["energy_lifetime_wh"]
    ]
    # Drop the problem pair rows.
    df_out = df_in.drop(
        df_in.loc[
            (df_in.index.isin(first_of_pairs.index))
            | (df_in.index.isin(second_of_pairs.index))
        ].index
    )

    df_out[["t_min", "t_max"]] = df_in[["t_min", "t_max"]].apply(pd.to_datetime)
    return df_out


def fill_missing_values(in_df: pd.DataFrame, first_valid_index: int) -> pd.DataFrame:
    """Apply forward fill starting from first valid (non-None) index."""
    df_filled = in_df.ffill().iloc[first_valid_index:].reset_index(drop=True)
    return df_filled


def extract_events(
    in_data: list[dict],
    max_energy_step_wh: int,
    min_event_energy_wh: int,
    min_event_duration_sec: int,
    min_power_w: int,
    max_power_w: int,
    cutoff_time_sec: int,
    category: str,
) -> list[dict]:
    """Apply Pandas transformations to extract events."""
    # Dict to DataFrame.
    df_in = pd.DataFrame(in_data)[
        ["device_uid", "serial_number", "metered_at", "energy_lifetime_wh"]
    ]

    # Create mapper for serial_number -> device_uid for later
    mapper = df_in.groupby("device_uid")["serial_number"].first().to_dict()

    # Get max recorded datetime of dataset.
    # Used by the Task module to determine from which timepoint to process new data.
    df_in["max_time"] = pd.to_datetime(
        df_in.groupby("device_uid")["metered_at"].transform("max")
    )

    # Process
    # order by meter datetime.
    df_reordered = df_in.sort_values(by=["metered_at"]).reset_index(drop=True)

    # identify the first index where 'energy_lifetime_wh' is not None (or not NaN).
    first_valid_index = df_reordered["energy_lifetime_wh"].first_valid_index()
    # If there is no valid data, return an empty list.
    # This catches the erreneous/buggy case where the entire sample payload coming in
    # has no non-null values for energy_lifetime_wh whatsoever.
    if first_valid_index is None:
        logger.info("No new data to process for this meter.")
        return []

    # This method will allow for initial None values followed by good values, and
    # prevent a certain meter form erroring infinitely just because the first value is
    # None.
    df_filled = fill_missing_values(df_reordered, first_valid_index)

    # Cluster
    df_clustered = cluster_energies(df_filled)

    # Construct Events
    df_events = construct_events(df_clustered, cutoff_time_sec)

    df_events = add_metrics(df_events)

    # Finetune
    df_events = correct(df_events, max_energy_step_wh)

    # Handle the datetimes
    df_events["start_at"] = df_events["start_at"].dt.strftime("%Y-%m-%dT%H:%M:%SZ")
    df_events["end_at"] = df_events["end_at"].dt.strftime("%Y-%m-%dT%H:%M:%SZ")

    # Add Category parameter
    df_events["category"] = category

    # Filter events
    df_events = filter_events(
        df_events,
        min_event_energy_wh,
        min_event_duration_sec,
        min_power_w,
        max_power_w,
    )
    df_events = df_events.drop(columns=["unaccounted_energy", "power"]).dropna()

    # Add Serial Number from mapper
    df_events["serial_number"] = df_events["device_uid"].map(mapper, na_action="ignore")

    # Convert back to dict
    out_dict = df_events.to_dict("records")

    return out_dict


def process(in_data: list[dict], primary_use: str) -> list[dict]:
    """Run the meter events processor for one meter from METERS_TS_TABLE.

    in_data: list of dicts, where each item in each dict represents one datapoint for
    one meter from METERS_TS_TABLE table.

    primary_use: string (e.g. 'cooking', 'milling') used to map to parameters for
    processor.

    Output (list of dicts) is ready to be written to EVENTS_TABLE in datawarehouse.
    """
    logger.info("Running meter events processor on meter timeseries data")
    if primary_use not in PARAMETERS:
        logger.warning("Specified primary use not known: {}.", primary_use)
        return []

    if len(in_data) <= 1:
        logger.info("No new data to process for this meter.")
        return []

    return extract_events(in_data=in_data, **PARAMETERS[primary_use])
