![Dataworker coverage](https://gitlab.com/prospect-energy/prospect-server/badges/main/coverage.svg?job=dataworker-tests&key_text=Dataworker+Coverage&key_width=130)

# Dataworker

## Important links

- [Prospect Wiki - Dataworker Home page](https://gitlab.com/prospect-energy/prospect-server/-/wikis/Development/DataWorker-Home)
- [Prospect Wiki - Dataworker Development workflow](https://gitlab.com/prospect-energy/prospect-server/-/wikis/Development/Dataworker-Development)
- [Prospect Gitlab Repo - Dataworker Issue tracker](https://gitlab.com/prospect-energy/prospect-server/-/boards/5718325?label_name%5B%5D=dataworker)

## Table of contents

- [Dataworker](#dataworker)
  - [Important links](#important-links)
  - [Table of contents](#table-of-contents)
  - [Architecture](#architecture)
    - [Code](#code)
    - [Definitions](#definitions)
  - [Installation](#installation)
    - [Pre-requirements](#pre-requirements)
      - [Python version and project management](#python-version-and-project-management)
      - [Python project management (mandatory)](#python-project-management-mandatory)
      - [Storage CLI (optional)](#storage-cli-optional)
      - [Database CLI (optional)](#database-cli-optional)
    - [Installing the project](#installing-the-project)
    - [Installing pre-commit hooks](#installing-pre-commit-hooks)
  - [Operation](#operation)
    - [Running python code directly](#running-python-code-directly)
    - [Command line tool](#command-line-tool)
  - [Configuration](#configuration)
    - [In python code](#in-python-code)
    - [Configuration sources](#configuration-sources)
      - [Files](#files)
      - [Environment](#environment)
    - [Workflow](#workflow)
  - [Using the various development setups](#using-the-various-development-setups)
    - [Simple script](#simple-script)
    - [With services](#with-services)
    - [Running a local worker](#running-a-local-worker)
    - [Containerized](#containerized)
  - [Testing](#testing)
  - [Linting and formatting](#linting-and-formatting)
  - [Development](#development)
    - [Developing new tasks](#developing-new-tasks)
      - [Ingestion Tasks](#ingestion-tasks)
    - [Howtos](#howtos)
      - [Using the logger](#using-the-logger)
      - [Configuring logging](#configuring-logging)
      - [Accessing the config](#accessing-the-config)
      - [Using a database connection and table objects](#using-a-database-connection-and-table-objects)
      - [Copying files from operational storage to local storage](#copying-files-from-operational-storage-to-local-storage)
  - [Configuration using environment variables](#configuration-using-environment-variables)
    - [Worker: python faktory client](#worker-python-faktory-client)
    - [Job queue: faktory server](#job-queue-faktory-server)
    - [Location service: nominatim server](#location-service-nominatim-server)
    - [Storage: minio server](#storage-minio-server)
    - [Datawarehouse: postgres](#datawarehouse-postgres)
    - [Error tracking: sentry](#error-tracking-sentry)

## Architecture

Dataworker is the module in prospect that does the background data processing. It is a
background worker where different tasks are registered. To run a task, the Core module
enqueues a job in a job queue, and the job is picked up by the dataworker. The
orchestrator library is [`faktory`](https://github.com/contribsys/faktory). The
orchestrator runs in one container, and the worker in another.

### Code

The dataworker code is in `dataworker`. The worker entry point —the file that is
initially called— is `worker.py`. Elements of the dataworker code are in various
submodules in `dataworker/dataworker`. This directory is installed as a package. The
package and its dependencies are managed by the `uv` tool.
Tasks are either internal tasks found in `dataworker/tasks/internal`, or are specific
datalake ingestion tasks —also called ETLs— found in `dataworker/tasks/ingestion`.
As we add more tasks to the worker, and find ways to generalize parts of them,
this architecture is subject to change in the medium term.

### Definitions

- *dataworker*: the background data processing module. It's code.
- *worker*: a python process that can execute tasks. Depending on the configuration,
there can be more than one such process. A process can eventually process tasks in
parallel via multi-processing.
- *task*: a piece of code that can be executed by a worker. For example, "import a
victron datafile" or "extract cooking events" are tasks. They are written in a file in
the dataworker codebase.
- *job*: an instance of a task. For example "import victron datafile with id `12345679`"

## Installation

It is assumed that during development, the python dataworker code is run on your machine
using a virtual environment for python dependencies.

### Pre-requirements

Below are tools that are external to the project python environment.

#### Python version and project management

We use [uv](https://docs.astral.sh/uv/) to install and manage Python, create a virtual env 
and manage cross-platform locked dependencies.

Install it globally, following the instructions of their website, or:
 * For MacOS and Linux

 ```curl -LsSf https://astral.sh/uv/install.sh | sh```
 * For Windows

 ```powershell -c "irm https://astral.sh/uv/install.ps1 | iex"```

#### Storage CLI (optional)

If you want to run manual commands to minio you can also install the [minio command line
tool](https://min.io/docs/minio/linux/reference/minio-mc.html#minio-client).

#### Database CLI (optional)

If you want to run manual sql commands on the database, you can install the postgresql
client.
On Linux, run `apt install postgresql-client`.

On MacOS, using [brew](https://brew.sh/):

```sh
brew install libpq
brew install postgresql
```

### Installing the project

Use `uv` to install the project and it's dependency in a local virtual environment
with:

`uv sync --frozen`

Dependencies are loosely defined in the `pyproject.toml` file, and a completely defined
set of versions is computed and written in `uv.lock`. When running the install
command above, the latter file is the one used.

### Installing pre-commit hooks

If you want to contribute.

There is a feature of git called `pre-commit hooks`, that are scripts that get called
when you want to commit changes. Those script must run without error on the applied
changes for git to proceed with the commit. The script used are normally code quality
tools that enforce standards in the commited code.

There is a tool called `pre-commit` that handles installing the tools in isolated
environments and registering scripts in the `.git` directory of the project so they get
run by git. `pre-commit` reads its config from `.pre-commit-config.yaml`. This config
describes what script should be installed and how it should run.

`pre-commit` will be installed by `uv` in the virtual env it has created. When you work
and commit with an activated venv (`uv sync --frozen && source .venv/bin/activate`) it
will work out of the box. If you wish to have `pre-commit` always available, you can run
`uv tool install pre-commit`.

When setting up the project for the first time with git, you can install the pre-commit
hooks with the `pre-commit` tool with the command `uv run pre-commit install`. After
that, hooks will be run on changed code before every commit. Running the hooks might
change your code, in which case you also need to add the resulting changes to the
changes you want to commit before retrying. It might also prevent you from commiting, if
tools detect a rule violation but cannot auto correct it. In that case you must correct
the error manually, or explicitely ignore it.

## Operation

### Running python code directly

To run python code in the project environment you can either:

- Activate the environment and a shell with `uv sync && source .venv/bin/activate`,
then run commands as usual.
- Run commands through uv with: `uv run <command>`

### Command line tool

Some operations are automated in a command line tool that is available within the
project environment. The code for the tool is in `cli.py`, and when in the context of
the project environment (`uv run <something>`), you can access it via it's
equivalent aliases `dataworker`, `hammer` or `sickle`.

`uv run hammer` will show the tool's documentation, generated from docstrings.

Commands are organized in groups, a bit like `git`. I recommend you explore the CLI to
find out what the available commands are.

## Configuration

### In python code

The configuration in the worker is handled by a library called Dynaconf. It supports a
hierarchical configuration. It builds a global configuration object from various
sources. Within a python process, the config is created in the module
`dataworker/config.py`. You can access the configuration object as follows: `from
dataworker.config import config`.

Access configuration values with `config.get("section.key")`, returning a default value
of None. Use `config.get("section.key", default=value)` to specifiy an alternate default
value.

### Configuration sources

The configuration object is built by loading layers in succession, with entries of the
latest source overwriting the ones of the previous sources.

#### Files

Firstly, config files are loaded in order.

`config-local-default.toml` is tracked in version control and contains settings for
worker behavior, as well as connection data for when the dataworker runs locally and
connects to services from the compose system. 

Next comes `config-secrets.toml` which is ignored by git and is intended to hold
configuration values that may not be commited in version control. There is a template
for this file—`config-secrets.template.toml`—that you can save under
`config-secrets.toml` and fill with actual values.

The last file is `config.toml`—which is also not tracked—and is intended to be the place
for the developer to change configuration values temporarily. Initialize or reset this
file by copying the default file: `cp config-default.toml config.toml`.

Additionally, any file following the pattern `config-extra*.toml` is also loaded. For
example, you can store config files with only the `[database]` or `[storage]` section
outside of the repo. When you want to run your code with those particular
configurations, link the relevant file to an extra config file within the repo following
the `config-extra*.toml` pattern. Delete the link after use to return to the previous
state.

```sh
ln ~/dev/configs/prospect-staging-database-readonly.toml config-extra-prospect-db.toml
# do some work
rm config-extra-prospect-db.toml # back to the previous state
```

#### Environment

After files, values are loaded from the system environment.

In order to be taken into account, environment values must follow the pattern:
`DW__<SECTION>__<KEY>`. Section and key must match the config file structure. For
example, the environment variable `DW__DATABASE__HOST=127.0.0.1` can be set manually in
your shell, but if a `.env` file is present, it is also loaded.

### Workflow

The config files in the repo are intended to be used when running code _locally_. When
packing the dataworker inside a _container_ and run as part of the greater prospect
system, only the default config file `config-local-default.toml` is copied over. In this
scenario, the configuration is better changed through environment variables. As an
example, see how the root-level `docker-compose.yml` file sets the environment of the
dataworker container.

The recommended workflow is as such:

**configure via the config files when running locally**

but

**let the environment variable mapping in the compose file configure the dataworker**
**when running as a container in the compose network**.

You can display and check the final configuration as it would be seen from inside the
application with the project command: `uv run dataworker config local`. This tool is
also available in the containered version of the dataworker; to see the configuration
used by the dataworker when containerized with docker compose, execute `uv run
dataworker config container`.

## Using the various development setups

These are the different ways that you can set up your system for dataworker development.

### Simple script

Use this method for developing code that does not interact with any external services.
It still uses the datworker python environment. Your code can simply be run in the
virtual environment created by uv. You'll have access to the installed python
packages and the dataworker modules.

`uv run something.py`

### With services

Use this if you use code that interacts with prospect services that the dataworker
depends on (storage, database, job scheduler, etc.). In this configuration, the python
code still runs from the same virtual environment, but the additional services will run
in containers. A command in the project task runner will start the required service
containers in the background:

`uv run hammer worker services`

Once the services are running, you can run developement scripts against those services
to build functionalities just as before. Connection to those services is configured in
`config-local-default.toml` and will be automatically loaded in your configuration.

`uv run something_using_services.py`

### Running a local worker

Use this if the code you're developing is already integrated in the dataworker as a
registered task and you want to run this code in the dataworker.

In this case, first run a worker with:

`uv run hammer worker local`

To trigger a job by the worker, modify the template file `ingestion_job.template.toml` or
`internal_job.template.toml` with your values and add the job to the queue (in a
second terminal):

`uv run hammer jobqueue add <your_job>.toml`

### Containerized

If the code you develop for the dataworker works when running locally and you want to
try the containered version, use this method. It's a use case similar to the one before,
but where the dataworker is built into a docker image itself and run containerized as a
service inside the docker-compose network. The command for this is:

`uv run hammer worker container`

You can then trigger some work as in the setup above.

Note that in the build instruction for a containerized dataworker, the
`config-local-default.toml` file is not included, as it contains connection data for
services running in compose, but exposed to localhost. When running the dataworker as a
`compose` service, the connection to the other services are made through the `compose`
internal network. In this case configuration comes from mapping env vars to the
container, as defined in the `compose` file.

## Testing

The project uses [Pytest](https://docs.pytest.org) for testing. The whole test suite is 
to be run inside the project environment with `uv run pytest`.

For Ingestion tasks tests (those that create a dummy database), it is important to do two things
***in the main `dataworker/` directory***, because the background functions depend on
the location:

 - `uv run hammer worker services` (to set up the database)
 - `uv run pytest tests/<your-etl-test-script>`

## Linting and formatting

The project uses [Ruff](https://docs.astral.sh/ruff/) for linting and formatting. It is
handled via [pre-commit hooks](#installing-pre-commit-hooks) which will lint before 
committing. It is also configured so that modern IDEs comply to the rules automatically.

## Development

### Developing new tasks

NOTE: A more detailed guide on how to develop tasks can be found 
[here](https://gitlab.com/prospect-energy/prospect-server/-/wikis/Guides/Creating-Dataworker-Tasks).

The following is a quick guide on developing Ingestion tasks.

#### Ingestion Tasks

To add a new Ingestion task (ETL), you just need to add a module in the `tasks/ingestion` directory.
The module must contain a function called `transform(file_data:dict)-> dict` whose input
is a dictionary that comes from parsing the data file, and returns a dictionary to be
inserted into the database of the following format:

```json
{
  "table_name":[
    {'col1':'val1','col2':'val2'},
    {'col1':'val1','col2':'val2'},
  ],
  "other_table_name":[
    {'col1':'val1','col2':'val2','col3':'val3'},
    {'col1':'val1','col2':'val2','col3':'val3'},
  ]
}
```

The detected tasks are registered with the worker as Ingestion tasks.
The name of the registered task is —by default— the name of the module.
This can be overridden by setting a variable named `TASK_NAME` within the module itself.
Similarly, the entrypoint function can be overridden with the variable `ENTRYPOINT`.

### Howtos

#### Using the logger

See [the docs](https://loguru.readthedocs.io) of the logging library.

```py
# basics
from dataworker import logger

logger.info('Start of a process')
logger.debug('A detail')

# sending key-value data that can be accessed in the structured logs
logger.info('importing file',file_key='datalake/shs/beziers_123456789.json')

# Using . Note that no f-string is used
logger.info('importing file :{file_key}',file_key='datalake/shs/beziers_123456789.json')
```

#### Configuring logging

With local default settings, the workers log to stderr in a human readable format. When
running from inside a container (and thus without a config file), the logging switches
to serialized. This behavior is controlled by `logging serialized_to_file`.
Additionally, config options allow adding context information to human readable logs
(`logging.inline_context`) or saving serialized logs to file in parallel with terminal
logging (`logging.serialized_to_file`).

#### Accessing the config

```py
from dataworker import config

# optimistic. raise exception if missing
value_from_config=config.section.subsection.key
# none-default
value_from_config=config.get("section.subsection.key")
```

#### Using a database connection and table objects

```py
# An example
from dataworker.database import get_table, get_connection

table = get_table("sensor")
connection = get_connection()
connection.execute(table.insert().values(**sample))

# Note that the transaction still needs to be commited
```

#### Copying files from operational storage to local storage

You first need to configure the connection to the prospect storage with read-only
credentials. If you want to copy to your local minio, this needs to be running as well,
and the connection configured to it with write credentials.

```sh
mc find <prospect_storage_alias>/datalake --name '*-aam_child.txt' --exec 'mc cp {}
<local_storage_alias>/datalake'
```

## Configuration using environment variables

### Worker: python faktory client

```
DW__WORKER__QUEUE
DW__WORKER__CONCURENCY
```

### Job queue: faktory server

```
DW__JOB_QUEUE__HOST
DW__JOB_QUEUE__PASSWORD
DW__JOB_QUEUE__PORT
```

### Location service: nominatim server

```
DW__LOCATION__HOST
DW__LOCATION__PORT
DW__LOCATION__USER
DW__LOCATION__PASSWORD
```
### Storage: minio server

```
DW__STORAGE__HOST
DW__STORAGE__port
DW__STORAGE__USE_SECURE # bool.default false. put true if using https
DW__STORAGE__USER
DW__STORAGE__PASSWORD
DW__STORAGE__BUCKET # bucket that contains the data files
```

### Datawarehouse: postgres

```
DW__DATABASE__HOST
DW__DATABASE__PORT
DW__DATABASE__USER
DW__DATABASE__PASSWORD
DW__DATABASE__NAME
```

### Error tracking: sentry

```
DW__MONITORING__SENTRY_DSN
```
