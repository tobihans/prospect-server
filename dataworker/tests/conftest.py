import datetime
from collections.abc import Callable, Generator

import pytest
import sqlalchemy
import time_machine

from dataworker import config, logger
from dataworker.database import (
    _delete_metadata,
    close_all_connections,
    get_or_create_connection,
    inspect_engines,
    test_database_connection,
)
from dataworker.logging import add_serialized_file_sink
from dataworker.queries import (
    drop_database,
    recreate_database,
    run_sql_from_file,
)

from .data_fixtures import *  # noqa: F403
from .utils import load_test_data

TEST_DB_NAME = "test"
DB_DUMP_FILE = "tests/structure.sql"


# optionally also log to a file
if config.get("logging.serialized_to_file", default=False):
    add_serialized_file_sink()

# ------------------------- Database -------------------------


@pytest.fixture(scope="session", autouse=True)
def _database_test_configuration() -> None:
    """Tests use a distinct database."""
    logger.info("changing default database name to {}", TEST_DB_NAME)
    config.set("database.name", TEST_DB_NAME)
    # disable auto-commit on functions that use
    # the decorator `dataworker.database.manage_transaction`
    config.set("database.commit_managed_transactions", False)


@pytest.fixture(scope="session")
def database_creator() -> Generator[Callable, None, None]:
    """Tool to ensure empty databases.

    Using the tool will delete and recreate an empty database of the given name.
    Database are cleaned at the end of the test session. Subsequent calls with
    the same name are ignored.

    Usage:

        def test_or_fixture(database_creator):
            database_creator("name")
    """
    databases = []  # <- closure! that variable get attached to the function

    def _create_database(name) -> None:
        if name not in databases:
            recreate_database(name)
            databases.append(name)

    yield _create_database
    inspect_engines()
    close_all_connections()
    inspect_engines()
    if config.get("dev.clean_test_db", default=True):
        for name in databases:
            drop_database(name)


@pytest.fixture(scope="session")
def _setup_test_database(database_creator) -> None:
    """Create and initialize database for the duration of the test session.

    If your test requires a database, don't use this fixture directly.
    `rollback` is the one you are looking for.
    """
    test_database_connection()
    logger.info("setting up test database {} from {}", TEST_DB_NAME, DB_DUMP_FILE)
    database_creator(TEST_DB_NAME)
    run_sql_from_file(DB_DUMP_FILE, TEST_DB_NAME, commit=True)
    _delete_metadata()  # clear invalid metadata


@pytest.fixture(autouse=True)
def _mocked_test_connection_string(mocker, request):
    """This autoused fixture can be disabled using the 'original_connection' mark."""
    if request.node.get_closest_marker("original_connection"):
        return
    mocker.patch("dataworker.database._stringify_connection_name", return_value="test")
    # mocking read-only checks as we always use one read enabled connection in tests
    mocker.patch("dataworker.database._wrong_readonly", return_value=None)


@pytest.fixture(autouse=True, scope="session")
def _mocked_test_nominatim_address_obj(session_mocker):
    """Avoid URL calls in tests."""
    session_mocker.patch("dataworker.location._get_address_object", return_value={})


@pytest.fixture
def _database(_setup_test_database) -> Generator[None, None, None]:
    """Rollback changes after each test.

    Use this fixture whenever a test needs a connection to the database.
    """
    # TODO: get a connection subclass for the tests.
    #       Ideally we would listen to statements and raise if trying to write
    #       on a connection requested as read-only
    with get_or_create_connection():
        logger.debug("starting new transaction")
        yield
        logger.debug("rolling back transaction and closing connection")


# --------------------- config -----------------------------


@pytest.fixture(scope="session", autouse=True)
def _dummy_location_config() -> None:
    """Don't use real location config.

    To establish new records, comment out this fixture
    """
    config.location = {
        "user": "nomi_user",
        "password": "nomi_pass",
        "host": "nominatimhost",
        "port": "12345",
    }


@pytest.fixture(scope="session", autouse=True)
def _dummy_worker_queue_config() -> None:
    """Set a fixed name for the default queue in tests."""
    config.worker.queue = "test_queue"


# ------------------------- logging ------------------------


# NB: this is directly copied from:
# https://loguru.readthedocs.io/en/stable/resources/migration.html#replacing-caplog-fixture-from-pytest-library
@pytest.fixture
def caplog(caplog: pytest.LogCaptureFixture):
    handler_id = logger.add(
        caplog.handler,
        format="{message}",
        level=0,
        filter=lambda record: record["level"].no >= caplog.handler.level,
        enqueue=False,  # Set to 'True' if your test is spawning child processes.
    )
    yield caplog
    logger.remove(handler_id)


# ------------------------- events -------------------------


# @pytest.fixture
# def count_events():
#     """Install a log collector and provide a counting function."""
#     events = []  # wow, a closure!
#     # event collector
#     def collect_event(log) -> None:
#         event = log.record["extra"].get("event")
#         if event and isinstance(event, dataworker.tracking.Event):
#             events.append(event)
#     logger.add(collect_event)
#     def count():
#         return collections.Counter(events)
#     return count


@pytest.fixture
def event_buffer():
    """Setup capture for events emmited through logs."""
    buffer = []  # a closure!

    def record_event(log) -> None:
        if event := log.record["extra"].get("event"):
            buffer.append(event)

    sink = logger.add(
        record_event,
    )
    yield buffer

    logger.remove(sink)


@pytest.fixture
def events(event_buffer):
    """Reset the buffer every test."""
    event_buffer.clear()
    return event_buffer


# ------------------------- IO -------------------------


@pytest.fixture(autouse=True, scope="session")
def _swap_test_data_for_storage(session_mocker) -> None:
    """Read files from test data directory instead of real storage server."""
    session_mocker.patch("dataworker.task.read_datafile", load_test_data)


@pytest.fixture
def load_datafile_mock(mocker):
    """Mock loader to inject test data manually."""
    return mocker.patch("dataworker.task.read_datafile")


@pytest.fixture
def store_recordset_mock(mocker):
    """Mock inserter to test ingestion post-processings."""
    return mocker.patch("dataworker.task.store_recordset")


@pytest.fixture
def mocked_connection(mocker):
    bad_connection = mocker.MagicMock()
    mocker.patch(
        "dataworker.datawarehouse.get_or_create_connection", return_value=bad_connection
    )
    return bad_connection


class MockedFireException(Exception):
    """Exception returned by a mock in tests."""

    def __str__(self) -> str:
        """Return a spicy message."""
        return "something on fire"


@pytest.fixture
def _execute_sends_exception(mocked_connection) -> None:
    mocked_connection.execute.side_effect = MockedFireException


@pytest.fixture
def _execute_sends_sqla_error(mocked_connection) -> None:
    mocked_connection.execute.side_effect = sqlalchemy.exc.SQLAlchemyError(
        "database on fire"
    )


# ------------------------- time -------------------------


@pytest.fixture
def now():
    with time_machine.travel(
        datetime.datetime(2024, 3, 18, 22, 23, 37, tzinfo=datetime.UTC), tick=False
    ):
        yield datetime.datetime.now(datetime.UTC)


# ------------------ geo-lookup ---------------------------


@pytest.fixture
def _mocked_address_object(mocker):
    """Mocks address object return from reverse geo lookup."""
    mocker.patch(
        "dataworker.location._get_address_object",
        return_value={
            "road": "RD45",
            "village": "Chaba",
            "state_district": "Chilubi District",
            "state": "Northern Province",
            "ISO3166-2-lvl4": "ZM-05",
            "country": "Zambia",
            "country_code": "zm",
        },
    )
