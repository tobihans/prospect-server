import operator

import arrow
import pydantic
import pytest

import dataworker
from dataworker.dataset import (
    Dataset,
    RecordSet,
    count_values,
    stabilize,
)


def test__map_fields() -> None:
    r = {"a": 10, "b": 20, "c": 30}
    assert dataworker.dataset.map_fields(lambda x: x * 2, ["a", "c"], r) == {
        "a": 20,
        "b": 20,
        "c": 60,
    }


def test__stabilize__invariants() -> None:
    assert stabilize("this") == "this"
    assert stabilize(1) == 1
    assert stabilize([1, 2, 3]) == [1, 2, 3]


def test__stabilize__mappings() -> None:
    assert stabilize({"c": 3, "b": 2, "a": 1}) == [("a", 1), ("b", 2), ("c", 3)]


def test__stabilize__datetime() -> None:
    assert stabilize(arrow.get("2024-01-18").datetime) == "2024-01-18 00:00:00+00:00"


def test__count_values() -> None:
    # no missing
    assert (
        count_values([
            {"a": 1, "b": 2, "c": 3},
            {"a": 1, "b": 2, "c": 3},
            {"a": 1, "b": 2, "c": 3},
        ])
        == 9
    )
    # counts only Nones
    assert (
        count_values([
            {"a": 1, "b": 0, "c": 3},
            {"a": "", "b": 2, "c": 3},
            {"a": 1, "b": 2, "c": []},
        ])
        == 9
    )
    # missing values
    assert (
        count_values([
            {"a": 1, "b": None, "c": 3},
            {"a": None, "b": 2, "c": 3},
            {"a": 1, "b": 2, "c": None},
        ])
        == 6
    )


# --- RecordSet


def test__RecordSet__creation() -> None:
    RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )


def test__RecordSet__as_dict() -> None:
    recordset = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )
    assert recordset.as_dict() == {
        "tulips": [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    }


def test__RecordSet__repr() -> None:
    recordset = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )
    assert repr(recordset) == "RecordSet('tulips' <2 records>)"


def test__RecordSet__validation() -> None:
    # not records
    with pytest.raises(pydantic.ValidationError):
        RecordSet(
            "tulips",
            [1, 2, 3],
        )


def test__RecordSet__len() -> None:
    recordset = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )
    assert len(recordset) == 2
    recordset = RecordSet("tulips", [])
    assert len(recordset) == 0


def test__RecordSet__value_count() -> None:
    recordset = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple"},
            {"size": 17, "color": "turquoise", "nothing": None},
        ],
    )
    assert recordset.value_count() == 4


def test__RecordSet__hash() -> None:
    recordset = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple"},
            {"size": 17, "color": "turquoise"},
        ],
    )
    assert (
        recordset.hash()
        == "a74d4ed06b02c5a5f1a4ddf74293d33445a134e4240f53d1e666b1f0d2a88752"
    )


def test__RecordSet__key_set() -> None:
    recordset = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple"},
            {"size": 17, "petal_count": "too many"},
        ],
    )
    assert recordset.key_set() == {"color", "petal_count", "size"}


# --- RecordSet operations


def make_it_pink(record):
    return record | {"color": "pink"}


def test__RecordSet__apply_to_records() -> None:
    rs = RecordSet(
        "tulips", [{"size": 17, "color": "turquoise"}, {"size": 12, "color": "purple"}]
    )
    assert rs.apply_to_records(
        lambda records: sorted(records, key=operator.itemgetter("size"))
    ).records == [{"color": "purple", "size": 12}, {"color": "turquoise", "size": 17}]


def test__RecordSet__map_records() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )

    assert rs.map_records(make_it_pink).records == [
        {"size": 12, "color": "pink"},
        {"size": 17, "color": "pink"},
    ]


def test__RecordSet__map_records_via_apply() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )

    assert rs.apply("map_records", make_it_pink).records == [
        {"size": 12, "color": "pink"},
        {"size": 17, "color": "pink"},
    ]


def double_ints(value):
    return value * 2 if isinstance(value, int) else value


def test__RecordSet__map_values() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )

    assert rs.map_values(double_ints).records == [
        {"size": 24, "color": "purple"},
        {"size": 34, "color": "turquoise"},
    ]


def test__RecordSet__map_values_via_apply() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )

    assert rs.apply("map_values", double_ints).records == [
        {"size": 24, "color": "purple"},
        {"size": 34, "color": "turquoise"},
    ]


def test__RecordSet__filter_records() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )
    assert rs.filter_records(lambda flower: flower["size"] >= 15).records == [
        {"size": 17, "color": "turquoise"},
    ]


def test__RecordSet__filter_records_via_apply() -> None:
    rs = RecordSet(
        "tulips", [{"size": 12, "color": "purple"}, {"size": 17, "color": "turquoise"}]
    )
    assert rs.apply("filter_records", lambda flower: flower["size"] >= 15).records == [
        {"size": 17, "color": "turquoise"},
    ]


def test__RecordSet__filter_values() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.filter_values(lambda value: isinstance(value, int)).records == [
        {"size": 12, "petal_count": 4},
        {"size": 17, "petal_count": 9},
    ]


def test__RecordSet__filter_values_via_apply() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.apply("filter_values", lambda value: isinstance(value, int)).records == [
        {"size": 12, "petal_count": 4},
        {"size": 17, "petal_count": 9},
    ]


def test__RecordSet__filter_keys() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.filter_keys(lambda key: key == "size").records == [
        {"size": 12},
        {"size": 17},
    ]


def test__RecordSet__filter_keys_via_apply() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.apply("filter_keys", lambda key: key == "size").records == [
        {"size": 12},
        {"size": 17},
    ]


def test__RecordSet__omit_keys() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.omit_keys(["size", "petal_count", "speed"]).records == [
        {"color": "purple"},
        {"color": "turquoise"},
    ]


def test__RecordSet__omit_keys_via_apply() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.apply("omit_keys", ["size", "petal_count", "speed"]).records == [
        {"color": "purple"},
        {"color": "turquoise"},
    ]


def test__RecordSet__omit_key_pattern() -> None:
    rs = RecordSet(
        "forest_beings",
        [
            {"size": 12, "gnome_count": 7, "gnome_id": 7},
            {"size": 17, "gnome_count": 12, "gnome_id": 44, "last_gnome_id": 44},
        ],
    )
    assert rs.omit_key_pattern("gnome_.*").records == [
        {"size": 12},
        {"size": 17, "last_gnome_id": 44},
    ]


def test__RecordSet__omit_key_pattern_via_apply() -> None:
    rs = RecordSet(
        "forest_beings",
        [
            {"size": 12, "gnome_count": 7, "gnome_id": 7},
            {"size": 17, "gnome_count": 12, "gnome_id": 44},
        ],
    )
    assert rs.apply("omit_key_pattern", "gnome_.*").records == [
        {"size": 12},
        {"size": 17},
    ]


def test__RecordSet__add() -> None:
    rs = RecordSet(
        "forest_beings",
        [
            {"size": 12, "gnome_count": 7, "gnome_id": 7},
            {"size": 17, "gnome_count": 12, "gnome_id": 44},
        ],
    )
    assert rs.add({"timestamp": "long_time_ago"}).records == [
        {"size": 12, "gnome_count": 7, "gnome_id": 7, "timestamp": "long_time_ago"},
        {"size": 17, "gnome_count": 12, "gnome_id": 44, "timestamp": "long_time_ago"},
    ]


def test__RecordSet__add_via_apply() -> None:
    rs = RecordSet(
        "forest_beings",
        [
            {"size": 12, "gnome_count": 7, "gnome_id": 7},
            {"size": 17, "gnome_count": 12, "gnome_id": 44},
        ],
    )
    assert rs.apply("add", {"timestamp": "long_time_ago"}).records == [
        {"size": 12, "gnome_count": 7, "gnome_id": 7, "timestamp": "long_time_ago"},
        {"size": 17, "gnome_count": 12, "gnome_id": 44, "timestamp": "long_time_ago"},
    ]


def test__RecordSet__select() -> None:
    rs = RecordSet(
        "forest_beings",
        [
            {"size": 12, "gnome_count": 7, "gnome_id": 7},
            {"size": 17, "gnome_count": 12, "gnome_id": 44},
        ],
    )
    assert rs.select(["gnome_count", "gnome_id"]).records == [
        {"gnome_count": 7, "gnome_id": 7},
        {"gnome_count": 12, "gnome_id": 44},
    ]


def test__RecordSet__apply_multiple() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.apply_multiple(
        ("omit_keys", ["color"]), ("map_values", lambda x: 2 * x)
    ).records == [
        {"size": 24, "petal_count": 8},
        {"size": 34, "petal_count": 18},
    ]


def test__RecordSet__sort() -> None:
    rs = RecordSet(
        "tulips",
        [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 49, "color": "turquoise", "petal_count": 9},
            {"size": 17, "color": "turquoise", "petal_count": 9},
            {"size": 5, "color": "turquoise", "petal_count": 9},
        ],
    )
    assert rs.sort("size").records == [
        {"size": 5, "color": "turquoise", "petal_count": 9},
        {"size": 12, "color": "purple", "petal_count": 4},
        {"size": 17, "color": "turquoise", "petal_count": 9},
        {"size": 49, "color": "turquoise", "petal_count": 9},
    ]


def test__Recordset__is_update__insert():
    dataset = Dataset.from_dict({
        "table": [{"this": 1000}, {"this": 34150, "that": "plic"}]
    })
    assert dataset.recordsets[0].is_update is False


def test__Recordset__is_update__update_flag() -> None:
    # if recordset dict has an update flag => update
    dataset = Dataset.from_dict({
        "_update": "table_1",
        "table_1": [{"this": 1000}, {"this": 34150, "that": "plic"}],
    })
    assert dataset.recordsets[0].is_update is True


def test__Recordset__is_update__update_flag_multiple_flags() -> None:
    # if recordset dict has an update flag => update
    dataset = Dataset.from_dict({
        "_update": ["table_1", "table_2"],
        "table_1": [{"this": 1000}, {"this": 34150, "that": "plic"}],
    })
    assert dataset.recordsets[0].is_update is True


def test__Recordset__one_is_update__update_flag_multiple_tables() -> None:
    # if recordset dict has an update flag => update
    dataset = Dataset.from_dict({
        "_update": "table_1",
        "table_1": [{"this": 1000}, {"this": 34150, "that": "plic"}],
        "table_2": [{"this": 1000}, {"this": 34150, "that": "plic"}],
    })
    recordset_table_1 = dataset.recordsets[0]
    recordset_table_2 = dataset.recordsets[1]
    assert recordset_table_1.is_update is True
    assert recordset_table_2.is_update is False


def test__Recordset__both_is_update__update_flag_multiple_tables() -> None:
    # if recordset dict has an update flag => update
    dataset = Dataset.from_dict({
        "_update": ["table_1", "table_2"],
        "table_1": [{"this": 1000}, {"this": 34150, "that": "plic"}],
        "table_2": [{"this": 1000}, {"this": 34150, "that": "plic"}],
    })
    recordset_table_1 = dataset.recordsets[0]
    recordset_table_2 = dataset.recordsets[1]
    assert recordset_table_1.is_update is True
    assert recordset_table_2.is_update is True


def test__Recordset__is_update__update_flag_as_list() -> None:
    # if recordset dict has an update flag => update
    dataset = Dataset.from_dict({
        "_update": ["table_1"],
        "table_1": [{"this": 1000}, {"this": 34150, "that": "plic"}],
    })
    assert dataset.recordsets[0].is_update is True


def test__is_payload_update__uid() -> None:
    # if all samples have uid => update
    dataset = Dataset.from_dict({
        "table_1": [
            {"this": 1000, "uid": "123"},
            {"this": 34150, "that": "plic", "uid": "456"},
        ],
        "table_2": [
            {"pim": 4444, "pam": "wiz", "uid": "789"},
            {"pim": 3333, "pam": "pouc", "uid": "321"},
        ],
    })
    recordset_table_1 = dataset.recordsets[0]
    recordset_table_2 = dataset.recordsets[1]
    assert recordset_table_1.is_update is True
    assert recordset_table_2.is_update is True
    # if a sample is missing in one table => insert
    dataset = Dataset.from_dict({
        "table_1": [
            {"this": 1000, "uid": "123"},
            {"this": 34150, "that": "plic", "uid": "456"},
            {"this": 3012, "that": "ploc"},  # no uid
        ],
        "table_2": [
            {"pim": 4444, "pam": "wiz", "uid": "789"},
            {"pim": 3333, "pam": "pouc", "uid": "321"},
        ],
    })
    recordset_table_1 = dataset.recordsets[0]
    recordset_table_2 = dataset.recordsets[1]
    assert recordset_table_1.is_update is False
    assert recordset_table_2.is_update is True


# --- Dataset


def test__Dataset__creation_from_dict() -> None:
    dataset = Dataset.from_dict(
        {
            "tulips": [
                {"size": 12, "color": "purple", "petal_count": 4},
                {"size": 17, "color": "turquoise", "petal_count": 9},
            ],
            "gnomes": [
                {"size": 14, "color": "green", "hat_type": "pointy"},
                {"size": 21, "color": "brown", "hat_type": "floppy"},
            ],
        },
    )
    assert dataset.recordsets == [
        RecordSet(
            table="tulips",
            records=[
                {"size": 12, "color": "purple", "petal_count": 4},
                {"size": 17, "color": "turquoise", "petal_count": 9},
            ],
        ),
        RecordSet(
            table="gnomes",
            records=[
                {"size": 14, "color": "green", "hat_type": "pointy"},
                {"size": 21, "color": "brown", "hat_type": "floppy"},
            ],
        ),
    ]


def test__Dataset__as_dict__empty() -> None:
    dataset = Dataset.from_dict({})
    assert dataset.as_dict() == {}


def test__Dataset__as_dict() -> None:
    in_dict = {
        "tulips": [
            {"size": 12, "color": "purple", "petal_count": 4},
            {"size": 17, "color": "turquoise", "petal_count": 9},
        ],
        "gnomes": [
            {"size": 14, "color": "green", "hat_type": "pointy"},
            {"size": 21, "color": "brown", "hat_type": "floppy"},
        ],
    }
    dataset = Dataset.from_dict(in_dict)
    assert dataset.as_dict() == in_dict


def test__Dataset__creation_from_dict__none() -> None:
    dataset = Dataset.from_dict(None)
    assert dataset.recordsets == []


def test__Dataset__repr() -> None:
    dataset = Dataset.from_dict(
        {
            "tulips": [
                {"size": 12, "color": "purple", "petal_count": 4},
                {"size": 17, "color": "turquoise", "petal_count": 9},
            ],
            "gnomes": [
                {"size": 14, "color": "green", "hat_type": "pointy"},
                {"size": 21, "color": "brown", "hat_type": "floppy"},
            ],
        },
    )
    assert repr(dataset) == "Dataset('tulips' <2 records>,'gnomes' <2 records>)"


def test__Dataset__has_records() -> None:
    # recordsets with data
    dataset = Dataset.from_dict(
        {
            "tulips": [
                {"size": 12, "color": "purple", "petal_count": 4},
                {"size": 17, "color": "turquoise", "petal_count": 9},
            ],
            "gnomes": [
                {"size": 14, "color": "green", "hat_type": "pointy"},
                {"size": 21, "color": "brown", "hat_type": "floppy"},
            ],
        },
    )
    assert dataset.has_records is True
    # recordsets with no data
    dataset = Dataset.from_dict(
        {
            "tulips": [],
            "gnomes": [],
        },
    )
    assert dataset.has_records is False
    # no recordsets
    dataset = Dataset.from_dict(
        {},
    )
    assert dataset.has_records is False
