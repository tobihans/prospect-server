import geopy.geocoders
import pytest

from dataworker import config, location
from dataworker.metadata import creation_timestamps
from dataworker.queries import fetch_one_row
from tests.utils import insert_records

# -----| region level map mocks


@pytest.fixture
def _test_region_levels(mocker):
    """Mocks loading of region level asset."""
    mock = mocker.patch("dataworker.location._load_country_region_levels")
    mock.return_value = {
        "zm": {"location_area_lvl1": "state", "location_area_lvl2": "state_district"}
    }
    yield
    location._load_country_region_levels.cache_clear()


@pytest.fixture
def _bad_region_levels(mocker):
    """Loads region names that won't match the data."""
    mock = mocker.patch("dataworker.location._load_country_region_levels")
    mock.return_value = {
        "zm": {
            "location_area_lvl1": "big_region",
            "location_area_lvl2": "smaller_region",
        }
    }
    yield
    location._load_country_region_levels.cache_clear()


@pytest.fixture
def _region_level_load_error(mocker):
    """Make the asset loader fail."""
    location._load_country_region_levels.cache_clear()
    mocker.patch("dataworker.location.tomllib.load", side_effect=Exception)
    yield
    location._load_country_region_levels.cache_clear()


# -----| location client mock


@pytest.fixture
def _location_service_not_available(mocker):
    """Nominatim client returns server error when called."""
    get_location_client = mocker.patch("dataworker.location._get_location_client")
    mocked_location_client = mocker.Mock()
    mocked_location_client.reverse.side_effect = geopy.exc.GeocoderUnavailable
    get_location_client.return_value = mocked_location_client
    yield
    location._get_location_client.cache_clear()


# -----| address function mock


@pytest.fixture
def _mocked_address_object(mocker) -> None:
    """Mocks address object return."""
    mocker.patch(
        "dataworker.location._get_address_object",
        return_value={
            "state_district": "Chibombo District",
            "state": "Central Province",
            "ISO3166-2-lvl4": "ZM-02",
            "country": "Zambia",
            "country_code": "zm",
        },
    )


@pytest.fixture
def _mocked_address_object_unmapped_country(mocker) -> None:
    """If country data is not in the server, the response has only country data."""
    mocker.patch(
        "dataworker.location._get_address_object",
        return_value={"country": "France", "country_code": "fr"},
    )


@pytest.fixture
def _location_not_configured(mocker) -> None:
    """No location section in the config object."""
    mocked_config = config.dynaconf_clone()
    mocked_config.unset("location", force=True)
    mocker.patch("dataworker.location.config", mocked_config)


# -----| arguments


@pytest.fixture
def point():
    """Somewhere in the zambian countryside."""
    return (-14.572717594325104, 27.312722644927074)


@pytest.fixture
def unexpected_point():
    """Somewhere unexpected."""
    return (43.8036, 3.3236)


# --------------------------------------------------


def test__get_location_client__creation() -> None:
    client = location._get_location_client()
    assert isinstance(client, geopy.geocoders.nominatim.Nominatim)
    assert client.domain == "nomi_user:nomi_pass@nominatimhost:12345"


def test__get_location_client__singleton() -> None:
    client = location._get_location_client()
    another_client = location._get_location_client()
    assert client is another_client


@pytest.mark.usefixtures("_location_not_configured")
def test__get_location_client__location_not_configured() -> None:
    location._get_location_client.cache_clear()
    with pytest.raises(RuntimeError):
        location._get_location_client()


@pytest.mark.usefixtures("_test_region_levels")
def test__load_country_region_levels() -> None:
    levels = location._load_country_region_levels()
    assert isinstance(levels, dict)


@pytest.mark.usefixtures("_location_not_configured", "_region_level_load_error")
def test__load_country_region_levels__region_map_dont_load() -> None:
    levels = location._load_country_region_levels()
    assert levels == {}


@pytest.mark.usefixtures("_test_region_levels", "_mocked_address_object")
def test__reverse_geo_lookup(
    point,
) -> None:
    """Goes as expected."""
    assert location.reverse_geo_lookup(*point) == {
        "country": "ZM",
        "location_area_lvl1": "Central Province",
        "location_area_lvl2": "Chibombo District",
    }


@pytest.mark.usefixtures("_test_region_levels", "_mocked_address_object")
def test__reverse_geo_lookup_with_prefix(
    point,
) -> None:
    """Goes as expected."""
    assert location.reverse_geo_lookup(*point, geo_col_prefix="customer_") == {
        "customer_country": "ZM",
        "customer_location_area_lvl1": "Central Province",
        "customer_location_area_lvl2": "Chibombo District",
    }


@pytest.mark.usefixtures("_region_level_load_error", "_mocked_address_object")
def test__reverse_geo_lookup__region_map_dont_load(point) -> None:
    """Asset do not load, but at least we get the country."""
    assert location.reverse_geo_lookup(*point) == {
        "country": "ZM",
    }


@pytest.mark.usefixtures(
    "_test_region_levels",
    "_mocked_address_object_unmapped_country",
)
def test__reverse_geo_lookup__country_not_in_map(unexpected_point) -> None:
    """Country not in region map, but we at least get the country."""
    assert location.reverse_geo_lookup(*unexpected_point) == {
        "country": "FR",
    }


def test__reverse_geo_lookup__no_country_in_address(point) -> None:
    """Nothing to do, just don't crash."""
    assert location.reverse_geo_lookup(*point) == {}


@pytest.mark.usefixtures("_bad_region_levels", "_mocked_address_object")
def test__reverse_geo_lookup__levels_not_matching(point) -> None:
    """Don't crash even if region levels are not matching."""
    assert location.reverse_geo_lookup(*point) == {"country": "ZM"}


@pytest.mark.usefixtures("_test_region_levels")
def test__reverse_geo_lookup__bad_coordinates() -> None:
    """Don't crash even if region levels are not matching."""
    assert location.reverse_geo_lookup(None, None) == {}
    assert location.reverse_geo_lookup(12.34, None) == {}
    assert location.reverse_geo_lookup(None, 12.34) == {}
    assert location.reverse_geo_lookup(None, "12.34") == {}


@pytest.mark.usefixtures("_location_service_not_available", "_test_region_levels")
def test__reverse_geo_lookup__broken_connection(point) -> None:
    assert location.reverse_geo_lookup(*point) == {}


@pytest.mark.usefixtures("_location_not_configured")
def test__reverse_geo_lookup__no_location_config(point) -> None:
    assert location.reverse_geo_lookup(*point) == {}


SHS_SAMPLE_DICT = {
    "uid": "realllly-uniq",
    "account_uid": "foobar",
    "device_external_id": "D74320525",
    "account_external_id": "A74320525",
    "serial_number": "D74320525",
    "manufacturer": "da best",
    "customer_external_id": "C74320525",
    "customer_uid": "foobar1",
    "device_uid": "foobar2",
    "import_id": 1234,
    "organization_id": 45,
    "source_id": 206,
    "data_origin": "solar-system",
    "customer_latitude_b": 1.63157694739998,
    "customer_longitude_b": 32.8350548033075,
} | creation_timestamps()


@pytest.mark.usefixtures("_database", "_mocked_address_object")
def test__location_area_from_lat_lon_missing_columns():
    insert_records("data_shs", [SHS_SAMPLE_DICT])
    with pytest.raises(TypeError, match="Required columns"):
        location.location_area_from_lat_lon(
            "data_shs",
            SHS_SAMPLE_DICT["organization_id"],
            SHS_SAMPLE_DICT["source_id"],
            "impossible_prefix_",
        )


@pytest.mark.parametrize(
    "shs_record",
    [
        SHS_SAMPLE_DICT,
        SHS_SAMPLE_DICT | {"customer_country": "zm"},
        SHS_SAMPLE_DICT | {"customer_country": "ZM"},
    ],
)
@pytest.mark.usefixtures("_database", "_mocked_address_object")
def test__location_area_from_lat_lon_works(shs_record):
    insert_records("data_shs", [shs_record])
    record_set = location.location_area_from_lat_lon(
        "data_shs",
        shs_record["organization_id"],
        shs_record["source_id"],
        "customer_",
    )
    assert record_set["_update"] == "data_shs"
    record = record_set["data_shs"][0]
    assert record["customer_country"] == "ZM"
    assert record["customer_location_area_1"] == "Central Province"
    assert record["customer_location_area_2"] == "Chibombo District"
    assert record["uid"] == shs_record["uid"]


@pytest.mark.usefixtures("_database", "_mocked_address_object")
def test__location_area_from_lat_lon_ignore_wrong_country():
    insert_records("data_shs", [SHS_SAMPLE_DICT | {"customer_country": "de"}])
    record_set = location.location_area_from_lat_lon(
        "data_shs",
        SHS_SAMPLE_DICT["organization_id"],
        SHS_SAMPLE_DICT["source_id"],
        "customer_",
    )
    assert len(record_set["data_shs"]) == 0


@pytest.mark.usefixtures("_database", "_mocked_address_object")
def test__enrich_location_area_from_lat_lon():
    insert_records("data_shs", [SHS_SAMPLE_DICT])
    location.enrich_location_area_from_lat_lon(
        "data_shs",
        SHS_SAMPLE_DICT["organization_id"],
        SHS_SAMPLE_DICT["source_id"],
        "customer_",
    )
    record = fetch_one_row("data_shs", ("uid", SHS_SAMPLE_DICT["uid"]))
    assert record["customer_country"] == "ZM"
    assert record["customer_location_area_1"] == "Central Province"
    assert record["customer_location_area_2"] == "Chibombo District"
    assert record["uid"] == SHS_SAMPLE_DICT["uid"]
