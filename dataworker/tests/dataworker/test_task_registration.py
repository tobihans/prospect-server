from dataworker.jobqueue import collect_tasks, collect_tasks_config


def test__collect_internal_tasks():
    """Non regression test for tasks."""
    assert list(collect_tasks_config("tasks/internal")) == [
        ("backfill_shs_wh", "tasks.internal.backfill_shs_wh.run"),
        (
            "crm_payment_backed_by_payment_statement",
            "tasks.internal.crm_payment_backed_by_payment_statement.run",
        ),
        (
            "fuel_sale_wallet_matches_validated_payment",
            "tasks.internal.fuel_sale_wallet_matches_validated_payment.run",
        ),
        ("meter_events", "tasks.internal.meter_events.task.run"),
        (
            "paygo_accounts_snapshots",
            "tasks.internal.paygo_accounts_snapshots.task.run",
        ),
        ("rbf_easp_ogs_is_eligible", "tasks.internal.rbf_easp.ogs_is_eligible.run"),
        ("rbf_easp_pue_is_eligible", "tasks.internal.rbf_easp.pue_is_eligible.run"),
        ("rbf_easp_ccs_is_eligible", "tasks.internal.rbf_easp.ccs_is_eligible.run"),
        (
            "rbf_easp_is_eligible_per_import",
            "tasks.internal.rbf_easp.is_eligible_per_import.run",
        ),
        (
            "update_demo_data_timestamps",
            "tasks.internal.update_demo_data_timestamps.run",
        ),
    ]


def test__internal_tasks_are_importable():
    assert len(list(collect_tasks("tasks/internal"))) == 10


def test__collect_ingestion_tasks():
    """Non regression test for tasks."""
    assert list(collect_tasks_config("tasks/ingestion")) == [
        ("aam_v3", "tasks.ingestion.aam_v3.main.transform"),
        ("airtel_zm_customer_transactions", "tasks.ingestion.airtel_zm.transform"),
        ("api/angaza", "tasks.ingestion.angaza.transform"),
        ("generic", "tasks.ingestion.generic.transform"),
        ("api/sparkmeter_koios", "tasks.ingestion.koios.transform"),
        ("mtn_zm_payments_statement", "tasks.ingestion.mtn_zm.transform"),
        ("api/payg_ops", "tasks.ingestion.payg_ops.transform"),
        ("api/solarman", "tasks.ingestion.solarman.transform"),
        ("api/spark_meter", "tasks.ingestion.spark_meter.transform"),
        ("api/stellar", "tasks.ingestion.stellar.transform"),
        ("api/stellar_v2", "tasks.ingestion.stellar_v2.transform"),
        ("sunking", "tasks.ingestion.sunking.transform"),
        ("api/supamoto", "tasks.ingestion.supamoto.transform"),
        ("api/upya", "tasks.ingestion.upya.transform"),
        ("api/victron_grids", "tasks.ingestion.victron_grids.transform"),
        ("api/victron_shs", "tasks.ingestion.victron_shs.transform"),
        ("yaka", "tasks.ingestion.yaka.transform"),
    ]


def test__ingestion_tasks_are_importable():
    assert len(list(collect_tasks("tasks/ingestion"))) == 17
