import pytest

from dataworker.jobqueue import enqueue_job, send_ingestion_task, send_internal_task


@pytest.fixture
def mock_faktory_client(mocker):
    client = mocker.Mock()
    faktory = mocker.patch("dataworker.jobqueue.faktory")
    # mocks the value returned via context manager protocol (`with ... as ...:`)
    faktory.connection.return_value.__enter__.return_value = client
    return client


def test__enqueue_job__no_args(mock_faktory_client) -> None:
    enqueue_job("task", "queue")
    mock_faktory_client.queue.assert_called_once_with(
        "task", queue="queue", args=(), retry=-1
    )


def test__enqueue_job__args(mock_faktory_client) -> None:
    enqueue_job("task", "queue", ("one", 2))
    mock_faktory_client.queue.assert_called_once_with(
        "task", queue="queue", args=("one", 2), retry=-1
    )


def test__send_ingestion_task(mock_faktory_client) -> None:
    send_ingestion_task(
        "task",
        import_id=123456,
        source_id="die_quelle",
        file_key="that_file.data",
        file_format=".py",
        data_origin="far_away",
        organization_id=654321,
    )
    mock_faktory_client.queue.assert_called_once_with(
        "task",
        queue="test_queue",
        args=(123456, "die_quelle", "that_file.data", ".py", "far_away", 654321),
        retry=-1,
    )


def test__send_internal_task(mock_faktory_client) -> None:
    send_internal_task("task")
    mock_faktory_client.queue.assert_called_once_with(
        "task",
        queue="test_queue",
        args=(),
        retry=-1,
    )


def test__send_internal_task_with_arg(mock_faktory_client) -> None:
    send_internal_task("task", "foo")
    mock_faktory_client.queue.assert_called_once_with(
        "task",
        queue="test_queue",
        args=("foo",),
        retry=-1,
    )
