from operator import attrgetter

import funcy
import pytest

from dataworker.dataset import RecordSet
from dataworker.datawarehouse import (
    TABLE_DEFAULT_METHOD,
    store_recordset,
    table_unique_constraint_columns,
)
from dataworker.queries import count_rows, fetch_one_row, table_content
from tests.conftest import MockedFireException


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None: ...


# ------------------------- table properties -------------------------


def test__datawarehouse_tables_only_one_unique_constraint() -> None:
    """All datawarehouse tables should return one and only one unique constraint."""
    for table in TABLE_DEFAULT_METHOD:
        assert table_unique_constraint_columns(table) is not None


@pytest.mark.parametrize(
    ("table", "constraint"),
    [
        ("data_custom", {"uid"}),
        ("data_grids", {"uid"}),
        ("data_grids_ts", {"grid_uid", "metered_at"}),
        (
            "data_meter_events_ts",
            {"device_uid", "start_at"},
        ),
        ("data_meters", {"uid"}),
        ("data_meters_ts", {"device_uid", "metered_at"}),
        (
            "data_paygo_accounts_snapshots_ts",
            {"account_uid", "snapshot_date"},
        ),
        ("data_payments_ts", {"uid", "paid_at"}),
        ("data_shs", {"uid"}),
        ("data_shs_ts", {"device_uid", "metered_at"}),
    ],
)
def test__datawarehouse_tables_unique_columns(table, constraint) -> None:
    """Unique constraint columns of all tables."""
    assert (
        set(
            funcy.lmap(
                attrgetter("name"),
                table_unique_constraint_columns(table),
            )
        )
        == constraint
    )


# ------------------------- STORE DATASET -------------------------

# ------ SAMPLE TABLE => INSERT


def test__store_recordset__sample_table__new_insertion(
    grid_sample_record_processed, events
) -> None:
    """Store sample inserts properly."""
    store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    # ---
    assert events == ["table_insertion_begin", "row_insert_success"]
    assert count_rows("data_grids_ts") == 1
    [row] = table_content("data_grids_ts", shed_nulls=True, string_timestamps=True)
    assert row == {
        "battery_charge_state_percent": 37.0,
        "battery_to_customer_kwh": 221.0,
        "created_at": "2024-04-08T16:43:00",
        "custom": {},
        "data_origin": "solar_coop",
        "grid_name": "La centrale solaire des Causses Vieilles",
        "grid_uid": "222222_La centrale solaire des Causses Vieilles",
        "households_connected": 32,
        "import_id": 111111,
        "interval_seconds": 300,
        "metered_at": "2024-01-01T16:00:00",
        "organization_id": 222222,
        "primary_source_to_battery_kwh": 3.0,
        "primary_source_to_customer_kwh": 188.0,
        "secondary_source_to_battery_kwh": 31.0,
        "secondary_source_to_customer_kwh": 43.0,
        "source_id": 333333,
        "updated_at": "2024-04-08T16:43:00",
    }


def test__store_recordset__sample_table__duplicate(
    grid_sample_record_processed, events
) -> None:
    """Insert a record two times, duplicate detected."""
    store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    events.clear()
    # ---
    store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    # ---
    assert count_rows("data_grids_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_duplicate"]


def test__store_recordset__sample_table__missing_mandatory_field(
    grid_sample_record_processed, events
) -> None:
    """Should produce an error."""
    del grid_sample_record_processed["metered_at"]
    store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    # ---
    assert count_rows("data_grids_ts") == 0
    assert events == ["table_insertion_begin", "row_operation_fail"]


@pytest.mark.usefixtures("_execute_sends_sqla_error")
def test__store_recordset__sample_table__sqla_error(
    grid_sample_record_processed, events
) -> None:
    """Database errors."""
    store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    # ---
    assert count_rows("data_grids_ts") == 0
    assert events == ["table_insertion_begin", "row_operation_fail"]


@pytest.mark.usefixtures("_execute_sends_exception")
def test__store_recordset__sample_table__exception(
    grid_sample_record_processed, events
) -> None:
    """Other exceptions get raised."""
    with pytest.raises(MockedFireException):
        store_recordset(RecordSet("data_grids_ts", [grid_sample_record_processed]))
    # ---
    assert count_rows("data_grids_ts") == 0
    assert events == ["table_insertion_begin"]


def test__store_recordset__blocklist_table(
    grid_sample_record_processed, events
) -> None:
    """Trying to insert in a table that is not valid will result an error."""
    with pytest.raises(ValueError):  # noqa: PT011
        store_recordset(RecordSet("forbidden_table", [grid_sample_record_processed]))
    assert events == []


# ------ ENTITY TABLE => INSERT or UPDATE


def test__store_recordset__entity_table__insertion(
    grid_record_processed, events
) -> None:
    """Insert a record, fetch it back."""
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    # ---
    assert events == ["table_insertion_begin", "row_insert_success"]
    assert count_rows("data_grids") == 1
    [inserted] = table_content(
        "data_grids", shed_nulls=True, string_timestamps=True, shed_id=True
    )
    assert inserted == {
        "country": "FR",
        "created_at": "2024-04-08T16:43:00",
        "custom": {"ambient_smell": "thym", "gnome_count": 13},
        "data_origin": "solar_coop",
        "import_id": 111111,
        "is_offgrid": True,
        "latitude": 43.8001088,
        "location_area_1": "Languedoc",
        "location_area_2": "Hérault",
        "location_area_3": "Haut-Cantons",
        "location_area_4": "Pégairolles",
        "longitude": 3.3059847,
        "name": "Peyresourde",
        "operator_company": "Coopérative Énergitique des Hauts Cantons",
        "operator_phone_e": "Cdc4E0325e7AbfBaF986",
        "operator_phone_p": "FE8c0A96aeBDd274fC51",
        "organization_id": 222222,
        "power_rating_kw": 1000.0,
        "primary_input_installed_kw": 500.0,
        "primary_input_source": "solar panel",
        "secondary_input_installed_kw": 500000.0,
        "secondary_input_source": "micro fusion generator",
        "source_id": 333333,
        "storage_capacity_kwh": 1000.0,
        "uid": "222222_Peyresourde",
        "updated_at": "2024-04-08T16:43:00",
    }


def test__store_recordset__entity_table__missing_mandatory_field(
    grid_record_processed, events
) -> None:
    """Should produce an error."""
    del grid_record_processed["name"]
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    assert count_rows("data_grids") == 0
    assert events == ["table_insertion_begin", "row_operation_fail"]


def test__store_recordset__entity_table__duplicate(
    grid_record_processed, events
) -> None:
    """Insert submitting a record with no change is skipped.

    Leads to duplicate report and no insert.
    """
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    assert count_rows("data_grids") == 1
    events.clear()
    # ---
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_insert_duplicate"]


def test__store_recordset__entity_table__subset__duplicate(
    grid_record_processed, events
) -> None:
    """Insert submitting a smaller record with no new field is skipped.

    Leads to duplicate report and no insert.
    """
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    events.clear()
    # ---
    store_recordset(
        RecordSet(
            "data_grids",
            [
                funcy.project(
                    grid_record_processed,
                    keys=[
                        "created_at",
                        "custom",
                        "data_origin",
                        "import_id",
                        "name",
                        "organization_id",
                        "source_id",
                        "uid",
                        "updated_at",
                        "country",
                    ],
                ),
            ],
        )
    )
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_insert_duplicate"]


def test__store_recordset__entity_table__update_field(
    grid_record_processed, events
) -> None:
    """Update an standard field."""
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    events.clear()
    # ---
    grid_record_processed["primary_input_source"] = "goat power wheel"
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_update_success"]
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["primary_input_source"] == "goat power wheel"


def test__store_recordset__entity_table__update_extra(
    grid_record_processed, events
) -> None:
    """Update an extra field."""
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    events.clear()
    # ---
    grid_record_processed["custom"] |= {"ambient_smell": "rosemarine"}
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_update_success"]
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["custom"] == {
        "ambient_smell": "rosemarine",
        "gnome_count": 13,
    }


def test__store_recordset__entity_table__add_extra(
    grid_record_processed, events
) -> None:
    """Add a new extra field."""
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    events.clear()
    # ---
    grid_record_processed["custom"] |= {"temperature": 15}
    store_recordset(RecordSet("data_grids", [grid_record_processed]))
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_update_success"]
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["custom"] == {
        "ambient_smell": "thym",
        "gnome_count": 13,
        "temperature": 15,
    }


@pytest.mark.usefixtures("_execute_sends_exception")
def test__store_recordset__entity_table__exception(
    grid_record_processed, events
) -> None:
    """Database returns an error, exception gets raised."""
    with pytest.raises(MockedFireException):
        store_recordset(RecordSet("data_grids", [grid_record_processed]))
    assert count_rows("data_grids") == 0
    assert events == ["table_insertion_begin"]


# ------ ENTITY TABLE => EXPLICIT UPDATE


def test__store_recordset__explicit_update__force_update_option(mocker) -> None:
    """The update flag shoud cause the update function to be called."""
    store = mocker.patch("dataworker.datawarehouse.update_records")
    store.__name__ = "something"  # prevents exception from logging statement
    required = mocker.patch("dataworker.datawarehouse.required_but_null_columns")
    required.return_value = []
    non_existing = mocker.patch("dataworker.datawarehouse.filter_non_existing_records")
    non_existing.return_value = [{"this": "that"}, {"some": "data"}]
    store_recordset(
        RecordSet("data_grids", [{"this": "that"}, {"some": "data"}]), use_update=True
    )
    assert store.call_count == 1


def test__store_recordset__explicit_updates__update_one_field(
    grid_recordset, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # updating with the same value should count as successful too
    store_recordset(
        RecordSet(
            "data_grids",
            [{"uid": record["uid"], "power_rating_kw": record["power_rating_kw"]}],
        ),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_update_success"]
    events.clear()

    # now updating with another value
    store_recordset(
        RecordSet("data_grids", [{"uid": record["uid"], "power_rating_kw": 999}]),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["power_rating_kw"] == 999


def test__store_recordset__explicit_updates__update_non_existing_uid(
    grid_recordset, events
) -> None:
    store_recordset(grid_recordset)
    events.clear()
    # update a non existing should fail
    store_recordset(
        RecordSet("data_grids", [{"uid": "wrong_uid", "power_rating_kw": 999}]),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_operation_fail"]

    # update a non-existing should allow the other to succeed
    record = grid_recordset.records[0]
    events.clear()
    store_recordset(
        RecordSet(
            "data_grids",
            [
                {"uid": "wrong_uid", "power_rating_kw": 999},
                {"uid": record["uid"], "power_rating_kw": 999},
            ],
        ),
        use_update=True,
    )
    # ---
    assert events == [
        "table_insertion_begin",
        "row_operation_fail",
        "row_update_success",
    ]


def test__store_recordset__explicit_update__update_non_existing_field(
    grid_recordset, events
) -> None:
    """Trying to update a non-existing field leads to error."""
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    store_recordset(
        RecordSet("data_grids", [{"uid": record["uid"], "wavelength": 3012}]),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_operation_fail"]


def test__store_recordset__explicit_update__missing_uid(
    grid_recordset, events, mocker
) -> None:
    """No uid provided results in rejection."""
    store_recordset(grid_recordset)
    events.clear()

    recordset = RecordSet("data_grids", [{"power_rating_kw": 999}])
    store_recordset(recordset, use_update=True)

    assert events == ["table_insertion_begin", "row_operation_fail"]


def test__store_recordset__explicit_update__insert_in_custom(
    grid_recordset, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    store_recordset(
        RecordSet("data_grids", [{"uid": record["uid"], "custom": {"combien": 1000}}]),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["custom"] == {
        "ambient_smell": "thym",
        "combien": 1000,
        "gnome_count": 13,
    }


def test__store_recordset__explicit_update__update_in_custom(
    grid_recordset, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    store_recordset(
        RecordSet(
            "data_grids",
            [
                {
                    "uid": record["uid"],
                    "custom": {"ambient_smell": "rosemarine"},
                }
            ],
        ),
        use_update=True,
    )
    # ---
    assert events == ["table_insertion_begin", "row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["custom"] == {
        "ambient_smell": "rosemarine",
        "gnome_count": 13,
    }


@pytest.mark.usefixtures("_execute_sends_exception")
def test__store_recordset__explicit_update__exception(
    grid_record_processed, events
) -> None:
    """Database returns an error, exception gets raised."""
    with pytest.raises(MockedFireException):
        store_recordset(
            RecordSet(
                "data_grids",
                [grid_record_processed],
            ),
            use_update=True,
        )
    assert events == ["table_insertion_begin"]
