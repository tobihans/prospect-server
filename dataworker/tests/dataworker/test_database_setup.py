import pytest

from dataworker.database import get_connection, get_table


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test_database_exists() -> None:
    """Verify that the test database does exist."""
    get_connection()


def test_database_schema_is_set() -> None:
    """Verify that the test database does exist."""
    for table in [
        "data_custom",
        "data_grids",
        "data_grids_ts",
        "data_meters",
        "data_meters_ts",
        "data_payments_ts",
        "data_shs",
        "data_shs_ts",
        "data_test",
    ]:
        get_table(table)
