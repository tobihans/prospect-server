import funcy
import numpy
import pandas
import pytest
import sqlalchemy

from dataworker.database import get_table
from dataworker.datawarehouse import (
    insert_or_update_record,
    insert_or_update_records,
    insert_records,
    required_but_null_columns,
    store_recordset,
    update_records,
)
from dataworker.queries import count_rows, fetch_one_row, table_content
from tests.conftest import MockedFireException
from tests.utils import prepare_for_compare


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None: ...


# ------------------------- storage methods -------------------------


# --- INSERT


def test__insert_record__success(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert a record, fetch it back."""
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    assert events == ["row_insert_success"]
    assert count_rows("data_grids") == 1
    [row] = table_content("data_grids", shed_nulls=True, string_timestamps=True)
    assert prepare_for_compare(row) == {
        "country": "FR",
        "custom": {"ambient_smell": "thym", "gnome_count": 13},
        "data_origin": "solar_coop",
        "import_id": 111111,
        "is_offgrid": True,
        "latitude": 43.8001088,
        "location_area_1": "Languedoc",
        "location_area_2": "Hérault",
        "location_area_3": "Haut-Cantons",
        "location_area_4": "Pégairolles",
        "longitude": 3.3059847,
        "name": "Peyresourde",
        "operator_company": "Coopérative Énergitique des Hauts Cantons",
        "operator_phone_e": "Cdc4E0325e7AbfBaF986",
        "operator_phone_p": "FE8c0A96aeBDd274fC51",
        "organization_id": 222222,
        "power_rating_kw": 1000.0,
        "primary_input_installed_kw": 500.0,
        "primary_input_source": "solar panel",
        "secondary_input_installed_kw": 500000.0,
        "secondary_input_source": "micro fusion generator",
        "source_id": 333333,
        "storage_capacity_kwh": 1000.0,
        "uid": "222222_Peyresourde",
    }


def test__insert_record__duplicate(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert a record two times, duplicate detected."""
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    events.clear()
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    assert count_rows("data_grids") == 1
    assert events == ["row_insert_duplicate"]


def test__insert_record__many_thousands(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert 3000 grid records creates more than 65535 parameters, requiring batch."""
    insert_records(grid_table, 3000 * [grid_record_processed], grid_unique_columns)
    assert count_rows("data_grids") == 1
    assert events == ["row_insert_success"] + 2999 * ["row_insert_duplicate"]


def test__insert_record__multiple_with_duplicate(
    grid_table, grid_recordset, grid_unique_columns, events
) -> None:
    """Insert 4 records from which two are duplicated."""
    records = [*grid_recordset.records, *grid_recordset.records]
    insert_records(grid_table, records, grid_unique_columns)
    assert count_rows("data_grids") == 2
    assert events == [
        "row_insert_success",
        "row_insert_success",
        "row_insert_duplicate",
        "row_insert_duplicate",
    ]


def test__insert_record__missing_mandatory_field(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Should produce an error."""
    del grid_record_processed["name"]
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    assert count_rows("data_grids") == 0
    assert events == []


@pytest.mark.usefixtures("_execute_sends_exception")
def test__insert_record__exception(
    grid_table,
    grid_record_processed,
    grid_unique_columns,
    events,
) -> None:
    """Database returns an error, exception gets raised."""
    with pytest.raises(MockedFireException):
        insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    assert events == []


# --- INSERT OR UPDATE


def test__insert_or_update_record__insertion(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert a record, fetch it back."""
    insert_or_update_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    assert events == ["row_insert_success"]
    assert count_rows("data_grids") == 1
    [inserted] = table_content(
        "data_grids", shed_nulls=True, string_timestamps=True, shed_id=True
    )
    assert inserted == {
        "country": "FR",
        "created_at": "2024-04-08T16:43:00",
        "custom": {"ambient_smell": "thym", "gnome_count": 13},
        "data_origin": "solar_coop",
        "import_id": 111111,
        "is_offgrid": True,
        "latitude": 43.8001088,
        "location_area_1": "Languedoc",
        "location_area_2": "Hérault",
        "location_area_3": "Haut-Cantons",
        "location_area_4": "Pégairolles",
        "longitude": 3.3059847,
        "name": "Peyresourde",
        "operator_company": "Coopérative Énergitique des Hauts Cantons",
        "operator_phone_e": "Cdc4E0325e7AbfBaF986",
        "operator_phone_p": "FE8c0A96aeBDd274fC51",
        "organization_id": 222222,
        "power_rating_kw": 1000.0,
        "primary_input_installed_kw": 500.0,
        "primary_input_source": "solar panel",
        "secondary_input_installed_kw": 500000.0,
        "secondary_input_source": "micro fusion generator",
        "source_id": 333333,
        "storage_capacity_kwh": 1000.0,
        "uid": "222222_Peyresourde",
        "updated_at": "2024-04-08T16:43:00",
    }


def test__insert_or_update_record__missing_mandatory_field(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Should produce an error."""
    del grid_record_processed["name"]
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        insert_or_update_record(grid_table, grid_record_processed, grid_unique_columns)
    assert count_rows("data_grids") == 0
    assert events == []


def test__insert_or_update_record__duplicate(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert submitting a record with no change is skipped.

    Leads to duplicate report and no insert.
    """
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    assert count_rows("data_grids") == 1
    events.clear()
    # ---
    insert_or_update_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["row_insert_duplicate"]


def test__insert_or_update_record__subset__duplicate(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Insert submitting a smaller record with no new field is skipped.

    Leads to duplicate report and no insert.
    """
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    events.clear()
    # ---
    insert_or_update_records(
        grid_table,
        [
            funcy.project(
                grid_record_processed,
                keys=[
                    "created_at",
                    "custom",
                    "data_origin",
                    "import_id",
                    "name",
                    "organization_id",
                    "source_id",
                    "uid",
                    "updated_at",
                    "country",
                ],
            )
        ],
        grid_unique_columns,
    )
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["row_insert_duplicate"]


def test__insert_or_update_record__update_field(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Update an standard field."""
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    events.clear()
    # ---
    grid_record_processed["primary_input_source"] = "goat power wheel"
    insert_or_update_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["primary_input_source"] == "goat power wheel"
    assert grid_record_processed.get("last_import_id") is None
    assert updated["last_import_id"] == grid_record_processed["import_id"]
    assert events == ["row_update_success"]


def test__insert_or_update_record__update_extra(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Update an extra field."""
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    events.clear()
    # ---
    grid_record_processed["custom"] |= {"ambient_smell": "rosemarine"}
    insert_or_update_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["custom"] == {
        "ambient_smell": "rosemarine",
        "gnome_count": 13,
    }
    assert events == ["row_update_success"]


def test__insert_or_update_record__add_extra(
    grid_table, grid_record_processed, grid_unique_columns, events
) -> None:
    """Add a new extra field."""
    insert_records(grid_table, [grid_record_processed], grid_unique_columns)
    events.clear()
    # ---
    grid_record_processed["custom"] |= {"temperature": 15}
    insert_or_update_records(grid_table, [grid_record_processed], grid_unique_columns)
    # ---
    updated = fetch_one_row("data_grids", ("uid", grid_record_processed["uid"]))
    assert updated["custom"] == {
        "ambient_smell": "thym",
        "gnome_count": 13,
        "temperature": 15,
    }
    assert events == ["row_update_success"]


@pytest.mark.usefixtures("_execute_sends_exception")
def test__insert_or_update_record__exception(
    grid_table,
    grid_record_processed,
    grid_unique_columns,
    events,
) -> None:
    """Database returns an error, exception gets raised."""
    with pytest.raises(MockedFireException):
        insert_or_update_records(
            grid_table, [grid_record_processed], grid_unique_columns
        )
    assert events == []


# --- UPDATE


def test__update_record__update_one_field(
    grid_recordset, grid_unique_columns, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    update_records(
        get_table("data_grids"),
        [{"uid": record["uid"], "power_rating_kw": 999}],
        grid_unique_columns,
    )
    # ---
    assert events == ["row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["power_rating_kw"] == 999


def test__update_record__update_non_existing_field(
    grid_recordset, grid_unique_columns, events
) -> None:
    """Trying to update a non-existing field leads to error."""
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    with pytest.raises(sqlalchemy.exc.CompileError):
        update_records(
            get_table("data_grids"),
            [{"uid": record["uid"], "wavelength": 3012}],
            grid_unique_columns,
        )
    # ---
    assert events == []


def test__update_record__missing_uid(
    grid_recordset, grid_unique_columns, events
) -> None:
    """No uid provided results in rejection."""
    store_recordset(grid_recordset)
    grid_recordset.records[0]
    events.clear()
    # ---
    with pytest.raises(RuntimeError):
        update_records(
            get_table("data_grids"),
            [{"power_rating_kw": 999}],
            grid_unique_columns,
        )
    # ---
    assert events == []


def test__update_record__insert_in_custom(
    grid_recordset, grid_unique_columns, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    update_records(
        get_table("data_grids"),
        [{"uid": record["uid"], "custom": {"combien": 1000}}],
        grid_unique_columns,
    )
    # ---
    assert events == ["row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["custom"] == {
        "ambient_smell": "thym",
        "combien": 1000,
        "gnome_count": 13,
    }


def test__update_record__update_in_custom(
    grid_recordset, grid_unique_columns, events
) -> None:
    store_recordset(grid_recordset)
    record = grid_recordset.records[0]
    events.clear()
    # ---
    update_records(
        get_table("data_grids"),
        [
            {
                "uid": record["uid"],
                "custom": {"ambient_smell": "rosemarine"},
            }
        ],
        grid_unique_columns,
    )
    # ---
    assert events == ["row_update_success"]
    updated_record = fetch_one_row("data_grids", ("uid", record["uid"]))
    assert updated_record["custom"] == {
        "ambient_smell": "rosemarine",
        "gnome_count": 13,
    }


@pytest.mark.usefixtures("_execute_sends_exception")
def test__update_record__exception(
    grid_table,
    grid_record_processed,
    grid_unique_columns,
    events,
) -> None:
    """Database returns an error, exception gets raised."""
    with pytest.raises(MockedFireException):
        update_records(grid_table, [grid_record_processed], grid_unique_columns)
    assert events == []


def test_required_but_null_columns_general_column(grid_table, grid_record_processed):
    # all fine returns an empty list
    assert required_but_null_columns(grid_table, grid_record_processed, True) == []
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    # checks for different null types
    grid_record_processed["source_id"] = None
    assert required_but_null_columns(grid_table, grid_record_processed, True) == [
        "required column source_id is None"
    ]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    grid_record_processed["source_id"] = numpy.nan
    assert required_but_null_columns(grid_table, grid_record_processed, True) == [
        "required column source_id is nan"
    ]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    grid_record_processed["source_id"] = pandas.NaT
    assert required_but_null_columns(grid_table, grid_record_processed, True) == [
        "required column source_id is NaT"
    ]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    # A second null adds one more output line
    grid_record_processed["organization_id"] = None
    req_but_null = required_but_null_columns(grid_table, grid_record_processed, True)
    assert req_but_null[0] == "required column organization_id is None"
    assert len(req_but_null) == 2
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    # Removing one item is returned and adds one more line
    del grid_record_processed["created_at"]
    req_but_null = required_but_null_columns(grid_table, grid_record_processed, True)
    assert len(req_but_null) == 3
    assert req_but_null[-1].startswith("required columns created_at not found for")
    assert "uid: 222222_Peyresourde" in req_but_null[-1]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []
    # Removing one more item is returned and does not add one more line
    del grid_record_processed["updated_at"]
    req_but_null = required_but_null_columns(grid_table, grid_record_processed, True)
    assert len(req_but_null) == 3
    assert req_but_null[-1].startswith("required columns created_at, updated_at not")
    assert "uid: 222222_Peyresourde" in req_but_null[-1]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == []


def test_required_but_null_columns_unique_index(grid_table, grid_record_processed):
    # Null-like uid (unique index col) will return it also on update
    grid_record_processed["uid"] = ""
    assert required_but_null_columns(grid_table, grid_record_processed, True) == [
        "required column uid is "
    ]
    assert required_but_null_columns(grid_table, grid_record_processed, False) == [
        "required column uid is "
    ]
    # Removing uid (unique index col) will return it also on update
    del grid_record_processed["uid"]
    req_but_null = required_but_null_columns(grid_table, grid_record_processed, True)
    assert len(req_but_null) == 1
    assert req_but_null[0].startswith("required columns uid not found")
    req_but_null = required_but_null_columns(grid_table, grid_record_processed, False)
    assert len(req_but_null) == 1
    assert req_but_null[0].startswith("required columns uid not found")
