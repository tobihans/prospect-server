import pytest

from dataworker.dataset import RecordSet
from dataworker.datawarehouse import store_recordset
from dataworker.queries import count_rows
from dataworker.task import prepare_for_storage


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None: ...


# ------------------------- Static tables -------------------------

# --- custom


def test__insert_records__insert__data_custom(
    events, custom_record, import_context
) -> None:
    recordset = RecordSet("data_custom", [custom_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_custom") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


def test__insert_records__update__data_custom(
    events, custom_record, import_context
) -> None:
    recordset = RecordSet("data_custom", [custom_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    recordset = RecordSet(
        "data_custom", [custom_record | {"hat_color": "bright yellow"}]
    )
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_custom") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
        "table_insertion_begin",
        "row_update_success",
    ]


# --- grids


def test__insert_records__insert__data_grids(
    events, grid_record, import_context
) -> None:
    recordset = RecordSet("data_grids", [grid_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_grids") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


def test__insert_records__update__data_grids(
    events, grid_record, import_context
) -> None:
    recordset = RecordSet("data_grids", [grid_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    recordset = RecordSet("data_grids", [grid_record | {"power_rating_kw": 3210}])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_grids") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
        "table_insertion_begin",
        "row_update_success",
    ]


# --- meters


def test__insert_records__insert__data_meters(
    events, meter_record, import_context
) -> None:
    recordset = RecordSet("data_meters", [meter_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_meters") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


def test__insert_records__update__data_meters(
    events, meter_record, import_context
) -> None:
    recordset = RecordSet("data_meters", [meter_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    recordset = RecordSet(
        "data_meters", [meter_record | {"customer_location_area_1": "brandenburg"}]
    )
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_meters") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
        "table_insertion_begin",
        "row_update_success",
    ]


# --- solar system


def test__insert_records__insert__data_shs(
    events, solar_system_record, import_context
) -> None:
    recordset = RecordSet("data_shs", [solar_system_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_shs") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


def test__insert_records__update__data_shs(
    events, solar_system_record, import_context
) -> None:
    recordset = RecordSet("data_shs", [solar_system_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    recordset = RecordSet(
        "data_shs", [solar_system_record | {"customer_phone_p": "123rst456wfp"}]
    )
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_shs") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
        "table_insertion_begin",
        "row_update_success",
    ]


# --- payments


def test__insert_records__insert__data_payments_ts(
    events, payment_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet("data_payments_ts", [payment_sample_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_payments_ts") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
    ]


def test__insert_records__update__data_payments_ts(
    events, payment_sample_record, import_context
) -> None:
    """Store sample updates properly."""
    # payments_ts is the only measurement table that is updatable
    recordset = RecordSet("data_payments_ts", [payment_sample_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    recordset = RecordSet(
        "data_payments_ts", [payment_sample_record | {"currency": "DEM"}]
    )
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_payments_ts") == 1
    assert events == [
        "table_insertion_begin",
        "row_insert_success",
        "table_insertion_begin",
        "row_update_success",
    ]


# ------------------------- Dynamic tables -------------------------


# --- grid sample


def test__insert_records__insert__data_grids_ts(
    events, grid_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet("data_grids_ts", [grid_sample_record])
    recordset = prepare_for_storage(recordset, import_context=import_context)
    store_recordset(recordset)
    # ---
    assert count_rows("data_grids_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


# --- events


def test__insert_records__insert__data_meter_events_ts(
    events, meter_event_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet("data_meter_events_ts", [meter_event_sample_record])
    recordset = prepare_for_storage(
        recordset, import_context=import_context, is_internal=True
    )
    store_recordset(recordset)
    # ---
    assert count_rows("data_meter_events_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


# --- meter sample


def test__insert_records__insert__data_meters_ts(
    events, meter_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet("data_meters_ts", [meter_sample_record])
    recordset = prepare_for_storage(
        recordset, import_context=import_context, is_internal=True
    )
    store_recordset(recordset)
    # ---
    assert count_rows("data_meters_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


# --- paygo


def test__insert_records__insert__data_paygo_accounts_snapshots_ts(
    events, paygo_account_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet(
        "data_paygo_accounts_snapshots_ts", [paygo_account_sample_record]
    )
    recordset = prepare_for_storage(
        recordset, import_context=import_context, is_internal=True
    )
    store_recordset(recordset)
    # ---
    assert count_rows("data_paygo_accounts_snapshots_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]


# --- solar system sample


def test__insert_records__insert__data_shs_ts(
    events, solar_system_sample_record, import_context
) -> None:
    """Store sample inserts properly."""
    recordset = RecordSet("data_shs_ts", [solar_system_sample_record])
    recordset = prepare_for_storage(
        recordset, import_context=import_context, is_internal=True
    )
    store_recordset(recordset)
    # ---
    assert count_rows("data_shs_ts") == 1
    assert events == ["table_insertion_begin", "row_insert_success"]
