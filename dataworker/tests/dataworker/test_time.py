import arrow

from dataworker.time import (
    parse_utc_ms_timestamp,
    parse_utc_s_timestamp,
    parse_utc_us_timestamp,
    stringify_date,
)


def test__parse_utc_s_timestamp() -> None:
    assert parse_utc_s_timestamp(1669910124).isoformat() == "2022-12-01T15:55:24+00:00"
    assert (
        parse_utc_s_timestamp("1669910124").isoformat() == "2022-12-01T15:55:24+00:00"
    )


def test__parse_utc_ms_timestamp() -> None:
    assert (
        parse_utc_ms_timestamp(1669910124159).isoformat()
        == "2022-12-01T15:55:24.159000+00:00"
    )
    assert (
        parse_utc_ms_timestamp("1669910124159").isoformat()
        == "2022-12-01T15:55:24.159000+00:00"
    )


def test__parse_utc_us_timestamp() -> None:
    assert (
        parse_utc_us_timestamp(1669910124159167).isoformat()
        == "2022-12-01T15:55:24.159167+00:00"
    )
    assert (
        parse_utc_us_timestamp("1669910124159167").isoformat()
        == "2022-12-01T15:55:24.159167+00:00"
    )


def test__stringify_dates() -> None:
    assert stringify_date(12) == 12
    assert stringify_date("something") == "something"
    assert (
        stringify_date(arrow.get(2023, 6, 19).datetime) == "2023-06-19T00:00:00+00:00"
    )
