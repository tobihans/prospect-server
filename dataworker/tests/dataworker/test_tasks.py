import arrow
import pytest
import time_machine

from dataworker.queries import count_rows, fetch_one_row, table_content
from tests.utils import prepare_for_compare


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


@pytest.fixture
def insert_via_generic_IngestionTask(
    load_datafile_mock, generic_IngestionTask, etl_parameters
):
    """Function that injects data into the loader and calls a generic IngestionTask."""

    def insert_records_(dataset) -> None:
        load_datafile_mock.return_value = dataset
        generic_IngestionTask(**etl_parameters)

    return insert_records_


# ------------------------- IngestionTask - Default Insert -------------------------


@time_machine.travel("2024-03-14T20:19:42")
def test__IngestionTask_postprocessing__grid_record_postprocessing(
    grid_record, insert_via_generic_IngestionTask
) -> None:
    """A simple IngestionTask execution."""
    insert_via_generic_IngestionTask({"grids": [grid_record]})
    # ---
    [grid] = table_content("data_grids", shed_nulls=True, string_timestamps=True)
    assert prepare_for_compare(grid) == {
        "country": "FR",
        "custom": {"ambient_smell": "thym", "gnome_count": 13},
        "data_origin": "solar_coop",
        "import_id": 111111,
        "is_offgrid": True,
        "latitude": 43.8001088,
        "location_area_1": "Languedoc",
        "location_area_2": "Hérault",
        "location_area_3": "Haut-Cantons",
        "location_area_4": "Pégairolles",
        "longitude": 3.3059847,
        "name": "Peyresourde",
        "operator_company": "Coopérative Énergitique des Hauts Cantons",
        "operator_phone_e": "Cdc4E0325e7AbfBaF986",
        "operator_phone_p": "FE8c0A96aeBDd274fC51",
        "organization_id": 222222,
        "power_rating_kw": 1000.0,
        "primary_input_installed_kw": 500.0,
        "primary_input_source": "solar panel",
        "secondary_input_installed_kw": 500000.0,
        "secondary_input_source": "micro fusion generator",
        "source_id": 333333,
        "storage_capacity_kwh": 1000.0,
        "uid": "222222_Peyresourde",
    }


def test__IngestionTask_postprocessing__timestamps_ignored(
    grid_record_editable, insert_via_generic_IngestionTask
) -> None:
    """Some fields should not be possible to be set from an IngestionTask.

    Especially timestamp fields, 'created_at' and 'updated_at'
    """
    changed_date = arrow.get("2000-01-01").datetime
    grid_record_editable |= {
        "created_at": changed_date,
        "updated_at": changed_date,
    }
    insert_via_generic_IngestionTask({"grids": [grid_record_editable]})
    # ---
    inserted_sample = table_content(
        "data_grids", shed_nulls=True, string_timestamps=True
    )[0]
    assert inserted_sample["created_at"] != changed_date
    assert inserted_sample["updated_at"] != changed_date


def test__IngestionTask_postprocessing__import_context_ignored(
    grid_record_editable, insert_via_generic_IngestionTask
) -> None:
    """None of the four import context fields should be set from a datafile.

    Those are set in the context module of a dataworker task.
    """
    grid_record_editable |= {
        "import_id": 0000,
        "source_id": 0000,
        "data_origin": "behind the bush",
        "organization_id": 0000,
    }
    insert_via_generic_IngestionTask({"grids": [grid_record_editable]})
    # ---
    inserted_sample = table_content(
        "data_grids", shed_nulls=True, string_timestamps=True
    )[0]
    assert inserted_sample["import_id"] == 111111
    assert inserted_sample["source_id"] == 333333
    assert inserted_sample["data_origin"] == "solar_coop"
    assert inserted_sample["organization_id"] == 222222


def test__IngestionTask__database_fields_ignored(
    grid_record_editable, insert_via_generic_IngestionTask
) -> None:
    """Test IngestionTask database fields aren't set by the dataworker.

    last_import_id and last_source_id are set by the database directly.
    Those should not be set from the dataworker at all.
    """
    grid_record_editable |= {
        "last_import_id": 222222,
        "last_source_id": 1111,
    }
    insert_via_generic_IngestionTask({"grids": [grid_record_editable]})
    # ---
    inserted_sample = table_content(
        "data_grids", shed_nulls=True, string_timestamps=True
    )[0]
    assert inserted_sample.get("last_import_id") != 2222
    assert inserted_sample.get("last_source_id") != 1111


def test__IngestionTask__ids_ignored(
    grid_record_editable, insert_via_generic_IngestionTask
) -> None:
    # no uids please
    grid_record_editable |= {
        "barbecue_uid": "grill_that_sausage",
        "socks_uid": "orange_winter_socks",
        "id": 123456789,
    }
    insert_via_generic_IngestionTask({"grids": [grid_record_editable]})
    # ---
    inserted = table_content("data_grids")
    assert inserted[0]["uid"] != "not_the_one_we_want"
    assert "barbecue_uid" not in inserted[0]
    assert "barbecue_uid" not in inserted[0]["custom"]
    assert "socks_uid" not in inserted[0]
    assert "socks_uid" not in inserted[0]["custom"]
    assert "id" not in inserted[0]
    assert "id" not in inserted[0]["custom"]


def test__IngestionTask__custom_ignored(
    grid_record_editable, insert_via_generic_IngestionTask
) -> None:
    # temporary, so that user don't rely on the current behavior before we
    # change it. no custom update for now
    grid_record_editable |= {
        "custom": {"let": "me", "put": "data", "where": "I", "want": "please"},
    }
    insert_via_generic_IngestionTask({"grids": [grid_record_editable]})
    # ---
    inserted = table_content("data_grids")
    assert len(inserted) == 1
    assert "let" not in inserted[0]["custom"]
    assert "put" not in inserted[0]["custom"]
    assert "where" not in inserted[0]["custom"]
    assert "want" not in inserted[0]["custom"]


# ------------------------- IngestionTask - Explicit Update -------------------------


def test__IngestionTask__explicit_update__import_context(
    insert_via_generic_IngestionTask, store_recordset_mock
) -> None:
    """Explicit update flow should create new import context."""
    insert_via_generic_IngestionTask({
        "_update": "grids",
        "grids": [
            {
                "uid": "that_sample_solar_coop",
                "latitude": 43.8026942,
                "longitude": 3.3229438,
            }
        ],
    })
    # ---
    ((recordset,), options) = store_recordset_mock.call_args
    assert options == {"use_update": True}
    assert recordset.table == "data_grids"
    assert recordset.records == [
        {
            "latitude": 43.8026942,
            "longitude": 3.3229438,
            "uid": "that_sample_solar_coop",
            "data_origin": "solar_coop",
            "import_id": "111111",
            "organization_id": "222222",
            "source_id": "333333",
        },
    ]


def test__explicit_update__not_updatable(
    insert_via_generic_IngestionTask, store_recordset_mock
) -> None:
    """Explicit update context building should ignore certain fields.

    Any given organization_id or data_origin value should be ignored.
    """
    insert_via_generic_IngestionTask({
        "_update": "grids",
        "grids": [
            {
                "uid": "that_sample_solar_coop",
                "organization_id": 9,
                "data_origin": "a_whole_new_world",
            }
        ],
    })
    # ---
    ((recordset,), options) = store_recordset_mock.call_args
    assert options == {"use_update": True}
    assert recordset.table == "data_grids"
    assert recordset.records == [
        {
            "data_origin": "solar_coop",
            "organization_id": "222222",
            "uid": "that_sample_solar_coop",
            "import_id": "111111",
            "source_id": "333333",
        },
    ]


def test__generic_IngestionTask__explicit_update__success(
    events, grid_record, other_grid_record, insert_via_generic_IngestionTask
):
    """Simplest successful explicit update, via generic upload IngestionTask."""
    insert_via_generic_IngestionTask({"grids": [grid_record, other_grid_record]})
    # fetch back uid from
    first_grid = fetch_one_row("data_grids", ("name", grid_record["name"]))
    events.clear()
    insert_via_generic_IngestionTask({
        "_update": "grids",
        "grids": [{"uid": first_grid["uid"], "power_rating_kw": 1000}],
    })
    # ---
    assert count_rows("data_grids") == 2
    assert events == [
        "start",
        "table_insertion_begin",
        "row_update_success",
        "complete",
        "end",
    ]
    assert table_content(
        "data_grids", fields=("uid", "power_rating_kw"), sort="uid"
    ) == [
        {"power_rating_kw": 1000.0, "uid": "222222_Peyresourde"},
        {"power_rating_kw": 1000.0, "uid": "222222_Tournadous"},
    ]


def test__generic_IngestionTask__explicit_update__no_metadata(
    events, grid_record, other_grid_record, insert_via_generic_IngestionTask
):
    """Metadata get filtered during explicit update."""
    insert_via_generic_IngestionTask({"grids": [grid_record, other_grid_record]})
    # fetch back uid from
    first_grid = fetch_one_row("data_grids", ("name", grid_record["name"]))
    events.clear()
    insert_via_generic_IngestionTask({
        "_update": "grids",
        "grids": [
            {
                "uid": first_grid["uid"],
                "power_rating_kw": 1000,
                "import_id": 1000,  # import metadata
                "id": 1000,  # internal id
                "created_at": arrow.get("1986-06-19").datetime,  # timestamp
            }
        ],
    })
    # ---
    assert count_rows("data_grids") == 2
    assert events == [
        "start",
        "table_insertion_begin",
        "row_update_success",
        "complete",
        "end",
    ]
    first_grid_updated = fetch_one_row("data_grids", ("uid", first_grid["uid"]))
    # none of the metadata got through the update
    assert "id" not in first_grid_updated
    assert first_grid_updated["import_id"] != 1000
    assert first_grid_updated["created_at"] != arrow.get("1986-06-19").datetime
    # but update was successful
    assert first_grid_updated["power_rating_kw"] == 1000
