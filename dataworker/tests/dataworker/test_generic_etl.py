import pytest

from dataworker.queries import count_rows
from dataworker.task import IngestionTask
from tasks.ingestion import generic


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    generic_etl = IngestionTask("generic", generic.transform)
    generic_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/generic_meter.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    assert count_rows("data_meters") == 4
