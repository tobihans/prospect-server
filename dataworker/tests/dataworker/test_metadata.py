from dataclasses import asdict
from datetime import UTC, datetime
from zoneinfo import ZoneInfo

import arrow
import funcy
import pytest

from dataworker import metadata

# ------------------------- build uid -------------------------


@pytest.mark.parametrize(
    ("datetime", "expected_str"),
    [
        (
            datetime(2023, 5, 22, 16, 46, 29, 495853, tzinfo=UTC),
            "2023-05-22T16:46:29.495853+00:00",
        ),
        (
            datetime(2023, 5, 22, 16, 46, 29, 495853, tzinfo=ZoneInfo("Europe/Berlin")),
            "2023-05-22T14:46:29.495853+00:00",
        ),  # this generates a 2:00 offset
        (
            arrow.Arrow(2023, 5, 22, 16, 46, 29, 495853, tzinfo=UTC),
            "2023-05-22T16:46:29.495853+00:00",
        ),
        (
            arrow.Arrow(
                2023, 5, 22, 16, 46, 29, 495853, tzinfo=ZoneInfo("Europe/Berlin")
            ),
            "2023-05-22T14:46:29.495853+00:00",
        ),  # this generates a 2:00 offset
    ],
)
def test__uid_timestamp_element_format(datetime, expected_str) -> None:
    """Checks that the timestamp follow the desired format."""
    assert metadata.uid_timestamp_element_format(datetime) == expected_str


def test__build_uid__strings() -> None:
    assert metadata._build_uid(["this", "that"]) == "this_that"
    assert metadata._build_uid(["pif", "paf", "pouf"]) == "pif_paf_pouf"


def test__build_uid__string_conversion() -> None:
    assert metadata._build_uid(["this", 12]) == "this_12"


def test__build_uid__timestamps() -> None:
    assert (
        metadata._build_uid([
            "this",
            datetime(2023, 5, 22, 16, 46, 29, 495853, tzinfo=UTC),
        ])
        == "this_2023-05-22T16:46:29.495853+00:00"
    )


def test__build_uid__missing_element() -> None:
    assert metadata._build_uid(["this", None, "that"]) is None


# ------------------------- collect extra fields -------------------------


@pytest.mark.usefixtures("_database")
def test__table_fields() -> None:
    assert metadata._table_fields("data_grids") == [
        "country",
        "created_at",
        "custom",
        "data_origin",
        "external_id",
        "import_id",
        "is_offgrid",
        "last_import_id",
        "last_source_id",
        "latitude",
        "location_area_1",
        "location_area_2",
        "location_area_3",
        "location_area_4",
        "longitude",
        "name",
        "operator_company",
        "operator_phone_e",
        "operator_phone_p",
        "organization_id",
        "power_rating_kw",
        "primary_input_installed_kw",
        "primary_input_source",
        "secondary_input_installed_kw",
        "secondary_input_source",
        "source_id",
        "storage_capacity_kwh",
        "uid",
        "updated_at",
    ]


def test__collect_dataset_extra_fields(mocker) -> None:
    mocker.patch(
        "dataworker.metadata._table_fields",
        return_value=["voltage", "current", "custom"],
    )
    file_data = {"device": [{"temp": 12, "volume": 45, "voltage": 14, "current": 1000}]}
    assert metadata.collect_dataset_extra_fields(file_data) == {
        "device": [
            {"voltage": 14, "current": 1000, "custom": {"temp": 12, "volume": 45}}
        ]
    }


def test__collect_dataset_extra_fields_without_custom_field(mocker) -> None:
    mocker.patch(
        "dataworker.metadata._table_fields", return_value=["voltage", "current"]
    )
    file_data = {"device": [{"temp": 12, "volume": 45, "voltage": 14, "current": 1000}]}
    assert metadata.collect_dataset_extra_fields(file_data) == {
        "device": [{"voltage": 14, "current": 1000}]
    }


def test__collect_dataset_extra_fields__serialize_datetimes(mocker) -> None:
    mocker.patch(
        "dataworker.metadata._table_fields",
        return_value=["voltage", "current", "custom"],
    )

    file_data = {
        "device": [
            {
                "temp": 12,
                "volume": 45,
                "voltage": 14,
                "current": 1000,
                "time": arrow.get(2023, 6, 19).datetime,
            }
        ]
    }
    assert metadata.collect_dataset_extra_fields(file_data) == {
        "device": [
            {
                "voltage": 14,
                "current": 1000,
                "custom": {
                    "temp": 12,
                    "volume": 45,
                    "time": "2023-06-19T00:00:00+00:00",
                },
            }
        ]
    }


# ------------------------- table-specific uid building -------------------------


def test__custom_uid_builder(custom_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            custom_record | asdict(import_context), "data_custom"
        )["uid"]
        == "222222_764-EXTID-549"
    )


def test__grids_uid_builder(grid_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            grid_record | asdict(import_context), "data_grids"
        )["uid"]
        == "222222_Peyresourde"
    )


def test__grids_ts_uid_builder(grid_sample_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            grid_sample_record | asdict(import_context), "data_grids_ts"
        )["grid_uid"]
        == "222222_La centrale solaire des Causses Vieilles"
    )


def test__meters_uid_builder(meter_record, import_context) -> None:
    assert funcy.project(
        metadata.add_record_identifiers(
            meter_record | asdict(import_context), "data_meters"
        ),
        ["device_uid", "customer_uid", "account_uid", "grid_uid", "uid"],
    ) == {
        "account_uid": "222222_solar_coop_ags-761-qqq",
        "customer_uid": "222222_solar_coop_123-rst-987",
        "device_uid": "222222_Ateliers Escudier_654-789-321",
        "grid_uid": "222222_Société Générale de l'Énergie des Cévènes",
        "uid": "222222_solar_coop_123-rst-987_222222_Ateliers Escudier_654-789-321",
    }


def test__meters_ts_uid_builder(meter_sample_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            meter_sample_record | asdict(import_context), "data_meters_ts"
        )["device_uid"]
        == "222222_Ateliers Mécaniques de Lagamas_456-zzz-761"
    )


def test__meter_events_ts_uid_builder(
    meter_event_sample_record, import_context
) -> None:
    assert (
        metadata.add_record_identifiers(
            meter_event_sample_record | asdict(import_context), "data_meter_events_ts"
        )["device_uid"]
        == "222222_Manufacture De Ciseaux de Précision de la Narbonnaise_123-456-789"
    )


def test__payments_ts_uid_builder(payment_sample_record, import_context) -> None:
    assert funcy.project(
        metadata.add_record_identifiers(
            payment_sample_record | asdict(import_context), "data_payments_ts"
        ),
        ["uid", "account_uid"],
    ) == {
        "account_uid": "222222_solar_coop_312465497845",
        "uid": (
            "shs_222222_solar_coop_312465497845_ats-789-trs_2024-04-17T14:00:00+00:00"
        ),
    }


def test__shs_uid_builder(solar_system_record, import_context) -> None:
    assert funcy.project(
        metadata.add_record_identifiers(
            solar_system_record | asdict(import_context), "data_shs"
        ),
        ["uid", "device_uid", "customer_uid", "account_uid"],
    ) == {
        "account_uid": "222222_solar_coop_123-arst-2012",
        "customer_uid": "222222_solar_coop_648132",
        "device_uid": "222222_Atelier Solaire de Montpeyroux_987654324",
        "uid": "222222_solar_coop_648132_222222_Atelier Solaire de Montpeyroux_987654324",  # noqa: E501
    }


def test__shs_ts_uid_builder(solar_system_sample_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            solar_system_sample_record | asdict(import_context), "data_shs_ts"
        )["device_uid"]
        == "222222_L'atelier d'Émile_ATR-1000"
    )


def test__trust_trace_uid_builder(trust_trace_record, import_context) -> None:
    assert (
        metadata.add_record_identifiers(
            trust_trace_record | asdict(import_context), "data_trust_trace"
        )["uid"]
        == "payments_ts_chop-an-hour_check-if-life-has-a-meaning"
    )


def test__paygo_accounts_snapshot_ts_uid_builder(
    paygo_account_sample_record, import_context
) -> None:
    assert (
        metadata.add_record_identifiers(
            paygo_account_sample_record | asdict(import_context),
            "data_paygo_accounts_snapshots_ts",
        )["account_uid"]
        == "222222_solar_coop_123-aat-731"
    )
