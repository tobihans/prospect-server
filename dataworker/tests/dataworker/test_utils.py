from dataworker.utils import unpack

# ------------------------- generic utils -------------------------


def test__unpack():
    def two_sum(a, b):  # noqa: FURB118
        return a + b

    assert unpack(two_sum)((1, 2)) == 3
