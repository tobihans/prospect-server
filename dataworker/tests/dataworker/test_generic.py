import tasks.ingestion.generic


def test__preserve_and_alter_flag_str_value() -> None:
    assert tasks.ingestion.generic.transform({
        "_update": "grids",
        "grids": [{"this": "that", "plic": 3012}],
    }) == {"_update": "data_grids", "data_grids": [{"this": "that", "plic": 3012}]}


def test__preserve_and_alter_flag_list_value() -> None:
    assert tasks.ingestion.generic.transform({
        "_update": ["grids"],
        "grids": [{"this": "that", "plic": 3012}],
    }) == {"_update": ["data_grids"], "data_grids": [{"this": "that", "plic": 3012}]}


def test__preserve_and_alter_flag_list_value_multiple() -> None:
    assert tasks.ingestion.generic.transform({
        "_update": ["grids", "meters"],
        "grids": [{"this": "that", "plic": 3012}],
    }) == {
        "_update": ["data_grids", "data_meters"],
        "data_grids": [{"this": "that", "plic": 3012}],
    }
