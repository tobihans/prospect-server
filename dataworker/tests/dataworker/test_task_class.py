from dataclasses import asdict

import pytest

from dataworker.queries import table_content
from dataworker.task import IngestionTask, InternalTask, StoringTaskBase, Task
from dataworker.tracking import Event


@pytest.fixture(autouse=True)
def _module_autouse(_database, now) -> None:
    """Common fixtures required for this module."""
    ...


@pytest.fixture
def failing_function(mocker):
    return mocker.Mock(side_effect=RuntimeError())


@pytest.fixture
def _mocked_task_post_init(mocker):
    """This allows to avoid an Attribute Error that mock has no `__name__`."""
    mocker.patch("dataworker.task.Task.__post_init__", return_value=None)


@pytest.fixture
def task_and_mocked_function(mocker, _mocked_task_post_init):
    function = mocker.Mock(return_value=None)
    task = Task.from_function(function)
    return task, function


# ------------------------- Task -------------------------


def test__Task__creation():
    def function():
        pass

    task = Task.from_function(function)
    assert task.name == "test_task_class"


def test__Task__repr():
    def function():
        pass

    task = Task(name="bake_biscuit", function=function)
    assert (
        repr(task) == "Task('bake_biscuit','tests.dataworker.test_task_class.function')"
    )


def test__Task__running_function__no_arg(task_and_mocked_function):
    task, function = task_and_mocked_function
    task()
    function.assert_called_once_with()


def test__Task__running_function__with_arg(task_and_mocked_function, caplog):
    task, function = task_and_mocked_function
    task("this", "that")
    function.assert_called_once_with("this", "that")
    log_extras = caplog.records[-1].extra  # Test we contextualize the logs properly
    assert log_extras["task"] == "mock"
    assert log_extras["import_id"] == "this"  # the first arg is logged as import_id
    assert log_extras["event"] == Event.END


@pytest.mark.usefixtures("_mocked_task_post_init")
def test__Task__running_function__exception_passes_through(failing_function):
    task = Task.from_function(failing_function)
    with pytest.raises(RuntimeError):
        task()


# ------------------------- StoringTaskBase ----------------------


def test__StoringTaskBase__creation():
    def function(): ...

    task = StoringTaskBase.from_function(function)
    assert task.name == "test_task_class"


@pytest.mark.usefixtures("_mocked_task_post_init")
def test__StoringTaskBase_call__raises(mocker):
    function = mocker.Mock(return_value=None, name="foo")
    task = StoringTaskBase.from_function(function)
    with pytest.raises(NotImplementedError):
        task()


# ------------------------- InternalTask -------------------------


def test__InternalTask__creation():
    def no_arg_return_nothing(): ...

    internal_task = InternalTask.from_function(no_arg_return_nothing)
    assert internal_task.name == "test_task_class"


def test__InternalTask__function_accepts_import_id_arg(mocker, caplog):
    def one_arg_return_nothing(import_id): ...

    internal_task = InternalTask.from_function(one_arg_return_nothing)
    store_dataset = mocker.spy(InternalTask, "store_dataset")

    # call with args
    internal_task(1)

    dataset = store_dataset.call_args.args[1]  # Task.store_dataset(self,dataset)
    assert dataset.as_dict() == {}  # dataset send for storage is empty
    log_extras = caplog.records[-1].extra  # Test we contextualize the logs properly
    assert log_extras["task"] == "test_task_class"
    assert log_extras["import_id"] == 1
    assert log_extras["event"] == Event.END

    # call with kwargs
    internal_task(import_id=1)

    dataset = store_dataset.call_args.args[1]  # Task.store_dataset(self,dataset)
    assert dataset.as_dict() == {}  # dataset send for storage is empty
    log_extras = caplog.records[-1].extra  # Test we contextualize the logs properly
    assert log_extras["task"] == "test_task_class"
    assert log_extras["import_id"] == 1
    assert log_extras["event"] == Event.END


def test__InternalTask__function_more_than_one_arg_raises(mocker):
    def one_arg_return_nothing(import_id): ...

    internal_task = InternalTask.from_function(one_arg_return_nothing)

    with pytest.raises(TypeError, match="more than one unnamed arg"):
        internal_task(1, 2)


def test__InternalTask__function_accepts_no_other_arg():
    """InternalTask is aimed to be used with a function accepting import_id."""

    def one_arg_return_nothing(a): ...

    with pytest.raises(TypeError, match=r"strictly.*import_id.*args"):
        InternalTask.from_function(one_arg_return_nothing)


def test__InternalTask__none_returning_function(mocker):
    def no_arg_return_nothing(): ...

    internal_task = InternalTask.from_function(function=no_arg_return_nothing)
    store_dataset = mocker.spy(InternalTask, "store_dataset")
    # ---
    internal_task()
    # ---
    dataset = store_dataset.call_args.args[1]  # Task.store_dataset(self,dataset)
    assert dataset.as_dict() == {}  # dataset send for storage is empty


def test__InternalTask__store(grid_record, import_context):
    def function():
        return {
            "data_grids": [
                grid_record | asdict(import_context),
            ]
        }

    internal_task = InternalTask.from_function(function)
    # ---
    internal_task()
    # ---
    assert table_content("data_grids", string_timestamps=True, shed_id=True) == [
        {
            "country": "FR",
            "created_at": "2024-03-18T22:23:37",
            "custom": {"ambient_smell": "thym", "gnome_count": 13},
            "data_origin": "solar_coop",
            "external_id": None,
            "import_id": 111111,
            "is_offgrid": True,
            "last_import_id": None,
            "last_source_id": None,
            "latitude": 43.8001088,
            "location_area_1": "Languedoc",
            "location_area_2": "Hérault",
            "location_area_3": "Haut-Cantons",
            "location_area_4": "Pégairolles",
            "longitude": 3.3059847,
            "name": "Peyresourde",
            "operator_company": "Coopérative Énergitique des Hauts Cantons",
            "operator_phone_e": "Cdc4E0325e7AbfBaF986",
            "operator_phone_p": "FE8c0A96aeBDd274fC51",
            "organization_id": 222222,
            "power_rating_kw": 1000.0,
            "primary_input_installed_kw": 500.0,
            "primary_input_source": "solar panel",
            "secondary_input_installed_kw": 500000.0,
            "secondary_input_source": "micro fusion generator",
            "source_id": 333333,
            "storage_capacity_kwh": 1000.0,
            "uid": "222222_Peyresourde",
            "updated_at": "2024-03-18T22:23:37",
        },
    ]


# ------------------------- IngestionTask -------------------------


def test__IngestionTask__creation():
    def one_arg_return_nothing(file_data): ...

    ingestion_task = IngestionTask.from_function(one_arg_return_nothing)
    assert ingestion_task.name == "test_task_class"


def test__IngestionTask__function_accepts_file_data_arg(mocker, caplog):
    def one_arg_return_nothing(file_data): ...

    mocker.patch("dataworker.task.read_datafile", return_value={})
    ingestion_task = IngestionTask.from_function(one_arg_return_nothing)
    store_dataset = mocker.spy(IngestionTask, "store_dataset")

    # calling with args
    ingestion_task(1, 2, 3, 4, 5, 6)

    dataset = store_dataset.call_args.args[1]  # Task.store_dataset(self,dataset)
    assert dataset.as_dict() == {}  # dataset send for storage is empty
    log_extras = caplog.records[-1].extra  # Test we contextualize the logs properly
    assert log_extras["task"] == "test_task_class"
    assert log_extras["import_id"] == 1
    assert log_extras["event"] == Event.END

    # same, but calling with kwargs
    ingestion_task(
        import_id=1,
        source_id=2,
        file_key=3,
        file_format=4,
        data_origin=5,
        organization_id=6,
    )

    dataset = store_dataset.call_args.args[1]  # Task.store_dataset(self,dataset)
    assert dataset.as_dict() == {}  # dataset send for storage is empty
    log_extras = caplog.records[-1].extra  # Test we contextualize the logs properly
    assert log_extras["task"] == "test_task_class"
    assert log_extras["import_id"] == 1
    assert log_extras["organization_id"] == 6
    assert log_extras["event"] == Event.END


def test__IngestionTask__function_accepts_no_other_arg():
    """InternalTask is aimed to be used with a function accepting import_id."""

    def one_arg_return_nothing(a): ...

    with pytest.raises(TypeError, match=r"strictly.*file_data.*args"):
        IngestionTask.from_function(one_arg_return_nothing)


def test__IngestionTask__no_arg_function_raises():
    def no_arg_return_nothing(): ...

    with pytest.raises(TypeError, match="requires argument, None was given"):
        IngestionTask.from_function(function=no_arg_return_nothing)


def test__IngestionTask__wrong_args_raises(mocker):
    def one_arg_return_nothing(file_data): ...

    mocker.patch("dataworker.task.read_datafile", side_effect=Exception)
    ingestion_task = IngestionTask.from_function(one_arg_return_nothing)

    with pytest.raises(TypeError, match="should be called with 6 args or 6 kwargs"):
        ingestion_task(1, 2, 3, 4, 5)

    with pytest.raises(TypeError, match="should be called with 6 args or 6 kwargs"):
        ingestion_task(
            import_id=1,
            source_id=2,
            file_key=3,
            file_format=4,
            data_origin=5,
        )


def test__IngestionTask__read_datafile_raises(mocker):
    def one_arg_return_nothing(file_data): ...

    mocker.patch("dataworker.task.read_datafile", side_effect=Exception)
    ingestion_task = IngestionTask.from_function(one_arg_return_nothing)

    with pytest.raises(RuntimeError, match="error while reading datafile"):
        ingestion_task(1, 2, 3, 4, 5, 6)


def test__IngestionTask__insertion(grid_record, import_context, mocker):
    def function(file_data):
        return {
            "data_grids": [
                grid_record | asdict(import_context),
            ]
        }

    mocker.patch("dataworker.task.read_datafile", return_value={})
    ingestion_task = IngestionTask.from_function(function)
    # ---
    ingestion_task(
        import_context.import_id,
        import_context.source_id,
        "",
        "",
        import_context.data_origin,
        import_context.organization_id,
    )
    # ---
    assert table_content("data_grids", string_timestamps=True, shed_id=True) == [
        {
            "country": "FR",
            "created_at": "2024-03-18T22:23:37",
            "custom": {"ambient_smell": "thym", "gnome_count": 13},
            "data_origin": "solar_coop",
            "external_id": None,
            "import_id": 111111,
            "is_offgrid": True,
            "last_import_id": None,
            "last_source_id": None,
            "latitude": 43.8001088,
            "location_area_1": "Languedoc",
            "location_area_2": "Hérault",
            "location_area_3": "Haut-Cantons",
            "location_area_4": "Pégairolles",
            "longitude": 3.3059847,
            "name": "Peyresourde",
            "operator_company": "Coopérative Énergitique des Hauts Cantons",
            "operator_phone_e": "Cdc4E0325e7AbfBaF986",
            "operator_phone_p": "FE8c0A96aeBDd274fC51",
            "organization_id": 222222,
            "power_rating_kw": 1000.0,
            "primary_input_installed_kw": 500.0,
            "primary_input_source": "solar panel",
            "secondary_input_installed_kw": 500000.0,
            "secondary_input_source": "micro fusion generator",
            "source_id": 333333,
            "storage_capacity_kwh": 1000.0,
            "uid": "222222_Peyresourde",
            "updated_at": "2024-03-18T22:23:37",
        },
    ]
