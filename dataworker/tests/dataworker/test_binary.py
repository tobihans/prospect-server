import struct

import pytest

from dataworker.binary import (
    BitField,
    binary_string_bytes,
    binary_string_int,
    binary_string_short_int,
    make_int_from_shorts,
    make_uint_from_shorts,
)


def test__BitField__smoke_test() -> None:
    BitField(57)


def test__BitField__not_an_int() -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        BitField("notanint")


def test__BitField__repr() -> None:
    assert repr(BitField(57)) == "BitField(00111001)"
    assert repr(BitField(600)) == "BitField(0000001001011000)"


def test__BitField__single_bit() -> None:
    bf = BitField(0b11001010)
    assert bf[0] == 0b0
    assert bf[1] == 0b1
    assert bf[2] == 0b0
    assert bf[3] == 0b1
    assert bf[4] == 0b0
    assert bf[5] == 0b0
    assert bf[6] == 0b1
    assert bf[7] == 0b1
    assert bf[8] == 0b0
    assert bf[20] == 0b0


def test__BitField__boolean() -> None:
    bf = BitField(0b11001010)
    assert bf[0] is False
    assert bf[1] is True


def test__BitField__multi_bit() -> None:
    bf = BitField(0b11001010)
    assert bf[1:0] == 0b10
    assert bf[3:1] == 0b101
    assert bf[7:4] == 0b1100


def test__make_uint_from_shorts() -> None:
    # high short int 0: 00000000 00000000
    # low short int 0: 00000000 00000000
    # uint 0: 00000000 00000000 00000000 00000000
    assert make_uint_from_shorts(0, 0) == 0
    #
    # high short int 1: 00000000 00000001
    # low short int 0: 00000000 00000000
    # uint 65536: 00000000 00000001 00000000 00000000
    assert make_uint_from_shorts(1, 0) == 65536
    #
    # high short int -1: 11111111 11111111
    # low short int 0: 00000000 00000000
    # uint 4294901760: 11111111 11111111 00000000 00000000
    assert make_uint_from_shorts(-1, 0) == 4294901760
    #
    # high short int 23158: 01011010 01110110
    # low short int -15413: 11000011 11001011
    # uint 1517732811: 01011010 01110110 11000011 11001011
    assert make_uint_from_shorts(23158, -15413) == 1517732811


def test__make_int_from_shorts() -> None:
    # high short int 0: 00000000 00000000
    # low short int 0: 00000000 00000000
    # uint 0: 00000000 00000000 00000000 00000000
    assert make_int_from_shorts(0, 0) == 0
    #
    # high short int 1: 00000000 00000001
    # low short int 0: 00000000 00000000
    # uint 65536: 00000000 00000001 00000000 00000000
    assert make_int_from_shorts(1, 0) == 65536
    #
    # high short int -1: 11111111 11111111
    # low short int 0: 00000000 00000000
    # uint 4294901760: 11111111 11111111 00000000 00000000
    assert make_int_from_shorts(-1, 0) == -65536
    #
    # high short int 23158: 01011010 01110110
    # low short int -15413: 11000011 11001011
    # uint 1517732811: 01011010 01110110 11000011 11001011
    assert make_int_from_shorts(23158, -15413) == 1517732811


# --------------------------------------------------


def test__binary_string_bytes() -> None:
    assert binary_string_bytes(b"abc") == "01100001 01100010 01100011"
    assert binary_string_bytes(bytes([1, 2, 3])) == "00000001 00000010 00000011"


def test__binary_string_short_int() -> None:
    assert binary_string_short_int(0) == "00000000 00000000"
    assert binary_string_short_int(-1) == "11111111 11111111"
    assert binary_string_short_int(6245) == "00011000 01100101"
    assert binary_string_short_int(-32184) == "10000010 01001000"
    with pytest.raises(struct.error):
        binary_string_short_int(123456789)


def test__binary_string_int() -> None:
    assert binary_string_int(0) == "00000000 00000000 00000000 00000000"
    assert binary_string_int(-1) == "11111111 11111111 11111111 11111111"
    assert binary_string_int(6245) == "00000000 00000000 00011000 01100101"
    assert binary_string_int(-32184) == "11111111 11111111 10000010 01001000"
    assert binary_string_int(123456789) == "00000111 01011011 11001101 00010101"
