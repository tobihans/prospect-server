from copy import deepcopy
from dataclasses import asdict
from datetime import UTC, datetime

import pytest
from pandas import NaT

from dataworker.database import get_table
from dataworker.dataset import RecordSet
from dataworker.task import (
    adapt_types,
    add_metadata,
    add_uids,
    prepare_for_storage,
    remove_unwanted_fields,
)

# ------------------------- remove unwanted fields -------------------------


def test__remove_unwanted_fields__shed_nothings() -> None:
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {"this": 12, "that": None, "something": ""},
                {"ping": "pong", "else": 0, "answer": False},
            ],
        )
    ).records == [
        {"this": 12},
        {
            "answer": False,
            "else": 0,
            "ping": "pong",
        },
    ]


def test__remove_unwanted_fields__custom_not_allowed() -> None:
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {
                    "ping": "pong",
                    "this": 12,
                    "that": 32,
                    "custom": {"sausage_length": 51, "spice": "cumin"},
                },
                {
                    "ping": "pang",
                    "this": 43,
                    "that": 1000,
                    "custom": {"juggling_object": "skis"},
                },
            ],
        )
    ).records == [
        {"ping": "pong", "that": 32, "this": 12},
        {
            "ping": "pang",
            "that": 1000,
            "this": 43,
        },
    ]


def test__remove_unwanted_fields__timestamps_not_allowed() -> None:
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {
                    "ping": "pong",
                    "this": 12,
                    "that": 32,
                    "created_at": "2000-01-01",
                    "updated_at": "2000-03-01",
                    "id": 15,
                    "import_id": 13574,
                    "data_origin": "ton_cul",
                    "organization_id": 22222246,
                    "source_id": 33333378,
                },
                {
                    "ping": "pang",
                    "this": 43,
                    "that": 1000,
                    "created_at": "2000-01-01",
                    "updated_at": "2000-03-01",
                    "id": 134,
                    "import_id": 13574,
                    "data_origin": "ton_cul",
                    "last_import_id": 13574,
                },
            ],
        )
    ).records == [
        {"ping": "pong", "that": 32, "this": 12},
        {
            "ping": "pang",
            "that": 1000,
            "this": 43,
        },
    ]


def test__remove_unwanted_fields__shed_primary_uid() -> None:
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {"this": 12, "that": 41, "uid": "XYZ"},
                {"this": 15, "that": 1000, "uid": "ABC"},
            ],
        )
    ).records == [{"that": 41, "this": 12}, {"that": 1000, "this": 15}]


def test__remove_unwanted_fields__shed_primary_uid__update() -> None:
    # uid is preserved in case of update
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {"this": 12, "that": 41, "uid": "XYZ"},
                {"this": 15, "that": 1000, "uid": "ABC"},
            ],
        ),
        is_update=True,
    ).records == [
        {"this": 12, "that": 41, "uid": "XYZ"},
        {"this": 15, "that": 1000, "uid": "ABC"},
    ]


def test__remove_unwanted_fields__shed_secondary_uid() -> None:
    assert remove_unwanted_fields(
        RecordSet(
            "table",
            [
                {"this": 12, "that": 41, "shoe_uid": "DEF"},
                {"this": 15, "that": 1000, "pepperoni_uid": "GHI"},
            ],
        )
    ).records == [{"that": 41, "this": 12}, {"that": 1000, "this": 15}]


def test__remove_unwanted_fields__shed_arrays() -> None:
    recordset = RecordSet(
        "table",
        [
            {"this": 12, "that": 41, "tags": []},
            {"this": 13, "that": 42, "tags": ["", None]},
            {"this": 15, "that": 1000, "tags": ["G", "HI"]},
        ],
    )
    assert remove_unwanted_fields(recordset).records == [
        {"that": 41, "this": 12},
        {"this": 13, "that": 42},
        {"that": 1000, "this": 15, "tags": ["G", "HI"]},
    ]


def test__remove_unwanted_fields__shed_dicts() -> None:
    recordset = RecordSet(
        "table",
        [
            {"this": 12, "that": 41, "extra": {}},
            {"this": 13, "that": 42, "extra": {"time": NaT, "foo": None}},
            {"this": 15, "that": 1000, "extra": {"G": "g", "HI": "hi"}},
        ],
    )
    assert remove_unwanted_fields(recordset).records == [
        {"that": 41, "this": 12},
        {"this": 13, "that": 42},
        {"that": 1000, "this": 15, "extra": {"G": "g", "HI": "hi"}},
    ]


# ------------------------- add metadata -------------------------


@pytest.mark.usefixtures("_database")
def test__add_metadata__add_timestamps(grid_record, import_context, now) -> None:
    assert add_metadata(
        RecordSet(
            "data_grids",
            [
                grid_record,
            ],
        ),
        import_context=import_context,
    ).select(["created_at", "updated_at"]).records == [
        {
            "created_at": now,
            "updated_at": now,
        },
    ]


@pytest.mark.usefixtures("_database")
def test__add_metadata__add_timestamps__update(grid_record, import_context) -> None:
    # in case of update, no timestamps are added
    assert add_metadata(
        RecordSet(
            "data_grids",
            [
                grid_record,
            ],
        ),
        is_update=True,
        import_context=import_context,
    ).select(["created_at", "updated_at"]).records == [
        {},
    ]


@pytest.mark.usefixtures("_database")
def test__add_metadata__add_import_context(grid_record, import_context) -> None:
    assert add_metadata(
        RecordSet(
            "data_grids",
            [
                grid_record,
            ],
        ),
        import_context=import_context,
    ).select(["import_id", "source_id", "data_origin", "organization_id"]).records == [
        {
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "source_id": 333333,
        },
    ]


# ------------------------- add uids ------------------------------


@pytest.mark.usefixtures("_database")
def test__add_uids__add_record_identifiers(grid_record, import_context) -> None:
    assert add_uids(
        RecordSet(
            "data_grids",
            [
                grid_record | asdict(import_context),
            ],
        ),
    ).select(["uid"]).records == [
        {"uid": "222222_Peyresourde"},
    ]


@pytest.mark.usefixtures("_database")
def test__add_uids__add_record_identifiers__update(grid_record, import_context) -> None:
    """Identifier building is skipped in case of update."""
    assert add_uids(
        RecordSet(
            "data_grids",
            [
                grid_record | asdict(import_context),
            ],
        ),
        is_update=True,
    ).select(["uid"]).records == [
        {},
    ]


# ------------------------- adapt types -------------------------


@pytest.mark.usefixtures("_database")
def test__adapt_types__collect_extra(grid_record) -> None:
    assert adapt_types(
        RecordSet(
            "data_grids",
            [
                grid_record,
            ],
        )
    ).records == [
        {
            "country": "FR",
            "custom": {"ambient_smell": "thym", "gnome_count": 13},
            "is_offgrid": True,
            "latitude": 43.8001088,
            "location_area_1": "Languedoc",
            "location_area_2": "Hérault",
            "location_area_3": "Haut-Cantons",
            "location_area_4": "Pégairolles",
            "longitude": 3.3059847,
            "name": "Peyresourde",
            "operator_company": "Coopérative Énergitique des Hauts Cantons",
            "operator_phone_e": "Cdc4E0325e7AbfBaF986",
            "operator_phone_p": "FE8c0A96aeBDd274fC51",
            "power_rating_kw": 1000,
            "primary_input_installed_kw": 500,
            "primary_input_source": "solar panel",
            "secondary_input_installed_kw": 500000,
            "secondary_input_source": "micro fusion generator",
            "storage_capacity_kwh": 1000,
        },
    ]


@pytest.mark.usefixtures("_database")
def test__adapt_types__type_based_processing(grid_record_editable) -> None:
    # change a str field, process record and verify field has been processed
    grid_record_editable["name"] = "   words floating in spaces     "
    assert adapt_types(RecordSet("data_grids", [grid_record_editable])).select([
        "name"
    ]).records == [{"name": "words floating in spaces"}]


@pytest.mark.usefixtures("_database")
def test__adapt_types__field_based_processing(grid_record_editable) -> None:
    # change a country field, process record and verify field has been processed
    grid_record_editable["country"] = "france"
    assert adapt_types(RecordSet("data_grids", [grid_record_editable])).select([
        "country"
    ]).records == [{"country": "FR"}]


@pytest.mark.parametrize(
    ("in_", "out", "log_str"),
    [
        ("Male", "M", None),
        ("female", "F", None),
        ("o", "O", None),
        ("Crazy", None, "Nullifying invalid value Crazy for enum"),
        (None, None, None),
        ("", None, None),
    ],
)
@pytest.mark.usefixtures("_database")
def test__adapt_types__gender_field_and_type_processing(
    meter_sample_record, in_, out, log_str, caplog
):
    # make sure that the caplog will be always empty
    get_table("data_meters")
    caplog.clear()
    gender_col = "customer_gender"
    meter_record = deepcopy(meter_sample_record)
    meter_record[gender_col] = in_
    meters_rs = RecordSet("data_meters", [meter_record])
    assert adapt_types(meters_rs).select([gender_col]).records == [{gender_col: out}]
    if log_str:
        assert log_str in caplog.messages[-1]
        assert "gender" in caplog.messages[-1]
    else:
        assert len(caplog.messages) == 0


@pytest.mark.parametrize(
    ("in_", "out", "log_str"),
    [
        (1, "1", None),
        ("1", "1", None),
        ("2", "2", None),
        ("3", "3", None),
        ("3ph", "3", None),
        ("2nd", "2", None),
        ("a", "1", None),
        ("B", "2", None),
        ("c", "3", None),
        ("Red", "1", None),
        ("yellow", "2", None),
        ("BLUE", "3", None),
        ("Crazy", None, "Nullifying invalid value Crazy for enum"),
        (None, None, None),
        ("", None, None),
    ],
)
@pytest.mark.usefixtures("_database")
def test__adapt_types__phase_field_and_type_processing(
    meter_sample_record, in_, out, log_str, caplog
):
    # make sure that the caplog will be always empty
    get_table("data_meters_ts")
    caplog.clear()
    meter_record = deepcopy(meter_sample_record)
    meter_record["phase"] = in_
    meters_rs = RecordSet("data_meters_ts", [meter_record])
    assert adapt_types(meters_rs).select(["phase"]).records == [{"phase": out}]
    if log_str:
        assert log_str in caplog.messages[-1]
        assert "phase" in caplog.messages[-1]
    else:
        assert len(caplog.messages) == 0


# ------------------------- prepare_for_storage -------------------------


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__default(grid_record, import_context, now) -> None:
    assert prepare_for_storage(
        RecordSet(
            "data_grids",
            [
                grid_record,
            ],
        ),
        import_context=import_context,
    ).records == [
        {
            "country": "FR",  # field-based processing
            "is_offgrid": True,
            "latitude": 43.8001088,
            "location_area_1": "Languedoc",
            "location_area_2": "Hérault",
            "location_area_3": "Haut-Cantons",
            "location_area_4": "Pégairolles",
            "longitude": 3.3059847,
            "name": "Peyresourde",
            "operator_company": "Coopérative Énergitique des Hauts Cantons",
            "operator_phone_e": "Cdc4E0325e7AbfBaF986",
            "operator_phone_p": "FE8c0A96aeBDd274fC51",
            "power_rating_kw": 1000.0,
            "primary_input_installed_kw": 500.0,
            "primary_input_source": "solar panel",
            "secondary_input_installed_kw": 500000.0,
            "secondary_input_source": "micro fusion generator",
            "storage_capacity_kwh": 1000.0,
            # import context
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "source_id": 333333,
            # identifiers
            "uid": "222222_Peyresourde",
            # timestamps
            "created_at": now,
            "updated_at": now,
            # extra fields collected
            "custom": {"ambient_smell": "thym", "gnome_count": 13},
        },
    ]


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__update(import_context):
    assert prepare_for_storage(
        RecordSet(
            "data_grids",
            [
                {
                    "uid": "222222_Peyresourde",
                    "power_rating_kw": 1000,
                    "ambient_smell": "rosemarine",
                },
            ],
        ),
        is_update=True,
        import_context=import_context,
    ).records == [
        {
            # uid is preserved
            "uid": "222222_Peyresourde",
            # field to update
            "power_rating_kw": 1000.0,
            # import context
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "source_id": 333333,
            # extra fields are collected
            "custom": {"ambient_smell": "rosemarine"},
        },
    ]


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__processor__build_identifiers(trust_trace_record, now):
    """Post-processing as done for processors.

    - mandatory import context values are set by the
    transformer itself.
    """
    assert prepare_for_storage(
        RecordSet(
            "data_trust_trace",
            [
                trust_trace_record
                | {  # manually set import context
                    "data_origin": "centrale_de_fusion_des_cévènes",
                    "import_id": 444444,
                    "organization_id": 555555,
                    "source_id": 666666,
                }
            ],
        ),
        is_internal=True,
    ).records == [
        {
            "check": "check-if-life-has-a-meaning",
            "result": "probably not",
            "subject_origin": "payments_ts",
            "subject_uid": "chop-an-hour",
            # manually set import context untouched
            "data_origin": "centrale_de_fusion_des_cévènes",
            "import_id": 444444,
            "organization_id": 555555,
            "source_id": 666666,
            # uid created from parts
            "uid": "payments_ts_chop-an-hour_check-if-life-has-a-meaning",
            # timestamps
            "created_at": now,
            "updated_at": now,
        },
    ]


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__processor__provide_identifiers(trust_trace_record, now):
    """Post-processing as done for processors.

    If an identifier is provided, it takes precedence over one that is built
    from parts.
    """
    assert prepare_for_storage(
        RecordSet(
            "data_trust_trace",
            [
                trust_trace_record
                | {  # manually set import context
                    "data_origin": "centrale_de_fusion_des_cévènes",
                    "import_id": 444444,
                    "organization_id": 555555,
                    "source_id": 666666,
                }
                | {"uid": "lemme-set-that-myself-mate"}
            ],
        ),
        is_internal=True,
    ).records == [
        {
            "check": "check-if-life-has-a-meaning",
            "result": "probably not",
            "subject_origin": "payments_ts",
            "subject_uid": "chop-an-hour",
            # manually set import context untouched
            "data_origin": "centrale_de_fusion_des_cévènes",
            "import_id": 444444,
            "organization_id": 555555,
            "source_id": 666666,
            # provided uid takes precedence
            "uid": "lemme-set-that-myself-mate",
            # timestamps
            "created_at": now,
            "updated_at": now,
        },
    ]


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__update_from_processor():
    """Post-processing as done for processors in case of update.

    - No uid building happens
    - The mandatory import context values are set by the
    transformer itself. They are neither removed from nor added to the record.
    """
    assert prepare_for_storage(
        RecordSet(
            "data_grids",
            [
                {
                    "uid": "777777_Peyresourde",
                    "power_rating_kw": 1000,
                    "ambient_smell": "rosemarine",
                }
                | {
                    "data_origin": "centrale_de_fusion_des_cévènes",
                    "import_id": 444444,
                    "organization_id": 777777,
                    "source_id": 666666,
                },
            ],
        ),
        is_internal=True,
        is_update=True,
    ).records == [
        {
            "power_rating_kw": 1000,
            # identifiers
            "uid": "777777_Peyresourde",
            # import context, untouched
            "data_origin": "centrale_de_fusion_des_cévènes",
            "import_id": 444444,
            "organization_id": 777777,
            "source_id": 666666,
            # extra fields collected
            "custom": {"ambient_smell": "rosemarine"},
        },
    ]


@pytest.mark.usefixtures("_database")
def test__prepare_for_storage__build_identifiers_from_datetime(
    payment_sample_record, now
):
    """Post-processing as done for processors.

    - verify that timestamp handling is constant.
    NB: for now, absence of microseconds will make a difference
    Will be completely fixed, only and once delegated to the DB.
    """
    payment_with_import_context = payment_sample_record | {
        "data_origin": "foo",
        "import_id": 444444,
        "organization_id": 555555,
        "source_id": 666666,
    }
    paid_dt = datetime(2024, 4, 18, 14, 0, 1, 0, tzinfo=UTC)
    paid_dt_str = "2024-04-18T14:00:01+00:00"
    paid_dt_ms = datetime(2024, 4, 18, 14, 0, 1, 1000, tzinfo=UTC)
    paid_dt_ms_str = "2024-04-18T14:00:01.001000+00:00"
    payments = prepare_for_storage(
        RecordSet(
            "data_payments_ts",
            [
                payment_with_import_context
                | {  # overwrite paid_at without ms
                    "paid_at": "2024-04-18T14:00:01+00:00"
                },
                payment_with_import_context
                | {  # overwrite paid_at 0 ms get ignored
                    "paid_at": "2024-04-18T14:00:01.000+00:00"
                },
                payment_with_import_context
                | {  # overwrite paid_at
                    "paid_at": paid_dt
                },
                payment_with_import_context
                | {  # overwrite paid_at
                    "paid_at": paid_dt_ms
                },
                payment_with_import_context
                | {  # overwrite paid_at millis convert to micros
                    "paid_at": "2024-04-18T14:00:01.001+00:00"
                },
            ],
        ),
        is_internal=True,
    ).records
    assert payments[0] == {
        "account_external_id": "312465497845",
        "account_origin": "shs",
        "account_uid": "555555_foo_312465497845",
        "amount": 124,
        "created_at": now,
        "currency": "milcoin",
        "data_origin": "foo",
        "days_active": 67,
        "import_id": 444444,
        "organization_id": 555555,
        "paid_at": paid_dt,
        "payment_external_id": "ats-789-trs",
        "provider_name": "Le Bistrot des Halles",
        "provider_transaction_id": "TCG-768",
        "purchase_item": "Courge Spaghetti",
        "purchase_quantity": 4.5,
        "purchase_unit": "kilo",
        "purpose": "Soupe",
        "reverted_at": "2024-04-30T14:00:00+00:00",
        "source_id": 666666,
        "uid": f"shs_555555_foo_312465497845_ats-789-trs_{paid_dt_str}",
        "updated_at": now,
    }
    assert payments[1]["paid_at"] == paid_dt
    assert (
        payments[1]["uid"] == f"shs_555555_foo_312465497845_ats-789-trs_{paid_dt_str}"
    )
    assert payments[2]["paid_at"] == paid_dt
    assert (
        payments[2]["uid"] == f"shs_555555_foo_312465497845_ats-789-trs_{paid_dt_str}"
    )
    assert payments[3]["paid_at"] == paid_dt_ms
    assert (
        payments[3]["uid"]
        == f"shs_555555_foo_312465497845_ats-789-trs_{paid_dt_ms_str}"
    )
    assert payments[4]["paid_at"] == paid_dt_ms
    assert (
        payments[4]["uid"]
        == f"shs_555555_foo_312465497845_ats-789-trs_{paid_dt_ms_str}"
    )
