import random
import unittest.mock
from datetime import UTC, timedelta
from operator import itemgetter
from textwrap import dedent
from typing import Any
from zoneinfo import ZoneInfo

import arrow
import funcy
import loguru._defaults
import pytest
import time_machine

from dataworker import metadata
from dataworker.database import get_or_create_connection, get_table
from dataworker.queries import fetch_one_row
from dataworker.tracking import Event, ImportTracker, TrackerManager

# ------------------------- constants -------------------------

IMPORT_ID = 123
RUN_ID = 456
PROCESS_ID = 789


REFERENCE_DATE = "2023-01-01"
TIME_SINCE_CREATION_M = 12
TIME_SINCE_INGESTION_M = 10
INGESTION_DURATION_S = 10
TIME_SINCE_PROCESSING_M = 8
PROCESSING_DURATION_S = 10

NOW = arrow.get(REFERENCE_DATE).datetime
CREATION_TIME = NOW - timedelta(minutes=TIME_SINCE_CREATION_M)
INGESTION_START_TIME = NOW - timedelta(minutes=TIME_SINCE_INGESTION_M)
INGESTION_END_TIME = INGESTION_START_TIME + timedelta(seconds=INGESTION_DURATION_S)
PROCESSING_START_TIME = NOW - timedelta(minutes=TIME_SINCE_PROCESSING_M)
PROCESSING_END_TIME = PROCESSING_START_TIME + timedelta(seconds=PROCESSING_DURATION_S)


# ------------------------- fixtures and utils -------------------------


# --- import table


def fetch_import_data(import_id):
    return fetch_one_row("imports", ("id", import_id))


def init_tracking_record(import_id):
    with time_machine.travel(CREATION_TIME):
        import_record = {
            "id": import_id,
            "protocol": "\n".join([
                "ingestion starting",
                "ingesting",
                "ingestion finished",
            ]),
            "ingestion_started_at": INGESTION_START_TIME,
            "ingestion_finished_at": INGESTION_END_TIME,
        } | metadata.creation_timestamps()
    get_or_create_connection().execute(
        get_table("imports").insert().values(**import_record)
    )


# --- logs


def make_log(**kwargs):
    """A minimal log object to be edited."""
    DEFAULT_MESSAGE = "something is computing"

    extra = {
        "run_id": RUN_ID,
        "import_id": kwargs.get("import_id") or IMPORT_ID,
    }
    if event := kwargs.get("event"):
        extra["event"] = event

    level = unittest.mock.Mock()
    match kwargs.get("level", "info"):
        case "debug":
            level.name = "DEBUG"
            level.no = loguru._defaults.LOGURU_DEBUG_NO
        case "info":
            level.name = "INFO"
            level.no = loguru._defaults.LOGURU_INFO_NO
        case _:
            raise ValueError(f"unsupported level {level!r}")

    process = unittest.mock.Mock()
    process.id = kwargs.get("process_id") or PROCESS_ID

    return {
        "message": kwargs.get("message") or DEFAULT_MESSAGE,
        "time": kwargs.get("time") or NOW,
        "level": level,
        "extra": extra,
        "process": process,
    }


# --- single logs


@pytest.fixture
def start_log() -> dict[str, Any]:
    return make_log(
        message="starting job",
        time=PROCESSING_START_TIME,
        event=Event.START,
    )


# @pytest.fixture
# def end_log() -> dict[str, Any]:
#     return make_log(
#         message="this is the end",
#         event=Event.END,
#         time=PROCESSING_END_TIME,
#     )


@pytest.fixture
def insert_success_log() -> dict[str, Any]:
    return make_log(
        event=Event.ROW_INSERT_SUCCESS,
        message="row inserted",
    )


@pytest.fixture
def update_success_log() -> dict[str, Any]:
    return make_log(
        event=Event.ROW_UPDATE_SUCCESS,
        message="row updated",
    )


@pytest.fixture
def duplicate_log() -> dict[str, Any]:
    return make_log(
        event=Event.ROW_INSERT_DUPLICATE,
        message="duplicate row detected",
    )


@pytest.fixture
def row_fail_log() -> dict[str, Any]:
    return make_log(
        event=Event.ROW_OPERATION_FAIL,
        message="error during row storage",
    )


# --- scenarios


@pytest.fixture
def log_scenario__start(start_log) -> list:
    """Just a start event."""
    return [start_log]


@pytest.fixture
def log_scenario__inserts(
    start_log, insert_success_log, duplicate_log, row_fail_log
) -> list:
    """Start event, info, two insert success, one fail."""
    return [
        start_log,
        make_log(),
        insert_success_log,
        insert_success_log,
        duplicate_log,
        row_fail_log,
    ]


@pytest.fixture
def log_scenario__update(start_log, update_success_log, row_fail_log) -> list:
    """Start event, info, two insert success, one fail."""
    return [
        start_log,
        make_log(),
        update_success_log,
        update_success_log,
        row_fail_log,
    ]


@pytest.fixture
def log_scenario__fail(start_log, update_success_log) -> list:
    """A scenario that ends in job failure."""
    return [
        start_log,
        make_log(),
        update_success_log,
        update_success_log,
        make_log(message="server on fire"),
        make_log(message="giving up", event=Event.JOB_FAIL),
        make_log(message="go for a beer", event=Event.END, time=PROCESSING_END_TIME),
    ]


@pytest.fixture
def log_scenario__complete(start_log, update_success_log) -> list:
    """A scenario that completes."""
    return [
        start_log,
        make_log(message="computing"),
        update_success_log,
        update_success_log,
        make_log(
            message="job done",
            event=Event.COMPLETE,
        ),
        make_log(message="go for a beer", event=Event.END, time=PROCESSING_END_TIME),
    ]


# --- tracker


@pytest.fixture
def tracker(_database) -> ImportTracker:
    """Simple instance of a TaskTracker."""
    return ImportTracker(IMPORT_ID)


@pytest.fixture
def _no_sync(mocker) -> None:
    """Disable sync on trackers."""
    mocker.patch.object(ImportTracker, "_sync")


# ------------------------- TaskTracker -------------------------


def test__TaskTracker__create():
    """Smoke test."""
    ImportTracker(IMPORT_ID)


def test__TaskTracker__repr(tracker):
    tracker.add_log(make_log())
    tracker.add_log(make_log())
    tracker.add_log(make_log())
    assert repr(tracker) == "ImportTracker(<3 logs>)"


def test__TaskTracker__add_log(tracker):
    """Logs can be added."""
    tracker.add_log(make_log())
    assert len(tracker._logs) == 1


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__update_trigger(tracker, start_log):
    """Some logs trigger update."""
    tracker.add_log(make_log())
    tracker._sync.assert_not_called()
    tracker.add_log(start_log)
    tracker._sync.assert_called_once()


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__protocol_flow(tracker):
    # no logs, no import id
    assert tracker._collect_protocol() == ""
    # first record comes with the separator
    tracker.add_log(make_log(message="something"))
    assert tracker._collect_protocol() == dedent("""\
            =========DATAWORKER IMPORT TASK=========
            something""")
    # updating the pointer, collecting nothing
    tracker._update_protocol_pointer()
    assert tracker._collect_protocol() == ""
    # more messages, no more separator
    tracker.add_log(make_log(message="something else"))
    tracker.add_log(make_log(message="one last thing"))
    assert tracker._collect_protocol() == dedent("""\
            something else
            one last thing""")
    # updating the pointer, collecting again
    tracker._update_protocol_pointer()
    assert tracker._collect_protocol() == ""


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__protocol__levels(tracker):
    """Only info level and above should end up in the protocol."""
    tracker.add_log(make_log(message="nothing very important", level="debug"))
    tracker.add_log(make_log(message="a real info"))
    assert tracker._collect_protocol() == dedent("""\
            =========DATAWORKER IMPORT TASK=========
            a real info""")


def test__TaskTracker__update__protocol(tracker, start_log):
    """Add logs, update. Check protocol got appended."""
    init_tracking_record(IMPORT_ID)
    # first update adds nothing
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished""")
    # adding first logs
    tracker.add_log(start_log)
    tracker.add_log(make_log(message="this happened"))
    tracker.add_log(make_log(message="code is running"))
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting job
            this happened
            code is running""")
    # update without new logs don't change protocol
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting job
            this happened
            code is running""")
    # adding more logs
    tracker.add_log(make_log(message="one last thing"))
    tracker.add_log(make_log(message="ok stop now"))
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting job
            this happened
            code is running
            one last thing
            ok stop now""")


@pytest.mark.parametrize(
    "dt_obj",
    [
        NOW,
        NOW.replace(tzinfo=None),
        NOW.astimezone(tz=ZoneInfo("Europe/Berlin")),
    ],
)
def test_TaskTracker__different_timezones(dt_obj, tracker):
    init_tracking_record(IMPORT_ID)
    # first update adds nothing
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["processing_started_at"] is None
    assert import_info["processing_finished_at"] is None
    # adding start log to set start time
    tracker.add_log(make_log(time=dt_obj, event=Event.START))
    # adding start log to set end time
    tracker.add_log(make_log(time=dt_obj, event=Event.END))
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    # rebuilding the expected start_at info to compensate the various server timezones
    started_at = (
        import_info["processing_started_at"]
        .replace(tzinfo=UTC)
        .astimezone(dt_obj.tzinfo)
        .replace(tzinfo=dt_obj.tzinfo)
    )
    finished_at = (
        import_info["processing_finished_at"]
        .replace(tzinfo=UTC)
        .astimezone(dt_obj.tzinfo)
        .replace(tzinfo=dt_obj.tzinfo)
    )
    assert started_at == dt_obj
    assert finished_at == dt_obj


# --- collect_tracking_data on all 6 scenarios


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__collect_tracking_data__start(tracker, log_scenario__start):
    for log in log_scenario__start:
        tracker.add_log(log)
    assert tracker._collect_tracking_data() == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME,
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 0,
    }


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__collect_tracking_data__inserts(tracker, log_scenario__inserts):
    for log in log_scenario__inserts:
        tracker.add_log(log)
    assert tracker._collect_tracking_data() == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME,
        "rows_duplicated": 1,
        "rows_failed": 1,
        "rows_inserted": 2,
        "rows_updated": 0,
    }


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__collect_tracking_data__update(tracker, log_scenario__update):
    for log in log_scenario__update:
        tracker.add_log(log)
    assert tracker._collect_tracking_data() == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME,
        "rows_duplicated": 0,
        "rows_failed": 1,
        "rows_inserted": 0,
        "rows_updated": 2,
    }


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__collect_tracking_data__fail(tracker, log_scenario__fail):
    for log in log_scenario__fail:
        tracker.add_log(log)
    assert tracker._collect_tracking_data() == {
        "error": "giving up",
        "processing_finished_at": PROCESSING_END_TIME,
        "processing_started_at": PROCESSING_START_TIME,
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 2,
    }


@pytest.mark.usefixtures("_no_sync")
def test__TaskTracker__collect_tracking_data__complete(tracker, log_scenario__complete):
    for log in log_scenario__complete:
        tracker.add_log(log)
    assert tracker._collect_tracking_data() == {
        "error": None,
        "processing_finished_at": PROCESSING_END_TIME,
        "processing_started_at": PROCESSING_START_TIME,
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 2,
    }


# --- update on all 6 scenarios


IMPORT_TRACKING_FIELDS = [
    "processing_started_at",
    "processing_finished_at",
    "updated_at",
    "error",
    "rows_duplicated",
    "rows_failed",
    "rows_inserted",
    "rows_updated",
]


def test__TaskTracker__update__start(tracker, log_scenario__start, now):
    init_tracking_record(IMPORT_ID)
    for log in log_scenario__start:
        tracker.add_log(log)
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 0,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                starting job""")


def test__TaskTracker__update__inserts(tracker, log_scenario__inserts, now):
    init_tracking_record(IMPORT_ID)
    for log in log_scenario__inserts:
        tracker.add_log(log)
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 1,
        "rows_failed": 1,
        "rows_inserted": 2,
        "rows_updated": 0,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                starting job
                something is computing
                row inserted
                row inserted
                duplicate row detected
                error during row storage""")


def test__TaskTracker__update__update(tracker, log_scenario__update, now):
    init_tracking_record(IMPORT_ID)
    for log in log_scenario__update:
        tracker.add_log(log)
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 1,
        "rows_inserted": 0,
        "rows_updated": 2,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                starting job
                something is computing
                row updated
                row updated
                error during row storage""")


def test__TaskTracker__update__fail(tracker, log_scenario__fail, now):
    init_tracking_record(IMPORT_ID)
    for log in log_scenario__fail:
        tracker.add_log(log)
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": "giving up",
        "processing_finished_at": PROCESSING_END_TIME.replace(tzinfo=None),
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 2,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                starting job
                something is computing
                row updated
                row updated
                server on fire
                giving up
                go for a beer""")


def test__TaskTracker__update__complete(tracker, log_scenario__complete, now):
    init_tracking_record(IMPORT_ID)
    for log in log_scenario__complete:
        tracker.add_log(log)
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": PROCESSING_END_TIME.replace(tzinfo=None),
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 2,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                starting job
                computing
                row updated
                row updated
                job done
                go for a beer""")


def test__TaskTracker__update__levels(tracker, now):
    init_tracking_record(IMPORT_ID)
    tracker.add_log(make_log(message="nothing very important", level="debug"))
    tracker.add_log(make_log(message="a real info"))
    tracker._sync()
    import_info = fetch_import_data(IMPORT_ID)
    assert import_info["protocol"] == dedent("""\
                ingestion starting
                ingesting
                ingestion finished
                =========DATAWORKER IMPORT TASK=========
                a real info""")


# ------------------------- TrackerManager -------------------------


def test__TrackerManager__initial_state():
    tm = TrackerManager()
    assert tm._trackers == {}
    assert tm._trackers_status() == []


def test__TrackerManager__record_without_tracker():
    tm = TrackerManager()
    tm.process_log(make_log())
    assert tm._trackers_status() == []


@pytest.mark.usefixtures("_database")
def test__TrackerManager__Tracker_creation():
    tm = TrackerManager()
    tm.process_log(make_log(event=Event.START))
    assert tm._trackers_status() == [{"import_id": 123, "logs": 1, "process": 789}]
    tm.process_log(make_log())
    assert tm._trackers_status() == [{"import_id": 123, "logs": 2, "process": 789}]


@pytest.mark.usefixtures("_database")
def test__TrackerManager__Tracker_reset():
    tm = TrackerManager()
    # three logs in a tracker
    for log_args in [{"event": Event.START}, {}, {}]:
        tm.process_log(make_log(**log_args))
    assert tm._trackers_status() == [{"import_id": 123, "logs": 3, "process": 789}]
    # two similar logs from a different import_id
    # tracker for that process get replaced and start tracking the other import
    for log_args in [
        {"event": Event.START, "import_id": IMPORT_ID + 1},
        {"import_id": IMPORT_ID + 1},
    ]:
        tm.process_log(make_log(**log_args))
    assert tm._trackers_status() == [{"import_id": 124, "logs": 2, "process": 789}]


@pytest.mark.usefixtures("_database")
def test__TrackerManager__record_dispatch():
    tm = TrackerManager()
    # three logs in a tracker
    for log_args in [
        # start + 2 logs
        {"event": Event.START},
        {},
        {},
        # start + 2 logs from another process, processing another import
        {
            "event": Event.START,
            "import_id": IMPORT_ID + 1,
            "process_id": PROCESS_ID + 1,
        },
        {"import_id": IMPORT_ID + 1, "process_id": PROCESS_ID + 1},
        {"import_id": IMPORT_ID + 1, "process_id": PROCESS_ID + 1},
        # just 2 logs without start from a third process, processing yet another import
        {"import_id": IMPORT_ID + 2, "process_id": PROCESS_ID + 2},
        {"import_id": IMPORT_ID + 2, "process_id": PROCESS_ID + 2},
    ]:
        tm.process_log(make_log(**log_args))
    assert tm._trackers_status() == [
        {"import_id": IMPORT_ID, "logs": 3, "process": PROCESS_ID},
        {"import_id": IMPORT_ID + 1, "logs": 3, "process": PROCESS_ID + 1},
    ]


# ------------------------- final test -------------------------


def random_interlace(*sequences):
    """Interlace sequences, while preserving internal order of each sequence."""
    iterators = [iter(seq) for seq in sequences]
    while True:
        if not iterators:
            return
        try:
            yield next(next_iterator := random.choice(iterators))
        except StopIteration:
            iterators.remove(next_iterator)


@pytest.mark.usefixtures("_database")
def test__TrackerManager__parallel_scenarios(now):
    # Complete import (Tread 1, Job 1)
    import_id_T1J1 = IMPORT_ID
    process_id_1 = PROCESS_ID
    init_tracking_record(import_id_T1J1)
    import_logs_T1J1 = [
        {
            "message": "starting import T1J1",
            "event": Event.START,
            "time": PROCESSING_START_TIME,
        },
        {"message": "import T1J1 at work"},
        {
            "message": "import T1J1 successful",
            "event": Event.COMPLETE,
        },
        {
            "message": "import T1J1 complete",
            "event": Event.END,
            "time": PROCESSING_END_TIME,
        },
    ]
    # Other import in progress on the same process. (Tread 1, Job 2)
    import_id_T1J2 = IMPORT_ID + 1
    init_tracking_record(import_id_T1J2)
    import_logs_T1J2 = [
        {
            "message": "starting import T1J2",
            "event": Event.START,
            "time": PROCESSING_START_TIME,
            "import_id": import_id_T1J2,
        },
        {"message": "import T1J2 at work", "import_id": import_id_T1J2},
        {
            "message": "row insert",
            "event": Event.ROW_INSERT_SUCCESS,
            "import_id": import_id_T1J2,
        },
    ]
    # Other failed import on another process. (Tread 2 Job 1)
    import_id_T2J1 = IMPORT_ID + 2
    process_id_2 = PROCESS_ID + 1
    init_tracking_record(import_id_T2J1)
    import_logs_C = [
        {
            "message": "starting import T2J1",
            "event": Event.START,
            "time": PROCESSING_START_TIME,
            "process_id": process_id_2,
            "import_id": import_id_T2J1,
        },
        {
            "message": "import T2J1 at work",
            "import_id": import_id_T2J1,
            "process_id": process_id_2,
        },
        {
            "message": "something snaky",
            "import_id": import_id_T2J1,
            "process_id": process_id_2,
        },
        {
            "message": "catastrophe",
            "import_id": import_id_T2J1,
            "event": Event.JOB_FAIL,
            "process_id": process_id_2,
        },
        {
            "message": "this is the end",
            "import_id": import_id_T2J1,
            "event": Event.END,
            "process_id": process_id_2,
            "time": PROCESSING_END_TIME,
        },
    ]
    tm = TrackerManager()
    # ---
    # ingest logs, with different processes randomly interlaced
    for log_args in random_interlace(
        funcy.concat(import_logs_T1J1, import_logs_T1J2), import_logs_C
    ):
        tm.process_log(make_log(**log_args))
    # ---
    import_info = fetch_import_data(import_id_T1J1)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": PROCESSING_END_TIME.replace(tzinfo=None),
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 0,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting import T1J1
            import T1J1 at work
            import T1J1 successful
            import T1J1 complete""")
    # ---
    import_info = fetch_import_data(import_id_T1J2)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": None,
        "processing_finished_at": None,
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 0,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting import T1J2""")
    # ---
    import_info = fetch_import_data(import_id_T2J1)
    assert funcy.project(
        import_info,
        IMPORT_TRACKING_FIELDS,
    ) == {
        "error": "catastrophe",
        "processing_finished_at": PROCESSING_END_TIME.replace(tzinfo=None),
        "processing_started_at": PROCESSING_START_TIME.replace(tzinfo=None),
        "rows_duplicated": 0,
        "rows_failed": 0,
        "rows_inserted": 0,
        "rows_updated": 0,
        "updated_at": now.replace(tzinfo=None),
    }
    assert import_info["protocol"] == dedent("""\
            ingestion starting
            ingesting
            ingestion finished
            =========DATAWORKER IMPORT TASK=========
            starting import T2J1
            import T2J1 at work
            something snaky
            catastrophe
            this is the end""")
    # ---
    assert sorted(tm._trackers_status(), key=itemgetter("process")) == [
        {"import_id": import_id_T1J2, "logs": 3, "process": process_id_1},
        {"import_id": import_id_T2J1, "logs": 5, "process": process_id_2},
    ]  # sorting compensates the randomness of the first log to arrive
