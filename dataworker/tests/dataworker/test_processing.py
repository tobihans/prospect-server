from copy import deepcopy
from datetime import UTC, datetime

import pandas
import pytest
from dateutil.parser import ParserError

from dataworker import processing
from dataworker.database import get_table

# ------------------------- type-based processing -------------------------

# -- INT -----


def test__clip_int4() -> None:
    assert [
        processing.clip_int4(n) for n in [-1e10, -1e9, -1e8, -1e7, 1e7, 1e8, 1e9, 1e10]
    ] == [
        -2147483648,
        -1000000000.0,
        -100000000.0,
        -10000000.0,
        10000000.0,
        100000000.0,
        1000000000.0,
        2147483647,
    ]


# -- STR -----


def test__wrap__single() -> None:
    assert processing.wrap("*", "important") == "*important*"


def test__wrap__double() -> None:
    assert processing.wrap(("(", ")"), "detail") == "(detail)"


def test__quote() -> None:
    assert processing.quote("spaced separated words") == '"spaced separated words"'


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        ("string", "string"),
        ("string ", "string"),
        (" string", "string"),
        (" string ", "string"),
        ("  string   ", "string"),
        (5, "5"),
        (5.0, "5.0"),
        (True, "True"),
        (None, None),
    ],
)
def test__cast_to_string(in_, out) -> None:
    assert processing.cast_to_str(in_) == out


# -- BOOL -----


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        (1, True),
        (0, False),
        (True, True),
        (False, False),
        ("True", True),
        ("False", False),
        ("TrUe", True),
        ("false", False),
        ("t", True),
        ("F", False),
        ("yEs", True),
        ("NO", False),
        ("Y", True),
        ("n", False),
        ("string", None),
        (3, None),
        (None, None),
    ],
)
def test__cast_to_bool(in_, out) -> None:
    assert processing.cast_to_bool(in_) == out


# -- DATETIME -----


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        ("2024-09-20 10:43:00 UTC", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20T10:43:00 UTC", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20 10:43:00Z", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20T10:43:00Z", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20 10:43:00+00:00", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20T10:43:00+00:00", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20 10:43:00.000+00:00", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
        ("2024-09-20T10:43:00.000+00:00", datetime(2024, 9, 20, 10, 43, tzinfo=UTC)),
    ],
)
def test__cast_to_dt(in_, out) -> None:
    assert processing.cast_to_dt(in_) == out


def test__cast_to_dt__do_not_raise_but_log(caplog) -> None:
    assert processing.cast_to_dt("202024-09-20T10:43:00.000+00:00") is None
    rec = caplog.records[-1]
    assert isinstance(rec.exc_info[1], ParserError)


# --- ENUM --------------


@pytest.mark.parametrize(
    ("in_", "out", "log_error"),
    [
        ("1", "1", None),
        (1, "1", None),
        (None, None, None),
        ("Foobar", None, "Nullifying invalid value Foobar"),
    ],
)
@pytest.mark.usefixtures("_database")
def test__nullify_invalid_enum_val(in_, out, log_error, caplog):
    enum = get_table("data_meters_ts").columns.phase.type
    caplog.clear()
    assert processing.nullify_invalid_enums(enum, in_) == out
    if log_error:
        # NB: the first message contains the backtrace including the parametrize args
        # -> searching inside caplog.text returns True whatever you are looking for!
        assert log_error in caplog.messages[-1]
        assert "phase" in caplog.messages[-1]
    else:
        assert len(caplog.messages) == 0


# ------------------------- field-based processing -------------------------

# -- GEO -----


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        ("canada", "CA"),
        ("Canada", "CA"),
        ("Cannnaada", "XX"),
        ("CA", "CA"),
        ("", "XX"),
        (5, "XX"),
    ],
)
def test__resolve_country_code(in_, out) -> None:
    assert processing.resolve_country_code(in_) == out


@pytest.mark.parametrize(
    ("in_", "expected_exception"),
    [
        (None, TypeError),
    ],
)
def test__resolve_country_code_fails(in_, expected_exception) -> None:
    with pytest.raises(expected_exception):
        processing.resolve_country_code(in_)


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        (10.2, 10.2),
        (-10.2, -10.2),
        (10, 10),
        (-10, -10),
    ],
)
def test__validate_latitude(in_, out) -> None:
    assert processing.validate_latitude(in_) == out


@pytest.mark.parametrize(
    ("in_", "expected_exception"),
    [
        (100.2, ValueError),
        (-100.2, ValueError),
        (None, TypeError),
    ],
)
def test__validate_latitude_fails(in_, expected_exception) -> None:
    with pytest.raises(expected_exception):
        processing.validate_latitude(in_)


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        (100.2, 100.2),
        (-100.2, -100.2),
        (100, 100),
        (-100, -100),
    ],
)
def test__validate_longitude(in_, out) -> None:
    assert processing.validate_longitude(in_) == out


@pytest.mark.parametrize(
    ("in_", "expected_exception"),
    [
        (180.2, ValueError),
        (-180.2, ValueError),
        (None, TypeError),
    ],
)
def test__validate_longitude_fails(in_, expected_exception) -> None:
    with pytest.raises(expected_exception):
        processing.validate_longitude(in_)


# -- GENDER -----


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        ("male", "M"),
        ("Male", "M"),
        ("female", "F"),
        ("Female", "F"),
        ("m", "M"),
        ("M", "M"),
        ("f", "F"),
        ("F", "F"),
        ("o", "O"),
        ("O", "O"),
        ("MaLE", "M"),
        (" fEMALE  ", "F"),
    ],
)
def test__resolve_gender(in_, out) -> None:
    assert processing.resolve_gender(in_) == out


@pytest.mark.parametrize(
    ("in_", "expected_exception"),
    [
        (1, AttributeError),
        ("unknown", ValueError),
        ("Mle", ValueError),
        (None, AttributeError),
    ],
)
def test__resolve_gender_fails(in_, expected_exception) -> None:
    with pytest.raises(expected_exception):
        processing.resolve_gender(in_)


# -- PHASE ---


@pytest.mark.parametrize(
    ("in_", "out"),
    [
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("3ph", "3"),
        ("2nd", "2"),
        ("a", "1"),
        ("B", "2"),
        ("c", "3"),
        ("Red", "1"),
        ("yellow", "2"),
        ("BLUE", "3"),
    ],
)
def test__resolve_phase(in_, out) -> None:
    assert processing.resolve_phase(in_) == out


@pytest.mark.parametrize(
    ("in_", "expected_exception"),
    [
        (1, TypeError),
        ("unknown", ValueError),
        ("three", ValueError),
        (None, TypeError),
    ],
)
def test__resolve_phase_fails(in_, expected_exception) -> None:
    with pytest.raises(expected_exception):
        processing.resolve_phase(in_)


# ------------------------- dataset cleaning -------------------------


def test__shed_nones() -> None:
    assert processing.shed_nones({"a": 1, "b": 0, "c": None, "d": ""}) == {
        "a": 1,
        "b": 0,
        "d": "",
    }


def test_df_nan_to_none(grid_recordset_raw):
    grid_rs = deepcopy(grid_recordset_raw)
    grid_rs.records[0]["created_at"] = pandas.NaT
    grid_rs.records[0]["primary_input_source"] = ""
    grid_df = pandas.DataFrame(grid_rs.records)
    grid_df_none = processing.df_nan_to_none(in_df=grid_df)

    assert grid_df_none.loc[0]["primary_input_source"] is None
    assert grid_df_none.loc[0]["created_at"] is None
    # the other record is untouched, but created_at is also None as it was absent
    assert grid_df_none.loc[1]["primary_input_source"] is not None
    assert grid_df_none.loc[1]["created_at"] is None


# ------------------------- generic field adapters -------------------------


@pytest.mark.usefixtures("_database")
def test__clean_record_values_by_type(grid_record_editable) -> None:
    grid_record_editable["name"] = "   words floating in spaces     "
    assert (
        processing.process_record_values_by_type(
            grid_record_editable, table="data_grids"
        )["name"]
        == "words floating in spaces"
    )


@pytest.mark.usefixtures("_database")
def test__clean_record_values_by_field(grid_record_editable) -> None:
    grid_record_editable["country"] = "france"
    assert (
        processing.process_record_values_by_field(
            grid_record_editable, table="data_grids"
        )["country"]
        == "FR"
    )
