import sys

import pytest
import sqlalchemy.exc
from psycopg import capabilities
from psycopg.errors import FeatureNotSupported
from sqlalchemy import Connection, text

from dataworker import config, logger
from dataworker.database import (
    close_all_connections,
    get_connection,
    get_engine,
    get_or_create_connection,
    get_table,
    get_table_prefix_resilient,
    manage_connection,
    manage_read_connection,
    open_connection,
)


@pytest.fixture(autouse=True)
def _setup_databases(database_creator) -> None:
    """We need two databases to test multi-database features."""
    database_creator("other")
    database_creator("test")


@pytest.fixture(autouse=True)
def _close_all_connections_fixture():
    yield
    close_all_connections()


# ------------------------- engines -------------------------


def test__get_engine() -> None:
    """Smoke test."""
    get_engine()


def test__get_engine__url() -> None:
    """Default engine connects to the configured database."""
    engine = get_engine()
    assert engine.url.database == config.database.name


def test__get_engine__specify_name() -> None:
    """Engines to other databases on the same server can be created."""
    default_engine = get_engine()
    other_engine = get_engine(db_name="other")
    assert other_engine.url.host == default_engine.url.host
    assert other_engine.url.port == default_engine.url.port
    assert other_engine.url.username == default_engine.url.username
    assert other_engine.url.password == default_engine.url.password
    assert other_engine.url.database == "other"


# ------------------------- connections -------------------------


def test__open_connection() -> None:
    """Smoke test."""
    with open_connection():
        pass


def test__open_connection__not_singleton() -> None:
    """Multiple calls return different connections."""
    with open_connection() as connection_a, open_connection() as connection_b:
        assert connection_a is not connection_b


def test__open_connection__database_name() -> None:
    """Database name is the one in the config."""
    with open_connection() as default_connection:
        assert default_connection.engine.url.database == config.database.name


def test__open_connection__specify_database() -> None:
    """Database name can be specified."""
    with open_connection("other") as connection_to_other:
        assert connection_to_other.engine.url.database == "other"


@pytest.mark.original_connection
def test__get_connection() -> None:
    """Smoke test."""
    get_connection()


@pytest.mark.original_connection
def test__get_connection__singleton() -> None:
    """Second execution gives the same connection."""
    with get_connection() as connection_a, get_connection() as connection_b:
        assert connection_a is connection_b


@pytest.mark.original_connection
def test__get_or_create_connection__singleton() -> None:
    """Second execution gives the same connection."""
    with (
        get_or_create_connection() as connection_a,
        get_or_create_connection() as connection_b,
    ):
        assert connection_a is connection_b


@pytest.mark.original_connection
def test__get_connection__database_name() -> None:
    """Default database is the one from the config."""
    with open_connection() as default_connection:
        assert default_connection.engine.url.database == config.database.name


@pytest.mark.original_connection
def test__get_connection__specify_database() -> None:
    """Connection can be open to different databases."""
    with (
        get_or_create_connection() as default_connection,
        get_or_create_connection("other") as other_connection,
        get_or_create_connection("other") as other_connection_again,
    ):
        assert default_connection.engine.url.database == config.database.name
        assert default_connection is not other_connection
        assert other_connection.engine.url.database == "other"
        assert other_connection_again is other_connection


@pytest.mark.original_connection
def test__get_connection__closed() -> None:
    connection = get_connection()
    connection.execute(text("select 1"))  # it's working
    connection.close()
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        connection.execute(text("select 1"))
    other_connection = get_connection()
    other_connection.execute(text("select 1"))  # has been recreated
    assert other_connection is not connection


@pytest.mark.original_connection
def test__get_connection__invalidated() -> None:
    connection = get_connection()
    connection.execute(text("select 1"))  # it's working
    connection.invalidate()
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        connection.execute(text("select 1"))
    other_connection = get_connection()
    other_connection.execute(text("select 1"))  # has been rolled back
    assert other_connection is connection  # same connection


@pytest.mark.original_connection
def test__get_or_create_connection__transaction_status_inerror(mocker) -> None:
    connection = get_or_create_connection()
    connection.execute(text("select 1"))  # it's working
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        connection.execute(text("INSERT INTO foo VALUES (True)"))
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        connection.execute(text("select 1"))  # rollback required
    other_connection = get_or_create_connection()  # does rollback
    other_connection.execute(text("select 1"))  # has been rolled back
    assert other_connection is connection  # same connection

    # put the connection in INERROR state
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        connection.execute(text("INSERT INTO foo VALUES (True)"))
    # we mock the status to prevent rollbacking the connection, leaving a bad state
    mocker.patch("psycopg._connection_info.ConnectionInfo.transaction_status", "FOO")
    # in level debug (and higher), this should not fail, as connectioninfo is lazy
    logger.configure(handlers=[{"sink": sys.stdout, "level": "DEBUG"}])
    other_connection = get_or_create_connection()
    # in level trace, this should fail, as connectioninfo will query for isolation level
    logger.configure(handlers=[{"sink": sys.stdout, "level": "TRACE"}])
    with pytest.raises(sqlalchemy.exc.SQLAlchemyError):
        other_connection = get_or_create_connection()


@pytest.mark.original_connection
def test__get_or_create_connection__db_recovery_mode(mocker) -> None:
    """Test handling a connection that pgBouncer returns as a DB in recovery.

    Ideally handling a connection that pgBouncer keeps giving us to a DB in recovery.
    It is not doing exactly what we want. We may introduce a mocked Connection
    returning always the error, but it's not what we want, as we would ideally
    detect it before execution. See also:
    https://github.com/psycopg/psycopg/issues/904#issuecomment-2348560170
    """
    connection = get_or_create_connection()
    connection.execute(text("select 1"))  # it's working
    with pytest.raises(sqlalchemy.exc.NotSupportedError):
        connection.execute(
            text("""do $$
            begin
                raise 'pretending to be in recovery mode' using errcode = 'feature_not_supported';
            end
            $$ language plpgsql
        """)  # noqa: E501
        )
    with pytest.raises(sqlalchemy.exc.InternalError):
        connection.execute(text("select 1"))

    rollback = mocker.spy(Connection, "rollback")
    other_connection = get_or_create_connection()
    other_connection.execute(text("select 1"))
    assert rollback.called is True


@pytest.mark.original_connection
def test__get_or_create_connection__read_only_check(caplog) -> None:
    connection = get_or_create_connection()
    connection.execute(text("select 1"))  # it's working
    dbapi_connection = connection.connection.driver_connection
    assert bool(dbapi_connection.read_only) is False

    connection.rollback()
    connection.execution_options(postgresql_readonly=True)
    dbapi_connection = connection.connection.driver_connection
    assert dbapi_connection.read_only is True


@pytest.mark.original_connection
@pytest.mark.skipif(
    condition=capabilities.has_send_close_prepared() is True,
    reason="skip prepared_max to None if libpq (>=17) can send 'close prepared'",
)
def test__get_or_create_connection__prepared_max() -> None:
    """Testing that deallocation is disabled when using LibPQ<17.

    See the hint under
    https://www.psycopg.org/psycopg3/docs/advanced/prepare.html#using-prepared-statements-with-pgbouncer
    """
    connection = get_or_create_connection()
    driver_connection = connection._dbapi_connection.driver_connection
    assert driver_connection.prepared_max is None


@pytest.mark.original_connection
def test__get_or_create_connection__isolation_level_default() -> None:
    connection = get_or_create_connection()
    assert not connection._dbapi_connection.read_only
    assert connection._is_autocommit_isolation() is False
    assert connection.connection.autocommit is False
    assert connection.get_isolation_level() == "READ COMMITTED"


@pytest.mark.original_connection
def test__get_or_create_connection__isolation_level_autocommit() -> None:
    connection = get_or_create_connection(isolation_level="AUTOCOMMIT")
    assert not connection._dbapi_connection.read_only
    assert connection._is_autocommit_isolation() is True
    assert connection.connection.autocommit is True
    assert connection.get_isolation_level() == connection.default_isolation_level


@pytest.mark.original_connection
def test__get_or_create_connection__readonly() -> None:
    connection = get_or_create_connection(postgresql_readonly=True)
    assert connection._dbapi_connection.read_only is True
    assert connection.get_isolation_level() == "READ COMMITTED"


@pytest.mark.original_connection
def test__get_connection__readonly_autocommit() -> None:
    connection = get_connection()
    assert connection._dbapi_connection.read_only is True
    assert connection._is_autocommit_isolation() is True
    assert connection.connection.autocommit is True
    assert connection.get_isolation_level() == connection.default_isolation_level


@pytest.mark.original_connection
def test__get_connections__caplog_tests(caplog) -> None:
    from dataworker.database import engines, thread_local

    for connection_name in ["test-None-False", "test-AUTOCOMMIT-True"]:
        if connection_name in thread_local.connections:
            del thread_local.connections[connection_name]
    if "test" in engines:
        del engines["test"]

    old_value = config.get("logging.sql")
    config.set("logging.sql", True)
    connection = get_connection()
    caplog.clear()
    connection.execute(text("select 1")).scalar()
    log_message = caplog.messages[0]
    assert log_message.startswith("BEGIN")
    assert "autocommit" in log_message
    assert connection._dbapi_connection.read_only is True
    # NB: assertion uncommented as the behaviour is flaky, look again somewhen...
    # assert connection.execute(text("show transaction_read_only")).scalar() == "on"
    transaction_level = text("show transaction isolation level")
    assert connection.execute(transaction_level).scalar() == "read committed"

    connection = get_or_create_connection()
    caplog.clear()
    connection.execute(text("select 1")).scalar()
    log_message = caplog.messages[0]
    assert log_message.startswith("BEGIN")
    assert "autocommit" not in log_message
    assert connection.execute(text("show transaction_read_only")).scalar() == "off"
    transaction_level = text("show transaction isolation level")
    assert connection.execute(transaction_level).scalar() == "read committed"

    # clean up for upcoming tests
    del engines["test"]
    config.set("logging.sql", old_value)


@pytest.mark.original_connection
def test__manage_connection():
    def wrapped_func():
        connection = get_or_create_connection()
        assert connection._dbapi_connection.read_only is None
        assert connection._is_autocommit_isolation() is False
        assert connection.connection.autocommit is False

    manage_connection(wrapped_func)()


@pytest.mark.original_connection
def test__manage_read_connection():
    def wrapped_func():
        connection = get_connection()
        assert connection._dbapi_connection.read_only is True
        assert connection._is_autocommit_isolation() is True
        assert connection.connection.autocommit is True

    manage_read_connection(wrapped_func)()


@pytest.mark.original_connection
def test__manage_connection_raise_feature_not_supported(mocker):
    config.set("database.commit_managed_transactions", True)

    mocker.patch(
        "dataworker.database.get_or_create_connection",
        side_effect=FeatureNotSupported,
    )
    connection = get_or_create_connection()

    def wrapped_func(connection):
        with pytest.raises(FeatureNotSupported):
            connection.execute(text("select 1"))

    with pytest.raises(FeatureNotSupported):
        manage_connection(wrapped_func)(connection)

    # restore setting
    config.set("database.commit_managed_transactions", False)


# ------------------------- get table -------------------------


@pytest.mark.usefixtures("_database")
def test__get_table__success() -> None:
    assert isinstance(get_table("data_meters"), sqlalchemy.Table)


@pytest.mark.usefixtures("_database")
def test__get_table__error() -> None:
    with pytest.raises(RuntimeError):
        get_table("data_gnomes")


@pytest.mark.usefixtures("_database")
def test__get_table_prefix_resilient__success() -> None:
    assert isinstance(get_table_prefix_resilient("data_meters"), sqlalchemy.Table)


@pytest.mark.usefixtures("_database")
def test__get_table_prefix_resilient__error() -> None:
    with pytest.raises(RuntimeError):
        get_table_prefix_resilient("data_gnomes")


@pytest.mark.usefixtures("_database")
def test__get_table_prefix_resilient__auto_add_prefix() -> None:
    assert isinstance(get_table_prefix_resilient("meters"), sqlalchemy.Table)


@pytest.mark.usefixtures("_database")
def test__get_table_prefix_resilient__no_prefix() -> None:
    assert isinstance(get_table_prefix_resilient("imports"), sqlalchemy.Table)
