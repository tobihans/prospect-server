from functools import partial

import pytest

from dataworker.dataset import RecordSet
from dataworker.queries import count_rows
from tests.utils import (
    RecordSetWithModifiers,
    insert_records,
    insert_recordset,
    summarize_records,
)


@pytest.mark.usefixtures("_database")
def test__insert_records(
    grid_sample_record_processed, other_grid_sample_record_processed
):
    insert_records(
        "data_grids_ts",
        [grid_sample_record_processed, other_grid_sample_record_processed],
    )
    assert count_rows("data_grids_ts") == 2


@pytest.mark.usefixtures("_database")
def test__insert_recordset(
    grid_sample_record_processed, other_grid_sample_record_processed
):
    insert_recordset(
        RecordSet(
            "data_grids_ts",
            [grid_sample_record_processed, other_grid_sample_record_processed],
        ),
    )
    assert count_rows("data_grids_ts") == 2


def test__summarize_records():
    assert summarize_records([
        {"size": 12, "color": "purple", "petal_count": 4},
        {"size": 49, "color": "turquoise", "petal_count": 9},
        {"size": 17, "color": "turquoise", "petal_count": 9},
        {"size": 5, "color": "turquoise", "petal_count": 9},
    ]) == {
        "fields": ["color", "petal_count", "size"],
        "hash": "26f85a55208b00eb204c39dc37f40ae0f60525cd09e1caa8f4d7ad6f5ec0fb2c",
        "sample_count": 4,
        "value_count": 12,
    }


def test__RecordSetWithModifiers(grid_sample_record, other_grid_sample_record):
    """Test RecordSetWithModifiers can be used and gets checked."""
    # use partial to avoid repeating the same call with the same arguments
    part = partial(
        RecordSetWithModifiers,
        "data_grids_ts",
        [grid_sample_record, other_grid_sample_record],
    )

    # default should work
    _ = part()

    # Testing modifiers
    # passing an empty list works
    _ = part(modifiers=[])
    # passing a modifiers list of the same size as the recordset size should work
    _ = part(modifiers=[{"a": 1}, {"b": 2}])
    # passing a modifiers list of smaller size as the recordset size should break
    with pytest.raises(ValueError):  # noqa: PT011
        _ = part(modifiers=[{"a": "b"}])
    # passing a modifiers list of larger size as the recordset size should break
    with pytest.raises(ValueError):  # noqa: PT011
        _ = part(modifiers=[{"a": 1}, {"b": 2}, {"c": 3}])

    # Testing id_adjustments
    # passing an empty list works
    _ = part(id_adjustments=[])
    # passing a id_adjustments list of the same size as the recordset size should work
    _ = part(modifiers=[{"a": 1}, {}], id_adjustments=[{"a": ("table", "id")}, {}])
    # passing a id_adjustments list of smaller size as the recordset size should break
    with pytest.raises(ValueError):  # noqa: PT011
        _ = part(modifiers=[{"a": 1}, {}], id_adjustments=[{"a": ("table", "id")}])
    # passing a id_adjustments list of larger size as the recordset size should break
    with pytest.raises(ValueError):  # noqa: PT011
        _ = part(modifiers=[{"a": 1}, {}], id_adjustments=[{"a": ("t", "id")}, {}, {}])
