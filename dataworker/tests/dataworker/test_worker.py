def test_worker(mocker):
    mocker.patch("dataworker.jobqueue.faktory.Worker.run", return_value=None)
    from worker import worker

    assert len(worker._tasks) == 27
    assert len(worker.get_queues()) == 1
    assert worker.get_queues()[0] == "test_queue"
