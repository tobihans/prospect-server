import random

from dataworker import random as dw_random


def test__set_seed() -> None:
    with dw_random.set_seed("something"):
        random_1 = random.random()
        random_2 = random.random()
    with dw_random.set_seed("something"):
        random_1_again = random.random()
        random_2_again = random.random()
    assert random_1 == random_1_again
    assert random_2 == random_2_again


def test__jiggle() -> None:
    with dw_random.set_seed("something"):
        assert dw_random.jiggle(0.5, 50) == 52.746885338811076
        assert dw_random.jiggle(0.5, 50) == 32.74190697085929
        assert dw_random.jiggle(0.5, 50) == 60.48636231645892


def test__jiggle_record() -> None:
    with dw_random.set_seed("something"):
        record = {"a": 10, "b": 20, "c": 30}
        assert dw_random.jiggle_values(keys=["a", "b"], ratio=0.1, record=record) == {
            "a": 10.109875413552443,
            "b": 18.619352557668744,
            "c": 30,
        }


def test__jiggle_integers() -> None:
    with dw_random.set_seed("something else"):
        record = {"a": 10, "b": 20, "c": 30}
        assert dw_random.jiggle_integers(keys=["a", "b"], ratio=0.1, record=record) == {
            "a": 9,
            "b": 19,
            "c": 30,
        }
