import pytest

from dataworker.storage import DataFile, read_datafile_from_storage


@pytest.fixture(autouse=True)
def _mock_minio_client(mocker) -> None:
    mocker.patch("dataworker.storage.get_minio_client")


def test__read_datafile__format_not_recognized() -> None:
    datafile = DataFile("filename.ext", "application/lolml")
    with pytest.raises(NotImplementedError):
        read_datafile_from_storage(datafile)
