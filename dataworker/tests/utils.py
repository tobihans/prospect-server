"""Common tools for test modules."""

import contextlib
import json
from collections import defaultdict
from collections.abc import Generator, Iterable
from dataclasses import dataclass, field
from operator import eq, itemgetter
from pathlib import Path
from typing import Any

import funcy
from sqlalchemy import Connection, event, select

from dataworker import logger
from dataworker.database import get_table
from dataworker.dataset import (
    RecordSet,
    count_values,
    hash_data_object,
    key_set,
)
from dataworker.metadata import ImportContext
from dataworker.processing import shed_nones
from dataworker.queries import count_rows, execute, fetch_scalar
from dataworker.storage import DataFile
from dataworker.task import prepare_for_storage
from dataworker.types import Record

TEST_DATA_DIR = "data"


def load_file_content(filepath: str) -> str:
    """Load the content of a file interpreted as text."""
    logger.debug("loading file content: {filepath}", filepath=filepath)
    return Path(filepath).expanduser().resolve().read_text()


def load_test_data(datafile: str | DataFile) -> str | dict:
    """Load data from file from the test data directory.

    Return text content of the file. If the file extension is json, return the
    parsed data dict.
    """
    if isinstance(datafile, DataFile):
        datafile = datafile.file_key
    path = Path(__file__).parent / TEST_DATA_DIR / datafile
    content = load_file_content(path)

    if path.suffix == ".json":
        return json.loads(content)
    else:
        return content


# --------------------------------------------------


def summarize_records(records: Iterable[Record]) -> dict[str, Any]:
    """Combine multiple analytic functions on a list of records."""
    return {
        "sample_count": len(records),
        "fields": sorted(key_set(records)),
        "hash": hash_data_object(records),
        "value_count": count_values(records),
    }


# Remove fields from datawarehouse rows that are not stable across test runs,
# and Nones. Use this function on returned data before comparison to expected values.
prepare_for_compare = funcy.compose(
    funcy.partial(funcy.omit, keys=("created_at", "updated_at", "id")), shed_nones
)


def prepare_many_for_compare(
    records: Iterable[Record], sort_key: str = "uid"
) -> list[Record]:
    """Call prepare_for_compare over a list-like of records and sort them."""
    return sorted([prepare_for_compare(t) for t in records], key=itemgetter(sort_key))


@execute
def insert_records(table: str, records: Iterable[Record], returning: bool = False):
    """Simple insertion, for tests only."""
    table = get_table(table)
    statement = table.insert().values(records)
    if returning:
        return statement.returning(table)
    return statement


@execute
def insert_recordset(recordset: RecordSet, returning: bool = False):
    """Simple insertion, for tests only."""
    table = get_table(recordset.table)
    statement = table.insert().values(recordset.records)
    if returning:
        return statement.returning(table)
    return statement


def insert_recordset_returning_dicts(recordset: RecordSet) -> list[dict]:
    inserts = insert_recordset(recordset=recordset, returning=True)
    table = get_table(recordset.table)
    inserted_dicts = []
    for insert in inserts:
        inserted_dict = {}
        for i, col in enumerate(table.c):
            inserted_dict[col.name] = insert[i]
        inserted_dicts.append(inserted_dict)
    return inserted_dicts


@contextlib.contextmanager
def count_queries(conn: Connection) -> Generator[list[str], None, None]:
    """Retrieve the queries made to assess/observe/improve performance."""
    queries = []

    def before_cursor_execute(conn, cursor, statement, _params, _context, _executemany):
        queries.append(statement)

    event.listen(conn, "before_cursor_execute", before_cursor_execute)
    try:
        yield queries
    finally:
        event.remove(conn, "before_cursor_execute", before_cursor_execute)


@dataclass
class RecordSetWithModifiers(RecordSet):
    """Subclass of RecordSet adding modifiers for test setup.

    - modifiers takes a list of modified keys with their new values
    - apply_after specifies which modification should be applied
      after post_processing
    - id_adjustments takes a list of dicts pointing the column name
      to be adjusted to the value of a tuple(table_name, column_name)
      (required as sequences of ids are not resetted between tests)
    """

    modifiers: list[Record] = field(default_factory=list)
    apply_after: list[str] = field(default_factory=list)
    id_adjustments: list[Record] = field(default_factory=list)

    def __post_init__(self):
        """Some health-checks about the length of the lists."""
        records_count = len(self.records)
        for attribute in ["modifiers", "id_adjustments"]:
            if len(getattr(self, attribute)) not in (
                0,
                records_count,
            ):
                raise ValueError(
                    f"The list defined as {attribute} can only be empty or have the "
                    f"same length as `records` ({records_count})"
                )


# ----------------- test data modification -----------------


def prepare_test_data(
    recordsets: list[RecordSetWithModifiers],
    import_context: ImportContext,
) -> dict[str, list[dict[str, Any]]]:
    """Prepare and insert test data for parametrized tests."""
    inserts = defaultdict(list)
    for recordset in recordsets:
        if recordset.modifiers:
            for i, (record, modifier) in enumerate(
                zip(recordset.records, recordset.modifiers, strict=True)
            ):
                if modifier:
                    recordset.records[i] = record | modifier
        processed_recordset = prepare_for_storage(
            record_set=recordset, import_context=import_context, is_internal=True
        )
        # Overwrite value after post_processing if requested so
        for modifier_key in recordset.apply_after:
            for record, modifier in zip(
                processed_recordset.records, recordset.modifiers, strict=True
            ):
                if modifier_key in modifier:
                    record[modifier_key] = modifier[modifier_key]
        # Adjust IDs of linked tables
        if recordset.id_adjustments:
            for id_adjustment, record in zip(
                recordset.id_adjustments, processed_recordset.records, strict=True
            ):
                for column, (foreign_table, foreign_column) in id_adjustment.items():
                    # try to get the data from previous inserts, taking the last one
                    try:
                        record[column] = inserts[foreign_table][-1][foreign_column]
                    except (KeyError, IndexError):
                        # else get it from the DB, taking also the last one
                        foreign_table = get_table(foreign_table)
                        record[column] = fetch_scalar(
                            select(foreign_table.c[foreign_column]).order_by(
                                foreign_table.c.updated_at.desc()
                            )
                        )

        inserts[recordset.table] += insert_recordset_returning_dicts(
            recordset=processed_recordset
        )
        # limit the count per import_id if applicable
        # TODO: use the caplog fixture to avoid querying the DB?
        table = get_table(recordset.table)
        where_column_ops = []
        if "import_id" in table.c:
            where_column_ops.append(eq(table.c.import_id, import_context.import_id))
        row_count = count_rows(recordset.table, where_column_ops)
        assert row_count == len(inserts[recordset.table])
    return inserts
