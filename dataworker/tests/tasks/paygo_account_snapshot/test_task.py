import datetime
import json

import pytest
from inline_snapshot import snapshot
from time_machine import travel

import tests.utils
from dataworker.database import get_table
from dataworker.queries import fetch_dict_data
from dataworker.task import InternalTask
from tasks.internal.paygo_accounts_snapshots import task as pas_task
from tests.utils import load_test_data


def parse_data(data):
    if "created_at" in data and data["created_at"] is not None:
        data["created_at"] = datetime.datetime.strptime(
            data["created_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "updated_at" in data and data["updated_at"] is not None:
        data["updated_at"] = datetime.datetime.strptime(
            data["updated_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "purchase_date" in data and data["purchase_date"] is not None:
        data["purchase_date"] = datetime.datetime.strptime(
            data["purchase_date"], "%Y-%m-%d"
        )
    if "repossession_date" in data and data["repossession_date"] is not None:
        data["repossession_date"] = datetime.datetime.strptime(
            data["repossession_date"], "%Y-%m-%d"
        )
    if "paid_off_date" in data and data["paid_off_date"] is not None:
        data["paid_off_date"] = datetime.datetime.strptime(
            data["paid_off_date"], "%Y-%m-%d"
        )
    if "paid_at" in data and data["paid_at"] is not None:
        data["paid_at"] = datetime.datetime.strptime(
            data["paid_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "latest_snapshot" in data and data["latest_snapshot"] is not None:
        data["latest_snapshot"] = datetime.datetime.strptime(
            data["latest_snapshot"], "%Y-%m-%d"
        )
    if "custom" in data and data["custom"] is not None:
        data["custom"] = json.loads(data["custom"])
    return data


@pytest.fixture(scope="module")
def datawarehouse_shs_data():
    """Load shs test data from file."""
    data_shs_sample = load_test_data("records_for_task_test/shs_sample_task.json")
    data_shs_sample = [parse_data(i) for i in data_shs_sample]
    return data_shs_sample


@pytest.fixture(scope="module")
def datawarehouse_payments_ts_data():
    """Load payments_ts test data from file."""
    data_payments_ts_sample = load_test_data(
        "records_for_task_test/payments_ts_sample_task.json"
    )
    data_payments_ts_sample = [parse_data(i) for i in data_payments_ts_sample]
    return data_payments_ts_sample


@pytest.fixture(scope="module")
def datewarehouse_data_snapshot_latest():
    """Load latest_snaoshot test data from file."""
    data_latest_snapshot_sample = load_test_data(
        "records_for_task_test/latest_snapshot_sample_task.json"
    )
    data_latest_snapshot_sample = [parse_data(i) for i in data_latest_snapshot_sample]
    return data_latest_snapshot_sample


# --------------------------------------------------


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


@travel(datetime.datetime(2023, 11, 20, tzinfo=datetime.UTC), tick=False)
def test__create_paygo_accounts_snapshots(
    datawarehouse_shs_data,
    datawarehouse_payments_ts_data,
    datewarehouse_data_snapshot_latest,
) -> None:
    tests.utils.insert_records("data_shs", datawarehouse_shs_data)
    tests.utils.insert_records("data_payments_ts", datawarehouse_payments_ts_data)
    tests.utils.insert_records(
        "data_paygo_accounts_snapshots_ts", datewarehouse_data_snapshot_latest
    )
    now = datetime.datetime.now(tz=datetime.UTC).replace(tzinfo=None)

    paygo_task = InternalTask.from_function(pas_task.run)
    paygo_task()

    table_name = "data_paygo_accounts_snapshots_ts"

    table = get_table(table_name)
    statement = table.select().order_by(table.c.snapshot_date)
    result = fetch_dict_data(statement).all()

    last_account_c95 = list(
        filter(lambda x: x["account_external_id"] == "C950014", result)
    )[-1]
    last_account_c102 = list(
        filter(lambda x: x["account_external_id"] == "C1020015", result)
    )[-1]
    last_account_c47 = list(
        filter(lambda x: x["account_external_id"] == "C470013", result)
    )[-1]

    # check total_remaining_amount = 0 if account_status = 'Completed'
    assert last_account_c95 == {
        "account_external_id": "C950014",
        "account_status": "completed",
        "account_uid": "1_payg_ops_C950014",
        "created_at": now,
        "cumulative_days_locked": 1,
        "cumulative_paid_amount": 40000.0,
        "custom": {},
        "data_origin": "payg_ops",
        "days_in_repayment": 32,
        "days_to_cutoff": 28,
        "organization_id": 1,
        "snapshot_date": datetime.date(2023, 9, 30),
        "source_id": 4,
        "total_remaining_amount": 0.0,
        "updated_at": now,
    }

    assert last_account_c102 == {
        "account_external_id": "C1020015",
        "account_status": "locked",
        "account_uid": "1_payg_ops_C1020015",
        "created_at": now,
        "cumulative_days_locked": 66,
        "cumulative_paid_amount": 40000.0,
        "custom": {},
        "data_origin": "payg_ops",
        "days_in_repayment": 111,
        "days_to_cutoff": -51,
        "organization_id": 1,
        "snapshot_date": datetime.date(2023, 11, 19),
        "source_id": 4,
        "total_remaining_amount": 70000.0,
        "updated_at": now,
    }

    # check if active account that paid regularily is still active
    assert last_account_c47 == {
        "account_external_id": "C470013",
        "account_status": "active",
        "account_uid": "1_payg_ops_C470013",
        "created_at": now,
        "cumulative_days_locked": 0,
        "cumulative_paid_amount": 25000.0,
        "custom": {},
        "data_origin": "payg_ops",
        "days_in_repayment": 19,
        "days_to_cutoff": 11,
        "organization_id": 1,
        "snapshot_date": datetime.date(2023, 11, 19),
        "source_id": 4,
        "total_remaining_amount": 185000.0,
        "updated_at": now,
    }

    assert [last_account_c95, last_account_c102, last_account_c47] == snapshot([
        {
            "account_external_id": "C950014",
            "account_status": "completed",
            "account_uid": "1_payg_ops_C950014",
            "created_at": datetime.datetime(2023, 11, 20, 0, 0),
            "cumulative_days_locked": 1,
            "cumulative_paid_amount": 40000.0,
            "custom": {},
            "data_origin": "payg_ops",
            "days_in_repayment": 32,
            "days_to_cutoff": 28,
            "organization_id": 1,
            "snapshot_date": datetime.date(2023, 9, 30),
            "source_id": 4,
            "total_remaining_amount": 0.0,
            "updated_at": datetime.datetime(2023, 11, 20, 0, 0),
        },
        {
            "account_external_id": "C1020015",
            "account_status": "locked",
            "account_uid": "1_payg_ops_C1020015",
            "created_at": datetime.datetime(2023, 11, 20, 0, 0),
            "cumulative_days_locked": 66,
            "cumulative_paid_amount": 40000.0,
            "custom": {},
            "data_origin": "payg_ops",
            "days_in_repayment": 111,
            "days_to_cutoff": -51,
            "organization_id": 1,
            "snapshot_date": datetime.date(2023, 11, 19),
            "source_id": 4,
            "total_remaining_amount": 70000.0,
            "updated_at": datetime.datetime(2023, 11, 20, 0, 0),
        },
        {
            "account_external_id": "C470013",
            "account_status": "active",
            "account_uid": "1_payg_ops_C470013",
            "created_at": datetime.datetime(2023, 11, 20, 0, 0),
            "cumulative_days_locked": 0,
            "cumulative_paid_amount": 25000.0,
            "custom": {},
            "data_origin": "payg_ops",
            "days_in_repayment": 19,
            "days_to_cutoff": 11,
            "organization_id": 1,
            "snapshot_date": datetime.date(2023, 11, 19),
            "source_id": 4,
            "total_remaining_amount": 185000.0,
            "updated_at": datetime.datetime(2023, 11, 20, 0, 0),
        },
    ])


@travel(datetime.datetime(2023, 11, 20, tzinfo=datetime.UTC), tick=False)
def test__create_paygo_accounts_snapshots_empty_shs(
    datawarehouse_payments_ts_data, datewarehouse_data_snapshot_latest, caplog
) -> None:
    tests.utils.insert_records("data_payments_ts", datawarehouse_payments_ts_data)
    tests.utils.insert_records(
        "data_paygo_accounts_snapshots_ts", datewarehouse_data_snapshot_latest
    )

    paygo_task = InternalTask.from_function(pas_task.run)
    result = paygo_task()

    # assure that paygo_task returns None when shs data missing.
    assert not result

    # assure that the proper log message gets triggered.
    assert ("The following dataset(s) is empty: data_shs.") in caplog.text


@travel(datetime.datetime(2023, 11, 20, tzinfo=datetime.UTC), tick=False)
def test__create_paygo_accounts_snapshots_empty_payments_ts(
    datawarehouse_shs_data, datewarehouse_data_snapshot_latest, caplog
) -> None:
    tests.utils.insert_records("data_shs", datawarehouse_shs_data)
    tests.utils.insert_records(
        "data_paygo_accounts_snapshots_ts", datewarehouse_data_snapshot_latest
    )

    paygo_task = InternalTask.from_function(pas_task.run)
    result = paygo_task()

    # assure that paygo_task returns None when shs data missing.
    assert not result

    # assure that the proper log message gets triggered.
    assert ("The following dataset(s) is empty: data_payments_ts.") in caplog.text


@travel(datetime.datetime(2023, 11, 20, tzinfo=datetime.UTC), tick=False)
def test__create_paygo_accounts_snapshots_empty_shs_and_payments_ts(
    datawarehouse_shs_data, datewarehouse_data_snapshot_latest, caplog
) -> None:
    tests.utils.insert_records(
        "data_paygo_accounts_snapshots_ts", datewarehouse_data_snapshot_latest
    )

    paygo_task = InternalTask.from_function(pas_task.run)
    result = paygo_task()

    # assure that paygo_task returns None when shs data missing.
    assert not result

    # assure that the proper log message gets triggered.
    assert (
        "The following dataset(s) is empty: data_shs, data_payments_ts."
    ) in caplog.text
