import datetime
import json
from collections import Counter
from operator import itemgetter

from time_machine import travel

from dataworker.dataset import key_set
from tasks.internal.paygo_accounts_snapshots.processing import process
from tests.utils import load_test_data


def parse_data(data):
    if "created_at" in data and data["created_at"] is not None:
        data["created_at"] = datetime.datetime.strptime(
            data["created_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "updated_at" in data and data["updated_at"] is not None:
        data["updated_at"] = datetime.datetime.strptime(
            data["updated_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "purchase_date" in data and data["purchase_date"] is not None:
        data["purchase_date"] = datetime.datetime.strptime(
            data["purchase_date"], "%Y-%m-%d"
        )
    if "repossession_date" in data and data["repossession_date"] is not None:
        data["repossession_date"] = datetime.datetime.strptime(
            data["repossession_date"], "%Y-%m-%d"
        )
    if "paid_off_date" in data and data["paid_off_date"] is not None:
        data["paid_off_date"] = datetime.datetime.strptime(
            data["paid_off_date"], "%Y-%m-%d"
        )
    if "paid_at" in data and data["paid_at"] is not None:
        data["paid_at"] = datetime.datetime.strptime(
            data["paid_at"], "%Y-%m-%d %H:%M:%S.%f"
        )
    if "latest_snapshot" in data and data["latest_snapshot"] is not None:
        data["latest_snapshot"] = datetime.datetime.strptime(
            data["latest_snapshot"], "%Y-%m-%d"
        )
    if "custom" in data and data["custom"] is not None:
        data["custom"] = json.loads(data["custom"])
    return data


# @pytest.fixture(scope="module")
def datawarehouse_shs_data(file):
    """Load shs test data from file."""
    data_shs_sample = load_test_data(file)
    data_shs_sample = [parse_data(i) for i in data_shs_sample]
    return data_shs_sample


# @pytest.fixture(scope="module")
def datawarehouse_payments_ts_data(file):
    """Load payments_ts test data from file."""
    data_payments_ts_sample = load_test_data(file)
    data_payments_ts_sample = [parse_data(i) for i in data_payments_ts_sample]
    return data_payments_ts_sample


# @pytest.fixture(scope="module")
def datewarehouse_data_snapshot_latest(file):
    """Load latest_snapshot test data from file."""
    data_latest_snapshot_sample = load_test_data(file)
    data_latest_snapshot_sample = [parse_data(i) for i in data_latest_snapshot_sample]
    return data_latest_snapshot_sample


# --------------------------------------------------


@travel("2023-11-20")
def test__create_paygo_accounts_snapshots() -> None:
    paygo_snapshot = process(
        datawarehouse_shs_data("records_for_task_test/shs_sample_processor.json"),
        datawarehouse_payments_ts_data(
            "records_for_task_test/payments_ts_sample_processor.json"
        ),
        datewarehouse_data_snapshot_latest(
            "records_for_task_test/latest_snapshot_sample_processor.json"
        ),
    )

    # number of object extracted
    assert len(paygo_snapshot["data_paygo_accounts_snapshots"]) == 257
    # keys
    assert key_set(paygo_snapshot["data_paygo_accounts_snapshots"]) == {
        "total_remaining_amount",
        "cumulative_paid_amount",
        "source_id",
        "organization_id",
        "cumulative_days_locked",
        "account_status",
        "data_origin",
        "days_in_repayment",
        "days_to_cutoff",
        "snapshot_date",
        "account_external_id",
    }
    # check account_external_id counts
    counts = Counter(
        d["account_external_id"]
        for d in paygo_snapshot["data_paygo_accounts_snapshots"]
    )
    assert counts["C950014"] == 256
    assert counts["C35470012"] == 1

    # test the first records for exact values, after ordering the list
    ordered_snapshots = sorted(
        paygo_snapshot["data_paygo_accounts_snapshots"],
        key=itemgetter("account_external_id"),
    )
    assert ordered_snapshots[0]["account_external_id"] == "C35470012"
    assert ordered_snapshots[1] == {
        "account_external_id": "C950014",
        "snapshot_date": datetime.datetime.strptime(
            "2023-03-09 00:00:00", "%Y-%m-%d %H:%M:%S"
        ),
        "account_status": "active",
        "days_in_repayment": -30.0,
        "cumulative_paid_amount": 160000.0,
        "days_to_cutoff": 30.0,
        "cumulative_days_locked": 0.0,
        "organization_id": 1,
        "data_origin": "payg_ops",
        "source_id": 4,
        "total_remaining_amount": 510000.0,
    }


@travel("2023-11-20")
def test__create_paygo_accounts_snapshots_empty_shs() -> None:
    paygo_snapshot = process(
        datawarehouse_shs_data("records_for_task_test/shs_empty_sample_processor.json"),
        datawarehouse_payments_ts_data(
            "records_for_task_test/payments_ts_sample_processor.json"
        ),
        datewarehouse_data_snapshot_latest(
            "records_for_task_test/latest_snapshot_sample_processor.json"
        ),
    )

    # number of object extracted
    assert len(paygo_snapshot["data_paygo_accounts_snapshots"]) == 0


@travel("2023-11-20")
def test__create_paygo_accounts_snapshots_empty_paym() -> None:
    paygo_snapshot = process(
        datawarehouse_shs_data("records_for_task_test/shs_sample_processor.json"),
        datawarehouse_payments_ts_data(
            "records_for_task_test/payments_ts_empty_sample_processor.json"
        ),
        datewarehouse_data_snapshot_latest(
            "records_for_task_test/latest_snapshot_sample_processor.json"
        ),
    )

    # number of object extracted
    assert len(paygo_snapshot["data_paygo_accounts_snapshots"]) == 0
