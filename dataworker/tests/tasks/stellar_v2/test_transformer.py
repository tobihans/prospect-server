import pytest

from dataworker.dataset import key_set
from tasks.ingestion import stellar_v2
from tests.utils import insert_records, load_test_data


@pytest.fixture(scope="module")
def stellar_data():
    """Load test data from file."""
    return load_test_data("datafile/stellar_v2.json")


@pytest.fixture(scope="module")
def stellar_ffill_1_data():
    """Load test data from file."""
    return load_test_data("datafile/stellar_ffill_1_v2.json")


@pytest.fixture(scope="module")
def meters_ts_sample_data():
    """Load test data from file."""
    return load_test_data("records_for_task_test/meters_ts_sample_for_stellar_v2.json")


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


# --------------------------------------------------


def test__extract_meters_ts_data(stellar_data, meters_ts_sample_data) -> None:
    # insert some test data into data_meters_ts
    insert_records("data_meters_ts", meters_ts_sample_data)

    meters_ts = stellar_v2.transform(stellar_data)["data_meters_ts"]
    # number of object extracted (number of unique timestamps in incoming data)
    assert len(meters_ts) == 9
    # keys
    assert key_set(meters_ts) == {
        "power_w",
        "interval_seconds",
        "metered_at",
        "voltage_v",
        "manufacturer",
        "energy_lifetime_wh",
        "current_a",
        "frequency_hz",
        "serial_number",
        "power_factor",
        "signal_strength",
        "voltage_v_phase_1",
        "voltage_v_phase_2",
        "voltage_v_phase_3",
        "power_factor_phase_1",
        "power_factor_phase_2",
        "power_factor_phase_3",
        "power_w_phase_1",
        "power_w_phase_2",
        "power_w_phase_3",
        "current_a_phase_1",
        "current_a_phase_2",
        "current_a_phase_3",
    }
    # test the first records for exact values
    assert meters_ts[0] == {
        "metered_at": "2024-11-05T09:09:00.000Z",
        "manufacturer": "HOP",
        "serial_number": 111111,
        "power_factor": 0.931,
        "power_w": 1673.9,
        "energy_lifetime_wh": 31853320.0,
        "frequency_hz": 50.07,
        "signal_strength": None,
        "voltage_v_phase_1": 174.9,
        "voltage_v_phase_2": 0.0,
        "voltage_v_phase_3": 0.0,
        "power_factor_phase_1": 0.929,
        "power_factor_phase_2": None,
        "power_factor_phase_3": None,
        "power_w_phase_1": 1669.3,
        "power_w_phase_2": None,
        "power_w_phase_3": None,
        "current_a_phase_1": 10.631,
        "current_a_phase_2": None,
        "current_a_phase_3": None,
        "interval_seconds": 60,
        "voltage_v": 174.9,
        "current_a": 10.631,
    }


def test__extract_meters_data(stellar_data) -> None:
    meters = stellar_v2.extract_meters_data(stellar_data)
    # number of object extracted (should be 2)
    assert len(meters) == 2
    # keys
    assert key_set(meters) == {
        "manufacturer",
        "customer_country",
        "customer_latitude_p",
        "customer_address_e",
        "customer_latitude_b",
        "customer_longitude_e",
        "installation_date",
        "customer_longitude_b",
        "customer_longitude_p",
        "customer_latitude_e",
        "device_external_id",
        "serial_number",
        "customer_external_id",
        "customer_name_e",
        "customer_phone_p",
        "customer_address_p",
        "customer_name_p",
        "customer_phone_e",
    }
    # test the first records for exact values
    assert meters[0] == {
        "customer_external_id": "001",
        "customer_name_p": None,
        "customer_name_e": None,
        "customer_address_p": "some_address_p",
        "customer_address_e": "some_address_e",
        "customer_country": "NG",
        "customer_latitude_b": 9.1,
        "customer_longitude_b": 7.4,
        "customer_latitude_p": "some_lat_p",
        "customer_longitude_p": "some_lon_p",
        "customer_latitude_e": "some_lat_e",
        "customer_longitude_e": "some_lon_e",
        "customer_phone_p": "some_phone_1_p",
        "customer_phone_e": "some_phone_1_e",
        "device_external_id": 111111,
        "serial_number": 111111,
        "installation_date": "2024-06-01",
        "manufacturer": "HOP",
    }


# --------------------------------------------------

# Test ffill behaviour.


def test__ffill_nans(stellar_ffill_1_data, meters_ts_sample_data) -> None:
    # insert some test data into data_meters_ts
    insert_records("data_meters_ts", meters_ts_sample_data)

    # CASE 1
    # Incoming data is brand new meter with initial NaN.
    # We try ffill, but no data to ffill in DB.
    # We return original dataset.
    result = stellar_v2.transform(stellar_ffill_1_data)["data_meters_ts"]
    # assert that dataset is still returned with original None Value.
    assert result[0]["energy_lifetime_wh"] is None
    assert len(result) == 5
    assert (
        len(stellar_ffill_1_data["data"][0]["rawData_response"][0]["timestamps"])
    ) == 5
