import datetime

import pytest
import time_machine
from inline_snapshot import snapshot

from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from dataworker.queries import table_content
from dataworker.task import InternalTask
from tasks.internal.crm_payment_backed_by_payment_statement import (
    PAYMENTS_TABLE,
    VERIFICATIONS_TABLE,
    fetch_payments,
    fetch_statement_verified_uids,
    is_verified_against_mm_statement,
    prune_payload,
    run,
)
from tests.utils import RecordSetWithModifiers, prepare_test_data, summarize_records


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


# payment record
PAYMENTS_SAMPLE_DICT = {
    "payment_external_id": "123",
    "amount": 777.0,
    "currency": "UGX",
    "purpose": "payment",
    "paid_at": "2024-04-17 14:00:00",
    "account_external_id": "456",
    "account_origin": "meters",
    "provider_name": "airtel_zm",
}
PAYMENTS_SAMPLE_UID = "meters_222222_solar_coop_456_123_2024-04-17T14:00:00+00:00"
# mobile money statement
PAYMENTS_MM_SAMPLE_DICT = {
    "payment_external_id": "123",
    "amount": 777.0,
    "currency": "UGX",
    "purpose": "mobile money statement",
    "paid_at": "2024-04-16 14:00:00",
    "account_external_id": "789",
    "account_origin": "meters",
    "provider_name": "airtel_zm",
}
VERIFICATIONS_SAMPLE_DICT = {
    "subject_uid": "some_verified_uid",
    "subject_origin": "payments_ts",
    "check": "crm_payment_backed_by_payment_statement",
    "result": "ok",
}
SAMPLE_VERIFIED_UID = VERIFICATIONS_SAMPLE_DICT["subject_uid"]

PAYMENTS_RS = RecordSetWithModifiers(PAYMENTS_TABLE, [PAYMENTS_SAMPLE_DICT])
PAYMENTS_MM_RS = RecordSetWithModifiers(PAYMENTS_TABLE, [PAYMENTS_MM_SAMPLE_DICT])
VERIFICATIONS_RS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE, [VERIFICATIONS_SAMPLE_DICT]
)


# -------- test individual functions --------


# --------


VERIFICATIONS_RS_DIFFERENT_SUBJECT_ORIGIN = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"subject_origin": "shs"}],
)
VERIFICATIONS_RS_DIFFERENT_CHECK = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"check": "some_other_check"}],
)
VERIFICATIONS_RS_NO_SUCCESS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"result": "some_failure"}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([VERIFICATIONS_RS], [SAMPLE_VERIFIED_UID]),
        ([VERIFICATIONS_RS_DIFFERENT_SUBJECT_ORIGIN], []),
        ([VERIFICATIONS_RS_DIFFERENT_CHECK], []),
        ([VERIFICATIONS_RS_NO_SUCCESS], []),
    ],
)
def test__fetch_statement_verified_uids(
    recordsets: list[RecordSet], result: list, import_context: ImportContext
) -> None:
    """Test function list output on different input combinations."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    assert fetch_statement_verified_uids() == result


# --------


PAYMENTS_RS_DIFFERENT_PURPOSE = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_SAMPLE_DICT],
    modifiers=[{"purpose": "some_other_purpose"}],
)
PAYMENTS_RS_ALREADY_VERIFIED_UID = RecordSetWithModifiers(
    PAYMENTS_TABLE, [PAYMENTS_SAMPLE_DICT], modifiers=[{"uid": SAMPLE_VERIFIED_UID}]
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [PAYMENTS_RS],
            [
                {
                    "uid": PAYMENTS_SAMPLE_UID,
                    "payment_external_id": "123",
                    "provider_name": "airtel_zm",
                    "organization_id": 222222,
                    "source_id": 333333,
                    "data_origin": "solar_coop",
                    "import_id": 111111,
                    "amount": 777.0,
                    "currency": "UGX",
                },
            ],
        ),
        ([PAYMENTS_RS_DIFFERENT_PURPOSE], []),
        ([PAYMENTS_RS_ALREADY_VERIFIED_UID], []),
    ],
)
def test__fetch_payments(
    recordsets: list[RecordSet], result: list, import_context: ImportContext
) -> None:
    """Test function list output on different input combinations."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    assert fetch_payments([SAMPLE_VERIFIED_UID]) == result


# --------


PAYMENTS_RS_DIFFERENT_PURPOSE = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_MM_SAMPLE_DICT],
    modifiers=[{"purpose": "some_other_purpose"}],
)
PAYMENTS_MM_RS_NO_PURPOSE = RecordSetWithModifiers(
    PAYMENTS_TABLE, [PAYMENTS_MM_SAMPLE_DICT], modifiers=[{"purpose": None}]
)
PAYMENTS_MM_RS_DIFFERENT_PROVIDER_NAME = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_MM_SAMPLE_DICT],
    modifiers=[{"provider_name": "some_other_provider_name"}],
)
PAYMENTS_MM_RS_NO_PROVIDER_NAME = RecordSetWithModifiers(
    PAYMENTS_TABLE, [PAYMENTS_MM_SAMPLE_DICT], modifiers=[{"provider_name": None}]
)
PAYMENTS_MM_RS_DIFFERENT_PAYMENT_EXTERNAL_ID = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_MM_SAMPLE_DICT],
    modifiers=[{"payment_external_id": "some_other_payment_external_id"}],
)
PAYMENTS_MM_RS_DIFFERENT_AMOUNT = RecordSetWithModifiers(
    PAYMENTS_TABLE, [PAYMENTS_MM_SAMPLE_DICT], modifiers=[{"amount": 666}]
)
PAYMENTS_MM_RS_DIFFERENT_CURRENCY = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_MM_SAMPLE_DICT],
    modifiers=[{"currency": "some_other_curency"}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([PAYMENTS_MM_RS], True),
        ([PAYMENTS_RS_DIFFERENT_PURPOSE], False),
        ([PAYMENTS_MM_RS_NO_PURPOSE], False),
        ([PAYMENTS_MM_RS_DIFFERENT_PROVIDER_NAME], False),
        ([PAYMENTS_MM_RS_NO_PROVIDER_NAME], False),
        ([PAYMENTS_MM_RS_DIFFERENT_PAYMENT_EXTERNAL_ID], False),
        ([PAYMENTS_MM_RS_DIFFERENT_AMOUNT], False),
        ([PAYMENTS_MM_RS_DIFFERENT_CURRENCY], False),
    ],
)
def test__verify_against_mm_statement(
    recordsets: list[RecordSet], result: bool | None, import_context: ImportContext
) -> None:
    """Test function dict output on different input combinations."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    assert is_verified_against_mm_statement(PAYMENTS_SAMPLE_DICT) == result


# --------


def test__prune_payload() -> None:
    """Test if dict items are successfully removed from input."""
    VERIFICATIONS_SAMPLE_DICT_EXTRA_ITEMS = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "provider_name": "some_val",
        "amount": 777,
        "currency": "some_val",
    }
    VERIFICATIONS_SAMPLE_DICT_NULL_VALUE = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "provider_name": "some_val",
        "amount": 777,
        "currency": None,
    }
    VERIFICATIONS_SAMPLE_DICT_MISSING_KEY = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "provider_name": "some_val",
        "amount": 777,
    }
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_EXTRA_ITEMS)
        == VERIFICATIONS_SAMPLE_DICT
    )
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_NULL_VALUE) == VERIFICATIONS_SAMPLE_DICT
    )
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_MISSING_KEY)
        == VERIFICATIONS_SAMPLE_DICT
    )


# -------- test task --------


@time_machine.travel(
    datetime.datetime(2024, 6, 18, 20, 19, 42, tzinfo=datetime.UTC), tick=False
)
def test__task(import_context: ImportContext) -> None:
    """Test result end-to-end with db."""
    # Prepare and insert some test data.
    PAYMENTS_RS__FULL = RecordSetWithModifiers(
        PAYMENTS_TABLE, [PAYMENTS_SAMPLE_DICT, PAYMENTS_MM_SAMPLE_DICT]
    )

    prepare_test_data(
        recordsets=[
            PAYMENTS_RS__FULL,
            VERIFICATIONS_RS,
        ],
        import_context=import_context,
    )

    # Run the task.
    InternalTask.from_function(run)
    # Check the results.
    trust_trace_content = table_content("data_trust_trace", shed_id=True)
    assert summarize_records(trust_trace_content) == {
        "fields": [
            "check",
            "created_at",
            "custom",
            "data_origin",
            "import_id",
            "last_import_id",
            "last_source_id",
            "organization_id",
            "result",
            "source_id",
            "subject_origin",
            "subject_uid",
            "uid",
            "updated_at",
        ],
        "hash": "7124ff66ab984cd05e161850f90028cb24e4e17219f9cd69d3c29ca782d611ed",
        "sample_count": 1,
        "value_count": 12,
    }
    assert table_content("data_trust_trace") == snapshot([
        {
            "check": "crm_payment_backed_by_payment_statement",
            "created_at": datetime.datetime(2024, 6, 18, 20, 19, 42),
            "custom": {},
            "data_origin": "solar_coop",
            "import_id": 111111,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 222222,
            "result": "ok",
            "source_id": 333333,
            "subject_origin": "payments_ts",
            "subject_uid": "some_verified_uid",
            "uid": "payments_ts_some_verified_uid_crm_payment_backed_by_payment_statement",  # noqa: E501
            "updated_at": datetime.datetime(2024, 6, 18, 20, 19, 42),
        }
    ])
