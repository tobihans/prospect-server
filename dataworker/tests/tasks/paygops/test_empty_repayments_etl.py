import pytest

from dataworker.queries import count_rows
from dataworker.task import IngestionTask
from tasks.ingestion import payg_ops


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl_empty_repayments() -> None:
    payg_ops_etl = IngestionTask("paygops", payg_ops.transform)
    payg_ops_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/payg_ops_empty_repayments.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    assert count_rows("data_shs") == 1
    assert count_rows("data_payments_ts") == 0
