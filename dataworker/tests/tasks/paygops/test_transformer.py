import pytest

from dataworker.dataset import key_set
from tasks.ingestion import payg_ops
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def payg_ops_data():
    """Load test data from file."""
    return load_test_data("datafile/payg_ops.json")


# --------------------------------------------------
def test__extract_shs_data(payg_ops_data) -> None:
    shs = payg_ops.transform(payg_ops_data)
    # number of object extracted
    assert len(shs["data_shs"]) == 1
    # keys
    assert key_set(shs["data_shs"]) == {
        "account_external_id",
        "time_given_at_start_in_days",
        "customer_name_p",
        "customer_name_e",
        "offer_id",
        "time_to_ownership_in_days",
        "base_price_time_in_days",
        "reference_pricing_amount",
        "is_test",
        "customer_category",
        "manufacturer",
        "device_external_id",
        "customer_gender",
        "payment_plan_currency",
        "payment_plan_days",
        "offer_name",
        "contract_status",
        "purchase_date",
        "customer_latitude_b",
        "customer_latitude_p",
        "customer_latitude_e",
        "customer_birth_year",
        "customer_phone_p",
        "customer_phone_e",
        "repossession_date",
        "reference_pricing_duration",
        "registration_date",
        "payment_plan_down_payment",
        "customer_country",
        "customer_longitude_b",
        "customer_longitude_p",
        "customer_longitude_e",
        "payment_plan_type",
        "paid_off_date",
        "serial_number",
        "customer_external_id",
        "pv_power_w",
        "payment_plan_amount_financed",
        "tags",
        "customer_id_number",
        "lead_generator_id",
        "lead_generator_type",
        "model",
        "seller_name_p",
        "seller_name_e",
        "seller_phone_p",
        "seller_phone_e",
        "seller_gender",
    }
    # test the first records for exact values
    assert shs["data_shs"][0] == {
        "device_external_id": "OPT-AAM00000729",
        "account_external_id": "C220012",
        "serial_number": "729",
        "purchase_date": "2022-11-09T14:14:03.785827Z",
        "manufacturer": "A2EI",
        "pv_power_w": None,
        "customer_category": "household",
        "payment_plan_days": 762.0,
        "payment_plan_down_payment": 130000.0,
        "payment_plan_amount_financed": 320233.33,
        "payment_plan_type": "Loan",
        "payment_plan_currency": "NGN",
        "paid_off_date": None,
        "repossession_date": None,
        "is_test": False,
        "customer_external_id": 8,
        "customer_name_p": "e423d1b2f89cd38b5e51d296c4909a758eeb4d588d69a1256c5a827a3d7d46fc",  # noqa: E501
        "customer_name_e": None,
        "customer_gender": "Male",
        "customer_birth_year": 1970,
        "customer_country": "Nigeria",
        "customer_latitude_b": 7.466,
        "customer_longitude_b": 9.069,
        "customer_latitude_p": "2e3fb3caef04a50d788070101d307dd407be8cab53638aa3276cc2180c8f957d",  # noqa: E501
        "customer_longitude_p": "0cb487ab08aec2fe9a16c71362a949695ac6ee475af14782eedb3063ac0b4bc9",  # noqa: E501
        "customer_latitude_e": None,
        "customer_longitude_e": None,
        "customer_phone_p": "30bb707fe6d9803aa0180f1d8b27686d981fb1e800b3f80951c70e9bb683f430",  # noqa: E501
        "customer_phone_e": None,
        "offer_id": 7,
        "offer_name": "Offer Emmanuel Benjamin 729",
        "contract_status": "Enabled",
        "registration_date": "2022-11-09T14:14:03.701614Z",
        "reference_pricing_duration": "30.00",
        "reference_pricing_amount": "13000.00",
        "time_given_at_start_in_days": 23,
        "time_to_ownership_in_days": 762,
        "base_price_time_in_days": 30,
        "tags": ["rollout"],
        "customer_id_number": "A12345678",
        "lead_generator_id": 1,
        "lead_generator_type": "Mentor",
        "model": "OPT",
        "seller_name_p": "2b685620e3380ac7902b48162af5899f9a6209579375a0452ecd410dc3f19f5b",  # noqa: E501
        "seller_name_e": None,
        "seller_phone_p": "0203d59b353f5fdd0bee890240668b6309b939210ab3d68ace9dadd504512074",  # noqa: E501
        "seller_phone_e": None,
        "seller_gender": "Female",
    }


def test__extract_payments_ts_data(payg_ops_data) -> None:
    payments_ts = payg_ops.transform(payg_ops_data)
    # number of object extracted
    assert len(payments_ts["data_payments_ts"]) == 1
    # keys
    assert key_set(payments_ts["data_payments_ts"]) == {
        "account_origin",
        "provider_name",
        "amount",
        "currency",
        "paid_at",
        "discounted_amount",
        "credit_value",
        "type",
        "purpose",
        "payment_external_id",
        "account_external_id",
    }
    # test the first records for exact values
    assert payments_ts["data_payments_ts"][0] == {
        "payment_external_id": 70,
        "purpose": "paygo payment",
        "paid_at": "2022-11-09T14:28:04.109728Z",
        "amount": "0.0000",
        "currency": "NGN",
        "provider_name": "PaygOps",
        "account_external_id": "C220012",
        "account_origin": "shs",
        "discounted_amount": None,
        "type": "Manual Discount",
        "credit_value": "0.54",
    }
