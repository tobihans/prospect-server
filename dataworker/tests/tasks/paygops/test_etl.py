# ruff: noqa: E501
import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import count_rows, table_content
from dataworker.task import IngestionTask
from tasks.ingestion import payg_ops


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    payg_ops_etl = IngestionTask("paygops", payg_ops.transform)
    payg_ops_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/payg_ops.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    assert count_rows("data_shs") == 1
    assert count_rows("data_payments_ts") == 1
    assert table_content(
        "data_shs", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "C220012",
            "account_uid": "1234_over_there_C220012",
            "battery_energy_wh": None,
            "custom": {
                "offer_id": 7,
                "offer_name": "Offer Emmanuel Benjamin 729",
                "contract_status": "Enabled",
                "lead_generator_id": 1,
                "registration_date": "2022-11-09T14:14:03.701614Z",
                "customer_id_number": "A12345678",
                "lead_generator_type": "Mentor",
                "base_price_time_in_days": 30,
                "reference_pricing_amount": "13000.00",
                "time_to_ownership_in_days": 762,
                "reference_pricing_duration": "30.00",
                "time_given_at_start_in_days": 23,
            },
            "customer_address_e": None,
            "customer_address_p": None,
            "customer_birth_year": 1970,
            "customer_category": "household",
            "customer_country": "NG",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "8",
            "customer_former_electricity_source": None,
            "customer_gender": "M",
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": 7.466,
            "customer_latitude_e": None,
            "customer_latitude_p": "2e3fb3caef04a50d788070101d307dd407be8cab53638aa3276cc2180c8f957d",
            "customer_location_area_1": None,
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": 9.069,
            "customer_longitude_e": None,
            "customer_longitude_p": "0cb487ab08aec2fe9a16c71362a949695ac6ee475af14782eedb3063ac0b4bc9",
            "customer_name_e": None,
            "customer_name_p": "e423d1b2f89cd38b5e51d296c4909a758eeb4d588d69a1256c5a827a3d7d46fc",
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "30bb707fe6d9803aa0180f1d8b27686d981fb1e800b3f80951c70e9bb683f430",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_8",
            "data_origin": "over_there",
            "device_external_id": "OPT-AAM00000729",
            "device_uid": "1234_A2EI_729",
            "import_id": 123456789,
            "is_test": False,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "A2EI",
            "model": "OPT",
            "organization_id": 1234,
            "paid_off_date": None,
            "payment_plan_amount_financed": 320233.33,
            "payment_plan_currency": "NGN",
            "payment_plan_days": 762.0,
            "payment_plan_down_payment": 130000.0,
            "payment_plan_type": "Loan",
            "primary_use": None,
            "purchase_date": datetime.date(2022, 11, 9),
            "pv_power_w": None,
            "rated_power_w": None,
            "repossession_date": None,
            "seller_address_e": None,
            "seller_address_p": None,
            "seller_country": None,
            "seller_email_e": None,
            "seller_email_p": None,
            "seller_external_id": None,
            "seller_gender": "F",
            "seller_latitude_b": None,
            "seller_latitude_e": None,
            "seller_latitude_p": None,
            "seller_location_area_1": None,
            "seller_location_area_2": None,
            "seller_location_area_3": None,
            "seller_location_area_4": None,
            "seller_longitude_b": None,
            "seller_longitude_e": None,
            "seller_longitude_p": None,
            "seller_name_e": None,
            "seller_name_p": "2b685620e3380ac7902b48162af5899f9a6209579375a0452ecd410dc3f19f5b",
            "seller_phone_e": None,
            "seller_phone_p": "0203d59b353f5fdd0bee890240668b6309b939210ab3d68ace9dadd504512074",
            "serial_number": "729",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_8_1234_A2EI_729",
            "voltage_v": None,
        }
    ])
    assert table_content(
        "data_payments_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "C220012",
            "account_origin": "shs",
            "account_uid": "1234_over_there_C220012",
            "amount": 0.0,
            "currency": "NGN",
            "custom": {"type": "Manual Discount", "credit_value": "0.54"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2022, 11, 9, 14, 28, 4, 109728),
            "payment_external_id": "70",
            "provider_name": "PaygOps",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "paygo payment",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "shs_1234_over_there_C220012_70_2022-11-09T14:28:04.109728+00:00",
        }
    ])


@pytest.mark.parametrize(
    ("serial_number", "cleaned_serial_number"),
    [
        ("OPT-AAM00000729", "729"),
        ("00000729", "729"),
        ("OPT-AAM10000729", "10000729"),
        ("OPTAAM00000729", "OPTAAM00000729"),
        ("00OPT-AAM00000729", "OPT-AAM00000729"),
    ],
)
def test__clean_a2ei_serial_number(
    serial_number: str, cleaned_serial_number: str
) -> str:
    assert cleaned_serial_number == payg_ops.clean_a2ei_serial_number(serial_number)
