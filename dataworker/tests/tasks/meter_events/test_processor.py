from copy import copy

import pandas as pd
import pytest

from dataworker.dataset import key_set
from tasks.internal.meter_events import processing
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def datawarehouse_meters_ts_data():
    """Load test data from file."""
    return load_test_data("records_for_task_test/meters_ts_sample_1913.json")


# --------------------------------------------------


def test__extract_events(
    datawarehouse_meters_ts_data,
    PARAMETERS=None,
) -> None:
    if PARAMETERS is None:
        PARAMETERS = {
            "max_energy_step_wh": 300,
            "cutoff_time_sec": 360,
            "min_event_energy_wh": 10,
            "min_event_duration_sec": 60,
            "category": "productive_use",
            "min_power_w": 100,
            "max_power_w": 5000,
        }
    events = processing.extract_events(datawarehouse_meters_ts_data, **PARAMETERS)
    # number of object extracted
    assert len(events) == 9
    # keys
    assert key_set(events) == {
        "device_uid",
        "serial_number",
        "start_at",
        "energy_start_wh",
        "end_at",
        "energy_end_wh",
        "energy_consumed_wh",
        "duration_sec",
        "category",
    }

    # test the first records for exact values
    assert events[0] == {
        "device_uid": "1_HOP_202086000667",
        "serial_number": "202086000667",
        "start_at": "2023-04-17T14:50:00Z",
        "energy_start_wh": 23360.0,
        "end_at": "2023-04-17T14:53:00Z",
        "energy_end_wh": 23400.0,
        "energy_consumed_wh": 40.0,
        "duration_sec": 180.0,
        "category": "productive_use",
    }


def test__fill_missing_values() -> None:
    in_data = [
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689849000000,
            "energy_lifetime_wh": None,
            "max_time": 1689950400000,
        },
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689852000000,
            "energy_lifetime_wh": 1188020.0,
            "max_time": 1689950400000,
        },
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689852600000,
            "energy_lifetime_wh": None,
            "max_time": 1689950400000,
        },
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689853800000,
            "energy_lifetime_wh": 1188210.0,
            "max_time": 1689950400000,
        },
    ]
    in_df = pd.DataFrame.from_dict(in_data)
    out_df = processing.fill_missing_values(in_df, 1)

    # A. initial None energy values are disregarded
    # B. subsequent None values are successfully filled
    expected_df = pd.DataFrame.from_dict([
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689852000000,
            "energy_lifetime_wh": 1188020.0,
            "max_time": 1689950400000,
        },
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689852600000,
            "energy_lifetime_wh": 1188020.0,
            "max_time": 1689950400000,
        },
        {
            "device_uid": "1_HOP_541826",
            "serial_number": "541826",
            "metered_at": 1689853800000,
            "energy_lifetime_wh": 1188210.0,
            "max_time": 1689950400000,
        },
    ])
    pd.testing.assert_frame_equal(out_df, expected_df)


# --------------------------------------------------


@pytest.mark.parametrize("primary_use", processing.PARAMETERS.keys())
def test_process_with_parameters(datawarehouse_meters_ts_data, primary_use, caplog):
    """Ensure that all current PARAMETERS from processing are correctly defined."""
    PARAMETERS = processing.PARAMETERS
    # Call the process function
    result = processing.extract_events(
        datawarehouse_meters_ts_data, **PARAMETERS[primary_use]
    )
    # Assert that a result is produced (a list of events) without error.
    assert isinstance(result, list)

    # Make sure it breaks if something is incorrectly defined or missing.
    # Call the process function and assert TypeError.
    params_copy = copy(PARAMETERS[primary_use])
    del params_copy["max_power_w"]
    with pytest.raises(TypeError):
        processing.extract_events(datawarehouse_meters_ts_data, **params_copy)
