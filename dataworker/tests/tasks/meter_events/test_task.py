import datetime

import funcy
import pytest
import time_machine
from inline_snapshot import snapshot

from dataworker.database import get_or_create_connection
from dataworker.metadata import creation_timestamps
from dataworker.queries import count_rows, fetch_one_row, table_content
from dataworker.task import InternalTask
from tasks.internal.meter_events import task
from tests.utils import count_queries, insert_records, load_test_data


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


@time_machine.travel(datetime.datetime(2023, 8, 1, tzinfo=datetime.UTC), tick=False)
def test__task() -> None:
    # insert some test data into data_meters
    data_meters = load_test_data("records_for_task_test/meters_sample.json")
    insert_records("data_meters", data_meters)

    # insert some test data into data_meters_ts
    data_meters_ts = load_test_data(
        "records_for_task_test/meters_ts_sample_for_meter_events.json"
    )
    insert_records("data_meters_ts", data_meters_ts)

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 6 + n
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    # (1)  data_meters_events_ts (get_last_event_end)
    # (1)  data_meters_ts (get_meter_ts_data_since)
    # (3)  sa_savepoint_1 (savepoint) +
    #      data_meter_events_ts (insert) +
    #      sa_savepoint_1 (release savepoint)
    assert len(queries) == 9

    # validate length of results.
    assert count_rows("data_meter_events_ts") == 1

    # validate one sample from results.
    one_row = fetch_one_row(
        "data_meter_events_ts", ("start_at", datetime.datetime(2023, 7, 21, 10, 46))
    )

    assert funcy.omit(
        one_row,
        (
            "start_at",
            "created_at",
            "updated_at",
        ),
    ) == {
        "category": "productive_use",
        "custom": {},
        "data_origin": "stellar",
        "device_uid": "1_HOP_541826",
        "duration_sec": 240,
        "end_at": datetime.datetime(2023, 7, 21, 10, 50),
        "energy_consumed_wh": 20.0,
        "energy_end_wh": 1190370.0,
        "energy_start_wh": 1190350.0,
        "manufacturer": "",
        "organization_id": 1,
        "serial_number": "541826",
        "source_id": 1,
    }

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 6
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    # (1)  data_meters_events_ts (get_last_event_end)
    # (1)  data_meters_ts (get_meter_ts_data_since) -> will return empty
    assert len(queries) == 6

    assert table_content("data_meter_events_ts") == snapshot([
        {
            "category": "productive_use",
            "created_at": datetime.datetime(2023, 8, 1, 0, 0),
            "custom": {},
            "data_origin": "stellar",
            "device_uid": "1_HOP_541826",
            "duration_sec": 240,
            "end_at": datetime.datetime(2023, 7, 21, 10, 50),
            "energy_consumed_wh": 20.0,
            "energy_end_wh": 1190370.0,
            "energy_start_wh": 1190350.0,
            "manufacturer": "",
            "organization_id": 1,
            "serial_number": "541826",
            "source_id": 1,
            "start_at": datetime.datetime(2023, 7, 21, 10, 46),
            "updated_at": datetime.datetime(2023, 8, 1, 0, 0),
        }
    ])


@time_machine.travel(datetime.datetime(2024, 8, 1, tzinfo=datetime.UTC), tick=False)
def test__task__no_recent_activity() -> None:
    # insert some test data into data_meters
    data_meters = load_test_data("records_for_task_test/meters_sample.json")
    insert_records("data_meters", data_meters)

    # insert some test data into data_meters_ts
    data_meters_ts = load_test_data(
        "records_for_task_test/meters_ts_sample_for_meter_events.json"
    )
    insert_records("data_meters_ts", data_meters_ts)

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 1 + n
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    assert len(queries) == 4
    # make sure last query is expected
    assert "metered_at >=" in queries[-1]

    # validate length of results is 0.
    assert count_rows("data_meter_events_ts") == 0


@time_machine.travel(datetime.datetime(2023, 8, 1, tzinfo=datetime.UTC), tick=False)
def test__task_run_does_nothing_on_one_single_row() -> None:
    # insert some test data into data_meters
    data_meters = load_test_data("records_for_task_test/meters_sample.json")
    insert_records("data_meters", data_meters)

    # insert some test data made up of only one row into data_meters_ts.
    # should trigger no processing as one row is not sufficient to calculate new events.
    data_meters_ts = load_test_data("records_for_task_test/meters_ts_bug_270624.json")
    insert_records("data_meters_ts", data_meters_ts)

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 3 + n
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    # (1)  data_meters_events_ts (get_last_event_end)
    # (1)  data_meters_ts (get_meter_ts_data_since)
    assert len(queries) == 6
    # make sure last query is expected
    assert "serial_number" in queries[-1]

    # validate length of results is 0.
    assert count_rows("data_meter_events_ts") == 0


@time_machine.travel(datetime.datetime(2023, 8, 1, tzinfo=datetime.UTC), tick=False)
def test__task_run_does_nothing_bad_data(caplog) -> None:
    # insert some test data into data_meters
    data_meters = load_test_data("records_for_task_test/meters_sample.json")
    insert_records("data_meters", data_meters)

    # insert some test data made up of empty lifetime energy values into data_meters_ts.
    # should trigger no processing as this will get thrown out.
    data_meters_ts = load_test_data(
        "records_for_task_test/meters_ts_sample_for_meter_events_no_energy.json"
    )
    insert_records("data_meters_ts", data_meters_ts)

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 3 + n
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    # (1)  data_meters_events_ts (get_last_event_end)
    # (1)  data_meters_ts (get_meter_ts_data_since)
    assert len(queries) == 6
    # make sure last query is expected
    assert "serial_number" in queries[-1]

    # make sure the data is indeed empty before being sent to the DB
    assert (
        "Task function returned Dataset('data_meter_events_ts' <0 records>)"
    ) in caplog.text

    # validate length of results is 0.
    assert count_rows("data_meter_events_ts") == 0


@time_machine.travel(datetime.datetime(2023, 8, 1, tzinfo=datetime.UTC), tick=False)
def test__task_run_succeeds_despite_some_missing_energy_values() -> None:
    # insert some test data into data_meters
    data_meters = load_test_data("records_for_task_test/meters_sample.json")
    insert_records("data_meters", data_meters)

    # insert some test data with some missing energy values.
    data_meters_ts = load_test_data(
        "records_for_task_test/meters_ts_sample_for_meter_events_partial_missing_energy.json"
    )
    insert_records("data_meters_ts", data_meters_ts)

    with count_queries(get_or_create_connection()) as queries:
        # run meter events processor with test data.
        InternalTask.from_function(task.run)()

    # QUERIES: 6 + n
    # (1)  data_meters (get_applianced_meters)
    # (n)* data_meters_ts (meter_is_active) *n=3
    # (1)  data_meters_events_ts (get_last_event_end)
    # (1)  data_meters_ts (get_meter_ts_data_since)
    # (3)  sa_savepoint_1 (savepoint) +
    #      data_meter_events_ts (insert) +
    #      sa_savepoint_1 (release savepoint)
    assert len(queries) == 9

    # validate length of results: event still created despite missing values?
    assert count_rows("data_meter_events_ts") == 1

    # validate one sample from results.
    one_row = fetch_one_row(
        "data_meter_events_ts", ("start_at", datetime.datetime(2023, 7, 21, 10, 46))
    )
    assert funcy.omit(
        one_row,
        (
            "start_at",
            "created_at",
            "updated_at",
        ),
    ) == {
        "category": "productive_use",
        "custom": {},
        "data_origin": "stellar",
        "device_uid": "1_HOP_541826",
        "duration_sec": 240,
        "end_at": datetime.datetime(2023, 7, 21, 10, 50),
        "energy_consumed_wh": 20.0,
        "energy_end_wh": 1190370.0,
        "energy_start_wh": 1190350.0,
        "manufacturer": "",
        "organization_id": 1,
        "serial_number": "541826",
        "source_id": 1,
    }


def test__task_get_last_event_end():
    device_uid = "1_HOP_541826"
    dt_1 = datetime.datetime(2023, 7, 21, 10, 50)
    dt_2 = datetime.datetime(2023, 7, 21, 11, 51)
    dt_3 = datetime.datetime(2023, 7, 21, 12, 52)
    dt_4 = datetime.datetime(2023, 7, 21, 13, 53)
    dt_5 = datetime.datetime(2023, 7, 21, 14, 54)
    event_dict = {
        "start_at": dt_1,
        "device_uid": device_uid,
        "end_at": dt_2,
        "category": "productive_use",
        "data_origin": "stellar",
        "organization_id": 1,
        "source_id": 1,
    } | creation_timestamps()
    event_records = [
        event_dict,
        event_dict | {"start_at": dt_2, "end_at": dt_5},
        event_dict | {"start_at": dt_3, "end_at": dt_4},
    ]
    insert_records(task.EVENTS_TABLE, event_records)
    assert task.get_last_event_end(device_uid) == dt_5
