# ruff: noqa: E501
import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import count_rows, table_content
from dataworker.task import IngestionTask
from tasks.ingestion import spark_meter


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    spark_meter_etl = IngestionTask("sparkmeter", spark_meter.transform)
    spark_meter_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/spark_meter.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    # 2 samples will raise NotNullViolation for customer_external_id
    # one other data point has 2 times the same phone_number
    assert count_rows("data_meters") == 5
    assert count_rows("data_meters_ts") == 7
    assert count_rows("data_payments_ts") == 5

    assert table_content(
        "data_meters", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc",
            "account_uid": "1234_over_there_d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc",
            "custom": {
                "city": "Mwinilunga",
                "active": True,
                "street1_p": "215473844fac38515285a565dc78d6e00fe072392cc528e9b28d60a832362005",
                "bootloader": "0100020000",
            },
            "customer_address_e": None,
            "customer_address_p": "8b2cc2b17ccfe23f50fb8218594a8eeda464d2185bdf174bf26b5749aee837b3",
            "customer_birth_year": None,
            "customer_category": None,
            "customer_country": "ZM",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "118f5688d5b3c1bf5992a8b848cbfe7382f3b9a2f729ee0a71ba98aae9a87039",
            "customer_former_electricity_source": None,
            "customer_gender": None,
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "North Western Province",
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": None,
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "118f5688d5b3c1bf5992a8b848cbfe7382f3b9a2f729ee0a71ba98aae9a87039",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_118f5688d5b3c1bf5992a8b848cbfe7382f3b9a2f729ee0a71ba98aae9a87039",
            "data_origin": "over_there",
            "device_external_id": "SM60R-07-000135E4",
            "device_uid": "1234_sparkmeter_SM60R-07-000135E4",
            "firmware_version": "0100050000",
            "grid_name": None,
            "grid_uid": None,
            "import_id": 123456789,
            "installation_date": None,
            "is_locked": True,
            "is_test": None,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "sparkmeter",
            "max_power_w": None,
            "model": "SM60R",
            "organization_id": 1234,
            "payment_plan": "Medium Consumption",
            "payment_plan_amount_financed": None,
            "payment_plan_currency": None,
            "payment_plan_days": None,
            "payment_plan_down_payment": None,
            "primary_use": None,
            "purchase_date": None,
            "repossession_date": None,
            "serial_number": "SM60R-07-000135E4",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_118f5688d5b3c1bf5992a8b848cbfe7382f3b9a2f729ee0a71ba98aae9a87039_1234_sparkmeter_SM60R-07-000135E4",
        },
        {
            "account_external_id": "67a4aec3-0b06-4957-be16-12206d0f1c59",
            "account_uid": "1234_over_there_67a4aec3-0b06-4957-be16-12206d0f1c59",
            "custom": {
                "city": "MWINILUNGA",
                "active": True,
                "street1_p": "7b983a30474011d2d02e6601510850242e4dcb8223a142565d8a0431d805041c",
                "bootloader": "0100020000",
            },
            "customer_address_e": None,
            "customer_address_p": "1efa458e6b5e4157c0559e7b88bbe683798d757857ad4cce4bb239234aab80ec",
            "customer_birth_year": None,
            "customer_category": None,
            "customer_country": "ZM",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "16dd4733a276adddc966fb5c29bfed09f05cd1013becddc11238e128316c42b4",
            "customer_former_electricity_source": None,
            "customer_gender": None,
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "NORTHWESTERN",
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": None,
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "16dd4733a276adddc966fb5c29bfed09f05cd1013becddc11238e128316c42b4",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_16dd4733a276adddc966fb5c29bfed09f05cd1013becddc11238e128316c42b4",
            "data_origin": "over_there",
            "device_external_id": "SM60R-07-0001463B",
            "device_uid": "1234_sparkmeter_SM60R-07-0001463B",
            "firmware_version": "0100050000",
            "grid_name": None,
            "grid_uid": None,
            "import_id": 123456789,
            "installation_date": None,
            "is_locked": True,
            "is_test": None,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "sparkmeter",
            "max_power_w": None,
            "model": "SM60R",
            "organization_id": 1234,
            "payment_plan": "Medium Consumption",
            "payment_plan_amount_financed": None,
            "payment_plan_currency": None,
            "payment_plan_days": None,
            "payment_plan_down_payment": None,
            "primary_use": None,
            "purchase_date": None,
            "repossession_date": None,
            "serial_number": "SM60R-07-0001463B",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_16dd4733a276adddc966fb5c29bfed09f05cd1013becddc11238e128316c42b4_1234_sparkmeter_SM60R-07-0001463B",
        },
        {
            "account_external_id": "dae0f75f-2580-4383-bf18-9705a954d25d",
            "account_uid": "1234_over_there_dae0f75f-2580-4383-bf18-9705a954d25d",
            "custom": {
                "city": "Mwinilunga",
                "active": True,
                "street1_p": "f96b9c46f76a87ae9c4b3c3301496e48f1d67c8abbdcb1cd8bee9348a0b774e1",
                "bootloader": "0100020000",
            },
            "customer_address_e": None,
            "customer_address_p": "7ee79f3af9c4c9dca1f428deb1212fbde882ba323caf10836a1d0738eb14e3e4",
            "customer_birth_year": None,
            "customer_category": None,
            "customer_country": "ZM",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "59cdb2f77d0827123d91cd98ebe0b345fa56f037f8ce877b1c4a047608eee99e",
            "customer_former_electricity_source": None,
            "customer_gender": None,
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "North Western Province",
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": None,
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "59cdb2f77d0827123d91cd98ebe0b345fa56f037f8ce877b1c4a047608eee99e",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_59cdb2f77d0827123d91cd98ebe0b345fa56f037f8ce877b1c4a047608eee99e",
            "data_origin": "over_there",
            "device_external_id": "SM60R-07-000144FE",
            "device_uid": "1234_sparkmeter_SM60R-07-000144FE",
            "firmware_version": "0100050000",
            "grid_name": None,
            "grid_uid": None,
            "import_id": 123456789,
            "installation_date": None,
            "is_locked": True,
            "is_test": None,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "sparkmeter",
            "max_power_w": None,
            "model": "SM60R",
            "organization_id": 1234,
            "payment_plan": "Medium Consumption",
            "payment_plan_amount_financed": None,
            "payment_plan_currency": None,
            "payment_plan_days": None,
            "payment_plan_down_payment": None,
            "primary_use": None,
            "purchase_date": None,
            "repossession_date": None,
            "serial_number": "SM60R-07-000144FE",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_59cdb2f77d0827123d91cd98ebe0b345fa56f037f8ce877b1c4a047608eee99e_1234_sparkmeter_SM60R-07-000144FE",
        },
        {
            "account_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
            "account_uid": "1234_over_there_0efb960c-5b95-45fc-a2c5-9c474720f789",
            "custom": {
                "city": "Mwinilunga",
                "active": True,
                "street1_p": "790262e30d8cb3ab99c05ebdff4b20af2511d9c9c49f6149fc2acc1e46d0b06a",
                "bootloader": "0100020000",
            },
            "customer_address_e": None,
            "customer_address_p": "5b226d014117d365cb4f745785bc8a2928f8c9fa185ae5ac6e65a03fcc00687a",
            "customer_birth_year": None,
            "customer_category": None,
            "customer_country": "ZM",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "customer_former_electricity_source": None,
            "customer_gender": None,
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "North Western Province",
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": None,
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "data_origin": "over_there",
            "device_external_id": "SM60R-07-00018557",
            "device_uid": "1234_sparkmeter_SM60R-07-00018557",
            "firmware_version": "0100050000",
            "grid_name": None,
            "grid_uid": None,
            "import_id": 123456789,
            "installation_date": None,
            "is_locked": True,
            "is_test": None,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "sparkmeter",
            "max_power_w": None,
            "model": "SM60R",
            "organization_id": 1234,
            "payment_plan": "High Consumption - 2nd Bouquet",
            "payment_plan_amount_financed": None,
            "payment_plan_currency": None,
            "payment_plan_days": None,
            "payment_plan_down_payment": None,
            "primary_use": None,
            "purchase_date": None,
            "repossession_date": None,
            "serial_number": "SM60R-07-00018557",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47_1234_sparkmeter_SM60R-07-00018557",
        },
        {
            "account_external_id": "96a8a136-afc8-46cf-ae18-6a3d9e561a03",
            "account_uid": "1234_over_there_96a8a136-afc8-46cf-ae18-6a3d9e561a03",
            "custom": {
                "city": "MWINILUNGA",
                "active": True,
                "street1_p": "66b71c7aab98db044ed1941304234aab65bfd22d971941c6edb5febb9ab558b0",
                "bootloader": "0100020000",
            },
            "customer_address_e": None,
            "customer_address_p": "214664cec62e56e9753c2f4a49f059e93ec78ef2e6ff92eb67cf9c1c264b8aee",
            "customer_birth_year": None,
            "customer_category": None,
            "customer_country": "ZM",
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "customer_former_electricity_source": None,
            "customer_gender": None,
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "NORTHWESTERN",
            "customer_location_area_2": None,
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": None,
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "customer_profession": None,
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",
            "data_origin": "over_there",
            "device_external_id": "SM60R-07-000186E5",
            "device_uid": "1234_sparkmeter_SM60R-07-000186E5",
            "firmware_version": "0100050000",
            "grid_name": None,
            "grid_uid": None,
            "import_id": 123456789,
            "installation_date": None,
            "is_locked": True,
            "is_test": None,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "sparkmeter",
            "max_power_w": None,
            "model": "SM60R",
            "organization_id": 1234,
            "payment_plan": "High Consumption - 1st Bouquet",
            "payment_plan_amount_financed": None,
            "payment_plan_currency": None,
            "payment_plan_days": None,
            "payment_plan_down_payment": None,
            "primary_use": None,
            "purchase_date": None,
            "repossession_date": None,
            "serial_number": "SM60R-07-000186E5",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47_1234_sparkmeter_SM60R-07-000186E5",
        },
    ])
    assert table_content(
        "data_meters_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "billing_cycle_start_at": None,
            "current_a": None,
            "custom": {
                "operating_mode": 0,
                "last_config_datetime": "2022-04-08T05:48:18.787929",
                "last_meter_state_code": 0,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM200E-04-0003F98A",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 41852.316875,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM200E-04-0003F98A",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": None,
            "current_a": None,
            "custom": {
                "operating_mode": 2,
                "last_config_datetime": "2022-04-08T06:17:35.523704",
                "last_meter_state_code": 0,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM200E-04-0003FB8A",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 31025.27178125,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM200E-04-0003FB8A",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": datetime.datetime(2023, 1, 1, 8, 0),
            "current_a": None,
            "custom": {
                "operating_mode": 1,
                "total_cycle_energy": 23.3001562499999,
                "last_config_datetime": "2022-12-18T07:58:01.779579",
                "last_meter_state_code": 1,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM60R-07-000135E4",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 1085.64678125,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM60R-07-000135E4",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": datetime.datetime(2023, 1, 1, 8, 0),
            "current_a": None,
            "custom": {
                "operating_mode": 1,
                "total_cycle_energy": 30.7969687499996,
                "last_config_datetime": "2022-12-27T06:21:52.600039",
                "last_meter_state_code": 1,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM60R-07-000144FE",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 1700.90646875,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM60R-07-000144FE",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": datetime.datetime(2023, 1, 1, 17, 45),
            "current_a": None,
            "custom": {
                "operating_mode": 1,
                "total_cycle_energy": 49.5970000000001,
                "last_config_datetime": "2022-12-16T17:43:40.896992",
                "last_meter_state_code": 1,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM60R-07-0001463B",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 346.066125,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM60R-07-0001463B",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": datetime.datetime(2022, 12, 1, 20, 45),
            "current_a": None,
            "custom": {
                "operating_mode": 0,
                "total_cycle_energy": 138.790687500003,
                "last_config_datetime": "2022-12-04T18:05:18.842360",
                "last_meter_state_code": 1,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM60R-07-00018557",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 10384.89446875,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2022, 12, 5, 11, 15),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM60R-07-00018557",
            "source_id": 123456789,
            "voltage_v": None,
        },
        {
            "billing_cycle_start_at": datetime.datetime(2023, 1, 1, 17, 15),
            "current_a": None,
            "custom": {
                "operating_mode": 2,
                "total_cycle_energy": 104.52140625,
                "last_config_datetime": "2022-12-28T10:24:31.080556",
                "last_meter_state_code": 1,
            },
            "data_origin": "over_there",
            "device_uid": "1234_sparkmeter_SM60R-07-000186E5",
            "energy_interval_wh": None,
            "energy_lifetime_wh": 753.43584375,
            "frequency_hz": None,
            "import_id": 123456789,
            "interval_seconds": None,
            "manufacturer": "sparkmeter",
            "metered_at": datetime.datetime(2023, 1, 6, 4, 30),
            "organization_id": 1234,
            "phase": "1",
            "power_factor": None,
            "power_w": None,
            "serial_number": "SM60R-07-000186E5",
            "source_id": 123456789,
            "voltage_v": None,
        },
    ])

    assert table_content(
        "data_payments_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
            "account_origin": "meters",
            "account_uid": "1234_over_there_0efb960c-5b95-45fc-a2c5-9c474720f789",
            "amount": 23.5219603261024,
            "currency": "NULL",
            "custom": {"last_plan_expiration_date": "2022-12-05T20:45:00"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2022, 12, 4, 20, 45),
            "payment_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
            "provider_name": None,
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": None,
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_0efb960c-5b95-45fc-a2c5-9c474720f789_0efb960c-5b95-45fc-a2c5-9c474720f789_2022-12-04T20:45:00+00:00",
        },
        {
            "account_external_id": "67a4aec3-0b06-4957-be16-12206d0f1c59",
            "account_origin": "meters",
            "account_uid": "1234_over_there_67a4aec3-0b06-4957-be16-12206d0f1c59",
            "amount": 8.19552142857104,
            "currency": "NULL",
            "custom": {"last_plan_expiration_date": "2023-01-06T17:45:00"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 1, 5, 17, 45),
            "payment_external_id": "67a4aec3-0b06-4957-be16-12206d0f1c59",
            "provider_name": None,
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": None,
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_67a4aec3-0b06-4957-be16-12206d0f1c59_67a4aec3-0b06-4957-be16-12206d0f1c59_2023-01-05T17:45:00+00:00",
        },
        {
            "account_external_id": "96a8a136-afc8-46cf-ae18-6a3d9e561a03",
            "account_origin": "meters",
            "account_uid": "1234_over_there_96a8a136-afc8-46cf-ae18-6a3d9e561a03",
            "amount": 15.1846734375,
            "currency": "NULL",
            "custom": {"last_plan_expiration_date": "2023-01-06T17:15:00"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 1, 5, 17, 15),
            "payment_external_id": "96a8a136-afc8-46cf-ae18-6a3d9e561a03",
            "provider_name": None,
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": None,
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_96a8a136-afc8-46cf-ae18-6a3d9e561a03_96a8a136-afc8-46cf-ae18-6a3d9e561a03_2023-01-05T17:15:00+00:00",
        },
        {
            "account_external_id": "d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc",
            "account_origin": "meters",
            "account_uid": "1234_over_there_d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc",
            "amount": 7.66368437499886,
            "currency": "NULL",
            "custom": {"last_plan_expiration_date": "2023-01-06T08:00:00"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 1, 5, 8, 0),
            "payment_external_id": "d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc",
            "provider_name": None,
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": None,
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc_d2e4f47a-cfed-4a79-8ebd-9658b0a1a8dc_2023-01-05T08:00:00+00:00",
        },
        {
            "account_external_id": "dae0f75f-2580-4383-bf18-9705a954d25d",
            "account_origin": "meters",
            "account_uid": "1234_over_there_dae0f75f-2580-4383-bf18-9705a954d25d",
            "amount": 7.34663526785562,
            "currency": "NULL",
            "custom": {"last_plan_expiration_date": "2023-01-06T08:00:00"},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 1, 5, 8, 0),
            "payment_external_id": "dae0f75f-2580-4383-bf18-9705a954d25d",
            "provider_name": None,
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": None,
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_dae0f75f-2580-4383-bf18-9705a954d25d_dae0f75f-2580-4383-bf18-9705a954d25d_2023-01-05T08:00:00+00:00",
        },
    ])
