import pytest

from dataworker.dataset import key_set
from tasks.ingestion import spark_meter
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def spark_meter_data():
    """Load test data from file."""
    return load_test_data("datafile/spark_meter.json")


# --------------------------------------------------


def test__extract_payments_ts_data(spark_meter_data) -> None:
    payment_ts = spark_meter.extract_payments_ts_data(spark_meter_data)
    # number of object extracted
    assert len(payment_ts) == 5
    # keys
    assert key_set(payment_ts) == {
        "amount",
        "account_external_id",
        "last_plan_expiration_date",
        "paid_at",
        "currency",
        "payment_external_id",
        "account_origin",
    }
    # test the first records for exact values
    assert payment_ts[0] == {
        "amount": 23.5219603261024,
        "paid_at": "2022-12-04T20:45:00",
        "payment_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
        "currency": "NULL",
        "last_plan_expiration_date": "2022-12-05T20:45:00",
        "account_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
        "account_origin": "meters",
    }


def test__extract_meters_ts_data(spark_meter_data) -> None:
    meters_ts = spark_meter.extract_meters_ts_data(spark_meter_data)
    # number of object extracted
    assert len(meters_ts) == 7
    # keys
    assert key_set(meters_ts) == {
        "metered_at",
        "billing_cycle_start_at",
        "last_meter_state_code",
        "last_config_datetime",
        "operating_mode",
        "current_daily_energy",
        "energy_lifetime_wh",
        "serial_number",
        "manufacturer",
        "total_cycle_energy",
    }
    # test the first records for exact values
    assert meters_ts[0] == {
        "serial_number": "SM60R-07-00018557",
        "manufacturer": "sparkmeter",
        "billing_cycle_start_at": "2022-12-01T20:45:00",
        "metered_at": "2022-12-05T11:15:00",
        "energy_lifetime_wh": 10384.89446875,
        "last_meter_state_code": 1,
        "operating_mode": 0,
        "last_config_datetime": "2022-12-04T18:05:18.842360",
        "current_daily_energy": None,
        "total_cycle_energy": 138.790687500003,
    }


def test__extract_meters_data(spark_meter_data) -> None:
    meters = spark_meter.extract_meters_data(spark_meter_data)
    # number of object extracted
    assert len(meters) == 5
    # keys
    assert key_set(meters) == {
        "postalcode",
        "active",
        "street1_p",
        "customer_country",
        "serial_number",
        "tags",
        "street2_e",
        "bootloader",
        "account_external_id",
        "customer_external_id",
        "customer_phone_p",
        "customer_latitude_b",
        "customer_longitude_b",
        "customer_longitude_p",
        "customer_phone_e",
        "device_external_id",
        "payment_plan",
        "customer_address_p",
        "model",
        "is_locked",
        "customer_latitude_p",
        "customer_latitude_e",
        "customer_location_area_1",
        "city",
        "manufacturer",
        "street1_e",
        "firmware_version",
        "street2_p",
        "customer_longitude_e",
        "customer_address_e",
    }
    # test the first records for exact values
    assert meters[0] == {
        "model": "SM60R",
        "manufacturer": "sparkmeter",
        "account_external_id": "0efb960c-5b95-45fc-a2c5-9c474720f789",
        "customer_address_p": "5b226d014117d365cb4f745785bc8a2928f8c9fa185ae5ac6e65a03fcc00687a",  # noqa: E501
        "customer_address_e": None,
        "customer_country": "Zambia",
        "payment_plan": "High Consumption - 2nd Bouquet",
        "firmware_version": "0100050000",
        "is_locked": True,
        "serial_number": "SM60R-07-00018557",
        "device_external_id": "SM60R-07-00018557",
        "customer_location_area_1": "North Western Province",
        "customer_latitude_b": None,
        "customer_longitude_b": None,
        "customer_latitude_p": "",
        "customer_longitude_p": "",
        "customer_latitude_e": "",
        "customer_longitude_e": "",
        "customer_phone_p": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",  # noqa: E501
        "customer_phone_e": None,
        "customer_external_id": "c28b5f939c2fde6fe465eff5c723942ee369ba75b9e3eea41d56000df9ef0d47",  # noqa: E501
        "tags": [],
        "street1_p": "790262e30d8cb3ab99c05ebdff4b20af2511d9c9c49f6149fc2acc1e46d0b06a",
        "street1_e": None,
        "postalcode": "",
        "active": True,
        "bootloader": "0100020000",
        "city": "Mwinilunga",
        "street2_p": None,
        "street2_e": None,
    }
