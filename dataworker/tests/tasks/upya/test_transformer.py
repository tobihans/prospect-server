import pandas as pd
import pytest

from dataworker.dataset import key_set
from tasks.ingestion import upya
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def upya_data():
    """Load test data from file."""
    return load_test_data("datafile/upya.json")


# --------------------------------------------------


def test__extract_shs_data(upya_data) -> None:
    shs = upya.upya_shs_dataframe(upya_data)

    shs_data = shs.to_dict(orient="records")

    # number of object extracted
    assert len(shs_data) == 2

    # keys
    assert key_set(shs_data) == {
        "purchase_date",
        "account_external_id",
        "seller_external_id",
        "payment_plan_type",
        "payment_plan_currency",
        "customer_external_id",
        "customer_birth_year",
        "customer_country",
        "customer_gender",
        "customer_phone_p",
        "customer_latitude_e",
        "customer_longitude_e",
        "payment_plan_down_payment",
        "device_external_id",
        "customer_first_name",
        "customer_last_name",
        "total_cost",
        "contract_status",
        "contract_last_update",
        "total_days_active",
        "deal_number",
        "remaining_debt",
        "total_paid",
        "product_name",
        "first_days",
        "recurr_payment",
        "recurr_freq_days",
        "min_payment",
        "customer_name_p",
        "serial_number_actual",
        "manufacturer",
        "serial_number",
        "payment_plan_amount_financed",
        "time_given_at_start_in_days",
        "paid_off_date",
    }

    # test the first records for exact values
    assert shs_data[0] == {
        "purchase_date": "2022-05-12T10:00:00.000Z",
        "account_external_id": "A36948279",
        "seller_external_id": "09JLX",
        "payment_plan_type": "PAYG",
        "payment_plan_currency": "ZMW",
        "customer_external_id": "C36945959",
        "customer_birth_year": 1985,
        "customer_country": "Zambia",
        "customer_gender": "Male",
        "customer_phone_p": "eeec406f114e450762067f919a3860769f02eaab1d94682381692f1cee57ab19",  # noqa: E501
        "customer_latitude_e": None,
        "customer_longitude_e": None,
        "payment_plan_down_payment": 400,
        "device_external_id": "A36948279",
        "customer_first_name": "d4272e6dcb406cc549e63d0f6d36c01dfe445db0ee12117183b080cac6908ab3",  # noqa: E501
        "customer_last_name": "5b12902c33a10f8243f9cae2eec1fab1dabb718f783cd865a731fbf9bd54dedb",  # noqa: E501
        "total_cost": 2500,
        "contract_status": "PAIDOFF",
        "contract_last_update": pd.to_datetime("2024-04-01 16:34:19.996000+00:00"),
        "total_days_active": 339,
        "deal_number": "XH0HY",
        "remaining_debt": 0,
        "total_paid": 2500,
        "product_name": "P1 Ogress Sunking Home 200x",
        "first_days": 7,
        "recurr_payment": 175,
        "recurr_freq_days": 30,
        "min_payment": 6,
        "paid_off_date": pd.to_datetime("2024-04-01 16:34:19.996000+00:00"),
        "customer_name_p": "d4272e6dcb406cc549e63d0f6d36c01dfe445db0ee12117183b080cac6908ab35b12902c33a10f8243f9cae2eec1fab1dabb718f783cd865a731fbf9bd54dedb",  # noqa: E501
        "serial_number_actual": "4340377572154",
        "manufacturer": "GLP",
        "serial_number": "4340377572154",
        "time_given_at_start_in_days": 7,
        "payment_plan_amount_financed": 2100,
    }


def test__extract_payments_ts_data(upya_data) -> None:
    pmt = upya.upya_payment_dataframe(upya_data)

    payment_ts = pmt.to_dict(orient="records")

    # number of object extracted
    assert len(payment_ts) == 21

    # keys
    assert key_set(payment_ts) == {
        "payment_external_id",
        "paid_at",
        "amount",
        "currency",
        "provider_name",
        "provider_transaction_id",
        "account_external_id",
        "account_origin",
        "days_active",
    }

    # test the first records for exact values
    assert payment_ts[0] == {
        "payment_external_id": "523663706",
        "paid_at": pd.to_datetime("2023-10-30 16:26:44.768000+00:00"),
        "amount": 10,
        "currency": "ZMW",
        "provider_name": "Primenet",
        "provider_transaction_id": "BLX321826302310P10376",
        "account_external_id": "A23560380",
        "account_origin": "shs",
        "days_active": 14,
    }
