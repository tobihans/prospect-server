import pytest
from glom.core import UnregisteredTarget

from tasks.ingestion import upya
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def upya_data():
    """Load test data from file."""
    return load_test_data("upya_test_missing.json")


# --------------------------------------------------


def test__extract_w_missing(upya_data):
    with pytest.raises(UnregisteredTarget):
        upya.transform(upya_data)

    # TODO: desired behavior?
    # assert missed_data == {}
