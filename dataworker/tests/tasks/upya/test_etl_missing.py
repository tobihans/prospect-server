import pytest
from glom.core import UnregisteredTarget

from dataworker.queries import count_rows
from dataworker.task import IngestionTask
from tasks.ingestion import upya


@pytest.fixture(autouse=True)
def _module_autouse(_database):
    """Common fixtures required for this module."""
    ...


def test__etl_missing():
    upya_etl = IngestionTask("upya", upya.transform)
    with pytest.raises(UnregisteredTarget):
        upya_etl(
            import_id="123456789",
            source_id="123456789",
            file_key="upya_test_missing.json",
            file_format="application/json",
            data_origin="over_there",
            organization_id="1234",
        )

    # TODO: check behavior
    assert count_rows("data_shs") == 0
    assert count_rows("data_payments_ts") == 0
