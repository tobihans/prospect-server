# ruff: noqa: E501
import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import count_rows, table_content
from dataworker.task import IngestionTask
from tasks.ingestion import mtn_zm


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    upya_etl = IngestionTask("mtn_zt", mtn_zm.transform)
    upya_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/mtn_zm.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    # Check for successfull insert.
    assert count_rows("data_payments_ts") == 2
    assert table_content(
        "data_payments_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "unknown",
            "account_origin": "meters",
            "account_uid": "1234_over_there_unknown",
            "amount": 130.0,
            "currency": "ZMW",
            "custom": {
                "note": "Merchant: Supamoto",
                "payee": "FRI:36172410/MM",
                "payer_p": "1263d4b2ec3ec5020e31a7cfb2983fc5e716a49d8620bf6377041bba48a26b03",
                "payee_name": "Supamoto Solar Payment",
                "payer_name_p": "55a892fd399fc30cad79aad6bb786f312e7952c909c4e6833016d6bc254a329e",
                "payee_handler_name": "Supamoto Solar Payment",
                "payer_handler_name_p": "55a892fd399fc30cad79aad6bb786f312e7952c909c4e6833016d6bc254a329e",
                "external_transaction_id": "a032a08b-8369-4099-8bbf-9ce6c4883429",
            },
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 9, 30, 14, 12),
            "payment_external_id": "4601621234",
            "provider_name": "mtn_zm",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "mobile money statement",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_unknown_4601621234_2023-09-30T14:12:00+00:00",
        },
        {
            "account_external_id": "unknown",
            "account_origin": "meters",
            "account_uid": "1234_over_there_unknown",
            "amount": 130.0,
            "currency": "ZMW",
            "custom": {
                "note": "Merchant: Supamoto",
                "payee": "FRI:36172410/MM",
                "payer_p": "c0a179aa4d58c2a5fa38368a293cfbe492c18de15796f9107e7aaff033a8067f",
                "payee_name": "Supamoto Solar Payment",
                "payer_name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7",
                "payee_handler_name": "Supamoto Solar Payment",
                "payer_handler_name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7",
                "external_transaction_id": "8d4c7e17-d356-4fee-990d-1f759f7eddc2",
            },
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2023, 9, 30, 16, 42),
            "payment_external_id": "4602231234",
            "provider_name": "mtn_zm",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "mobile money statement",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_unknown_4602231234_2023-09-30T16:42:00+00:00",
        },
    ])
