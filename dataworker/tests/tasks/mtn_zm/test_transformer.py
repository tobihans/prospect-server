import datetime

import pytest

from dataworker.dataset import key_set
from tasks.ingestion import mtn_zm
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def mtn_data():
    """Load test data from file."""
    return load_test_data("datafile/mtn_zm.json")


# --------------------------------------------------


def test__extract_payments_ts_data(mtn_data) -> None:
    payments_ts = mtn_zm.extract_payments_ts_data(mtn_data)

    # number of object extracted
    assert len(payments_ts) == 2
    # keys
    assert key_set(payments_ts) == {
        "account_external_id",
        "payee",
        "payee_handler_name",
        "purpose",
        "amount",
        "payer_name_e",
        "external_transaction_id",
        "payer_handler_name_e",
        "payer_handler_name_p",
        "payer_e",
        "currency",
        "payer_p",
        "payee_name",
        "account_origin",
        "payer_name_p",
        "payment_external_id",
        "note",
        "paid_at",
        "provider_name",
    }
    # test the first records for exact values
    assert payments_ts[0] == {
        "payment_external_id": "4602231234",
        "account_external_id": "unknown",
        "paid_at": datetime.datetime(2023, 9, 30, 16, 42, 0, tzinfo=datetime.UTC),
        "amount": "130.00",
        "provider_name": "mtn_zm",
        "purpose": "mobile money statement",
        "currency": "ZMW",
        "account_origin": "meters",
        "payer_e": None,
        "payer_p": "c0a179aa4d58c2a5fa38368a293cfbe492c18de15796f9107e7aaff033a8067f",
        "payer_name_e": None,
        "payer_name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7",  # noqa: E501
        "payer_handler_name_e": None,
        "payer_handler_name_p": "c29faf4f203517ad687bc39edec2be28b030e0236a4eecc5bf10a1132e5361d7",  # noqa: E501
        "external_transaction_id": "8d4c7e17-d356-4fee-990d-1f759f7eddc2",
        "payee": "FRI:36172410/MM",
        "payee_name": "Supamoto Solar Payment",
        "payee_handler_name": "Supamoto Solar Payment",
        "note": "Merchant: Supamoto",
    }
