from copy import deepcopy

import pandas as pd
import pytest

from dataworker.dataset import key_set
from tasks.ingestion import angaza
from tests.utils import load_test_data, prepare_for_compare


@pytest.fixture(scope="module")
def _angaza_json_data():
    """Load test data from file."""
    return load_test_data("datafile/angaza.json")


@pytest.fixture
def angaza_data(_angaza_json_data):
    """Copy test data for use at function level."""
    return deepcopy(_angaza_json_data)


@pytest.fixture
def angaza_data_bug_1():
    """Load test data from file."""
    return load_test_data("datafile/angaza_4_accounts_3_clients.json")


# --------------------------------------------------
def test__extract_shs_data(angaza_data) -> None:
    """Load and transform shs data."""
    shs = angaza.transform(angaza_data)
    # number of object extracted
    assert len(shs["data_shs"]) == 1
    # test for exact values
    assert prepare_for_compare(shs["data_shs"][0]) == {
        "account_external_id": "AC10011067",
        "contract_status": "ENABLED",
        "cumulative_days_disabled": 0,
        "customer_address_p": "205ab9e54e1de46d51e9a7e3d6fee7c609a0485c4b5f71e2677486e62c9bd524",  # noqa: E501
        "customer_category": "Household use only",
        "customer_external_id": "CL9800667",
        "customer_gender": "Female",
        "customer_household_size_original": "10+",
        "customer_location_area_1": "Copperbelt Province",
        "customer_location_area_2": "Kitwe",
        "customer_name_p": "fb6b4bb309373ce57608d80ceae7f4d4d98229a9518ec45b164c355bc11064e0",  # noqa: E501
        "customer_phone_p": "1d7ba2349da2a669577f341a2c867d5dc51a9e292fa2acfd4ff55ebf175587f7",  # noqa: E501
        "customer_profession": "Teacher (Education)",
        "device_external_id": 822802500,
        "is_pay_as_you_go": False,
        "is_test": False,
        "latest_payment_when": "2024-02-29T10:03:51Z",
        "manufacturer": "RDG",
        "minimum_payment": "7",
        "payment_due_date": "2024-03-30T10:03:51Z",
        "payment_plan_amount_financed": "5703",
        "payment_plan_currency": "ZMW",
        "payment_plan_days": 30,
        "payment_plan_down_payment": "399",
        "payment_plan_type": "PAYG",
        "purchase_date": "2024-02-29T10:03:51Z",
        "reference_pricing_amount": "221.000",
        "serial_number": 822802500,
        "time_given_at_start_in_days": 30,
        "total_paid": "400.00",
    }


def test__extract_shs_data_bug_1(angaza_data_bug_1) -> None:
    """Load and transform shs data."""
    shs = angaza.transform(angaza_data_bug_1)
    # number of object extracted
    # There are 4 accounts, but only three matching clients. should return 3.
    assert len(shs["data_shs"]) == 3
    # test for exact values
    assert prepare_for_compare(shs["data_shs"][0]) == {
        "account_external_id": "AC11688804",
        "contract_status": "ENABLED",
        "cumulative_days_disabled": 0,
        "customer_address_p": "28295db6d72f7fd730afb8ecd57ec570c7dd49a907fd03e660b9511761ab0e34",  # noqa: E501
        "customer_category": "Business - Farming",
        "customer_external_id": "CL11438113",
        "customer_gender": "Female",
        "customer_household_size": 4,
        "customer_household_size_original": "4",
        "customer_location_area_1": "Central Province",
        "customer_location_area_2": "Shibuyunji",
        "customer_name_p": "4e6698a73df4de91bf30497e7706ab00820c400024d1c07176a533de543efc53",  # noqa: E501
        "customer_phone_p": "1d2f5011bbfca8c90b4cd9b94fb266abb72b305024ef127e20471b43c4058414",  # noqa: E501
        "customer_profession": "Administration",
        "device_external_id": 277798013,
        "is_pay_as_you_go": False,
        "is_test": False,
        "latest_payment_when": "2024-09-26T19:17:08Z",
        "manufacturer": "RDG",
        "minimum_payment": "9",
        "payment_due_date": "2024-10-26T19:17:08Z",
        "payment_plan_amount_financed": "6791",
        "payment_plan_currency": "ZMW",
        "payment_plan_days": 30,
        "payment_plan_down_payment": "599",
        "payment_plan_type": "PAYG",
        "purchase_date": "2024-09-26T19:17:08Z",
        "reference_pricing_amount": "258.000",
        "serial_number": 277798013,
        "time_given_at_start_in_days": 30,
        "total_paid": "599.00",
    }


@pytest.mark.parametrize(
    ("value", "customer_houshold_size", "customer_household_size_original"),
    [
        (None, None, None),
        (9, 9, 9),
        ("9", 9, "9"),
        ("10+", None, "10+"),
    ],
)
def test__extract_shs_data_varying_household_size(
    angaza_data, value, customer_houshold_size, customer_household_size_original
) -> None:
    """Load and transform shs data, handling possible household size value types.

    -as integer (e.g. 9)
    -as string representation of integer (e.g. "9")
    -as string (e.g. "10+")
    """
    for attr_dict in angaza_data["clients"]["_embedded"]["item"][0]["attribute_values"]:
        if attr_dict["name"] == "customer_household_size":
            attr_dict["value"] = value
    shs = angaza.transform(angaza_data)
    # number of object extracted
    assert len(shs["data_shs"]) == 1
    # this should be None when integer conversion fails.
    assert shs["data_shs"][0]["customer_household_size"] == customer_houshold_size
    # this should always be the unchanged original value.
    assert (
        shs["data_shs"][0]["customer_household_size_original"]
        == customer_household_size_original
    )


def test__extract_payments_ts_data(angaza_data) -> None:
    """Load and transform payments_ts data."""
    payments_ts = angaza.transform(angaza_data)
    # number of object extracted
    assert len(payments_ts["data_payments_ts"]) == 2
    # keys
    assert key_set(payments_ts["data_payments_ts"]) == {
        "account_external_id",
        "account_origin",
        "payment_external_id",
        "amount",
        "currency",
        "paid_at",
        "purpose",
        "provider_name",
    }
    # test the first records for exact values
    assert payments_ts["data_payments_ts"][0] == {
        "account_external_id": "AC10011067",
        "account_origin": "shs",
        "amount": "52.00",
        "currency": "ZMW",
        "paid_at": "2024-02-29T10:25:03Z",
        "payment_external_id": "RC718002056",
        "purpose": "paygo payment",
        "provider_name": "Angaza",
    }


# --------------------------------------------------


@pytest.mark.parametrize(
    ("removals", "shs_empty", "payments_empty"),
    [
        (["accounts", "clients", "payments"], True, True),
        (["clients", "payments"], True, True),
        (["accounts", "payments"], True, True),
        (["accounts", "clients"], True, False),
        (["accounts"], True, False),
        (["clients"], True, False),
        (["payments"], False, True),
    ],
)
def test__transform_empty_dict_items(
    angaza_data, removals, shs_empty, payments_empty
) -> None:
    """Load and transform shs data using several varations of incoming dicts.

    Check to see if output dicts are empty -- or not -- as expected.
    """
    variation = {
        k: {"_embedded": {"item": []}} if k in removals else v
        for k, v in angaza_data.items()
    }
    result = angaza.transform(variation)
    assert bool(result["data_shs"]) is not shs_empty
    assert bool(result["data_payments_ts"]) is not payments_empty


# --------------------------------------------------


@pytest.mark.parametrize(
    ("data_variant", "expected_columns"),
    [
        ([("A", 1), ("B", 2)], ["A", "B"]),
        ([("A", 1), ("B", 2), ("A", 3)], ["A", "B", "A1"]),
        ([("A", 1), ("A", 2), ("A", 3)], ["A", "A1", "A2"]),
    ],
)
def test___add_suffixes(data_variant, expected_columns):
    # Manually set columns to include duplicates.
    variant_df = pd.DataFrame(
        [(i[1] for i in data_variant)],
        columns=[i[0] for i in data_variant],
    )
    assert list(angaza._add_suffixes(variant_df).columns) == expected_columns
