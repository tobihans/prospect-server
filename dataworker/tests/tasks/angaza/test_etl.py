import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import count_rows, table_content
from dataworker.task import IngestionTask
from tasks.ingestion import angaza


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    """Run angaza etl process."""
    angaza_etl = IngestionTask("angaza", angaza.transform)
    angaza_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/angaza.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    assert count_rows("data_shs") == 1
    assert count_rows("data_payments_ts") == 2
    assert table_content(
        "data_shs", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "AC10011067",
            "account_uid": "1234_over_there_AC10011067",
            "battery_energy_wh": None,
            "custom": {
                "total_paid": "400.00",
                "contract_status": "ENABLED",
                "minimum_payment": "7",
                "is_pay_as_you_go": False,
                "payment_due_date": "2024-03-30T10:03:51Z",
                "latest_payment_when": "2024-02-29T10:03:51Z",
                "cumulative_days_disabled": 0,
                "reference_pricing_amount": "221.000",
                "time_given_at_start_in_days": 30,
                "customer_household_size_original": "10+",
            },
            "customer_address_e": None,
            "customer_address_p": "205ab9e54e1de46d51e9a7e3d6fee7c609a0485c4b5f71e2677486e62c9bd524",  # noqa: E501
            "customer_birth_year": None,
            "customer_category": "Household use only",
            "customer_country": None,
            "customer_email_e": None,
            "customer_email_p": None,
            "customer_external_id": "CL9800667",
            "customer_former_electricity_source": None,
            "customer_gender": "F",
            "customer_household_size": None,
            "customer_id_number_e": None,
            "customer_id_number_p": None,
            "customer_id_type": None,
            "customer_latitude_b": None,
            "customer_latitude_e": None,
            "customer_latitude_p": None,
            "customer_location_area_1": "Copperbelt Province",
            "customer_location_area_2": "Kitwe",
            "customer_location_area_3": None,
            "customer_location_area_4": None,
            "customer_location_area_5": None,
            "customer_longitude_b": None,
            "customer_longitude_e": None,
            "customer_longitude_p": None,
            "customer_name_e": None,
            "customer_name_p": "fb6b4bb309373ce57608d80ceae7f4d4d98229a9518ec45b164c355bc11064e0",  # noqa: E501
            "customer_phone_2_e": None,
            "customer_phone_2_p": None,
            "customer_phone_e": None,
            "customer_phone_p": "1d7ba2349da2a669577f341a2c867d5dc51a9e292fa2acfd4ff55ebf175587f7",  # noqa: E501
            "customer_profession": "Teacher (Education)",
            "customer_sub_category": None,
            "customer_uid": "1234_over_there_CL9800667",
            "data_origin": "over_there",
            "device_external_id": "822802500",
            "device_uid": "1234_RDG_822802500",
            "import_id": 123456789,
            "is_test": False,
            "last_import_id": None,
            "last_source_id": None,
            "manufacturer": "RDG",
            "model": None,
            "organization_id": 1234,
            "paid_off_date": None,
            "payment_plan_amount_financed": 5703.0,
            "payment_plan_currency": "ZMW",
            "payment_plan_days": 30.0,
            "payment_plan_down_payment": 399.0,
            "payment_plan_type": "PAYG",
            "primary_use": None,
            "purchase_date": datetime.date(2024, 2, 29),
            "pv_power_w": None,
            "rated_power_w": None,
            "repossession_date": None,
            "seller_address_e": None,
            "seller_address_p": None,
            "seller_country": None,
            "seller_email_e": None,
            "seller_email_p": None,
            "seller_external_id": None,
            "seller_gender": None,
            "seller_latitude_b": None,
            "seller_latitude_e": None,
            "seller_latitude_p": None,
            "seller_location_area_1": None,
            "seller_location_area_2": None,
            "seller_location_area_3": None,
            "seller_location_area_4": None,
            "seller_longitude_b": None,
            "seller_longitude_e": None,
            "seller_longitude_p": None,
            "seller_name_e": None,
            "seller_name_p": None,
            "seller_phone_e": None,
            "seller_phone_p": None,
            "serial_number": "822802500",
            "source_id": 123456789,
            "total_price": None,
            "uid": "1234_over_there_CL9800667_1234_RDG_822802500",
            "voltage_v": None,
        }
    ])
    assert table_content(
        "data_payments_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "AC10011067",
            "account_origin": "shs",
            "account_uid": "1234_over_there_AC10011067",
            "amount": 25.0,
            "currency": "ZMW",
            "custom": {},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2024, 3, 29, 10, 24, 18),
            "payment_external_id": "RC718001899",
            "provider_name": "Angaza",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "paygo payment",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "shs_1234_over_there_AC10011067_RC718001899_2024-03-29T10:24:18+00:00",  # noqa: E501
        },
        {
            "account_external_id": "AC10011067",
            "account_origin": "shs",
            "account_uid": "1234_over_there_AC10011067",
            "amount": 52.0,
            "currency": "ZMW",
            "custom": {},
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2024, 2, 29, 10, 25, 3),
            "payment_external_id": "RC718002056",
            "provider_name": "Angaza",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "paygo payment",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "shs_1234_over_there_AC10011067_RC718002056_2024-02-29T10:25:03+00:00",  # noqa: E501
        },
    ])
