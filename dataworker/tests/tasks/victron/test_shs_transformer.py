import pytest
from pandas import Timestamp

from dataworker.dataset import key_set
from tasks.ingestion import victron_shs
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def victron_data():
    """Load test data from file."""
    return load_test_data("datafile/victron_current.json")


# --------------------------------------------------


def test__extract_shs_ts_data(victron_data) -> None:
    shs_ts = victron_shs.extract_shs_ts_data(victron_data)
    # Number of object extracted.
    assert len(shs_ts) == 9
    # Keys.
    assert key_set(shs_ts) == {
        "pv_to_battery_kwh",
        "shs_name",
        "battery_charge_percent",
        "output_energy_interval_wh",
        "manufacturer",
        "system_output_w",
        "battery_to_customer_kwh",
        "pv_energy_interval_wh",
        "battery_io_w",
        "serial_number",
        "pv_to_customer_kwh",
        "pv_input_w",
        "metered_at",
        "interval_seconds",
    }
    # Test the first records for exact values.
    assert shs_ts[0] == {
        "shs_name": "Chaba Minigrid",
        "metered_at": Timestamp("2024-02-07 10:45:00"),
        "pv_to_customer_kwh": 3.317472219467163,
        "pv_to_battery_kwh": 5.33868408203125,
        "battery_to_customer_kwh": 0.0,
        "battery_charge_percent": 52.0,
        "serial_number": 125796,
        "manufacturer": "victron",
        "battery_io_w": 5338.68408203125,
        "pv_energy_interval_wh": 8656.156301498413,
        "output_energy_interval_wh": 3317.472219467163,
        "interval_seconds": 3600,
        "pv_input_w": 8656.156301498413,
        "system_output_w": 3317.472219467163,
    }


@pytest.mark.usefixtures("_mocked_address_object")
def test__extract_shs_data(victron_data) -> None:
    shs = victron_shs.extract_shs_data(victron_data)
    # Number of object extracted.
    assert len(shs) == 6
    # Keys.
    assert key_set(shs) == {
        "customer_latitude_p",
        "device_external_id",
        "customer_longitude_b",
        "fw_version",
        "customer_external_id",
        "serial_number",
        "customer_latitude_b",
        "customer_longitude_p",
        "manufacturer",
        "name",
        "customer_phone_p",
        "account_external_id",
        "customer_longitude_e",
        "customer_phone_e",
        "customer_latitude_e",
    }
    # test the first records for exact values
    assert shs[0] == {
        "account_external_id": 125796,
        "customer_external_id": 125796,
        "device_external_id": 125796,
        "serial_number": 125796,
        "manufacturer": "victron",
        "name": "Chaba Minigrid",
        "customer_latitude_b": None,
        "customer_latitude_p": "",
        "customer_latitude_e": None,
        "customer_longitude_b": None,
        "customer_longitude_p": "",
        "customer_longitude_e": None,
        "customer_phone_p": None,
        "customer_phone_e": None,
        "fw_version": "v3.01",
    }
