import pytest
from pandas import Timestamp

from dataworker.dataset import key_set
from tasks.ingestion import victron_grids
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def victron_data():
    """Load test data from file."""
    return load_test_data("datafile/victron_current.json")


# --------------------------------------------------


def test__extract_grids_ts_data(victron_data) -> None:
    grids_ts = victron_grids.extract_grids_ts_data(victron_data)
    # Number of object extracted.
    assert len(grids_ts) == 9
    # Keys.
    assert key_set(grids_ts) == {
        "battery_charge_state_percent",
        "battery_to_customer_kwh",
        "metered_at",
        "primary_source_to_battery_kwh",
        "primary_source_to_customer_kwh",
        "grid_name",
    }
    # Test the first records for exact values.
    assert grids_ts[0] == {
        "grid_name": "Chaba Minigrid",
        "metered_at": Timestamp("2024-02-07 10:45:00"),
        "primary_source_to_customer_kwh": 3.317472219467163,
        "primary_source_to_battery_kwh": 5.33868408203125,
        "battery_to_customer_kwh": None,
        "battery_charge_state_percent": 52.0,
    }


@pytest.mark.usefixtures("_mocked_address_object")
def test__extract_grids_data(victron_data) -> None:
    grids = victron_grids.extract_grids_data(victron_data)
    # Number of object extracted.
    assert len(grids) == 6
    # Keys.
    assert key_set(grids) == {
        "latitude",
        "name",
        "external_id",
        "longitude",
        "fw_version",
    }
    # test the first records for exact values
    assert grids[0] == {
        "external_id": 125796,
        "name": "Chaba Minigrid",
        "latitude": None,
        "longitude": None,
        "fw_version": "v3.01",
    }
