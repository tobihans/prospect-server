import io
from datetime import datetime

import pytest

from dataworker import storage
from dataworker.dataset import key_set
from tasks.ingestion import solarman
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def solarman_data():
    """Load test data from file."""
    return load_test_data("datafile/solarman.json")


# --------------------------------------------------


def test__extract_data(solarman_data, mocker) -> None:
    mocker.patch.object(solarman, "download_image", return_value="grids/60993186.jpg")

    result = solarman.transform(solarman_data)
    grids_ts = result["data_grids_ts"]
    grids = result["data_grids"]
    # Number of object extracted.
    assert len(grids_ts) == 2
    assert len(grids) == 1

    # Keys.
    assert key_set(grids_ts) == {
        "battery_to_customer_kwh",
        "metered_at",
        "primary_source_to_battery_kwh",
        "primary_source_to_customer_kwh",
        "grid_name",
    }
    # Test the first records for exact values.
    assert grids_ts[0] == {
        "battery_to_customer_kwh": 38.0,
        "metered_at": datetime.strptime("2023-11-13 23:59:59", "%Y-%m-%d %H:%M:%S"),
        "primary_source_to_battery_kwh": 39.6,
        "primary_source_to_customer_kwh": 9.299999999999997,
        "grid_name": "SAM",
    }
    assert grids_ts[1] == {
        "battery_to_customer_kwh": 39.0,
        "grid_name": "SAM",
        "metered_at": datetime(2023, 11, 12, 23, 59, 59),
        "primary_source_to_customer_kwh": 8.299999999999997,
        "primary_source_to_battery_kwh": 39.5,
    }


@pytest.mark.usefixtures("_mocked_address_object")
def test__extract_grids_data(solarman_data, mocker) -> None:
    mocker.patch.object(solarman, "download_image", return_value="grids/60993186.jpg")
    result = solarman.transform(solarman_data)
    grids_ts = result["data_grids_ts"]
    grids = result["data_grids"]

    # Number of object extracted.
    assert len(grids) == 1
    # Keys.
    assert key_set(grids) == {
        "external_id",
        "latitude",
        "longitude",
        "name",
        "country",
        "location_area_1",
        "location_area_2",
        "location_area_3",
        "location_area_4",
        "operator_phone_e",
        "operator_phone_p",
        "primary_input_installed_kw",
        "power_rating_kw",
        "custom",
    }
    # test the first records for exact values
    assert grids[0] == {
        "external_id": 60993186,
        "longitude": 2.1525448,
        "latitude": 7.1758639,
        "location_area_3": None,
        "location_area_4": None,
        "name": "SAM",
        "country": "ZM",
        "location_area_1": "Northern Province",
        "location_area_2": "Chilubi District",
        "power_rating_kw": 126.0,
        "primary_input_installed_kw": 17.48,
        "operator_phone_e": None,
        "operator_phone_p": None,
        "custom": {
            "station_image": "https://img1.solarmanpv.com/default/3fce41ab612144a2a2f1a4555d67ea851696346430577.jpg"
        },
    }

    assert len(grids_ts) == 2
    assert grids_ts[0] == {
        "battery_to_customer_kwh": 38.0,
        "grid_name": "SAM",
        "metered_at": datetime(2023, 11, 13, 23, 59, 59),
        "primary_source_to_customer_kwh": 9.299999999999997,
        "primary_source_to_battery_kwh": 39.6,
    }

    assert grids_ts[1] == {
        "battery_to_customer_kwh": 39.0,
        "grid_name": "SAM",
        "metered_at": datetime(2023, 11, 12, 23, 59, 59),
        "primary_source_to_customer_kwh": 8.299999999999997,
        "primary_source_to_battery_kwh": 39.5,
    }


@pytest.mark.usefixtures("_mocked_address_object")
def test_download_images(solarman_data, mocker) -> None:
    mocker.patch.object(
        solarman,
        "download_image",
        return_value=io.BytesIO(b"JournalDev Python: \x00\x01"),
    )

    storage.get_minio_client(write=True)
    mocker.patch.object(solarman, "save_to_minio", return_value=None)

    result = solarman.transform(solarman_data)
    grids = result["data_grids"]
    assert (
        grids[0]["custom"]["station_image"]
        == "https://img1.solarmanpv.com/default/3fce41ab612144a2a2f1a4555d67ea851696346430577.jpg"
    )
    assert (
        grids[0]["custom"]["station_image_local"]
        == "3fce41ab612144a2a2f1a4555d67ea851696346430577.jpg"
    )
