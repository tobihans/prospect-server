import pytest

from dataworker.dataset import key_set
from tasks.ingestion import supamoto
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def supamoto_data():
    """Load test data from file."""
    return load_test_data("datafile/supamoto.json")


# --------------------------------------------------


def test__extract_payments_ts_data(supamoto_data) -> None:
    payments_ts = supamoto.extract_payments_ts_data(supamoto_data)

    # number of object extracted
    assert len(payments_ts) == 9
    # keys
    assert key_set(payments_ts) == {
        "payment_external_id",
        "purchase_unit",
        "account_external_id",
        "amount",
        "account_origin",
        "purchase_quantity",
        "purchase_item",
        "currency",
        "paid_at",
        "provider_name",
        "purpose",
    }
    # test the first record (payment) for exact values
    assert payments_ts[0] == {
        "payment_external_id": "MP240131.1021.L05116",
        "amount": 18000,
        "currency": "MWK",
        "provider_name": "airtel_zm",
        "account_external_id": 310034435,
        "paid_at": "2024-01-31T10:21:00",
        "account_origin": "meters",
        "purpose": "payment",
    }
    # test another record (order) for exact values
    assert payments_ts[8] == {
        "payment_external_id": "SM240120.063435._2G5lZpDRg8",
        "amount": 1,
        "currency": "ZMW",
        "account_external_id": 550000310,
        "paid_at": "2024-01-20T06:34:35.769125",
        "purchase_item": "ACCESSORY",
        "purchase_unit": None,
        "purchase_quantity": None,
        "account_origin": "meters",
        "purpose": "order",
    }


def test__extract_meters_data(supamoto_data) -> None:
    meters = supamoto.extract_meters_data(supamoto_data)

    # number of object extracted
    assert len(meters) == 2
    # keys
    assert key_set(meters) == {
        "customer_longitude_e",
        "customer_longitude_p",
        "customer_latitude_b",
        "certificateCid",
        "customer_longitude_b",
        "model",
        "account_external_id",
        "customer_country",
        "device_external_id",
        "customer_latitude_p",
        "serial_number",
        "primary_use",
        "manufacturer",
        "installation_date",
        "customer_external_id",
        "customer_latitude_e",
    }
    # test the first records for exact values
    assert meters[0] == {
        "device_external_id": 310034879,
        "model": "MIMIMOTO",
        "customer_country": "MW",
        "certificateCid": "bafybeies7q3lko62xv5qmvmvmhy24jvhrcg3f22gw5a2e7hhpjasjc4npu",
        "installation_date": "2022-07-24T22:27:03",
        "customer_latitude_e": None,
        "customer_longitude_e": None,
        "customer_latitude_p": "b1204de5a02744b0a252b9ff4f07729fde02d2fe441e3dbf79dfc9b8ea5eb288",  # noqa: E501
        "customer_longitude_p": "090f599c88dacf3cc91745c71d76948fa01520722ad8996ccda6163b9dbe4a68",  # noqa: E501
        "customer_latitude_b": -13.914,
        "customer_longitude_b": 33.75,
        "customer_external_id": 310034879,
        "serial_number": 310034879,
        "account_external_id": 310034879,
        "manufacturer": "supamoto",
        "primary_use": "cooking",
    }
