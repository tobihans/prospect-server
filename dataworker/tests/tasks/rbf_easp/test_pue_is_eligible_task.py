from copy import deepcopy
from datetime import UTC, date, datetime
from unittest.mock import ANY

import pytest
import time_machine

from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from dataworker.queries import table_content
from dataworker.task import InternalTask
from tasks.internal.rbf_easp.common import (
    CLAIM_TEMPLATES_TABLE,
    METERS_TABLE,
    PAYMENTS_TS_TABLE,
    PRODUCT_PRICES_TABLE,
    PRODUCTS_TABLE,
    VERIFICATIONS_TABLE,
    calculate_incentive,
    calculate_subsidy,
    get_technology,
)
from tasks.internal.rbf_easp.pue_is_eligible import (
    CHECK_NAME,
    INCENTIVES,
    SUBSIDY_ADJUSTMENTS,
    run,
)
from tasks.internal.rbf_easp.subchecks import (
    subcheck__customer_data_is_complete,
    subcheck__customer_data_is_unique,
    subcheck__customer_is_eligible,
    subcheck__entity_is_repossessed_at_most_once,
    subcheck__payment_plan_is_complete,
    subcheck__product_is_on_white_list,
    subcheck__sale_is_not_older_than_three_months,
)
from tests.tasks.rbf_easp.conftest import (
    prepare_test_data_map_rbf_category,
)
from tests.utils import (
    RecordSetWithModifiers,
    insert_records,
    insert_recordset,
    prepare_many_for_compare,
    prepare_test_data,
)


@pytest.fixture(autouse=True)
def _module_autouse(_database):
    """Common fixtures required for this module."""
    ...


@time_machine.travel(datetime(2025, 4, 1, 20, 19, 42, tzinfo=UTC), tick=False)
@pytest.mark.usefixtures("_populate_rbf_helper_tables")
def test__rbf_easp_pue_is_eligible_task_from_json(
    pue_meters_data, ccs_pue_payments_ts_data
):
    # --- DATA INGESTION ---
    insert_recordset(pue_meters_data)
    insert_recordset(ccs_pue_payments_ts_data)
    # --- TASK ---
    InternalTask.from_function(run)()
    # --- CHECK RESULT ---
    trust_trace = table_content(
        "data_trust_trace",
        shed_nulls=True,
        string_timestamps=True,
        shed_custom_paths=["subsidy__price_id"],
    )
    trust_traces = [
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No payments found.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "technology": "device type A",
                "subcheck__customer_data_is_complete": {
                    "result": False,
                    "details": "NORTHERY administrative unit not found.",
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-05) exceeds "
                    "three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_B74320525_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_B74320525_222222_PUE Company A_D83209731_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "customer_category (other enterprise) is not one of "
                    "['mse', 'tfh'].",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase "
                    "date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) "
                    "combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds "
                    "three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C23488192_222222_PUE Company B_D23488192",
            "uid": "meters_222222_solar_coop_C23488192_222222_PUE Company B_D23488192_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company C) and model (PUE Device C) "
                    "combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase "
                    "date, sales mode and product.",
                },
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C26881862_222222_PUE Company C_D26881862",
            "uid": "meters_222222_solar_coop_C26881862_222222_PUE Company C_D26881862_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "customer&category is not unique (conflicting uid: "
                    "222222_solar_coop_C74320525_222222_PUE Company A_D74320525).",
                },
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "technology": "device type A",
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_PUE Company A_D74320521",
            "uid": "meters_222222_solar_coop_C74320525_222222_PUE Company A_D74320521_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "incentive__type": "Northern/Madi-Okollo/Rhd",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__aggregated_subsidy_adjustment": 50000,
                "subsidy__subsidized_sales_price": 40000,
                "subsidy__amount": 60000,
                "incentive__amount": 203000,
                "subsidy__category": "A/female_ownership",
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "technology": "device type A",
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "subsidy__price_calculated_at": "2025-04-01T20:19:42+00:00",
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "True",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_PUE Company A_D74320525",
            "uid": "meters_222222_solar_coop_C74320525_222222_PUE Company A_D74320525_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the "
                    "combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "technology": "device type A",
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {
                    "result": False,
                    "details": "More than one previous sale exists for the manufacturer"
                    "/serial combination.",
                },
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-15) exceeds "
                    "three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83209731_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_C83209731_222222_PUE Company A_D83209731_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "Enterprise has more than 50 employees and/or 100mio "
                    "UGX in assets.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase "
                    "date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) "
                    "combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83687814_222222_PUE Company B_D83687814",
            "uid": "meters_222222_solar_coop_C83687814_222222_PUE Company B_D83687814_"
            "rbf_easp_pue_is_eligible",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "Payment sums for account not sufficient to cover "
                    "payment_plan_down_payment of 5000.0.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "technology": "device type A",
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "The purchase date (2023-11-05) can't preceed "
                    "2024-11-01.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_X74320525_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_X74320525_222222_PUE Company A_D83209731_"
            "rbf_easp_pue_is_eligible",
        },
    ]
    assert prepare_many_for_compare(trust_trace) == trust_traces

    # make a new record that will fail for uniqueness in-DB check
    new_record = deepcopy(pue_meters_data.records[0])
    new_record["purchase_date"] = "2025-01-07"
    new_record["customer_external_id"] += "N"
    new_record["customer_uid"] += "N"
    new_record["uid"] = f"{new_record['customer_uid']}_{new_record['device_uid']}"
    insert_records(pue_meters_data.table, [new_record])
    # --- TASK ---
    InternalTask.from_function(run)()
    # --- CHECK RESULT ---
    trust_trace = table_content(
        "data_trust_trace",
        shed_nulls=True,
        string_timestamps=True,
        shed_custom_paths=["subsidy__price_id"],
        shed_last_ids=True,  # as this adds keys when recalculating failed verifications
    )
    new_record_trace = {
        "check": "rbf_easp_pue_is_eligible",
        "custom": {
            "subcheck__customer_data_is_complete": {"result": True},
            "subcheck__customer_data_is_unique": {
                "details": "customer&category is not unique (conflicting uid: "
                "222222_solar_coop_C74320525_222222_PUE Company A_D74320525).",
                "result": False,
            },
            "subcheck__customer_is_eligible": {"result": True},
            "subcheck__entity_is_repossessed_at_most_once": {"result": True},
            "subcheck__payment_plan_is_complete": {"result": True},
            "subcheck__product_is_on_white_list": {"result": True},
            "subcheck__sale_is_not_older_than_three_months": {"result": True},
            "technology": "device type A",
        },
        "data_origin": "solar_coop",
        "import_id": 111111,
        "organization_id": 222222,
        "result": "False",
        "source_id": 333333,
        "subject_origin": "meters",
        "subject_uid": "222222_solar_coop_C74320525N_222222_PUE Company A_D74320525",
        "uid": "meters_222222_solar_coop_C74320525N_222222_PUE Company "
        "A_D74320525_rbf_easp_pue_is_eligible",
    }
    trust_traces.insert(3, new_record_trace)
    assert prepare_many_for_compare(trust_trace) == trust_traces


######

PAYMENTS_SAMPLE_DICT = {
    "account_origin": "meters",
    "account_external_id": "A74320525",
    "payment_external_id": "P74320525",
    "paid_at": "2024-01-05 16:28:30",
    "amount": 90000,
    "currency": "UGX",
}
METERS_SAMPLE_DICT = {
    "device_external_id": "D74320525",
    "account_external_id": "A74320525",
    "serial_number": "D74320525",
    "purchase_date": "2024-01-05",
    "manufacturer": "Company A",
    "payment_plan_days": 730,
    "payment_plan_down_payment": 10000,
    "total_price": "",
    "payment_plan": "paygo",
    "payment_plan_currency": "UGX",
    "repossession_date": "",
    # TODO: we add these here for the tests, but we probably should not allow these to
    # be modified by the dataworker and then disable this restriction on tests
    "customer_external_id": "C74320525",
    "customer_id_number_e": "AB1234567DE89",
    "customer_id_number_p": "AB1234567DE89foo",
    "customer_id_type": "NIN",
    "customer_name_e": "Londiwe Makhathini-Mbhodwe",
    "customer_name_p": "crypto-name",
    "customer_gender": "M",
    "customer_birth_year": 1996,
    "customer_country": "UG",
    "customer_latitude_e": 1.63157694739998,
    "customer_longitude_e": 32.8350548033075,
    "customer_phone_e": "+256 2817098278",
    "customer_phone_p": "crypto-phone-number",
    "customer_location_area_1": "NORTHERY",
    "customer_location_area_2": "AMOLATAR",
    "customer_location_area_3": "KIOGA COUNTY",
    "customer_location_area_4": "ACII",
    "customer_location_area_5": "ALAKU",
    "model": "Device A",
    "customer_category": "mse",
    "is_below_50_employees_and_100mio_ugx_assets": True,
}
PRODUCTS_DICT = {
    "manufacturer": "Company A",
    "rbf_claim_template_id": "1",
    "organization_id": "222222",
    "model": "Device A",
    "rbf_category": "A",
    "technology": "device type A",
}
PRODUCT_PRICES_DICT = {
    "rbf_product_id": "1",
    "original_sales_price": 100000,
    "base_subsidized_sales_price": 90000,
    "base_subsidy_amount": 10000,
    "category": "paygo",
    "valid_from": "2024-01-01",
}
CLAIM_TEMPLATE_DICT = {
    "user_id": "1",
    "name": "EASP PUE",
    "trust_trace_check": "rbf_easp_pue_is_eligible",
    "verifying_organization_id": "1",
    "managing_organization_id": "1",
    "supervising_organization_id": "1",
    "organization_group_id": "1",
    "currency": "EUR",
}
VERIFICATIONS_TEMPLATE_DICT = {
    "check": CHECK_NAME,
    "subject_origin": "meters",
    "result": True,
    "subject_uid": "value-to-be-overwritten",
}
METERS_RS = RecordSetWithModifiers(METERS_TABLE, [METERS_SAMPLE_DICT])
PAYMENTS_RS = RecordSetWithModifiers(PAYMENTS_TS_TABLE, [PAYMENTS_SAMPLE_DICT])
PRODUCTS_RS = RecordSetWithModifiers(
    PRODUCTS_TABLE,
    [PRODUCTS_DICT],
    id_adjustments=[{"rbf_claim_template_id": (CLAIM_TEMPLATES_TABLE, "id")}],
)
PRODUCT_PRICES_RS = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
CLAIM_RS = RecordSetWithModifiers(CLAIM_TEMPLATES_TABLE, [CLAIM_TEMPLATE_DICT])
VERIFICATIONS_RS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    id_adjustments=[{"subject_uid": (METERS_TABLE, "uid")}],
)

METERS_RS_OTHER_ID = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"customer_external_id": "66666"}],
)
METERS_RS_MODEL_OTHER_CAT = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"model": "B1", "customer_external_id": "66"}],
)
METERS_RS_OTHER_ORGA = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"customer_external_id": "66666", "organization_id": 666}],
)
PRODUCTS_RS_OTHER_ORGA = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"organization_id": 666}]
)
PRODUCTS_RS_OTHER_CAT = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"model": "B1", "rbf_category": "B"}]
)
VERIFICATIONS_RS_FALSE = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    modifiers=[{"result": False}],
    id_adjustments=[{"subject_uid": (METERS_TABLE, "uid")}],
)


@pytest.mark.parametrize(
    (
        "recordsets",
        "result",
    ),
    [
        ([METERS_RS], False),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS], True),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS], True),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_FALSE], True),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], False),
        (
            [METERS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, METERS_RS_OTHER_ID],
            False,
        ),
        (
            [
                METERS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS_FALSE,
                METERS_RS_OTHER_ID,
            ],
            True,
        ),
        (
            [
                METERS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS,
                METERS_RS_OTHER_ORGA,
                PRODUCTS_RS_OTHER_ORGA,
            ],
            False,
        ),
        (
            [
                METERS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS,
                METERS_RS_MODEL_OTHER_CAT,
                PRODUCTS_RS_OTHER_CAT,
            ],
            True,
        ),
    ],
)
def test_subcheck__customer_data_is_unique(
    recordsets, result, import_context: ImportContext, meters_table
) -> None:
    """Test all possible combinations of data for the same enterprise."""
    inserts = prepare_test_data_map_rbf_category(
        recordsets=recordsets, import_context=import_context
    )
    entity = inserts[METERS_TABLE][-1]

    # Also Test we have all data in DB with customer_id_number_e and _p
    for meter in inserts[METERS_TABLE]:
        assert meter["customer_id_number_e"] is not None
        assert meter["customer_id_number_p"] is not None

    check = subcheck__customer_data_is_unique(entity, meters_table, CHECK_NAME)
    assert check["result"] is result


METERS_RS_CAT_OTHER = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_category": "other"}]
)
METERS_RS_NOT_ELIGIBLE = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"is_below_50_employees_and_100mio_ugx_assets": False}],
)
METERS_RS_CAT_OTHER_AND_NOT_ELIGIBLE = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_category": "other",
            "is_below_50_employees_and_100mio_ugx_assets": False,
        },
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result", "detail_count"),
    [
        ([METERS_RS], True, None),
        ([METERS_RS_CAT_OTHER], False, 55),
        ([METERS_RS_NOT_ELIGIBLE], False, 66),
        ([METERS_RS_CAT_OTHER_AND_NOT_ELIGIBLE], False, 122),
    ],
)
def test_subcheck__customer_is_eligible(
    recordsets: list[RecordSet],
    result: bool,
    detail_count: int | None,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of missing enterprise data."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    meter = inserts[METERS_TABLE][0]
    # we adapt the python bool to the usual stringified bool coming out of the JSON
    meter["is_small_enterprise"] = str(
        meter["custom"]["is_below_50_employees_and_100mio_ugx_assets"]
    ).lower()
    check = subcheck__customer_is_eligible(meter, meters_table)

    assert check["result"] is result
    if detail_count is None:
        assert "details" not in check or check["details"] is None
    else:
        assert len(check.get("details", [])) == detail_count


METERS_RS_NO_NAME = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_name_p": None}]
)
METERS_RS_NO_ID_NUMBER = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_id_number_p": None}]
)
METERS_RS_NO_PHONE = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_phone_p": None}]
)
METERS_RS_NO_LOC_AREA_1 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_1": None}]
)
METERS_RS_NO_LOC_AREA_2 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_2": None}]
)
METERS_RS_NO_LOC_AREA_3 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_3": None}]
)
METERS_RS_NO_LOC_AREA_4 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_4": None}]
)
METERS_RS_NO_LOC_AREA_5 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_5": None}]
)
METERS_RS_NO_LOC_AREA_4_AND_5 = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"customer_location_area_5": None, "customer_location_area_4": None}],
)
METERS_RS_EMPTY_NAME = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_name_p": ""}]
)
METERS_RS_EMPTY_ID_NUMBER = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_id_number_p": ""}]
)
METERS_RS_EMPTY_PHONE = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_phone_p": ""}]
)
METERS_RS_EMPTY_LOC_AREA_1 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_1": ""}]
)
METERS_RS_EMPTY_LOC_AREA_2 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_2": ""}]
)
METERS_RS_EMPTY_LOC_AREA_3 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_3": ""}]
)
METERS_RS_EMPTY_LOC_AREA_4 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_4": ""}]
)
METERS_RS_EMPTY_LOC_AREA_5 = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"customer_location_area_5": ""}]
)
METERS_RS_EMPTY_LOC_AREA_4_AND_5 = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"customer_location_area_5": "", "customer_location_area_4": ""}],
)
METERS_RS_CORRECT_REGION = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[
        {"customer_location_area_1": "Northern", "customer_location_area_2": "Foo"}
    ],
)
METERS_RS_CORRECT_DISTRICT = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"customer_location_area_2": "Nabilatuk"}],
)
METERS_RS_CORRECT_GEO = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "Nabilatuk",
            "customer_location_area_1": "Northern",
        }
    ],
)
METERS_RS_CORRECT_GEO_CASE_INSENSITIVE = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "nabilatuk",
            "customer_location_area_1": "NORTHERN",
        }
    ],
)
METERS_RS_CORRECT_GEO_ALL_LOWERCASE = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "madi-okollo",
            "customer_location_area_1": "northern",
        }
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result", "detail_count"),
    [
        ([METERS_RS], False, 39),
        ([METERS_RS_NO_NAME], False, 67),
        ([METERS_RS_NO_ID_NUMBER], False, 72),
        ([METERS_RS_NO_PHONE], False, 68),
        ([METERS_RS_NO_LOC_AREA_1], False, 72),
        ([METERS_RS_NO_LOC_AREA_2], False, 76),
        ([METERS_RS_NO_LOC_AREA_3], False, 76),
        ([METERS_RS_NO_LOC_AREA_4], False, 76),
        ([METERS_RS_NO_LOC_AREA_5], False, 76),
        ([METERS_RS_NO_LOC_AREA_4_AND_5], False, 113),
        ([METERS_RS_EMPTY_NAME], False, 67),
        ([METERS_RS_EMPTY_ID_NUMBER], False, 72),
        ([METERS_RS_EMPTY_PHONE], False, 68),
        ([METERS_RS_EMPTY_LOC_AREA_1], False, 72),
        ([METERS_RS_EMPTY_LOC_AREA_2], False, 76),
        ([METERS_RS_EMPTY_LOC_AREA_3], False, 76),
        ([METERS_RS_EMPTY_LOC_AREA_4], False, 76),
        ([METERS_RS_EMPTY_LOC_AREA_5], False, 76),
        ([METERS_RS_EMPTY_LOC_AREA_4_AND_5], False, 113),
        ([METERS_RS_CORRECT_REGION], False, 46),
        ([METERS_RS_CORRECT_DISTRICT], False, 39),
        ([METERS_RS_CORRECT_GEO], True, None),
        ([METERS_RS_CORRECT_GEO_CASE_INSENSITIVE], True, None),
        ([METERS_RS_CORRECT_GEO_ALL_LOWERCASE], True, None),
    ],
)
def test_subcheck__customer_data_is_complete(
    recordsets: list[RecordSet],
    result: bool,
    detail_count: int | None,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of missing customer data."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    check = subcheck__customer_data_is_complete(entity, meters_table, CHECK_NAME)
    assert check["result"] is result

    if detail_count is None:
        assert "details" not in check or check["details"] is None
    else:
        assert len(check.get("details", [])) == detail_count


METERS_RS_NO_PLAN_TYPE = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"payment_plan": None}]
)
METERS_RS_NO_PLAN_CURRENCY = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"payment_plan_currency": None}]
)
METERS_RS_NO_PLAN_DAYS = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"payment_plan_days": None}]
)

PRODUCT_PRICES_RS_OTHER_CATEGORY = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[{"category": "foo"}],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
METERS_RS_OTHER_PLAN_TYPE = RecordSetWithModifiers(
    METERS_TABLE, [METERS_SAMPLE_DICT], modifiers=[{"payment_plan": "foo"}]
)

PAYMENTS_RS_NO_AMOUNT = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE, [PAYMENTS_SAMPLE_DICT], modifiers=[{"amount": 0}]
)
PAYMENTS_RS_OTHER_ACCOUNT_UID = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE,
    [PAYMENTS_SAMPLE_DICT],
    modifiers=[{"account_uid": "foobar"}],
    apply_after=["account_uid"],
)
METERS_RS_NO_PLAN_DOWN_PAYMENT = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": None}],
)
PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE,
    [
        PAYMENTS_SAMPLE_DICT,
        {
            "account_origin": "meters",
            "account_external_id": "A74320525",
            "payment_external_id": "Q74320525",
            "paid_at": "2024-01-05 17:28:30",
            "amount": 10000,
            "currency": "UGX",
        },
    ],
)
METERS_RS_PAYMENTS_SUM_SUFFICIENT_TYPE_PAYGO = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": 60000}],
)
METERS_RS_DOWN_PAYMENT_EXCEEDING_PAYMENTS_SUM = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": 60001}],
)
PRODUCT_PRICES_RS_PAYMENTS_SUFFICIENT_TYPE_CASH = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[
        {
            "category": "foo",
            "base_subsidized_sales_price": 100000,
            "original_sales_price": 100001,
            "subsidy_adjustment": 10000,
        }
    ],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
PRODUCT_PRICES_RS_PAYMENTS_INSUFFICIENT_TYPE_CASH = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[
        {
            "category": "foo",
            "base_subsidized_sales_price": 100001,
            "original_sales_price": 100002,
            "subsidy_adjustment": 10000,
        }
    ],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([METERS_RS, PAYMENTS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS], True),
        (
            [
                METERS_RS_NO_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                METERS_RS_NO_PLAN_CURRENCY,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                METERS_RS_NO_PLAN_DAYS,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [METERS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            False,
        ),  # without payments
        (
            [
                METERS_RS,
                PAYMENTS_RS_NO_AMOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                METERS_RS,
                PAYMENTS_RS_OTHER_ACCOUNT_UID,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_OTHER_CATEGORY,
            ],
            True,
        ),
        (
            [
                METERS_RS_NO_PLAN_DOWN_PAYMENT,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                METERS_RS_PAYMENTS_SUM_SUFFICIENT_TYPE_PAYGO,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            True,
        ),
        (
            [
                METERS_RS_DOWN_PAYMENT_EXCEEDING_PAYMENTS_SUM,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
            ],
            False,
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_PAYMENTS_SUFFICIENT_TYPE_CASH,
            ],
            True,
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_PAYMENTS_INSUFFICIENT_TYPE_CASH,
            ],
            False,
        ),
    ],
)
def test_subcheck__payment_plan_is_complete(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of payment data."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    check = subcheck__payment_plan_is_complete(entity, meters_table, CHECK_NAME)
    assert check["result"] is result


PRODUCT_PRICES_RS_NEWER_VALIDITY = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[{"category": "foo"}, {"valid_from": "2024-01-02", "category": "foo"}],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)
PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[
        {"category": "foo"},
        {
            "valid_from": "2024-01-02",
            "original_sales_price": 150000,
            "base_subsidized_sales_price": 140000,
            "category": "foo",
        },
    ],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)

PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[
        {"category": "foo"},
        {
            "valid_from": "2024-07-02",
            "original_sales_price": 150000,
            "base_subsidized_sales_price": 140000,
            "category": "foo",
        },
    ],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_VALIDITY,
            ],
            True,
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT,
            ],
            False,
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT,
            ],
            True,
        ),
    ],
)
def test_subcheck__payment_plan_is_complete_two_prices(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test behavior when different prices are available."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]
    check = subcheck__payment_plan_is_complete(entity, meters_table, CHECK_NAME)
    assert check["result"] is result


PRODUCTS_RS_OTHER_MANUFACTURER = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"manufacturer": "666"}]
)
PRODUCTS_RS_OTHER_MODEL = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"model": "666"}]
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS], True),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], False),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MANUFACTURER], False),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MODEL], False),
    ],
)
def test_subcheck__product_is_on_white_list(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of products and claim templates."""
    inserts = prepare_test_data_map_rbf_category(
        recordsets=recordsets, import_context=import_context
    )
    entity = inserts[METERS_TABLE][0]

    check = subcheck__product_is_on_white_list(entity, meters_table, CHECK_NAME)
    assert check["result"] is result


def test_subcheck__sale_is_not_older_than_three_months(
    import_context: ImportContext, meters_table
) -> None:
    """Test possible combinations of date 3 months off."""
    METERS_RS_PURCHASE_DEC24 = RecordSetWithModifiers(
        METERS_TABLE,
        [METERS_SAMPLE_DICT],
        modifiers=[{"purchase_date": date(2024, 12, 1)}],
    )
    inserts = prepare_test_data(
        recordsets=[METERS_RS_PURCHASE_DEC24], import_context=import_context
    )
    entity = inserts[METERS_TABLE][0]

    # assert a good sale is True
    with time_machine.travel("2024-12-31"):
        check = subcheck__sale_is_not_older_than_three_months(entity, meters_table)
        assert check["result"] is True
    # assert an old sale is False
    with time_machine.travel("2025-04-01"):
        check = subcheck__sale_is_not_older_than_three_months(entity, meters_table)
        assert check["result"] is False
    # assert a future sale is False
    with time_machine.travel("2024-11-30"):
        check = subcheck__sale_is_not_older_than_three_months(entity, meters_table)
        assert check["result"] is False
    # assert a sale before project launch @ 2024-11-01 is False
    with time_machine.travel("2023-12-31"):
        check = subcheck__sale_is_not_older_than_three_months(entity, meters_table)
        assert check["result"] is False


METERS_RS_1_REPOSSESSION = RecordSetWithModifiers(
    METERS_TABLE,
    2 * [METERS_SAMPLE_DICT],
    modifiers=[{}, {"customer_external_id": "6666", "purchase_date": date(2024, 1, 1)}],
)
METERS_RS_2_REPOSSESSION = RecordSetWithModifiers(
    METERS_TABLE,
    3 * [METERS_SAMPLE_DICT],
    modifiers=[
        {},
        {"customer_external_id": "5555", "purchase_date": date(2023, 12, 5)},
        {"customer_external_id": "6666", "purchase_date": date(2024, 1, 1)},
    ],
)
METERS_RS_2_REPOSSESSION_DIFFERENT_SERIAL_NUMBER = RecordSetWithModifiers(
    METERS_TABLE,
    3 * [METERS_SAMPLE_DICT],
    modifiers=[
        {},
        {
            "customer_external_id": "5555",
            "purchase_date": date(2023, 12, 15),
        },
        {
            "customer_external_id": "6666",
            "purchase_date": date(2024, 1, 1),
            "serial_number": "6666",
        },
    ],
)
METERS_RS_2_REPOSSESSION_DIFFERENT_MANUFACTURER = RecordSetWithModifiers(
    METERS_TABLE,
    3 * [METERS_SAMPLE_DICT],
    modifiers=[
        {},
        {
            "customer_external_id": "5555",
            "purchase_date": date(2023, 12, 15),
        },
        {
            "customer_external_id": "6666",
            "purchase_date": date(2024, 1, 1),
            "manufacturer": "6666",
        },
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([METERS_RS], True),
        ([METERS_RS_1_REPOSSESSION], True),
        ([METERS_RS_2_REPOSSESSION], False),
        ([METERS_RS_2_REPOSSESSION_DIFFERENT_SERIAL_NUMBER], True),
        ([METERS_RS_2_REPOSSESSION_DIFFERENT_MANUFACTURER], True),
    ],
)
def test_subcheck__entity_is_repossessed_at_most_once(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible repossession possibilities."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    check = subcheck__entity_is_repossessed_at_most_once(entity, meters_table)
    assert check["result"] == result


PRODUCTS_RS_NO_TECHNOLOGY = RecordSetWithModifiers(
    PRODUCTS_TABLE,
    [PRODUCTS_DICT],
    modifiers=[{"technology": None}],
    id_adjustments=[{"rbf_claim_template_id": (CLAIM_TEMPLATES_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS], "device type A"),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], None),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MANUFACTURER], None),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MODEL], None),
        ([METERS_RS, CLAIM_RS, PRODUCTS_RS_NO_TECHNOLOGY], None),
    ],
)
def test__get_technology(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of products and claim templates."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    check = get_technology(entity, meters_table, CHECK_NAME)
    assert check["technology"] == result


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [METERS_RS],
            {
                "incentive__amount": 0,
                "incentive__type": "unknown region NORTHERY",
                "result": False,
            },
        ),
        (
            [METERS_RS_CORRECT_REGION],
            {
                "incentive__amount": 183000,
                "incentive__type": "Northern/Foo/Only Regional",
            },
        ),
        (
            [METERS_RS_CORRECT_DISTRICT],
            {
                "incentive__amount": 0,
                "incentive__type": "unknown region NORTHERY",
                "result": False,
            },
        ),
        (
            [METERS_RS_CORRECT_GEO],
            {
                "incentive__amount": 255000,
                "incentive__type": "Northern/Nabilatuk/Karamoja",
            },
        ),
        (
            [METERS_RS_CORRECT_GEO_CASE_INSENSITIVE],
            {
                "incentive__amount": 255000,
                "incentive__type": "Northern/Nabilatuk/Karamoja",
            },
        ),
        (
            [METERS_RS_CORRECT_GEO_ALL_LOWERCASE],
            {
                "incentive__amount": 203000,
                "incentive__type": "Northern/Madi-Okollo/Rhd",
            },
        ),
    ],
)
def test_calculate_incentive(
    recordsets: list[RecordSet],
    result: dict,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of incentive calculation failures."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    assert calculate_incentive(entity, INCENTIVES, meters_table) == result


METERS_RS_FEMALE_BOOL = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"is_female_owned": True}],
)
METERS_RS_FEMALE_STRING = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"is_female_owned": "True"}],
)
METERS_RS_FEMALE_INT = RecordSetWithModifiers(
    METERS_TABLE,
    [METERS_SAMPLE_DICT],
    modifiers=[{"is_female_owned": 1}],
)
PRODUCTS_PRICES_RS_PRICE_NOT_VALID = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[{"valid_from": "2024-01-10"}],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "subsidy_adjustments", "result"),
    [
        (
            [METERS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            None,
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 10000,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__category": "A",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 90000,
            },
        ),
        (
            [METERS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            SUBSIDY_ADJUSTMENTS,
            {
                "subsidy__amount": 10000,
                "subsidy__category": "A",
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__subsidized_sales_price": 90000,
                "subsidy__price_id": ANY,
            },
        ),
        (
            [METERS_RS_FEMALE_BOOL, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            SUBSIDY_ADJUSTMENTS,
            {
                "subsidy__aggregated_subsidy_adjustment": 50000,
                "subsidy__amount": 60000,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__category": "A/female_ownership",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 40000,
            },
        ),
        (
            [METERS_RS_FEMALE_STRING, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            SUBSIDY_ADJUSTMENTS,
            {
                "subsidy__aggregated_subsidy_adjustment": 50000,
                "subsidy__amount": 60000,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__category": "A/female_ownership",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 40000,
            },
        ),
        (
            [METERS_RS_FEMALE_INT, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            SUBSIDY_ADJUSTMENTS,
            {
                "subsidy__aggregated_subsidy_adjustment": 50000,
                "subsidy__amount": 60000,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__category": "A/female_ownership",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 40000,
            },
        ),
        (
            [METERS_RS_FEMALE_BOOL, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            None,
            {
                "subsidy__amount": 10000,
                "subsidy__category": "A",
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__subsidized_sales_price": 90000,
                "subsidy__price_id": ANY,
            },
        ),
        (
            [METERS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCTS_PRICES_RS_PRICE_NOT_VALID],
            None,
            {
                "result": False,
                "subsidy__amount": 0,
                "subsidy__error": "No valid price found for the combination of purchase"
                " date, sales mode and product.",
            },
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT,
            ],
            None,
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 10000,
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__category": "A",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 90000,
            },
        ),
        (
            [
                METERS_RS_OTHER_PLAN_TYPE,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT,
            ],
            None,
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 10000,
                "subsidy__base_subsidized_sales_price": 140000,
                "subsidy__category": "A",
                "subsidy__original_sales_price": 150000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__price_id": ANY,
                "subsidy__subsidized_sales_price": 140000,
            },
        ),
    ],
)
def test_calculate_subsidy(
    recordsets: list[RecordSet],
    subsidy_adjustments: dict | None,
    result: dict,
    import_context: ImportContext,
    meters_table,
) -> None:
    """Test all possible combinations of subsidy calculation failures."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[METERS_TABLE][0]

    with time_machine.travel(datetime(2024, 3, 31, tzinfo=UTC), tick=False):
        assert (
            calculate_subsidy(entity, meters_table, CHECK_NAME, subsidy_adjustments)
            == result
        )
