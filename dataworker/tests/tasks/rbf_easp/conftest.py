from copy import deepcopy
from datetime import date
from typing import Any

import pytest
from dateutil.relativedelta import relativedelta
from sqlalchemy import Table

from dataworker import logger
from dataworker.database import get_or_create_connection, get_table
from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from dataworker.task import prepare_for_storage
from tasks.internal.rbf_easp.common import METERS_TABLE, PRODUCTS_TABLE, SHS_TABLE
from tests.utils import RecordSetWithModifiers, load_test_data, prepare_test_data

DATETIMES = {
    "created_at": "2024-03-14T20:19:42",
    "updated_at": "2024-03-14T20:19:42",
}

EASP_VALUES = ["OGS", "PUE", "CCS"]


@pytest.fixture
def _easp_organization_groups(_database, import_context):
    inserts = [
        get_table("organization_group_members")
        .insert()
        .values(
            {
                "id": "1",
                "user_id": "1",
                "organization_id": import_context.organization_id,
                "organization_group_id": "1",
            }
            | DATETIMES
        ),
        get_table("organization_groups")
        .insert()
        .values(
            {
                "id": "1",
                "user_id": "1",
                "name": "EASP",
            }
            | DATETIMES
        ),
    ]
    _ = [get_or_create_connection().execute(i) for i in inserts]


RBF_PRODUCTS_BASE = {"organization_id": "222222"} | DATETIMES
RBF_PRODUCT_PRICES_BASE = {
    "valid_from": "2023-01-01",
    "base_subsidy_amount": 10000,
} | DATETIMES
RBF_CLAIM_TEMPLATE = {
    "user_id": "1",
    "verifying_organization_id": "1",
    "managing_organization_id": "1",
    "supervising_organization_id": "1",
    "organization_group_id": "1",
    "currency": "EUR",
} | DATETIMES


@pytest.fixture
def _populate_rbf_helper_tables(_easp_organization_groups):
    for easp_value in EASP_VALUES:
        lower = easp_value.lower()
        RBF_CLAIM_TEMPLATE["name"] = easp_value
        RBF_CLAIM_TEMPLATE["trust_trace_check"] = f"rbf_easp_{lower}_is_eligible"
        claim_templates = get_table("rbf_claim_templates")
        claim_template_id = (
            get_or_create_connection()
            .execute(
                claim_templates.insert()
                .values(RBF_CLAIM_TEMPLATE)
                .returning(claim_templates.c.id)
            )
            .scalar_one()
        )
        RBF_PRODUCTS_BASE["rbf_claim_template_id"] = claim_template_id
        RBF_PRODUCTS_BASE["manufacturer"] = f"{easp_value} Company A"
        products = get_table("rbf_products")
        product_insert_statement = (
            products.insert()
            .values([
                RBF_PRODUCTS_BASE
                | {
                    "model": f"{easp_value} Device A",
                    "rbf_category": "Ai" if easp_value == "CCS" else "A",
                    "rated_power_w": "10",
                    "technology": "device type A",
                },
                RBF_PRODUCTS_BASE
                | {
                    "model": f"{easp_value} Device B",
                    "rbf_category": "Ai" if easp_value == "CCS" else "A",
                    "rated_power_w": "20",
                    "technology": "device type B",
                },
                RBF_PRODUCTS_BASE
                | {
                    "model": f"{easp_value} Device C",
                    "rbf_category": "X",
                    "rated_power_w": "30",
                    "technology": "device type C",
                },
            ])
            .returning(products.c.id)
        )
        product_ids = (
            get_or_create_connection().execute(product_insert_statement).scalars().all()
        )
        product_prices_tables = get_table("rbf_product_prices")
        product_prices_insert_statement = product_prices_tables.insert().values([
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[0],
                "original_sales_price": 100000,
                "base_subsidized_sales_price": 90000,
                "category": "paygo",
            },
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[0],
                "original_sales_price": 100000,
                "base_subsidized_sales_price": 90000,
                "category": "foo",
            },
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[1],
                "original_sales_price": 100000,
                "base_subsidized_sales_price": 90000,
                "category": "paygo",
            },
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[1],
                "original_sales_price": 100000,
                "base_subsidized_sales_price": 90000,
                "category": "foo",
            },
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[2],
                "original_sales_price": 1000000,
                "base_subsidized_sales_price": 990000,
                "category": "paygo",
            },
            RBF_PRODUCT_PRICES_BASE
            | {
                "rbf_product_id": product_ids[2],
                "original_sales_price": 1000000,
                "base_subsidized_sales_price": 990000,
                "category": "foo",
            },
        ])
        get_or_create_connection().execute(product_prices_insert_statement)


# OGS
@pytest.fixture(scope="session")
def ogs_shs_data(import_context, _setup_test_database) -> RecordSet:
    """Load shs test data from file."""
    file_data = RecordSet(
        "data_shs",
        load_test_data("records_for_task_test/rbf_easp_ogs_shs_sample.json"),
    )
    return prepare_for_storage(file_data, import_context=import_context)


@pytest.fixture(scope="session")
def ogs_payments_ts_data(import_context, _setup_test_database) -> RecordSet:
    """Load payment test data from file."""
    file_data = RecordSet(
        "data_payments_ts",
        load_test_data("records_for_task_test/rbf_easp_ogs_payments_ts_sample.json"),
    )
    return prepare_for_storage(file_data, import_context=import_context)


# PUE
@pytest.fixture(scope="session")
def pue_meters_data(import_context, _setup_test_database) -> RecordSet:
    """Load meters test data from file."""
    file_data = RecordSet(
        "data_meters",
        load_test_data("records_for_task_test/rbf_easp_pue_meters_sample.json"),
    )
    return prepare_for_storage(file_data, import_context=import_context)


# CCS
@pytest.fixture(scope="session")
def ccs_meters_data(import_context, _setup_test_database) -> RecordSet:
    """Load meters test data from file."""
    file_data = RecordSet(
        "data_meters",
        load_test_data("records_for_task_test/rbf_easp_ccs_meters_sample.json"),
    )
    return prepare_for_storage(file_data, import_context=import_context)


# PUE - CCS
@pytest.fixture(scope="session")
def ccs_pue_payments_ts_data(import_context, _setup_test_database) -> RecordSet:
    """Load payment test data for CCS or PUE from file."""
    file_data = RecordSet(
        "data_payments_ts",
        load_test_data(
            "records_for_task_test/rbf_easp_ccs_pue_payments_ts_sample.json"
        ),
    )
    return prepare_for_storage(file_data, import_context=import_context)


@pytest.fixture(scope="session")
def meters_table(_setup_test_database) -> Table:
    """Resolves string to data_meters sqlalchemy Table object."""
    return get_table("data_meters")


@pytest.fixture(scope="session")
def shs_table(_setup_test_database) -> Table:
    """Resolves string to data_shs sqlalchemy Table object."""
    return get_table("data_shs")


def get_modified_sample_by_delta(_dict: dict, delta: relativedelta) -> dict:
    """Return a modified sample fixture by year quantity.

    Also changes the customer_external_id to avoid uid duplicates.
    """
    sample = deepcopy(_dict)
    sample["customer_external_id"] += f"{delta.years:+}Y{delta.days:+}"
    old_date = date.fromisoformat(sample["purchase_date"])
    new_date = old_date + delta
    sample["purchase_date"] = new_date.isoformat()
    return sample


def prepare_test_data_map_rbf_category(
    recordsets: list[RecordSetWithModifiers],
    import_context: ImportContext,
) -> dict[str, list[dict[str, Any]]]:
    """Prepare and insert data, returning it with rbf_category."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    if METERS_TABLE in inserts:
        table = METERS_TABLE
    elif SHS_TABLE in inserts:
        table = SHS_TABLE
    else:
        logger.warning("Neither {METERS_TABLE} nor {SHS_TABLE} found in {inserts}")
        return inserts
    entity = inserts[table][-1]
    keys_to_match = ["manufacturer", "model", "organization_id"]
    matching_product = {}
    for product in inserts[PRODUCTS_TABLE]:
        if all(product[key] == entity[key] for key in keys_to_match):
            matching_product = product
            break
    entity["rbf_category"] = matching_product.get("rbf_category")
    return inserts
