from copy import copy, deepcopy

import pytest

from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from tasks.internal.rbf_easp.ccs_is_eligible import CHECK_NAME as CCS_CHECK_NAME
from tasks.internal.rbf_easp.common import (
    CLAIM_TEMPLATES_TABLE,
    PAYMENTS_TS_TABLE,
    SHS_TABLE,
    VERIFICATIONS_TABLE,
    get_unique_categories,
    get_unverified_entities,
    invalidate_verification_not_unique,
    post_check_uniqueness_inside_verification_batch,
)
from tasks.internal.rbf_easp.ogs_is_eligible import CHECK_NAME as OGS_CHECK_NAME
from tasks.internal.rbf_easp.pue_is_eligible import CHECK_NAME as PUE_CHECK_NAME
from tests.tasks.rbf_easp.test_ccs_is_eligible_task import (
    CLAIM_TEMPLATE_DICT as CLAIM_TEMPLATE_DICT_CCS,
)
from tests.tasks.rbf_easp.test_ogs_is_eligible_task import (
    CLAIM_RS,
    PAYMENTS_RS,
    PAYMENTS_SAMPLE_DICT,
    PRODUCTS_RS,
    SHS_RS,
    SHS_SAMPLE_UID,
    VERIFICATIONS_TEMPLATE_DICT,
)
from tests.tasks.rbf_easp.test_ogs_is_eligible_task import (
    CLAIM_TEMPLATE_DICT as CLAIM_TEMPLATE_DICT_OGS,
)
from tests.tasks.rbf_easp.test_pue_is_eligible_task import (
    CLAIM_TEMPLATE_DICT as CLAIM_TEMPLATE_DICT_PUE,
)
from tests.utils import RecordSetWithModifiers, prepare_test_data


@pytest.fixture(autouse=True)
def _module_autouse(_database):
    """Common fixtures required for this module."""
    ...


CLAIM_RS_OTHER_ORGA = RecordSetWithModifiers(
    CLAIM_TEMPLATES_TABLE,
    [CLAIM_TEMPLATE_DICT_OGS],
    modifiers=[{"organization_group_id": "99"}],
)
CLAIM_RS_OTHER_CHECK = RecordSetWithModifiers(
    CLAIM_TEMPLATES_TABLE,
    [CLAIM_TEMPLATE_DICT_OGS],
    modifiers=[{"trust_trace_check": "foobar"}],
)
CLAIM_RS_ALL_THREE_PROGRAMS = RecordSetWithModifiers(
    CLAIM_TEMPLATES_TABLE,
    [CLAIM_TEMPLATE_DICT_CCS, CLAIM_TEMPLATE_DICT_PUE, CLAIM_TEMPLATE_DICT_OGS],
)
VERIFICATIONS_RS_TRUE = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    id_adjustments=[{"subject_uid": (SHS_TABLE, "uid")}],
)
VERIFICATIONS_RS_FALSE = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    modifiers=[{"result": False}],
    id_adjustments=[{"subject_uid": (SHS_TABLE, "uid")}],
)
VERIFICATIONS_RS_TRUE_OTHER_CHECK = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    modifiers=[{"check": "foobar"}],
    id_adjustments=[{"subject_uid": (SHS_TABLE, "uid")}],
)


@pytest.mark.parametrize(
    ("recordsets", "count"),
    [
        ([SHS_RS, CLAIM_RS], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], 1),
        ([SHS_RS, CLAIM_RS_OTHER_ORGA, PRODUCTS_RS], 0),
        ([SHS_RS, CLAIM_RS_OTHER_CHECK, PRODUCTS_RS], 0),
        ([SHS_RS, CLAIM_RS_ALL_THREE_PROGRAMS, PRODUCTS_RS], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_TRUE], 0),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_FALSE], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_TRUE_OTHER_CHECK], 1),
    ],
)
@pytest.mark.usefixtures("_easp_organization_groups")
def test_get_unverified_entities_all(
    recordsets: list[RecordSet],
    count: int,
    import_context: ImportContext,
    shs_table,
) -> None:
    """Testing the functionality only once with OGS data."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    entities = get_unverified_entities(table=shs_table, check_name=OGS_CHECK_NAME)
    assert len(entities) == count
    entities = get_unverified_entities(table=shs_table, check_name=CCS_CHECK_NAME)
    assert len(entities) == 0
    entities = get_unverified_entities(table=shs_table, check_name=PUE_CHECK_NAME)
    assert len(entities) == 0


@pytest.mark.parametrize(
    ("recordsets", "count"),
    [
        ([SHS_RS, CLAIM_RS], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], 1),
        ([SHS_RS, CLAIM_RS_OTHER_ORGA, PRODUCTS_RS], 0),
        ([SHS_RS, CLAIM_RS_OTHER_CHECK, PRODUCTS_RS], 0),
        ([SHS_RS, CLAIM_RS_ALL_THREE_PROGRAMS, PRODUCTS_RS], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_TRUE], 0),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_FALSE], 1),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_TRUE_OTHER_CHECK], 1),
    ],
)
@pytest.mark.usefixtures("_easp_organization_groups")
def test_get_unverified_entities_from_import(
    recordsets: list[RecordSet],
    count: int,
    import_context: ImportContext,
    shs_table,
) -> None:
    """Testing the functionality only once with OGS data."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    entities = get_unverified_entities(
        table=shs_table,
        check_name=OGS_CHECK_NAME,
        import_id=int(import_context.import_id),
    )
    assert len(entities) == count

    entities = get_unverified_entities(
        table=shs_table,
        check_name=CCS_CHECK_NAME,
        import_id=int(import_context.import_id),
    )
    assert len(entities) == 0

    entities = get_unverified_entities(
        table=shs_table,
        check_name=PUE_CHECK_NAME,
        import_id=int(import_context.import_id),
    )
    assert len(entities) == 0

    # check that asking from another import_id fails
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=-1
    )
    assert entities == []


PAYMENTS_RS_VALID_OTHER_ACCOUNT_UID = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE,
    [PAYMENTS_SAMPLE_DICT],
    modifiers=[{"account_uid": "foobar", "paid_at": "2024-06-05 16:28:30"}],
    apply_after=["account_uid"],
)


@pytest.mark.usefixtures("_easp_organization_groups")
def test_get_unverified_entities_from_import_by_path(import_context, shs_table) -> None:
    """Add data through various imports and check the correct entities are returned."""
    # Add data qualified as unverified in one import
    base_import_id = int(import_context.import_id)
    prepare_test_data(
        recordsets=[SHS_RS, CLAIM_RS_ALL_THREE_PROGRAMS, PRODUCTS_RS],
        import_context=import_context,
    )
    # Add payment to the same account in another import
    payment_import_id = 888888
    assert payment_import_id != base_import_id
    payment_import_context = copy(import_context)
    payment_import_context.import_id = payment_import_id
    prepare_test_data(recordsets=[PAYMENTS_RS], import_context=payment_import_context)
    # Add payment to another account in yet another import
    payment_other_import_id = 888889
    assert payment_import_id != base_import_id != payment_other_import_id
    payment_other_import_context = copy(import_context)
    payment_other_import_context.import_id = payment_other_import_id
    prepare_test_data(
        recordsets=[PAYMENTS_RS_VALID_OTHER_ACCOUNT_UID],
        import_context=payment_other_import_context,
    )

    # Base import gets returned
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=base_import_id
    )
    assert len(entities) == 1
    assert entities[0]["uid"] == SHS_SAMPLE_UID
    # Payment to same account returns the same
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=payment_import_id
    )
    assert len(entities) == 1
    assert entities[0]["uid"] == SHS_SAMPLE_UID
    # Payment to other account returns nothing
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=payment_other_import_id
    )
    assert entities == []

    # Now add verification and verify previous returning queries return nothing
    verification_import_id = 888880
    verification_import_context = copy(import_context)
    verification_import_context.import_id = verification_import_id
    prepare_test_data(
        recordsets=[VERIFICATIONS_RS_TRUE], import_context=verification_import_context
    )

    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=base_import_id
    )
    assert entities == []
    # Payment to same account returns the same
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=payment_import_id
    )
    assert entities == []
    # Payment to other account returns nothing
    entities = get_unverified_entities(
        table=shs_table, check_name=OGS_CHECK_NAME, import_id=payment_other_import_id
    )
    assert entities == []


@pytest.mark.parametrize(
    ("entity", "check_name", "result", "log_msg"),
    [
        ({"rbf_category": None}, CCS_CHECK_NAME, None, None),
        ({"rbf_category": "X"}, CCS_CHECK_NAME, "x", "unexpected category X"),
        # confirming case insensitivity.
        ({"rbf_category": "a"}, OGS_CHECK_NAME, "a", None),
        ({"rbf_category": "A"}, OGS_CHECK_NAME, "a", None),
        ({"rbf_category": "A"}, PUE_CHECK_NAME, "a", None),
        (
            {"rbf_category": "Ai"},
            CCS_CHECK_NAME,
            (
                "ai",
                "aii",
                "aiii",
            ),
            None,
        ),
        (
            {"rbf_category": "B"},
            CCS_CHECK_NAME,
            (
                "b",
                "c",
                "d",
                "ei",
                "eii",
                "f",
            ),
            None,
        ),
    ],
)
def test_get_unique_categories(entity, check_name, result, log_msg, caplog):
    categories = get_unique_categories(entity, check_name)
    assert categories == result
    if not log_msg:
        assert not caplog.text
    else:
        assert log_msg in caplog.text


def test_invalidate_verification_not_unique():
    verification = {
        "result": True,
        "subcheck__customer_data_is_unique": {"result": True},
        "subcheck__other": {"result": True},
        "incentive__amount": 13000,
        "incentive__type": "Northern/Madi-Okollo/RHD/Ai",
        "subsidy__amount": 10000,
        "subsidy__category": "Ai",
    }
    invalidate_verification_not_unique(verification, "other_uid")
    assert "incentive__amount" not in verification
    assert "incentive__type" not in verification
    assert "subsidy__amount" not in verification
    assert "subsidy__category" not in verification
    assert verification["result"] is False
    assert verification["subcheck__other"]["result"] is True  # unchanged
    assert verification["subcheck__customer_data_is_unique"]["result"] is False
    assert "other_uid" in verification["subcheck__customer_data_is_unique"]["details"]


ENTITY_ID_1_CAT_B = {"uid": "1_B", "customer_id_number_p": "1", "rbf_category": "B"}
VERIFICATION_1B_TRUE = {"subject_uid": "1_B", "result": True}
ENTRY_ID_2_CAT_B = {"uid": "2_B", "customer_id_number_p": "2", "rbf_category": "B"}
VERIFICATION_2B_TRUE = {"subject_uid": "2_B", "result": True}
VERIFICATION_2B_FALSE = {"subject_uid": "2_B", "result": False}
ENTRY_ID_2_CAT_C = {"uid": "2_C", "customer_id_number_p": "2", "rbf_category": "C"}
VERIFICATION_2C_TRUE = {"subject_uid": "2_C", "result": True}
ENTRY_ID_2_CAT_AI = {"uid": "2_Ai", "customer_id_number_p": "2", "rbf_category": "Ai"}
VERIFICATION_2AI_TRUE = {"subject_uid": "2_Ai", "result": True}
VERIFICATION_2B_CONFLICT_2B = {
    "result": False,
    "subcheck__customer_data_is_unique": {
        "details": "customer&category is not unique (conflicting uid: 2_B).",
        "result": False,
    },
    "subject_uid": "2_B",
}
VERIFICATION_2C_CONFLICT_2B = VERIFICATION_2B_CONFLICT_2B | {"subject_uid": "2_C"}


@pytest.mark.parametrize(
    ("entities", "verifications", "check_name", "results"),
    [
        # different id_number
        (
            [ENTITY_ID_1_CAT_B, ENTRY_ID_2_CAT_B],
            [VERIFICATION_1B_TRUE, VERIFICATION_2B_TRUE],
            PUE_CHECK_NAME,
            None,
        ),
        # same id_number and same category, only if both verified -> invalidate
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_B],
            [VERIFICATION_2B_TRUE, deepcopy(VERIFICATION_2B_TRUE)],
            OGS_CHECK_NAME,
            [VERIFICATION_2B_TRUE, VERIFICATION_2B_CONFLICT_2B],
        ),
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_B],
            [VERIFICATION_2B_TRUE, VERIFICATION_2B_FALSE],
            OGS_CHECK_NAME,
            None,
        ),
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_B],
            [VERIFICATION_2B_FALSE, VERIFICATION_2B_TRUE],
            OGS_CHECK_NAME,
            None,
        ),
        # same id_number but different category
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_C],
            [VERIFICATION_2B_TRUE, VERIFICATION_2C_TRUE],
            OGS_CHECK_NAME,
            None,
        ),
        # same id_number but different category grouping for CCS -> invalidate
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_C],
            [VERIFICATION_2B_TRUE, deepcopy(VERIFICATION_2C_TRUE)],
            CCS_CHECK_NAME,
            [VERIFICATION_2B_TRUE, VERIFICATION_2C_CONFLICT_2B],
        ),
        # same id_number but different category not grouping for CCS
        (
            [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_AI],
            [VERIFICATION_2B_TRUE, VERIFICATION_2AI_TRUE],
            CCS_CHECK_NAME,
            None,
        ),
    ],
)
def test_post_check_uniqueness_inside_verification_batch(
    entities, verifications, check_name, results
):
    # simplify test setup when no change/invalidation is expected
    if results is None:
        results = deepcopy(verifications)

    adapted_verifications = post_check_uniqueness_inside_verification_batch(
        entities, verifications, check_name
    )
    assert adapted_verifications == results


def test_post_check_uniqueness_inside_verification_batch_error_logs(caplog):
    verifications = [VERIFICATION_2B_TRUE, VERIFICATION_2C_TRUE]
    adapted_verifications = post_check_uniqueness_inside_verification_batch(
        [ENTRY_ID_2_CAT_B, ENTRY_ID_2_CAT_B],
        verifications,
        OGS_CHECK_NAME,
    )
    assert adapted_verifications == verifications
    assert len(caplog.records) == 1
    log_record = caplog.records[0]
    assert log_record.levelname == "ERROR"
    assert log_record.msg == "entity uid 2_B does not match verification uid 2_C"
