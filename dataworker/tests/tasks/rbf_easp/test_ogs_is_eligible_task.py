from copy import deepcopy
from datetime import UTC, date, datetime
from unittest.mock import ANY

import pytest
import time_machine
from dateutil.relativedelta import relativedelta

from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from dataworker.queries import table_content
from dataworker.task import InternalTask
from tasks.internal.rbf_easp.common import (
    CLAIM_TEMPLATES_TABLE,
    PAYMENTS_TS_TABLE,
    PRODUCT_PRICES_TABLE,
    PRODUCTS_TABLE,
    SHS_TABLE,
    VERIFICATIONS_TABLE,
    calculate_incentive,
    calculate_subsidy,
    get_rated_power,
    get_technology,
)
from tasks.internal.rbf_easp.ogs_is_eligible import (
    CHECK_NAME,
    INCENTIVES,
    run,
)
from tasks.internal.rbf_easp.subchecks import (
    subcheck__customer_data_is_complete,
    subcheck__customer_data_is_unique,
    subcheck__entity_is_repossessed_at_most_once,
    subcheck__payment_plan_is_complete,
    subcheck__product_is_on_white_list,
    subcheck__sale_is_not_older_than_three_months,
)
from tests.tasks.rbf_easp.conftest import (
    get_modified_sample_by_delta,
    prepare_test_data_map_rbf_category,
)
from tests.utils import (
    RecordSetWithModifiers,
    insert_records,
    insert_recordset,
    prepare_many_for_compare,
    prepare_test_data,
)


@pytest.fixture(autouse=True)
def _module_autouse(_database):
    """Common fixtures required for this module."""
    ...


@time_machine.travel(datetime(2025, 4, 1, 20, 19, 42, tzinfo=UTC), tick=False)
@pytest.mark.usefixtures("_populate_rbf_helper_tables")
def test__rbf_easp_ogs_is_eligible_task_from_json(ogs_shs_data, ogs_payments_ts_data):
    # --- DATA INGESTION ---
    insert_recordset(ogs_shs_data)
    insert_recordset(ogs_payments_ts_data)

    # --- TASK ---
    InternalTask.from_function(run)()

    # --- CHECK RESULT ---
    trust_trace = table_content(
        "data_trust_trace",
        shed_nulls=True,
        string_timestamps=True,
        shed_custom_paths=["subsidy__price_id"],
    )
    trust_traces = [
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "rated_power_w": 10,
                "technology": "device type A",
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No payments found.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {
                    "result": False,
                    "details": "NORTHERY administrative unit not found.",
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-05) "
                    "exceeds three months.",
                },
                "subcheck__customer_data_is_unique": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_B74320525_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_B74320525_222222_OGS Company "
            "A_D83209731_rbf_"
            "easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "subcheck__payment_plan_is_complete": {
                    "details": "No valid price found for the combination of "
                    "purchase "
                    "date, sales mode and product.",
                    "result": False,
                },  # different manufacturer.
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company B) and model (OGS Device "
                    "B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) "
                    "exceeds three months.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C23488192_222222_OGS Company B_D23488192",
            "uid": "shs_222222_solar_coop_C23488192_222222_OGS Company "
            "B_D23488192_rbf_"
            "easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "subcheck__payment_plan_is_complete": {
                    "details": "No valid price found for the combination of "
                    "purchase date, sales mode and product.",
                    "result": False,
                },  # different manufacturer.
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company C) and model (OGS Device "
                    "C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C26881862_222222_OGS Company C_D26881862",
            "uid": "shs_222222_solar_coop_C26881862_222222_OGS Company "
            "C_D26881862_rbf_"
            "easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "rated_power_w": 10,
                "technology": "device type A",
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "customer&category is not unique (conflicting uid: "
                    "222222_solar_coop_C74320525_222222_OGS Company A_D74320525).",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C74320525_222222_OGS Company A_D74320521",
            "uid": "shs_222222_solar_coop_C74320525_222222_OGS Company "
            "A_D74320521_rbf_easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "rated_power_w": 10,
                "technology": "device type A",
                "incentive__type": "Northern/Madi-Okollo/Rhd",
                "incentive__amount": 16000,
                "subsidy__amount": 10000,
                "subsidy__category": "A",
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__subsidized_sales_price": 90000,
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2025-04-01T20:19:42+00:00",
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "True",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C74320525_222222_OGS Company A_D74320525",
            "uid": "shs_222222_solar_coop_C74320525_222222_OGS Company "
            "A_D74320525_rbf_easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "rated_power_w": 10,
                "technology": "device type A",
                "subcheck__payment_plan_is_complete": {
                    "details": "No valid price found for the combination of "
                    "purchase date, sales mode and product.",
                    "result": False,
                },  # different manufacturer.
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {
                    "result": False,
                    "details": "More than one previous sale exists for the "
                    "manufacturer/serial combination.",
                },
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-15) "
                    "exceeds three months.",
                },
                "subcheck__customer_data_is_unique": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C83209731_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_C83209731_222222_OGS Company A_D83209731_"
            "rbf_easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "subcheck__payment_plan_is_complete": {
                    "details": "No valid price found for the combination of "
                    "purchase date, sales mode and product.",
                    "result": False,
                },  # different manufacturer.
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company B) and model (OGS Device "
                    "B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess "
                    "uniqueness",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C83687814_222222_OGS Company B_D83687814",
            "uid": "shs_222222_solar_coop_C83687814_222222_OGS Company "
            "B_D83687814_rbf_"
            "easp_ogs_is_eligible",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "custom": {
                "rated_power_w": 10,
                "technology": "device type A",
                "subcheck__payment_plan_is_complete": {
                    "details": "Payment sums for account not sufficient to cover "
                    "payment_plan_down_payment of 5000.0.",
                    "result": False,
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {
                    "result": True,
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "The purchase date (2023-11-05) can't preceed "
                    "2024-11-01.",
                },
                "subcheck__customer_data_is_unique": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_X74320525_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_X74320525_222222_OGS Company "
            "A_D83209731_rbf_easp_ogs_is_eligible",
        },
    ]

    assert prepare_many_for_compare(trust_trace) == trust_traces

    # make a new record that will fail for uniqueness in-DB check
    new_record = deepcopy(ogs_shs_data.records[0])
    new_record["purchase_date"] = "2025-01-07"
    new_record["customer_external_id"] += "N"
    new_record["customer_uid"] += "N"
    new_record["uid"] = f"{new_record['customer_uid']}_{new_record['device_uid']}"
    insert_records(ogs_shs_data.table, [new_record])
    # --- TASK ---
    InternalTask.from_function(run)()
    # --- CHECK RESULT ---
    trust_trace = table_content(
        "data_trust_trace",
        shed_nulls=True,
        string_timestamps=True,
        shed_custom_paths=["subsidy__price_id"],
        shed_last_ids=True,  # as this adds keys when recalculating failed verifications
    )
    new_record_trace = {
        "check": "rbf_easp_ogs_is_eligible",
        "custom": {
            "rated_power_w": 10,
            "subcheck__customer_data_is_complete": {"result": True},
            "subcheck__customer_data_is_unique": {
                "details": "customer&category is not unique (conflicting uid: "
                "222222_solar_coop_C74320525_222222_OGS Company A_D74320525).",
                "result": False,
            },
            "subcheck__entity_is_repossessed_at_most_once": {"result": True},
            "subcheck__payment_plan_is_complete": {"result": True},
            "subcheck__product_is_on_white_list": {"result": True},
            "subcheck__sale_is_not_older_than_three_months": {"result": True},
            "technology": "device type A",
        },
        "data_origin": "solar_coop",
        "import_id": 111111,
        "organization_id": 222222,
        "result": "False",
        "source_id": 333333,
        "subject_origin": "shs",
        "subject_uid": "222222_solar_coop_C74320525N_222222_OGS Company A_D74320525",
        "uid": "shs_222222_solar_coop_C74320525N_222222_OGS Company "
        "A_D74320525_rbf_easp_ogs_is_eligible",
    }
    trust_traces.insert(3, new_record_trace)
    assert prepare_many_for_compare(trust_trace) == trust_traces


######

PAYMENTS_SAMPLE_DICT = {
    "account_origin": "shs",
    "account_external_id": "A74320525",
    "payment_external_id": "P74320525",
    "paid_at": "2024-01-05 16:28:30",
    "amount": 50000,
    "currency": "UGX",
}
SHS_SAMPLE_DICT = {
    "device_external_id": "D74320525",
    "account_external_id": "A74320525",
    "serial_number": "D74320525",
    "purchase_date": "2024-01-05",
    "manufacturer": "OGS Company A",
    "payment_plan_days": 730,
    "payment_plan_down_payment": 10000,
    "total_price": "",
    "payment_plan_type": "paygo",
    "payment_plan_currency": "UGX",
    "paid_off_date": "",
    "repossession_date": "",
    "customer_external_id": "C74320525",
    # TODO: we add these here for the tests, but we probably should not allow these to
    # be modified by the dataworker and then disable this restriction on tests
    "customer_id_number_e": "AB1234567DE89",
    "customer_id_number_p": "fancy_foobar",
    "customer_id_type": "NIN",
    "customer_name_e": "Londiwe Makhathini-Mbhodwe",
    "customer_name_p": "crypto-name",
    "customer_gender": "M",
    "customer_birth_year": 1996,
    "customer_country": "UG",
    "customer_latitude_e": 1.63157694739998,
    "customer_longitude_e": 32.8350548033075,
    "customer_phone_e": "+256 2817098278",
    "customer_phone_p": "crypto-phone-number",
    "customer_location_area_1": "NORTHERY",
    "customer_location_area_2": "AMOLATAR",
    "customer_location_area_3": "KIOGA COUNTY",
    "customer_location_area_4": "ACII",
    "customer_location_area_5": "ALAKU",
    "model": "OGS Device A",
}
SHS_SAMPLE_UID = "222222_solar_coop_C74320525_222222_OGS Company A_D74320525"
PRODUCTS_DICT = {
    "manufacturer": "OGS Company A",
    "rbf_claim_template_id": "1",
    "organization_id": "222222",
    "model": "OGS Device A",
    "rated_power_w": "10",
    "technology": "device type A",
    "rbf_category": "A",
}
PRODUCT_PRICES_DICT = {
    "rbf_product_id": "1",
    "original_sales_price": 50000,
    "base_subsidized_sales_price": 20000,
    "base_subsidy_amount": 30000,
    "category": "paygo",
    "valid_from": "2024-01-01",
}

CLAIM_TEMPLATE_DICT = {
    "user_id": "1",
    "name": "EASP OGS",
    "trust_trace_check": "rbf_easp_ogs_is_eligible",
    "verifying_organization_id": "1",
    "managing_organization_id": "1",
    "supervising_organization_id": "1",
    "organization_group_id": "1",
    "currency": "EUR",
}
VERIFICATIONS_TEMPLATE_DICT = {
    "check": CHECK_NAME,
    "subject_origin": "shs",
    "result": True,
    "subject_uid": "value-to-be-overwritten",
}
SHS_RS = RecordSetWithModifiers(SHS_TABLE, [SHS_SAMPLE_DICT])
PAYMENTS_RS = RecordSetWithModifiers(PAYMENTS_TS_TABLE, [PAYMENTS_SAMPLE_DICT])
PRODUCTS_RS = RecordSetWithModifiers(
    PRODUCTS_TABLE,
    [PRODUCTS_DICT],
    id_adjustments=[{"rbf_claim_template_id": (CLAIM_TEMPLATES_TABLE, "id")}],
)
PRODUCT_PRICES_RS = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
CLAIM_RS = RecordSetWithModifiers(CLAIM_TEMPLATES_TABLE, [CLAIM_TEMPLATE_DICT])
VERIFICATIONS_RS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    id_adjustments=[{"subject_uid": (SHS_TABLE, "uid")}],
)

SHS_RS_OTHER_ID = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"customer_external_id": "66666"}],
)
SHS_RS_MODEL_OTHER_CAT = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"model": "B1", "customer_external_id": "66"}],
)
SHS_RS_OTHER_ORGA = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"customer_external_id": "66666", "organization_id": 666}],
)
PRODUCTS_RS_OTHER_ORGA = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"organization_id": 666}]
)
PRODUCTS_RS_OTHER_CAT = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"model": "B1", "rbf_category": "B"}]
)
VERIFICATIONS_RS_FALSE = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_TEMPLATE_DICT],
    modifiers=[{"result": False}],
    id_adjustments=[{"subject_uid": (SHS_TABLE, "uid")}],
)

shs_sample_early = get_modified_sample_by_delta(
    SHS_SAMPLE_DICT, relativedelta(years=-2)
)
shs_sample_earlier = get_modified_sample_by_delta(
    SHS_SAMPLE_DICT, relativedelta(years=-2, days=-1)
)
shs_sample_late = get_modified_sample_by_delta(SHS_SAMPLE_DICT, relativedelta(years=2))
shs_sample_later = get_modified_sample_by_delta(
    SHS_SAMPLE_DICT, relativedelta(years=2, days=1)
)

SHS_LATE_RS = RecordSetWithModifiers(SHS_TABLE, [shs_sample_late])
SHS_EARLY_RS = RecordSetWithModifiers(SHS_TABLE, [shs_sample_early])
SHS_LATER_RS = RecordSetWithModifiers(SHS_TABLE, [shs_sample_later])
SHS_EARLIER_RS = RecordSetWithModifiers(SHS_TABLE, [shs_sample_earlier])


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS], False),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], True),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS], True),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS_FALSE], True),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], False),
        (
            [SHS_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, SHS_RS_OTHER_ID],
            False,
        ),
        (
            [
                SHS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS_FALSE,
                SHS_RS_OTHER_ID,
            ],
            True,
        ),
        (
            [
                SHS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS,
                SHS_RS_OTHER_ORGA,
                PRODUCTS_RS_OTHER_ORGA,
            ],
            False,
        ),
        (
            [
                SHS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                VERIFICATIONS_RS,
                SHS_RS_MODEL_OTHER_CAT,
                PRODUCTS_RS_OTHER_CAT,
            ],
            True,
        ),
        # Tests for two years range behavior
        (
            [SHS_LATE_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, SHS_RS],
            False,
        ),
        (
            [SHS_EARLY_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, SHS_RS],
            False,
        ),
        (
            [SHS_LATER_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, SHS_RS],
            True,
        ),
        (
            [SHS_EARLIER_RS, CLAIM_RS, PRODUCTS_RS, VERIFICATIONS_RS, SHS_RS],
            True,
        ),
    ],
)
def test_subcheck__customer_data_is_unique(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of data for the same customer."""
    inserts = prepare_test_data_map_rbf_category(
        recordsets=recordsets, import_context=import_context
    )
    entity = inserts[SHS_TABLE][-1]

    # Also Test we have all data in DB with customer_id_number_e and _p
    for device in inserts[SHS_TABLE]:
        assert device["customer_id_number_e"] is not None
        assert device["customer_id_number_p"] is not None

    check = subcheck__customer_data_is_unique(entity, shs_table, CHECK_NAME)
    assert check["result"] is result


SHS_RS_NO_NAME = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_name_p": None}]
)
SHS_RS_NO_ID_NUMBER = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_id_number_p": None}]
)
SHS_RS_NO_PHONE = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_phone_p": None}]
)
SHS_RS_NO_LOC_AREA_1 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_1": None}]
)
SHS_RS_NO_LOC_AREA_2 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_2": None}]
)
SHS_RS_NO_LOC_AREA_3 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_3": None}]
)
SHS_RS_NO_LOC_AREA_4 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_4": None}]
)
SHS_RS_NO_LOC_AREA_5 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_5": None}]
)
SHS_RS_NO_LOC_AREA_4_AND_5 = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"customer_location_area_5": None, "customer_location_area_4": None}],
)
SHS_RS_EMPTY_NAME = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_name_p": ""}]
)
SHS_RS_EMPTY_ID_NUMBER = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_id_number_p": ""}]
)
SHS_RS_EMPTY_PHONE = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_phone_p": ""}]
)
SHS_RS_EMPTY_LOC_AREA_1 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_1": ""}]
)
SHS_RS_EMPTY_LOC_AREA_2 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_2": ""}]
)
SHS_RS_EMPTY_LOC_AREA_3 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_3": ""}]
)
SHS_RS_EMPTY_LOC_AREA_4 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_4": ""}]
)
SHS_RS_EMPTY_LOC_AREA_5 = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_5": ""}]
)
SHS_RS_EMPTY_LOC_AREA_4_AND_5 = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"customer_location_area_5": "", "customer_location_area_4": ""}],
)
SHS_RS_CORRECT_REGION = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[
        {"customer_location_area_1": "Northern", "customer_location_area_2": "Foo"}
    ],
)
SHS_RS_CORRECT_DISTRICT = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"customer_location_area_2": "Nabilatuk"}]
)
SHS_RS_CORRECT_GEO = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "Nabilatuk",
            "customer_location_area_1": "Northern",
        }
    ],
)
SHS_RS_CORRECT_GEO_CASE_INSENSITIVE = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "nabilatuk",
            "customer_location_area_1": "NORTHERN",
        }
    ],
)
SHS_RS_CORRECT_GEO_ALL_LOWERCASE = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[
        {
            "customer_location_area_2": "madi-okollo",
            "customer_location_area_1": "northern",
        }
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result", "detail_count"),
    [
        ([SHS_RS], False, 39),
        ([SHS_RS_NO_NAME], False, 67),
        ([SHS_RS_NO_ID_NUMBER], False, 72),
        ([SHS_RS_NO_PHONE], False, 68),
        ([SHS_RS_NO_LOC_AREA_1], False, 72),
        ([SHS_RS_NO_LOC_AREA_2], False, 76),
        ([SHS_RS_NO_LOC_AREA_3], False, 76),
        ([SHS_RS_NO_LOC_AREA_4], False, 76),
        ([SHS_RS_NO_LOC_AREA_5], False, 76),
        ([SHS_RS_NO_LOC_AREA_4_AND_5], False, 113),
        ([SHS_RS_EMPTY_NAME], False, 67),
        ([SHS_RS_EMPTY_ID_NUMBER], False, 72),
        ([SHS_RS_EMPTY_PHONE], False, 68),
        ([SHS_RS_EMPTY_LOC_AREA_1], False, 72),
        ([SHS_RS_EMPTY_LOC_AREA_2], False, 76),
        ([SHS_RS_EMPTY_LOC_AREA_3], False, 76),
        ([SHS_RS_EMPTY_LOC_AREA_4], False, 76),
        ([SHS_RS_EMPTY_LOC_AREA_5], False, 76),
        ([SHS_RS_EMPTY_LOC_AREA_4_AND_5], False, 113),
        ([SHS_RS_CORRECT_REGION], False, 46),
        ([SHS_RS_CORRECT_DISTRICT], False, 39),
        ([SHS_RS_CORRECT_GEO], True, None),
        ([SHS_RS_CORRECT_GEO_CASE_INSENSITIVE], True, None),
        ([SHS_RS_CORRECT_GEO_ALL_LOWERCASE], True, None),
    ],
)
def test_subcheck__customer_data_is_complete(
    recordsets: list[RecordSet],
    result: bool,
    detail_count: int | None,
    import_context: ImportContext,
    shs_table,
) -> None:
    """Test all possible combinations of missing customer data."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    check = subcheck__customer_data_is_complete(entity, shs_table, CHECK_NAME)
    assert check["result"] is result

    if detail_count is None:
        assert "details" not in check or check["details"] is None
    else:
        assert len(check.get("details", [])) == detail_count


SHS_RS_NO_PLAN_TYPE = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"payment_plan_type": None}]
)
SHS_RS_NO_PLAN_CURRENCY = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"payment_plan_currency": None}]
)
SHS_RS_NO_PLAN_DAYS = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"payment_plan_days": None}]
)

PRODUCT_PRICES_RS_OTHER_CATEGORY = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[{"category": "foo"}],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
SHS_RS_OTHER_PLAN_TYPE = RecordSetWithModifiers(
    SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"payment_plan_type": "foo"}]
)

PAYMENTS_RS_NO_AMOUNT = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE, [PAYMENTS_SAMPLE_DICT], modifiers=[{"amount": 0}]
)
PAYMENTS_RS_OTHER_ACCOUNT_UID = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE,
    [PAYMENTS_SAMPLE_DICT],
    modifiers=[{"account_uid": "foobar"}],
    apply_after=["account_uid"],
)
SHS_RS_NO_PLAN_DOWN_PAYMENT = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": None}],
)
PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT = RecordSetWithModifiers(
    PAYMENTS_TS_TABLE,
    [
        PAYMENTS_SAMPLE_DICT,
        {
            "account_origin": "shs",
            "account_external_id": "A74320525",
            "payment_external_id": "Q74320525",
            "paid_at": "2024-01-05 17:28:30",
            "amount": 10000,
            "currency": "UGX",
        },
    ],
)
SHS_RS_PAYMENTS_SUM_SUFFICIENT_TYPE_PAYGO = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": 60000}],
)
SHS_RS_DOWN_PAYMENT_EXCEEDING_PAYMENTS_SUM = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"payment_plan_down_payment": 60001}],
)
PRODUCT_PRICES_RS_PAYMENTS_SUFFICIENT_TYPE_CASH = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[
        {
            "category": "foo",
            "base_subsidized_sales_price": 60000,
            "original_sales_price": 60001,
        }
    ],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)
PRODUCT_PRICES_RS_PAYMENTS_INSUFFICIENT_TYPE_CASH = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[
        {
            "category": "foo",
            "base_subsidized_sales_price": 60001,
            "original_sales_price": 60002,
        }
    ],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS, PAYMENTS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS], True),
        (
            [
                SHS_RS_NO_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                SHS_RS_NO_PLAN_CURRENCY,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                SHS_RS_NO_PLAN_DAYS,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS], False),  # without payments
        (
            [SHS_RS, PAYMENTS_RS_NO_AMOUNT, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            False,
        ),
        (
            [
                SHS_RS,
                PAYMENTS_RS_OTHER_ACCOUNT_UID,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_OTHER_CATEGORY,
            ],
            True,
        ),
        (
            [
                SHS_RS_NO_PLAN_DOWN_PAYMENT,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            False,
        ),
        (
            [
                SHS_RS_PAYMENTS_SUM_SUFFICIENT_TYPE_PAYGO,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS,
            ],
            True,
        ),
        (
            [
                SHS_RS_DOWN_PAYMENT_EXCEEDING_PAYMENTS_SUM,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
            ],
            False,
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_PAYMENTS_SUFFICIENT_TYPE_CASH,
            ],
            True,
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS_TWO_PAYMENTS_SAME_ACCOUNT,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_PAYMENTS_INSUFFICIENT_TYPE_CASH,
            ],
            False,
        ),
    ],
)
def test_subcheck__payment_plan_is_complete(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of payment data."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    check = subcheck__payment_plan_is_complete(entity, shs_table, CHECK_NAME)
    assert check["result"] is result


PRODUCT_PRICES_RS_NEWER_VALIDITY = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[{"category": "foo"}, {"valid_from": "2024-01-02", "category": "foo"}],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)
PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[
        {"category": "foo"},
        {
            "valid_from": "2024-01-02",
            "original_sales_price": 100000,
            "base_subsidized_sales_price": 70000,
            "category": "foo",
        },
    ],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)

PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT, PRODUCT_PRICES_DICT],
    modifiers=[
        {"category": "foo"},
        {
            "valid_from": "2024-07-02",
            "original_sales_price": 100000,
            "base_subsidized_sales_price": 70000,
            "category": "foo",
        },
    ],
    id_adjustments=[
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
        {"rbf_product_id": (PRODUCTS_TABLE, "id")},
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_VALIDITY,
            ],
            True,
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT,
            ],
            False,
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                PAYMENTS_RS,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT,
            ],
            True,
        ),
    ],
)
def test_subcheck__payment_plan_is_complete_two_prices(
    recordsets: list[RecordSet],
    result: bool,
    import_context: ImportContext,
    shs_table,
) -> None:
    """Test behavior when different prices are available."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]
    check = subcheck__payment_plan_is_complete(entity, shs_table, CHECK_NAME)
    assert check["result"] is result


PRODUCTS_RS_OTHER_ORGA = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"organization_id": 666}]
)
PRODUCTS_RS_OTHER_MANUFACTURER = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"manufacturer": "666"}]
)
PRODUCTS_RS_OTHER_MODEL = RecordSetWithModifiers(
    PRODUCTS_TABLE, [PRODUCTS_DICT], modifiers=[{"model": "666"}]
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], True),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], False),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MANUFACTURER], False),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MODEL], False),
    ],
)
def test_subcheck__product_is_on_white_list(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of products and claim templates."""
    inserts = prepare_test_data_map_rbf_category(
        recordsets=recordsets, import_context=import_context
    )
    entity = inserts[SHS_TABLE][0]

    check = subcheck__product_is_on_white_list(entity, shs_table, CHECK_NAME)
    assert check["result"] is result


def test_subcheck__sale_is_not_older_than_three_months(
    import_context: ImportContext, shs_table
) -> None:
    """Test possible combinations of dates."""
    SHS_RS_PURCHASE_DEC24 = RecordSetWithModifiers(
        SHS_TABLE, [SHS_SAMPLE_DICT], modifiers=[{"purchase_date": date(2024, 12, 1)}]
    )
    inserts = prepare_test_data(
        recordsets=[SHS_RS_PURCHASE_DEC24], import_context=import_context
    )
    entity = inserts[SHS_TABLE][0]

    # assert a good sale is True
    with time_machine.travel("2024-12-31"):
        check = subcheck__sale_is_not_older_than_three_months(entity, shs_table)
        assert check["result"] is True
    # assert an old sale is False
    with time_machine.travel("2025-04-01"):
        check = subcheck__sale_is_not_older_than_three_months(entity, shs_table)
        assert check["result"] is False
    # assert a future sale is False
    with time_machine.travel("2024-11-30"):
        check = subcheck__sale_is_not_older_than_three_months(entity, shs_table)
        assert check["result"] is False
    # assert a sale before project launch @ 2024-11-01 is False
    with time_machine.travel("2023-12-31"):
        check = subcheck__sale_is_not_older_than_three_months(entity, shs_table)
        assert check["result"] is False


SHS_RS_1_REPOSSESSION = RecordSetWithModifiers(
    SHS_TABLE,
    2 * [SHS_SAMPLE_DICT],
    modifiers=[{}, {"customer_external_id": "6666", "purchase_date": date(2024, 1, 1)}],
)
SHS_RS_2_REPOSSESSION = RecordSetWithModifiers(
    SHS_TABLE,
    3 * [SHS_SAMPLE_DICT],
    modifiers=[
        {},
        {"customer_external_id": "5555", "purchase_date": date(2023, 12, 5)},
        {"customer_external_id": "6666", "purchase_date": date(2024, 1, 1)},
    ],
)
SHS_RS_2_REPOSSESSION_DIFFERENT_SERIAL_NUMBER = RecordSetWithModifiers(
    SHS_TABLE,
    3 * [SHS_SAMPLE_DICT],
    modifiers=[
        {},
        {
            "customer_external_id": "5555",
            "purchase_date": date(2023, 12, 15),
        },
        {
            "customer_external_id": "6666",
            "purchase_date": date(2024, 1, 1),
            "serial_number": "6666",
        },
    ],
)
SHS_RS_2_REPOSSESSION_DIFFERENT_MANUFACTURER = RecordSetWithModifiers(
    SHS_TABLE,
    3 * [SHS_SAMPLE_DICT],
    modifiers=[
        {},
        {
            "customer_external_id": "5555",
            "purchase_date": date(2023, 12, 15),
        },
        {
            "customer_external_id": "6666",
            "purchase_date": date(2024, 1, 1),
            "manufacturer": "6666",
        },
    ],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS], True),
        ([SHS_RS_1_REPOSSESSION], True),
        ([SHS_RS_2_REPOSSESSION], False),
        ([SHS_RS_2_REPOSSESSION_DIFFERENT_SERIAL_NUMBER], True),
        ([SHS_RS_2_REPOSSESSION_DIFFERENT_MANUFACTURER], True),
    ],
)
def test_subcheck__entity_is_repossessed_at_most_once(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible repossession possibilities."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    check = subcheck__entity_is_repossessed_at_most_once(entity, shs_table)
    assert check["result"] is result


PRODUCTS_RS_NO_RATED_POWER_W = RecordSetWithModifiers(
    PRODUCTS_TABLE,
    [PRODUCTS_DICT],
    modifiers=[{"rated_power_w": None}],
    id_adjustments=[{"rbf_claim_template_id": (CLAIM_TEMPLATES_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], 10),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MANUFACTURER], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MODEL], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_NO_RATED_POWER_W], None),
    ],
)
def test__get_rated_power(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of products and claim templates."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    check = get_rated_power(entity, shs_table, CHECK_NAME)
    assert check["rated_power_w"] == result


PRODUCTS_RS_NO_TECHNOLOGY = RecordSetWithModifiers(
    PRODUCTS_TABLE,
    [PRODUCTS_DICT],
    modifiers=[{"technology": None}],
    id_adjustments=[{"rbf_claim_template_id": (CLAIM_TEMPLATES_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS], "device type A"),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_ORGA], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MANUFACTURER], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_OTHER_MODEL], None),
        ([SHS_RS, CLAIM_RS, PRODUCTS_RS_NO_TECHNOLOGY], None),
    ],
)
def test__get_technology(
    recordsets: list[RecordSet], result: bool, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of products and claim templates."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    check = get_technology(entity, shs_table, CHECK_NAME)
    assert check["technology"] == result


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [SHS_RS],
            {
                "incentive__amount": 0,
                "incentive__type": "unknown region NORTHERY",
                "result": False,
            },
        ),
        (
            [SHS_RS_CORRECT_REGION],
            {
                "incentive__amount": 11000,
                "incentive__type": "Northern/Foo/Only Regional",
            },
        ),
        (
            [SHS_RS_CORRECT_DISTRICT],
            {
                "incentive__amount": 0,
                "incentive__type": "unknown region NORTHERY",
                "result": False,
            },
        ),
        (
            [SHS_RS_CORRECT_GEO],
            {
                "incentive__amount": 15000,
                "incentive__type": "Northern/Nabilatuk/Karamoja",
            },
        ),
        (
            [SHS_RS_CORRECT_GEO_CASE_INSENSITIVE],
            {
                "incentive__amount": 15000,
                "incentive__type": "Northern/Nabilatuk/Karamoja",
            },
        ),
        (
            [SHS_RS_CORRECT_GEO_ALL_LOWERCASE, CLAIM_RS, PRODUCTS_RS],
            {
                "incentive__amount": 16000,
                "incentive__type": "Northern/Madi-Okollo/Rhd",
            },
        ),
    ],
)
def test_calculate_incentive(
    recordsets: list[RecordSet], result: dict, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of incentive calculation failures."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    assert calculate_incentive(entity, INCENTIVES, shs_table) == result


SHS_RS_FEMALE = RecordSetWithModifiers(
    SHS_TABLE,
    [SHS_SAMPLE_DICT],
    modifiers=[{"female_ownership": True}],
)
PRODUCTS_PRICES_RS_PRICE_NOT_VALID = RecordSetWithModifiers(
    PRODUCT_PRICES_TABLE,
    [PRODUCT_PRICES_DICT],
    modifiers=[{"valid_from": "2024-01-10"}],
    id_adjustments=[{"rbf_product_id": (PRODUCTS_TABLE, "id")}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        (
            [SHS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 30000,
                "subsidy__base_subsidized_sales_price": 20000,
                "subsidy__category": "A",
                "subsidy__price_id": ANY,
                "subsidy__original_sales_price": 50000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__subsidized_sales_price": 20000,
            },
        ),
        (
            [SHS_RS_FEMALE, CLAIM_RS, PRODUCTS_RS, PRODUCT_PRICES_RS],
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 30000,
                "subsidy__base_subsidized_sales_price": 20000,
                "subsidy__category": "A",
                "subsidy__price_id": ANY,
                "subsidy__original_sales_price": 50000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__subsidized_sales_price": 20000,
            },
        ),
        (
            [SHS_RS, CLAIM_RS, PRODUCTS_RS, PRODUCTS_PRICES_RS_PRICE_NOT_VALID],
            {
                "result": False,
                "subsidy__amount": 0,
                "subsidy__error": "No valid price found for the combination of purchase"
                " date, sales mode and product.",
            },
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_FUTURE_HIGHER_THAN_PRODUCT,
            ],
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 30000,
                "subsidy__base_subsidized_sales_price": 20000,
                "subsidy__category": "A",
                "subsidy__price_id": ANY,
                "subsidy__original_sales_price": 50000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__subsidized_sales_price": 20000,
            },
        ),
        (
            [
                SHS_RS_OTHER_PLAN_TYPE,
                CLAIM_RS,
                PRODUCTS_RS,
                PRODUCT_PRICES_RS_NEWER_HIGHER_THAN_PRODUCT,
            ],
            {
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subsidy__amount": 30000,
                "subsidy__base_subsidized_sales_price": 70000,
                "subsidy__category": "A",
                "subsidy__price_id": ANY,
                "subsidy__original_sales_price": 100000.0,
                "subsidy__price_calculated_at": "2024-03-31T00:00:00+00:00",
                "subsidy__subsidized_sales_price": 70000,
            },
        ),
    ],
)
def test_calculate_subsidy(
    recordsets: list[RecordSet], result: dict, import_context: ImportContext, shs_table
) -> None:
    """Test all possible combinations of subsidy calculation failures."""
    inserts = prepare_test_data(recordsets=recordsets, import_context=import_context)
    entity = inserts[SHS_TABLE][0]

    with time_machine.travel(datetime(2024, 3, 31, tzinfo=UTC), tick=False):
        assert calculate_subsidy(entity, shs_table, CHECK_NAME) == result
