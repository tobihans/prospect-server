# ruff: noqa: E501
from collections import Counter
from datetime import UTC, datetime

import pytest
import time_machine
from inline_snapshot import snapshot

from dataworker.database import get_or_create_connection
from dataworker.queries import table_content
from dataworker.task import InternalTask
from tasks.internal.rbf_easp.is_eligible_per_import import run
from tests.utils import (
    count_queries,
    insert_recordset,
    prepare_many_for_compare,
    summarize_records,
)


@time_machine.travel(datetime(2025, 4, 1, 20, 19, 42, tzinfo=UTC), tick=False)
@pytest.mark.usefixtures("_populate_rbf_helper_tables")
def test__rbf_easp_is_eligible_per_import_task_from_json(
    pue_meters_data,
    ccs_pue_payments_ts_data,
    ccs_meters_data,
    ogs_shs_data,
    ogs_payments_ts_data,
    import_context,
    caplog,
):
    # --- DATA INGESTION ---
    # import meters, systems and payments together
    with count_queries(get_or_create_connection()) as queries:
        insert_recordset(ogs_shs_data)
        insert_recordset(ccs_meters_data)
        insert_recordset(pue_meters_data)
        insert_recordset(ogs_payments_ts_data)
        insert_recordset(ccs_pue_payments_ts_data)
    assert len(queries) == 5

    # --- RUN ---
    verifications = run(import_id=import_context.import_id)
    assert len(verifications["data_trust_trace"]) == 30
    verifications_per_check = Counter(
        dtt["check"] for dtt in verifications["data_trust_trace"]
    )
    # count is 8 per check + 3 non-matching on products for PUE and CCS
    # that end up counted in each other
    assert verifications_per_check["rbf_easp_ogs_is_eligible"] == 8
    assert verifications_per_check["rbf_easp_pue_is_eligible"] == 11
    assert verifications_per_check["rbf_easp_ccs_is_eligible"] == 11
    summary_logs = [
        msg
        for msg in caplog.messages
        if msg.startswith("Finished generating verifications")
    ]
    assert "rbf_easp_ogs_is_eligible with result: False: 7, True: 1" in summary_logs[0]
    assert "rbf_easp_pue_is_eligible with result: False: 10, True: 1" in summary_logs[1]
    assert "rbf_easp_ccs_is_eligible with result: False: 10, True: 1" in summary_logs[2]

    # --- TASK ---
    with count_queries(get_or_create_connection()) as queries:
        InternalTask.from_function(run)(import_id=import_context.import_id)
    # this is where we are and what we could/should improve
    assert len(queries) == 241
    savepoint_count = release_savepoint_count = 0
    for query in queries:
        if query.startswith("SAVEPOINT "):
            savepoint_count += 1
        elif query.startswith("RELEASE SAVEPOINT "):
            release_savepoint_count += 1
    assert savepoint_count == 30
    assert release_savepoint_count == savepoint_count

    # --- CHECK RESULT ---
    # shed price_ids as they depend on test orders
    trust_traces = table_content(
        "data_trust_trace",
        shed_nulls=True,
        string_timestamps=True,
        shed_custom_paths=["subsidy__price_id"],
    )

    assert summarize_records(prepare_many_for_compare(trust_traces)) == {
        "sample_count": 30,
        "fields": [
            "check",
            "custom",
            "data_origin",
            "import_id",
            "organization_id",
            "result",
            "source_id",
            "subject_origin",
            "subject_uid",
            "uid",
        ],
        "hash": "2091f5e8d50320d2a461355c654b800b4381c6f5b7b01809cb3ea9d6a97c3151",
        "value_count": 300,
    }
    assert trust_traces == snapshot([
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No payments found.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {
                    "result": False,
                    "details": "NORTHERY administrative unit not found.",
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-05) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_B74320525_222222_CCS Company A_D83209731",
            "uid": "meters_222222_solar_coop_B74320525_222222_CCS Company A_D83209731_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No payments found.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {
                    "result": False,
                    "details": "NORTHERY administrative unit not found.",
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-05) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_B74320525_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_B74320525_222222_PUE Company A_D83209731_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company B) and model (CCS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C23488192_222222_CCS Company B_D23488192",
            "uid": "meters_222222_solar_coop_C23488192_222222_CCS Company B_D23488192_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "customer_category (None) is not one of ['mse', 'tfh']. Enterprise has more than 50 employees and/or 100mio UGX in assets.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company B) and model (CCS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C23488192_222222_CCS Company B_D23488192",
            "uid": "meters_222222_solar_coop_C23488192_222222_CCS Company B_D23488192_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C23488192_222222_PUE Company B_D23488192",
            "uid": "meters_222222_solar_coop_C23488192_222222_PUE Company B_D23488192_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "customer_category (other enterprise) is not one of ['mse', 'tfh'].",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C23488192_222222_PUE Company B_D23488192",
            "uid": "meters_222222_solar_coop_C23488192_222222_PUE Company B_D23488192_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company C) and model (CCS Device C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C26881862_222222_CCS Company C_D26881862",
            "uid": "meters_222222_solar_coop_C26881862_222222_CCS Company C_D26881862_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "customer_category (None) is not one of ['mse', 'tfh']. Enterprise has more than 50 employees and/or 100mio UGX in assets.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company C) and model (CCS Device C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C26881862_222222_CCS Company C_D26881862",
            "uid": "meters_222222_solar_coop_C26881862_222222_CCS Company C_D26881862_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company C) and model (PUE Device C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C26881862_222222_PUE Company C_D26881862",
            "uid": "meters_222222_solar_coop_C26881862_222222_PUE Company C_D26881862_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company C) and model (PUE Device C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C26881862_222222_PUE Company C_D26881862",
            "uid": "meters_222222_solar_coop_C26881862_222222_PUE Company C_D26881862_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "customer&category is not unique (conflicting uid: 222222_solar_coop_C74320525_222222_CCS Company A_D74320525).",
                },
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_CCS Company A_D74320521",
            "uid": "meters_222222_solar_coop_C74320525_222222_CCS Company A_D74320521_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "incentive__type": "Northern/Madi-Okollo/Rhd/Ai",
                "subsidy__amount": 10000,
                "incentive__amount": 13000,
                "subsidy__category": "Ai",
                "subsidy__price_calculated_at": "2025-04-01T20:19:42+00:00",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__subsidized_sales_price": 90000,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "True",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_CCS Company A_D74320525",
            "uid": "meters_222222_solar_coop_C74320525_222222_CCS Company A_D74320525_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "customer&category is not unique (conflicting uid: 222222_solar_coop_C74320525_222222_PUE Company A_D74320525).",
                },
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_PUE Company A_D74320521",
            "uid": "meters_222222_solar_coop_C74320525_222222_PUE Company A_D74320521_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "incentive__type": "Northern/Madi-Okollo/Rhd",
                "subsidy__amount": 60000,
                "incentive__amount": 203000,
                "subsidy__category": "A/female_ownership",
                "subsidy__price_calculated_at": "2025-04-01T20:19:42+00:00",
                "subsidy__original_sales_price": 100000.0,
                "subcheck__customer_is_eligible": {"result": True},
                "subsidy__subsidized_sales_price": 40000,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__aggregated_subsidy_adjustment": 50000,
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "True",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C74320525_222222_PUE Company A_D74320525",
            "uid": "meters_222222_solar_coop_C74320525_222222_PUE Company A_D74320525_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {
                    "result": False,
                    "details": "More than one previous sale exists for the manufacturer/serial combination.",
                },
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-15) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83209731_222222_CCS Company A_D83209731",
            "uid": "meters_222222_solar_coop_C83209731_222222_CCS Company A_D83209731_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {
                    "result": False,
                    "details": "More than one previous sale exists for the manufacturer/serial combination.",
                },
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-15) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83209731_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_C83209731_222222_PUE Company A_D83209731_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company B) and model (CCS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83687814_222222_CCS Company B_D83687814",
            "uid": "meters_222222_solar_coop_C83687814_222222_CCS Company B_D83687814_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "customer_category (None) is not one of ['mse', 'tfh']. Enterprise has more than 50 employees and/or 100mio UGX in assets.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (CCS Company B) and model (CCS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83687814_222222_CCS Company B_D83687814",
            "uid": "meters_222222_solar_coop_C83687814_222222_CCS Company B_D83687814_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83687814_222222_PUE Company B_D83687814",
            "uid": "meters_222222_solar_coop_C83687814_222222_PUE Company B_D83687814_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_is_eligible": {
                    "result": False,
                    "details": "Enterprise has more than 50 employees and/or 100mio UGX in assets.",
                },
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (PUE Company B) and model (PUE Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_C83687814_222222_PUE Company B_D83687814",
            "uid": "meters_222222_solar_coop_C83687814_222222_PUE Company B_D83687814_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ccs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "Payment sums for account not sufficient to cover payment_plan_down_payment of 5000.0.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "The purchase date (2023-11-05) can't preceed 2024-11-01.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_X74320525_222222_CCS Company A_D83209731",
            "uid": "meters_222222_solar_coop_X74320525_222222_CCS Company A_D83209731_rbf_easp_ccs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_pue_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "subcheck__customer_is_eligible": {"result": True},
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "Payment sums for account not sufficient to cover payment_plan_down_payment of 5000.0.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "The purchase date (2023-11-05) can't preceed 2024-11-01.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "meters",
            "subject_uid": "222222_solar_coop_X74320525_222222_PUE Company A_D83209731",
            "uid": "meters_222222_solar_coop_X74320525_222222_PUE Company A_D83209731_rbf_easp_pue_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "rated_power_w": 10,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No payments found.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {
                    "result": False,
                    "details": "NORTHERY administrative unit not found.",
                },
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-05) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_B74320525_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_B74320525_222222_OGS Company A_D83209731_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company B) and model (OGS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-12) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C23488192_222222_OGS Company B_D23488192",
            "uid": "shs_222222_solar_coop_C23488192_222222_OGS Company B_D23488192_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company C) and model (OGS Device C) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C26881862_222222_OGS Company C_D26881862",
            "uid": "shs_222222_solar_coop_C26881862_222222_OGS Company C_D26881862_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "rated_power_w": 10,
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "customer&category is not unique (conflicting uid: 222222_solar_coop_C74320525_222222_OGS Company A_D74320525).",
                },
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C74320525_222222_OGS Company A_D74320521",
            "uid": "shs_222222_solar_coop_C74320525_222222_OGS Company A_D74320521_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "rated_power_w": 10,
                "incentive__type": "Northern/Madi-Okollo/Rhd",
                "subsidy__amount": 10000,
                "incentive__amount": 16000,
                "subsidy__category": "A",
                "subsidy__price_calculated_at": "2025-04-01T20:19:42+00:00",
                "subsidy__original_sales_price": 100000.0,
                "subsidy__subsidized_sales_price": 90000,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {"result": True},
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subsidy__base_subsidized_sales_price": 90000,
                "subsidy__aggregated_subsidy_adjustment": 0,
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "True",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C74320525_222222_OGS Company A_D74320525",
            "uid": "shs_222222_solar_coop_C74320525_222222_OGS Company A_D74320525_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "rated_power_w": 10,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {
                    "result": False,
                    "details": "More than one previous sale exists for the manufacturer/serial combination.",
                },
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "Elapsed time since purchase date (2024-12-15) exceeds three months.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C83209731_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_C83209731_222222_OGS Company A_D83209731_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "subcheck__customer_data_is_unique": {
                    "result": False,
                    "details": "device not in product whitelist, can't assess uniqueness",
                },
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "No valid price found for the combination of purchase date, sales mode and product.",
                },
                "subcheck__product_is_on_white_list": {
                    "result": False,
                    "details": "Manufacturer (OGS Company B) and model (OGS Device B) combination not whitelisted in rbf_claim_templates.",
                },
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {"result": True},
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_C83687814_222222_OGS Company B_D83687814",
            "uid": "shs_222222_solar_coop_C83687814_222222_OGS Company B_D83687814_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
        {
            "check": "rbf_easp_ogs_is_eligible",
            "created_at": "2025-04-01T20:19:42",
            "custom": {
                "technology": "device type A",
                "rated_power_w": 10,
                "subcheck__customer_data_is_unique": {"result": True},
                "subcheck__payment_plan_is_complete": {
                    "result": False,
                    "details": "Payment sums for account not sufficient to cover payment_plan_down_payment of 5000.0.",
                },
                "subcheck__product_is_on_white_list": {"result": True},
                "subcheck__customer_data_is_complete": {"result": True},
                "subcheck__entity_is_repossessed_at_most_once": {"result": True},
                "subcheck__sale_is_not_older_than_three_months": {
                    "result": False,
                    "details": "The purchase date (2023-11-05) can't preceed 2024-11-01.",
                },
            },
            "data_origin": "solar_coop",
            "import_id": 111111,
            "organization_id": 222222,
            "result": "False",
            "source_id": 333333,
            "subject_origin": "shs",
            "subject_uid": "222222_solar_coop_X74320525_222222_OGS Company A_D83209731",
            "uid": "shs_222222_solar_coop_X74320525_222222_OGS Company A_D83209731_rbf_easp_ogs_is_eligible",
            "updated_at": "2025-04-01T20:19:42",
        },
    ])


# TODO: add test for import updating existing datasets
