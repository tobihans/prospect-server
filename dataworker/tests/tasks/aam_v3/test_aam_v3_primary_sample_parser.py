import datetime

import arrow
import funcy
import pytest

import tasks.ingestion.aam_v3.samples_parser

from ...utils import load_test_data

# ------------------------- filter -------------------------


@pytest.fixture
def valid_sample():
    return {
        "time": datetime.datetime(2022, 2, 9, 0, 45, 52, tzinfo=datetime.UTC),
        "coulomb_battery_status": 12353,
        "coulomb_gauge_status": 64,
        "coulomb_operational_status": 514,
        "coulomb_time_to_empty": 3028,
        "coulomb_time_to_full": 65535,
        "coulomb_cycle_count": 0,
        "coulomb_state_of_charge": 72,
        "coulomb_state_of_health": 100,
        "coulomb_external_temperature": 2340,
        "coulomb_internal_temperature": 3041,
        "coulomb_battery_voltage": 24528.0,
        "coulomb_battery_current": -984.0,
        "coulomb_remaining_capacity": 50260.0,
        "coulomb_full_charge_capacity": 70000.0,
        "coulomb_calibration_status": 3,
        "file": "a2ei-skgsp3-logs/4018/a2ei_1645013520305.icsv",
        "aam_id": 4018,
    }


def test__validate_sample__pass(valid_sample) -> None:
    """A normal sample should pass."""
    assert tasks.ingestion.aam_v3.samples_parser._validate_sample(valid_sample) is True


def test__validate_sample__wrong_dates(valid_sample) -> None:
    """A few samples with different real life bad dates should fail the test."""
    too_early = funcy.set_in(
        valid_sample,
        ("time",),
        datetime.datetime(1970, 2, 9, 0, 45, 52, tzinfo=datetime.UTC),
    )
    assert tasks.ingestion.aam_v3.samples_parser._validate_sample(too_early) is False

    too_late = funcy.set_in(
        valid_sample,
        ("time",),
        datetime.datetime(2060, 2, 9, 0, 45, 52, tzinfo=datetime.UTC),
    )
    assert tasks.ingestion.aam_v3.samples_parser._validate_sample(too_late) is False


# ------------- sample validation -------------


def test__filter_samples__date() -> None:
    samples = [
        {"current": 1, "tension": 7, "time": arrow.get("2022-06-19")},
        {"current": 2, "tension": 8, "time": arrow.get("1986-06-19")},
    ]
    filtered = tasks.ingestion.aam_v3.samples_parser._filter_samples(samples)
    assert len(filtered)
    assert filtered[0] == {"current": 1, "tension": 7, "time": arrow.get("2022-06-19")}


# ------------- primary parsing, including filtering -------------


def test__parse_file__v3_cosuper() -> None:
    cosuper_data = load_test_data("aam_v3/cosuper_FWv1_MCU_samples.icsv")
    assert len(tasks.ingestion.aam_v3.samples_parser.parse_samples(cosuper_data)) == 5


def test__parse_file__v3_epsolar() -> None:
    epsolar_data = load_test_data("aam_v3/epsolar_FWv1_MCU_samples.icsv")
    assert len(tasks.ingestion.aam_v3.samples_parser.parse_samples(epsolar_data)) == 6


# ------------------------- forward fill -------------------------


def test__forward_fill__no_harm() -> None:
    """Don't touch samples if nothing is missing."""
    samples = [{"a": 1, "b": 4}, {"a": 2, "b": 5}, {"a": 3, "b": 6}]
    assert (
        tasks.ingestion.aam_v3.samples_parser._forward_fill_missing_values(samples)
        == samples
    )


def test__forward_fill__fill_missing() -> None:
    """Don't touch samples if nothing is missing."""
    samples = [{"a": 1, "b": 4}, {"a": None, "b": 5}, {"a": None, "b": None}]
    assert tasks.ingestion.aam_v3.samples_parser._forward_fill_missing_values(
        samples
    ) == [
        {"a": 1, "b": 4},
        {"a": 1, "b": 5},
        {"a": 1, "b": 5},
    ]


def test__forward_fill__no_backfill() -> None:
    """Don't touch samples if nothing is missing."""
    samples = [{"a": None, "b": None}, {"a": None, "b": 5}, {"a": 3, "b": 6}]
    assert tasks.ingestion.aam_v3.samples_parser._forward_fill_missing_values(
        samples
    ) == [
        {"a": None, "b": None},
        {"a": None, "b": 5},
        {"a": 3, "b": 6},
    ]


# ------------------------- aggregation -------------------------


def test__line_tag_aggregation() -> None:
    samples = [
        {
            "time": arrow.get("2023-11-07T22:34:37.706608+01:00").datetime,
            "line_tag": "V",
            "voltage": 13,
        },
        {
            "time": arrow.get("2023-11-07T22:34:37.908176+01:00").datetime,
            "line_tag": "A",
            "current": 2,
        },
        {
            "time": arrow.get("2023-11-07T22:34:38.109627+01:00").datetime,
            "line_tag": "P",
            "power": 26,
        },
    ]
    assert tasks.ingestion.aam_v3.samples_parser._aggregate_logs_timewise(samples) == [
        {
            "time": datetime.datetime(
                2023,
                11,
                7,
                22,
                34,
                37,
                706608,
                tzinfo=datetime.timezone(datetime.timedelta(hours=1)),
            ),
            "log_types": "V,A,P",
            "voltage": 13,
            "current": 2,
            "power": 26,
        }
    ]
