import glom

import tasks.ingestion.aam_v3.reshape


def test__coulomb_counter_calibrated__true() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.is_coulomb_counter_calibrated({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
        })
        is True
    )


def test__coulomb_counter_calibrated__other_protocol() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.is_coulomb_counter_calibrated({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 42,
        })
        is False
    )


def test__coulomb_counter_calibrated__not_calibrated() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.is_coulomb_counter_calibrated({
            "coulomb_calibration_complete": False,
            "upower_protocol_type": 0,
        })
        is False
    )


def test__coulomb_counter_calibrated__missing() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.is_coulomb_counter_calibrated({
            "upower_protocol_type": 0
        })
        is False
    )
    assert (
        tasks.ingestion.aam_v3.reshape.is_coulomb_counter_calibrated({
            "coulomb_calibration_complete": True
        })
        is False
    )


def test__battery_io_w__calibrated() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.battery_io_w({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
            "coulomb_battery_voltage": 12,
            "coulomb_battery_current": 1.5,
            "upower_battery_voltage": 11,
            "upower_battery_current": 1.4,
        })
        == 18
    )


def test__battery_io_w__not_calibrated() -> None:
    """Fallback on upower values."""
    assert (
        tasks.ingestion.aam_v3.reshape.battery_io_w({
            "coulomb_calibration_complete": False,
            "upower_protocol_type": 0,
            "coulomb_battery_voltage": 12,
            "coulomb_battery_current": 1.5,
            "upower_battery_voltage": 10,
            "upower_battery_current": 1.2,
        })
        == 12.0
    )


def test__battery_io_w__calibrated__coulomb_missing() -> None:
    """Fallback on upower values."""
    assert (
        tasks.ingestion.aam_v3.reshape.battery_io_w({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
            "coulomb_battery_current": 1.5,
            "upower_battery_voltage": 10,
            "upower_battery_current": 1.2,
        })
        == 12.0
    )


def test__battery_io_w__calibrated__both_missing() -> None:
    """Ultimate fallback on None."""
    assert (
        tasks.ingestion.aam_v3.reshape.battery_io_w({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
            "coulomb_battery_current": 1.5,
            "upower_battery_voltage": 10,
        })
        is None
    )


def test__battery_charge_percent__coulomb_measure() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.battery_charge_percent({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
            "coulomb_state_of_charge": 85,
            "upower_battery_state_of_charge": 60,
        })
        == 85
    )


def test__battery_charge_percent__not_calibrated() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.battery_charge_percent({
            "coulomb_calibration_complete": False,
            "upower_protocol_type": 0,
            "coulomb_state_of_charge": 85,
            "upower_SOC": 60,
        })
        == 60
    )


def test__battery_charge_percent__missing_coulomb() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.battery_charge_percent({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
            "upower_SOC": 60,
        })
        == 60
    )


def test__battery_charge_percent__both_missing() -> None:
    assert (
        tasks.ingestion.aam_v3.reshape.battery_charge_percent({
            "coulomb_calibration_complete": True,
            "upower_protocol_type": 0,
        })
        is None
    )


# ------------------------- bug correction -------------------------


def test__reshape__paygo_time_left() -> None:
    sample = {
        "time": "2023-09-28 23:18:02.705992+0000",
        "line_tag": "CO0",
        "coulomb_gauge_status": None,
        "coulomb_time_to_empty": None,
        "coulomb_time_to_full": None,
        "coulomb_cycle_count": None,
        "coulomb_state_of_charge": None,
        "coulomb_state_of_health": None,
        "coulomb_battery_voltage": None,
        "coulomb_battery_current": 0.78,
        "coulomb_calibration_status": None,
        "coulomb_calibration_complete": None,
        "upower_input_voltage": 206.75,
        "upower_input_current": None,
        "upower_input_frequency": None,
        "upower_battery_temperature": None,
        "upower_device_temperature": None,
        "upower_output_voltage": 206.87,
        "upower_output_current": 0.02,
        "upower_pv_input_voltage": None,
        "upower_pv_input_current": None,
        "upower_battery_voltage": None,
        "upower_battery_current": None,
        "upower_SOC": None,
        "upower_protocol_type": None,
        "paygo_time_left": None,
        "paygo_token_count": None,
    }
    result = glom.glom(sample, tasks.ingestion.aam_v3.reshape.shs_ts_sample_spec)
    assert result == {
        "metered_at": "2023-09-28 23:18:02.705992+0000",
        "pv_input_w": None,
        "grid_input_w": None,
        "grid_input_v": 206.75,
        "battery_v": None,
        "battery_io_w": None,
        "battery_charge_percent": None,
        "system_output_w": 4.1374,
        "grid_energy_lifetime_wh": None,
        "pv_energy_lifetime_wh": None,
        "output_energy_lifetime_wh": None,
        "paygo_is_paid_off": None,
        "paygo_days_left": None,
    }
