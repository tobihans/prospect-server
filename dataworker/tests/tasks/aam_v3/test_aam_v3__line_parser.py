import pytest

from dataworker.processing import stringify_dates_in_sample
from tasks.ingestion.aam_v3 import line_parser


def test__split_line__basic() -> None:
    line = "16898, 129221, IENRST, 227600, 79846"
    assert line_parser.split_line(line) == [
        "16898",
        "129221",
        "IENRST",
        "227600",
        "79846",
    ]


def test__split_line__spaced() -> None:
    line = "  16898  , 129221  , IENRST   , 227600   , 79846   "
    assert line_parser.split_line(line) == [
        "16898",
        "129221",
        "IENRST",
        "227600",
        "79846",
    ]


def test__split_line__missing() -> None:
    line = "  16898  ,   , IENRST   , 227600   , "
    assert line_parser.split_line(line) == [
        "16898",
        "",
        "IENRST",
        "227600",
        "",
    ]


def test_parse_line__unknown_type() -> None:
    line = "1664462002141744, 377683, SLUSHMETER,4611686018426161765,0,"
    assert line_parser.parse_line(line) is None


def test_parse_line__log_rotation_alone(caplog) -> None:
    """Line with log rotation misses a linebreak and contains the next line..."""
    line = "356845570276, 307900, FINE LOG ROTATED"
    assert line_parser.parse_line(line) is None
    assert len(caplog.messages) == 2
    assert caplog.messages[0] == "line tag: FINE LOG ROTATED"
    assert caplog.messages[1] == "encountered log rotation"


def test_parse_line__debug_value(caplog) -> None:
    """Line with EV0 value from DEBUG mode is ignored."""
    line = "3171864, 17, EV0,AAM:6,"
    assert line_parser.parse_line(line) is None
    assert len(caplog.messages) == 1
    assert caplog.messages[0] == "line tag: EV0"


def test_parse_line__other_value(caplog) -> None:
    """Line with unknown value is handled apart."""
    line = "3171865, 18, FOOBAR,BLABLA,"
    assert line_parser.parse_line(line) is None
    assert len(caplog.messages) == 2
    assert caplog.messages[0] == "line tag: FOOBAR"
    assert caplog.messages[1] == "unknown line type: FOOBAR"


def test_parse_line__log_rotation_trailing_line() -> None:
    """Line with log rotation misses a linebreak and contains the next line..."""
    line = "356845570276, 307900, FINE LOG ROTATED356845656208, 307901, CO0,,,0,,,,26988,-56,,"  # noqa: E501
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "coulomb_battery_current": -0.056,
        "coulomb_battery_voltage": 26.988,
        "coulomb_calibration_complete": None,
        "coulomb_calibration_status": None,
        "coulomb_cycle_count": None,
        "coulomb_gauge_status": None,
        "coulomb_state_of_charge": None,
        "coulomb_state_of_health": None,
        "coulomb_time_to_empty": None,
        "coulomb_time_to_full": 0,
        "line_tag": "CO0",
        "log_id": 307901,
        "time": "1970-01-05T03:07:25.656208+00:00",
    }


def test_parse_line__fcb_rotation_trailing_line() -> None:
    """Line with fcb rotation misses a linebreak and contains the next line..."""
    line = "3764050312480, 50230, FCB ROTATED3764050406224, 50231, COULOMB,8514,0,642,65535,0,0,100,100,2341,3031,680,740,8800,8800,0,"  # noqa: E501
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "coulomb_battery_current": 740.0,
        "coulomb_battery_status": 8514,
        "coulomb_battery_voltage": 680.0,
        "coulomb_calibration_status": 0,
        "coulomb_cycle_count": 0,
        "coulomb_external_temperature": 2341,
        "coulomb_full_charge_capacity": 8800.0,
        "coulomb_gauge_status": 0,
        "coulomb_internal_temperature": 3031,
        "coulomb_operational_status": 642,
        "coulomb_remaining_capacity": 8800.0,
        "coulomb_state_of_charge": 100,
        "coulomb_state_of_health": 100,
        "coulomb_time_to_empty": 65535,
        "coulomb_time_to_full": 0,
        "line_tag": "COULOMB",
        "log_id": 50231,
        "time": "1970-02-13T13:34:10.406224+00:00",
    }


# ------------------------- line parsers: FWv1 log lines -------------------------


def test_parse_line__inv_v1() -> None:
    line = "1664462001977628, 61569, INV,0,229800,233000,0,50000,12000,25500,69,"
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "2022-09-29T14:33:21.977628+00:00",
        "log_id": 61569,
        "line_tag": "INV",
        "inverter_input_voltage": 0.0,
        "inverter_output_voltage": 229.8,
        "inverter_input_fault_voltage": 233.0,
        "inverter_relative_output_power": 0.0,
        "inverter_output_frequency": 50.0,
        "inverter_battery_voltage": 24.0,
        "inverter_temperature": 25.5,
        "inverter_status": 69,
    }


def test_parse_line__mppt_v1() -> None:
    line = "1664462002024500, 377681, MPPT,2485,0,2361,0,2361,5,2377,2377,1949,0,2,11,"
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "2022-09-29T14:33:22.024500+00:00",
        "log_id": 377681,
        "line_tag": "MPPT",
        "mppt_panel_voltage": 24.85,
        "mppt_panel_current": 0.0,
        "mppt_battery_voltage": 23.61,
        "mppt_battery_current": 0.0,
        "mppt_output_voltage": 23.61,
        "mppt_output_current": 0.05,
        "mppt_external_temperature": 23.77,
        "mppt_internal_temperature": 23.77,
        "mppt_battery_temperature": 19.49,
        "mppt_state_of_charge": 0,
        "mppt_battery_status": 2,
        "mppt_mppt_status": 11,
    }


def test_parse_line__coulomb_v1() -> None:
    line = (
        "1664462002079248, 377682,"
        " COULOMB,8640,320,642,65535,65535,0,64,100,2341,2934,619,0,5596,8800,0,"
    )
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "2022-09-29T14:33:22.079248+00:00",
        "log_id": 377682,
        "line_tag": "COULOMB",
        "coulomb_battery_status": 8640,
        "coulomb_gauge_status": 320,
        "coulomb_operational_status": 642,
        "coulomb_time_to_empty": 65535,
        "coulomb_time_to_full": 65535,
        "coulomb_cycle_count": 0,
        "coulomb_state_of_charge": 64,
        "coulomb_state_of_health": 100,
        "coulomb_external_temperature": 2341,
        "coulomb_internal_temperature": 2934,
        "coulomb_battery_voltage": 619.0,
        "coulomb_battery_current": 0.0,
        "coulomb_remaining_capacity": 5596.0,
        "coulomb_full_charge_capacity": 8800.0,
        "coulomb_calibration_status": 0,
    }


def test_parse_line__paygo_v1() -> None:
    line = "1664462002141744, 377683, PAYGO,4611686018426161765,0,"
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "2022-09-29T14:33:22.141744+00:00",
        "log_id": 377683,
        "line_tag": "PAYGO",
        "paygo_time_left": 4611686018426161765,
        "paygo_token_count": 0,
    }


def test_parse_line__upower_v1() -> None:
    line = (
        "1669315421221480, 15689,"
        " UPOWER,0,0,0,0,0,12,0,0,2500,2500,3900,5082,23002,0,1,5000,0,0,3900,3500,"
        "0,0,0,0,0,0,0,5082,0,0,97,0,"
    )
    data = line_parser.parse_line(line)
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "2022-11-24T18:43:41.221480+00:00",
        "log_id": 15689,
        "line_tag": "UPOWER",
        "upower_grid_input_voltage": 0.0,
        "upower_grid_input_current": 0.0,
        "upower_grid_input_power": 0.0,
        "upower_grid_input_frequency": 0.0,
        "upower_total_cumulative_grid_energy": 0.12,
        "upower_grid_charging_battery_temperature": 25.0,
        "upower_grid_charging_device_temperature": 25.0,
        "upower_grid_charging_power_device_temperature": 39.0,
        "upower_load_input_voltage": 50.82,
        "upower_load_output_voltage": 230.02,
        "upower_load_output_current": 0.0,
        "upower_load_output_frequency": 50.0,
        "upower_load_total_cumulative_energy_consumption": 0.0,
        "upower_load_heat_sink_1_temperature": 39.0,
        "upower_load_heat_sink_2_temperature": 35.0,
        "upower_pv_input_voltage": 0.0,
        "upower_pv_input_current": 0.0,
        "upower_pv_input_power": 0.0,
        "upower_pv_total_cumulative_charge_energy": 0.0,
        "upower_battery_voltage": 50.82,
        "upower_battery_current": 0.0,
        "upower_battery_state_of_charge": "97",
        "upower_grid_status_register": "00000000 00000000",
        "upower_grid_input_voltage_status": "normal",
        "upower_grid_output_power_load_status": "light",
        "upower_grid_busbar_over_voltage": False,
        "upower_grid_busbar_under_voltage": False,
        "upower_grid_input_over_current": False,
        "upower_grid_abnormal_output_voltage": False,
        "upower_grid_heat_sink_overheating": False,
        "upower_grid_hardware_overvoltage": False,
        "upower_grid_short_circuit": False,
        "upower_grid_low_temperature": False,
        "upower_grid_status": "no",
        "upower_grid_run": False,
        "upower_load_output_status_register": "00000000 00000001",
        "upower_load_output_input_voltage_status": "normal",
        "upower_load_output_output_power_load_status": "light",
        "upower_load_output_output_fail": False,
        "upower_load_output_high_voltage_side_short_circuit": False,
        "upower_load_output_input_over_current": False,
        "upower_load_output_abnormal_output_voltage": False,
        "upower_load_output_unable_to_stop_discharge": False,
        "upower_load_output_unable_to_discharge": False,
        "upower_load_output_short_circuit": False,
        "upower_load_output_abnormal_frequency": False,
        "upower_load_output_high_temperature": False,
        "upower_load_output_low_temperature": False,
        "upower_load_output_run": False,
        "upower_load_output_faults": True,
        "upower_pv_status_register": "00000000 00000000",
        "upower_pv_input_voltage_status": "normal",
        "upower_pv_charging_mos_tube_short_circuit": False,
        "upower_pv_charging_antireverse_mos_tube_open_circuit": False,
        "upower_pv_antireverse_mos_tube_short_circuit": False,
        "upower_pv_input_over_current": False,
        "upower_pv_load_over_current": False,
        "upower_pv_load_short_circuit": False,
        "upower_pv_load_mos_tube_short_circuit": False,
        "upower_pv_run": False,
        "upower_pv_faults": False,
        "upower_pv_charging_status": "no",
        "upower_pv_pv_input_short_circuit": False,
        "upower_pv_led_load_open_circuit": False,
        "upower_pv_three_way_circuit_imbalance": False,
        "upower_battery_status_register": "00000000 00000000",
        "upower_battery_status": "normal",
        "upower_battery_temperature_status": "normal",
        "upower_battery_abnormal_internal_resistance": False,
        "upower_battery_lithium_charging_protection": False,
        "upower_battery_lithium_discharging_protection": False,
        "upower_battery_nominal_voltage_identification_error": False,
        "upower_battery_power": 0.0,
        "upower_load_output_power": 0.0,
    }


def test_parse_line__upower_v1__bug(caplog) -> None:
    """This line contains bad values.

    - one binary value is too big for the intended format
    """
    line = (
        "2874875632772, 229520, UPOWER,23026,0,0,0,5000,4526,0,5,2500,3300,3200,2752,"
        "23026,35,0,5000,12380,0,3200,3100,12502,32,4004,0,1917,0,5,2752,46,0,100,"
        "02874875679644, 229521, COULOMB,12378,32778,690,65535,0,0,100,100,2340,2981,"
        "27564,396,35000,35000,15,"
    )
    data = line_parser.parse_line(line)
    assert data
    data["time"] = data["time"].isoformat()
    assert data == {
        "time": "1970-02-03T06:34:35.632772+00:00",
        "log_id": 229520,
        "line_tag": "UPOWER",
        "upower_grid_input_voltage": 230.26,
        "upower_grid_input_current": 0.0,
        "upower_grid_input_power": 0.0,
        "upower_grid_input_frequency": 50.0,
        "upower_total_cumulative_grid_energy": 45.26,
        "upower_grid_charging_battery_temperature": 25.0,
        "upower_grid_charging_device_temperature": 33.0,
        "upower_grid_charging_power_device_temperature": 32.0,
        "upower_load_input_voltage": 27.52,
        "upower_load_output_voltage": 230.26,
        "upower_load_output_current": 0.35,
        "upower_load_output_frequency": 50.0,
        "upower_load_total_cumulative_energy_consumption": 123.8,
        "upower_load_heat_sink_1_temperature": 32.0,
        "upower_load_heat_sink_2_temperature": 31.0,
        "upower_pv_input_voltage": 125.02,
        "upower_pv_input_current": 0.32,
        "upower_pv_input_power": 40.04,
        "upower_pv_total_cumulative_charge_energy": 19.17,
        "upower_battery_voltage": 27.52,
        "upower_battery_current": 0.46,
        "upower_battery_state_of_charge": "100",
        "upower_grid_status_register": "00000000 00000101",
        "upower_grid_input_voltage_status": "normal",
        "upower_grid_output_power_load_status": "light",
        "upower_grid_busbar_over_voltage": False,
        "upower_grid_busbar_under_voltage": False,
        "upower_grid_input_over_current": False,
        "upower_grid_abnormal_output_voltage": False,
        "upower_grid_heat_sink_overheating": False,
        "upower_grid_hardware_overvoltage": False,
        "upower_grid_short_circuit": False,
        "upower_grid_low_temperature": False,
        "upower_grid_status": "float",
        "upower_grid_run": True,
        "upower_load_output_status_register": "00000000 00000000",
        "upower_load_output_input_voltage_status": "normal",
        "upower_load_output_output_power_load_status": "light",
        "upower_load_output_output_fail": False,
        "upower_load_output_high_voltage_side_short_circuit": False,
        "upower_load_output_input_over_current": False,
        "upower_load_output_abnormal_output_voltage": False,
        "upower_load_output_unable_to_stop_discharge": False,
        "upower_load_output_unable_to_discharge": False,
        "upower_load_output_short_circuit": False,
        "upower_load_output_abnormal_frequency": False,
        "upower_load_output_high_temperature": False,
        "upower_load_output_low_temperature": False,
        "upower_load_output_run": False,
        "upower_load_output_faults": False,
        "upower_pv_status_register": "00000000 00000101",
        "upower_pv_input_voltage_status": "normal",
        "upower_pv_charging_mos_tube_short_circuit": False,
        "upower_pv_charging_antireverse_mos_tube_open_circuit": False,
        "upower_pv_antireverse_mos_tube_short_circuit": False,
        "upower_pv_input_over_current": False,
        "upower_pv_load_over_current": False,
        "upower_pv_load_short_circuit": False,
        "upower_pv_load_mos_tube_short_circuit": False,
        "upower_pv_run": True,
        "upower_pv_faults": False,
        "upower_pv_charging_status": "float",
        "upower_pv_pv_input_short_circuit": False,
        "upower_pv_led_load_open_circuit": False,
        "upower_pv_three_way_circuit_imbalance": False,
        "upower_battery_status_register": None,
        "upower_battery_status": "faults",
        "upower_battery_temperature_status": "over_temperature",
        "upower_battery_abnormal_internal_resistance": True,
        "upower_battery_lithium_charging_protection": True,
        "upower_battery_lithium_discharging_protection": False,
        "upower_battery_nominal_voltage_identification_error": True,
        "upower_battery_power": 12.6592,
        "upower_load_output_power": 80.591,
    }
    assert len(caplog.records) == 2
    rec = caplog.records[1]
    assert rec.levelname == "WARNING"
    assert "struct.error" in rec.msg


# ------------------------- FWv2 log lines -------------------------


def test__parse_line__inverter_v2() -> None:
    line = "1689858525765000, 1292215082, IN,227600,227000,2000,50000,34000,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-07-20T13:08:45.765000+00:00",
        "log_id": 1292215082,
        "line_tag": "IN",
        "inverter_input_voltage": 227.6,
        "inverter_output_voltage": 227.0,
        "inverter_relative_output_power": 80.0,
        "inverter_output_frequency": 50.0,
        "inverter_temperature": 34.0,
    }


def test__parse_line__inverter_v2__missing() -> None:
    line = "1689858525765000, 1292215082, IN,,,,,,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-07-20T13:08:45.765000+00:00",
        "log_id": 1292215082,
        "line_tag": "IN",
        "inverter_input_voltage": None,
        "inverter_output_voltage": None,
        "inverter_relative_output_power": None,
        "inverter_output_frequency": None,
        "inverter_temperature": None,
    }


def test__parse_line__mppt_v2() -> None:
    line = "56131937440, 1292230827, MP,506,300,2522,456,2439,2196,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "1970-01-01T15:35:31.937440+00:00",
        "log_id": 1292230827,
        "line_tag": "MP",
        "mppt_panel_voltage": 5.06,
        "mppt_panel_current": 3.0,
        "mppt_battery_voltage": 25.22,
        "mppt_battery_current": 4.56,
        "mppt_internal_temperature": 24.39,
        "mppt_battery_temperature": 21.96,
    }


def test__parse_line__mppt_v2__missing() -> None:
    line = "56131937440, 1292230827, MP,,,,,,,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "1970-01-01T15:35:31.937440+00:00",
        "log_id": 1292230827,
        "line_tag": "MP",
        "mppt_panel_voltage": None,
        "mppt_panel_current": None,
        "mppt_battery_voltage": None,
        "mppt_battery_current": None,
        "mppt_internal_temperature": None,
        "mppt_battery_temperature": None,
    }


def test__parse_line__coulomb_v2() -> None:
    line = "1693330416700944, 5105, CO,5,65535,459,7,30,95,11,808,42,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T17:33:36.700944+00:00",
        "log_id": 5105,
        "line_tag": "CO",
        "coulomb_gauge_status": 5,
        "coulomb_time_to_empty": 65535,
        "coulomb_time_to_full": 459,
        "coulomb_cycle_count": 7,
        "coulomb_state_of_charge": 30,
        "coulomb_state_of_health": 95,
        "coulomb_battery_voltage": 0.011,
        "coulomb_battery_current": 0.808,
        "coulomb_calibration_status": 42,
        "coulomb_calibration_complete": False,
    }


def test__parse_line__coulomb_v2__missing() -> None:
    line = "1693330416700944, 5105, CO,,,,,,,,,,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T17:33:36.700944+00:00",
        "log_id": 5105,
        "line_tag": "CO",
        "coulomb_gauge_status": None,
        "coulomb_time_to_empty": None,
        "coulomb_time_to_full": None,
        "coulomb_cycle_count": None,
        "coulomb_state_of_charge": None,
        "coulomb_state_of_health": None,
        "coulomb_battery_voltage": None,
        "coulomb_battery_current": None,
        "coulomb_calibration_status": None,
        "coulomb_calibration_complete": None,
    }


def test__parse_line__metering_v2() -> None:
    line = (
        "1693299031011312, 35038,"
        " ME,239402,8243905,1260518,1044112,1272833,9064969,0,5616,-105,2361,"
    )
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T08:50:31.011312+00:00",
        "log_id": 35038,
        "line_tag": "ME",
        "metering_pv_e_in": 239402.0,
        "metering_load_e_battery": 8243905.0,
        "metering_load_e_grid": 1260518.0,
        "metering_grid_e_charged": 1044112.0,
        "metering_bat_e_charged": 1272833.0,
        "metering_bat_e_discharged": 9064969.0,
        "metering_bat_v_min": 0.0,
        "metering_bat_v_max": 56.16,
        "metering_bat_i_min": -1.05,
        "metering_bat_i_max": 23.61,
    }


def test__parse_line__metering_v2__missing() -> None:
    line = "1693299031011312, 35038, ME,,,,,,,,,,,"
    data = line_parser.parse_line(line)
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T08:50:31.011312+00:00",
        "log_id": 35038,
        "line_tag": "ME",
        "metering_pv_e_in": None,
        "metering_load_e_battery": None,
        "metering_load_e_grid": None,
        "metering_grid_e_charged": None,
        "metering_bat_e_charged": None,
        "metering_bat_e_discharged": None,
        "metering_bat_v_min": None,
        "metering_bat_v_max": None,
        "metering_bat_i_min": None,
        "metering_bat_i_max": None,
    }


def test__parse_line__upower_v2() -> None:
    line = "1693318853807636, 76865, UP,,,,,3200,22873,0,8973,338,5334,1062,,39,,,"
    data = line_parser.parse_line(line)
    assert data is not None
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T14:20:53.807636+00:00",
        "log_id": 76865,
        "line_tag": "UP",
        "upower_input_voltage": None,
        "upower_input_current": None,
        "upower_input_frequency": None,
        "upower_battery_temperature": None,
        "upower_device_temperature": 32.0,
        "upower_output_voltage": 228.73,
        "upower_output_current": 0.0,
        "upower_pv_input_voltage": 89.73,
        "upower_pv_input_current": 3.38,
        "upower_battery_voltage": 53.34,
        "upower_battery_current": None,
        "upower_SOC": 39,
        "upower_protocol_type": None,
    }


def test__parse_line__upower_v2__missing() -> None:
    line = "1693318853807636, 76865, UP,,,,,,,,,,,,,,,,"
    data = line_parser.parse_line(line)
    assert data is not None
    assert stringify_dates_in_sample(data) == {
        "time": "2023-08-29T14:20:53.807636+00:00",
        "log_id": 76865,
        "line_tag": "UP",
        "upower_input_voltage": None,
        "upower_input_current": None,
        "upower_input_frequency": None,
        "upower_battery_temperature": None,
        "upower_device_temperature": None,
        "upower_output_voltage": None,
        "upower_output_current": None,
        "upower_pv_input_voltage": None,
        "upower_pv_input_current": None,
        "upower_battery_voltage": None,
        "upower_battery_current": None,
        "upower_SOC": None,
        "upower_protocol_type": None,
    }


# ------------------------- bitfields -------------------------


def test__parse_epsolar_charging_status() -> None:
    assert line_parser._parse_epsolar_grid_status(0b0100101010001111) == {
        "grid_status_register": "01001010 10001111",
        "grid_input_voltage_status": "low",
        "grid_output_power_load_status": "light",
        "grid_busbar_over_voltage": False,
        "grid_busbar_under_voltage": False,
        "grid_input_over_current": True,
        "grid_abnormal_output_voltage": False,
        "grid_heat_sink_overheating": True,
        "grid_hardware_overvoltage": False,
        "grid_short_circuit": True,
        "grid_low_temperature": False,
        "grid_status": "equalizing",
        "grid_run": True,
    }


def test__parse_epsolar_load_output_status() -> None:
    assert line_parser._parse_epsolar_load_output_status(0b0100101010001111) == {
        "load_output_status_register": "01001010 10001111",
        "load_output_input_voltage_status": "low",
        "load_output_output_power_load_status": "light",
        "load_output_output_fail": False,
        "load_output_high_voltage_side_short_circuit": False,
        "load_output_input_over_current": True,
        "load_output_abnormal_output_voltage": False,
        "load_output_unable_to_stop_discharge": True,
        "load_output_unable_to_discharge": False,
        "load_output_short_circuit": True,
        "load_output_abnormal_frequency": False,
        "load_output_high_temperature": True,
        "load_output_low_temperature": True,
        "load_output_run": True,
        "load_output_faults": True,
    }


def test__parse_epsolar_charging_device_status() -> None:
    assert line_parser._parse_epsolar_pv_status(0b0100101010001111) == {
        "pv_status_register": "01001010 10001111",
        "pv_input_voltage_status": "without",
        "pv_charging_mos_tube_short_circuit": False,
        "pv_charging_antireverse_mos_tube_open_circuit": False,
        "pv_antireverse_mos_tube_short_circuit": True,
        "pv_input_over_current": False,
        "pv_load_over_current": True,
        "pv_load_short_circuit": False,
        "pv_load_mos_tube_short_circuit": True,
        "pv_run": True,
        "pv_faults": True,
        "pv_charging_status": "equalizing",
        "pv_pv_input_short_circuit": False,
        "pv_led_load_open_circuit": False,
        "pv_three_way_circuit_imbalance": False,
    }


@pytest.mark.parametrize(
    ("bits", "bit_string", "status", "temp_status"),
    [
        (0b0100101000000000, "01001010 00000000", "normal", "normal"),
        (0b0100101000010001, "01001010 00010001", "over_voltage", "over_temperature"),
        (0b0100101000100010, "01001010 00100010", "under_voltage", "under_temperature"),
        (0b0100101000110011, "01001010 00110011", "over_discharge", "faults"),
        (0b0100101000110111, "01001010 00110111", "faults", "faults"),
    ],
)
def test__parse_epsolar_battery_status(bits, bit_string, status, temp_status) -> None:
    parsed_status = line_parser._parse_epsolar_battery_status(bits)
    assert parsed_status == {
        "battery_status_register": bit_string,
        "battery_status": status,
        "battery_temperature_status": temp_status,
        "battery_abnormal_internal_resistance": False,
        "battery_lithium_charging_protection": True,
        "battery_lithium_discharging_protection": False,
        "battery_nominal_voltage_identification_error": False,
    }
