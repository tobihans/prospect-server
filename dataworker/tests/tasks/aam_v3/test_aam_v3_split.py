import datetime
from textwrap import dedent

import tasks.ingestion.aam_v3.split
from tasks.ingestion.aam_v3.split import FileMetadata

# ------------------------- split file parts -------------------------


def test__extract_json_header() -> None:
    file_data = dedent(
        """\
        {"a":123,"b":"something"}
        ---
        123,456,a string,data
        789,1012,value,id
        """
    )
    file_metadata, file_data_rest = tasks.ingestion.aam_v3.split._extract_json_header(
        file_data
    )
    assert file_metadata == {"a": 123, "b": "something"}
    assert file_data_rest == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )


def test__extract_file_metadata() -> None:
    file_data = dedent(
        """\
        {"last_modified": "2023-08-31 19:14:23 UTC",
        "key": "5031/a2ei_1691083416104.log",
        "etag": "\\"ac10229b0d7b0307a59491699b8854cb\\""}
        ---
        123,456,a string,data
        789,1012,value,id
        """
    )
    file_metadata, file_data_rest = tasks.ingestion.aam_v3.split._extract_file_metadata(
        file_data
    )
    assert file_metadata == FileMetadata(
        device_id=5031,
        file_creation_time=datetime.datetime(
            2023, 8, 3, 17, 23, 36, tzinfo=datetime.UTC
        ),
        file_upload_time=datetime.datetime(
            2023, 8, 31, 19, 14, 23, tzinfo=datetime.UTC
        ),
    )
    assert file_data_rest == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )


def test__extract_device_info() -> None:
    file_data = dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        {
        "key": "value",
        "ki": "valiu"
        }
        """
    )
    device_info, file_data_rest = tasks.ingestion.aam_v3.split._extract_device_info(
        file_data
    )
    assert device_info == {"key": "value", "ki": "valiu"}
    assert file_data_rest == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )


def test__extract_device_info__no_info_block() -> None:
    """Earlier firmware version did not include device info."""
    file_data = dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )
    device_info, file_data_rest = tasks.ingestion.aam_v3.split._extract_device_info(
        file_data
    )
    assert device_info == {}
    assert file_data_rest == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )


def test__extract_device_info__repeated() -> None:
    """Device info extraction is consistent even when info is repeated.

    In some files, the device info block is repeated. The extraction should
    be robust to this and produce the same result.
    """
    file_data = dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        {
        "key": "value",
        "ki": "valiu"
        }
        {
        "key": "value",
        "ki": "valiu"
        }
        {
        "key": "value",
        "ki": "valiu"
        }
        """
    )
    device_info, file_data_rest = tasks.ingestion.aam_v3.split._extract_device_info(
        file_data
    )
    assert device_info == {"key": "value", "ki": "valiu"}
    assert file_data_rest == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )


def test__convert_file_metadata() -> None:
    file_metadata = {
        "last_modified": "2023-08-31 19:14:23 UTC",
        "key": "5031/a2ei_1691083416104.log",
        "etag": '"ac10229b0d7b0307a59491699b8854cb"',
    }
    assert tasks.ingestion.aam_v3.split._convert_file_metadata(
        file_metadata
    ) == FileMetadata(
        device_id=5031,
        file_creation_time=datetime.datetime(
            2023, 8, 3, 17, 23, 36, tzinfo=datetime.UTC
        ),
        file_upload_time=datetime.datetime(
            2023, 8, 31, 19, 14, 23, tzinfo=datetime.UTC
        ),
    )


def test__parse_file_parts() -> None:
    file_data = dedent(
        """\
        {"last_modified": "2023-08-31 19:14:23 UTC",
        "key": "5031/a2ei_1691083416104.log",
        "etag": "\\"ac10229b0d7b0307a59491699b8854cb\\""}
        ---
        123,456,a string,data
        789,1012,value,id
        {
        "key": "value",
        "ki": "valiu"
        }
        """
    )
    (
        file_metadata,
        device_info,
        sample_data,
    ) = tasks.ingestion.aam_v3.split.split_file(file_data)
    assert file_metadata == FileMetadata(
        device_id=5031,
        file_creation_time=datetime.datetime(
            2023, 8, 3, 17, 23, 36, tzinfo=datetime.UTC
        ),
        file_upload_time=datetime.datetime(
            2023, 8, 31, 19, 14, 23, tzinfo=datetime.UTC
        ),
    )

    assert device_info == {"key": "value", "ki": "valiu"}
    assert sample_data == dedent(
        """\
        123,456,a string,data
        789,1012,value,id
        """
    )
