"""Test WH imputation."""

import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import fetch_one_row, table_content
from dataworker.task import InternalTask
from tasks.internal import backfill_shs_wh
from tests.utils import insert_records, load_test_data


@pytest.fixture(autouse=True)
def _module_autouse(_database: None) -> None:
    """Common fixtures required for this module."""
    ...


def test__task() -> None:
    # insert some test data into data_meters_ts.
    data_shs_ts = load_test_data("records_for_task_test/shs_ts_sample_wh_impute.json")
    insert_records("data_shs_ts", data_shs_ts)

    # validate value before.
    one_row = fetch_one_row("data_shs_ts")
    assert one_row["output_energy_interval_wh"] is None

    # run processor with test data.
    update_backfill_shs_wh_task = InternalTask.from_function(backfill_shs_wh.run)
    update_backfill_shs_wh_task()

    # validate value after.
    one_row = fetch_one_row("data_shs_ts")

    assert one_row["output_energy_interval_wh"] == 18.2
    assert table_content("data_shs_ts") == snapshot([
        {
            "battery_charge_percent": None,
            "battery_io_w": None,
            "battery_v": None,
            "created_at": datetime.datetime(2023, 4, 18, 14, 42, 9, 93283),
            "custom": {},
            "data_origin": "victron",
            "device_uid": "12_victron_293432",
            "grid_energy_interval_wh": None,
            "grid_energy_lifetime_wh": None,
            "grid_input_v": None,
            "grid_input_w": None,
            "import_id": 2618790,
            "interval_seconds": 3600,
            "manufacturer": "victron",
            "metered_at": datetime.datetime(2023, 4, 17, 15, 50),
            "organization_id": 12,
            "output_energy_interval_wh": 18.2,
            "output_energy_lifetime_wh": None,
            "paygo_days_left": None,
            "paygo_is_paid_off": None,
            "pv_energy_interval_wh": None,
            "pv_energy_lifetime_wh": None,
            "pv_input_w": None,
            "serial_number": "293432",
            "source_id": 126,
            "system_output_w": 18.2,
            "updated_at": datetime.datetime(2023, 4, 18, 14, 42, 9, 93289),
        }
    ])
