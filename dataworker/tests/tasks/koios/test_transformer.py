from copy import deepcopy

import pandas as pd
import pytest

from dataworker.dataset import key_set
from tasks.ingestion import koios
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def _koios_json_data():
    """Load test data from file."""
    return load_test_data("datafile/koios_datafile.json")


@pytest.fixture
def koios_data(_koios_json_data):
    """Copy test data for use at function level."""
    return deepcopy(_koios_json_data)


# --------------------------------------------------


def test__extract_meters_ts_data(koios_data) -> None:
    meters_ts = koios.extract_meters_ts_data(koios_data)
    # number of object extracted
    assert len(meters_ts) == 5
    # keys
    assert key_set(meters_ts) == {
        "serial_number",
        "metered_at_str",
        "phase",
        "voltage_v",
        "power_factor",
        "power_w",
        "energy_interval_wh",
        "frequency_hz",
        "current_a",
        "interval_start",
        "interval_end",
        "billing_cycle_start_at",
        "manufacturer",
        "metered_at",
        "start",
        "end",
        "interval",
    }
    # test the first records for exact values
    assert meters_ts[0] == {
        "serial_number": "SMRSDRF-02-00037425",
        "metered_at_str": "2024-01-31T13:00:00",
        "phase": "ABC",  # crap value will be nullified later, handling enums
        "voltage_v": 188.4099884033203,
        "power_factor": 0.6340000033378601,
        "power_w": 16.0,
        "energy_interval_wh": 76.303,
        "frequency_hz": 49.04999923706055,
        "current_a": 0.09600000083446503,
        "interval_start": "2024-01-31T13:00:00",
        "interval_end": "2024-01-31T13:15:00",
        "billing_cycle_start_at": None,
        "manufacturer": "Sparkmeter",
        "metered_at": pd.to_datetime("2024-01-31 13:00:00"),
        "start": pd.to_datetime("2024-01-31 13:00:00"),
        "end": pd.to_datetime("2024-01-31 13:15:00"),
        "interval": 900.0,
    }
    # test phase values, although handled later
    assert str(meters_ts[1]["phase"]) == "1"
    assert str(meters_ts[2]["phase"]) == "A"
    assert str(meters_ts[3]["phase"]) == "b"
    assert meters_ts[4]["phase"] is None

    # test interval values when no measurement is given
    assert meters_ts[4]["interval_start"] is None
    assert meters_ts[4]["interval_end"] is None
    assert meters_ts[4]["interval"] == 0.0


@pytest.mark.parametrize(
    ("tariff_val", "max_power_w"),
    [
        (None, None),
        (0, 0),
        ("1000", "1000"),
        (20000, 20000),
        (
            [
                {"start_time": "00:00", "load_limit": 0},
                {"start_time": "09:00", "load_limit": 20000},
            ],
            20000,
        ),
    ],
)
@pytest.mark.usefixtures("_mocked_address_object")
def test__extract_meters_ts_data_various_tariff(
    koios_data, tariff_val, max_power_w
) -> None:
    one_data = {"customers": [koios_data["customers"][0]]}
    one_data["customers"][0]["meters"][0]["tariff"]["load_limit"]["value"] = tariff_val
    meters = koios.extract_meters_data(one_data)
    assert len(meters) == 1
    assert meters[0]["max_power_w"] == max_power_w


@pytest.mark.usefixtures("_mocked_address_object")
def test__extract_meters_data(koios_data) -> None:
    meters = koios.extract_meters_data(koios_data)
    # number of object extracted
    assert len(meters) == 5
    # keys
    assert key_set(meters) == {
        "customer_external_id",
        "customer_name_p",
        "customer_name_e",
        "customer_phone_e",
        "customer_phone_p",
        "serial_number",
        "grid_name",
        "manufacturer",
        "is_test",
        "customer_address_e",
        "customer_address_p",
        "customer_latitude_b",
        "customer_longitude_b",
        "device_external_id",
        "customer_category",
        "account_external_id",
        "payment_plan",
        "max_power_w",
        "customer_country",
        "customer_location_area_1",
        "customer_location_area_2",
    }
    # test the first records for exact values
    assert meters[0] == {
        "customer_external_id": "6f4796f5-cd3d-43d2-8b13-6138572a60af",
        "customer_name_p": "942bb8b13e11a401daadc9e50f07a2892ea086db57bb3e31ad26319c4b9636b3",  # noqa: E501
        "customer_name_e": None,
        "customer_phone_e": None,
        "customer_phone_p": None,
        "serial_number": "SMRSDRF-02-00037425",
        "grid_name": "ac071a94-de1d-43bb-b7ff-fa0a24f02709",
        "manufacturer": "Sparkmeter",
        "is_test": False,
        "customer_address_e": None,
        "customer_address_p": "0194f6a8929a440332fe51ade6ca561935b1a965a31187b193a8990aadcca4ef",  # noqa: E501
        "customer_latitude_b": -15.4,
        "customer_longitude_b": 28.3,
        "device_external_id": "1aab2c8b-f8f6-4f1c-869f-87820a500742",
        "customer_category": "Residential",
        "account_external_id": "188cf040-1401-4c40-bb1f-63c81581511c",
        "payment_plan": "Residential",
        "max_power_w": 25000,
        "customer_country": "ZM",
        "customer_location_area_1": "Northern Province",
        "customer_location_area_2": "Chilubi District",
    }
