import datetime

import pytest

from dataworker.dataset import key_set
from tasks.ingestion import airtel_zm
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def airtel_data():
    """Load test data from file."""
    return load_test_data("datafile/airtel_zm.json")


# --------------------------------------------------


def test__extract_payments_ts_data(airtel_data) -> None:
    payments_ts = airtel_zm.extract_payments_ts_data(airtel_data)

    # number of object extracted
    assert len(payments_ts) == 2
    # keys
    assert key_set(payments_ts) == {
        "payee_grade",
        "payer_user_name_p",
        "payee_category",
        "payer_account_e",
        "payer_account_p",
        "payee_nickname",
        "payer_user_name_e",
        "paid_at",
        "purpose",
        "amount",
        "provider_name",
        "payer_nickname_e",
        "payer_nickname_p",
        "payee_account",
        "currency",
        "payer_phone_e",
        "account_origin",
        "payee_phone",
        "payer_grade",
        "payment_external_id",
        "account_external_id",
        "payer_category",
        "payer_phone_p",
        "payee_user_name",
    }
    # test the first records for exact values
    assert payments_ts[0] == {
        "payment_external_id": "MP240206.1904.N12345",
        "account_external_id": "unknown",
        "paid_at": datetime.datetime(2024, 2, 6, 19, 4, 41, tzinfo=datetime.UTC),
        "amount": "130",
        "provider_name": "airtel_zm",
        "purpose": "mobile money statement",
        "currency": "ZMW",
        "account_origin": "meters",
        "payer_account_e": None,
        "payer_account_p": "ba7bfbd110b9f588a33576a6218fd8d5e29fe1aa1320e59c8c525bf22417a71f",  # noqa: E501
        "payer_user_name_e": None,
        "payer_user_name_p": "e56dd02199c6eca5809cfe6fc1e2606a1eb185333ab00d43bbbaf0d5c10be2be",  # noqa: E501
        "payer_nickname_e": None,
        "payer_nickname_p": None,
        "payer_phone_e": None,
        "payer_phone_p": "ba7bfbd110b9f588a33576a6218fd8d5e29fe1aa1320e59c8c525bf22417a71f",  # noqa: E501
        "payer_grade": "Gold Subscriber",
        "payer_category": "Subscriber",
        "payee_account": "889073368",
        "payee_user_name": "EMERGING COOKING SOLUTIONS",
        "payee_nickname": "XXXXMOTO",
        "payee_phone": "889073368",
        "payee_grade": "2PercentMerchant",
        "payee_category": "Head Merchant",
    }
