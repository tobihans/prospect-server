# ruff: noqa: E501
import datetime

import pytest
from inline_snapshot import snapshot

from dataworker.queries import count_rows, table_content
from dataworker.task import IngestionTask
from tasks.ingestion import airtel_zm


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


def test__etl() -> None:
    upya_etl = IngestionTask("airtel", airtel_zm.transform)
    upya_etl(
        import_id="123456789",
        source_id="123456789",
        file_key="datafile/airtel_zm.json",
        file_format="application/json",
        data_origin="over_there",
        organization_id="1234",
    )
    # Check for successfull insert.
    assert count_rows("data_payments_ts") == 2
    assert table_content(
        "data_payments_ts", shed_cols=["created_at", "updated_at"]
    ) == snapshot([
        {
            "account_external_id": "unknown",
            "account_origin": "meters",
            "account_uid": "1234_over_there_unknown",
            "amount": 130.0,
            "currency": "ZMW",
            "custom": {
                "payee_grade": "2PercentMerchant",
                "payee_phone": "889073368",
                "payer_grade": "Gold Subscriber",
                "payee_account": "889073368",
                "payer_phone_p": "ba7bfbd110b9f588a33576a6218fd8d5e29fe1aa1320e59c8c525bf22417a71f",
                "payee_category": "Head Merchant",
                "payee_nickname": "XXXXMOTO",
                "payer_category": "Subscriber",
                "payee_user_name": "EMERGING COOKING SOLUTIONS",
                "payer_account_p": "ba7bfbd110b9f588a33576a6218fd8d5e29fe1aa1320e59c8c525bf22417a71f",
                "payer_user_name_p": "e56dd02199c6eca5809cfe6fc1e2606a1eb185333ab00d43bbbaf0d5c10be2be",
            },
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2024, 2, 6, 19, 4, 41),
            "payment_external_id": "MP240206.1904.N12345",
            "provider_name": "airtel_zm",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "mobile money statement",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_unknown_MP240206.1904.N12345_2024-02-06T19:04:41+00:00",
        },
        {
            "account_external_id": "unknown",
            "account_origin": "meters",
            "account_uid": "1234_over_there_unknown",
            "amount": 100.0,
            "currency": "ZMW",
            "custom": {
                "payee_grade": "2PercentMerchant",
                "payee_phone": "889073368",
                "payer_grade": "Gold Subscriber",
                "payee_account": "889073368",
                "payer_phone_p": "116185b60e6dc0099b306a74ef18fdf4fda8c641eea03266e81fd2e5148f5fc1",
                "payee_category": "Head Merchant",
                "payee_nickname": "XXXXMOTO",
                "payer_category": "Subscriber",
                "payee_user_name": "EMERGING COOKING SOLUTIONS",
                "payer_account_p": "116185b60e6dc0099b306a74ef18fdf4fda8c641eea03266e81fd2e5148f5fc1",
                "payer_user_name_p": "3edef253f4e39a3a132da09fa3c26716fb81e3df4d1724782af2ab8ca06a9e16",
            },
            "data_origin": "over_there",
            "days_active": None,
            "import_id": 123456789,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 1234,
            "paid_at": datetime.datetime(2024, 2, 6, 19, 42, 15),
            "payment_external_id": "MP240206.1942.K12345",
            "provider_name": "airtel_zm",
            "provider_transaction_id": None,
            "purchase_item": None,
            "purchase_quantity": None,
            "purchase_unit": None,
            "purpose": "mobile money statement",
            "reverted_at": None,
            "source_id": 123456789,
            "uid": "meters_1234_over_there_unknown_MP240206.1942.K12345_2024-02-06T19:42:15+00:00",
        },
    ])
