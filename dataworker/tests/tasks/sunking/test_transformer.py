import pytest

from dataworker.dataset import key_set
from tasks.ingestion import sunking
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def sunking_data():
    """Load test data from file."""
    return load_test_data("datafile/sunking.json")


# --------------------------------------------------


def test__extract_shs_data(sunking_data) -> None:
    shs = sunking.extract_shs_data(sunking_data)
    # number of object extracted
    assert len(shs) == 1
    # keys
    assert key_set(shs) == {
        "account_external_id",
        "customer_external_id",
        "device_external_id",
        "serial_number",
        "manufacturer",
    }
    # test the first record for exact values
    assert shs[0] == {
        "account_external_id": "142806611",
        "customer_external_id": "142806611",
        "device_external_id": "142806611",
        "manufacturer": "sunking",
        "serial_number": "142806611",
    }


def test__extract_shs_ts_data(sunking_data) -> None:
    shs_ts = sunking.extract_shs_ts_data(sunking_data)
    # number of object extracted
    assert len(shs_ts) == 27
    # keys
    assert key_set(shs_ts) == {
        "battery_charge_percent",
        "battery_io_w",
        "energy_charged_interval_wh",
        "energy_discharged_interval_wh",
        "firmware_version",
        "grid_input_v",
        "interval_seconds",
        "manufacturer",
        "metered_at",
        "output_energy_interval_wh",
        "pv_current_a",
        "pv_energy_interval_wh",
        "pv_input_w",
        "pv_voltage_v",
        "serial_number",
        "system_output_w",
    }
    # test the first record for exact values
    assert shs_ts[0] == {
        "battery_charge_percent": 6.7,
        "battery_io_w": -2.0,
        "energy_charged_interval_wh": 0,
        "energy_discharged_interval_wh": 1,
        "firmware_version": "B01F0504",
        "grid_input_v": 0,
        "interval_seconds": 1800,
        "manufacturer": "sunking",
        "metered_at": "2024-06-09 20:19:38",
        "output_energy_interval_wh": 0.0,
        "pv_current_a": 0.0,
        "pv_energy_interval_wh": 0.0,
        "pv_input_w": 0.0,
        "pv_voltage_v": 0.0,
        "serial_number": "142806611",
        "system_output_w": 0,
    }
