from datetime import UTC, datetime

import pytest
import sqlalchemy as sqla

from dataworker.database import get_or_create_connection, get_table
from dataworker.queries import fetch_one_row
from dataworker.task import InternalTask
from tasks.internal import update_demo_data_timestamps
from tests.utils import insert_records, load_test_data


@pytest.mark.usefixtures("_database")
def test__task() -> None:
    # insert some test data into data_meters_ts.
    data_meters_ts = load_test_data(
        "records_for_task_test/meters_ts_sample_demo_timestamps.json"
    )
    insert_records("data_meters_ts", data_meters_ts)

    # validate value before.
    one_row = fetch_one_row("data_meters_ts", ("serial_number", "541821"))
    assert one_row["metered_at"] == datetime(2023, 4, 17, 15, 50)

    # insert organization data via sqla connection.
    connection = get_or_create_connection()
    organizations = get_table("organizations")
    now = datetime.now(UTC)
    statement = sqla.insert(organizations).values(
        created_at=now, updated_at=now, id=1, name="Prospect Demo"
    )
    connection.execute(statement)

    # run processor with test data.
    update_demo_data_timestamps_task = InternalTask.from_function(
        update_demo_data_timestamps.run
    )
    update_demo_data_timestamps_task()

    # validate value after.
    one_row = fetch_one_row("data_meters_ts", ("serial_number", "541821"))
    assert one_row["metered_at"] == datetime(2023, 4, 18, 15, 50)
