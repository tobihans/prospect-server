import pytest

import tests.utils
from dataworker.dataset import key_set
from tasks.ingestion import stellar
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def stellar_data():
    """Load test data from file."""
    return load_test_data("datafile/stellar.json")


@pytest.fixture(scope="module")
def stellar_ffill_1_data():
    """Load test data from file."""
    return load_test_data("datafile/stellar_ffill_1.json")


@pytest.fixture(scope="module")
def meters_ts_sample_data():
    """Load test data from file."""
    return load_test_data("records_for_task_test/meters_ts_sample_for_stellar.json")


@pytest.fixture(autouse=True)
def _module_autouse(_database) -> None:
    """Common fixtures required for this module."""
    ...


# --------------------------------------------------


def test__extract_meters_ts_data(stellar_data, meters_ts_sample_data) -> None:
    # insert some test data into data_meters_ts
    tests.utils.insert_records("data_meters_ts", meters_ts_sample_data)

    meters_ts = stellar.extract_meters_ts_data(stellar_data)
    # number of object extracted
    assert len(meters_ts) == 5
    # keys
    assert key_set(meters_ts) == {
        "power_w",
        "interval_seconds",
        "metered_at",
        "voltage_v",
        "manufacturer",
        "energy_lifetime_wh",
        "current_a",
        "frequency_hz",
        "serial_number",
        "power_factor",
        "signal_strength",
        "voltage_v_phase_1",
        "voltage_v_phase_2",
        "voltage_v_phase_3",
        "power_factor_phase_1",
        "power_factor_phase_2",
        "power_factor_phase_3",
        "power_w_phase_1",
        "power_w_phase_2",
        "power_w_phase_3",
        "current_a_phase_1",
        "current_a_phase_2",
        "current_a_phase_3",
    }

    # test the first records for exact values
    assert meters_ts[0] == {
        "metered_at": "2023-04-19T11:50:00.000Z",
        "manufacturer": "HOP",
        "serial_number": 541821,
        "power_factor": 0.943,
        "power_w": 108.2,
        "energy_lifetime_wh": 258650.0,
        "frequency_hz": 50.04,
        "signal_strength": 16.0,
        "voltage_v_phase_1": 204.9,
        "voltage_v_phase_2": None,
        "voltage_v_phase_3": None,
        "power_factor_phase_1": 0.937,
        "power_factor_phase_2": None,
        "power_factor_phase_3": None,
        "power_w_phase_1": 107.1,
        "power_w_phase_2": None,
        "power_w_phase_3": None,
        "current_a_phase_1": 0.551,
        "current_a_phase_2": None,
        "current_a_phase_3": None,
        "interval_seconds": 600,
        "voltage_v": 204.9,
        "current_a": 0.551,
    }


def test__extract_meters_data(stellar_data) -> None:
    meters = stellar.extract_meters_data(stellar_data)
    # number of object extracted
    assert len(meters) == 3
    # keys
    assert key_set(meters) == {
        "manufacturer",
        "customer_country",
        "customer_latitude_p",
        "customer_address_e",
        "customer_latitude_b",
        "customer_longitude_e",
        "installation_date",
        "customer_longitude_b",
        "customer_longitude_p",
        "customer_latitude_e",
        "device_external_id",
        "serial_number",
        "customer_external_id",
        "customer_name_e",
        "customer_phone_p",
        "customer_address_p",
        "customer_name_p",
        "customer_phone_e",
    }
    # test the first records for exact values
    assert meters[0] == {
        "customer_external_id": "009",
        "customer_name_p": None,
        "customer_name_e": None,
        "customer_address_p": None,
        "customer_address_e": None,
        "customer_country": "NG",
        "customer_latitude_b": 9.086,
        "customer_longitude_b": 7.212,
        "customer_latitude_p": "f800209be3d97211342c9f822c35503e04c1bea185a841a28d7f8cdeec69210d",  # noqa: E501
        "customer_longitude_p": "4cfdd4f57818b020a45c41e6a05ca9ea2a05db5feb7a3d0a86dde76ad42fc14f",  # noqa: E501
        "customer_latitude_e": None,
        "customer_longitude_e": None,
        "customer_phone_p": None,
        "customer_phone_e": None,
        "device_external_id": 541810,
        "serial_number": 541810,
        "installation_date": "2019-11-29",
        "manufacturer": "HOP",
    }


# --------------------------------------------------

# Test bad api data.

STELLAR_WRONG_FORMAT_SAMPLE_DICT = {
    "meters": [
        {
            "customer_birth_year": "",
            "customer_external_id": 2062,
            "customer_former_electricity_source": "",
            "customer_gender": "",
            "customer_household_size": "",
            "customer_id_number_e": "",
            "customer_id_number_p": "",
            "customer_id_type": "",
            "customer_name_e": "",
            "customer_name_p": "",
        }
    ]
}


def test__extract_meters_ts_data__wrong_key(meters_ts_sample_data) -> None:
    # insert some test data into data_meters_ts
    tests.utils.insert_records("data_meters_ts", meters_ts_sample_data)

    meters_ts = stellar.extract_meters_ts_data(STELLAR_WRONG_FORMAT_SAMPLE_DICT)
    # number of object extracted
    assert len(meters_ts) == 0
    # confirm it's empty.
    assert meters_ts == []


def test__extract_meters_data__wrong_key() -> None:
    meters = stellar.extract_meters_data(STELLAR_WRONG_FORMAT_SAMPLE_DICT)
    # number of object extracted
    assert len(meters) == 0
    # confirm it's empty.
    assert meters == []


# --------------------------------------------------

# Test ffill behaviour.


def test__ffill_nans(stellar_ffill_1_data, meters_ts_sample_data) -> None:
    # insert some test data into data_meters_ts
    tests.utils.insert_records("data_meters_ts", meters_ts_sample_data)

    # CASE 1
    # Incoming data is brand new meter with initial NaN.
    # We try ffill, but no data to ffill in DB.
    # We return original dataset.
    result = stellar.extract_meters_ts_data(stellar_ffill_1_data)
    # assert that dataset is still returned with original None Value.
    assert result[0]["energy_lifetime_wh"] is None
    assert (len(stellar_ffill_1_data["data"][0]["rawData_response"]["data"])) == len(
        result
    )
