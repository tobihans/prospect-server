import datetime

import pytest
import time_machine
from inline_snapshot import snapshot

from dataworker.dataset import RecordSet
from dataworker.metadata import ImportContext
from dataworker.queries import table_content
from dataworker.task import InternalTask
from tasks.internal.fuel_sale_wallet_matches_validated_payment import (
    PAYMENTS_TABLE,
    VERIFICATIONS_TABLE,
    construct_wallets,
    fetch_statement_verified_uids,
    is_verified_against_wallet,
    prune_payload,
    run,
)
from tests.utils import RecordSetWithModifiers, prepare_test_data, summarize_records


@pytest.fixture(autouse=True)
def _module_autouse(_database: None) -> None:
    """Common fixtures required for this module."""
    ...


# payment record
PAYMENTS_SAMPLE_DICT = {
    "payment_external_id": "123",
    "amount": 777.0,
    "currency": "UGX",
    "purpose": "payment",
    "paid_at": "2024-04-16 14:00:00",
    "account_external_id": "456",
    "account_origin": "meters",
    "provider_name": "some_provider",
    "purchase_item": "anything",
}
PAYMENTS_SAMPLE_UID = "meters_222222_solar_coop_456_123_2024-04-16T14:00:00+00:00"
# order record
PAYMENTS_ORDER_SAMPLE_DICT = {
    "payment_external_id": "123",
    "amount": 777.0,
    "currency": "UGX",
    "purpose": "order",
    "paid_at": "2024-04-17 14:00:00",
    "account_external_id": "456",
    "account_origin": "meters",
    "provider_name": "some_provider_name",
    "purchase_item": "PELLETS",
}
# data_trust_trace record
VERIFICATIONS_SAMPLE_DICT = {
    "subject_uid": PAYMENTS_SAMPLE_UID,
    "subject_origin": "payments_ts",
    "check": "crm_payment_backed_by_payment_statement",
    "result": "ok",
}
SAMPLE_VERIFIED_UID = VERIFICATIONS_SAMPLE_DICT["subject_uid"]

PAYMENTS_RS = RecordSetWithModifiers(PAYMENTS_TABLE, [PAYMENTS_SAMPLE_DICT])
PAYMENTS_WITH_ORDERS_RS = RecordSetWithModifiers(
    PAYMENTS_TABLE, [PAYMENTS_SAMPLE_DICT, PAYMENTS_ORDER_SAMPLE_DICT]
)
VERIFICATIONS_RS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE, [VERIFICATIONS_SAMPLE_DICT]
)


# -------- test individual functions --------


# --------


VERIFICATIONS_RS_DIFFERENT_SUBJECT_ORIGIN = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"subject_origin": "shs"}],
)
VERIFICATIONS_RS_DIFFERENT_CHECK = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"check": "some_other_check"}],
)
VERIFICATIONS_RS_NO_SUCCESS = RecordSetWithModifiers(
    VERIFICATIONS_TABLE,
    [VERIFICATIONS_SAMPLE_DICT],
    modifiers=[{"result": "some_failure"}],
)


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([VERIFICATIONS_RS], [SAMPLE_VERIFIED_UID]),
        ([VERIFICATIONS_RS_DIFFERENT_SUBJECT_ORIGIN], []),
        ([VERIFICATIONS_RS_DIFFERENT_CHECK], []),
        ([VERIFICATIONS_RS_NO_SUCCESS], []),
    ],
)
def test__fetch_statement_verified_uids(
    recordsets: list[RecordSet], result: list, import_context: ImportContext
) -> None:
    """Test function list output on different input combinations."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    assert fetch_statement_verified_uids() == result


# --------


PAYMENTS_WITH_ORDERS_RS_PAYMENT_UID_NOT_VERIFIED = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_SAMPLE_DICT, PAYMENTS_ORDER_SAMPLE_DICT],
    modifiers=[{"uid": "some_unverified_id"}, {}],
)
PAYMENTS_WITH_ORDERS_RS_DIFFERENT_PURPOSE = RecordSetWithModifiers(
    PAYMENTS_TABLE,
    [PAYMENTS_SAMPLE_DICT, PAYMENTS_ORDER_SAMPLE_DICT],
    modifiers=[{"purpose": "some_other_purpose"}, {}],
)

# wallet samples
WALLET_SAMPLE = [
    {
        "payments": [
            {
                "payment_external_id": "123",
                "amount": 777.0,
                "paid_at": datetime.datetime(2024, 4, 16, 14, 0),
            }
        ],
        "orders": [
            {
                "uid": "meters_222222_solar_coop_456_123_2024-04-17T14:00:00+00:00",
                "payment_external_id": "123",
                "amount": 777.0,
                "purchase_item": "PELLETS",
                "paid_at": datetime.datetime(2024, 4, 17, 14, 0),
                "organization_id": 222222,
                "source_id": 333333,
                "data_origin": "solar_coop",
                "import_id": 111111,
            }
        ],
    }
]
WALLET_PAYMENT_UID_NOT_VERIFIED_SAMPLE = []
WALLET_DIFFERENT_PURPOSE_SAMPLE = [
    {
        "payments": [],
        "orders": [
            {
                "uid": "meters_222222_solar_coop_456_123_2024-04-17T14:00:00+00:00",
                "payment_external_id": "123",
                "amount": 777.0,
                "purchase_item": "PELLETS",
                "paid_at": datetime.datetime(2024, 4, 17, 14, 0),
                "organization_id": 222222,
                "source_id": 333333,
                "data_origin": "solar_coop",
                "import_id": 111111,
            }
        ],
    }
]


@pytest.mark.parametrize(
    ("recordsets", "result"),
    [
        ([PAYMENTS_WITH_ORDERS_RS], WALLET_SAMPLE),
        (
            [PAYMENTS_WITH_ORDERS_RS_PAYMENT_UID_NOT_VERIFIED],
            WALLET_PAYMENT_UID_NOT_VERIFIED_SAMPLE,
        ),
        (
            [PAYMENTS_WITH_ORDERS_RS_DIFFERENT_PURPOSE],
            WALLET_DIFFERENT_PURPOSE_SAMPLE,
        ),
    ],
)
def test__construct_wallets(
    recordsets: list[RecordSet], result: list, import_context: ImportContext
) -> None:
    """Test function list output on different input combinations."""
    prepare_test_data(recordsets=recordsets, import_context=import_context)
    assert construct_wallets([SAMPLE_VERIFIED_UID]) == result


# --------


def test__is_verified_against_wallet() -> None:
    """Test function list output on different input combinations."""
    WALLET_SAMPLE_DICT = {
        "orders": [
            {
                "payment_external_id": "123",
                "amount": 777.0,
                "purchase_item": "PELLETS",
                "paid_at": datetime.datetime(2024, 4, 17, 14, 0),
                "organization_id": 222222,
                "source_id": 333333,
                "data_origin": "solar_coop",
                "import_id": 111111,
            },
        ],
        "payments": [
            {
                "payment_external_id": "123",
                "amount": 777.0,
                "paid_at": datetime.datetime(2024, 4, 16, 14, 0),
            },
        ],
    }
    WALLET_SAMPLE_DICT_PAYMENT_NOT_SUFFICIENT = {
        "orders": [
            {
                "payment_external_id": "123",
                "amount": 777.0,
                "purchase_item": "PELLETS",
                "paid_at": datetime.datetime(2024, 4, 17, 14, 0),
                "organization_id": 222222,
                "source_id": 333333,
                "data_origin": "solar_coop",
                "import_id": 111111,
            },
        ],
        "payments": [
            {
                "payment_external_id": "123",
                "amount": 776.0,
                "paid_at": datetime.datetime(2024, 4, 16, 14, 0),
            },
        ],
    }
    assert (
        is_verified_against_wallet(
            datetime.datetime(2024, 4, 17, 14, 0), WALLET_SAMPLE_DICT
        )
        is True
    )
    assert (
        is_verified_against_wallet(
            datetime.datetime(2024, 4, 17, 14, 0),
            WALLET_SAMPLE_DICT_PAYMENT_NOT_SUFFICIENT,
        )
        is False
    )


# # --------


def test__prune_payload() -> None:
    """Test if dict items are successfully removed from input."""
    VERIFICATIONS_SAMPLE_DICT_EXTRA_ITEMS = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "purchase_item": "some_val",
        "amount": 777,
        "paid_at": "some_val",
    }
    VERIFICATIONS_SAMPLE_DICT_NULL_VALUE = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "purchase_item": None,
        "amount": 777,
        "paid_at": "some_val",
    }
    VERIFICATIONS_SAMPLE_DICT_MISSING_KEY = VERIFICATIONS_SAMPLE_DICT | {
        "payment_external_id": "some_val",
        "uid": "some_val",
        "amount": 777,
        "paid_at": "some_val",
    }
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_EXTRA_ITEMS)
        == VERIFICATIONS_SAMPLE_DICT
    )
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_NULL_VALUE) == VERIFICATIONS_SAMPLE_DICT
    )
    assert (
        prune_payload(VERIFICATIONS_SAMPLE_DICT_MISSING_KEY)
        == VERIFICATIONS_SAMPLE_DICT
    )


# -------- test task --------


@time_machine.travel(
    datetime.datetime(2024, 6, 18, 20, 19, 42, tzinfo=datetime.UTC), tick=False
)
def test__task(import_context: ImportContext) -> None:
    """Test result end-to-end with db."""
    prepare_test_data(
        recordsets=[
            PAYMENTS_WITH_ORDERS_RS,
            VERIFICATIONS_RS,
        ],
        import_context=import_context,
    )

    # Run the task.
    InternalTask.from_function(run)
    # Check the results.
    trust_trace_content = table_content("data_trust_trace", shed_id=True)
    assert summarize_records(trust_trace_content) == {
        "fields": [
            "check",
            "created_at",
            "custom",
            "data_origin",
            "import_id",
            "last_import_id",
            "last_source_id",
            "organization_id",
            "result",
            "source_id",
            "subject_origin",
            "subject_uid",
            "uid",
            "updated_at",
        ],
        "hash": "c247d39e8250e99d99df31411d312c3ffae6681262c3ad0073f93b4654debe5a",
        "sample_count": 1,
        "value_count": 12,
    }
    assert table_content("data_trust_trace") == snapshot([
        {
            "check": "crm_payment_backed_by_payment_statement",
            "created_at": datetime.datetime(2024, 6, 18, 20, 19, 42),
            "custom": {},
            "data_origin": "solar_coop",
            "import_id": 111111,
            "last_import_id": None,
            "last_source_id": None,
            "organization_id": 222222,
            "result": "ok",
            "source_id": 333333,
            "subject_origin": "payments_ts",
            "subject_uid": "meters_222222_solar_coop_456_123_2024-04-16T14:00:00+00:00",
            "uid": "payments_ts_meters_222222_solar_coop_456_123_2024-04-16T14:00:00+00:00_crm_payment_backed_by_payment_statement",  # noqa: E501
            "updated_at": datetime.datetime(2024, 6, 18, 20, 19, 42),
        }
    ])
