import datetime
from copy import deepcopy

import arrow
import pytest
import time_machine

from dataworker.database import get_table
from dataworker.dataset import Record, RecordSet
from dataworker.datawarehouse import table_unique_constraint_columns
from dataworker.metadata import ImportContext
from dataworker.random import jiggle_integers, set_seed
from dataworker.task import IngestionTask, prepare_for_storage
from tasks.ingestion.generic import transform
from tests.utils import load_test_data


@pytest.fixture(scope="module")
def generic_IngestionTask():
    """The generic dataworker IngestionTask, for tests."""
    return IngestionTask("generic", transform)


@pytest.fixture
def etl_parameters(scope="session"):
    return {
        "file_key": "data.json",
        "file_format": "application/json",
        "import_id": "111111",
        "organization_id": "222222",
        "source_id": "333333",
        "data_origin": "solar_coop",
    }


# ------------------------- objects and records -------------------------


@pytest.fixture(scope="session")
def import_context() -> ImportContext:
    return ImportContext(
        import_id=111111,
        organization_id=222222,
        source_id=333333,
        data_origin="solar_coop",
    )


# ------------------------- grids -------------------------


@pytest.fixture(scope="session")
def grid_table(_setup_test_database):
    """The table object."""
    return get_table("data_grids")


@pytest.fixture(scope="session")
def grid_unique_columns(grid_table):
    return table_unique_constraint_columns(grid_table)


@pytest.fixture(scope="session")
def grid_record() -> Record:
    """A record for a grid. Completely made up."""
    return load_test_data("records/grid.json")


@pytest.fixture
def grid_record_editable(grid_record) -> Record:
    return deepcopy(grid_record)


@pytest.fixture(scope="session")
def other_grid_record(grid_record) -> Record:
    """A variation from the original record."""
    return deepcopy(grid_record) | {
        "name": "Tournadous",
    }


@pytest.fixture(scope="session")
def grid_recordset_raw(grid_record, other_grid_record):
    """A recordset of two grids."""
    return RecordSet("data_grids", [grid_record, other_grid_record])


@pytest.fixture(scope="session")
@time_machine.travel(
    datetime.datetime(2024, 4, 8, 16, 43, tzinfo=datetime.UTC), tick=False
)
def grid_recordset(grid_recordset_raw, import_context, _setup_test_database):
    """A recordset of two grids, after post-processing for insert.

    Requiring the mock _setup_test_database to be sure it is setup first.
    """
    return prepare_for_storage(
        grid_recordset_raw, import_context=import_context, is_update=False
    )


@pytest.fixture
def grid_record_processed(grid_recordset):
    """A processed version of the record."""
    return deepcopy(grid_recordset.records[0])


# @pytest.fixture
# def other_grid_record_processed(grid_recordset):
#     """A processed version of the record."""
#     return grid_recordset.records[1]


# ------------------------- grid samples -------------------------


# @pytest.fixture
# def grid_ts_table() -> Table:
#     """The table object."""
#     return get_table("data_grids_ts")


@pytest.fixture(scope="session")
def grid_sample_record() -> Record:
    """A sample record for a grid. Completely made up."""
    return load_test_data("records/grid_sample.json")


@pytest.fixture(scope="session")
def other_grid_sample_record(grid_sample_record) -> Record:
    """Numerical variation of the original grid sample."""
    keys = [
        "battery_charge_state_percent",
        "battery_to_customer_kwh",
        "primary_source_to_battery_kwh",
        "primary_source_to_customer_kwh",
        "secondary_source_to_battery_kwh",
        "secondary_source_to_customer_kwh",
    ]
    a_bit_later = (
        arrow.get(grid_sample_record["metered_at"]).shift(minutes=15).isoformat()
    )
    with set_seed("other_grid_sample_record"):
        return jiggle_integers(
            keys,
            0.1,
            grid_sample_record | {"metered_at": a_bit_later},
        )


@pytest.fixture(scope="session")
def grid_sample_recordset_raw(
    grid_sample_record, other_grid_sample_record
) -> RecordSet:
    """A recordset of two grid sample."""
    return RecordSet("data_grids_ts", [grid_sample_record, other_grid_sample_record])


@pytest.fixture(scope="session")
@time_machine.travel(
    datetime.datetime(2024, 4, 8, 16, 43, tzinfo=datetime.UTC), tick=False
)
def grid_sample_recordset(
    grid_sample_recordset_raw, import_context, _setup_test_database
) -> RecordSet:
    """A recordset of two grid sample, after post-processing."""
    return prepare_for_storage(
        grid_sample_recordset_raw, import_context=import_context, is_update=False
    )


@pytest.fixture
def grid_sample_record_processed(grid_sample_recordset) -> Record:
    """Processed version of the above record."""
    return deepcopy(grid_sample_recordset.records[0])


@pytest.fixture
def other_grid_sample_record_processed(grid_sample_recordset) -> Record:
    """Processed version of the above record."""
    return deepcopy(grid_sample_recordset.records[1])


# ---------------------- meter ----------------------------


@pytest.fixture(scope="session")
def meter_record() -> Record:
    """A made up record for meter table."""
    return load_test_data("records/meter.json")


# ---------------------- meter sample ----------------------------


@pytest.fixture(scope="session")
def meter_sample_record() -> Record:
    """A made up record for meter_ts table."""
    return load_test_data("records/meter_sample.json")


# ---------------------- meter event sample ----------------------------


@pytest.fixture(scope="session")
def meter_event_sample_record() -> Record:
    """A made up record for meter_events_ts table."""
    return load_test_data("records/meter_event_sample.json")


# ---------------------- payment sample ----------------------------


@pytest.fixture(scope="session")
def payment_sample_record() -> Record:
    """A made up record for paygo table."""
    return load_test_data("records/payments_sample.json")


# ---------------------- solar system ----------------------------


@pytest.fixture(scope="session")
def solar_system_record() -> Record:
    """A made up record for shs table."""
    return load_test_data("records/shs.json")


# ---------------------- solar system sample ----------------------------


@pytest.fixture(scope="session")
def solar_system_sample_record() -> Record:
    """A made up record for shs_ts table."""
    return load_test_data("records/shs_sample.json")


# ---------------------- trust trace ----------------------------


@pytest.fixture(scope="session")
def trust_trace_record() -> Record:
    """A made up record for trust_trace table."""
    return load_test_data("records/trust_trace.json")


# ------------------------- custom -------------------------


@pytest.fixture(scope="session")
def custom_record() -> Record:
    """A made up record for custom table."""
    return load_test_data("records/custom.json")


# ---------------------- paygo sample ----------------------------


@pytest.fixture(scope="session")
def paygo_account_sample_record() -> Record:
    """A made up record for data_paygo_accounts_snapshots_ts table."""
    return load_test_data("records/paygo_accounts_snapshots_sample.json")


# ------------------------- meters -------------------------


# @pytest.fixture(scope="session")
# def meter_sample() -> Record:
#     """A valid sample, straight from the real db. not all fields are filled."""
#     return load_test_data("samples/data_meters_sample.json")
