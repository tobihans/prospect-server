"""Extract data from binary fields."""

import math
import struct


class BitField:
    """Number with slice access to bit slices.

    Bit indexes starts with 0, which is the Least Significant Bit of the field

    Single bit addressing: bit_field[BitIndex]
    Returns boolean
    ```
    >> bf=BitField(0b11001010)
    >> bin(bf[0])
    0b0
    >> bin(bf[1])
    0b1
    >> bin(bf[2])
    0b0
    ```

    Bit range index use the same indexing as for the single bit index. Bounds
    are inclusive, and the indexing order is reversed ([])

    Bit Range addressing: bit_field[HighBitIndex:LowBitIndex]
    Returns integer
    ```
    >> bf=BitField(0b11001010)

    >> bin(bf[3:0])
    0b1010
    >> bin(bf[8:5])
    0b1100
    ```

    The repr of the BitField displays an integer number of bytes (8,16,24,32,...
    bits)
    """

    def __init__(self, value: str | int) -> None:
        """Initialize by converting the value to integer."""
        self.value = int(value)

    def __repr__(self) -> str:
        """Binary representation."""
        nb_bytes = math.ceil(math.log2(int(self.value)) / 8)
        return f"{self.__class__.__name__}({self.value:0{nb_bytes * 8}b})"

    def _get_slice(self, lsb: int, width: int) -> int:
        """Get slice by bitshifting by least significant bit."""
        return (self.value >> lsb) % 2 ** (width)

    def __getitem__(self, index: slice) -> bool | int:
        """Get item by index or slice."""
        if isinstance(index, int):
            return bool(self._get_slice(index, 1))
        elif isinstance(index, slice):
            return self._get_slice(index.stop, index.start - index.stop + 1)


# ------------------------- binary assembly -------------------------


# --- struct format language ---
# ">" : struct is big-endian. the fist byte is the most significant
# "h" : two-bytes signed integer
# "i" : four-bytes signed integer
# "I" : four-bytes unsigned integer


def make_uint_from_shorts(high: int, low: int) -> bytes:
    """Assemble two short ints (2 bytes) into an unsigned int (4 bytes).

    high: short int, to be interpreted as the two high bytes
    low: short int, to be interpreted as the two low bytes
    """
    return struct.unpack(">I", struct.pack(">h", high) + struct.pack(">h", low))[0]


def make_int_from_shorts(high: int, low: int) -> bytes:
    """Assemble two short ints (2 bytes) into an int (4 bytes).

    high: short int, to be interpreted as the two high bytes
    low: short int, to be interpreted as the two low bytes
    """
    return struct.unpack(">i", struct.pack(">h", high) + struct.pack(">h", low))[0]


# ------------------------- binary print -------------------------


def binary_string_bytes(byte_string: bytes) -> str:
    """Print byte string as binary."""
    return " ".join(f"{b:08b}" for b in byte_string)


def binary_string_short_int(number: int) -> str:
    """Binary representation of a short signed integer (2 bytes).

    >> binary_string_short_int(16573)
    '01000000 10111101'
    >> binary_string_short_int(-3516)
    '11110010 01000100'
    """
    return binary_string_bytes(struct.pack(">h", number))


def binary_string_int(number: int) -> str:
    """Binary representation of a signed integer (4 bytes).

    >> binary_string_int(21684351)
    '00000001 01001010 11100000 01111111'
    >> binary_string_int(-21684351)
    '11111110 10110101 00011111 10000001'
    """
    return binary_string_bytes(struct.pack(">i", number))


def binary_string_uint(number: int) -> str:
    """Binary representation of an unsigned integer (4 bytes).

    >> binary_string_uint(2169935116)
    '10000001 01010110 10010101 00001100'
    """
    return binary_string_bytes(struct.pack(">I", number))
