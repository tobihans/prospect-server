"""Integration with Nominatim service for geographical data."""

import tomllib
from dataclasses import dataclass, field
from functools import cache
from typing import ParamSpec

import geopy.exc
from geopy.adapters import RequestsAdapter
from geopy.geocoders import Nominatim
from sqlalchemy import select

from dataworker import assets, config, logger
from dataworker.database import get_table
from dataworker.queries import fetch_dict_data
from dataworker.task import StoringTaskBase

TIMEOUT = 3
COUNTRY_REGION_LEVELS_FILE = assets / "country_region_levels.toml"
ENGLISH_NAMES = True  # get english version of location names


@cache
def _load_country_region_levels() -> dict:
    """Mapping numbered administrative levels to their names for a given country.

    In case of problem with loading the map, an empty dictionary is returned.
    """
    try:
        with open(COUNTRY_REGION_LEVELS_FILE, "rb") as file:
            return tomllib.load(file)
    except Exception:
        logger.exception("failed to load region levels")
        return {}


@cache
def _get_location_client() -> Nominatim:
    """Create client for the location service."""
    try:
        connection_string = (
            f"{config.location.user}:{config.location.password}@"
            f"{config.location.host}:{config.location.port}"
        )
    except AttributeError as exc:
        raise RuntimeError(
            "could not build location service connection string from config"
        ) from exc
    logger.debug("connecting to location service at {}", config.location.host)
    return Nominatim(
        user_agent="dataworker",
        domain=connection_string,
        adapter_factory=RequestsAdapter,
        timeout=TIMEOUT,
    )


def _get_address_object(point: tuple[float, float]) -> dict:
    connection = _get_location_client()
    kwargs = {"language": "en"} if ENGLISH_NAMES else {}
    geopy_location = connection.reverse(point, **kwargs)
    if not geopy_location:
        logger.warning("Could not reverse geocode point {}", point)
        return {}
    return geopy_location.raw["address"]


def _map_regions(address: dict, geo_col_prefix: str = "") -> dict:
    """Extract different levels of address.

    An address object is composed of different levels like region or district.
    The names of those subdivisions is different for each country. This function
    maps those named levels into four standardized levels to fit in our datawarehouse.
    """
    country_region_levels_map = _load_country_region_levels()
    region_levels = {}
    try:
        country_region_levels = country_region_levels_map[address["country_code"]]
    except KeyError:
        logger.warning(
            "no matching entry in region name map for {}", address["country_code"]
        )
        # don't crash if the country don't match in the map
        return {}
    for generic_level_name, country_level_name in country_region_levels.items():
        if country_level_name in address:
            level_name = f"{geo_col_prefix}{generic_level_name}"
            region_levels[level_name] = address[country_level_name]

    return region_levels


def reverse_geo_lookup(
    latitude: float,
    longitude: float,
    geo_col_prefix: str = "",
) -> dict:
    """Returns named regions corresponding to given coordinates."""
    if any(not isinstance(coord, float) for coord in (latitude, longitude)):
        # for safety, early return if anything else than two floats is given
        return {}
    try:
        address = _get_address_object((latitude, longitude))
    except (geopy.exc.GeocoderUnavailable, RuntimeError):
        logger.opt(exception=True).warning("could not connect to location service")
        return {}
    try:
        country_code = address["country_code"].upper()
    except KeyError:
        if address:
            logger.warning("no country code found in the address object {}", address)
        # if we cant even start with a country code, we just return an empty dict
        return {}
    location_area_dict = _map_regions(address, geo_col_prefix)
    return {f"{geo_col_prefix}country": country_code} | location_area_dict


def location_area_from_lat_lon(
    table: str, organization_id: int, source_id: int, geo_col_prefix: str = ""
) -> dict:
    """Retrieve location area where it is missing from lat/long.

    geo_col_prefix is the prefix for the desired location-relevant columns.
    Some possible values: "", "customer_", "seller_".
    """
    table_obj = get_table(table)
    country_col = f"{geo_col_prefix}country"
    lat_col = f"{geo_col_prefix}latitude_b"
    lon_col = f"{geo_col_prefix}longitude_b"
    location_area_1_col = f"{geo_col_prefix}location_area_1"
    required_columns = {country_col, lat_col, lon_col, location_area_1_col}
    missing_columns = required_columns - {col.name for col in table_obj.c}
    if missing_columns:
        raise TypeError(
            "Required columns {} not found on table {}", missing_columns, table
        )

    data = fetch_dict_data(
        select(
            table_obj.c.uid,
            table_obj.c[country_col],
            table_obj.c[lat_col],
            table_obj.c[lon_col],
        ).where(
            table_obj.c[lat_col] != None,  # noqa: E711
            table_obj.c[lon_col] != None,  # noqa: E711
            table_obj.c[location_area_1_col] == None,  # noqa: E711
            table_obj.c.organization_id == organization_id,
            table_obj.c.source_id == source_id,
        )
    )
    enriched_data = []
    for entity in data:
        location_dict = reverse_geo_lookup(
            entity[lat_col], entity[lon_col], geo_col_prefix
        )
        db_country = entity.get(country_col)
        reverse_country = location_dict.get(country_col)
        if (
            db_country
            and reverse_country
            and db_country.upper() != reverse_country.upper()
        ):
            logger.warning(
                "DB country info {} does not match returned {} from lat {} and lon {}",
                db_country,
                reverse_country,
                entity[lat_col],
                entity[lon_col],
            )
            continue
        location_dict["uid"] = entity["uid"]
        enriched_data.append(location_dict)
    return {"_update": table, table: enriched_data}


Param = ParamSpec("Param")


@dataclass
class EnrichLocationTask(StoringTaskBase):
    """Enrich location data via reverse geocoding from and to storage.

    Arguments of the task are:
    - table: str
    - organization_id: int
    - source_id: int
    - geo_col_prefix: str
    """

    is_internal: bool = True
    args_required: bool = True
    function_args_set: set = field(
        default_factory=lambda: {
            "table",
            "organization_id",
            "source_id",
            "geo_col_prefix",
        }
    )
    task_args_list: list = field(
        default_factory=lambda: [
            "table",
            "organization_id",
            "source_id",
            "geo_col_prefix",
        ]
    )

    def get_function_input(
        self, *args: Param.args, **kwargs: Param.kwargs
    ) -> tuple[tuple, dict[str, dict]]:
        """Validate the kwargs."""
        if sorted(kwargs) != sorted(self.task_args_list):
            raise TypeError(
                "EnrichLocationTask should be called with these kwargs exactly {}.",
                self.task_args_list,
            )

        logger.info(
            "starting location enrichment on table {} with column prefix `{}`. "
            "For organization {}, source {}.",
            kwargs["table"],
            kwargs["geo_col_prefix"],
            kwargs["organization_id"],
            kwargs["source_id"],
        )
        return (), kwargs


def enrich_location_area_from_lat_lon(
    table: str, organization_id: int, source_id: int, geo_col_prefix: str = ""
) -> None:
    """Enrich location area where it's missing, from lat/long via task in storage."""
    task = EnrichLocationTask("enrich_location", location_area_from_lat_lon)
    task(
        table=table,
        organization_id=organization_id,
        source_id=source_id,
        geo_col_prefix=geo_col_prefix,
    )
