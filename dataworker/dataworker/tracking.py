"""Record tracking data on the datawarehouse via logs from jobs.

There is in the datawarehouse an import table that expect to be updated about
jobs run in the dataworker.

# Context
A dataworker process can run multiple workers, implemented as
processes. Threads keep running between job execution, therefore one process_id
identified one worker.
- Log messages are emitted during jobs, which along with the message also carry
  context information.
- Log context can contain Events, enum instances that mark specific events in a
  job.
- Tracking as implemented in this module works by collecting log messages, and
  together with the context data, infers the tracking data.

# Tracker process
Logs emitted in the dataworker are collected into a sink that filters the ones
corresponding to import jobs, and add them to a queue. The tracker runs in it's
own process. It consumes logs from the queue and pass them to a TrackerManager instance.

# TrackerManager
Manages one or more TaskTracker instances. A TaskTracker corresponds to one job
(task execution). Incoming logs are mapped to a tracker object via the id of
their worker process. The manager also takes care of creating a new tracker when
the worker starts a new job.

A worker process works through jobs sequentially. Therefore when logs from a same
worker process comes with a different import_id, a new job has started. The task
tracker for this process get replaced by a new one. This avoid memory leaks that
were not explicitly protected from in the previous design.

# TaskTracker
Only receives logs from the same import job. They are recorded. When a START or
COMPLETE event comes in the tracker, the tracking data is computed from the
stored logs and updated in the datawarehouse.

tracking data consists in:
- counting events relative to row insertion/update
- report start and end time of process. if the process fail, there is no end
  time. you'll never know how long it lasted before failing.
- report last error message if the job fails
- appending logs of level INFO and above to the protocol field
"""

import dataclasses
from datetime import UTC, datetime
from enum import StrEnum, auto
from multiprocessing import Process
from typing import Any, NoReturn

import funcy
import loguru._defaults
from box import Box

from dataworker import logger
from dataworker.database import (
    get_or_create_connection,
    get_table,
    manage_connection,
    manage_transaction,
)
from dataworker.logging import ingestion_logs
from dataworker.metadata import update_timestamp
from dataworker.utils import skip_if


class Event(StrEnum):
    """Tokens emitted via logging, that are used to track task progress."""

    START = auto()
    COMPLETE = auto()  # task has completed without crashing
    JOB_FAIL = auto()  # an error made the task fail
    END = auto()  # task execution has finished, regardless of the success of the task
    TABLE_INSERTION_BEGIN = auto()
    ROW_INSERT_SUCCESS = auto()  # insert and success
    ROW_UPDATE_SUCCESS = auto()  # values successfully updated
    ROW_INSERT_DUPLICATE = auto()  # insertion skipped because of duplicate key
    ROW_OPERATION_FAIL = auto()  # failure to insert or update
    TIME = auto()  # to report execution times
    METRIC = auto()  # to report metrics


def ensure_box(something: dict | Box) -> Box:
    """Ensure to return the DefaultBox version of a dict if it is not already.

    Box is an object wrapper that allow nested access through different types of
     objects with an uniform interface. It is configured here to default to None
     if the attribute/item can't be accessed.
    """
    match something:
        case Box():
            return something
        case dict():
            return Box(something, default_box=True, default_box_attr=None)
        case _:
            raise ValueError(f"unsupported type for boxing: {type(something)}")


@dataclasses.dataclass
class ImportTracker:
    """Buffer logs of a run, and update tracking."""

    import_id: int

    _logs: list[Box] = dataclasses.field(default_factory=list)
    _protocol_pointer: int = 0

    # receiving one of those events cause tracking data to be synced with the
    # database.
    _UPDATING_EVENTS = (Event.START, Event.END)

    def __repr__(self) -> str:
        """Ex: TaskTracker(<3 logs>)."""
        return f"{self.__class__.__name__}(<{len(self._logs)} logs>)"

    @property
    def start_time(self) -> datetime | None:
        """Task starting time."""
        dt = funcy.first(
            log.time for log in self._logs if log.extra.event is Event.START
        )
        return dt.astimezone(UTC) if dt else dt

    @property
    def end_time(self) -> datetime | None:
        """Moment when the task is complete."""
        dt = funcy.first(log.time for log in self._logs if log.extra.event is Event.END)
        return dt.astimezone(UTC) if dt else dt

    @property
    def error_message(self) -> str | None:
        """Message of the JOB_FAIL event, if any."""
        return funcy.first(
            log.message for log in self._logs if log.extra.event is Event.JOB_FAIL
        )

    def _collect_tracking_data(self) -> dict:
        event_count = funcy.count_reps(
            funcy.keep(log.extra.event for log in self._logs)
        )
        return {
            "rows_inserted": event_count[Event.ROW_INSERT_SUCCESS],
            "rows_updated": event_count[Event.ROW_UPDATE_SUCCESS],
            "rows_duplicated": event_count[Event.ROW_INSERT_DUPLICATE],
            "rows_failed": event_count[Event.ROW_OPERATION_FAIL],
            "error": self.error_message,
            "processing_started_at": self.start_time,
            "processing_finished_at": self.end_time,
        }

    def _collect_protocol(self) -> str:
        messages = [
            log.message
            for log in self._logs[self._protocol_pointer :]
            if log.level.no >= loguru._defaults.LOGURU_INFO_NO
        ]
        if messages and self._protocol_pointer == 0:
            # when the task start running, the protocol is expected to already
            # contain logs from the connector. This protocol get displayed in
            # the Prospect UI. To mark the transition between connector and
            # ingestion task, we insert a separator before the first task log.
            protocol_separator = f"{'dataworker import task'.upper():=^40}"
            messages.insert(0, protocol_separator)
        return "\n".join(messages)

    def _update_protocol_pointer(self) -> None:
        self._protocol_pointer = len(self._logs)

    # ---

    @manage_connection
    @manage_transaction
    def _sync(self) -> None:
        """Update the tracking table based on the accumulated logs.

        - row event count
        - start/end timestamps
        - error messages
        - log messages

        Log messages are appended to previous messages from the connector, or
        from that very task tracker. Because sync can be invoked multiple times
        on the same tracker object we need to keep track of which logs have been
        already synced to the tracking table. This is done with an index value
        on the logs that get advanced to the latest position after each sync.
        """
        logger.debug(
            "syncing tracking data for import {import_id}", import_id=self.import_id
        )
        table = get_table("imports")
        update_statement = (
            table.update()
            .where(table.c.id == self.import_id)
            .values(self._collect_tracking_data() | update_timestamp())
        )
        if protocol := self._collect_protocol():
            update_statement = update_statement.values(
                protocol=table.c.protocol + "\n" + protocol
            )
        connection = get_or_create_connection()
        connection.execute(update_statement)
        self._update_protocol_pointer()
        logger.info("tracking info updated")

    def add_log(self, record: dict) -> None:
        """Add a log to the run buffer."""
        logger.trace("log collected")
        record = ensure_box(record)
        self._logs.append(record)
        if record.extra.event in self._UPDATING_EVENTS:
            self._sync()


@dataclasses.dataclass
class TrackerManager:
    """Manage creation of tracker and dispatch logs to them.

    - We assume all incoming log record have an import_id
    - The tracker manager maintains zero or one tracker object per worker
      process.
    - The tracker associated to a worker process, is created anew when a START
      event comes in from that same process.
    - Log records whithout associated tracker are discarded.
    """

    _trackers: dict[int, ImportTracker] = dataclasses.field(
        init=False, default_factory=dict
    )

    def _trackers_status(self) -> list[dict[str, Any]]:
        """Summary of the managed trackers.

        Use for testing and debugging.
        """
        return [
            {
                "process": process,
                "import_id": tracker.import_id,
                "logs": len(tracker._logs),
            }
            for process, tracker in self._trackers.items()
        ]

    def process_log(self, record: dict) -> None:
        """Ingests log record into the tracking system."""
        record = ensure_box(record)
        # records at this point should all have import_id, but just in case:
        if record.extra.import_id is None:
            logger.error("logger in the tracking system should all have import_id")
            return
        # Trackers get recreated when a start event comes in.
        if record.extra.event == Event.START:
            logger.debug(
                "Start event received for process {process}. Creating new tracker.",
                process=record.process.id,
            )
            self._trackers[record.process.id] = ImportTracker(record.extra.import_id)
        # For a logs to be taking into account, we need first to find a tracker
        # already associated with the emitting process, and for that tracker to
        # correspond to the current log's import_id.
        if ((tracker := self._trackers.get(record.process.id)) is not None) and (
            record.extra.import_id == tracker.import_id
        ):
            tracker.add_log(record)


class TrackerProcess(Process):
    """Manages a tracker for each process via a TrackerManager instance.

    Run as a daemon process that terminates together with the main process.
    """

    def __init__(self) -> None:  # noqa: D107
        super().__init__(daemon=True, name="tracking")
        self.tracker_manager = TrackerManager()

    def run(self) -> NoReturn:
        """Run the TrackerThread reading logs from the queue and processing them."""
        logger.info("tracking process starting")
        while True:
            log = ingestion_logs.get()
            try:
                self.tracker_manager.process_log(log)
            except Exception:
                logger.exception("error while processing log")


@skip_if("dev.disable_tracking")
def start_tracking_process() -> None:
    """Thin method to start the TrackerProcess."""
    TrackerProcess().start()
