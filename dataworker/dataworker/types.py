"""Common data types for dataworker."""

import datetime

# values for database can be of the following types
Value = int | float | str | bool | datetime.datetime | dict | list | None
# a record represents a point of data, that is the counterpart of a database row
Record = dict[str, Value]
# A recordset as a dict
RecordSetType = dict[str, list[Record]]
# dataset as a dict. as returned by a task core function
DictDataset = dict[str, RecordSetType | str]
