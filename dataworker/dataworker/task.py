"""Module providing task handling methods.

Features data post-processing as well as task wrappers.
"""

import functools
import inspect
from collections.abc import Callable, Collection, Mapping
from dataclasses import asdict, dataclass, field
from pathlib import Path
from typing import Any, ParamSpec

import funcy
import pandas
from faktory import Worker

from dataworker import logger
from dataworker.database import (
    manage_connection,
    manage_read_connection,
    manage_transaction,
)
from dataworker.dataset import Dataset, RecordSet
from dataworker.datawarehouse import _is_insert_method, store_recordset
from dataworker.logging import run_context
from dataworker.metadata import (
    ImportContext,
    add_record_identifiers,
    collect_record_extra_fields,
    timestamps,
)
from dataworker.processing import (
    process_record_values_by_field,
    process_record_values_by_type,
)
from dataworker.storage import DataFile, read_datafile
from dataworker.tracking import Event
from dataworker.types import DictDataset

# ------------------------- prepare for storage -------------------------


def _notna(value: Any) -> bool:  # noqa: ANN401
    # pandas can't evaluate na on arrays or dicts, avoid warnings
    if isinstance(value, Mapping):
        return any(_notna(val) for val in value.values())
    if isinstance(value, Collection) and not isinstance(value, str):
        return any(_notna(val) for val in value)
    return pandas.notna(value) and value != ""


def remove_unwanted_fields(
    record_set: RecordSet,
    is_update: bool = False,
    is_insert: bool = False,
    is_internal: bool = False,
) -> RecordSet:
    """Remove fields not intended to be added or modified in the database.

    These values should never come from the task themselves. We will:
    - Shed empty strings and Nones
    - Avoid setting/modifying data to custom
    - Omit IDs, timestamps and similar.

    Note that this could be extended to make sure that all records of the record_set
    have the same set of (valid) attributes. For now we rely on the fact that record
    sets generated by tasks should have the same set of valid attributes.
    """
    operations = []
    # Shed empty strings, lists, dicts, Nones, nan, NaT and similar
    if not is_insert:
        operations.append((
            "filter_values",
            lambda value: _notna(value),
        ))
    # We don't accept custom columns coming from task themselves nor internal metadata
    operations.append((
        "omit_keys",
        [
            "custom",
            "created_at",
            "updated_at",
            "id",
            "last_import_id",
            "last_source_id",
        ],
    ))
    # On ingestion task, we omit more keys
    if not is_internal:
        # We don't accept manual import context
        keys_to_omit = [
            "import_id",
            "source_id",
            "data_origin",
            "organization_id",
        ]
        # We accept uid only in case of update
        if not is_update:
            keys_to_omit.append("uid")
        operations.append((  # noqa: FURB113
            "omit_keys",
            keys_to_omit,
        ))
        # We don't accept secondary uid, but rebuild them.
        operations.append((
            "omit_key_pattern",
            ".*_uid",
        ))
    return record_set.apply_multiple(*operations)


def add_metadata(
    record_set: RecordSet, import_context: ImportContext = None, is_update: bool = False
) -> RecordSet:
    """Add required metadata before storage to the records.

    We add data that is not task-specific but required for the database:
    - Add import context (import ID, organization, data_origin, etc.)
    - Add creation and update timestamps on inserts
    """
    operations = []
    # Add import context
    if import_context is not None:
        operations.append((
            "add",
            asdict(import_context),
        ))
    # Add creation/update timestamp
    if not is_update:
        operations.append((
            "add",
            timestamps(),
        ))
    return record_set.apply_multiple(*operations)


def add_uids(record_set: RecordSet, is_update: bool = False) -> RecordSet:
    """Add required uids on update.

    - Build primary and secondary identifiers from the records
    """
    if is_update:
        return record_set
    return record_set.apply(
        "map_records",
        funcy.partial(
            add_record_identifiers,
            table=record_set.table,
        ),
    )


def adapt_types(record_set: RecordSet) -> RecordSet:
    """Adapt values to conform to the database types."""
    operations = []
    # Collect extra fields in a JSON field
    operations.append((  # noqa: FURB113
        "map_records",
        funcy.partial(collect_record_extra_fields, table=record_set.table),
    ))
    # Process values depending on the destination column type
    operations.append((
        "map_records",
        funcy.partial(process_record_values_by_type, table=record_set.table),
    ))
    # Process values depending on the destination column field
    operations.append((
        "map_records",
        funcy.partial(process_record_values_by_field, table=record_set.table),
    ))
    return record_set.apply_multiple(*operations)


def prepare_for_storage(
    record_set: RecordSet,
    import_context: ImportContext = None,
    is_update: bool = False,
    is_internal: bool = False,
) -> RecordSet:
    """Prepare record set for storage."""
    post_processing_pipeline = funcy.rcompose(
        functools.partial(
            remove_unwanted_fields,
            is_update=is_update,
            is_insert=_is_insert_method(record_set.table),
            is_internal=is_internal,
        ),
        functools.partial(
            add_metadata, is_update=is_update, import_context=import_context
        ),
        functools.partial(adapt_types),
        functools.partial(add_uids, is_update=is_update),
    )
    return post_processing_pipeline(record_set)


# --------------------------------------------------


def function_import_path(function: Callable) -> str:
    """Function module and name.

    ex: dataworker.binary.extract_fields
    """
    return f"{function.__module__}.{function.__name__}"


Param = ParamSpec("Param")


@dataclass
class Task:
    """The simplest dataworker task.

    A task is a wrapper around a function.
    As all subclasses in usage inherit from StoringTaskBase, we may
    merge both classes at some point, overwriting/mocking store_dataset if necessary.

    Argument validation is done in the __post_init__ function and based on two
    attributes, as doing it with one is pretty tricky:
      1. `args_required` specifies if args are required for the task (sub)class
      2. `function_args_set` specifies the required args

    `import_context` defined here for visibility only, used in StoringTask subclasses
    """

    name: str
    function: Callable
    is_internal: bool = False
    args_required: bool = False
    function_args_set: set | None = None
    import_context: ImportContext | None = None

    @classmethod
    def from_function(cls, function: Callable) -> None:
        """Builder from only a function."""
        name = function.__module__.split(".")[-1]
        return cls(function=function, name=name)

    @classmethod
    def from_config(cls, path: str | Path) -> None:
        """Builder from config file."""
        raise NotImplementedError

    def __repr__(self) -> str:
        """Ex: Task('etl/aamv3','aamv3.main.transform')."""
        return (
            f"{self.__class__.__name__}"
            f"({self.name!r},{function_import_path(self.function)!r})"
        )

    def __post_init__(self) -> None:
        """Only accepts functions with expected arguments."""
        function_args = inspect.getfullargspec(self.function).args
        if self.args_required and not function_args:
            raise TypeError(
                f"{self.__class__.__name__} requires argument, None was given."
            )
        if function_args and set(function_args) != self.function_args_set:
            raise TypeError(
                f"{self.__class__.__name__} can only be built from "
                f"function that accepts strictly {self.function_args_set} args. "
                f"{function_import_path(self.function)} expects {function_args} args."
            )

    def register(self, worker: Worker) -> None:
        """Register task to a faktory worker."""
        worker.register(self.name, self)

    def store_dataset(self, dataset: DictDataset) -> None:
        """Override in child classes if needed."""
        pass

    def get_function_input(
        self, *args: Param.args, **kwargs: Param.kwargs
    ) -> tuple[tuple, dict]:
        """Override in child classes if needed."""
        logger.info("task starting")
        return args, kwargs

    @run_context
    def __call__(self, *args: Param.args, **kwargs: Param.kwargs) -> None:
        """Execute the task. The first argument should be import_id."""
        logger_context = {"task": self.name}
        logger_context.update(kwargs)
        if args:
            logger_context["import_id"] = args[0]
        with logger.contextualize(**logger_context):
            try:
                args, kwargs = self.get_function_input(*args, **kwargs)
                # TODO: add methods on task level to dispatch connections
                # NB: the function call will most probably use read-only connections
                dataset = Dataset.from_dict(
                    manage_read_connection(self.function)(*args, **kwargs)
                )
                logger.info(
                    "Task function returned {dataset}",
                    dataset=repr(dataset),
                )
                # Then use a classic write-enabled connection to store the data
                manage_connection(manage_transaction(self.store_dataset))(dataset)
                logger.info("Task complete", event=Event.COMPLETE)
            except Exception:
                logger.exception("An error interrupted the task", event=Event.JOB_FAIL)
                raise
            finally:
                logger.info("Job done", event=Event.END)


@dataclass
class StoringTaskBase(Task):
    """Task that stores the returned dataset.

    Calling this class will fail until get_function_input is overwritten in subclasses.
    """

    def store_dataset(self, dataset: DictDataset) -> None:
        """Prepare dataset and send for storage."""
        for recordset in dataset.recordsets:
            processed_recordset = prepare_for_storage(
                recordset,
                import_context=self.import_context,
                is_update=recordset.is_update,
                is_internal=self.is_internal,
            )
            store_recordset(processed_recordset, use_update=recordset.is_update)

    def get_function_input(
        self, *args: Param.args, **kwargs: Param.kwargs
    ) -> tuple[tuple, dict]:
        """Override in child classes."""
        raise NotImplementedError(
            "You should inherit from StoringTaskBase and overwrite `get_function_input`"
        )


@dataclass
class InternalTask(StoringTaskBase):
    """Task that stores the returned dataset.

    The internal function is called without arguments, or only by import_id.
    It can have no data input or query it's own input from the database.
    The returned dataset, if any, is stored in the datawarehouse.

    It was called previously a processing task.
    """

    is_internal: bool = True
    args_required: bool = False
    function_args_set: set = field(default_factory=lambda: {"import_id"})

    def get_function_input(
        self, *args: Param.args, **kwargs: Param.kwargs
    ) -> tuple[tuple, dict[str, str | int]]:
        """Return import_id from args inside a kwargs dict."""
        logger.info("task starting")
        if len(args) > 1:
            raise TypeError("Got more than one unnamed arg when calling task function")
        if args:
            return (), {"import_id": args[0]}
        return (), kwargs


@dataclass
class IngestionTask(StoringTaskBase):
    """Task that reshape data from storage and returns a dataset for datawarehouse.

    Arguments of an ingestion task are:
    - import_id: int
    - source_id: int
    - file_key: str
    - file_format: str
    - data_origin: str
    - organization_id: int

    It was called previously an ETL task.
    """

    is_internal: bool = False
    args_required: bool = True
    function_args_set: set = field(default_factory=lambda: {"file_data"})
    task_args_list: list = field(
        default_factory=lambda: [
            "import_id",
            "source_id",
            "file_key",
            "file_format",
            "data_origin",
            "organization_id",
        ]
    )

    @staticmethod
    def parse_ingestion_kwargs(
        **kwargs: Param.kwargs,
    ) -> tuple[DataFile, ImportContext]:
        """Assemble dataclasses from keyword args."""
        datafile = DataFile(
            file_key=kwargs["file_key"], file_format=kwargs["file_format"]
        )
        import_context = ImportContext(
            import_id=kwargs["import_id"],
            source_id=kwargs["source_id"],
            data_origin=kwargs["data_origin"],
            organization_id=kwargs["organization_id"],
        )
        return datafile, import_context

    def get_function_input(
        self, *args: Param.args, **kwargs: Param.kwargs
    ) -> tuple[tuple, dict[str, dict]]:
        """Load datafile from storage."""
        # First check the args and recreate kwargs from args to support both formats
        if sorted((len(args), len(kwargs))) != [0, len(self.task_args_list)]:
            raise TypeError(
                "IngestionTask should be called with 6 args or 6 kwargs only."
            )
        if args:
            kwargs.update(zip(self.task_args_list, args, strict=True))

        datafile, self.import_context = self.parse_ingestion_kwargs(**kwargs)
        logger.info(
            "starting import {import_id}. "
            "Transforming data from {file_key} with {function}",
            file_key=datafile.file_key,
            function=self.function.__module__.split(".")[-1],
            event=Event.START,
            **self.import_context.__dict__,
        )
        try:
            return (), {"file_data": read_datafile(datafile)}
        except Exception as exc:
            raise RuntimeError(f"error while reading datafile {datafile}") from exc
