"""Module containing functions to send records to the database."""

import functools
import operator
from collections.abc import Iterable
from pprint import pformat

import funcy
import sqlalchemy
from pandas import isna
from sqlalchemy import BinaryExpression, Column, Table, select, update
from sqlalchemy.dialects.postgresql import insert

from dataworker import logger
from dataworker.database import ensure_table_object, get_or_create_connection, get_table
from dataworker.dataset import Record, RecordSet
from dataworker.logging import log_process
from dataworker.tracking import Event
from dataworker.utils import skip_if

# ------------------------- base datawarehouse interactions -------------------------


@functools.cache
def table_unique_constraint_columns(table: Table | str) -> tuple[Column]:
    """Return a tuple containing those columns with a unique constraint, given a table.

    Will return the primary key being normally:
     - a tuple (uid, datetime_col) for timeseries tables.
     - the uid for non-timeseries-tables.

    Raises RuntimeError if there is not one and only one unique index.
    """
    table_obj = ensure_table_object(table)
    if table_obj.primary_key:
        return tuple(table_obj.primary_key.columns)
    # this should never happen
    raise RuntimeError(f"there is not one and only one unique index on table {table}")


@functools.cache
def table_not_null_columns(table: Table | str) -> list[str]:
    """Return a tuple containing not nullable without default columns, given a table."""
    return [
        col.name
        for col in ensure_table_object(table).columns
        if col.nullable is False and not col.server_default
    ]


def required_but_null_columns(
    table: Table | str, record: Record, is_insert: bool
) -> list[str]:
    """Return a list of required record keys that are null or non-existent."""
    not_found = []
    required = []
    if is_insert:
        required_cols = table_not_null_columns(table=table)
    else:
        required_cols = [col.name for col in table_unique_constraint_columns(table)]
    for required_col in required_cols:
        if required_col not in record:
            not_found.append(required_col)
        elif isna(record[required_col]) or record[required_col] == "":
            required.append(f"required column {required_col} is {record[required_col]}")
    if not_found:
        record_ids = ", ".join(
            f"{key}: {value}" for key, value in record.items() if key.endswith("id")
        )
        required.append(
            f"required columns {', '.join(not_found)} not found for record {record_ids}"
        )
    return required


def insert_records(
    table: Table, records: Iterable[Record], unique_columns: tuple[Column]
) -> None:
    """Insert the records in the table in a nested transaction.

    If UniqueViolation, logs and returns silently without insert.
    """
    connection = get_or_create_connection()
    with connection.begin_nested():
        cursor = connection.execute(
            insert(table).on_conflict_do_nothing(index_elements=unique_columns),
            records,
        )
    # The cursor counts how many are inserted
    if cursor.rowcount < 0:
        logger.error(
            "insertion failed, returned a <0 rowcount {}",
            cursor.rowcount,
            event=Event.ROW_OPERATION_FAIL,
        )
    for _ in range(cursor.rowcount):
        logger.debug("sample inserted", event=Event.ROW_INSERT_SUCCESS)
    for _ in range(len(records) - cursor.rowcount):
        logger.debug(
            "sample not inserted. Table unique keys combination already exist",
            event=Event.ROW_INSERT_DUPLICATE,
        )


def update_records(
    table: Table, records: Iterable[Record], unique_columns: tuple[Column]
) -> None:
    """Update the records in the table in a nested transaction.

    Raises RuntimeError if the unique constraint is not satisfied.
    Else update the record matched on the columns with unique constraints.

    We do not update these columns, nor `organization_id` or `data_origin`.
    We merge old and new content of the `custom` field.
    """
    # TODO: Check if last_import_id is contained?
    # NB: updating many is tricky, as the update construct does not support
    #     multiple parameter sets. Only achievable with bindparams, which requires
    #     that all updates contain the same columns
    unique_col_names = {col.name for col in unique_columns}
    # we don't send matching columns for update, as well as organization_id
    # that would be anyway rejected by the database.
    # We additionally want to exclude data_origin.
    not_updatable = unique_col_names | {"organization_id", "data_origin"}
    errors = []
    records_to_update = []
    for record in records:
        # columns that are part of the table one and only unique index
        if not unique_col_names <= record.keys():
            errors.append(f"missing: {unique_col_names - record.keys()}")
        else:
            to_update = funcy.omit(record, not_updatable)
            if "custom" in table.c:
                # merge custom json field with already existing values
                to_update["custom"] = table.c.custom.concat(to_update.get("custom", {}))
            match_condition = match_condition_from_unique_col_names(
                table, unique_col_names, record
            )
            records_to_update.append((match_condition, to_update))

    connection = get_or_create_connection()
    for match_condition, to_update in records_to_update:
        statement = update(table).where(match_condition).values(to_update)

        with connection.begin_nested():
            result = connection.execute(statement)

        if result.rowcount == 1:
            logger.debug("sample updated", event=Event.ROW_UPDATE_SUCCESS)
        elif result.rowcount == 0:
            logger.warning(
                "sample update nothing changed for match condition {}",
                match_condition,
                event=Event.ROW_UPDATE_SUCCESS,
            )
        else:
            # a little bit paranoiac check, should never happen
            logger.error(
                "sample update match condition {} failed: unexpected rowcount {}",
                match_condition,
                result.rowcount,
                event=Event.ROW_OPERATION_FAIL,
            )

    # Finally raise after the updates if there was missing data on unique indices
    if errors:
        raise RuntimeError(f"the sample is missing index values: {errors}")


def match_condition_from_unique_col_names(
    table: Table, unique_col_names: Iterable[str], record: Record
) -> BinaryExpression:
    """Return match condition for update based on the table unique index values."""
    match_condition = functools.reduce(
        operator.and_,
        [
            operator.eq(table.c[col_name], record[col_name])
            for col_name in unique_col_names
        ],
    )

    return match_condition


def insert_or_update_record(
    table: Table, record: Record, unique_columns: tuple[Column]
) -> None:
    """Insert or update the record in the table in a nested transaction.

    We merge old and new content of the `custom` field.
    """
    # TODO: Also check `organization_id` and `data_origin`?
    # accidentaly trying to insert the following key would break the tracking logic
    record = funcy.omit(record, ("last_import_id",))
    stmt = insert(table).values(**record)
    if "custom" in table.c:
        # merge custom json field with already existing values
        record["custom"] = table.c.custom.concat(stmt.excluded.custom)
    stmt = stmt.on_conflict_do_update(
        index_elements=unique_columns, set_=record
    ).returning(table.c.import_id, table.c.last_import_id)

    connection = get_or_create_connection()
    with connection.begin_nested():
        ret = connection.execute(stmt).one_or_none()

    match ret:
        case None:
            logger.debug(
                "sample not inserted. Table unique keys combination already exist",
                event=Event.ROW_INSERT_DUPLICATE,
            )
        case (_, None):
            logger.debug("sample inserted", event=Event.ROW_INSERT_SUCCESS)
        case (_, _):
            logger.debug("sample updated", event=Event.ROW_UPDATE_SUCCESS)


def insert_or_update_records(
    table: Table, records: Iterable[Record], unique_columns: tuple[str]
) -> None:
    """Insert or update the records in the table in nested transactions."""
    for record in records:
        try:
            insert_or_update_record(table, record, unique_columns)
        except sqlalchemy.exc.SQLAlchemyError as err:
            error_headline = err.args[0].split("\n")[0]
            logger.opt(exception=True).warning(
                "sample insertion failed: {error_headline}",
                error_headline=error_headline,
                event=Event.ROW_OPERATION_FAIL,
                error_context=err.args,
            )


# Mapping of default datawarehouse interaction function for each table.
# Interaction with database from task payload is only allowed with the tables in
# the following mapping.
# 2024.09.04 from Karl Welter: Exceptionally, payments_ts is updatable
# (for the purpose of correcting incorrectly submitted payment data)
TABLE_DEFAULT_METHOD = {
    # updatable tables
    "data_custom": insert_or_update_records,
    "data_grids": insert_or_update_records,
    "data_meters": insert_or_update_records,
    "data_shs": insert_or_update_records,
    "data_test": insert_or_update_records,
    "data_trust_trace": insert_or_update_records,
    "data_payments_ts": insert_or_update_records,
    # non-updatable tables
    "data_grids_ts": insert_records,
    "data_meter_events_ts": insert_records,
    "data_meters_ts": insert_records,
    "data_paygo_accounts_snapshots_ts": insert_records,
    "data_shs_ts": insert_records,
}


def _is_insert_method(table: str) -> bool:
    return TABLE_DEFAULT_METHOD.get(table) == insert_records


def count_sample_values(sample: dict) -> tuple[int, int]:
    """Count standard and extra values of a sample."""
    value_count = len(sample.keys())
    if custom_fields := sample.get("custom"):
        extra_value_count = len(custom_fields)
        value_count -= 1
    else:
        extra_value_count = 0
    return value_count, extra_value_count


@skip_if("dev.disable_datawarehouse_inserts")
@log_process
def store_recordset(recordset: RecordSet, use_update: bool = False) -> None:
    """Store records in tables of the recordset by using update or insert.

    Will update if 'use_update' is True, else get method from TABLE_DEFAULT_METHOD.
    If any record update/insertion fails, it is logged. Others are stored.
    """
    if use_update:
        table_database_function = update_records
    else:
        try:
            table_database_function = TABLE_DEFAULT_METHOD[recordset.table]
        except KeyError as exc:
            raise ValueError(
                f"insertion into table {recordset.table!r} is not supported"
            ) from exc
    logger.info(
        "sending {nb_samples} records to table {table!r} via function {function!r}",
        nb_samples=len(recordset.records),
        event=Event.TABLE_INSERTION_BEGIN,
        table=recordset.table,
        function=table_database_function.__name__,
    )
    table = get_table(recordset.table)
    unique_columns = table_unique_constraint_columns(table)
    records = filter_incomplete_records(
        recordset.records, table_database_function, table
    )
    records = filter_non_existing_records(
        records, table_database_function, table, unique_columns
    )

    # in case all the records already led to a failure
    if not records:
        return

    try:
        table_database_function(table, records, unique_columns)
    except sqlalchemy.exc.SQLAlchemyError as err:
        error_headline = err.args[0].split("\n")[0]
        logger.opt(exception=True).warning(
            "sample insertion failed: {error_headline}",
            error_headline=error_headline,
            event=Event.ROW_OPERATION_FAIL,
            error_context=err.args,
        )


def _log_failed_update_records(
    records: list[Record], matching_conditions: list[BinaryExpression]
) -> None:
    for record, matching_condition in zip(records, matching_conditions, strict=False):
        logger.warning(
            "sample update failed, no record found for matching condition {}",
            matching_condition.compile(compile_kwargs={"literal_binds": True}),
            event=Event.ROW_OPERATION_FAIL,
            record=record,
        )


def filter_non_existing_records(
    records: list[Record],
    table_database_function: callable,
    table: Table,
    unique_columns: tuple[Column],
) -> list[Record]:
    """Filter out records not matchable if strictly updating.

    This is required to distinguish updates not matching existing records,
    which count as failed, from updates not modifying the record, counting updated.
    """
    if table_database_function != update_records or not records:
        return records

    unique_col_names = {col.name for col in unique_columns}
    matching_conditions = [
        match_condition_from_unique_col_names(table, unique_col_names, record)
        for record in records
    ]
    matching_condition = functools.reduce(operator.or_, matching_conditions)
    statement = select(table).where(matching_condition)
    result = get_or_create_connection().execute(statement)
    if result.rowcount == len(records):
        return records
    # return an empty list and log all if none is matching
    if result.rowcount == 0:
        _log_failed_update_records(records, matching_conditions)
        return []

    # now remove the non existing records and add corresponding failed events
    existing_records = {
        tuple(existing[col] for col in unique_col_names)
        for existing in result.mappings()
    }
    missing_records_indices = []
    for i, record in enumerate(records):
        if tuple(record[col] for col in unique_col_names) not in existing_records:
            missing_records_indices.append(i)
    failed_records = []
    failed_matching_conditions = []
    for index in reversed(missing_records_indices):
        failed_records.append(records.pop(index))
        failed_matching_conditions.append(matching_conditions[index])

    _log_failed_update_records(failed_records, failed_matching_conditions)
    return records


def filter_incomplete_records(
    records: list[Record], table_database_function: callable, table: Table
) -> list[Record]:
    """Filter out records having nulls for required columns."""
    indices_to_be_discarded = []
    for i, record in enumerate(records):
        # first bind sample and table as they can't be evaluated lazily later
        logger.bind(sample=record, table=table.name).opt(lazy=True).debug(
            "sending record: {counts[0]} standard fields + {counts[1]} extra fields",
            counts=functools.partial(count_sample_values, record),
        )
        logger.opt(lazy=True).trace("{rec}", rec=functools.partial(pformat, record))

        # Check and fail if any required non-nullable columns is not satisfied
        is_insert = table_database_function == insert_records
        required = required_but_null_columns(
            table=table, record=record, is_insert=is_insert
        )
        if required:
            indices_to_be_discarded.append(i)
            logger.warning(
                "sample insertion failed: {required}",
                required=required,
                event=Event.ROW_OPERATION_FAIL,
                record=record,
            )

    for index in reversed(indices_to_be_discarded):
        records.pop(index)
    return records
