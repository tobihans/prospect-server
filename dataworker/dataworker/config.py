"""Configuration module for the dataworker, powered by dynaconf."""

import dynaconf

config = dynaconf.Dynaconf(
    preload=["config-local-default.toml"],
    settings_file=[
        "config.toml",
        "config-secret-*.toml",
        "config-extra-*.toml",
    ],
    envvar_prefix="DW_",
    load_dotenv=True,
    merge_enabled=True,
)
