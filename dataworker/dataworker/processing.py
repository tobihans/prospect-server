"""Helper functions and utilites for dataworker tasks."""

import contextlib
from collections.abc import Callable
from datetime import UTC, date, datetime
from functools import cache, partial
from typing import Any

import country_converter
import funcy
import pandas as pd
import sqlalchemy
from dateutil.parser import parse
from glom import Coalesce, M

from dataworker import logger
from dataworker.database import get_table
from dataworker.time import stringify_date
from dataworker.types import DictDataset, Record, Value
from dataworker.utils import nofail

# ------------------------- type oriented processing -------------------------

# -- INT -----


INT4_MIN = -2147483648
INT4_MAX = +2147483647


def clip_int4(number: int) -> int:
    """Limit value of signed integer so that it fits into 4 bytes."""
    return min(max(number, INT4_MIN), INT4_MAX)


# -- STR -----


def wrap(wrapper: str, string: str) -> str:
    """Surround string with something.

    ex:
        wrap("*", "important") => "*important*"
        wrap(("(", ")"), "detail") => "(detail)"
    """
    match wrapper:
        case str(char):
            return char + string + char
        case (left, right):
            return left + string + right
    raise ValueError("wrapper must be a string or tuple")


quote = funcy.partial(wrap, '"')


def strip_string(string: str) -> str:
    """Remove leading and trailing whitespace from string."""
    return string.strip()


# First-order data cleaning for STR.
cast_to_str = funcy.silent(
    funcy.rcompose(lambda x: str(x) if x is not None else None, strip_string)
)


# -- BOOL -----


def convert_to_bool(string: str | int | float) -> bool | None:
    """Convert to a boolean if possible."""
    if isinstance(string, str):
        lower_stripped = string.strip().lower()
        if lower_stripped in ["true", "t", "1", "yes", "y"]:
            return True
        if lower_stripped in ["false", "f", "0", "no", "n"]:
            return False
    elif isinstance(string, int):
        if string in (0, 1):
            return bool(string)
    elif isinstance(string, float) and string in (0.0, 1.0):
        return bool(string)
    return None


# First-order data cleaning for BOOL.
cast_to_bool = funcy.silent(convert_to_bool)


# --- DATETIME ----------


def convert_to_datetime(input: datetime | date | str) -> datetime | date | None:
    """Convert to a datetime if possible."""
    if isinstance(input, datetime | date):
        return input
    if isinstance(input, str):
        try:
            dt = parse(input)
        except Exception:
            logger.exception("Could not parse string {} to a datetime", input)
            raise
        if not dt.tzinfo:
            dt = dt.replace(tzinfo=UTC)
        return dt


cast_to_dt = funcy.silent(convert_to_datetime)

# --- ENUM --------------


def nullify_invalid_enum_val(
    enum: sqlalchemy.Enum, input: str | int
) -> str | int | None:
    """Type adjust and nullify illegal enum values."""
    if isinstance(input, str):
        input = input.strip()
    if not input:
        return None
    if not isinstance(input, enum.python_type):
        with contextlib.suppress(TypeError):
            input = enum.python_type(input)
    # special handling for gender
    if enum.name == "gender":
        input = nofail(resolve_gender)(input) or input
    # special handling for gender
    elif enum.name == "phase":
        input = nofail(resolve_phase)(input) or input
    if input in enum.enums:
        return input
    logger.warning("Nullifying invalid value {} for enum {}.", input, repr(enum))
    return None


nullify_invalid_enums = funcy.silent(nullify_invalid_enum_val)

# ------------------------- field oriented processing -------------------------

# -- GEO -----


def resolve_country_code(country_string: str) -> str:
    """Resolve string to ISO 3166-1 alpha-2 country code.

    Set unknown values to 'XX'.
    """
    return country_converter.CountryConverter().convert(
        country_string, to="ISO2", not_found="XX"
    )


def validate_latitude(latitude: float) -> float:
    """Validate whether the provided coord is within legal range."""
    if -90 <= latitude <= 90:
        return latitude
    else:
        raise ValueError("Latitude must be between -90 and 90.")


def validate_longitude(longitude: float) -> float:
    """Validate whether the provided coord is within legal range."""
    if -180 <= longitude <= 180:
        return longitude
    else:
        raise ValueError("Longitude must be between -180 and 180")


# -- GENDER -----


def resolve_gender(gender_string: str) -> str | None:
    """Resolve string according to mapping."""
    match gender_string.strip().lower():
        case "male" | "m":
            return "M"
        case "female" | "f":
            return "F"
        case "other" | "o":
            return "O"
        case _:
            raise ValueError(
                f"Gender input string does not match any known values: {gender_string}"
            )


# -- PHASE ------


def resolve_phase(phase_string: str) -> str | None:
    """Resolve string according to mapping.

    Besides 1,2,3, is sometimes returned as A,B,C and more rarely as
    Red, Yellow, Blue, the old UK wiring colors.
    """
    match phase_string[0]:
        case "1":
            return "1"
        case "2":
            return "2"
        case "3":
            return "3"
    match phase_string.upper():
        case "A" | "RED":
            return "1"
        case "B" | "YELLOW":
            return "2"
        case "C" | "BLUE":
            return "3"
    raise ValueError(
        f"Phase input string does not match any known values: {phase_string}"
    )


# -- DATETIME -----


def stringify_dates_in_sample(sample: dict) -> dict:
    """Stringify dates inside sample."""
    return funcy.walk_values(stringify_date, sample)


# ------------------------- dataset cleaning -------------------------


def recursive_filter_values(filter: Callable, smth: Any) -> dict[str, Any] | Any:  # noqa: ANN401
    """Apply a filter on values and nested values of a dict.

    Do not work through arrays.
    """
    if isinstance(smth, dict):
        return {
            k: recursive_filter_values(filter, v)
            for k, v in funcy.select_values(filter, smth).items()
        }
    else:
        return smth


# Recursively remove all entries of a nested dict that have a None value
shed_nones = funcy.partial(recursive_filter_values, funcy.complement(funcy.isnone))


# ------------------------- utilities -------------------------

# -- GLOM -----


def optional(key: str) -> Coalesce:
    """Call glom's Coalesce with a default of None so that it plays well with SQL."""
    return Coalesce(key, default=None)


def optional_float(key: str) -> Coalesce:
    """Call glom's Coalesce with a default of None so that it plays well with SQL.

    Additionally, ensure non-null result is of type float, else return None.
    """
    return Coalesce((key, M, float), default=None)


# -- PANDAS and other Nones -----


def df_nan_to_none(in_df: pd.DataFrame) -> pd.DataFrame:
    """Convert DataFrame '', nan, NaT and null to None to plays well with SQL."""
    return in_df.replace("", None).astype(object).where(in_df.notna(), None)


# ------------------------- shared post-processing -------------------------


# Processing function dispatcher

# any function that processes a single value in a dataset
ValueProcessor = Callable[[Value], Value]
FunctionMapper = Callable[[str, str], ValueProcessor]


@cache
def _type_based_processor_mapper(table: str, field: str) -> ValueProcessor:
    """Return the cleaning function that's specific to the target field."""
    try:
        column = getattr(get_table(table).columns, field)
    except (AttributeError, RuntimeError):
        return funcy.identity
    match column.type:
        case sqlalchemy.VARCHAR():
            return cast_to_str
        case sqlalchemy.BOOLEAN():
            return cast_to_bool
        case sqlalchemy.Enum():
            return partial(nullify_invalid_enums, column.type)
        # case sqlalchemy.DOUBLE_PRECISION() | sqlalchemy.FLOAT():
        #     return cast_to_float
        # case sqlalchemy.INTEGER() | sqlalchemy.BIGINT():
        #     return cast_to_int
        # TODO: add general processing here and remove from inside tasks
        case sqlalchemy.TIMESTAMP():
            # convert to datetime to stabilize string representation of this field
            # which is used to generate a uid
            if table == "data_payments_ts" and field == "paid_at":
                return cast_to_dt
            return funcy.identity
        case _:
            return funcy.identity


@cache
def _field_based_processor_mapper(table: str, field: str) -> ValueProcessor:
    """Return the cleaning function that corresponds to the target data type."""
    match table, field:
        # -- data_grids -----
        case ("data_grids", "country"):
            processor = resolve_country_code
        case ("data_grids", "latitude"):
            processor = validate_latitude
        case ("data_grids", "longitude"):
            processor = validate_longitude
        # -- data_meters -----
        case ("data_meters", "customer_country"):
            processor = resolve_country_code
        # -- data_shs -----
        case ("data_shs", "customer_country"):
            processor = resolve_country_code
        case ("data_shs", "seller_country"):
            processor = resolve_country_code
        case _:
            return funcy.identity

    return nofail(processor)


# Record-oriented functions


def _process_record_values(
    record: Record, table: str, function_mapper: FunctionMapper
) -> Record:
    """Map and apply functions to record values based on table name."""
    return {key: function_mapper(table, key)(value) for key, value in record.items()}


def process_record_values_by_type(record: Record, table: str) -> Record:
    """Apply processing to a record's values depending on destination column type."""
    return _process_record_values(
        record, table, function_mapper=_type_based_processor_mapper
    )


def process_record_values_by_field(record: Record, table: str) -> Record:
    """Apply processing to a record's values depending on destination column field."""
    return _process_record_values(
        record, table, function_mapper=_field_based_processor_mapper
    )


# Dataset-oriented functions


def _process_dataset_values(
    dataset: DictDataset, function_mapper: FunctionMapper
) -> DictDataset:
    """Map and apply functions to dataset based on table and field name."""
    return {
        table: [
            {
                field: (function_mapper(table, field)(value))
                for field, value in record.items()
            }
            for record in records
        ]
        for table, records in dataset.items()
    }


def process_dataset_values_by_type(dataset: DictDataset) -> DictDataset:
    """Adjust dataset values according to the column type of the destination table."""
    return _process_dataset_values(
        dataset, function_mapper=_type_based_processor_mapper
    )


def process_dataset_values_by_field(dataset: DictDataset) -> DictDataset:
    """Adjust dataset values according to the column of the destination table.

    Will adapt data to i.e. country or geolocation.
    """
    return _process_dataset_values(
        dataset, function_mapper=_field_based_processor_mapper
    )
