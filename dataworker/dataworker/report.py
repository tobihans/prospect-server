"""Module containing functions and classes related to logging and reporting."""

import datetime
import json
import operator
import random
from collections.abc import Callable, Generator, Iterable, Iterator, Sequence
from dataclasses import dataclass
from operator import attrgetter
from pathlib import Path
from typing import Any, ParamSpec, Self

import arrow
import funcy
import humanize
import pandas
import requests
import rich
import sqlalchemy
import tqdm
import yaml
from boltons import iterutils
from glom import Coalesce, Match, T, glom
from rich import box, print
from rich.console import Group
from rich.panel import Panel
from rich.style import Style
from rich.table import Table
from rich.text import Text
from rich.tree import Tree

from dataworker import config, logger
from dataworker.database import (
    configured_database_url,
    get_connection,
    get_table,
    get_table_prefix_resilient,
)
from dataworker.logging import timer
from dataworker.processing import quote, wrap
from dataworker.queries import count_rows


def file_size_string(file: str) -> str:
    """Return file size as string."""
    return humanize.naturalsize(Path(file).stat().st_size)


def format_timestamp(ts: datetime.datetime) -> str:
    """Format datetime object to zulu datetime string format."""
    return ts.strftime("%Y-%m-%d %k:%M:%S %Z")


def format_time(ts: datetime.time) -> str:
    """Format time object to zulu time string format."""
    return ts.strftime("%k:%M:%S %Z")


def format_duration(delta: datetime.timedelta) -> str:
    """Format deltatime to human-readable string."""
    return humanize.time.naturaldelta(delta)


def read_lines(file_path: str) -> Generator[str]:
    """Generator yielding lines from file_path."""
    logger.debug("reading {}", file_path)
    with Path(file_path).expanduser().resolve().open() as file:
        count = 0
        for line in file:
            count += 1
            yield line
    logger.debug("{} lines read", count)


@timer
def load_jsonl_logs(log_file: str) -> list[dict]:
    """Read JSON logs lines from file.

    The expected log format is one json object per line. Load errors are ignored
    """
    lines = read_lines(log_file)
    objects = funcy.map(r"{.*}", lines)  # extract json object in line
    logs = funcy.lkeep(funcy.silent(json.loads)(object_) for object_ in objects)
    logger.debug("{} objects parsed", len(logs))
    return logs


@timer
def load_json_logs(log_file: str) -> list[dict]:
    """Read JSON logs from file.

    The expected log format is one json list.
    """
    logs = json.loads(Path(log_file).read_text())
    logger.debug("{} objects parsed", len(logs))
    return logs


@timer
def load_loki_json_logs(log_file: str) -> list[dict]:
    """Read JSON logs in Loki format from file.

    The expected log format is one json list, with a nested json in the "line" field.
    """
    content = json.loads(Path(log_file).read_text())
    logs = glom(content, [("line", json.loads)])
    logger.debug("{} objects parsed", len(logs))
    return logs


def log_level_style(level: str) -> str:
    """Derive a color name string from a given logging level, to inform logger style."""
    match level.lower():
        case "debug":
            return "blue"
        case "info":
            return "white"
        case "warning":
            return "dark_orange"
        case "error":
            return "red"
        case _:
            return "white"


# ------------------------- OBJECT VERSION -------------------------


@dataclass
class Log:
    """Dataclass defining log objects."""

    message: str
    level: str
    time: datetime.datetime
    elapsed: datetime.timedelta
    location: str
    process: str
    context: dict

    @classmethod
    def from_dict(cls, dict_: dict) -> Self:
        """Create a Log instance from dictionary."""
        standard_fields_names = [
            "message",
            "level",
            "time",
            "elapsed",
            "location",
            "process",
        ]
        standard_fields = funcy.project(dict_, standard_fields_names)
        if standard_fields.get("time"):
            standard_fields["time"] = arrow.get(standard_fields["time"]).datetime
        context = funcy.omit(dict_, standard_fields_names)
        return cls(
            **standard_fields,
            context=context,
        )

    def __getitem__(self, key: str) -> Any:  # noqa: ANN401
        """Getter returning the value from the context."""
        return self.context.get(key)

    def __repr__(self) -> str:
        """Representation using the message."""
        return f"<Log({self.message!r})>"

    @property
    def colored(self) -> str:
        """Return styled aka colored message as string."""
        return f"[{log_level_style(self.level)}]{self.message}[/]"

    @property
    def tags(self) -> list[str]:
        """Return a list of tags from the context."""
        return funcy.keep([
            tag.strip()
            for tag in (
                self.context.get("tag", "") + self.context.get("tags", "")
            ).split(",")
        ])

    def __str__(self) -> str:
        """String representation returning the message."""
        return self.message

    def __rich__(self) -> str:
        """Richer representation featuring level and message."""
        return f"[{log_level_style(self.level)}]{self.message}[/]"

    def get(self, key: str) -> Any:  # noqa: ANN401
        """Get value by key."""
        return self[key]

    def print(self) -> None:
        """Print log entry as well as exception if there was one."""
        print(self)
        if self["event"] == "error":
            print(self["exception"])
            print(self["traceback"])


def print_as_table(
    seq: Sequence[Sequence],
    names: Sequence[str] | None = None,
    title: str | None = None,
    lines: bool = False,
) -> None:
    """Print collections in a straight table format.

    >>print_table([(1,2,3),(4,5,6),(7,8,9)])
    +---+---+---+
    | 1 | 2 | 3 |
    | 4 | 5 | 6 |
    | 7 | 8 | 9 |
    +---+---+---+
    """
    table = Table(box=box.SQUARE if lines else None, title=title)

    if names:
        for name in names:
            table.add_column(name)
    else:
        table.show_header = False
    for element in seq:
        table.add_row(*element)
    print(table)


@dataclass
class LogCollection:
    """Dataclass defining a list of logs and associated methods."""

    _logs: list[Log]

    @property
    def logs(self) -> list[Log]:
        """Return logs sorted by time."""
        return sorted(self._logs, key=attrgetter("time"))

    # constructors

    @classmethod
    def from_dicts(cls: Self, log_dicts: list[Log]) -> Self:
        """Create a LogCollection from a list of log dicts."""
        logs = funcy.lkeep(funcy.silent(Log.from_dict)(d) for d in tqdm.tqdm(log_dicts))
        collection = cls(logs)
        logger.debug("Collection created with {} valid logs", len(collection))
        return collection

    @classmethod
    def from_file(cls, file: str, loki: bool = False) -> Self:
        """Create a LogCollection from a file containing a list of log dicts.

        Accepts .json and .jsonl file type.
        """
        if loki:
            log_dicts = load_loki_json_logs(file)
        else:
            match Path(file).suffix:
                case ".json":
                    log_dicts = load_json_logs(file)
                case ".jsonl":
                    log_dicts = load_jsonl_logs(file)
                case _:
                    raise ValueError(f"unknown file extension: {file}")
        return cls.from_dicts(log_dicts)

    # magics

    def __repr__(self) -> str:
        """Representation using the logs."""
        return f"<LogCollection({len(self.logs)})>"

    def __iter__(self) -> iter:
        """Iterator over the logs."""
        return iter(self.logs)

    def __getitem__(self, idx: int | slice) -> Log | Self:
        """Itemgetter using an integer as index or a slice."""
        if isinstance(idx, int):
            return self.logs[idx]
        elif isinstance(idx, slice):
            return LogCollection(self.logs.__getitem__(idx))
        else:
            raise TypeError(f"{idx} is not int or slice")

    def __len__(self) -> int:
        """Return length from the logs."""
        return len(self.logs)

    # base operations

    def sample(self, n: int = 1) -> Log | list[Log]:
        """Return n (default 1) samples from the log collection."""
        samples = random.sample(self.logs, n)
        if len(samples) == 1:
            return samples[0]
        else:
            return samples

    def filter(self, filter_function: Callable) -> Self:
        """Filter collections by function."""
        selection = [log for log in self.logs if filter_function(log)]
        return LogCollection(selection)

    def group(self, key_function: Callable) -> dict[str, Self]:
        """Split logs, grouping them by key through a function."""
        grouped = iterutils.bucketize(self.logs, key_function)
        return {key: LogCollection(logs) for key, logs in grouped.items()}

    def get_unique_value(self, key: str) -> Any:  # noqa: ANN401
        """Collect all occurences of a context value that should all be the same.

        Will raise an exception if it's not the case
        """
        value_set = {log.get(key) for log in self}
        if len(value_set) > 1:
            raise ValueError(f"multiple distinct occurences of a value for {key}")
        return value_set.pop()

    # high level

    def runs(self) -> "RunCollection":
        """Split logs by runs.

        Collection with no run_id are discarded
        """
        logger.debug("grouping logs by run")
        runs_with_id = funcy.filter(lambda log: log.get("run_id"), self.logs)
        runs = [
            RunLogs(run_logs)
            for run_logs in funcy.group_by(
                lambda log: log.get("run_id"), runs_with_id
            ).values()
        ]
        return RunCollection(funcy.lfilter(operator.attrgetter("run_id"), runs))

    def stats(self, key_function: Callable) -> pandas.DataFrame:
        """Return a DataFrame showing count of logs per group, by key via a function."""
        return pandas.DataFrame(
            [(key, len(logs)) for key, logs in self.group(key_function).items()],
            columns=("key", "count"),
        ).set_index("key")

    def search_unique_event(self, event: str) -> Log | None:
        """Get a unique log event or none if it does not exist.

        Raises exception if multiple logs are found.
        """
        logs = self.select_event(event)
        match len(logs):
            case 0:
                return
            case 1:
                return logs[0]
            case _:
                raise Exception(  # noqa: TRY002
                    f"Found multiple logs for event {event}, expected to be unique."
                )

    def select_event(self, event: str) -> Self:
        """Returns a LogCollection filtered by event."""
        return self.filter(
            lambda log: log.get("event") is not None
            and log.get("event").lower() == event.lower()
        )

    def select_run(self, run_id: str) -> "RunLogs":
        """Create a RunLogs Instance from the filtered RunCollection by run_id."""
        return RunLogs(
            self.filter(lambda log: log.get("run_id").lower() == run_id.lower())
        )

    def print_messages(
        self,
        location: bool = False,
        context: bool | Iterable = False,
        index: bool = True,
        process: bool = False,
    ) -> None:
        """Print messages, with option for more information.

        location: code location of the origin of the log
        context: log context data
        index: enumerate logs
        process: which process emmited the log
        """
        selectors = [lambda log: log.get("event"), funcy.identity]
        if location:
            selectors.insert(0, lambda log: log.location)
        if process:
            selectors.insert(0, lambda log: log.process)
        if context:
            if context is True:

                def context_selector(log: Log) -> str:
                    if context_ := log.context:
                        return yaml.dump(context_)

                selectors.append(context_selector)
            elif isinstance(context, Iterable):

                def context_selector(log: Log) -> str:
                    if context_ := funcy.project(log.context, context):
                        return str(context_)

                selectors.append(context_selector)
        if index:
            rows = [
                [str(i)] + [selector(log) for selector in selectors]
                for i, log in enumerate(self.logs)
            ]
        else:
            rows = [[selector(log) for selector in selectors] for log in self.logs]
        print_as_table(rows)

    def print(self, **kwargs: dict) -> None:
        """Print messages."""
        self.print_messages(**kwargs)


class RunLogs(LogCollection):
    """Sub(data)class of LogCollection providing extra methods."""

    def __init__(self, *args: tuple, **kwargs: dict) -> None:
        """Init the object and set a unique run_id."""
        super().__init__(*args, **kwargs)
        self.run_id = self.get_unique_value("run_id")

    def __repr__(self) -> str:
        """Representation using the task and the logs length."""
        return f"<RunLogs task={self.task!r} nb={len(self.logs)}>"

    @property
    def start_event(self) -> Log | None:
        """Get unique start event log from the logs."""
        return self.search_unique_event("start")

    @property
    def end_event(self) -> Log | None:
        """Get unique end event log from the logs."""
        return self.search_unique_event("end")

    @property
    def error_event(self) -> Log | None:
        """Get unique error event log from the logs."""
        return self.search_unique_event("error")

    # ---

    @property
    @funcy.silent
    def start_time(self) -> datetime.time:
        """Get the time from unique start event."""
        return self.start_event.time

    @property
    @funcy.silent
    def import_id(self) -> str:
        """Get the import_id from unique start event."""
        return self.start_event.get("import_id")

    @property
    @funcy.silent
    def task(self) -> str:
        """Get the task from unique start event."""
        return self.start_event.get("task")

    @property
    @funcy.silent
    def file_key(self) -> str:
        """Get the file_key from unique start event."""
        return self.start_event.get("file_key")

    @property
    @funcy.silent
    def end_time(self) -> datetime.time:
        """Get the time from unique end event."""
        return self.end_event.time

    @property
    @funcy.silent
    def has_completed(self) -> bool:
        """Return True if logs have start, end and complete events, else False."""
        return (
            (self.start_event is not None)
            and (self.end_event is not None)
            and (self.search_unique_event("complete") is not None)
        )

    @property
    @funcy.silent
    def duration(self) -> datetime.timedelta:
        """Get the duration of event from start and end timestamps."""
        return self.end_event.time - self.start_event.time

    def print_run_data(self) -> None:
        """Print important data about the run itself."""
        run_info = [
            ("task name", f"[bold]{self.task}"),
            ("run_id", self.run_id),
            ("import_id", str(self.import_id)),
            ("file_key", f"[bold]{self.file_key}"),
            (
                "start_time",
                (
                    self.start_time.isoformat(timespec="seconds")
                    if self.start_time
                    else None
                ),
            ),
        ]
        if self.has_completed:
            run_info.extend((
                ("status", "[green]complete"),
                (
                    "duration",
                    humanize.naturaldelta(self.duration, minimum_unit="milliseconds"),
                ),
            ))
        else:
            run_info.append((
                "status",
                "[red]incomplete",
            ))
        print_as_table(run_info)

    def print(self, **kwargs: dict) -> None:
        """Print important data about the run and the messages inside the logs."""
        self.print_run_data()
        print("-" * 30)
        self.print_messages(**kwargs)


@dataclass
class RunCollection:
    """Dataclass gathering the RunLogs."""

    _runs: list[RunLogs]

    @property
    def runs(self) -> list[RunLogs]:
        """Return RunLogs sorted by start time."""
        return sorted(self._runs, key=attrgetter("start_time"))

    @property
    def last(self) -> RunLogs:
        """Return the RunLog with the greatest start time."""
        return self.runs[-1]

    # magics

    def __repr__(self) -> str:
        """Representation using the number of runs."""
        return f"<RunCollection({len(self._runs)})>"

    def __iter__(self) -> Iterator[RunLogs]:
        """Iterator over the runs."""
        return iter(self._runs)

    def __getitem__(self, idx: int) -> RunLogs:
        """Itemgetter by index (integer)."""
        return self._runs[idx]

    def __len__(self) -> int:
        """Return the length of the _runs list."""
        return len(self._runs)

    # actions

    def sample(self, n: int = 1) -> RunLogs | Self:
        """Return n (default 1) samples from the run collection."""
        if n == 1:
            return random.choice(self._runs)  # noqa: S311
        elif n > 1:
            return RunCollection(random.sample(self._runs, n))
        else:
            raise ValueError(f"Got {n}, expected to be greater than or equal to 1.")

    def filter(self, filter_function: Callable) -> Self:
        """Filter run collections by function."""
        selection = [run for run in self.runs if filter_function(run)]
        return RunCollection(selection)

    def filter_attribute(self, name: str, value: bool) -> Self:
        """Filter run collections by attribute name and value."""

        def filter_function(run: RunCollection) -> bool:
            return getattr(run, name, None) == value

        return self.filter(filter_function)

    def print(self, **kwargs: dict) -> None:
        """Print RunCollection as table."""
        selectors = [
            lambda run: run.import_id,
            lambda run: run.file_key,
            lambda run: "[green]complete" if run.has_completed else "[red]error",
        ]
        rows = [[selector(run) for selector in selectors] for run in self._runs]
        print_as_table(rows, **kwargs)

    # common filters

    def _filter_complete(self, has_completed: bool = True) -> Self:
        """Filter run collections by 'has_completed' attribute."""
        return self.filter_attribute("has_completed", has_completed)


# ------------------------- IMPORT TRACKING VISUALIZATION -------------------------


@dataclass
class ImportInfo:
    """Dataclass gathering import information."""

    _data: dict

    @classmethod
    def from_id(cls, import_id: str) -> Self:
        """Create ImportInfo instance from the database given an import_id."""
        import_table = get_table("imports")
        query = import_table.select().where(import_table.c.id == import_id)

        # raise also if no result is found
        with get_connection() as connection:
            import_row = connection.execute(query).one()
        return cls(import_row._mapping)

    def __repr__(self) -> str:
        """Represent using the _data.id."""
        return f"ImportInfo.from_id({self._data['id']})"

    def table(self) -> rich.table.Table:
        """Return a rich table containing all import info, to be printed."""
        table = rich.table.Table(title="import info", show_header=False)

        timespec = Coalesce(
            (Match(datetime.datetime), T.isoformat(timespec="seconds", sep=" ")),
            default="",
        )

        table_spec = [
            [("import", ("id", str)), ("source", ("source_id", str))],
            [
                ("creation", ("created_at", timespec)),
                ("update", ("updated_at", timespec)),
                ("ingestion start", ("ingestion_started_at", timespec)),
                ("ingestion finish", ("ingestion_finished_at", timespec)),
                ("processing start", ("processing_started_at", timespec)),
                ("processing finish", ("processing_finished_at", timespec)),
            ],
            [
                ("insertions", ("rows_inserted", str)),
                ("update", ("rows_updated", str)),
                ("ignored", ("rows_duplicated", str)),
                ("errors", ("rows_failed", str)),
            ],
            [
                ("protocol", "protocol"),
            ],
            [
                ("error", "error"),
            ],
        ]

        for section in table_spec:
            for name, spec in section:
                if (value := glom(self._data, spec)) is not None:
                    table.add_row(name, value)
            table.add_section()
        return table


# ------------------------- DATABASE SUMMARY -------------------------


def table_count_element(alias: str, table_name: str) -> Text:
    """Return a rich Text with counts for a table."""
    count = count_rows(table_name)
    style = Style(bold=True, color="green" if count else "bright_black")
    count = Text(
        str(count) if count < 1000 else humanize.number.metric(count),
        style=style,
    )
    ret = Text.assemble(
        alias, ": ", count, " rows ", Text(f"[{table_name}]", style="bright_black")
    )
    return ret


def table_count_section(section_name: str, tables: list[tuple[str, str]]) -> Tree:
    """Return a Tree containing counts of the desired tables."""
    section = Tree(Text(section_name, style="bold"))
    for alias, table in tables:
        section.add(table_count_element(alias, table))
    return section


def database_summary() -> Tree:
    """Return a rich Tree containing counts about various database tables."""
    tree = Tree(
        Text("Datawarehouse")
        + " "
        + Text(str(configured_database_url()), style="bright_black")
    )
    tree.add(table_count_section("System", [("imports", "imports")]))
    tree.add(
        table_count_section(
            "Solar Home Systems",
            [
                ("devices", "data_shs"),
                ("samples", "data_shs_ts"),
            ],
        )
    )
    tree.add(
        table_count_section(
            "Grids",
            [
                ("devices", "data_grids"),
                ("samples", "data_grids_ts"),
            ],
        )
    )
    tree.add(
        table_count_section(
            "Meters",
            [
                ("devices", "data_meters"),
                ("samples", "data_meters_ts"),
                ("events", "data_meter_events_ts"),
            ],
        )
    )
    return tree


# ------------------------- DATABASE INSPECTION -------------------------


def table_structure(table_name: str) -> Group:
    """Create a printable Table object with columns name and types.

    - Mandatory columns have their names colored blue
    - Table name is prefix-flexible. Both <table> and data_<table> give back
      data_<table> object.
    """
    table = get_table_prefix_resilient(table_name)
    columns = Table("name", "type", title=Text(table.name, style="bold"))
    for c in sorted(table.columns, key=lambda c: (c.nullable, c.name)):
        columns.add_row(
            c.name if c.nullable else Text(c.name, style="bold blue"), str(c.type)
        )

    def show_index(index: sqlalchemy.Index) -> Text:
        return Text(
            ", ".join(column.name for column in index.columns),
            style="blue" if index.unique else None,
        )

    indexes = Text.assemble(
        *funcy.interpose(
            "\n",
            [
                show_index(index)
                for index in sorted(
                    table.indexes, key=attrgetter("unique"), reverse=True
                )
            ],
        )
    )
    indexes_panel = Panel(
        Group(indexes), title=Text("Indexes", style="bold"), expand=False
    )
    return Group(columns, indexes_panel)


# ------------------------- PRINT RESULTS -------------------------


def _print_rows_as_table(rows: sqlalchemy.CursorResult, title: str) -> None:
    if not rows:
        print("no rows to print")
        return
    table = rich.table.Table(title=title)
    # use first row for column headers
    for column in rows[0]._asdict():
        table.add_column(column)
    for row in rows:
        table.add_row(*funcy.lmap(str, row))
    print(table)


Param = ParamSpec("Param")


def print_rows(func: Callable[Param, sqlalchemy.Executable]) -> Callable[Param, None]:
    """Decorator. Execute the returned statement and print rows as table.

    Accepts an extra keyword arg `title` to name the table.

    @print_rows
    def build_statment(...):
        return sqlalchemy.select(some_table).where(some_condition)
    """

    def wrapped(
        *args: Param.args, title: str | None = None, **kwargs: Param.kwargs
    ) -> None:
        statement = func(*args, **kwargs)
        rows = get_connection().execute(statement).all()
        _print_rows_as_table(rows, title=title or str(statement))

    return wrapped


def print_table(table: str) -> None:
    """Print the WHOLE database table."""
    print_rows(lambda: sqlalchemy.select(table))(title=table.name)


# ------------------------- Fetch from Loki -------------------------


def build_logql_stream_selector(filters: dict) -> str:
    """Render a dictionary to LogQL syntax.

    ex: {host="abidjan", container_name="prospect_dataworker"}
    """
    return wrap(
        ("{", "}"),
        ", ".join(
            funcy.walk(
                lambda item: "=".join((item[0], quote(item[1]))), filters.items()
            )
        ),
    )


def fetch_logs(filters: dict) -> Any:  # noqa: ANN401
    """Query logs from Loki."""
    import furl

    LOKI_API_PATH = "/loki/api/v1/query_range"
    api_endpoint = furl.furl(scheme="https", host=config.logs.host, path=LOKI_API_PATH)
    logger.info("querying logs from {}", api_endpoint)
    query = build_logql_stream_selector(filters)
    logger.info("query: {}", query)
    response = requests.get(
        api_endpoint,
        auth=(config.logs.user, config.logs.password),
        params={"query": query},
        timeout=5,
    ).json()
    return glom(
        response,
        (
            "data",
            "result",
            "0",  # until we encounter a case where we receive more than one result
            "values",
            [("1", json.loads)],  # apply for each value of the result
            LogCollection.from_dicts,  # directly load into a collection
        ),
    )
