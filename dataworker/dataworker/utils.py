"""Generic utils."""

import sys
from collections.abc import Callable, Mapping
from functools import wraps
from threading import Lock
from typing import Any, Generic, ParamSpec, TypeVar

import funcy

from dataworker import config, logger

Param = ParamSpec("Param")
T = TypeVar("T")


class SingletonMixin(Generic[T]):
    """Mixin class to turn a class into a thread-safe singleton class.

    Allows to define a _post_init_func to be executed also a single time.
    As we allow a function to run and also wants this to be thread-safe, you have to
    pass an attribute name in _init_check_attribute that will determine if
    initialization and the function to run after have run and thread lock it in the
    __init__ method too.

    Recipe adapated from:
    https://medium.com/analytics-vidhya/how-to-create-a-thread-safe-singleton-class-in-python-822e1170a7f6

    TODO: adapt/simplify as we don't need multithreading anymore
    """

    _instance: T | None = None
    _lock: Lock = Lock()  # TODO: if still required, use RLock
    _post_init_func: Callable | None = None
    _init_check_attribute: str

    def __new__(cls) -> T:
        """Make the singleton thread-safe."""
        if cls._instance is None:
            with cls._lock:
                # Another thread could have created the instance
                # before we acquired the lock. So check that the
                # instance is still nonexistent.
                if cls._instance is None:
                    cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, *args: Param.args, **kwargs: Param.kwargs) -> None:
        """Initialize one single-time and execute post_init_func if defined.

        We check through the given attr if the instance was initiated completely
        and also here use a locking mechanism to do it only once.
        """
        if not getattr(self._instance, self._init_check_attribute, None):
            with self._lock:
                # Another thread could have run the init
                # before we acquired the lock. So check that the
                # condition is still False.
                if not getattr(self._instance, self._init_check_attribute, None):
                    super().__init__(*args, **kwargs)
                    if self._post_init_func:
                        self._post_init_func()
                self._instance = self


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def skip_if(config_key: str) -> Callable[Param, RetType | None]:
    """Set an off-switch on the decorated function.

    If config_key is True, the function call is skipped
    """

    def decorator(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
        @wraps(func)
        def wrapped(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
            if not (config.get(config_key, False)):
                return func(*args, **kwargs)
            else:
                logger.warning(
                    "{} was skipped because config.{} is True",
                    func.__name__,
                    config_key,
                )

        return wrapped

    return decorator


RetType = TypeVar("RetType")


def unpack(func: Callable[..., RetType]) -> Callable[..., RetType]:
    """Feed unpacked tuple argument to the decorated function.

    Allow using lambdas with multiple arguments to function that would otherwise
    send tuples to the lambda. This can help in code clarity.

    Example:
        filter(unpack(lambda lat,long:lat>0),coordinates)
        # instead of
        filter(lambda coord:coord[0]>0,coordinates)
    """

    @wraps(func)
    def wrapped(packed_args: tuple) -> RetType:
        return func(*packed_args)

    return wrapped


@funcy.decorator
def nofail(call: Callable) -> Callable:
    """Prevent function call from raising exception.

    Instead, log function and args as warning and return None.
    """
    try:
        return call()
    except Exception as exc:
        logger.opt(exception=True).warning(
            "processing error in {function}",
            exception=exc,
            function=(
                f"{call._func.__module__}.{call._func.__name__}:"
                f"{sys.exc_info()[2].tb_frame.f_lineno}"
            ),
            args=call._args,
            kwargs=call._kwargs,
        )


@nofail
def map_access(map: Mapping, index: str) -> Any:  # noqa: ANN401
    """Safely access map, with logging."""
    try:
        return map[index]
    except KeyError as err:
        raise KeyError(f"key {err.args[0]} do not address map") from err
