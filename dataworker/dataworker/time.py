"""Module containing timestap parsing and conversion methods."""

import datetime


def parse_utc_s_timestamp(ts: int | float) -> datetime.datetime:
    """Convert a timestamp in seconds to a UTC datetime object."""
    return datetime.datetime.fromtimestamp(float(ts), tz=datetime.UTC)


def parse_utc_ms_timestamp(ts: int | float) -> datetime.datetime:
    """Convert a timestamp in milliseconds to a UTC datetime object."""
    return parse_utc_s_timestamp(float(ts) / 1e3)


def parse_utc_us_timestamp(ts: int | float) -> datetime.datetime:
    """Convert a timestamp in microseconds to a UTC datetime object."""
    return parse_utc_s_timestamp(float(ts) / 1e6)


def stringify_date(value: str | datetime.datetime) -> str:
    """Convert datetime objects values into iso strings."""
    match value:
        case datetime.datetime():
            return value.isoformat()
        case _:
            return value
