"""Module containing all functions related to database queries."""

from collections.abc import Callable, Iterable, Sequence
from functools import wraps
from pathlib import Path
from typing import Any, ParamSpec

import funcy
import sqlalchemy
import sqlalchemy.sql.functions
from sqlalchemy import func, select, text
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.compiler import SQLCompiler
from sqlalchemy.sql.expression import FunctionElement

from dataworker import config, logger, processing
from dataworker.database import (
    ensure_table_object,
    get_connection,
    get_or_create_connection,
    get_table,
)

# ------------------------- EXECUTION -------------------------


def execute_admin_database_command(*commands: tuple[str, ...]) -> None:
    """Execute commands from admin database in autocommit mode.

    This mode of execution is required for commands like database creation and deletion
    """
    connection = get_or_create_connection("postgres", isolation_level="AUTOCOMMIT")
    for command in commands:
        logger.debug("running db admin command: {}", command)
        connection.execute(text(command))


def run_sql_from_file(file: str, name: str | None = None, commit: bool = False) -> None:
    """Execute sql commands from a file."""
    connection = get_or_create_connection(name)
    logger.info(
        "running sql from file {} into database {}", file, connection.engine.url
    )
    commands = Path(file).read_text()
    connection.execute(text(commands))
    if commit:
        connection.commit()


Param = ParamSpec("Param")


def execute(
    func: Callable[Param, sqlalchemy.Executable],
) -> Callable[Param, Sequence[sqlalchemy.Row] | None]:
    """Decorator. Execute sqlalchemy statement returned by the function.

    @execute
    def insert_data(values):
        return sqlalchemy.insert(that_table).values(**values)
    """

    @wraps(func)
    def wrapped(
        *args: Param.args, **kwargs: Param.kwargs
    ) -> Sequence[sqlalchemy.Row] | None:
        statement = func(*args, **kwargs)
        result = get_or_create_connection().execute(statement)
        if result.returns_rows:
            return result.all()

    return wrapped


Param = ParamSpec("Param")


def fetch_dict_data(statement: sqlalchemy.Select) -> sqlalchemy.MappingResult:
    """Execute a sqlalchemy statement and return data as MappingResult iterator."""
    connection = get_connection()
    return connection.execute(statement).mappings()


def fetch_scalars(statement: sqlalchemy.Select) -> sqlalchemy.ScalarResult:
    """Execute a sqlalchemy statement and return data as ScalarResult iterator.

    Yields scalar values from the first column of each row.
    """
    connection = get_connection()
    return connection.scalars(statement)


def fetch_scalar(statement: sqlalchemy.Select) -> Any:  # noqa: ANN401
    """Return a scalar value representing the first column of the first row returned."""
    connection = get_connection()
    return connection.scalar(statement)


# ------------------------- QUERIES -------------------------


def drop_database(database: str) -> None:
    """Drops the requested database if it exists."""
    logger.info("dropping database {}", database)
    execute_admin_database_command(f"drop database if exists {database} with (force);")


def recreate_database(name: str | None = None) -> None:
    """Ensure an empty database of the given name."""
    name = name or config.database.name
    logger.info("creating database : {}", name)
    execute_admin_database_command(
        f"drop database if exists {name};", f"create database {name};"
    )


@execute
def _truncate_table(table: str) -> sqlalchemy.TextClause:
    """Get SqlAlchemy Statement to Truncate table.

    For dev usage.
    """
    logger.debug("truncating {table}", table=table)
    return sqlalchemy.text(f"truncate table {table}")


# ------------------------- query functions returning rows -------------------------


@execute
def select_all(
    table: str, fields: Sequence[str] | None = None
) -> sqlalchemy.CursorResult:
    """Query all rows from a table, ordered by first col of PK."""
    table = ensure_table_object(table)
    order_col = table.primary_key.c.keys()[0]
    if fields is None:
        return select(table).order_by(order_col)
    else:
        return select(*[getattr(table.c, field) for field in fields]).order_by(
            order_col
        )


@execute
def _sample_table(table: str, n: int = 1) -> sqlalchemy.CursorResult:
    """Return n (default 1) random samples from a table.

    For dev usage.
    """
    meters_table = get_table(table)
    return meters_table.select().order_by(sqlalchemy.sql.functions.random()).limit(n)


def fetch_one_row(table_name: str, condition: tuple | None = None) -> dict:
    """Fetch a row as a dict, based on a simple equality condition.

    ex:
    fetch_one_row('meters',('uid','beziers_123_meter_456'))
    """
    table = get_table(table_name)
    statement = table.select()
    if condition is not None:
        condition = table.c.get(condition[0]) == condition[1]
        statement = statement.where(condition)
    connection = get_connection()
    return dict(connection.execute(statement).one()._mapping)


# ------------------------- query functions returning dicts -------------------------


def table_content(
    table: str | sqlalchemy.Table,
    fields: Sequence[str] | None = None,
    sort: str | None = None,
    shed_nulls: bool = False,
    shed_id: bool = False,
    shed_last_ids: bool = False,
    string_timestamps: bool = False,
    shed_cols: list[str] | None = None,
    shed_custom_paths: list[str] | None = None,
) -> list[dict]:
    """All rows from a table, as dicts.

    fields:= list of fields to return
    sort:= key to sort by
    shed_nulls:= remove null fields
    shed_id:= shed 'id' from the dict
    shed_last_ids:= shed 'last_import_id' & 'last_source_id' from the dict
    string_timestamps:= return string as timestamp strings
    shed_cols:= shed the given cols from the dict
    shed_custom_paths := shed the given paths from `custom` from the dicts
    """
    rows = [row._asdict() for row in select_all(table, fields)]
    keys_to_omit = shed_cols or []
    if shed_id:
        keys_to_omit.append("id")
    if shed_last_ids:
        keys_to_omit.extend(["last_import_id", "last_source_id"])
    if shed_nulls:
        rows = funcy.lmap(processing.shed_nones, rows)
    if keys_to_omit:
        rows = funcy.lmap(funcy.partial(funcy.omit, keys=keys_to_omit), rows)
    if sort:
        rows = sorted(rows, key=lambda row: row.get(sort))
    if string_timestamps:
        rows = funcy.lmap(processing.stringify_dates_in_sample, rows)
    if shed_custom_paths:
        rows = funcy.lmap(
            funcy.partial(
                funcy.update_in,
                path=["custom"],
                update=funcy.rpartial(funcy.omit, shed_custom_paths),
                default={},
            ),
            rows,
        )
    return rows


# ------------------------- scalar returning queries -------------------------


def count_rows(table: str, where_column_ops: Iterable | None = None) -> int:
    """Query table for it's number of rows."""
    table = get_table(table)
    query = select(func.count()).select_from(table)
    if where_column_ops:
        query = query.where(*where_column_ops)
    connection = get_connection()
    return connection.scalar(query)


# ----------------- sql function lookups -----------------


class greatest(FunctionElement):
    """Return the highest value from the given arguments (like Python's max)."""

    name = "greatest"
    inherit_cache = True


@compiles(greatest)
def compile_greatest(element: FunctionElement, compiler: SQLCompiler, **kwargs) -> str:  # noqa: ANN003
    """Compile the custom "Greatest" SQL function to its SQL representation."""
    return compiler.visit_function(element)
