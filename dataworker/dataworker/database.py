"""Database operation module.

Providing (among others):
utils, engine management, connection management, sqlalchemy table interactions.
"""

import re
import threading
from collections.abc import Callable, Iterator
from contextlib import contextmanager, nullcontext
from dataclasses import dataclass
from functools import wraps
from typing import ParamSpec, Self, TypeVar

import sqlalchemy
from codenamize import codenamize
from psycopg import capabilities
from psycopg.pq import TransactionStatus
from sqlalchemy import Connection, Engine, MetaData, Pool, Table
from sqlalchemy.engine import URL
from sqlalchemy.engine import create_engine as sqla_create_engine
from sqlalchemy.engine.interfaces import IsolationLevel
from sqlalchemy.event import listens_for
from sqlalchemy.pool.base import _ConnectionRecord

from dataworker import config, logger
from dataworker.utils import SingletonMixin

# global engine registry
engines = {}

# thread storage to keep one connection for each thread
thread_local = threading.local()


# ------------------------- UTILS -------------------------


@contextmanager
def no_echo() -> Iterator[None]:
    """Force the engine 'echo' setting to false inside the context.

    ```
    with no_echo():
        verbose_sql()
    interesting_sql()
    ```
    """
    engine = get_engine()
    echo = engine.echo
    engine.echo = False
    yield
    engine.echo = echo


def connection_state(connection: Connection) -> str:
    """State of the database connection object.

    Possible states are:
        - closed: the connection cannot be used anymore
        - open: the connection is live and ready to be used
        - in transaction: a transaction is in progress
        - in nested transaction: a savepoint has been
    """
    if connection.in_nested_transaction():
        return "in nested transaction"
    elif connection.in_transaction():
        transaction = connection.get_transaction()
        is_active = "active" if transaction.is_active else "inactive"
        is_valid = "valid" if transaction.is_valid else "invalid"
        return f"in transaction: {is_active},{is_valid}"
    elif not connection.closed:
        return "open"
    else:
        return "closed"


@dataclass
class ConnectionInfo:
    """Easy access to connection properties for debugging."""

    name: str  # a naming convention for easier debugging
    db_name: str
    state: str  # open,closed,in transaction(valid/invalid),in nested transaction
    isolation_level: str
    transaction_status: str
    readonly: bool
    autocommit: bool

    @classmethod
    def from_connection(cls, sqla_connection: Connection) -> Self:
        """Extract attributes from sqlalchemy connection object."""
        dbapi_connection = sqla_connection.connection.driver_connection
        params = {
            "name": codenamize(id(sqla_connection)),
            "db_name": dbapi_connection.info.dbname,
            "state": connection_state(sqla_connection),
            "transaction_status": dbapi_connection.info.transaction_status,
            "readonly": dbapi_connection.read_only,
            "autocommit": dbapi_connection.autocommit,
        }
        # This does a query and can raise, but should not. If it does, find the problem!
        params["isolation_level"] = sqla_connection.get_isolation_level()
        return cls(**params)


def inspect_engines() -> None:
    """Log info about engines and their connection pools."""
    for engine in engines.values():
        logger.info("engine for db {}: {}", engine.url.database, engine.pool.status())


def test_database_connection() -> None:
    """Sends an error and exit if database connection can't be opened."""
    try:
        with get_engine("postgres").connect():
            logger.debug("database connection operational")
    except sqlalchemy.exc.OperationalError:
        logger.critical("could not connect to the database")
        exit()


# ------------------------- ENGINE MANAGEMENT -------------------------


def configured_database_url() -> URL:
    """Create an URL object to connect to the default database."""
    if config.database.host != "localhost" and not config.get(
        "monitoring.sentry_dsn", default=None
    ):
        logger.warning(
            "The configured database connection is pointing to a remote server: {}",
            config.database.host,
        )
    return URL.create(
        drivername="postgresql+psycopg",
        host=config.database.host,
        port=config.database.port,
        username=config.database.user,
        password=config.database.password,
        database=config.database.name,
    )


def _create_engine(db_name: str) -> Engine:
    """Create an engine for a given database.

    Databases are all located in the default configured server.
    """
    url = configured_database_url().set(database=db_name)
    options = {
        "echo": config.get("logging.sql", default=False),
        "pool_pre_ping": True,
        "connect_args": {
            "application_name": "dataworker",
            "options": "-c timezone=utc",
        },
    }
    if pool_size := config.get("database.connection_pool_size", default=None):
        options["pool_size"] = pool_size
    if config.get("logging.connection_pool", default=False):
        options["echo_pool"] = "debug"
    return sqla_create_engine(url, **options)


def get_engine(db_name: str | None = None) -> Engine:
    """Access or create an engine to the given database.

    As per SqlAlchemy documentation:
    "The typical usage of create_engine() is once per particular database URL, held
    globally for the lifetime of a single application process"
    https://docs.sqlalchemy.org/en/20/core/connections.html#basic-usage
    """
    db_name = db_name or config.database.name
    if db_name not in engines:
        engines[db_name] = _create_engine(db_name)
    return engines[db_name]


@listens_for(Pool, "connect")
def _on_connect_set_prepared_max(
    dbapi_con: Connection, connection_record: _ConnectionRecord
) -> None:
    """Setting `prepared_max=None` if libpq<17.

    See prepared_statements with pgBouncer
    https://www.psycopg.org/psycopg3/docs/advanced/prepare.html#using-prepared-statements-with-pgbouncer
    """
    if not capabilities.has_send_close_prepared():
        dbapi_con.prepared_max = None


# ------------------------- CONNECTION MANAGEMENT -------------------------


def _set_execution_options(
    connection: Connection,
    isolation_level: IsolationLevel | None = None,
    postgresql_readonly: bool = False,
) -> Connection:
    execution_dict = {"preserve_rowcount": True}
    if isolation_level:
        execution_dict["isolation_level"] = isolation_level
    if postgresql_readonly:
        execution_dict["postgresql_readonly"] = postgresql_readonly

    return connection.execution_options(**execution_dict)


def open_connection(
    db_name: str | None = None,
    isolation_level: IsolationLevel | None = None,
    postgresql_readonly: bool = False,
) -> Connection:
    """Open a new connection from an engine to the configured database.

    Specifying a name uses an engine that connect to another database on the
    same server.
    """
    logger.trace("opening new connection from engine", database=db_name)
    engine = get_engine(db_name)
    try:
        connection = engine.connect()
    except sqlalchemy.exc.OperationalError as exc:
        raise RuntimeError("could not connect to database") from exc
    return _set_execution_options(
        connection=connection,
        isolation_level=isolation_level,
        postgresql_readonly=postgresql_readonly,
    )


def _stringify_connection_name(
    db_name: str | None,
    isolation_level: IsolationLevel,
    postgresql_readonly: bool,
) -> str:
    db_name = db_name or config.database.name
    return f"{db_name}-{isolation_level}-{postgresql_readonly}"


def _wrong_readonly(
    connection: Connection,
    postgresql_readonly: bool = False,
) -> bool | None:
    dbapi_connection = connection.connection.driver_connection
    if bool(dbapi_connection.read_only) != postgresql_readonly:
        return dbapi_connection.read_only


def get_or_create_connection(
    db_name: str | None = None,
    isolation_level: IsolationLevel | None = None,
    postgresql_readonly: bool = False,
) -> Connection:
    """Returns the global connection object.

    This is the function that should be used by all worker processes interacting
    with the database to get a connection object. Those functions MUST NOT do
    the following: starting a transaction, commiting a transaction, rolling back
    a transaction, closing the connection. Function CAN create a savepoint and
    rolling back to the savepoint or commit it.

    Getting a connection through this function allows multiple functions to use
    the same global connection to interact with the database, and respecting the
    rules above allows the connection management to be done in a centralized way
    outside of the task scope.

    If the function is called for the first time, a new connection is open. If
    a connection have been already created, that same connection is returned,
    unless that connection is already closed, in which case a new connection is
    created.

    Since the dataworker can run multiple threads, to threads running different
    tasks to use the same connection, there is one connection container per
    thread.

    `isolation_level` is a str/Literal as defined in IsolationLevel
    Default is back to READ_COMMITTED, as REPEATABLE_READ was creating troubles.

    `postgresql_readonly` is a bool allowing to have read-only connections.
    """
    connection_name = _stringify_connection_name(
        db_name, isolation_level, postgresql_readonly
    )
    if not hasattr(thread_local, "connections"):
        thread_local.connections = {}
    connection = thread_local.connections.get(connection_name)
    if connection is None:
        # open a new connection
        logger.trace("opening first connection")
        thread_local.connections[connection_name] = connection = open_connection(
            db_name, isolation_level, postgresql_readonly
        )
    elif connection.closed:
        logger.trace("opening new connection")
        # current connection is closed, open a new one
        thread_local.connections[connection_name] = connection = open_connection(
            db_name, isolation_level, postgresql_readonly
        )
    elif connection.invalidated:
        logger.trace("rolling back current invalid transaction")
        # current connection terminated from server side
        connection.rollback()
        connection = _set_execution_options(
            connection=connection,
            isolation_level=isolation_level,
            postgresql_readonly=postgresql_readonly,
        )
        thread_local.connections[connection_name] = connection
    else:
        # we have to make sure to not have a connection that is valid, in
        # transaction, but in a useless state
        dbapi_connection = connection.connection.driver_connection
        transaction_status = dbapi_connection.info.transaction_status
        error_message = dbapi_connection.info.error_message
        if (
            transaction_status == TransactionStatus.INERROR
            and "ReadOnlySqlTransaction" not in error_message
        ):
            # NB: setting logger level error as this should not happen
            logger.error("rolling back current in-error transaction", error_message)
            # current connection terminated from server side
            connection.rollback()
            connection = _set_execution_options(
                connection=connection,
                isolation_level=isolation_level,
                postgresql_readonly=postgresql_readonly,
            )
            thread_local.connections[connection_name] = connection
        elif transaction_status in (
            TransactionStatus.UNKNOWN,
            TransactionStatus.INERROR,
        ):
            # NB: setting logger level error as this should not happen either
            logger.error(
                "closing current transaction with status {}",
                TransactionStatus(transaction_status).name,
            )
            # Due to connection being terminated or made read-only from server side
            # TODO: if we keep getting read-only connections after a DB failover, we
            # may need to change that to connection.disconnect()
            connection.close()
            logger.trace("opening new connection")
            thread_local.connections[connection_name] = connection = open_connection(
                db_name, isolation_level, postgresql_readonly
            )
        else:
            logger.debug("using existing connection")

    # should never happen, but is a try to handle connections to DB in recovery mode
    if conn_readonly := _wrong_readonly(connection, postgresql_readonly):
        logger.warning(
            "Got connection with read-only {} where {} was requested",
            conn_readonly,
            postgresql_readonly,
        )
        connection.close()
        logger.trace("opening new connection")
        thread_local.connections[connection_name] = connection = open_connection(
            db_name, isolation_level, postgresql_readonly
        )

        if conn_readonly := _wrong_readonly(connection, postgresql_readonly):
            connection.invalidate()
            raise Exception(  # noqa: TRY002
                "Permanently getting read-only {} connections while {} is required.",
                conn_readonly,
                postgresql_readonly,
            )
    logger.opt(lazy=True).trace(
        "{conn}", conn=lambda: ConnectionInfo.from_connection(connection)
    )
    return connection


def get_connection() -> Connection:
    """Get a read-only connection, default for tasks."""
    return get_or_create_connection(
        isolation_level="AUTOCOMMIT", postgresql_readonly=True
    )


def close_all_connections() -> None:
    """Close all registered connections, and clear engine pools.

    Useful if you want to delete a database. The operation usually fails if
    there are open connections to it.
    """
    if not hasattr(thread_local, "connections"):
        return
    for connection in thread_local.connections.values():
        if not connection.closed:
            logger.info("closing connection to {}", connection.engine.url.database)
            connection.close()
    for engine in engines.values():
        logger.info("closing connection pool to {}", engine.url.database)
        engine.dispose()  # close all connections from the engine pool


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def manage_connection(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Close connection after execution."""

    @wraps(func)
    def wrapped(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        """Begin transaction before execution, closes after."""
        # Disable the decorator, especially the rollback on returning connection
        is_managed = config.get("database.commit_managed_transactions", True)
        with get_or_create_connection() if is_managed else nullcontext():
            result = func(*args, **kwargs)
            return result

    return wrapped


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def manage_read_connection(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Close and rollback connection after execution of the wrapped function.

    This should not change anything, but we found connections with open transactions
    in read-only mode.
    """

    @wraps(func)
    def wrapped(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        """Begin transaction before execution, closes after."""
        # Disable the decorator, especially the rollback on returning connection
        is_managed = config.get("database.commit_managed_transactions", True)
        with get_connection() if is_managed else nullcontext():
            result = func(*args, **kwargs)
            return result

    return wrapped


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def manage_transaction(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Commit transaction on main connection after execution of the wrapped function.

     The standard behavior above can be altered as follow:

     # Rollback

    During manual development, you might want to execute the sql statements but not
    persist the changes; if you run the same code repeatedly, you
    might want the transactions to be rolled back at the end of the function.
    This is done by setting the following config entry:

     ```
     [dev]
     rollback_transactions = true
     ```

     ## Skip commit

     During tests, we don't want the managed transaction to be commited, but the
     rollback is taken care by the test fixtures. In this case we don't want
     neither commit or rollback. This is done by setting the following config entry:

     ```
     [database]
     commit_managed_transactions = false
     ```
    """

    @wraps(func)
    def wrapped(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        commit = config.get("database.commit_managed_transactions", True)
        rollback = config.get("dev.rollback_managed_transactions", False)
        try:
            result = func(*args, **kwargs)
        except:
            get_or_create_connection().rollback()
            raise
        else:
            match (commit, rollback):
                case (_, True):
                    logger.warning("rolling back transaction")
                    get_or_create_connection().rollback()
                case (False, _):
                    logger.debug("commit disabled")
                case _:
                    logger.info("committing transaction")
                    get_or_create_connection().commit()
            return result

    return wrapped


# ----------------------- SQLALCHEMY TABLES ---------------------------


class SingletonMetaData(SingletonMixin, MetaData):
    """Read-write thread-safe singleton MetaData class."""

    def _reflect_metadata(self) -> None:
        logger.debug("reflecting metadata from database")
        # use a different connection to not mess with the current one
        with no_echo(), open_connection() as connection:
            self.reflect(connection)
            logger.debug("metadata reflected {} tables from database", len(self.tables))

    _post_init_func = _reflect_metadata
    _init_check_attribute = "tables"


def _delete_metadata() -> None:
    """Delete the global metadata object.

    Use when the database has been modified during program execution, like after
    a test database setup, to make sure the metadata will be updated next time
    it is accessed.
    """
    logger.debug("clearing reflected metadata")
    metadata = SingletonMetaData()
    metadata.clear()


def get_metadata() -> MetaData:
    """Create or access the SingletonMetaData class instance."""
    return SingletonMetaData()


def get_table(name: str) -> Table:
    """Access the sqlalchemy Table object of the given name."""
    try:
        return get_metadata().tables[name]
    except KeyError as exc:
        raise RuntimeError(f'table "{name}" not found in the reflected schema') from exc


def get_table_prefix_resilient(name: str) -> Table:
    """Access the sqlalchemy Table object of the given name without prefix.

    Also works without the `data_` prefix.
    """
    try:
        return get_metadata().tables[name]
    except KeyError as exc:
        if not re.fullmatch(r"data_.*", name):
            name_with_prefix = f"data_{name}"
            try:
                return get_metadata().tables[name_with_prefix]
            except KeyError as exc2:
                raise RuntimeError(
                    f'neither tables "{name}" or "{name_with_prefix}" '
                    "were found in the reflected schema"
                ) from exc2

        else:
            raise RuntimeError(
                f'table "{name}" not found in the reflected schema'
            ) from exc


def ensure_table_object(str_or_table: str | Table) -> Table:
    """Return itself if already a table object or gets it from the name by string."""
    match str_or_table:
        case str():
            return get_table(str_or_table)
        case Table():
            return str_or_table
        case _:
            raise ValueError(
                f"variable must be either str or Table, instead is {type(str_or_table)}"
            )
