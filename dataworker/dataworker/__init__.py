"""The Dataworker package.

Offers top level imports for config and logger.
"""

import importlib.resources
import logging as standard_library_logging
import pathlib

# convenience imports to the top level
from .config import config
from .logging import logger

# reduce unwanted asyncio logs
standard_library_logging.getLogger("asyncio").setLevel(standard_library_logging.WARNING)

assets = importlib.resources.files("dataworker.assets")

ROOT_DIR = pathlib.Path(__file__).parents[1]

__all__ = ["config", "logger"]
