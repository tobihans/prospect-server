"""Utils to send tasks to the faktory orchestrator."""

import importlib
import pkgutil
from collections.abc import Callable, Generator, Iterable
from functools import cache

import faktory

from dataworker import config, logger


@cache
def get_jobqueue_url() -> str:
    """Build jobqueue url from config.

    Don't build at module load time to avoid exceptions when no jobqueue is configured.
    """
    return f"tcp://:{config.job_queue.password}@{config.job_queue.host}:{config.job_queue.port}"


def make_worker() -> faktory.Worker:
    """Return a faktory Worker based on the config settings.

    Setting disconnect_wait to 9, to be under the default timeout of 10 seconds for
    Linux containers (https://docs.docker.com/reference/cli/docker/container/stop/#time).
    """
    return faktory.Worker(
        faktory=get_jobqueue_url(),
        queues=[config.worker.queue],
        concurrency=config.worker.concurency,
        disconnect_wait=9,
    )


def collect_tasks_config(path: str) -> Generator[tuple[str, str], None, None]:
    """Crawl the task directory for task name and entrypoints.

    # module with a single task

    The task name is the name of the module. It can be overridden with a
    variable called TASK_NAME at the root of the module. The entrypoint is by
    default a function called `run` at the root of the module. It can be
    overridden with a variable called ENTRYPOINT at the root of the module; the
    entrypoint can be a simple function name, or a dotted import path to the function
    inside the module, for example `processing.update_locations` for a function
    located at `tasks/update_grid_locations/processing.py:update_locations`.

    # module with multiple tasks

    To define multiple tasks in a module, a list of dictionaries called
    `SUBTASKS` placed at the root of the module should be defined, with each
    dictionary having entries `TASK_NAME` and `ENTRYPOINT` with the same meaning
    as for a single task.
    """
    logger.info("searching for tasks modules")
    for module_info in pkgutil.iter_modules(
        [path], prefix=path.replace("/", ".") + "."
    ):
        try:
            logger.debug("loading task module: {}", module_info.name)
            module = importlib.import_module(module_info.name)
            if hasattr(module, "SUBTASKS"):
                # task module defines multiple tasks
                logger.debug("loading subtasks")
                for subtask in module.SUBTASKS:
                    task_name = subtask["TASK_NAME"]
                    entrypoint = subtask["ENTRYPOINT"]
                    yield task_name, f"{module_info.name}.{entrypoint}"
            else:
                task_name = getattr(
                    module, "TASK_NAME", module_info.name.rsplit(".", 1)[1]
                )
                entrypoint = getattr(module, "ENTRYPOINT", "run")
                yield task_name, f"{module_info.name}.{entrypoint}"
        except Exception:
            logger.exception(
                "error while collecting config of task module {}", module_info.name
            )


def collect_tasks(path: str) -> Generator[tuple[str, Callable], None, None]:
    """Crawl the task directory for task name and task function.

    See `collect_task_config` for task declaration logic.
    """
    for task_name, entrypoint in collect_tasks_config(path):
        module_path, function_name = entrypoint.rsplit(".", 1)
        module = importlib.import_module(module_path)
        function = getattr(module, function_name)
        yield task_name, function


def enqueue_job(task: str, queue: str, args: Iterable | None = None) -> None:
    """Sends a generic task to the orchestrator.

    task: task name, as registered in the worker
    queue: queue name the worker is listening to
    args: Iterable (tuple or list) of args. in the right order
    """
    if args is None:
        args = ()
    with faktory.connection(faktory=get_jobqueue_url()) as client:
        logger.info('sending task "{}" to {} queue', task, queue)
        logger.debug("arguments: {}", args)
        client.queue(task, queue=queue, args=args, retry=-1)


def send_ingestion_task(
    task: str,
    import_id: str,
    source_id: str,
    file_key: str,
    file_format: str,
    data_origin: str,
    organization_id: str,
) -> None:
    """Sends an ingestion task. Not used in production."""
    args = (
        import_id,
        source_id,
        file_key,
        file_format,
        data_origin,
        organization_id,
    )
    enqueue_job(task=task, queue=config.worker.queue, args=args)


def send_internal_task(
    task: str,
    *args: str,
) -> None:
    """Sends an internal task. Not used in production."""
    enqueue_job(task=task, queue=config.worker.queue, args=args)
