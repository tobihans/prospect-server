"""Utils for object storage.

This module is configured with the following environment variables:

- MINIO_URL
    server url
- MINIO_DATALAKE_READER_USERNAME
- MINIO_DATALAKE_READER_PASSWORD
    credentials of user with read access to the datalake
- MINIO_DATALAKE_BUCKET
"""

import json
import pathlib
from dataclasses import dataclass
from typing import Self

from minio import Minio

from dataworker.config import config
from dataworker.logging import log_process, logger


def get_minio_client(write: bool = False) -> Minio:
    """Create minio datalake client from values in env vars."""
    try:
        host = config.storage.host
        port = config.storage.port
        if write:
            user = config.storage.write_user
            password = config.storage.write_password
        else:
            user = config.storage.user
            password = config.storage.password
    except KeyError:
        logger.exception("missing storage configuration")
        raise
    server_url = f"{host}:{port}"
    logger.debug(
        'creating minio client with user "{user}" to server "{server}"',
        user=user,
        server=server_url,
    )
    return Minio(
        server_url,
        access_key=user,
        secret_key=password,
        secure=config.storage.get("use_secure", False),  # secure means https
    )


@dataclass
class DataFile:
    """Represents a file in the datalake.

    Used as argument for IngestionTasks
    """

    # identify the file in the data lake
    file_key: str
    # JSON or CSV
    file_format: str

    @staticmethod
    def guess_format(path: str) -> str:
        """Static method to guess the format from the path suffix."""
        match pathlib.Path(path).suffix:
            case ".json":
                return "application/json"
            case ".log":
                return "text/plain"
            case _:
                return "text/plain"

    @classmethod
    def from_path(cls, path: str) -> Self:
        """Create a DataFile instance from path."""
        return cls(file_key=path, file_format=cls.guess_format(path))


def load_object_from_storage(file_key: str) -> bytes:
    """Load Object from MinIO storage by name."""
    client: Minio = get_minio_client()
    try:
        response = None
        response = client.get_object(config.get("storage.bucket"), file_key)
        data = response.data
        logger.debug("file read successfully. {length} bytes", length=len(data))
    except Exception as e:
        logger.exception("error while fetching file from storage", error=e)
        raise
    finally:
        if response:
            response.close()  # release network resource
            response.release_conn()  # allow connection reuse
    return data


def decode_object(binary_content: bytes, format_: str) -> dict | str:
    """Decode bytes to dict or string depending on the format."""
    match format_.lower():
        case "application/json":
            logger.debug("parsing as JSON")
            return json.loads(binary_content)
        case "text/plain":
            logger.debug("decoding as plain text")
            return binary_content.decode()
        case _:
            logger.error("file type not recognized: {}", format_)
            raise NotImplementedError


@log_process
def read_datafile_from_storage(datafile: DataFile) -> dict:
    """Reads and parse a file from the datalake."""
    logger.info(
        'fetching {fileformat} data from datalake file "{file_key}"',
        fileformat=datafile.file_format,
        file_key=datafile.file_key,
    )
    object_ = load_object_from_storage(datafile.file_key)
    data = decode_object(object_, datafile.file_format)
    return data


def read_datafile_from_disk(datafile: DataFile) -> dict:
    """Reads and parse a file from the file system.

    For development usage. Substitute datalake loader with config value
    `config.dev.load_from_filesystem`.
    """
    logger.info(
        'fetching {fileformat} data from local filesystem "{file_key}"',
        fileformat=datafile.file_format,
        file_key=datafile.file_key,
    )
    object_ = pathlib.Path(datafile.file_key).read_bytes()
    data = decode_object(object_, datafile.file_format)
    return data


if config.get("dev.load_from_filesystem", False):
    # use for development
    read_datafile = read_datafile_from_disk
else:
    # normal production config
    read_datafile = read_datafile_from_storage
