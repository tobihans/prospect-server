"""Module providing logging functionalities."""

import datetime
import functools
import inspect
import json
import logging as std_logging
import pprint
import sys
import textwrap
import time
import traceback
import uuid
from collections.abc import Callable
from functools import wraps
from multiprocessing import Queue
from pathlib import Path
from typing import ParamSpec, TypeVar

import codenamize
import humanize
import loguru
from loguru import _Logger, logger

from dataworker.config import config

DEFAULT_LOGGING_LEVEL = "INFO"
LOG_FILE = "worker.jsonl"
SENTRY_FORMAT: str = (
    "{time:YYYY-MM-DD HH:mm:ss.SSS} | {name}:{function}:{line} - {process} - {message}"
)

# --------------------------------------------------

# all worker processes log events into this object
ingestion_logs = Queue()


# ----------------------- logging decorators ---------------------------

Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def timer(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Log execution time of a function."""

    @functools.wraps(func)
    def timedFunction(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        start = time.time()
        out = func(*args, **kwargs)
        duration = datetime.timedelta(seconds=(time.time() - start))
        human_duration = humanize.precisedelta(duration, minimum_unit="microseconds")
        logger.info(f"{func.__name__!r} execution time: {human_duration}")
        return out

    return timedFunction


Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def log_process(func: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Log process start, success, error and timing."""

    @wraps(func)
    def process(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        func_name = repr(func.__name__)
        logger.debug(f"----> {func_name}", tag="process")
        try:
            start = time.time()
            res = func(*args, **kwargs)
            duration = humanize.precisedelta(
                datetime.timedelta(seconds=time.time() - start),
                minimum_unit="microseconds",
            )
        except Exception:
            logger.exception(f"<---- [exit with exception] {func_name}", tag="process")
            raise
        else:
            logger.debug(f"<---- {func_name}", tag="process", duration=duration)
            return res

    return process


# --------------------------------------------------


def redirect_std_logging_to(logger: _Logger) -> None:
    """Redirect python std logging to loguru logger."""

    class InterceptHandler(std_logging.Handler):
        def emit(self, record: std_logging.LogRecord) -> None:
            # Get corresponding Loguru level if it exists.
            level: str | int
            try:
                level = logger.level(record.levelname).name
            except ValueError:
                level = record.levelno

            # Find caller from where originated the logged message.
            frame, depth = inspect.currentframe(), 0
            while frame and (
                depth == 0 or frame.f_code.co_filename == std_logging.__file__
            ):
                frame = frame.f_back
                depth += 1

            logger.opt(depth=depth, exception=record.exc_info).log(
                level, record.getMessage()
            )

    std_logging.basicConfig(handlers=[InterceptHandler()], level=0, force=True)


# --------------------- formatting -----------------------------


def serialize_record(record: dict) -> str:
    """Custom log record serializer."""
    log = record["extra"] | {
        "message": record["message"],
        "level": record["level"].name,
        "time": str(record["time"]),
        "elapsed": str(record["elapsed"]),
        "location": f"{record['name']}:{record['function']}:{record['line']}",
        "process": record["process"].name,
    }
    if exception := record.get("exception"):
        log["exception"] = repr(exception.value)
        log["traceback"] = "".join(traceback.format_tb(exception.traceback))
    return json.dumps(log, default=str)


def serialized_format(record: dict) -> str:
    """A custom formatter for serialized logging."""
    record["serialized"] = serialize_record(record)
    return "{serialized}\n"


def format_exception_record(exception_record: Exception) -> str:
    """Custom exception record serializer.

    Formatted traceback followed by exception type and message.

    """
    return "\n".join((
        "".join(traceback.format_tb(exception_record.traceback)),
        f"{exception_record.type.__name__} : {exception_record.value}",
    ))


def human_readable_format(record: dict) -> str:
    """A custom formatter for human readable logs.

    Thread info, colored by level.

    If config.logging.inline_context is True, print eventual context dict.
    If config.logging.inline_exceptions is True, print eventual exceptions
    """
    # building main line
    main_line_elements = ["<level>{message}</level>"]
    if config.get("logging.process", True):
        main_line_elements.insert(-1, "{process.name}")
    if config.get("logging.location", True):
        main_line_elements.insert(-1, "{name}:{function}:{line}")
    main_line_format = " ".join(main_line_elements)

    # extra main line
    log_lines = [main_line_format]
    if config.get("logging.inline_context", False) and (extra := record.get("extra")):
        record["extra_formatted"] = textwrap.indent(pprint.pformat(extra), prefix="\t")
        log_lines.append("{extra_formatted}")
    if config.get("logging.inline_exceptions", False) and (
        exception := record.get("exception")
    ):
        record["exception_formatted"] = format_exception_record(exception)
        log_lines.append("{exception_formatted}")
    return "\n".join(log_lines) + "\n"


# ---------------------- sinks configuration ----------------------------


def add_readable_stderr_sink() -> int:
    """Setup a free text logging sink to stderr. Use in local development."""
    level = config.get("logging.level", default=DEFAULT_LOGGING_LEVEL).upper()
    return logger.add(
        sys.stderr,
        format=human_readable_format,
        level=level,
        filter=config.get("logging.filter", default=None),
    )


def add_serialized_stderr_sink() -> int:
    """Setup serialized logging sink to stderr.

    Use in production
    """
    level = config.get("logging.level", default=DEFAULT_LOGGING_LEVEL).upper()
    return logger.add(
        sys.stderr,
        format=serialized_format,
        level=level,
    )


def add_serialized_file_sink() -> int:
    """Setup serialized logging sink to file.

    Use locally to analyse large amount of logs
    """
    level = config.get("logging.level", default=DEFAULT_LOGGING_LEVEL).upper()
    log_file_path = Path(LOG_FILE)
    log_file_path = log_file_path.with_stem(log_file_path.stem + "_{time}")
    return logger.add(
        log_file_path,
        retention=3,
        format=serialized_format,
        level=level,
    )


def add_tracking_sink() -> int:
    """Send event to a queue for tracking module to work with."""

    def send_log_to_tracking(message: loguru._handler.Message) -> None:
        ingestion_logs.put(message.record)

    def select_ingestion_logs(record: dict) -> bool:
        """Only forward messages from transformation tasks.

        (In theory). With unified tasks, it's not anymore the case.
        """
        return (record["process"].name not in ("MainProcess", "tracking")) and (
            "import_id" in record["extra"]
        )

    return logger.add(
        send_log_to_tracking,
        filter=select_ingestion_logs,
        enqueue=True,
    )


# --------------------------------------------------

Param = ParamSpec("Param")
RetType = TypeVar("RetType")


def run_context(task: Callable[Param, RetType]) -> Callable[Param, RetType]:
    """Add run ids to log context."""

    @wraps(task)
    def wrapped_task(*args: Param.args, **kwargs: Param.kwargs) -> RetType:
        # generate an id for the current job
        run_id = str(uuid.uuid4())
        run_name = codenamize.codenamize(run_id)  # human readable version of run_id
        # the contextualize manager stores the variables in a thread local
        # variable so that every job will have a different id
        logger.info(f"starting run {codenamize.codenamize(run_id)}")
        with logger.contextualize(run_id=run_id, run_name=run_name):
            return task(*args, **kwargs)

    return wrapped_task


# ----------------------- automatic logging configuration ---------------------------


logger.remove()  # remove default sink

redirect_std_logging_to(logger)  # integrate default logging

if not config.get("dev.disable_tracking", default=False):
    add_tracking_sink()  # redirect events for job tracking

# what get logged to stderr
if config.get("logging.log_to_stderr", default=True):
    if config.get("logging.serialize", default=True):
        stderr_sink = add_serialized_stderr_sink()
    else:
        stderr_sink = add_readable_stderr_sink()

# ------------------------- utils to undo logging setup -------------------------


def remove_stderr_sink() -> None:
    """Remove default stderr sink."""
    logger.remove(stderr_sink)
