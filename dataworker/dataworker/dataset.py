"""Defines data container classes."""

import datetime
import functools
import hashlib
import json
import operator
from collections.abc import Callable, Iterable
from typing import Any, Self

import funcy
from pydantic.dataclasses import dataclass

from dataworker.types import DictDataset, Record, Value
from dataworker.utils import unpack


def count_values(records: list[Record]) -> int:
    """Count non-null values in a list of records."""
    return sum(
        funcy.map(
            funcy.notnone,
            funcy.cat(funcy.map(lambda d: d.values(), records)),
        )
    )


def stabilize(var: Value) -> Value:
    """Make objects stable for comparison.

    Stabilize is recursive through mapping and iterables
    Transformation on an item are the following:
    - timestamps: cast to string
    - dicts: sort by key
    """
    match var:
        case datetime.datetime():
            return str(var)
        case dict():
            return sorted(
                ((key, stabilize(value)) for key, value in var.items()),
                key=operator.itemgetter(0),
            )
        case list():
            return [stabilize(value) for value in var]
        case _:
            return var


def hash_data_object(data_object: dict) -> str:
    """Return a hash string of a dictionary.

    Use for non-regression test of complex mappings.
    """
    return hashlib.sha256(json.dumps(stabilize(data_object)).encode()).hexdigest()


def key_set(records: list[dict]) -> set[str]:
    """Set of all keys that appears in one of the dict in the list.

    >>> key_set([
        {"this": 2, "that": 3},
        {"this": 4, "them": 5},
        {"those": 6, "that": 7}
        ])
    {'this', 'them', 'those', 'that'}
    """
    return functools.reduce(
        lambda key_set, obj: key_set | set(obj.keys()), records, set()
    )


def map_fields(func: Callable, keys: list[str], record: Record) -> Record:
    """Apply a function to values of a record, restricted to the given list of keys."""
    return funcy.walk(
        funcy.iffy(lambda k_v: k_v[0] in keys, lambda k_v: (k_v[0], func(k_v[1]))),
        record,
    )


# ------------------------- Data Containers -------------------------


@dataclass
class RecordSet:
    """Dataclass to store a table and corresponding records."""

    table: str
    records: list[Record]
    is_update: bool = False

    def __repr__(self) -> str:
        """RecordSet(table_name <#record>)."""
        return f"{self.__class__.__name__}({self.table!r} <{len(self)} records>)"

    def __len__(self) -> int:
        """Number of records in set."""
        return len(self.records)

    def as_dict(self) -> DictDataset:
        """Return equivalent DictDataset."""
        return {self.table: self.records}

    def value_count(self) -> int:
        """Count non-null values in the whole set.

        Use for partially testing RecordSet content.
        """
        return count_values(self.records)

    def hash(self) -> str:
        """Compute a hash that differs at any meaningful data variation.

        The hash is stable for:
        - change of order of the record list
        - change of order in nested dict
        - use of different timestamp objects

        Use for partially testing RecordSet content.

        """
        return hash_data_object(self.as_dict())

    def key_set(self) -> list[str]:
        """Return the set of keys of all records combined.

        Use for partially testing RecordSet content.
        """
        return key_set(self.records)

    # --- operations

    def apply_to_records(self, function: Callable) -> Self:
        """Apply a function to records as a whole."""
        return RecordSet(
            table=self.table, records=function(self.records), is_update=self.is_update
        )

    def map_records(self, function: Callable) -> Self:
        """Apply a function to every record."""
        return RecordSet(
            table=self.table,
            records=funcy.lmap(function, self.records),
            is_update=self.is_update,
        )

    def filter_records(self, predicate: Any) -> Self:  # noqa: ANN401
        """Remove records that don't pass the predicate value."""
        return RecordSet(
            table=self.table,
            records=funcy.lfilter(predicate, self.records),
            is_update=self.is_update,
        )

    def map_values(self, function: Callable) -> Self:
        """Apply a function to every value of every record."""
        return self.map_records(funcy.partial(funcy.walk_values, function))

    def filter_values(self, function: Callable) -> Self:
        """Remove every entry of every record whose value don't pass the predicate."""
        return self.map_records(funcy.partial(funcy.select_values, function))

    def filter_keys(self, function: Callable) -> Self:
        """Remove every entry of every record whose key don't pass the predicate."""
        return self.map_records(funcy.partial(funcy.select_keys, function))

    def omit_keys(self, keys: Iterable[str]) -> Self:
        """Remove every field that is in the key list."""
        return self.map_records(funcy.partial(funcy.omit, keys=keys))

    def omit_key_pattern(self, pattern: str) -> Self:
        """Remove every field whose key match the regex pattern exactly."""
        return self.filter_keys(funcy.complement(f"^{pattern}$"))

    def add(self, mapping: dict) -> Self:
        """Extend records with a given mapping."""
        return self.map_records(funcy.partial(funcy.merge, mapping))

    def select(self, keys: Iterable[str]) -> Self:
        """Restrict records to certain fields."""
        return self.map_records(funcy.partial(funcy.project, keys=keys))

    def sort(self, key: str) -> Self:
        """Sort records by a key."""
        return self.apply_to_records(
            lambda records: sorted(records, key=operator.itemgetter(key))
        )

    def apply(self, name: str, argument: Any) -> Self:  # noqa: ANN401
        """Generic operation execution."""
        match (name, argument):
            case "map_records", function:
                return self.map_records(function)
            case "filter_records", predicate:
                return self.filter_records(predicate)
            case "map_values", function:
                return self.map_values(function)
            case "filter_values", predicate:
                return self.filter_values(predicate)
            case "filter_keys", predicate:
                return self.filter_keys(predicate)
            case "omit_keys", keys:
                return self.omit_keys(keys)
            case "omit_key_pattern", keys:
                return self.omit_key_pattern(keys)
            case "add", keys:
                return self.add(keys)
            case _:
                raise ValueError(f"operation not recognized: {name}")

    def apply_multiple(self, *operations: tuple[str, Any]) -> Self:
        """Generic multiple operation execution."""
        return functools.reduce(
            lambda record_set, operation: record_set.apply(*operation), operations, self
        )


# ------------------------- Dataset -------------------------


@dataclass
class Dataset:
    """A collection of RecordSet."""

    recordsets: list[RecordSet]

    def __repr__(self) -> str:
        """Dataset(table_name:<# records>, ...)."""
        recordsets_string = ",".join([
            f"{recordset.table!r} <{len(recordset)} records>"
            for recordset in self.recordsets
        ])
        return f"{self.__class__.__name__}({recordsets_string})"

    @classmethod
    def from_dict(cls, dict_: DictDataset) -> Self:
        """Create dataset instance from task core function output."""
        if dict_ is None:
            return Dataset([])
        flags, recordsets = funcy.lmap(
            dict, funcy.split(unpack(lambda k, v: k.startswith("_")), dict_.items())
        )

        def is_update_flagged(flags: dict, table: str, records: list) -> bool:
            """Determine if a given table should be marked as an update."""
            return table in flags.get("_update", []) or all(
                "uid" in record for record in records
            )

        return cls(
            recordsets=[
                RecordSet(
                    table=table,
                    records=records,
                    is_update=is_update_flagged(flags, table, records),
                )
                for table, records in recordsets.items()
            ]
        )

    def as_dict(self) -> DictDataset:
        """Return equivalent dict dataset."""
        if len(self.recordsets) == 0:
            return {}
        else:
            return funcy.join(recordset.as_dict() for recordset in self.recordsets)

    @property
    def has_records(self) -> bool:
        """If dataset contains any Record at all."""
        return len(self.recordsets) > 0 and any(
            len(recordset) > 0 for recordset in self.recordsets
        )
