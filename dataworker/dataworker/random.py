"""Functions for randomizing data."""

import random
from collections.abc import Iterator
from contextlib import contextmanager

import funcy

from dataworker.dataset import map_fields
from dataworker.types import Record


@contextmanager
def set_seed(seed: int | float | str | bytes | bytearray | None) -> Iterator[None]:
    """Temporary set the random seed."""
    state = random.getstate()
    random.seed(seed)
    try:
        yield
    finally:
        random.setstate(state)


def jiggle(ratio: float, value: float) -> float:
    """Add a random normal noise to a value.

    ratio: ratio of the value used as variance for noise
    """
    return random.normalvariate(mu=value, sigma=ratio * value)


def jiggle_values(keys: list[str], ratio: float, record: Record) -> Record:
    """Add a random normal noise to certain values of a record.

    ratio: ratio of the value used as variance for noise
    """
    return map_fields(funcy.curry(jiggle)(ratio), keys, record)


def jiggle_integers(keys: list[str], ratio: float, record: Record) -> Record:
    """Add a random normal noise to certain integers of a record.

    ratio: ratio of the value used as variance for noise
    """
    return map_fields(funcy.rcompose(funcy.curry(jiggle)(ratio), int), keys, record)
