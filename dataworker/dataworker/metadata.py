"""Module containing all functionalities around metadata, like uid."""

import functools
from collections.abc import Iterable
from dataclasses import dataclass
from datetime import UTC, datetime

import funcy
import pandas
from funcy import constantly, isnone

from dataworker import dataset, logger
from dataworker.database import get_table
from dataworker.logging import log_process
from dataworker.time import stringify_date
from dataworker.types import RecordSetType

# ------------------------- utils -------------------------
UID_KEYS_MAPPING = {
    "device_uid": ["organization_id", "manufacturer", "serial_number"],
    "customer_uid": ["organization_id", "data_origin", "customer_external_id"],
    "account_uid": ["organization_id", "data_origin", "account_external_id"],
    "grid_uid": ["organization_id", "grid_name"],
}


def timestamps(creation: bool = True) -> dict[str, datetime]:
    """Generic timestamp elements for sample completion."""
    now = datetime.now(UTC)
    stamps = {"updated_at": now}
    if creation:
        stamps["created_at"] = now
    return stamps


def update_timestamp() -> dict[str, datetime]:
    """Return updated_at: now as dict."""
    return {"updated_at": datetime.now(UTC)}


def creation_timestamps() -> dict[str, datetime]:
    """Return created_at & updated_at: now as dict."""
    now = datetime.now(UTC)
    return {"updated_at": now, "created_at": now}


def uid_timestamp_element_format(timestamp: datetime) -> str:
    """Format datetime for building uids.

    In some UIDs, a string version of timestamps with an odd format are
    required.

    The formatted date should be in utc timezone and was aimed as follow:

        "2022-07-30 14:45:32.876Z"

    However, since uid generation is also non-constant on the core side and
    the biggest amount of timestamps were not formatted as they were not passed
    as datetime but as strings, we'll deliver isoformat.
    """
    return timestamp.astimezone(UTC).isoformat(sep="T")


def _build_uid(elements: Iterable[int | str | datetime]) -> str | None:
    """Generic recipe to build composite UIDs.

    In the Prospect datawarehouse, UIDs are dash-separated concatenation of
    other ids. Datetimes are converted to a precise format. If one of the
    elements is None, the whole UID is also None.
    """
    if any(map(isnone, elements)):
        return None
    else:
        return "_".join(
            (uid_timestamp_element_format(e) if isinstance(e, datetime) else str(e))
            for e in elements
        )


def build_uids_for_record(
    record: dataset.Record,
    secondary_keys: list[str] | None = None,
    primary_keys: list[str] | None = None,
) -> dataset.Record:
    """Build uids for a record and the uid key lists passed.

    The primary_keys is a list of key that will compose the "uid". It can include
    a secondary_key.
    The secondary_keys is a list of keys that are secondary uids, defined in the
    UID_KEYS_MAPPING.
    """
    if secondary_keys:
        uids = {
            key: _build_uid([record.get(k) for k in UID_KEYS_MAPPING[key]])
            for key in secondary_keys
        }
    else:
        uids = {}
    if primary_keys:
        uids["uid"] = _build_uid([
            record.get(key, uids.get(key)) for key in primary_keys
        ])
    return uids


@dataclass
class ImportContext:
    """Holds contextual information for the import of a datafile.

    Used as argument for ingestion tasks.
    """

    # identify the import task in the imports tracking table
    import_id: int
    # no idea what is this ID
    source_id: int
    # no idea what is this ID
    data_origin: str
    # organization who's members use prospect to visualize their data
    organization_id: int


# ---------------------- table context functions----------------------------

# The following functions build the fields that are missing to the samples
# before being inserted into the datawarehouse. Those fields are a combination
# of the arguments given to the worker at the beginning of the process, elements
# from the sample itself, and generated timestamps. After the first extraction of the
# fields from the datafile, the process of completing the sample can be
# generalized to one function per table, regardless of the source of the the data.
#
# Those function have a standardized interface, where arguments are the sample
# itself and the import context, that contains some of the arguments received
# by the worker function.
#
# Some UIDs are optional. To express this case, make it so that accessing the
# missing elements result in None, and the `build_uid()` function will make the
# whole UID None.
#
# Example of mandatory UID:
#   # The following fails if "device_id" is not in sample
#   build_uid([record.get("organization_id"),sample["device_id"]])
# Example of optional UID:
#   # The following is None if "device_id" is not in sample
#   build_uid([record.get("organization_id"),record.get("device_id")])


def custom__uid_builder(record: dict) -> dataset.Record:
    """Return uid dict from record based on organization and external IDs."""
    return build_uids_for_record(
        record=record, primary_keys=["organization_id", "external_id"]
    )


def grids__uid_builder(record: dict) -> dataset.Record:
    """Return grids uid dict from record based on organization ID and name."""
    return build_uids_for_record(
        record=record, primary_keys=["organization_id", "name"]
    )


def grids_ts__uid_builder(record: dict) -> dataset.Record:
    """Return grid_uid dict from record through the generic uid builder."""
    return build_uids_for_record(record=record, secondary_keys=["grid_uid"])


def meters__uid_builder(record: dict) -> dataset.Record:
    """Return meters device_uid dict from record with additional uids."""
    return build_uids_for_record(
        record=record,
        primary_keys=["customer_uid", "device_uid"],
        secondary_keys=["device_uid", "customer_uid", "account_uid", "grid_uid"],
    )


def meters_ts__uid_builder(record: dict) -> dataset.Record:
    """Return device_uid dict from record through the generic uid builder."""
    return build_uids_for_record(record=record, secondary_keys=["device_uid"])


def payments_ts__uid_builder(record: dict) -> dataset.Record:
    """Return payments uid and account_uid dict from record."""
    return build_uids_for_record(
        record=record,
        primary_keys=[
            "account_origin",
            "account_uid",
            "payment_external_id",
            "paid_at",
        ],
        secondary_keys=["account_uid"],
    )


def shs__uid_builder(record: dict) -> dataset.Record:
    """Return shs device_uid dict from record with additional uids."""
    return build_uids_for_record(
        record=record,
        primary_keys=["customer_uid", "device_uid"],
        secondary_keys=["device_uid", "customer_uid", "account_uid"],
    )


def shs_ts__uid_builder(record: dict) -> dataset.Record:
    """Return device_uid dict from record through the generic uid builder."""
    return build_uids_for_record(record=record, secondary_keys=["device_uid"])


def trust_trace__uid_builder(record: dict) -> dataset.Record:
    """Return uid dict for trust trace from record."""
    return build_uids_for_record(
        record=record, primary_keys=["subject_origin", "subject_uid", "check"]
    )


def paygo_accounts_snapshots_ts__uid_builder(record: dict) -> dataset.Record:
    """Return account_uid dict from record through the generic uid builder."""
    return build_uids_for_record(record=record, secondary_keys=["account_uid"])


# --------------------------------------------------


def add_record_identifiers(record: dataset.Record, table: str) -> dataset.Record:
    """Adds secondary identifiers to a record."""
    match table:
        case "data_custom":
            table_id_builder = custom__uid_builder
        case "data_grids":
            table_id_builder = grids__uid_builder
        case "data_grids_ts":
            table_id_builder = grids_ts__uid_builder
        case "data_meters":
            table_id_builder = meters__uid_builder
        case "data_meters_ts":
            table_id_builder = meters_ts__uid_builder
        case "data_meter_events_ts":
            table_id_builder = meters_ts__uid_builder
        case "data_payments_ts":
            table_id_builder = payments_ts__uid_builder
        case "data_shs":
            table_id_builder = shs__uid_builder
        case "data_shs_ts":
            table_id_builder = shs_ts__uid_builder
        case "data_paygo_accounts_snapshots_ts":
            table_id_builder = paygo_accounts_snapshots_ts__uid_builder
        case "data_trust_trace":
            table_id_builder = trust_trace__uid_builder
        case _:
            logger.warning("no uid building function found for table {}", table)
            table_id_builder = constantly({})
    logger.debug("building identifiers with function {}", table_id_builder.__name__)
    return table_id_builder(record) | record


@functools.cache
def _table_fields(table: str) -> list[str]:
    return get_table(table).columns.keys()


def collect_record_extra_fields(record: dataset.Record, table: str) -> dataset.Record:
    """Move columns not specified in table schema to the 'custom' JSON field."""
    expected_fields = funcy.project(record, _table_fields(table))
    # TODO: This can still lead to silently ignoring some data.
    # But this is required as data from the import_context can not be put
    # to tables without custom field, like "products"
    if "custom" not in _table_fields(table):
        return expected_fields
    extra_fields = funcy.omit(record, _table_fields(table))
    if not extra_fields:
        return expected_fields
    # Remove empty values in custom as skipped generally for bulk operations
    extra_fields = funcy.select_values(
        lambda value: pandas.isna(value) is False, extra_fields
    )
    # serialize datetimes because JSON serializer do not handle them
    extra_fields = funcy.walk_values(stringify_date, extra_fields)
    # Remove empty values in custom as skipped generally for bulk operations
    extra_fields = funcy.select_values(
        lambda value: value not in ["", None], extra_fields
    )
    return expected_fields | {"custom": extra_fields}


# TODO: Anoine in this case, used only in tests, and handling recordset, not dataset
@log_process
def collect_dataset_extra_fields(
    dataset: RecordSetType,
) -> RecordSetType:
    """Per RecordSet, move columns not in table schema to the 'custom' JSON field."""
    # TODO: used only in tests for now. Move out?
    return {
        table: [collect_record_extra_fields(sample, table) for sample in samples]
        for table, samples in dataset.items()
    }
