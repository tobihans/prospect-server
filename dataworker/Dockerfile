# debian base image with python
FROM python:3.12-slim
COPY --from=ghcr.io/astral-sh/uv:0.5 /uv /bin/uv

ENV UV_SYSTEM_PYTHON=1 UV_LINK_MODE=copy UV_COMPILE_BYTECODE=1
WORKDIR /dataworker

# install dependencies, without a virtualenv
RUN --mount=type=bind,source=uv.lock,target=uv.lock \
    --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
    --mount=type=bind,source=faktory-1.1.0-py3-none-any.whl,target=faktory-1.1.0-py3-none-any.whl \
    uv sync --frozen --no-install-project --no-dev --no-cache

# copy and install the rest of the project
COPY . /dataworker
RUN uv sync --frozen --no-dev --no-cache

# Place executables in the environment at the front of the path
ENV PATH="/dataworker/.venv/bin:$PATH"

# run the worker
CMD ["python", "worker.py"]
