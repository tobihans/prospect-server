"""Dataworker entrypoint.

- setup logging
- register tasks
- start tracking process
- start worker
"""

import sentry_sdk
from sentry_sdk.integrations.loguru import LoggingLevels, LoguruIntegration
from sentry_sdk.integrations.threading import ThreadingIntegration

from dataworker import config, logger
from dataworker.jobqueue import collect_tasks, make_worker
from dataworker.logging import SENTRY_FORMAT, add_serialized_file_sink
from dataworker.task import IngestionTask, InternalTask
from dataworker.tracking import start_tracking_process

logger.info("-" * 80)  # to make reloads in the terminal more readable


# optionally also log to a file
if config.get("logging.serialized_to_file", default=False):
    add_serialized_file_sink()

# error monitoring
if config.get("monitoring.use_sentry", default=True) and (
    dsn := config.get("monitoring.sentry_dsn", default=None)
):
    logger.debug("setting up sentry")
    sentry_sdk.init(
        dsn=dsn,
        integrations=[
            # still required to avoid duplicates from faktory, even with multiprocessing
            ThreadingIntegration(propagate_scope=True),
            LoguruIntegration(
                breadcrumb_format=SENTRY_FORMAT,
                event_format=SENTRY_FORMAT,
                event_level=LoggingLevels.WARNING.value,
            ),
        ],
    )


worker = make_worker()


# ------------------------- Tasks registration -------------------------
TASK_REGISTRATION_CONFIGS = [
    ("tasks/ingestion", IngestionTask),
    ("tasks/internal", InternalTask),
]

for path, TaskClass in TASK_REGISTRATION_CONFIGS:
    for task_name, function in collect_tasks(path):
        import_path = f"{function.__module__}.{function.__name__}"
        logger.info("registering {} from function {}", TaskClass, import_path)
        task = TaskClass(name=task_name, function=function)
        task.register(worker)


# ------------------------- startup -------------------------


logger.info("jobqueue server set to {}:{}", worker.faktory.host, worker.faktory.port)
logger.info(
    "pulling jobs from queue {}", ",".join(repr(q) for q in worker.get_queues())
)

# independant process that updates the database on the import progress and status
start_tracking_process()

logger.info("starting worker")

worker.run()
