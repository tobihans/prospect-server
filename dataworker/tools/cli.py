"""Dataworker CLI and related methods, powered by click."""

import os
from collections.abc import Callable, Sequence

import click


def exec_shell_command(command: Sequence[str]) -> None:
    """Run a shell commands by replacing the current python process."""
    import os

    print(" ".join(command))
    # the command name is used as first argument but also in the args is the command,
    # the rest fills argv. And argv[0] is the command itself.
    # A bit strange but the doc explicitely asks for it.
    os.execlp(command[0], *command)


def docker_compose_base_command() -> list[str]:
    """Return the base command for docker compose commands, as list of strings."""
    from dataworker import ROOT_DIR

    compose_file = ROOT_DIR.parent / "docker-compose.yml"
    return ["docker", "compose", "-f", str(compose_file)]


main = click.Group()

# --------------------------------------------------


config_group = click.Group("config", help="[group] Show configuration")


@config_group.command("local")
@click.option("-s", "section", type=click.STRING, help="print only a given section")
def print_local_config(section: str | None) -> None:
    """Show the configuration for a locally running worker.

    The final configuration is a combination of config files and environment variables.
    """
    import json

    import dynaconf

    from dataworker import config

    if section is not None:
        section_or_value = config.get(section)
        if isinstance(section_or_value, dynaconf.utils.boxing.DynaBox):
            print(json.dumps(section_or_value.to_dict(), indent=4))
        else:
            print(json.dumps(section_or_value, indent=4))
    else:
        print(json.dumps(config.as_dict(), indent=4))


@config_group.command("container")
def print_container_config() -> None:
    """Show the configuration for a containerized worker.

    !! Broken
    """
    from dataworker import logger

    logger.error("Broken command. Needs fixing")
    exit()
    # runs the above function manually inside the running service
    exec_shell_command([
        *docker_compose_base_command(),
        "run",
        "dataworker",
        "python",
        "-u",  # unbuffered
        "-c",
        "'import cli;cli.print_local_config()'",
    ])


main.add_command(config_group)

# ------------------------- runners -------------------------

worker = click.Group("worker", help="[group] Build and run the dataworker")


@worker.command
def build() -> None:
    """Build dataworker container."""
    exec_shell_command([*docker_compose_base_command(), "build", "dataworker"])


@worker.command(name="services")
def start_dataworker_service_containers() -> None:
    """Start detached services needed by dataworker."""
    services = ["minio", "postgres", "faktory"]
    exec_shell_command([*docker_compose_base_command(), "up", "-d", *services])


@worker.command(name="local")
@click.option(
    "--reload",
    "-r",
    is_flag=True,
    help="Autoreload with code changes. Requires `fd` search tool installed",
)
def start_local_dataworker(reload: bool = False) -> None:
    """Start a local dataworker process."""
    command = ["uv", "run", "worker.py"]
    if reload:
        reload_command_prefix = [
            "fd",
            "-e",
            "py",
            "-E",
            "tests",
            "-E",
            "tools",
            ".",
            "|",
            "entr",
            "-r",
        ]
        command = reload_command_prefix + command
        print(*command)
        return  # pipe do not seem to work with os.exec
    exec_shell_command(command)


@worker.command(name="container")
@click.option("--build", "-b", is_flag=True, help="force container rebuild")
def start_dataworker_container(build: bool = False) -> None:
    """Start a local dataworker container."""
    command = list(
        filter(  # clean Nones
            None,
            [
                *docker_compose_base_command(),
                "run",
                "--build" if build else "",
                "dataworker",
            ],
        )
    )
    exec_shell_command(command)


main.add_command(worker)

# ------------------------- storage -------------------------

storage = click.Group("storage", help="[group] Datalake operations")


@storage.command
def setup_minio_client() -> None:
    """Setup minio client.

    Emit the `mc` commands to setup the connection to the containerized local storage
    """
    import subprocess  # noqa: S404
    from pathlib import Path

    import dotenv

    from dataworker import config

    subprocess.run(
        [
            "mc",
            "alias",
            "set",
            "local",
            f"http://localhost:{config.storage.port}",
            config.storage.user,
            config.storage.password,
        ],
        check=True,
    )
    main_env_file = dotenv.dotenv_values(Path(__file__).parents[1] / ".env")
    subprocess.run(
        [
            "mc",
            "alias",
            "set",
            "local-admin",
            f"http://localhost:{config.storage.port}",
            main_env_file["MINIO_ROOT_USER"],
            main_env_file["MINIO_ROOT_PASSWORD"],
        ],
        check=True,
    )


@storage.command(name="datalake-gui")
def open_minio_console() -> None:
    """Open datalake GUI."""
    import dotenv

    import dataworker
    from dataworker import config

    main_env_file = dotenv.dotenv_values(dataworker.ROOT_DIR.parent / ".env")
    print("Use the following credentials to login to the UI")
    print(f"user: {main_env_file['MINIO_ROOT_USER']}")
    print(f"password: {main_env_file['MINIO_ROOT_PASSWORD']}")
    exec_shell_command(
        "xdg-open",
        f"http://{config.storage.host}:{config.storage.ui_port}/browser/{config.storage.bucket}",
    )


main.add_command(storage)

# ------------------------- job queue -------------------------

job_queue = click.Group("jobqueue", help="[group] interact with job orchestrator")

IMPORT_ID = "123"
ORGANIZATION_ID = "456"
SOURCE_ID = "789"
DATA_ORIGIN = "solar_coop"
TIME_SINCE_CREATION_S = 30
INGESTION_START_DELAY_S = 10
INGESTION_DURATION_S = 10


def is_import_registered(import_id: str) -> bool:
    """Return True if import_id is registered in the database, else False."""
    from dataworker.database import get_or_create_connection, get_table

    imports = get_table("imports")
    return (
        get_or_create_connection()
        .execute(imports.select().where(imports.c.id == import_id))
        .one_or_none()
    ) is not None


def create_import_record(import_id: str) -> None:
    """Insert import tracking record."""
    import datetime

    from dataworker import logger
    from dataworker.database import get_or_create_connection, get_table

    logger.info("creating import with id {}", import_id)
    creation = datetime.datetime.now(datetime.UTC) - datetime.timedelta(
        seconds=TIME_SINCE_CREATION_S
    )
    ingestion_start = creation + datetime.timedelta(seconds=INGESTION_START_DELAY_S)
    ingestion_end = ingestion_start + datetime.timedelta(seconds=INGESTION_DURATION_S)
    ingestion_protocol = "\n".join([
        "starting ingestion",
        "connection working",
        "ingestion successful",
    ])
    values = {
        "created_at": creation,
        "ingestion_started_at": ingestion_start,
        "ingestion_finished_at": ingestion_end,
        "updated_at": ingestion_end,
        "id": import_id,
        "protocol": ingestion_protocol,
        "source_id": SOURCE_ID,
    }
    imports = get_table("imports")

    get_or_create_connection().execute(imports.insert().values(**values))


def setup_import_tracking(import_id: str | int) -> None:
    """Check if an import record already exists for a given id, else creates it.

    When triggering a job during development, we want this row to exist in the
    import table to test functionalities that interact with this piece of data.
    """
    from dataworker import logger
    from dataworker.database import get_or_create_connection

    with get_or_create_connection() as connection, connection.begin():
        if not is_import_registered(import_id):
            create_import_record(import_id)
        else:
            logger.info("import with id {} already exists", import_id)


@job_queue.command("add")
@click.option("--config_file", type=click.Path(exists=True, readable=True))
@click.option("--task", type=click.STRING)
@click.option("--file", type=click.STRING)
def add_job_to_queue(config_file: str, task: str, file: str) -> None:
    """Add a job in the dataworker queue.

    Give JOBINFO_FILE that contains the task arguments.
    Job arguments are read from a toml file. Use `ETL_job.template.toml` to
    write a new job.
    """
    import pathlib
    import tomllib

    import funcy

    from dataworker import config, logger
    from dataworker.jobqueue import enqueue_job
    from dataworker.storage import DataFile

    # default job configuration
    job_config = {
        "queue": config.worker.queue,
        "import_id": IMPORT_ID,
        "data_origin": DATA_ORIGIN,
        "source_id": SOURCE_ID,
        "organization_id": ORGANIZATION_ID,
    }
    # override with config file
    if config_file is not None:
        with pathlib.Path(config_file).open("rb") as config_file:
            logger.debug("loading job info from file {}", config_file)
            job_config |= funcy.select_values(funcy.notnone, tomllib.load(config_file))
    # override with direct command line options
    job_config |= funcy.select_values(funcy.notnone, {"task": task, "file": file})
    # ---
    logger.debug("job parameters:\n{}", job_config)
    # ---
    if "file" in job_config:
        # ingestion task
        setup_import_tracking(import_id=job_config["import_id"])
        data_file = DataFile.from_path(job_config["file"])
        enqueue_job(
            task=job_config["task"],
            queue=job_config["queue"],
            args=(
                job_config["import_id"],
                job_config["source_id"],
                data_file.file_key,
                data_file.file_format,
                job_config["data_origin"],
                job_config["organization_id"],
            ),
        )
    else:
        # internal task
        enqueue_job(
            task=job_config["task"],
            queue=job_config["queue"],
        )


@job_queue.command(name="gui")
def open_faktory_console() -> None:
    """Open job queue GUI."""
    from dataworker import config

    print(f"use following password to sign in the UI: {config.job_queue.password}")
    exec_shell_command([
        "xdg-open",
        f"http://{config.job_queue.host}:{config.job_queue.ui_port}",
    ])


main.add_command(job_queue)

# ------------------------- database -------------------------

database = click.Group("database", help="[group] Interact with the datawarehouse")


@database.command("shell")
@click.argument("sql_command", nargs=-1)
@click.option("--database", "-d", type=click.STRING)
@click.option("--file", "-f", type=click.Path(readable=True))
def db_shell(database: str, file: str, sql_command: str) -> None:
    """Opens a psql shell.

    Use database connection information from the config
    """
    import subprocess  # noqa: S404

    from dataworker import config

    connection_variables = {
        "PGPASSWORD": config.database.password,
    }
    shell_command = [
        "psql",
        "--host",
        config.database.host,
        "--port",
        str(config.database.port),
        "--username",
        config.database.user,
        "--dbname",
        database or config.database.name,
    ]
    if file:
        shell_command += ["--file", file]
    if sql_command:
        # disable pager to print result directly
        shell_command += ["--pset", "pager=0", "-c", f"'{' '.join(sql_command)}'"]
    shell_command = " ".join(shell_command)
    print(shell_command)
    subprocess.run(shell_command, env=connection_variables, check=True, shell=True)


@database.command("clear")
def empty_all_tables() -> None:
    """Empty all tables, but don't delete them."""
    from dataworker.database import get_metadata, get_or_create_connection

    print("deleting all rows of all tables")
    with get_or_create_connection() as connection, connection.begin():
        for table in get_metadata().sorted_tables:
            print(f"deleting data from {table}")
            connection.execute(table.delete())


@database.command("reset")
def reset_databases() -> None:
    """Delete and recreate test and development database.

    Wrapping around ruby database building command.
    """
    from dataworker import config
    from dataworker.queries import recreate_database, run_sql_from_file

    DB_DUMP_FILE = "tests/structure.sql"
    recreate_database(config.database.name)
    run_sql_from_file(DB_DUMP_FILE, config.database.name, commit=True)


@database.command("dump")
@click.argument("file")
def dump_database(file: str) -> None:
    """Dump database content in a file."""
    from dataworker import config

    env = {
        "PGHOST": config.database.host,
        "PGPORT": str(config.database.port),
        "PGUSER": config.database.user,
        "PGPASSWORD": config.database.password,
        "PYTHONPATH": os.getenv("PYTHONPATH", ""),
        "PATH": os.getenv("PATH", ""),
    }
    print(
        "sending command to database server: {target}",
        target=(
            f"{config.database.user}:***@{config.database.host}:{config.database.port}"
        ),
    )
    command = ["pg_dump", "--format=custom", "--file", file, config.database.name]
    print(f"run command with postgres credentials: {command}")
    os.subprocess.run(command, env=env, check=True, capture_output=True)


@database.command("connection-string")
def print_database_connection_string() -> None:
    """Print the database URL to configure other tools."""
    from dataworker.database import configured_database_url

    print(configured_database_url().render_as_string(hide_password=False))


@database.command("env-var-config")
@click.option("--shell", "-d", default="bash", type=click.Choice(["bash", "fish"]))
def print_postgres_env_var_config(shell: str = "bash") -> None:
    """Configure postgres tools vir env vars.

    Print commands to set environment variables used by postgres commands like `psql`,
    `pg_dump` and `pg_restore`. Also sets up `DATABASE_URL` with a
    complete database access string for tools like `dslr`.

    Usage with a POSIX shell like bash:

    `source <(uv run dataworker database env-var-config)`

    Usage with fish shell:

    `uv run dataworker database env-var-config --shell fish | source`
    """
    from dataworker import config
    from dataworker.database import configured_database_url

    match shell:
        case "bash":
            statement = "export {varname}={value};"
        case "fish":
            statement = "set -x {varname} {value};"

    for varname, key in [
        ("PGHOST", "HOST"),
        ("PGPORT", "PORT"),
        ("PGUSER", "USER"),
        ("PGPASSWORD", "PASSWORD"),
        ("PGDATABASE", "NAME"),
    ]:
        print(statement.format(varname=varname, value=config.database[key]))
    print(
        statement.format(
            varname="DATABASE_URL",
            value=configured_database_url().render_as_string(hide_password=False),
        )
    )


@database.command("summary")
def print_database_summary() -> None:
    """Show row count of main database tables."""
    from rich import print

    from dataworker.report import database_summary

    print(database_summary())


@database.command("table")
@click.argument("table", type=click.STRING)
def print_table_structure(table: str) -> None:
    """Show table structure."""
    import rich
    import rich.text

    from dataworker.report import table_structure

    try:
        rich.print(table_structure(table))
    except RuntimeError as e:
        rich.print(rich.text.Text(str(e), style="red"))


@database.command("list")
def print_tables() -> None:
    """Print datawarehouse tables."""
    import funcy
    from rich import print

    from dataworker.database import get_metadata

    for table in funcy.lkeep("data_.*", get_metadata().tables.keys()):
        print(table)


main.add_command(database)

# ------------------------- dev -------------------------

dev = click.Group("dev", help="[group] Development tools")


def import_function(import_path: str) -> Callable:
    """Helper to import a function through a path."""
    import importlib

    function_path_elements = import_path.split(".")
    module_path = ".".join(function_path_elements[:-1])
    function_name = function_path_elements[-1]
    module = importlib.import_module(module_path)
    function = getattr(module, function_name)
    return function


@dev.command("internal_task")
@click.argument("task_path", type=click.STRING)
@click.argument(
    "file_path", type=click.Path(exists=True, dir_okay=False, readable=True)
)
@click.option("--shape", is_flag=True, help="only show shape of the task result")
def run_internal_task(task_path: str, file_path: str, shape: bool = False) -> None:
    """Run internal task function on a data file.

    Import the internal task function using TASK_PATH (ex: task.aam_v3.transform)
    and apply it to the data found in FILE_PATH (tests/test_data/aam_2648.txt).
    Do not try to insert results.
    """
    from rich import print

    from dataworker.dataset import dataset_summary
    from dataworker.storage import DataFile, read_datafile_from_disk

    function = import_function(task_path)
    data_file = DataFile.from_path(file_path)
    file_content = read_datafile_from_disk(data_file)
    dataset = function(file_content)
    if shape:
        print(dataset_summary(dataset))
    else:
        raise NotImplementedError("Need reimplementation")


@dev.command("import")
@click.argument("import_id", type=click.STRING)
def print_import_info(import_id: str) -> None:
    """Show info about an import job.

    Data comes from the import table and is matched with the IMPORT_ID
    """
    from rich import print

    from dataworker.report import ImportInfo

    import_info = ImportInfo.from_id(import_id)
    if import_info:
        print(import_info.table())


@dev.command
@click.option("--file", "-f")
@click.option("--loki", is_flag=True)
def explore_logs(file: str, loki: bool = False) -> None:
    """Explore dataworker structured logs.

    Load a dataworker json log file (FILE) into a
    `dataworker.report.LogCollection` objects and extract individual
    `dataworker.report.RunLogs` objects. The shell stays open in interactive
    mode for exploration.

    If not specified, the file loaded is the one matching the default pattern
    `worker_<date_string>.log`, if multiple are present, take the latest. This is made
    to work together with the `logging.serialized_to_file` option in the configuration.
    """
    import os

    command = ["ipython", "-i", "-m", "tools.explore_logs"]
    options = []
    if file:
        options += ["--file", file]
    if loki:
        options += ["--loki"]
    if options:
        command += ["--", *options]
    print(" ".join(command))
    os.execvp("ipython", command)


main.add_command(dev)
