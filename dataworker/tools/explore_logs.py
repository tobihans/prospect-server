"""Module to explore logs from file, using arguments.

Usage: python tools/explore_logs.py -f [filepath] [--loki]
"""

import argparse
from pathlib import Path

from dataworker import logger
from dataworker.report import LogCollection, file_size_string

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file")
parser.add_argument("--loki", action="store_true")
args = parser.parse_args()
if args.file:
    log_file = args.file
else:
    # default is to take latest log
    logger.info("choosing latest log file")
    log_file = max(Path().glob("worker_*.jsonl"))
logger.info("reading file: {} ({})", log_file, file_size_string(log_file))
if args.loki:
    logs = LogCollection.from_file(log_file, loki=True)
else:
    logs = LogCollection.from_file(log_file)
logger.info("{} logs", len(logs))
runs = logs.runs()
logger.info("{} runs", len(runs))
