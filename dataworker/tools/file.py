"""Module containing methods around file handling and json parsing."""

import json
from pathlib import Path


def expand_path(path: str | Path) -> Path:
    """Expand path to full form."""
    return Path(path).expanduser().resolve()


def dump_json(thing: dict, path: str, encoder: json.JSONEncoder | None = None) -> None:
    """Dump the thing dict to the given path as JSON."""
    output_path: Path = expand_path(path)
    output_path.parent.mkdir(exist_ok=True, parents=True)
    output_path.write_text(json.dumps(thing, cls=encoder, indent=4))
    print(f"saved json data to {path}")


def load_json(path: str) -> dict:
    """Load JSON from the path and return as a dict."""
    full_path: Path = expand_path(path)
    print(f"loading json data from {full_path}")
    try:
        return json.loads(full_path.read_text())
    except json.decoder.JSONDecodeError as e:
        print(f"error while loading file: {e}")
        raise
