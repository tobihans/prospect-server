# -*- coding: utf-8 -*-
"""
Verify sales made by Supamoto match up with mobile money payment records

Assumptions:
    - input CSVs names will remain constant
    - input column names will remain constant for all workbooks
    
Questions:
    - Can I assume presence in MTN/Airtel datasets is sufficient for verification
        - or do I need to filter on the status field
        - if need to filter, then I need map of status values
    - How much time is it worth productionalizing this
        - better parameterization
        - deduplication (no dups in existing data, so not currently needed)
        - stronger joins (in case more carriers come in and have same id structure)

@author: sweeneyiiid
"""

import pandas as pd
import datetime as dt

# =============================================================================
# parameters
# =============================================================================

in_path = "./"

in_supamoto = "supamoto_crm_rev1.csv"
in_mtn = "mtn_rev1.csv"
in_airtel = "airtel_rev1.csv"

out_file = "supamoto_verified_"+dt.datetime.now().strftime("%Y%m%d")+".csv"

# =============================================================================
# read in data
# =============================================================================

ds_supamoto = pd.read_csv(in_path+in_supamoto)
ds_mtn = pd.read_csv(in_path+in_mtn, dtype={"FINANCIAL_TRANSACTION_ID":"str"})
ds_airtel = pd.read_csv(in_path+in_airtel)

# =============================================================================
# use joins to verify
# =============================================================================

ds_mtn["in_mtn"] = 1
ds_airtel["in_airtel"] = 1

# Names changed in new data, move back to original for downstream work
ds_supamoto.rename(columns={"CRM Transaction IDs":"supamoto_transaction_id",
                            "Transaction Amount":"supamoto_amount",
                            "Wallet Operator":"supamoto_wallet_operator"}, 
                   inplace=True)

supamoto_keep_cols = ["supamoto_transaction_id",
                      "supamoto_amount",
                      "supamoto_wallet_operator"]

ds_supamoto = ds_supamoto.loc[:, supamoto_keep_cols].copy()

ds_check = ds_supamoto.merge(ds_airtel[["transaction_id", "in_airtel"]], 
                             how="left",
                             left_on="supamoto_transaction_id",
                             right_on="transaction_id")

ds_check = ds_check.merge(ds_mtn[["FINANCIAL_TRANSACTION_ID", "in_mtn"]],
                          how="left",
                          left_on="supamoto_transaction_id",
                          right_on="FINANCIAL_TRANSACTION_ID")

ds_check["in_airtel"].fillna(0, inplace=True)
ds_check["in_mtn"].fillna(0, inplace=True)

ds_check["supamoto_verified"] = ds_check.in_airtel + ds_check.in_mtn

ds_check = ds_check.loc[pd.notnull(ds_check["supamoto_transaction_id"]),
                        supamoto_keep_cols+["supamoto_verified"]].copy()

ds_check.to_csv(in_path+out_file, index=False)
