#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: viktoriaschaale

data preparation of raw flat file from REA for grid extension data

input: csv file, separated by semicolon
output: csv file to be uploaded through import wizzard as generic / meters format

"""

from datetime import datetime
import pandas as pd
import re
import tkinter as tk
from tkinter import filedialog

# select csv file to be used as input
root = tk.Tk()
# root.withdraw()
file_path = filedialog.askopenfilename()

df = pd.read_csv(file_path, sep=",", low_memory=False)

# rename columns
df.rename(
    columns={
        "wr": "account_external_id",
        "town": "customer_location_area_4",
        # "address": "customer_address",
        "phone_number_1": "customer_phone",
        "service_point_date": "installation_date",
    },
    inplace=True,
)

# duplicate account_external_id twice
df["device_external_id"] = df["account_external_id"]
df["serial_number"] = df["account_external_id"]

# extract gender from title
df.loc[df.title.str.contains("Ms|Miss|Mrs") == True, "customer_gender"] = "F"
df.loc[df.title.str.contains("Mr.$") == True, "customer_gender"] = "M"

# duplicate customer_name for customer_external_id
df["customer_external_id"] = df["customer_name"] + df["account_external_id"]

# customer_address: concat street & address
df["customer_address"] = df["street"] + df["address"]

# format date
df["installation_date"] = pd.to_datetime(
    df["installation_date"].astype(str), format="%Y-%m-%d"
)

# map customer_category based on work_request_type
df.loc[
    df.work_request_type == "AGRIC MD (16 - 300) KVA", "customer_category"
] = "Agriculture"
df.loc[
    df.work_request_type == "AGRIC METERED COMM (15)  KVA", "customer_category"
] = "Agriculture"
df.loc[
    df.work_request_type == "AGRIC UNMET MD (16 - 300) KVA",
    "customer_category",
] = "Agriculture"
df.loc[
    df.work_request_type == "METERED COMMERCIAL (15)  KVA", "customer_category"
] = "Commercial"
df.loc[
    df.work_request_type == "METERED RESIDENTIAL", "customer_category"
] = "Residential"
df.loc[
    df.work_request_type == "METERED ZESCO STAFF", "customer_category"
] = "Residential"
df.loc[
    df.work_request_type == "SOCIAL SERVICES", "customer_category"
] = "Institutional"
df.loc[
    df.work_request_type == "UNMETERED COMMERCIAL (15)  KVA",
    "customer_category",
] = "Commercial"
df.loc[
    df.work_request_type == "UNMETERED RESIDENTIAL", "customer_category"
] = "Residential"

# map payment_plan
df.loc[
    df.work_request_type == "AGRIC MD (16 - 300) KVA", "payment_plan"
] = "MD"
df.loc[
    df.work_request_type == "AGRIC METERED COMM (15)  KVA", "payment_plan"
] = "Metered"
df.loc[
    df.work_request_type == "AGRIC UNMET MD (16 - 300) KVA", "payment_plan"
] = "Unmetered"
df.loc[
    df.work_request_type == "MD CAPACITY (16 - 300) KVA", "payment_plan"
] = "MD"
df.loc[
    df.work_request_type == "METERED COMMERCIAL (15)  KVA", "payment_plan"
] = "Metered"
df.loc[
    df.work_request_type == "METERED RESIDENTIAL", "payment_plan"
] = "Metered"
df.loc[
    df.work_request_type == "METERED ZESCO STAFF", "payment_plan"
] = "Metered"
df.loc[
    df.work_request_type == "UNMETERED COMMERCIAL (15)  KVA", "payment_plan"
] = "Unmetered"
df.loc[
    df.work_request_type == "UNMETERED RESIDENTIAL", "payment_plan"
] = "Unmetered"

# new columns
df["customer_country"] = "ZM"
df["is_test"] = False
df["manufacturer"] = "ZESCO"
df["model"] = "ESAP"
# new columns for grids table
df["grid_name"] = "Zambia National Grid"
df["grid_operator_company"] = "ZESCO"
# uid columns
df["device_uid"] = "1_" + df["manufacturer"] + "_" + df["serial_number"]
df["customer_uid"] = "1_adhoc_" + df["customer_external_id"].astype(str)
df["account_uid"] = "1_adhoc_" + df["account_external_id"].astype(str)

# export csv
df.to_csv(
    file_path
    + "/"
    + datetime.now().strftime("%y%m%d_%H%M")
    + "_meters_for_wizzard.csv"
)
