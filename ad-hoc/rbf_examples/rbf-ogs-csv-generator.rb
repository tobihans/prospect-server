### ( 1 ) ADJUST THOSE CONSTANTS TO YOU LIKING

FIRST = 1
LAST = 50
BRAND = "Manuf"
MODEL = "Solarlight 5000"
PAYMENT_AMOUNT = "150000"

### ( 2 ) COPY PASTE EVERYTHING INTO A RAILS CONSOLE `bundle exec rails c`

h = "given_name,surname,national,id_number,gender,household_head,contact_phone,alternative_contact_phone,region,district,division,parish,village,gps_location,photo_upload,product_brand,product_model,product_serial_number,product_secondary_serial_number,date,paygo_or_cash_customer,payment_account_id,down_payment,duration,date_of_transaction,time_of_transaction,payment_amount,type_of_payment,provider_transaction_id"
l = "PeterXXX,PanXXX,National ID,ABCDEFGHIXXX,F,true,017834XXX,017835XXX,Northern,Gulu,Omel,Kuru,Kuru,,https://app.prospect.energy/assets/themes/prospect/logo-1741bfabe788ee6194e081c4a82268d2fa58a943877a53a642f992b1a09a60f0.png,#{BRAND},#{MODEL},SER-XXX,SER2-XXX,2024-11-05,cash,PAY-XXX,,,2024-11-05,,#{PAYMENT_AMOUNT},bank,"

x = (FIRST..LAST).map do
  s = _1.to_s.rjust(3, '0')
  l.gsub "XXX", s
end

puts "----------------------------"

puts [h] + x

puts "----------------------------"

### ( 3 ) COPY THE OUTPUT BETWEEN --------- INTO A CSV FILE
### ( 4 ) BULK UPLOAD CSV INTO BACKOFFICE SOURCE

