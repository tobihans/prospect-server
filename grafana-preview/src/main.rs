use axum::extract::State;
use axum::http::{Request, StatusCode};
use axum::{
    body::Body,
    http::header,
    response::{AppendHeaders, IntoResponse},
    routing::get,
    Router,
};
use image::{DynamicImage, ImageFormat};
use std::env;
use std::io::Cursor;
use std::process::{Command, Stdio};
use std::sync::Arc;
use std::time::Duration;
use std::{thread, time};
use thirtyfour::common::capabilities::firefox::LogLevel;
use thirtyfour::cookie::time::OffsetDateTime;
use thirtyfour::prelude::*;
use tokio::net::TcpListener;
use tokio::signal;
use tokio::sync::Mutex;
use tokio_util::io::ReaderStream;

// submit button for login
const SUBMIT_BUTTON: &str = "button[type='submit']";
// loading bar div
const LOADING_BAR: &str = "div[aria-label='Panel loading bar']";
// main element we want to take a screenshot from
const MAIN_ELEMENT: &str = "div[class$='page-wrapper']";

const WAIT_TIMEOUT: Duration = time::Duration::from_secs(60);
const INTERVAL: Duration = time::Duration::from_secs(1);

async fn login(driver: &WebDriver, user: String, password: String) -> WebDriverResult<()> {
    driver.find(By::Name("user")).await?.send_keys(user).await?;
    driver
        .find(By::Name("password"))
        .await?
        .send_keys(password)
        .await?;
    driver.find(By::Css(SUBMIT_BUTTON)).await?.click().await
}

fn cookie_expired(cookies: &[Cookie]) -> bool {
    cookies.iter().any(|cookie| {
        if let Some(expiration) = cookie.expires() {
            !(expiration.is_datetime()
                && expiration.datetime().unwrap_or(OffsetDateTime::UNIX_EPOCH)
                    > time::SystemTime::now())
        } else {
            true
        }
    })
}

#[derive(Clone)]
struct AppState {
    driver: Arc<Mutex<WebDriver>>,
    grafana_url: String,
    user: String,
    password: String,
}

#[tokio::main]
async fn main() {
    let user = env::var("GRAFANA_ADMIN_USER").expect("Grafana admin user not defined");
    let password = env::var("GRAFANA_ADMIN_PASSWORD").expect("Grafana admin password not defined");
    let grafana_url = env::var("GRAFANA_EMBED_URL").expect("Grafana url not defined");

    let _child = Command::new("geckodriver")
        .args(["--binary=/opt/firefox/firefox", "--log", "fatal"])
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()
        .expect("Could not spawn geckodriver");

    // give geckodriver a second to start
    thread::sleep(time::Duration::from_secs(1));

    let mut caps = DesiredCapabilities::firefox();
    let _ = caps.set_log_level(LogLevel::Fatal);

    let driver = WebDriver::new("http://127.0.0.1:4444", caps)
        .await
        .expect("Could not connect to geckodriver");

    // simulate full hd screen
    driver
        .set_window_rect(0, 0, 992, 2000)
        .await
        .expect("Could not change driver window");

    let _ = driver.goto(&grafana_url).await;
    let _ = driver.execute("localStorage.setItem('umami.disabled', 1)", Vec::new()).await;

    let app = Router::new().route("/*req", get(preview));

    let driver = Arc::new(Mutex::new(driver.clone()));

    let app = app.with_state(AppState {
        driver,
        grafana_url,
        user,
        password,
    });

    let listener = TcpListener::bind("0.0.0.0:8080").await.unwrap();

    axum::serve(listener, app.into_make_service())
        .with_graceful_shutdown(shutdown_signal())
        .await
        .unwrap();
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    println!("signal received, starting graceful shutdown");
}

async fn preview(State(state): State<AppState>, req: Request<Body>) -> impl IntoResponse {
    let url = format!(
        "{}{}&theme=light&kiosk=tv&refresh=30m",
        state.grafana_url,
        req.uri()
    );

    let driver = state.driver.lock().await;

    let result = get_page_preview(&driver, url, state.user, state.password).await;

    let image = match result {
        Ok(image) => image,
        Err(_) => return (StatusCode::INTERNAL_SERVER_ERROR, Body::empty()).into_response(),
    };

    let mut buffer: Cursor<Vec<u8>> = Cursor::new(Vec::new());
    image.write_to(&mut buffer, ImageFormat::Png).unwrap();
    let bytes: Vec<u8> = buffer.into_inner();

    let stream = ReaderStream::new(Cursor::new(bytes));
    let body = Body::from_stream(stream);

    let headers = AppendHeaders([
        (header::CONTENT_TYPE, "image/png"),
        (
            header::CONTENT_DISPOSITION,
            "attachment; filename=\"preview.png\"",
        ),
    ]);
    (headers, body).into_response()
}

async fn get_page_preview(
    driver: &WebDriver,
    url: String,
    user: String,
    password: String,
) -> WebDriverResult<DynamicImage> {
    driver.goto(url.clone()).await?;

    let cookies = driver.get_all_cookies().await?;

    if cookies.is_empty() || cookie_expired(&cookies) {
        login(driver, user, password).await?;
        // the redirect after login does not seem to work properly
        driver.goto(url).await?;
    }

    // wait for dashboard to load
    driver
        .query(By::Css(MAIN_ELEMENT))
        .wait(WAIT_TIMEOUT, INTERVAL)
        .exists()
        .await?;
    // wait for all panels to load data
    driver
        .query(By::Css(LOADING_BAR))
        .wait(WAIT_TIMEOUT, INTERVAL)
        .not_exists()
        .await?;

    // it could take a second to render all the fancy graphs after the "loading" is done
    thread::sleep(time::Duration::from_secs(1));

    let element = driver.find(By::Css(MAIN_ELEMENT)).await?;
    let image = element.screenshot_as_png().await?;

    let reader = image::ImageReader::new(Cursor::new(image))
        .with_guessed_format()
        .expect("This will never fail using Cursor");

    if let Ok(mut image) = reader.decode() {
        // snap a preview in the middle of the page
        image = image.crop_imm(0, 0, 992, 992);
        Ok(image)
    } else {
        Err(WebDriverError::CustomError(
            "Could not create preview image".into(),
        ))
    }
}
