#!/bin/bash
if [ -z "$GF_DATABASE_NAME" ]; then
  echo "Error: GF_DATABASE_NAME environment variable is not set."
  exit 1
fi

# Extract the database name from the environment variable
dbname="$GF_DATABASE_NAME"

# PostgreSQL commands to create the database
create_db_command="CREATE DATABASE \"$dbname\";"

# Execute the command
psql -c "$create_db_command"

# Check if the database creation was successful
if [ $? -eq 0 ]; then
  echo "Database \"$dbname\" created successfully."
else
  echo "Error: Failed to create database \"$dbname\"."
fi